$Id: readme.txt 1490 2007-10-22 15:33:15Z loom $


:: WICHTIG 

Die SQL-Dateien müssen mit den Scripten erstellt werden, die sich in 
diesem Verzeichnis befinden!



Dies bringt folgende Vorteile mit sich:
- Der Dump ist standardisiert (essentiell, um verschiedene Dumps mit einem 
  Diff zu vergleichen).
- Die Tabellen sind alphabetisch sortiert.
- Die Datensätze sind nach ID sortiert.
- Temporäre Daten (z.B. Sessions) werden nicht gespeichert.
- Es erfolgt eine automatische (allerdings nicht perfekte) Aufteilung der Daten 
  in System (flip.sql) und Testing (flip-testing.sql).
- Das Starten der .cmd-Datei geht eindeutig schneller als das Bedienen von 
  PhpMyAdmin ;-)
  

:: Dieses Verzeichnis enthält:

make-sql-file.*
  Scripte, um die *.sql-Dateien aus einer Datenbank zu generieren.

exec-sql-file.*
  Scripte, um die *.sql-Dateien in eine Datenbank zu schreiben.

Siehe auch:
  /tools/sql-file/readme.txt
  
