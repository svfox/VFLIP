<?php
require('nusoap.php');

function hallo($inputString) {
  if (is_string($inputString)){
    return "Hallo " . $inputString . "!";
  } else {
    return new soap_fault('Client', ''. 'Parameter ist kein String');
  }
}

$s = new soap_server;
$s->register('hallo');

$s->service($HTTP_RAW_POST_DATA);

?>
