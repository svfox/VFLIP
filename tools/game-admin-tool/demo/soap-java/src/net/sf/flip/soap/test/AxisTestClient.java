package net.sf.flip.soap.test;

import net.sf.flip.soap.test.client.*;
import net.sf.flip.soap.test.client.HalloBean;

import java.net.URL;

import org.apache.axis.client.Service;
import org.apache.axis.client.Call;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;


/**
 *
 */
public class AxisTestClient {

    private static final String ENDPOINT = "http://127.0.0.1:5555/axis/services/Hallo";
    //private static final String ENDPOINT = "http://127.0.0.1/soap/soap.php";
    private static final String MYSTRING = "Welt";

    public static void main(String[] args){
        try{
            callViaServiceLocator();
            callViaInvoke();
        }
        catch (Exception e){
            System.err.println(e.toString());
            e.printStackTrace();
        }
    } //main(...)

    private static void callViaServiceLocator() throws Exception{
        HalloServiceLocator loc = new HalloServiceLocator();
        net.sf.flip.soap.test.client.Hallo hallo = loc.getHallo(new URL(ENDPOINT));
        HalloBean b = hallo.hallo(MYSTRING);
        System.out.println("ViaServiceLocato: r" + b.getTestString() + " mit " + b.getTestInt());
    }

    private static void callViaInvoke() throws Exception{
        Service service = new Service();
        Call call = (Call) service.createCall();
        call.setTargetEndpointAddress(ENDPOINT);
        call.setOperationName(new QName("hallo"));
        call.registerTypeMapping(net.sf.flip.soap.test.client.HalloBean.class,
                new QName("http://test.soap.flip.sf.net", "HalloBean"),
                org.apache.axis.encoding.ser.BeanSerializerFactory.class,
                org.apache.axis.encoding.ser.BeanDeserializerFactory.class, false);
        call.addParameter(new javax.xml.namespace.QName("", "name"),
                new QName("http://schemas.xmlsoap.org/soap/encoding/", "string"),
                String.class, ParameterMode.IN);
        call.setReturnType(new QName("http://test.soap.flip.sf.net", "HalloBean"));

        net.sf.flip.soap.test.client.HalloBean b = (net.sf.flip.soap.test.client.HalloBean)call
                 .invoke(new Object[] {MYSTRING});
        System.out.println("ViaInvoke:" + b.getTestString() + " mit " + b.getTestInt());
    }
}
