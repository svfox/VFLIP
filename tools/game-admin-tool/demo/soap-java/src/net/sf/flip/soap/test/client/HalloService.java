/**
 * HalloService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2beta3 Aug 17, 2004 (09:50:21 EDT) WSDL2Java emitter.
 */

package net.sf.flip.soap.test.client;

public interface HalloService extends javax.xml.rpc.Service {
    public java.lang.String getHalloAddress();

    public net.sf.flip.soap.test.client.Hallo getHallo() throws javax.xml.rpc.ServiceException;

    public net.sf.flip.soap.test.client.Hallo getHallo(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
