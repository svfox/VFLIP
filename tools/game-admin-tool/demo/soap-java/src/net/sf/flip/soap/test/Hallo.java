package net.sf.flip.soap.test;

/**
 * klasse die angeboten wird
 */
public class Hallo {
    public static HalloBean hallo(String name){

        HalloBean bean = new HalloBean();
        bean.setTestString(name);
        bean.setTestInt(30);
        return bean;
    }
}
