package net.sf.flip.soap.test;

import org.apache.axis.transport.http.SimpleAxisServer;
import org.apache.axis.client.Call;
import org.apache.axis.message.SOAPBodyElement;

import java.net.ServerSocket;
import java.io.InputStream;
import java.util.Vector;


/**
 * Startet den AxisWebServer
 */
public class AxisTestServer extends SimpleAxisServer{

    private final static int PORT = 5555;
    private final static String WSDDFILE = "server.wsdd";
    
    /**
     * Los gehts
     */
    public static void main(String args[]) {
        SimpleAxisServer simpleServer = new SimpleAxisServer();
        ServerSocket socket;
        try {
            socket = new ServerSocket(PORT);
        } catch (Exception e){
            System.out.println("Fehler beim bindes des Ports");
            return;
        }
        simpleServer.setServerSocket(socket);
        try{
            simpleServer.start();
        } catch (Exception e) {
            System.out.println("Fehler beim starten des Servers. - "+e.getMessage());
            e.printStackTrace();
            return;
        }

        try {
            System.out.println("deploy:" + deployConfig());
        } catch (Exception e) {
            System.out.println("Fehler beim deployen - " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static String deployConfig() throws Exception {
        Call call = new Call("http://localhost:" + PORT + "/");
        call.setUseSOAPAction(true);
        call.setSOAPActionURI("AdminService");

        InputStream in = ClassLoader.getSystemResourceAsStream(WSDDFILE);

        Object[] params = new Object[] { new SOAPBodyElement(in) };
        Vector result = (Vector) call.invoke(params);

        in.close();

        if (result == null || result.isEmpty())
            throw new Exception("kein Ruegckgabewert beim deployen.");

        SOAPBodyElement body = (SOAPBodyElement) result.elementAt(0);
        return body.toString();
    }



}
