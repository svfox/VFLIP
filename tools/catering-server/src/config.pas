unit config;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, StdCtrls, ExtCtrls;

type
  Tconfig_form = class(TForm)
    MainMenu1: TMainMenu;
    Beenden1: TMenuItem;
    bernehmen1: TMenuItem;
    Panel1: TPanel;
    pg_config: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    e_mysql_host: TLabeledEdit;
    e_mysql_port: TLabeledEdit;
    e_mysql_database: TLabeledEdit;
    e_mysql_username: TLabeledEdit;
    e_mysql_password: TLabeledEdit;
    Panel3: TPanel;
    tb_interval: TTrackBar;
    Label1: TLabel;
    l_interval: TLabel;
    e_template_path: TLabeledEdit;
    tb_print_count: TTrackBar;
    l_printcount: TLabel;
    Label2: TLabel;
    procedure bernehmen1Click(Sender: TObject);
    procedure Beenden1Click(Sender: TObject);
    procedure tb_intervalChange(Sender: TObject);
    procedure tb_print_countChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  config_form: Tconfig_form;

implementation

uses main;

{$R *.dfm}



procedure Tconfig_form.bernehmen1Click(Sender: TObject);
begin
  main_form.WriteConfig();
end;

procedure Tconfig_form.Beenden1Click(Sender: TObject);
begin
  close;
end;

procedure Tconfig_form.tb_intervalChange(Sender: TObject);
begin
  l_interval.Caption := VarToStr(tb_interval.position) + ' Minute(n)';
end;

procedure Tconfig_form.tb_print_countChange(Sender: TObject);
begin
  l_printcount.Caption := VarToStr(tb_print_count.position) + ' Ausdruck(e)';
end;

end.
