object main_form: Tmain_form
  Left = 441
  Top = 463
  Width = 761
  Height = 561
  Caption = 'FLIP - Catering Server'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 241
    Height = 515
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object status: TListBox
      Left = 0
      Top = 0
      Width = 273
      Height = 515
      Align = alLeft
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 241
    Top = 0
    Width = 512
    Height = 515
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object wb_preview: TWebBrowser
      Left = 0
      Top = 40
      Width = 512
      Height = 475
      Align = alBottom
      TabOrder = 0
      OnDocumentComplete = wb_previewDocumentComplete
      ControlData = {
        4C000000EB340000183100000000000000000000000000000000000000000000
        000000004C000000000000000000000001000000E0D057007335CF11AE690800
        2B2E126208000000000000004C0000000114020000000000C000000000000046
        8000000000000000000000000000000000000000000000000000000000000000
        00000000000000000100000000000000000000000000000000000000}
    end
    object Button1: TButton
      Left = 101
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Manuell'
      TabOrder = 1
      OnClick = Button1Click
    end
    object b_startstop: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Start Timer'
      TabOrder = 2
      OnClick = b_startstopClick
    end
  end
  object MainMenu1: TMainMenu
    Left = 688
    object Datei1: TMenuItem
      Caption = '&Datei'
      object Einstellungen1: TMenuItem
        Caption = '&Einstellungen'
        OnClick = Einstellungen1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Beenden1: TMenuItem
        Caption = '&Beenden'
        OnClick = Beenden1Click
      end
    end
    object N2: TMenuItem
      Caption = '?'
    end
  end
  object t_interval: TTimer
    Enabled = False
    OnTimer = t_intervalTimer
    Left = 720
  end
end
