unit template;

interface

uses classes, Variants, StrUtils;

type
  Titem = record
    name:  String;
    price: Double;
    count: Integer;
  end;

  Tuserdata = record
    nickname:  String;
    name:      String;
    seat:      String;
    ordertime: String;
    counter:   Integer;
  end;

  Ttemplate = class(TObject)
  private
    filename: String;
    data:     TStringList;
    items:    TList;
    userdata: Tuserdata;

   public
    constructor Create(_filename: String);
    destructor  Destroy();
    function    parse(): TStringList;
    procedure   addItem(name: String; price: Double; count: Integer);
    procedure   setUserdata(nickname, name, seat, ordertime: String; counter: Integer);
    procedure   clear();
  end;


implementation

uses SysUtils;


constructor Ttemplate.Create(_filename: String);
begin
  filename := _filename;
  data     := TStringlist.Create();
  items    := TList.Create();
  
  if (FileExists(filename)) then begin
    data.LoadFromFile(_filename);
  end;

end;

destructor Ttemplate.Destroy();
var i: integer;
    p: ^Titem;
begin
  for i := 0 to items.Count-1 do begin
    p := items.Items[i];
    Dispose(p);
  end;
  data.Free;
  items.Free;
  inherited Destroy;
end;

procedure Ttemplate.clear();
begin
  data.Clear();
  items.Clear();
  with userdata do begin
    nickname := '';
    name := '';
    seat := '';
    ordertime := '';
  end;
end;

{
  parse(): TStringList;

  Parsen des Templates
  Gültige Variablem im header und footerbereich sind
  %nickname% -> Nickname des Teilnehmers
  %name%     -> Zu- und Vorname des Teilnehmers
  %seat%     -> Sitzplatz
  %order%    -> Zeit der Bestellung

  nur gültig im footerbereich sind
  %total%    -> Gesammtsumme der Bestellung

  der rechnungsbereich wird durch
  %list%
   ...
  %list%
  abgegrenzt, die %list% Tags, sollten eine eigene Zeile belegen
  und am Anfang der Zeile stehen
  innerhalb der %list% tags, sind folgende Variablen gültig:
  %item%     -> Name des bestellten Artikels
  %price%    -> Einzelpreis
  %count%    -> bestellte Menge
  %subtotal% -> Zwischensumme (price * count)
}
function Ttemplate.parse(): TStringList;
var  output:  TStringList;
     loop:    TStringList;
     i, j, k: Integer;
     p:       ^Titem;
     tmp:     String;
     total:   Double;
begin
  output := TStringList.Create();
  loop   := TStringList.Create();

  total := 0;
  i := 0;
  //zeilenweise das template parsen
  while (i < data.Count) do begin
    tmp := data.Strings[i];

    //sind wir bei den items in der wiederholung?
    if (pos('%list%', tmp) <> 0) then begin
      //kompletten wiederholungsblock auslesen
      inc(i); //zähler aktualisieren
      tmp := data.Strings[i];
      while (pos('%list%', tmp) = 0) do begin
        loop.Add(tmp);
        inc(i);
        tmp := data.Strings[i];
      end;
      //loop ist nun komplett ausgelesen

      // die items aus der TList einfügen
      for j := 0 to items.Count-1 do begin
        p := items.Items[j];
        total := total + (p^.price * p^.count);
        for k := 0 to loop.Count-1 do begin
          //variablen ersetzen
          tmp := loop.Strings[k];
          tmp := AnsiReplaceText(tmp, '%item%',     p^.name);
          tmp := AnsiReplaceText(tmp, '%price%',    floattostrF(p^.price, ffFixed, 4, 2));
          tmp := AnsiReplaceText(tmp, '%count%',    VarToStr(p^.count));
          tmp := AnsiReplaceText(tmp, '%subtotal%', floattostrF(p^.price * p^.count, ffFixed, 4, 2));
          output.Add(tmp);
        end;

      end;

    end else begin
      //ersetzen von %nickname%, %seat% und %name
      tmp := AnsiReplaceText(tmp, '%nickname%',  userdata.nickname);
      tmp := AnsiReplaceText(tmp, '%name%',      userdata.name);
      tmp := AnsiReplaceText(tmp, '%seat%',      userdata.seat);
      tmp := AnsiReplaceText(tmp, '%ordertime%', userdata.ordertime);
      tmp := AnsiReplaceText(tmp, '%total%',     floattostrF(total, ffFixed, 4, 2));
      tmp := AnsiReplaceText(tmp, '%counter%',   VarToStr(userdata.counter));
      //geparste zeile zum output hinzufügen
      output.Add(tmp);
    end;
    inc(i);
  end;
  loop.Free();
  result := output;
end;


procedure Ttemplate.addItem(name: String; price: Double; count: Integer);
var  p: ^Titem;
begin
  new(p);
  p^.name  := name;
  p^.price := price;
  p^.count := count;
  items.Add(p);
end;

procedure Ttemplate.setUserdata(nickname, name, seat, ordertime: String; counter: Integer);
begin
  userdata.nickname  := nickname;
  userdata.name      := name;
  userdata.seat      := seat;
  userdata.ordertime := ordertime;
  userdata.counter   := counter;
end;

end.
