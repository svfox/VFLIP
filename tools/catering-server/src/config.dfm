object config_form: Tconfig_form
  Left = 467
  Top = 278
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'FLIP - Catering System - Konfiguration'
  ClientHeight = 308
  ClientWidth = 364
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 364
    Height = 308
    Align = alClient
    TabOrder = 0
    object pg_config: TPageControl
      Left = 1
      Top = 1
      Width = 362
      Height = 306
      ActivePage = TabSheet1
      Align = alClient
      MultiLine = True
      Style = tsFlatButtons
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Allgemein'
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 354
          Height = 275
          Align = alClient
          TabOrder = 0
          object Label1: TLabel
            Left = 16
            Top = 8
            Width = 107
            Height = 13
            Caption = 'Aktualisierungsintervall'
          end
          object l_interval: TLabel
            Left = 8
            Top = 56
            Width = 145
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'l_interval'
          end
          object l_printcount: TLabel
            Left = 8
            Top = 176
            Width = 145
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'l_interval'
          end
          object Label2: TLabel
            Left = 16
            Top = 128
            Width = 104
            Height = 13
            Caption = 'Anzahl der Ausdrucke'
          end
          object tb_interval: TTrackBar
            Left = 8
            Top = 24
            Width = 150
            Height = 33
            Max = 120
            Min = 1
            Frequency = 5
            Position = 10
            TabOrder = 0
            OnChange = tb_intervalChange
          end
          object e_template_path: TLabeledEdit
            Left = 16
            Top = 96
            Width = 137
            Height = 21
            EditLabel.Width = 114
            EditLabel.Height = 13
            EditLabel.Caption = 'Relativer Template Pfad'
            TabOrder = 1
          end
          object tb_print_count: TTrackBar
            Left = 8
            Top = 144
            Width = 150
            Height = 33
            Position = 1
            TabOrder = 2
            OnChange = tb_print_countChange
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'MySQL'
        ImageIndex = 1
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 354
          Height = 275
          Align = alClient
          TabOrder = 0
          object e_mysql_host: TLabeledEdit
            Left = 8
            Top = 24
            Width = 137
            Height = 21
            EditLabel.Width = 64
            EditLabel.Height = 13
            EditLabel.Caption = 'e_mysql_host'
            TabOrder = 0
            Text = 'localhost'
          end
          object e_mysql_port: TLabeledEdit
            Left = 8
            Top = 64
            Width = 137
            Height = 21
            EditLabel.Width = 62
            EditLabel.Height = 13
            EditLabel.Caption = 'e_mysql_port'
            TabOrder = 1
            Text = '3306'
          end
          object e_mysql_database: TLabeledEdit
            Left = 8
            Top = 184
            Width = 137
            Height = 21
            EditLabel.Width = 88
            EditLabel.Height = 13
            EditLabel.Caption = 'e_mysql_database'
            TabOrder = 2
            Text = 'flip'
          end
          object e_mysql_username: TLabeledEdit
            Left = 8
            Top = 104
            Width = 137
            Height = 21
            EditLabel.Width = 90
            EditLabel.Height = 13
            EditLabel.Caption = 'e_mysql_username'
            TabOrder = 3
            Text = 'root'
          end
          object e_mysql_password: TLabeledEdit
            Left = 8
            Top = 144
            Width = 137
            Height = 21
            EditLabel.Width = 89
            EditLabel.Height = 13
            EditLabel.Caption = 'e_mysql_password'
            TabOrder = 4
          end
        end
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 416
    Top = 32
    object Beenden1: TMenuItem
      Caption = '&Schlie'#223'en'
      OnClick = Beenden1Click
    end
    object bernehmen1: TMenuItem
      Caption = '&'#220'bernehmen'
      OnClick = bernehmen1Click
    end
  end
end
