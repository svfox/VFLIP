<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Unbenanntes Dokument</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>

<table width="400"  border="1" cellspacing="1" cellpadding="1">
  <tr>
    <td width="14%"><strong>Name:</strong></td>
    <td width="86%">%nickname% - %name%</td>
  </tr>
  <tr>
    <td><strong>Sitzplatz:</strong></td>
    <td>%seat%</td>
  </tr>
  <tr>
    <td><strong>Bestellzeit:</strong></td>
    <td>%ordertime% Uhr</td>
  </tr>
</table>
<br>
<br>
<table width="500"  border="1" cellspacing="1" cellpadding="1">
  <tr>
    <td><strong>Artikel:</strong></td>
    <td><strong>Einzelpreis:</strong></td>
    <td><strong>Anzahl:</strong></td>
    <td><strong>Zwischensumme:</strong></td>
  </tr>
%list%
  <tr>
    <td>%item%</td>
    <td>%price% EUR</td>
    <td>%count%</td>
    <td>%subtotal% EUR</td>
  </tr>
%list%
  <tr>
    <td colspan="3"><div align="right">Zu zahlender Betrag: </div></td>
    <td>%total% EUR</td>
  </tr>
</table>
<br>
Nummer der Bestellung: %counter%
</body>
</html>