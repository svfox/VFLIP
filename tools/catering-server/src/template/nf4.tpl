<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Unbenanntes Dokument</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
table {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.oben {
	line-height: 40px;
	font-size: 18px;
}
.rechnung {
	font-family: courier, Arial, Helvetica, sans-serif;
	line-height: 30px;
	font-size: 16px;
}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<font size="+5"><strong>%counter%</strong></font><br><br>
<table width="437" height="620" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10" colspan="3"><img src="i/t.gif" width="1" height="1"></td>
  </tr>

  <tr>
    <td width="10">&nbsp;</td>
    <td valign="top"><table width="417" height="596" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="140" valign="top">
            <table width="417" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><img src="i/bestellung.gif" width="332" height="41"><br>
                  <table width="360" border="0" cellpadding="0" cellspacing="0" class="oben">
                    <tr>
                      <td width="90"><img src="i/t.gif" width="8" height="8">Nick</td>
                      <td width="270"><b>%nickname%</b></td>
                    </tr>
                    <tr>
                      <td><img src="i/t.gif" width="8" height="8">Name</td>
                      <td>%name%</td>
                    </tr>
                    <tr>
                      <td><img src="i/t.gif" width="8" height="8">Platz</td>
                      <td>%seat%</td>
                    </tr>
                    <tr>
                      <td><img src="i/t.gif" width="8" height="8">Uhrzeit</td>
                      <td>%ordertime% Uhr</td>
                    </tr>
                  </table> </td>
                <td width="85" valign="top"><img src="i/nfrag4logo.gif" width="85" height="89"></td>

              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td valign="top"><hr width="400" size="2">
            <table width="417" border="0" cellspacing="0" cellpadding="0">
              <tr bgcolor="#000000">
                <td width="246"><strong><font color="#FFFFFF">Artikel</font></strong></td>

                <td width="43"><div align="center"><strong><font color="#FFFFFF">Menge</font></strong></div></td>
                <td width="62"><strong><font color="#FFFFFF">Preis/Stck</font></strong></td>
                <td width="66"><strong><font color="#FFFFFF">Zwischens.</font></strong></td>
              </tr>
%list%
              <tr class="rechnung">
                <td>%item%</td>
                <td><div align="center">%count%</div></td>
                <td><div align="right">%price%</div></td>
                <td><div align="right">%subtotal%</div></td>
              </tr>
%list%
             <tr bgcolor="#000000">
                <td colspan="4"><img src="i/t.gif" width="1" height="5"></td>

              </tr>
              <tr class="rechnung">
                <td colspan="2"><em>Summe</em></td>
                <td colspan="2"><div align="right" class="oben"><strong>%total%</strong></div></td>
              </tr>
            </table></td>
        </tr>
        <tr>

          <td height="30"><div align="right"><em>Alle Preise verstehen sich in
              Euro, inkl. 16% Mehrwertsteuer.</em><br>
              <img src="i/danke.gif" width="417" height="16"></div></td>
        </tr>
      </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td height="10" colspan="3"><img src="i/t.gif" width="1" height="1"></td>
  </tr>

</table>
</body>
</html>