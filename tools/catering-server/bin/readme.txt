:: Hinweis
Dieses ist ein erstes Testrelease welches nur das vorgegebene
Template parst und im internen Webbrowser zur Ansicht öffnet.

Zur Ausführung benötigt man die dll libmysql.dll, zu finden
unter http://www.fichtner.net/delphi/mysql.delphi.phtml
Diese muss lediglich in das Programmverzeichnis entpackt werden.

Im Cateringscript muss die Warteliste aktiviert sein, damit der
Server diese Bestellungen abarbeitet.
