$Id: readme.txt 1490 2007-10-22 15:33:15Z loom $


:: WICHTIG: Siehe auch: /db/readme.txt


:: Dieses Verzeichnis enthält:

flip.sql
  Die rudimentäre Datenbank, mit der das FLIP lauffähig ist.
  Sie enthält alle Tabellenstrukturen und einige wichtige Datensätze.
  
flip-testig.sql
  Eine "Datenbank-Extension", welche Datensätze (keine Tabellenstrukturen!)
  enthält, die nützlich sind, um die Funktionalität des FLIPs zu
  Demonstrieren oder zu Testen.
  
make-sql-file.*
  Scripte, um die *.sql-Dateien aus einer Datenbank zu generieren.
  
make-sql-file.tables.php
  Die Konfigurationsdatei, in welcher festgelegt wird, welche Datensätze
  in der flip.sql, der flip-testing.sql oder gar nicht gespeichert werden.  
  
exec-sql-file.*
  Scripte, um die *.sql-Dateien in eine Datenbank zu schreiben.