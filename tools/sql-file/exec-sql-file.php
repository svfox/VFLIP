<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: exec-sql-file.php 1490 2007-10-22 15:33:15Z loom $
 * @copyright © 2001-2004 The FLIP Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Default-config **/
$ConfigFileName = "sql-file.config.php";
$SysDBFileName = "./../../db/flip.sql";
$TestingDBFileName = "./../../db/flip-testing.sql";
$ConfigFile = "<?php
\$cfg[\"host\"] = \"localhost\";
\$cfg[\"db\"] = \"flip\";
\$cfg[\"user\"] = \"root\";
\$cfg[\"pass\"] = \"\";
\$cfg[\"prefix\"] = \"\";

\$cfg[\"exec_system\"] = true;
\$cfg[\"exec_testing\"] = true;
\$cfg[\"exclude_tables\"] = array(\"flip_diesegibgarnicht\",\"flip_foo_bar_table\"); 
?>";

if(!function_exists("file_put_contents"))
{
	/**
	 * Ersatz für alte PHP Version (< 5.0)
	 *
	 * @see file_put_contents()
	 */
	function file_put_contents($Filename,$Data)
	{
		$f = fopen($Filename,"wb");
		$r = fwrite($f,$Data);
		fclose($f);
		return $r;
	}
}

// config laden
if(!is_file($ConfigFileName)) file_put_contents($ConfigFileName,$ConfigFile);
$cfg = array();
include($ConfigFileName);

// verbindung zur datenbank herstellen
$r = mysql_connect($cfg["host"],$cfg["user"],$cfg["pass"]);
if(!$r) die("\nKann Verbindung zur Datenbank nicht herstellen :/ ".mysql_error()."\n");
if(!mysql_select_db($cfg["db"])) die("\nKann Datenbank nicht auswaehlen :/ ".mysql_error()."\n");
// UTF-8 korrekt importieren
mysql_query('SET NAMES \'utf8\';');
$variables = mysql_query('SHOW SESSION VARIABLES');
$characterSet = null;
$continue = true;
while(($variable = mysql_fetch_assoc($variables)) && $continue) {
	if($variable['Variable_name'] == 'character_set_connection' || $variable['Variable_name'] == 'character_set'/*Mysql 4.0*/) {
		$characterSet = $variable['Value'];
		$continue = false;
	}
}
if($characterSet == 'utf8') {
	$convertToLatin1 = false;
} else {
	$convertToLatin1 = true;
}
echo 'Importiere Daten als '. ($convertToLatin1 ? 'latin1' : 'utf8') .' (Connection ist '.var_export($characterSet,true).")\n";
ob_implicit_flush();

if($cfg["exec_system"])
{
	echo "\nFuehre $SysDBFileName aus:";
	ExecFile($SysDBFileName);
}

if($cfg["exec_testing"])
{
	echo "\nFuehre $TestingDBFileName aus:";
	ExecFile($TestingDBFileName);
}

function ExecFile($File)
{
	global $cfg, $convertToLatin1;
	if(!is_file($File))
	{
		echo "\n$File wurde nicht ausgeführt, da sie nicht gefunden werden konnte.\n";
		return;
	}
	$cmds = array();
	$i = 0;
	foreach(file($File) as $line)
	{
		$line = trim($line);
		if($convertToLatin1) {
			$line = utf8_decode($line);
		}
		if($line{0} == "#") continue;

		$c .= "$line ";
		if($line{strlen($line)-1} == ";")
		{
			Query(str_replace("\$prefix\$",$cfg["prefix"], $c));
			if(($i++ % 20) == 0) echo ".";
			$c = "";
		}
	}
	echo "\nEs wurden $i Befehle erfolgreich ausgefuehrt.\n";
}

function Query($q)
{
	if(!mysql_query($q))
	trigger_error("Fehler: ".mysql_error()." Query: $q",E_USER_WARNING);
}


?>
