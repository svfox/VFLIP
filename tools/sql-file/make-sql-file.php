<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: make-sql-file.php 1390 2007-04-09 11:46:22Z loom $
 * @copyright (c) 2001-2004 The FLIP Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/
 
 
$ConfigFileName = "sql-file.config.php";
$SysDBFileName = "./../../db/flip.sql";
$TestingDBFileName = "./../../db/flip-testing.sql";
//!!! Dies sind Defaultwerte f&uuml;r die Configdatei, welche erstellt wird falls sie nicht exisitert.
//!!! Bitte die Datei $ConfigFileName bearbeiten!
$ConfigFile = "<?php
\$cfg[\"host\"] = \"localhost\";
\$cfg[\"db\"] = \"flip\";
\$cfg[\"user\"] = \"root\";
\$cfg[\"pass\"] = \"\";
\$cfg[\"prefix\"] = \"\"; 

\$cfg[\"exec_system\"] = true;
\$cfg[\"exec_testing\"] = true;
\$cfg[\"exclude_tables\"] = array(\"flip_diesegibgarnicht\",\"flip_foo_bar_table\"); 
?>";

if(!function_exists("file_put_contents"))
{
  function file_put_contents($Filename,$Data)
  {
    $f = fopen($Filename,"wb");
    $r = fwrite($f,$Data);
    fclose($f);  
    return $r;
  }
}
 
// config laden
if(!is_file($ConfigFileName)) file_put_contents($ConfigFileName,$ConfigFile);
$cfg = array();
include($ConfigFileName);
include("make-sql-file.tables.php");

// verbindung zur datenbank herstellen
$r = mysql_connect($cfg["host"],$cfg["user"],$cfg["pass"]);
if(!$r) die("\nKann Verbindung zur Datenbank nicht herstellen :/ ".mysql_error()."\n");
if(!mysql_select_db($cfg["db"])) die("\nKann Datenbank nicht auswaehlen :/ ".mysql_error()."\n"); 

$TableStatus = array();
$r = mysql_query("SHOW TABLE STATUS;");
while($a = mysql_fetch_array($r)) $TableStatus[$a[0]] = $a;

foreach($TableContent as $k => $v) {
  //add prefix
  $pref_k = $cfg["prefix"]. $k;
  unset($TableContent[$k]);
  $TableContent[$pref_k] = $v;
  $k = $pref_k;
  
  if(!isset($TableStatus[$k]))
  {
    if($v == "deleted") $TableStatus[$k] = array();
    else trigger_error("Die in make-sql-file.tables.php angegebene Tabelle `$k` existiert nicht.",E_USER_WARNING);
  }
}
  
foreach($cfg["exclude_tables"] as $t) unset($TableStatus[$t]);


ksort($TableStatus);
Append("Sys|Testing","# --------------------------- make-sql-file SQL Dump --------------------------- #");
Append("Sys|Testing","# \$Id: make-sql-file.php 1390 2007-04-09 11:46:22Z loom $");
Append("Sys|Testing","# Erstellungszeit: ". date("Y-m-d H:i"));
Append("Sys","# Type: System");
Append("Sys","# Dieser Dump enthaelt alle Daten und Tabellenstrukturen, die das FLIP zum laufen benoetigt.");
Append("Testing","# Type: Testing");
Append("Testing","# Dieser Dump enthaelt Daten, welche die aus $SysDBFileName um Debug/Test-Daten ergaenzen.");
Append("Sys|Testing","# ------------------------------------------------------------------------------ #\n");

foreach($TableStatus as $table => $options) {
	$type = (isset($TableContent[$table])) ? $TableContent[$table] : "testing";
	
	echo "[+] Bearbeite Tabelle $table (Typ == $type)...\n";
 
	Append(
		"Sys|Testing", 
		"# --------------------------- " 
			. str_replace("\$prefix\$", "", replace_prefix($table, 0)) 
			. " --------------------------- #"
	);

	if(is_array($type)) {
		Append("Sys",CreateTable($table));
		Append("Sys|Testing","# Die wichtigsten Datensaetze:");
		AppendContent($table,$type);
	} 
	
	else { 
		switch($type) {
			case("system"):
				Append("Sys",CreateTable($table));
				Append("Sys","# Alle Datensaetze:");
				Append("Testing","# Alle Datensaetze befinden sich in der Datei $SysDBFileName");
				AppendContent($table,$type);
				break;
				    
	    	case("testing"):
				Append("Sys",CreateTable($table));
				Append("Testing","# Alle Datensaetze:");
				Append("Sys","# Alle Datensaetze befinden sich in der Datei $TestingDBFileName");      
				AppendContent($table,$type);
				break;
				
			case("none"):
				Append("Sys",CreateTable($table));
				Append("Sys|Testing","# Diese Datensaetze sind nur Temporaer.");
				AppendContent($table,$type);
	      		break;
	      		
			case("deleted"):
				Append("Sys",CreateTable($table,true));
				Append("Sys|Testing","# Diese Tabelle wird nicht weiter verwendet.");
				break;
			
			default:
				trigger_error("Ungueltiger SpeicherTyp:$Type", E_USER_ERROR);
		}
	}
	
	Append(
		"Sys|Testing", 
		"# --------------------------- /" 
			. str_replace("\$prefix\$", "", replace_prefix($table, 0)) 
			. " -------------------------- #\n\n"
	);
}

Append("Sys|Testing","\n# --- EOF --- #");

function AppendContent($Table, $Type)
{
  if($Type == "none") return;
  
  // eventuell auswahl von datensaetzen zurechtlegen.
  if(is_array($Type))
  {
    $sel = array();
    foreach($Type as $query)
    {
      list($t,$q) = explode("|",$query,2);
      if($q == "*") $q = "SELECT `id` FROM `$Table`;";
      $r = mysql_query($q);
      if(!$r) trigger_error(mysql_error(),E_USER_ERROR);
      while($i = mysql_fetch_assoc($r)) $sel[$t][$i["id"]] = 1;
    }
    $Type = "selection";
  }
  
  // datensaetze einlesen und sortieren.
  $r = mysql_query("SELECT * FROM `$Table`;");
  $a = array();
  while($i = mysql_fetch_assoc($r)) {
    if(isset($i["id"])) {
    	$a[$i["id"]] = $i;
    } else {
    	$a[] = $i;
    }
  }
  
  ksort($a);
  
  // datensaetze schreiben
  switch($Type)
  {
    case("system"):
      foreach($a as $i) Append("Sys", Insert($Table,$i));
      break;
    case("testing"):
      foreach($a as $i) Append("Testing",Insert($Table,$i));
      break;
    case("selection"):
      foreach($a as $i)
      {
        if(isset($sel["system"][$i["id"]])) 
          Append("Sys",Insert($Table,$i));
        elseif(isset($sel["testing"][$i["id"]]))
          Append("Testing",Insert($Table,$i));
        else echo "nix\n";
      }
      break;
    default: trigger_error("Ungueltiger SpeicherTyp:$Type",E_USER_ERROR);
  }
}

function Insert($Table,$Row)
{
  foreach($Row as $k => $v) 
  {
  	
    if(strtolower($k) == "mtime") {
    	$Row[$k] = "NULL";
    } 
    
    elseif(is_null($v)) {
    	$Row[$k] = "NULL";
    }
    
    elseif(is_numeric($v)) {
    	$Row[$k] = utf8_encode($v);
    }
    
    elseif(preg_match('/[\x0-\x8\xB\xC\xE-\x1F]/',$v)) {
    	$Row[$k] = "0x".bin2hex($v);
    }
    
    else {
    	$Row[$k] = "'".addcslashes(utf8_encode($v),"\n\r\t'\\")."'";
    }
  }  
  return "INSERT INTO `".replace_prefix($Table, 0)."` VALUES (".implode(",",$Row).");";
}

function CreateTable($Table,$IsDeleted = false)
{
  $Drop = "DROP TABLE IF EXISTS `".replace_prefix($Table, 0)."`;\n";
  if(!$IsDeleted)
  {
    $q = mysql_query("SHOW CREATE TABLE `$Table`;");
    list($name,$Create) = mysql_fetch_row($q);  
    $Create = replace_prefix($Create, strpos("CREATE TABLE `hier", "hier"));
    return "$Drop$Create;";  
  }
  else return $Drop;
}

function Append($Type,$Data)
{
  global $SysDBFileName,$TestingDBFileName;
  global $FHandles;
  foreach(explode("|",$Type) as $t)
  {
    $f = "";
    if(isset($FHandles[$t])) $f = $FHandles[$t];
    else
    {
      $File = "{$t}DBFileName";
      if(empty($$File)) trigger_error("FEHLER: Kein Dateiname fuer: $File",E_USER_ERROR);
      $f = $FHandles[$t] = fopen($$File,"wb");
    }
    fwrite($f,"$Data\n");
  }
}

function replace_prefix($text, $pos)
{
    global $cfg;
    $len = strlen($cfg["prefix"]);
    if(substr($text, $pos, $len) == $cfg["prefix"])
    	return substr($text, 0, $pos). "\$prefix\$" .substr($text, $pos+$len);
    else
    	return $text;
}
 
?>
