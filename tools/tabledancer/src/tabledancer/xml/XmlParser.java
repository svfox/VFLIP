/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.xml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tabledancer.bean.Block;
import tabledancer.bean.Seat;

/**
 * es gibt 3 verschiedene Listen innerhalb dieses XML-Documentes auf deren
 * interne Nodes man wiederum ?ber eine Aufz?hlung zugreifen kann. Die Listen
 * heissen: tablegreen tablegrey chair
 * 
 * da die Listen nicht wirklich wohlgeformt sind (sieht nur so aus DocFX ;) )
 * gehe ich hier einfach durch alle "Icons" durch und sortiere sie entsprechend
 * ihrer Parents ein, statt einfach von oben herunter durch die Parents zu gehen
 * und sie so einzusortieren...
 */

/**
 * <p>
 * Title: TableDancer for Flip
 * </p>
 * <p>
 * Description: Applet for table positioning in the FLIP intranet system
 * </p>
 * 
 * Liest das XML-Dokument von FLIP und setzt daraus die Werte - dolmetscher
 * 
 * @author Kai 'Riffer' Posadowsky
 */
public class XmlParser extends XmlParserKonstanten {
	private DocumentBuilder docBuilder = null;

	/**
	 * default - Konstruktor Holt den DokumentenArbeiter
	 */
	public XmlParser() {
		try {
			docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			ex.printStackTrace();
		}
	}

	public Map<String, Map<String, String>> readIconURLs(InputStream iconStream) {
		Map<String, Map<String, String>> tables = new HashMap<String, Map<String, String>>(4, 0.8f);
		try {
			Document doc = docBuilder.parse(iconStream);
			// normalize text representation
			doc.getDocumentElement().normalize();
			// tags in XML (seatsadm.php?frame=geticons)
			String[] tagnames = { TABLEGREEN, TABLEGREY, CHAIR };
			for (String tagname : tagnames) {
				tables.put(tagname, new HashMap<String, String>());
				NodeList listOfTables = doc.getElementsByTagName(tagname);
				for (int j = 0; j < listOfTables.getLength(); j++) {
					Node tag = listOfTables.item(j);
					readIconUrls(tag, tables.get(tagname));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tables;
	}

	private void readIconUrls(Node node, Map<String, String> iconsByAngle) {
		NodeList icons = node.getChildNodes();
		for (int i = 0; i < icons.getLength(); i++) {
			Node icon = icons.item(i);
			NamedNodeMap attribs = icon.getAttributes();
			if (attribs != null) {
				if (attribs.getNamedItem(ANGLE_ATTRIB) != null) {
					String angle = attribs.getNamedItem(ANGLE_ATTRIB).getTextContent();
					iconsByAngle.put(angle, icon.getTextContent());
				}
			}
		}
	}

	/**
	 * TODO: Seats ist unnötig, da diese je Block gespeichert werden
	 */
	public void fillList(InputStream TableStream, List<Block> myBlocks, List<Seat> mySeats) {
		Document doc = getDocument(TableStream);
		NodeList listOfBlocks = doc.getElementsByTagName(BLOCK);
		for (int i = 0; i < listOfBlocks.getLength(); i++) {

			Node firstBlockNode = listOfBlocks.item(i);
			if (firstBlockNode.getNodeType() == Node.ELEMENT_NODE) {
				Block aBlock = parseBlockData((Element) firstBlockNode);
				myBlocks.add(aBlock);

				Node seats = ((Element) firstBlockNode).getElementsByTagName(SEATS).item(0);
				if (seats == null) {
					System.err.println("Fehlerhafte XML");
					continue;
				}
				NodeList listOfSeats = seats.getChildNodes();
				for (int s = 0; s < listOfSeats.getLength(); s++) {
					Node firstSeatNode = listOfSeats.item(s);
					if (firstSeatNode.getNodeType() == Node.ELEMENT_NODE) {
						Seat aSeat = parseSeatData((Element) firstSeatNode, aBlock);
						mySeats.add(aSeat);
						aBlock.addSeat(aSeat);
					}
				} // list of seats
			}
		} // list of blocks
	}

	public Document getDocument(InputStream dataStream) {
		try {
			Document doc = docBuilder.parse(dataStream);
			doc.normalizeDocument();
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Block parseBlockData(Element myBlock) {
		Block aBlock = new Block();
		// some mapping stuff
		aBlock.setId(getElementIntValue(myBlock, ID));
		aBlock.setCaption(getElementStringValue(myBlock, CAPTION));
		aBlock.setHref(getElementStringValue(myBlock, HREF));
		aBlock.setLink(getElementStringValue(myBlock, LINK));
		aBlock.setDescription(getElementStringValue(myBlock, DESCRIPTION));
		aBlock.setImageDirectory(getElementStringValue(myBlock, IMAGE_DIRECTORY));
		aBlock.setViewRight(getElementIntValue(myBlock, VIEW_RIGHT));

		return aBlock;
	}

	private Seat parseSeatData(Element mySeat, Block myBlock) {
		Seat aSeat = new Seat(myBlock);
		aSeat.setId(getElementIntValue(mySeat, ID));
		aSeat.setCenterPosX(getElementIntValue(mySeat, CENTERX));
		aSeat.setCenterPosY(getElementIntValue(mySeat, CENTERY));
		aSeat.setAngel(getElementIntValue(mySeat, ANGLE));
		aSeat.setEnabled(getElementStringValue(mySeat, ENABLED).equalsIgnoreCase(YES));
		aSeat.setName(getElementStringValue(mySeat, NAME));
		aSeat.setIp(getElementStringValue(mySeat, IP));

		return aSeat;
	}

	private String getElementStringValue(Element firstSeatElement, String name) {
		NodeList textNode = getTextNode(firstSeatElement, name);
		return textNode.item(0) != null ? textNode.item(0).getNodeValue().trim() : "";
	}

	private int getElementIntValue(Element firstSeatElement, String name) {
		NodeList textNode = getTextNode(firstSeatElement, name);
		return textNode.item(0) != null ? Integer.parseInt(textNode.item(0).getNodeValue().trim()) : 0;
	}

	private NodeList getTextNode(Element firstSeatElement, String name) {
		return firstSeatElement.getElementsByTagName(name).item(0).getChildNodes();
	}
}
