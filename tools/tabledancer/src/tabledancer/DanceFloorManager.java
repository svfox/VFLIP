/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import static tabledancer.Constants.CONFIG_DEFAULT_RIGHT;
import static tabledancer.Constants.CONFIG_ID_NEW_BLOCK;
import static tabledancer.Constants.EMPTY_STRING;
import static tabledancer.Constants.LABEL_NEW_BLOCK;

import javax.swing.JTabbedPane;

import tabledancer.flip.bean.Block;
import tabledancer.gui.dancefloor.DanceFloor;

/**
 * this class is responsible for the dancefloor handling.
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class DanceFloorManager {
	/**
	 * the TableDancer instance
	 */
	private final TableDancer tableDancer;

	/**
	 * the current container instance for all blocks
	 */
	private JTabbedPane guiContainer;

	/**
	 * the constructor
	 * 
	 * @param TableDancer
	 *            tableDancer the TableDancer instance
	 */
	public DanceFloorManager(final TableDancer tableDancer) {
		this.tableDancer = tableDancer;
	}

	/**
	 * sets the container which should be managed
	 * 
	 * @param JTabbedPane
	 *            the new JTabbedPane object
	 */
	public void setContainer(final JTabbedPane guiContainer) {
		this.guiContainer = guiContainer;
	}

	/**
	 * loads the current block/seat data from the DataManager and uses the data
	 * to initialize the dancefloors
	 */
	public void load() throws Exception {
		final DataManager dataManager;

		dataManager = tableDancer.getDataManager();
		dataManager.loadDataFromBackend();

		for (final Block block : dataManager.getBlocks()) {
			final DanceFloor danceFloor;
			danceFloor = DanceFloor.newInstance(block);

			guiContainer.addTab(block.getCaption(), danceFloor);
		}
	}

	/**
	 * initializes a new dancefloor with a corresponding block object and adds
	 * it to the workplace
	 * 
	 * @param boolean setFocus when true, the new dance floor gains the focus
	 * 
	 * @return DanceFloor the new dancefloor object or null if creation has
	 *         failed
	 */
	public DanceFloor create(final boolean setFocus) {
		final Block block;
		final DanceFloor danceFloor;

		/**
		 * TODO move the creation of the block object to the DataManager TODO
		 * better workflow - jframe with input fields that collect data for the
		 * new block - post request to flip to create the block - response
		 * returns xml with the complete block data (paths etc) - we can now use
		 * the existing code to create a dancefloor from the block data
		 * 
		 */

		block = new Block();
		block.setId(CONFIG_ID_NEW_BLOCK);
		block.setCaption(LABEL_NEW_BLOCK);
		block.setViewRight(CONFIG_DEFAULT_RIGHT);
		block.setDescription(EMPTY_STRING);
		block.setHref(EMPTY_STRING);

		danceFloor = DanceFloor.newInstance(block);

		tableDancer.getDataManager().getBlocks().add(block);
		guiContainer.addTab(block.getCaption(), danceFloor);

		if (setFocus) {
			guiContainer.setSelectedComponent(danceFloor);
		}

		return danceFloor;
	}

	/**
	 * calls create with setFocus true
	 * 
	 * @see DanceFloorManager#create(boolean)
	 * 
	 * @return DanceFloor the new dancefloor object or null if creation has
	 *         failed
	 */
	public DanceFloor create() {
		return create(true);
	}

	/**
	 * returns the current dancefloor object
	 * 
	 * TODO distinguish between dancehall and dancefloor TODO talk with flip
	 * developers about dancehall, scaling, block size, etc
	 * 
	 * @return DanceFloor the dancefloor object or NULL on failure
	 */
	public DanceFloor getCurrent() {
		DanceFloor danceFloor = null;

		// Do check if the cast succeeds.
		// guiContainer.getSelectedComponent() could return something else than
		// a DanceFloor
		// because of code changes and that would trigger an uncaught (== nasty)
		// ClassCastException.
		try {
			danceFloor = (DanceFloor) guiContainer.getSelectedComponent();
		} catch (ClassCastException ccEx) {
			System.err.println("Error in DanceFloorManager.getCurrent() - cannot cast!");
		}

		return danceFloor;
	}

	/**
	 * removes the current dancefloor object
	 * 
	 * actually the tab could be an arbitrary object but as the application only
	 * fills the workplace with dancefloor objects, we can leave out type
	 * checking here
	 * 
	 * @return DanceFloor the dancefloor object
	 */
	public void removeCurrent() {
		DanceFloor delinquent = null;

		try {
			delinquent = (DanceFloor) guiContainer.getSelectedComponent();
		} catch (ClassCastException ccEx) {
			System.err.println("Error in DanceFloorManager.removeCurrent() - cannot cast!");
		}

		remove(delinquent);
	}

	/**
	 * removes all dancefloors from the workspace
	 */
	public void removeAll() {		
		while(guiContainer.getComponentCount() > 0) {
			Object obj = guiContainer.getComponent(0);

			if (obj instanceof DanceFloor) {
				DanceFloor danceFloor;

				try {
					danceFloor = (DanceFloor) obj;
					remove(danceFloor);
				} catch (ClassCastException ccEx) {
					System.err.println("Error in DanceFloorManager.removeAll() - cannot cast!");
				}	
			}			
		}
	}

	/**
	 * removes the given DanceFloor object from the workplace and and nulls it
	 * for the gc. when the object is nulled all referenced objects (tables &
	 * seats) should be destroyed and removed from the meomory
	 * 
	 * @param DanceFloor
	 *            danceFloor the dancefloor
	 */
	private void remove(DanceFloor danceFloor) {
		if (danceFloor == null) {
			return;
		}

		guiContainer.remove(danceFloor);
		tableDancer.getDataManager().getBlocks().remove(danceFloor.getBlock());

		danceFloor = null;
	}
}
