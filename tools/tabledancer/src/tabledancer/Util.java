/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import java.awt.Point;
import java.awt.event.InputEvent;

/**
 * utility class that collects static methods which doesn't fit anywhere else.
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class Util {
	/**
	 * checks if the given input event contains exactly modifiers
	 * 
	 * @param InputEvent
	 *            event the input event
	 * @param int modifier the modifier bitmask
	 * 
	 * @return boolean true if the event contains the modifiers, otherwise false
	 */
	public static boolean hasModifier(final InputEvent event, final int modifier) {
		if (event == null) {
			return false;
		}

		final boolean result;

		result = ((event.getModifiers() & modifier) == modifier);

		return result;
	}

	/**
	 * checks if the position is within the range specified by the given minimum
	 * and maximum points. if the source x/y position is greater than the
	 * minimum or maximum x/y positions, those positions are returned instead.
	 * 
	 * @param Point
	 *            source the source position
	 * @param Point
	 *            min the maximal position
	 * @param Point
	 *            max the minimal position
	 * 
	 * @return Point the corrected source
	 */
	public static Point getPositionWithinRange(final Point source, final Point min, final Point max) {
		// TODO null checks

		final Point result;

		result = new Point(source);

		if (result.x < min.x) {
			result.x = min.x;
		}

		if (result.y < min.y) {
			result.y = min.y;
		}

		if (result.x > max.x) {
			result.x = max.x;
		}

		if (result.y > max.y) {
			result.y = max.y;
		}

		return result;
	}
}
