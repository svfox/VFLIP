/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Block {
	private int id;

	private int viewRight;

	private String caption;

	private String imageDirectory;

	private String href;

	private String link;

	private String description;

	// MapofMaps: "type" => "angle" => "URL"
	// z.B.: greentable => {0=>green0.png, 180,green180.png}
	private Map<String, Map<String, String>> iconUrls;

	private List<Seat> seats = new ArrayList<Seat>();

	public void addSeat(Seat seat) {
		seats.add(seat);
	}

	public void removeSeat(Seat seat) {
		seats.remove(seat);
	}

	public List<Seat> getSeats() {
		return seats;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImageDirectory() {
		return imageDirectory;
	}

	public void setImageDirectory(String imageDirectory) {
		this.imageDirectory = imageDirectory;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public int getViewRight() {
		return viewRight;
	}

	public void setViewRight(int viewRight) {
		this.viewRight = viewRight;
	}

	public Map<String, Map<String, String>> getIconUrls() {
		return iconUrls;
	}

	public void setIconUrls(Map<String, Map<String, String>> iconUrls) {
		this.iconUrls = iconUrls;
	}

	/**
	 * Map mit Winkel => Bild-URL z.B.: 0 =>
	 * tpl/default/images/seats/simple_15x11/small_tablegreen_0.png
	 * 
	 * @return Map
	 */
	public Map<String, String> getTableAngleUrls(String kind) throws NullPointerException {
		if (iconUrls.containsKey(kind)) {
			return iconUrls.get(kind);
		} else {
			throw new NullPointerException("No URLs for '" + kind + "' loaded");
		}
	}
}
