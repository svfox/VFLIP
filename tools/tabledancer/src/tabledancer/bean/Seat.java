/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.bean;

public class Seat {
	private int id;

	private int posX;

	private int posY;

	private int angel;

	private boolean enabled;

	private String name;

	private String ip;

	private Block block;

	private int width;

	private int height;

	public Seat(Block block) {
		this.block = block;
	}

	public Block getBlock() {
		return block;
	}

	public int getAngle() {
		return angel;
	}

	public void setAngel(int angel) {
		this.angel = angel;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCenterPosX() {
		return posX;
	}

	public void setCenterPosX(int posX) {
		this.posX = posX;
	}

	public int getTopLeftPosX() {
		return posX - width / 2;
	}

	public void setTopLeftPosX(int posX) {
		this.posX = posX + width / 2;
	}

	public int getCenterPosY() {
		return posY;
	}

	public void setCenterPosY(int posY) {
		this.posY = posY;
	}

	public int getTopLeftPosY() {
		return posY - height / 2;
	}

	public void setTopLeftPosY(int posY) {
		this.posY = posY + height / 2;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
