/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.bean;

public class Right {
	private final int id;

	private final String name;

	public Right(final int id, final String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
}
