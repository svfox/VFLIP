/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.bean;

import java.util.ArrayList;
import java.util.List;

public class Block {
	private int id;

	private Right viewRight;

	private String caption;

	private String imageDirectory;

	private String href;

	private String link;

	private String description;

	private List<Seat> seats;

	public Block() {
		this.seats = new ArrayList<Seat>();
	}

	@Override
	public String toString() {
		return this.getCaption();
	}

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public Right getViewRight() {
		return this.viewRight;
	}

	public void setViewRight(final Right viewRight) {
		this.viewRight = viewRight;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(final String caption) {
		this.caption = caption;
	}

	public String getImageDirectory() {
		return this.imageDirectory;
	}

	public void setImageDirectory(final String imageDirectory) {
		this.imageDirectory = imageDirectory;
	}

	public String getHref() {
		return href;
	}

	public void setHref(final String href) {
		this.href = href;
	}

	public String getLink() {
		return link;
	}

	public void setLink(final String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<Seat> getSeats() {
		return this.seats;
	}

	public void addSeat(final Seat seat) {
		this.seats.add(seat);
	}

	public void removeSeat(final Seat seat) {
		this.seats.remove(seat);
	}
}
