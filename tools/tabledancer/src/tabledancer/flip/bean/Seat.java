/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.bean;

public class Seat {
	private int id;

	private Block block;

	private String name;

	private String ip;

	private boolean enabled;

	private int width;

	private int height;

	private int posX;

	private int posY;

	private int angle;

	public Seat(final Block block) {
		this.block = block;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public Block getBlock() {
		return this.block;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(final String ip) {
		this.ip = ip;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(final int width) {
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(final int height) {
		this.height = height;
	}

	public int getCenterPosX() {
		return this.posX;
	}

	public void setCenterPosX(final int posX) {
		this.posX = posX;
	}

	public int getCenterPosY() {
		return posY;
	}

	public void setCenterPosY(final int posY) {
		this.posY = posY;
	}

	public int getTopLeftPosX() {
		return this.posX - this.width / 2;
	}

	public void setTopLeftPosX(final int posX) {
		this.posX = posX + this.width / 2;
	}

	public int getTopLeftPosY() {
		return this.posY - this.height / 2;
	}

	public void setTopLeftPosY(final int posY) {
		this.posY = posY + this.height / 2;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(final int angel) {
		this.angle = angel;
	}
}
