/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.xml;

import java.util.List;

import tabledancer.Constants;
import tabledancer.flip.bean.Block;
import tabledancer.flip.bean.Seat;

/**
 * convert java-beans into the flip xml structure
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @since 1.0
 * @version 1.1
 */
public class XmlWriter extends XmlWriterConstants {
	/**
	 * 
	 * 
	 * @param text
	 * @param find
	 * @param replace
	 * @return
	 */
	private static String replace(String text, final char find, final String replace) {
		int i = 0;
		int j = 0;

		while ((i = i + text.substring(j).indexOf(find)) > j) {
			j = i + 1;
			text = text.substring(0, i) + replace + text.substring(j);
		}

		return text;
	}

	/**
	 * 
	 * 
	 * @param text
	 * @return
	 */
	private static String encode(String text) {
		text = replace(text, CHAR_AMP, XML_AMP);
		text = replace(text, CHAR_LT, XML_LT);
		text = replace(text, CHAR_GT, XML_GT);

		return text;
	}

	/**
	 * creates a xml string representation of the given block list
	 * 
	 * TODO DOCTYPE vernueftig definieren TODO implementation auf DOM objekte +
	 * toString() funktionen umgestellen -> effizienter
	 * 
	 * @param List
	 *            <Block> blocks the block list
	 * 
	 * @return String the xml data
	 */
	public static String write(final List<Block> blocks) {
		final StringBuilder sb;

		// StringBuffer buffer=new StringBuffer("<DOCTYPE...>\n\n<seats>");
		sb = new StringBuilder(XMLHEAD);
		sb.append("<!-- Generiert mit FLIP TableDancer v" + Constants.TABLEDANCER_VERSION_STRING + " -->\n");
		sb.append(XML_BLOCKS_AUF);

		for (final Block block : blocks) {
			sb.append(XML_BLOCK_AUF);
			sb.append(XML_BLOCKID_AUF).append(block.getId()).append(XML_BLOCKID_ZU);
			sb.append(XML_BLOCKCAPTION_AUF).append(block.getCaption()).append(XML_BLOCKCAPTION_ZU);
			sb.append(XML_BLOCKDESCRIPTION_AUF).append(encode(block.getDescription())).append(XML_BLOCKDESCRIPTION_ZU);
			sb.append(XML_BLOCKVIEWRIGHT_AUF).append(block.getViewRight().getId()).append(XML_BLOCKVIEWRIGHT_ZU);
			sb.append(XML_BLOCKIMAGEDIR_AUF).append(block.getImageDirectory()).append(XML_BLOCKIMAGEDIR_ZU);
			sb.append(XML_BLOCKLINK_AUF).append(encode(block.getLink())).append(XML_BLOCKLINK_ZU);
			sb.append(XML_BLOCKHREF_AUF).append(encode(block.getHref())).append(XML_BLOCKHREF_ZU);
			sb.append(XML_SEATS_AUF);

			for (final Seat seat : block.getSeats()) {
				sb.append(XML_SEAT_AUF);
				sb.append(XML_ID_AUF).append(seat.getId()).append(XML_ID_ZU);
				sb.append(XML_ENABLED_AUF).append(seat.isEnabled() ? CDATA_YES : CDATA_NO).append(XML_ENABLED_ZU);
				sb.append(XML_CENTERX_AUF).append(seat.getCenterPosX()).append(XML_CENTERX_ZU);
				sb.append(XML_CENTERY_AUF).append(seat.getCenterPosY()).append(XML_CENTERY_ZU);
				sb.append(XML_ANGLE_AUF).append(seat.getAngle()).append(XML_ANGLE_ZU);
				sb.append(XML_NAME_AUF).append(encode(seat.getName())).append(XML_NAME_ZU);
				sb.append(XML_IP_AUF).append(seat.getIp()).append(XML_IP_ZU);
				sb.append(XML_SEAT_ZU);
			}

			sb.append(XML_SEATS_ZU);
			sb.append(XML_BLOCK_ZU);
		}

		sb.append(XML_BLOCKS_ZU);

		return sb.toString();
	}
}
