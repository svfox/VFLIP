/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.xml;

/**
 * Diese Klasse enthaelt nur die Namen alle Tags, damit diese nicht mehrfach
 * instaziert werden muessen
 * 
 * @author Kai 'Riffer' Posadowsky
 * 
 * @since 1.0
 * @version 1.0
 */
public abstract class XmlParserConstants {
	// general
	public static final String ELEMENT_ID = "id";

	// block
	public static final String ELEMENT_BLOCKS = "blocks";
	public static final String ELEMENT_BLOCK = "block";
	public static final String ELEMENT_VIEW_RIGHT = "view_right";
	public static final String ELEMENT_CAPTION = "caption";
	public static final String ELEMENT_DESCRIPTION = "description";
	public static final String ELEMENT_IMAGE_DIRECTORY = "imagedir_block";
	public static final String ELEMENT_LINK = "link";
	public static final String ELEMENT_HREF = "href";

	// seat
	public static final String ELEMENT_SEATS = "seats";
	public static final String ELEMENT_SEAT = "seat";
	public static final String ELEMENT_ENABLED = "enabled";
	public static final String ELEMENT_CENTER_X = "center_x";
	public static final String ELEMENT_CENTER_Y = "center_y";
	public static final String ELEMENT_NAME = "name";
	public static final String ELEMENT_ANGLE = "angle";
	public static final String ELEMENT_IP = "ip";

	// icon
	public static final String ELEMENT_TABLEGREEN = "tablegreen";
	public static final String ELEMENT_TABLEGREY = "tablegrey";
	public static final String ELEMENT_CHAIR = "chair";

	public static final String ATTRIBUTE_ANGLE = "angle";

	// value-mapping
	public static final String CDATA_YES = "Y";
	public static final String CDATA_NO = "N";
}
