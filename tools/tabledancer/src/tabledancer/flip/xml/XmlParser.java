/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.xml;

import static tabledancer.Constants.EMPTY_STRING;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import tabledancer.TableDancer;
import tabledancer.flip.bean.Block;
import tabledancer.flip.bean.IconList;
import tabledancer.flip.bean.Right;
import tabledancer.flip.bean.Seat;

/**
 * reads xml data from the backend system and converts it into bean objects
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @since 1.0
 * @version 1.1
 */
public class XmlParser extends XmlParserConstants {
	// icon tag names
	public static final String[] iconTagNames = { ELEMENT_TABLEGREEN, ELEMENT_TABLEGREY, ELEMENT_CHAIR };

	/**
	 * the DocumentBuilder instance for parsing xml data into a dom-tree
	 */
	private DocumentBuilder documentBuilder;

	/**
	 * constructor
	 */
	public XmlParser() {
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO elegantere fehlerbehandlung
			TableDancer.printException(e);
		}
	}

	/**
	 * returns a trimmed string representation of the first child node from the
	 * element
	 * 
	 * @param Element
	 *            element the root element
	 * @param String
	 *            name the element name to search for
	 * 
	 * @return String the element value
	 */
	private static String getElementStringValue(final Element element, final String name) {
		if (element == null || name == null || name.isEmpty()) {
			return EMPTY_STRING;
		}

		final NodeList textNode;
		final String result;

		textNode = element.getElementsByTagName(name).item(0).getChildNodes();

		result = textNode.item(0) != null ? textNode.item(0).getNodeValue().trim() : EMPTY_STRING;

		return result;
	}

	/**
	 * returns a boolean representation of the first child node from the element
	 * 
	 * @return
	 * 
	 * @see XmlParser#getElementStringValue(Element, String)
	 */
	private static boolean getElementBooleanValue(final Element element, final String name) {
		final boolean result;

		result = getElementStringValue(element, name).equalsIgnoreCase(CDATA_YES);

		return result;
	}

	/**
	 * returns a integer representation of the first child node from the element
	 * 
	 * @param Element
	 *            element the root element
	 * @param String
	 *            name the element name to search for
	 * 
	 * @return int the element value as an int
	 */
	private static int getElementIntegerValue(final Element element, final String name) {
		if (element == null || name == null || name.isEmpty()) {
			return 0;
		}

		final NodeList textNode;
		final int result;

		textNode = element.getElementsByTagName(name).item(0).getChildNodes();

		result = textNode.item(0) != null ? Integer.parseInt(textNode.item(0).getNodeValue().trim()) : 0;

		return result;
	}

	/**
	 * creates a new Block object from the flip xml data
	 * 
	 * @param Element
	 *            xmlBlock the xml data
	 * 
	 * @return Block the new java bean with mapped values from the xml data
	 */
	private static Block createBlockFromElement(final Element xmlBlock) {
		if (xmlBlock == null) {
			return null;
		}

		final int viewRight;
		final Right right;
		final Block block;

		viewRight = getElementIntegerValue(xmlBlock, ELEMENT_VIEW_RIGHT);
		right = TableDancer.getInstance().getDataManager().requestRight(viewRight, EMPTY_STRING);

		block = new Block();

		block.setId(getElementIntegerValue(xmlBlock, ELEMENT_ID));
		block.setCaption(getElementStringValue(xmlBlock, ELEMENT_CAPTION));
		block.setViewRight(right);
		block.setImageDirectory(getElementStringValue(xmlBlock, ELEMENT_IMAGE_DIRECTORY));
		block.setLink(getElementStringValue(xmlBlock, ELEMENT_LINK));
		block.setHref(getElementStringValue(xmlBlock, ELEMENT_HREF));
		block.setDescription(getElementStringValue(xmlBlock, ELEMENT_DESCRIPTION));

		return block;
	}

	/**
	 * creates a new Seat object from the flip xml data
	 * 
	 * @param Element
	 *            xmlSeat the xml data
	 * @param Block
	 *            block the associated block bean
	 * 
	 * @return Seat the new java bean with mapped values from the xml data
	 */
	private static Seat createSeatFromElement(final Element xmlSeat, final Block block) {
		if (xmlSeat == null || block == null) {
			return null;
		}

		final Seat seat;

		seat = new Seat(block);

		seat.setId(getElementIntegerValue(xmlSeat, ELEMENT_ID));
		seat.setName(getElementStringValue(xmlSeat, ELEMENT_NAME));
		seat.setIp(getElementStringValue(xmlSeat, ELEMENT_IP));
		seat.setEnabled(getElementBooleanValue(xmlSeat, ELEMENT_ENABLED));
		seat.setCenterPosX(getElementIntegerValue(xmlSeat, ELEMENT_CENTER_X));
		seat.setCenterPosY(getElementIntegerValue(xmlSeat, ELEMENT_CENTER_Y));
		seat.setAngle(getElementIntegerValue(xmlSeat, ELEMENT_ANGLE));

		return seat;
	}

	/**
	 * 
	 * @param inputStream
	 * @return
	 */
	public Document getDocumentFromInputStream(final InputStream inputStream) throws SAXException, IOException {
		if (inputStream == null) {
			return documentBuilder.newDocument();
		}

		final Document document;

		document = documentBuilder.parse(inputStream);

		System.out.println(inputStream);

		return document;
	}

	/**
	 * converts xml data from the inputstream into block/seat/right java-beans
	 * 
	 * @param InputStream
	 *            inputStream the inputStream
	 * 
	 * @return List<Block> the block list
	 */
	public List<Block> getBlocksFromInputStream(final InputStream inputStream) throws SAXException, IOException {
		final List<Block> result;

		result = new ArrayList<Block>();

		if (inputStream == null) {
			return result;
		}

		final Document document;
		final NodeList blockNodes;

		document = getDocumentFromInputStream(inputStream);
		document.normalizeDocument();

		// get all blocks from the document
		blockNodes = document.getElementsByTagName(ELEMENT_BLOCK);

		// loop over all blocks
		for (int i = 0; i < blockNodes.getLength(); i++) {
			final Node blockNode;

			blockNode = blockNodes.item(i);

			if (blockNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			// create java object for the block
			final Element blockElement;
			final Block block;

			blockElement = (Element) blockNode;
			block = createBlockFromElement(blockElement);

			final Node seats;

			// get the root seat element and all sub elements
			seats = blockElement.getElementsByTagName(ELEMENT_SEATS).item(0);

			if (seats == null) {
				System.err.println("invalid xml, missing seat elements");
				continue;
			}

			final NodeList seatNodes;

			// get the seat elements
			seatNodes = seats.getChildNodes();

			// loop over the seat elements
			for (int y = 0; y < seatNodes.getLength(); y++) {
				final Node seatNode;

				seatNode = seatNodes.item(y);

				if (seatNode.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				// create java object for the seat
				final Element seatElement;
				final Seat seat;

				seatElement = (Element) seatNode;

				seat = createSeatFromElement(seatElement, block);

				block.addSeat(seat);
			}

			result.add(block);
		}

		return result;
	}

	/**
	 * es gibt 3 verschiedene Listen innerhalb dieses XML-Documentes auf deren
	 * interne Nodes man wiederum ?ber eine Aufz?hlung zugreifen kann. Die
	 * Listen heissen: tablegreen tablegrey chair
	 * 
	 * da die Listen nicht wirklich wohlgeformt sind (sieht nur so aus DocFX ;)
	 * ) gehe ich hier einfach durch alle "Icons" durch und sortiere sie
	 * entsprechend ihrer Parents ein, statt einfach von oben herunter durch die
	 * Parents zu gehen und sie so einzusortieren...
	 * 
	 * @param inputStream
	 * @return
	 */
	public List<IconList> getIconURLsFromInputStream(final InputStream inputStream) {
		final List<IconList> result;

		result = new ArrayList<IconList>();

		if (inputStream == null) {
			return result;
		}

		try {
			final Document document;

			document = getDocumentFromInputStream(inputStream);
			document.getDocumentElement().normalize();

			// container loop to process all icon types with the same code
			for (final String iconType : iconTagNames) {
				final IconList iconList;
				final Map<Integer, String> iconMap;
				final NodeList iconTypeNodes;

				iconList = new IconList(iconType);
				iconMap = new HashMap<Integer, String>();

				// get the root and all sub elements of an icon type
				iconTypeNodes = document.getElementsByTagName(iconType);

				// process all icon type elements
				for (int i = 0; i < iconTypeNodes.getLength(); i++) {
					final Node tableNode;
					final NodeList iconNodes;

					// get all icon elements from the current icon type
					tableNode = iconTypeNodes.item(i);
					iconNodes = tableNode.getChildNodes();

					// process the icon elements
					for (int j = 0; j < iconNodes.getLength(); j++) {
						final Node iconNode;
						final NamedNodeMap iconAttributes;

						// get the icon element and its attributes
						iconNode = iconNodes.item(j);
						iconAttributes = iconNode.getAttributes();

						if (iconAttributes == null || iconAttributes.getNamedItem(ATTRIBUTE_ANGLE) == null) {
							// invalid xml
							continue;
						}

						// read the element data
						try {
							final String angleStr;
							final int angle;
							final String url;

							angleStr = iconAttributes.getNamedItem(ATTRIBUTE_ANGLE).getTextContent();
							angle = Integer.valueOf(angleStr);
							url = iconNode.getTextContent();

							iconMap.put(angle, url);
						} catch (NumberFormatException e) {
							// invalid angle, skip icon
						}
					}
				}

				iconList.setList(iconMap);

				result.add(iconList);
			}
		} catch (Exception e) {
			TableDancer.printException(e);
		}

		return result;
	}
}
