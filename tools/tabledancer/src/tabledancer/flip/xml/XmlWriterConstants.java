/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.xml;

/**
 * Definition der XML-Semantic. Alle Tags kommen aus der Ableitung.
 * 
 * WTF WTF WTF!
 * 
 * @author Kai 'Riffer' Posadowsky
 * 
 * @since 1.0
 * @version 1.0
 */
public abstract class XmlWriterConstants extends XmlParserConstants {
	public static final String XMLHEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";

	private static final String XMLAUF = "<";
	private static final String XMLZU = ">";
	private static final String XMLAUFENDE = "</";
	private static final String ENTER = "\n";

	// OMGOMGOMGWTF *selfslap*
	private static final String LEER = "\t";
	private static final String LEER_2 = LEER + LEER;
	private static final String LEER_3 = LEER_2 + LEER;
	private static final String LEER_4 = LEER_3 + LEER;

	// Leer 0
	public static final String XML_BLOCKS_AUF = XMLAUF + ELEMENT_BLOCKS + XMLZU + ENTER;
	public static final String XML_BLOCKS_ZU = XMLAUFENDE + ELEMENT_BLOCKS + XMLZU + ENTER;

	// Leer 1
	public static final String XML_BLOCK_AUF = LEER + XMLAUF + ELEMENT_BLOCK + XMLZU + ENTER;
	public static final String XML_BLOCK_ZU = LEER + XMLAUFENDE + ELEMENT_BLOCK + XMLZU + ENTER;

	// Leer 2
	// Alle Blockdaten
	public static final String XML_SEATS_AUF = LEER_2 + XMLAUF + ELEMENT_SEATS + XMLZU + ENTER;
	public static final String XML_BLOCKID_AUF = LEER_2 + XMLAUF + ELEMENT_ID + XMLZU;
	public static final String XML_BLOCKCAPTION_AUF = LEER_2 + XMLAUF + ELEMENT_CAPTION + XMLZU;
	public static final String XML_BLOCKVIEWRIGHT_AUF = LEER_2 + XMLAUF + ELEMENT_VIEW_RIGHT + XMLZU;
	public static final String XML_BLOCKDESCRIPTION_AUF = LEER_2 + XMLAUF + ELEMENT_DESCRIPTION + XMLZU;
	public static final String XML_BLOCKIMAGEDIR_AUF = LEER_2 + XMLAUF + ELEMENT_IMAGE_DIRECTORY + XMLZU;
	public static final String XML_BLOCKLINK_AUF = LEER_2 + XMLAUF + ELEMENT_LINK + XMLZU;
	public static final String XML_BLOCKHREF_AUF = LEER_2 + XMLAUF + ELEMENT_HREF + XMLZU;

	public static final String XML_SEATS_ZU = LEER_2 + XMLAUFENDE + ELEMENT_SEATS + XMLZU + ENTER;
	public static final String XML_BLOCKID_ZU = XMLAUFENDE + ELEMENT_ID + XMLZU + ENTER;
	public static final String XML_BLOCKCAPTION_ZU = XMLAUFENDE + ELEMENT_CAPTION + XMLZU + ENTER;
	public static final String XML_BLOCKVIEWRIGHT_ZU = XMLAUFENDE + ELEMENT_VIEW_RIGHT + XMLZU + ENTER;
	public static final String XML_BLOCKDESCRIPTION_ZU = XMLAUFENDE + ELEMENT_DESCRIPTION + XMLZU + ENTER;
	public static final String XML_BLOCKIMAGEDIR_ZU = XMLAUFENDE + ELEMENT_IMAGE_DIRECTORY + XMLZU + ENTER;
	public static final String XML_BLOCKLINK_ZU = XMLAUFENDE + ELEMENT_LINK + XMLZU + ENTER;
	public static final String XML_BLOCKHREF_ZU = XMLAUFENDE + ELEMENT_HREF + XMLZU + ENTER;

	// Leer 3
	// Seat
	public static final String XML_SEAT_AUF = LEER_3 + XMLAUF + ELEMENT_SEAT + XMLZU + ENTER;
	public static final String XML_SEAT_ZU = LEER_3 + XMLAUFENDE + ELEMENT_SEAT + XMLZU + ENTER;

	// Leer 4
	// Alle Tischdaten
	public static final String XML_ID_AUF = LEER_4 + XMLAUF + ELEMENT_ID + XMLZU;
	public static final String XML_ENABLED_AUF = LEER_4 + XMLAUF + ELEMENT_ENABLED + XMLZU;
	public static final String XML_CENTERX_AUF = LEER_4 + XMLAUF + ELEMENT_CENTER_X + XMLZU;
	public static final String XML_CENTERY_AUF = LEER_4 + XMLAUF + ELEMENT_CENTER_Y + XMLZU;
	public static final String XML_ANGLE_AUF = LEER_4 + XMLAUF + ELEMENT_ANGLE + XMLZU;
	public static final String XML_NAME_AUF = LEER_4 + XMLAUF + ELEMENT_NAME + XMLZU;
	public static final String XML_IP_AUF = LEER_4 + XMLAUF + ELEMENT_IP + XMLZU;
	public static final String XML_ID_ZU = XMLAUFENDE + ELEMENT_ID + XMLZU + ENTER;
	public static final String XML_ENABLED_ZU = XMLAUFENDE + ELEMENT_ENABLED + XMLZU + ENTER;
	public static final String XML_CENTERX_ZU = XMLAUFENDE + ELEMENT_CENTER_X + XMLZU + ENTER;
	public static final String XML_CENTERY_ZU = XMLAUFENDE + ELEMENT_CENTER_Y + XMLZU + ENTER;
	public static final String XML_ANGLE_ZU = XMLAUFENDE + ELEMENT_ANGLE + XMLZU + ENTER;
	public static final String XML_NAME_ZU = XMLAUFENDE + ELEMENT_NAME + XMLZU + ENTER;
	public static final String XML_IP_ZU = XMLAUFENDE + ELEMENT_IP + XMLZU + ENTER;

	// replacements
	public static final char CHAR_AMP = '&';
	public static final char CHAR_LT = '<';
	public static final char CHAR_GT = '>';

	public static final String XML_AMP = "&amp;";
	public static final String XML_LT = "&lt;";
	public static final String XML_GT = "&gt;";
}
