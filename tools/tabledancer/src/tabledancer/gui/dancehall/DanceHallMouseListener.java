/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancehall;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPopupMenu;

/**
 * MouseHandler-Klasse fuer alle Mausevents
 */
public class DanceHallMouseListener implements MouseMotionListener {
	/**
	 * the dancefloor
	 */
	private DanceHall danceHall;

	/**
	 * the context menu
	 */
	private JPopupMenu popupMenu;

	/**
	 * the current mouse position
	 */
	private Point currentMousePosition;

	/**
	 * the starting point of the selected area
	 */
	private Point dragStartPoint;

	/**
	 * the ending point of the selected area
	 */
	private Point dragEndPoint;

	/**
	 * markieren flag
	 */
	private boolean selecting;

	/**
	 * Konstruktor
	 * 
	 * @param panel
	 */
	public DanceHallMouseListener(final DanceHall danceHall) {
		final DanceHallMenu menu;

		menu = new DanceHallMenu(danceHall);

		this.danceHall = danceHall;
		this.popupMenu = menu.requestMenu();

		currentMousePosition = new Point(0, 0);
		dragStartPoint = new Point(0, 0);
		dragEndPoint = new Point(0, 0);

		selecting = false;
	}

	/**
	 * liefert die X-Position der Maus
	 * 
	 * @return
	 */
	public int getMouseX() {
		return currentMousePosition.x;
	}

	/**
	 * Setzt eine neue X-Position der Maus
	 * 
	 * @param mouseX
	 */
	@Deprecated
	public void setMouseX(final int mouseX) {
		currentMousePosition.x = mouseX;
	}

	/**
	 * liefert die Y-Position der Maus
	 * 
	 * @return
	 */
	public int getMouseY() {
		return currentMousePosition.y;
	}

	/**
	 * Setzt die Y-Position der Maus
	 * 
	 * @param mouseY
	 */
	@Deprecated
	public void setMouseY(final int mouseY) {
		currentMousePosition.y = mouseY;
	}

	/**
	 * Markieren
	 * 
	 * @return boolean ob das markieren flag gesetzt ist
	 */
	public boolean isSelecting() {
		return selecting;
	}

	/**
	 * Berechnung des Rectangles des markierten bereichs
	 * 
	 * @return Rectangle ein rectangle-objekt mit dem startpunkt und gr��e des
	 *         markierten bereichs
	 */
	public Rectangle getSelectedArea() {
		/*
		 * final int xStart; final int yStart;
		 * 
		 * final int xStop; final int yStop;
		 * 
		 * final int xDiff; final int yDiff;
		 * 
		 * xStart = Math.min(dragStartPoint.x, dragEndPoint.x); yStart =
		 * Math.min(dragStartPoint.y, dragEndPoint.y);
		 * 
		 * xStop = Math.max(dragStartPoint.x, dragEndPoint.x); yStop =
		 * Math.max(dragStartPoint.y, dragEndPoint.y);
		 * 
		 * xDiff = xStop - xStart; yDiff = yStop - yStart;
		 * 
		 * result = new Rectangle(xStart, yStart, xDiff, yDiff);
		 */

		final Point start;
		final Point stop;
		final Dimension size;
		final Rectangle result;

		start = new Point(Math.min(dragStartPoint.x, dragEndPoint.x), Math.min(dragStartPoint.y, dragEndPoint.y));

		stop = new Point(Math.max(dragStartPoint.x, dragEndPoint.x), Math.max(dragStartPoint.y, dragEndPoint.y));

		size = new Dimension(stop.x - start.x, stop.y - start.y);

		result = new Rectangle(start, size);

		return result;
	}

	// --------------------------------------------------------------------------
	// SECTION: interface methods
	// --------------------------------------------------------------------------

	/**
	 * wird bei jeder Mausbewegung aufgerufen, ohne das eine maustaste gedr�ckt
	 * ist
	 * 
	 * Aktualisieren der Mausposition
	 * 
	 * @param MouseEvent
	 *            event the mouse event
	 */
	@Override
	public void mouseMoved(final MouseEvent event) {
		currentMousePosition = event.getPoint();

		event.consume();
	}

	/**
	 * wird bei jeder Mausbewegung aufgerufen, w�hrend eine maustaste gedr�ckt
	 * ist
	 * 
	 * Makieren von Tischen
	 * 
	 * @param MouseEvent
	 *            event the mouse event
	 */
	@Override
	public void mouseDragged(final MouseEvent event) {
		currentMousePosition = event.getPoint();

		dragEndPoint.setLocation(currentMousePosition);

		if (selecting) {
			danceHall.selectTablesByRectangle(getSelectedArea());
			danceHall.repaint();
		}
	}

	/**
	 * wird bei jedem tastendruck aufgerufen, delegation vom Panel
	 * 
	 * Beinhaltet bewegen der Maus innerhalb des Applets,
	 * 
	 * @param MouseEvent
	 *            event the mouse event
	 */
	public void processMouseEvent(final MouseEvent event) {
		if (event.isPopupTrigger()) {
			System.out.println("DanceFloorMouseHandler.processMouseEvent()->popupTrigger");

			popupMenu.show(event.getComponent(), event.getX(), event.getY());

			return;
		} else if ((event.getModifiers() & InputEvent.BUTTON1_MASK) != 0 && event.getID() == MouseEvent.MOUSE_PRESSED) {
			System.out.println("DanceFloorMouseHandler.processMouseEvent()->mouse_pressed_button1");

			selecting = true;

			danceHall.clearSelection();
			danceHall.requestFocus();

			currentMousePosition = event.getPoint();

			dragStartPoint.setLocation(currentMousePosition);
			dragEndPoint.setLocation(currentMousePosition);
		} else if ((event.getModifiers() & InputEvent.BUTTON1_MASK) != 0 && event.getID() == MouseEvent.MOUSE_RELEASED) {
			System.out.println("DanceFloorMouseHandler.processMouseEvent()->mouse_released_button1");

			selecting = false;

			danceHall.selectTablesByRectangle(getSelectedArea());
			danceHall.repaint();
		}
	}
}
