/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancehall;

import static tabledancer.Constants.CONFIG_FLOOR_BACKGROUND_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_OPACITY;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_STROKE;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * Created by IntelliJ IDEA. User: jens Date: Jun 13, 2005 Time: 2:22:41 PM
 * 
 */
public class DanceHallBackgroundImageHandler {
	/**
	 * creates a new buffered image with the background image and raster
	 * settings of the given DanceFloorPanel
	 * 
	 * @param danceFloorPanel
	 * @return
	 */
	public static BufferedImage createBackgroundGridImage(final DanceHallPanel danceHallPanel) {
		return createBackgroundGridImage(danceHallPanel, danceHallPanel.sourceBackgroundImage);
	}

	/**
	 * creates a new buffered image with the given background image and the
	 * raster settings of the given DanceFloorPanel
	 * 
	 * @param DanceFloorPanel
	 *            danceFloorPanel the dancefloor panel
	 * @param Image
	 *            backgroundImage the source image to use
	 * 
	 * @return BufferedImage the image object
	 */
	public static BufferedImage createBackgroundGridImage(final DanceHallPanel danceHallPanel,
			final Image backgroundImage) {
		final int backgroundImageWidth;
		final int backgroundImageHeight;

		final BufferedImage resultImage;
		final Graphics2D resultImage2d;

		// ----------------------------------------------------------------------
		// SECTION: create image objects
		// ----------------------------------------------------------------------

		if (backgroundImage == null) {
			// TODO make configurable
			backgroundImageWidth = 800;
			backgroundImageHeight = 600;
		} else {
			backgroundImageWidth = backgroundImage.getWidth(null);
			backgroundImageHeight = backgroundImage.getHeight(null);
		}

		resultImage = new BufferedImage(backgroundImageWidth, backgroundImageHeight, BufferedImage.TYPE_INT_ARGB);

		resultImage2d = resultImage.createGraphics();

		resultImage2d.setComposite(AlphaComposite.SrcOver);

		// ----------------------------------------------------------------------
		// SECTION: create background
		// ----------------------------------------------------------------------

		if (danceHallPanel.isBackgroundVisible() && backgroundImage != null) {
			// use the backgroundImage
			resultImage2d.drawImage(backgroundImage, 0, 0, backgroundImageWidth, backgroundImageHeight, danceHallPanel);
		} else {
			// draw an empty white rect
			resultImage2d.setColor(CONFIG_FLOOR_BACKGROUND_COLOR);
			resultImage2d.fillRect(0, 0, backgroundImageWidth, backgroundImageHeight);
		}

		// ----------------------------------------------------------------------
		// SECTION: create grid
		// ----------------------------------------------------------------------

		if (danceHallPanel.isGridVisible()) {
			// draw grid lines
			resultImage2d.setColor(CONFIG_FLOOR_GRID_COLOR);
			resultImage2d.setStroke(CONFIG_FLOOR_GRID_STROKE);
			resultImage2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, CONFIG_FLOOR_GRID_OPACITY));

			// Senkrechte
			for (int i = 0; i <= backgroundImageWidth; i += danceHallPanel.getGridSize().width) {
				resultImage2d.drawLine(i, 0, i, backgroundImageHeight);
			}

			// Vertikale
			for (int i = 0; i <= backgroundImageHeight; i += danceHallPanel.getGridSize().height) {
				resultImage2d.drawLine(0, i, backgroundImageWidth, i);
			}
		}

		return resultImage;
	}
}
