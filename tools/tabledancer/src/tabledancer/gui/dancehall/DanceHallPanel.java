/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancehall;

import static tabledancer.Constants.CONFIG_FLOOR_GRID_SIZE;
import static tabledancer.Constants.CONFIG_FLOOR_SELECTION_BORDER_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_SELECTION_BORDER_STROKE;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import tabledancer.ImageManager;
import tabledancer.TableDancer;
import tabledancer.gui.dancetable.DanceTable;

/**
 * panel handling of the dancehall
 */
@SuppressWarnings("serial")
public abstract class DanceHallPanel extends JPanel {
	/**
	 * overlay class for the selecting rect
	 * 
	 */
	class SlopePanel extends JPanel {
		private static final long serialVersionUID = -1L;

		/**
		 * constructor
		 */
		public SlopePanel() {
			setOpaque(false);
		}

		/**
		 * paint the component
		 */
		@Override
		public void paintComponent(final Graphics g) {
			super.paintComponent(g);

			if (!mouseHandler.isSelecting()) {
				return;
			}

			final Rectangle slope;
			final Graphics2D g2d;

			slope = mouseHandler.getSelectedArea();

			g2d = (Graphics2D) g;
			g2d.setColor(CONFIG_FLOOR_SELECTION_BORDER_COLOR);
			g2d.setStroke(CONFIG_FLOOR_SELECTION_BORDER_STROKE);
			g2d.drawRect(slope.x, slope.y, slope.width, slope.height);
		}
	}

	protected DanceHallMouseListener mouseHandler;

	protected Image sourceBackgroundImage;
	protected Image currentBackgroundImage;

	protected Dimension gridSize;

	private boolean backgroundVisible;
	private boolean gridVisible;
	private boolean gridAlign;

	private boolean shiftPressed;

	/**
	 * Konstruktor
	 * 
	 * @param theTableDancer
	 */
	protected DanceHallPanel() {
		final ImageManager resourceHandler;
		final MediaTracker mediaTracker;
		final JPanel slopePanel;

		this.gridSize = CONFIG_FLOOR_GRID_SIZE;

		resourceHandler = TableDancer.getInstance().getImageManager();
		mediaTracker = new MediaTracker(this);
		mouseHandler = new DanceHallMouseListener((DanceHall) this);
		sourceBackgroundImage = resourceHandler.getBackgroundImageOverview();
		currentBackgroundImage = generateBackgroundGridImage();

		mediaTracker.addImage(sourceBackgroundImage, 0);
		try {
			mediaTracker.waitForAll();
		} catch (InterruptedException e) {
		}

		setBackgroundVisible(true);
		setGridVisible(true);
		setGridAlign(false);
		setShiftPressed(false);

		// spezieller JPanel, der ?ber allen anderen liegt, damit die Tables von
		// der Linie ?bermalt werden k?nnen und sie nicht darunter herumrutscht.
		// sieht einfach besser aus ;-)
		slopePanel = new SlopePanel();
		slopePanel.setPreferredSize(getPreferredSize());
		slopePanel.setMaximumSize(getPreferredSize());
		slopePanel.setMinimumSize(getPreferredSize());
		slopePanel.setBounds(0, 0, getMaxWidth(), getMaxHeight());

		setLayout(null);
		setBounds(0, 0, getWidth(), getHeight());
		addMouseMotionListener(mouseHandler);
		add(slopePanel);

		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
		enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}

	/**
	 * re-creates the background image for this block with current settings
	 * applied
	 * 
	 * @return BufferedImage the image object
	 */
	private BufferedImage generateBackgroundGridImage() {
		final BufferedImage resultImage;

		resultImage = DanceHallBackgroundImageHandler.createBackgroundGridImage(this);

		return resultImage;
	}

	/**
	 * paints the component
	 */
	@Override
	public void paintComponent(final Graphics g) {
		super.paintComponent(g);

		g.drawImage(currentBackgroundImage, 0, 0, currentBackgroundImage.getWidth(null),
				currentBackgroundImage.getHeight(null), this);
	}

	// --------------------------------------------------------------------------
	// SECTION: mouse wrapper
	// --------------------------------------------------------------------------

	/**
	 * Wrapper fuer Mausevents
	 * 
	 * @param event
	 */
	@Override
	public void processMouseEvent(final MouseEvent event) {
		mouseHandler.processMouseEvent(event);
	}

	// --------------------------------------------------------------------------
	// SECTION: background
	// --------------------------------------------------------------------------

	/**
	 * returns the current grid size
	 * 
	 * @return Dimension for one grid box
	 */
	public Dimension getGridSize() {
		return gridSize;
	}

	/**
	 * set the grid size
	 * 
	 * @param int width the width of one grid box
	 * @param int height the height of one grid box
	 */
	public void setGridSize(final int width, final int height) {
		gridSize.setSize(width, height);

		currentBackgroundImage = generateBackgroundGridImage();
		repaint();
	}

	/**
	 * liefert die Breite des Hintergrundbildes
	 * 
	 * @return maxWidth
	 */
	public int getMaxWidth() {
		return 1000;
		// return currentBackgroundImage.getWidth(null);
	}

	/**
	 * lierfert die hoehe des Hintergrundbildes
	 * 
	 * @return maxHeigth
	 */
	public int getMaxHeight() {
		return 1000;
		// return currentBackgroundImage.getHeight(null);
	}

	// --------------------------------------------------------------------------
	// SECTION: flags
	// --------------------------------------------------------------------------

	/**
	 * Am Raster ausrichten
	 * 
	 * @return boolean flag
	 */
	public boolean isGridAlign() {
		return gridAlign;
	}

	/**
	 * Am Raster ausrichten
	 * 
	 * @param boolean value
	 */
	public void setGridAlign(final boolean value) {
		gridAlign = value;
		// TODO methode aufrufen die alle tische am raster ausrichtet
	}

	/**
	 * Raster sichbar
	 * 
	 * @return boolean flag
	 */
	public boolean isGridVisible() {
		return gridVisible;
	}

	/**
	 * Raster sichtbar
	 * 
	 * @param boolean value
	 */
	public void setGridVisible(final boolean value) {
		gridVisible = value;

		currentBackgroundImage = generateBackgroundGridImage();
		repaint();
	}

	/**
	 * Hintergrundbild anzeigen
	 * 
	 * @return boolean flag
	 */
	public boolean isBackgroundVisible() {
		return backgroundVisible;
	}

	/**
	 * Hintergrundbild anzeigen
	 * 
	 * @param boolean value
	 */
	public void setBackgroundVisible(final boolean value) {
		backgroundVisible = value;

		currentBackgroundImage = generateBackgroundGridImage();
		repaint();
	}

	/**
	 * Shift-Taste
	 * 
	 * @return boolean flag
	 */
	public boolean isShiftPressed() {
		return shiftPressed;
	}

	/**
	 * Shift-Taste
	 * 
	 * @param boolean value
	 */
	public void setShiftPressed(final boolean value) {
		shiftPressed = value;
	}

	// --------------------------------------------------------------------------
	// SECTION: business logic
	// --------------------------------------------------------------------------

	/**
	 * Setzt das Makiert-Flag fuer alle Tische
	 * 
	 * @param boolean value das flag
	 */
	public void selectTables(final boolean value) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			danceTable.setSelected(value);
		}

		repaint();
	}

	/**
	 * waehlt die Tische im gezogenden Rechteck aus
	 * 
	 * @param Rectangle
	 *            selectedArea der ausgewaehlte bereich
	 */
	public void selectTablesByRectangle(final Rectangle selectedArea) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			/*
			 * we need to use the seat position here, because the seat could be
			 * positioned outside the range of the background image
			 */
			danceTable.setSelected(selectedArea.contains(danceTable.getSeatPosition()));
		}

		repaint();
	}

	/**
	 * wrapper fuer selectTables() mit argument true
	 */
	public void selectAllTables() {
		selectTables(true);
	}

	/**
	 * wrapper fuer selectTables() mit argument false
	 */
	public void clearSelection() {
		selectTables(false);
	}
}
