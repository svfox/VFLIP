/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancehall;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;

/**
 * Verarbeitung den JPopupMenu
 */
public class DanceHallMenuActionListener implements ActionListener {
	/**
	 * der dancefloor dem der actionlistener zugeordnet ist
	 */
	private DanceHall danceHall;

	/**
	 * Konstruktor
	 * 
	 * @param DanceFloor
	 *            danceFloor der kontext
	 */
	public DanceHallMenuActionListener(final DanceHall danceFloor) {
		this.danceHall = danceFloor;
	}

	/***************************************************************************
	 * Verarbeitung des Menuaufrufs
	 * 
	 * @param ActionEvent
	 *            event das event
	 */
	public void actionPerformed(final ActionEvent event) {
		final JComponent menuentry;

		menuentry = (JComponent) event.getSource();

		switch (Integer.parseInt(menuentry.getName())) {
		// Neuen Tisch erzeugen
		case 1: {
			// TableDancer.getInstance().createNewDanceFloor(false);
		}
			break;

		// Rastersichtbarkeit
		case 2: {
			danceHall.setGridVisible(((JCheckBoxMenuItem) menuentry).getState());
		}
			break;

		// Raster einrasten...
		case 3: {
			danceHall.setGridAlign(((JCheckBoxMenuItem) menuentry).getState());
		}
			break;

		// Raumsichtbarkeit
		case 4: {
			danceHall.setBackgroundVisible(((JCheckBoxMenuItem) menuentry).getState());
		}
			break;

		// Alle Bl�cke selektieren
		case 5: {
			danceHall.selectAllBlocks();
		}
			break;
		}
	}

}
