package tabledancer.gui.dancehall;

import java.awt.Cursor;

import tabledancer.gui.dancetable.DanceTable;

/**
 * <p>
 * Description: Applet for table positioning in the FLIP intranet system
 * </p>
 * 
 * @author Kai 'Riffer' Posadowsky
 * @version 1.0
 */
public class DanceHall extends DanceHallPanel {
	/**
	 * serial id
	 */
	private static final long serialVersionUID = -6894533268218762371L;

	/**
	 * drag & drop state handling
	 */
	private boolean isGettingDragged;

	/**
	 * drag & drop state handling
	 */
	private boolean isDragCanceled;

	/**
	 * Konstruktor
	 * 
	 * @param Block
	 *            block die Block-Bean fuer diesen DanceFloor
	 * @throws Exception
	 */
	public DanceHall()

	{
		super();
		setVisible(false);

		isGettingDragged = false;
		isDragCanceled = false;
	}

	/**
	 * wird ausgefuehrt, wenn ...?
	 * 
	 * @param xdiff
	 * @param ydiff
	 */
	public void setDragPosition(final int xdiff, final int ydiff) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			if (!danceTable.isSelected()) {
				continue;
			}

			/*
			 * final DanceTable dragTable;
			 * 
			 * dragTable = danceTable.requestDragInstance();
			 * 
			 * dragTable.setPosition( danceTable.getPosition().x + xdiff,
			 * danceTable.getPosition().y + ydiff );
			 */
		}
	}

	/**
	 * Wird ausgefuehrt, wenn min. ein Chair bewegt wurde.
	 */
	public void fixSelectedPosition() {
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			if (!danceTable.isSelected()) {
				continue;
			}

			/*
			 * final DanceTable dragTable;
			 * 
			 * dragTable = danceTable.requestDragInstance();
			 * 
			 * danceTable.setPosition(dragTable.getPosition());
			 * danceTable.removeDragInstance();
			 */
		}
	}

	/**
	 * wird ausgefuehrt, wenn ...?
	 */
	public void cancelSelectedPosition() {
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			if (!danceTable.isSelected()) {
				continue;
			}

			// danceTable.removeDragInstance();
		}

		setCursor(Cursor.getDefaultCursor());
	}

	/**
	 * Drueckt jemand mit der Maus auf einen Chair oder laesst los.
	 * 
	 * @param value
	 */
	public void setGettingDragged(final boolean value) {
		isGettingDragged = value;
	}

	/**
	 * ob grade jemand mit der maus auf einen chair drueckt.
	 * 
	 * @return boolean flag
	 */
	public boolean isGettingDragged() {
		return isGettingDragged;
	}

	/**
	 * drueckt jemand waehrend der auswahl die escape-taste
	 * 
	 * @param boolean value flag
	 */
	public void setDragCanceled(final boolean value) {
		isDragCanceled = value;
	}

	/**
	 * ob jemand waehrend der auswahl die escape-taste drueckt
	 * 
	 * @return boolean flag
	 */
	public boolean isDragCanceled() {
		return isDragCanceled;
	}

	/**
     * 
     */
	public void selectAllBlocks() {
	}
}
