/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancetable;

import static tabledancer.Constants.COMMAND_CLONE;
import static tabledancer.Constants.COMMAND_REMOVE;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * action listener for the popup menu
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class DanceTableMenuActionListener implements ActionListener {
	/**
	 * the dancefloor instance
	 */
	private DanceTable danceTable;

	/**
	 * constructor
	 * 
	 * @param DanceTable
	 *            danceTable the instance
	 */
	public DanceTableMenuActionListener(final DanceTable danceTable) {
		this.danceTable = danceTable;
	}

	/**
	 * Invoked when an action occurs.
	 * 
	 * @param ActionEvent
	 *            event the event
	 */
	public void actionPerformed(final ActionEvent event) {
		final List<DanceTable> selectedTables;

		selectedTables = danceTable.getDanceFloor().getSelectedTables();

		if (COMMAND_CLONE.equals(event.getActionCommand())) {
			if (selectedTables.size() == 0) {
				DanceTable.cloneTable(danceTable);
			} else {
				for (int i = 0; i < selectedTables.size(); i++) {
					DanceTable.cloneTable(selectedTables.get(i));
				}
			}
		}

		if (COMMAND_REMOVE.equals(event.getActionCommand())) {
			if (selectedTables.size() == 0) {
				DanceTable.removeTable(danceTable);
			} else {
				for (int i = 0; i < selectedTables.size(); i++) {
					DanceTable.removeTable(selectedTables.get(i));
				}
			}
		}

		if (event.getActionCommand().length() <= 3) {
			int newAngle;

			newAngle = Integer.parseInt((event.getActionCommand()));

			if (selectedTables.size() == 0) {
				danceTable.setAngle(newAngle);
			} else {
				for (int i = 0; i < selectedTables.size(); i++) {
					final DanceTable danceTable;

					danceTable = selectedTables.get(i);
					danceTable.setAngle(newAngle);
				}
			}
		}

		danceTable.getDanceFloor().repaint();
	}
}
