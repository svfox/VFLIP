/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancetable;

import static tabledancer.Constants.CONFIG_TABLE_ROTATION_MULTIPLIER;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import tabledancer.Constants;

/**
 * Verarbeitung der Tastatureingaben
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class DanceTableKeyListener implements KeyListener {
	private static int CTRL = KeyEvent.CTRL_MASK;
	private static int CTRL_SHIFT = KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK;

	/**
	 * the dancetablepanel context
	 */
	private DanceTable danceTable;

	/**
	 * construktor
	 * 
	 * @param DanceTablePanel
	 *            table the table context
	 */
	public DanceTableKeyListener(final DanceTable table) {
		this.danceTable = table;
	}

	/**
	 * Invoked when a key has been pressed. See the class description for
	 * KeyEvent for a definition of a key pressed event.
	 * 
	 * arrow keys: moves selected tables by 1 unit arrow keys plus ctrl: moves
	 * selected tables on the grid arrow keys plus ctrl plus shift: rotates the
	 * table by CONFIG_TABLE ROTATION_MULTIPLIER degrees
	 * 
	 * @param KeyEvent
	 *            event the event
	 * 
	 * @see Constants#CONFIG_TABLE_ROTATION_MULTIPLIER
	 */
	@Override
	public void keyPressed(final KeyEvent event) {
		final int keyCode;
		final int keyModifiers;
		int movement;

		keyCode = event.getKeyCode();
		keyModifiers = event.getModifiers();
		movement = 1;

		switch (keyCode) {
		case KeyEvent.VK_UP: {
			if ((keyModifiers & CTRL_SHIFT) == CTRL_SHIFT) {
				danceTable.getDanceFloor().rotateSelectedTables(
						-CONFIG_TABLE_ROTATION_MULTIPLIER * CONFIG_TABLE_ROTATION_MULTIPLIER);
			} else {
				if ((keyModifiers & CTRL) == CTRL) {
					movement *= danceTable.getDanceFloor().getGridSize().height;
				}

				danceTable.getDanceFloor().moveSelectedTables(new Point(0, -movement));
			}

			event.consume();
			break;
		}

		case KeyEvent.VK_DOWN: {
			if ((keyModifiers & CTRL_SHIFT) == CTRL_SHIFT) {
				danceTable.getDanceFloor().rotateSelectedTables(
						CONFIG_TABLE_ROTATION_MULTIPLIER * CONFIG_TABLE_ROTATION_MULTIPLIER);
			} else {
				if ((keyModifiers & CTRL) == CTRL) {
					movement *= danceTable.getDanceFloor().getGridSize().height;
				}

				danceTable.getDanceFloor().moveSelectedTables(new Point(0, movement));
			}
			event.consume();
			break;
		}

		case KeyEvent.VK_LEFT: {
			if ((keyModifiers & CTRL_SHIFT) == CTRL_SHIFT) {
				danceTable.getDanceFloor().rotateSelectedTables(CONFIG_TABLE_ROTATION_MULTIPLIER);
			} else {
				if ((keyModifiers & CTRL) == CTRL) {
					movement *= danceTable.getDanceFloor().getGridSize().width;
				}

				danceTable.getDanceFloor().moveSelectedTables(new Point(-movement, 0));
			}

			event.consume();
			break;
		}

		case KeyEvent.VK_RIGHT: {
			if ((keyModifiers & CTRL_SHIFT) == CTRL_SHIFT) {
				danceTable.getDanceFloor().rotateSelectedTables(-CONFIG_TABLE_ROTATION_MULTIPLIER);
			} else {
				if ((keyModifiers & CTRL) != 0) {
					movement *= danceTable.getDanceFloor().getGridSize().width;
				}

				danceTable.getDanceFloor().moveSelectedTables(new Point(movement, 0));
			}

			event.consume();
			break;
		}
		}
	}

	/**
	 * Invoked when a key has been released. See the class description for
	 * KeyEvent for a definition of a key released event.
	 * 
	 * @param KeyEvent
	 *            event the event
	 */
	@Override
	public void keyReleased(final KeyEvent event) {
//		final DanceFloor danceFloor;
//
//		danceFloor = danceTable.getDanceFloor();
//
//		if (danceFloor.isDragCanceled()) {
//			return;
//		}
//
//		if (danceFloor.isDragging()) {
//			danceFloor.updateDraggedTables();
//			danceFloor.setDragging(false);
//		}
	}

	/**
	 * Invoked when a key has been typed. See the class description for KeyEvent
	 * for a definition of a key typed event.
	 * 
	 * @param KeyEvent
	 *            event the event
	 */
	@Override
	public void keyTyped(final KeyEvent event) {
		int keyValue;

		keyValue = Integer.valueOf(event.getKeyChar());

		switch (keyValue) {
		case KeyEvent.VK_ESCAPE: {
			danceTable.getDanceFloor().cancelDragging();
			break;
		}

		case KeyEvent.VK_DELETE: {
			danceTable.getDanceFloor().removeTable(danceTable);
			break;
		}

		}
	}
}
