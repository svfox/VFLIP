/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancetable;

import static tabledancer.Constants.CONFIG_ID_NEW_SEAT;
import static tabledancer.Constants.CONFIG_TABLE_ANGLE_COLOR;
import static tabledancer.Constants.CONFIG_TABLE_ANGLE_FONT;
import static tabledancer.Constants.CONFIG_TABLE_CLONE_DISTANCE;
import static tabledancer.Constants.CONFIG_TABLE_DOT_COLOR;
import static tabledancer.Constants.CONFIG_TABLE_DOT_SIZE;
import static tabledancer.Constants.CONFIG_TABLE_SELECTED_BACKGROUND_COLOR;
import static tabledancer.Constants.CONFIG_TABLE_SELECTED_BORDER_COLOR;
import static tabledancer.Constants.CONFIG_TABLE_SELECTED_BORDER_STROKE;
import static tabledancer.Constants.EMPTY_STRING;
import static tabledancer.Constants.LABEL_CLONE;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import tabledancer.flip.bean.Seat;
import tabledancer.gui.dancefloor.DanceFloor;

/**
 * represents a seat on the dancefloor
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class DanceTable extends JPanel {
	/**
	 * serial id
	 */
	private static final long serialVersionUID = 19685263610197526L;

	/**
	 * the mouse listener
	 */
	private final DanceTableMouseListener danceTableMouseListener;

	/**
	 * the popup menu
	 */
	private final JPopupMenu danceTablePopupMenu;

	/**
	 * the associated dancefloor
	 */
	private final DanceFloor danceFloor;

	/**
	 * the assigned seat object
	 */
	private final Seat seat;

	/**
	 * is selected
	 */
	private boolean isSelected;

	/**
	 * is dragged
	 */
	private boolean isDragged;
	
	private int currentPositionX;
	private int currentPositionY;
	
	/**
	 * 
	 * @param sourceTable
	 */
	public static void cloneTable(final DanceTable sourceTable) {
		final DanceTable cloneTable;

		cloneTable = sourceTable.clone();
		cloneTable.setVisible(true);
		cloneTable.setSelected(true);
		cloneTable.moveToSeatPosition();

		// notify block and dancefloor
		sourceTable.getSeat().getBlock().addSeat(cloneTable.getSeat());
		sourceTable.getDanceFloor().addTable(cloneTable);
		sourceTable.setSelected(false);
		sourceTable.requestFocus();

		cloneTable.repaint();
	}

	/**
	 * deletes the given dancetable instance
	 * 
	 * @param DanceTable
	 *            danceTable the dancetable to delete
	 * 
	 * @see DanceFloor#removeTable(DanceTable)
	 */
	public static void removeTable(DanceTable danceTable) {
		danceTable.getDanceFloor().removeTable(danceTable);
	}

	/**
	 * constructor
	 * 
	 * @param DanceFloor
	 *            danceFloor the associated dancefloor
	 * @param Seat
	 *            seat the associated seat
	 */
	public DanceTable(final DanceFloor danceFloor, final Seat seat) {

		this.danceFloor = danceFloor;
		this.seat = seat;

		this.isSelected = false;
		this.isDragged = false;

		this.danceTableMouseListener = new DanceTableMouseListener(this);
		this.danceTablePopupMenu = DanceTableMenuFactory.getInstance().newMenu(this);

		this.adjustBoundingRect();
		
		this.setOpaque(false);
		this.setFocusable(true);
		this.setToolTipText(createToolTipText());

		this.addMouseListener(danceTableMouseListener);
		this.addMouseMotionListener(danceTableMouseListener);
		this.addMouseWheelListener(danceTableMouseListener);
		this.addKeyListener(new DanceTableKeyListener(this));
	}

	/**
	 * clones the current object and its related objects
	 * 
	 * @return DanceTable the cloned object
	 */
	@Override
	public DanceTable clone() {
		final Seat newSeat;
		final DanceTable newTable;

		newSeat = new Seat(seat.getBlock());

		newSeat.setId(CONFIG_ID_NEW_SEAT);
		newSeat.setName(LABEL_CLONE + seat.getName());
		newSeat.setIp(EMPTY_STRING);
		newSeat.setEnabled(seat.isEnabled());
		newSeat.setAngle(seat.getAngle());
		newSeat.setCenterPosX(seat.getCenterPosX() + CONFIG_TABLE_CLONE_DISTANCE);
		newSeat.setCenterPosY(seat.getCenterPosY() + CONFIG_TABLE_CLONE_DISTANCE);

		newTable = new DanceTable(danceFloor, newSeat);

		return newTable;
	}

	/**
	 * draws the table
	 */
	@Override
	public void paintComponent(final Graphics g) {
		super.paintComponent(g);

		final Graphics2D g2d;
		// final Point chairOffset;
		final Image image;
		final int imageWidth;
		final int imageHeight;
		final int xdiff;
		final int ydiff;

		image = createImage(true);
		imageWidth = image.getWidth(null);
		imageHeight = image.getHeight(null);

		xdiff = (getWidth() - imageWidth) / 2;
		ydiff = (getHeight() - imageHeight) / 2;

		g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g2d.translate(xdiff, ydiff);
		g2d.rotate(Math.toRadians(360 - getAngle()), imageWidth / 2, imageHeight / 2);

		if (isDragged()) {
			// a dragged table is a little bit transparent
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
		}

		g2d.drawImage(image, 0, 0, this);

		if (isSelected()) {
			// a selected table has a background color
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.25f));

			g2d.setColor(CONFIG_TABLE_SELECTED_BACKGROUND_COLOR);
			g2d.fillRect(0, 0, imageWidth, imageHeight);

			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
		}

		if (hasFocus()) {
			// a focussed table has a border
			if (CONFIG_TABLE_SELECTED_BORDER_STROKE != null) {
				g2d.setStroke(CONFIG_TABLE_SELECTED_BORDER_STROKE);
			}

			g2d.setColor(CONFIG_TABLE_SELECTED_BORDER_COLOR);
			g2d.drawRect(0, 0, imageWidth, imageHeight);
		}

		// roter Punkt in der Mitte
		g2d.setColor(CONFIG_TABLE_DOT_COLOR);
		g2d.fillOval(imageWidth / 2 - CONFIG_TABLE_DOT_SIZE / 2, imageHeight / 2 - CONFIG_TABLE_DOT_SIZE / 2,
				CONFIG_TABLE_DOT_SIZE, CONFIG_TABLE_DOT_SIZE);

		/*
		 * double angle; double cos; double sin; Dimension test;
		 * 
		 * test = new Dimension();
		 * 
		 * angle = Math.toRadians(360 - getAngle()); cos = Math.cos(angle); sin
		 * = Math.sin(angle);
		 * 
		 * test.width = Math.abs((int)Math.round(imageWidth * (1.0d + cos)));
		 * test.height = Math.abs((int)Math.round(imageHeight * (1.0d + sin)));
		 * 
		 * System.out.println(test);
		 * 
		 * this.setSize(test); this.setPreferredSize(test);
		 */
	}

	/**
	 * shows the popup of this dancefloor
	 * 
	 * @param int x the x position
	 * @param int y the y position
	 */
	public void showPopupMenu(final int x, final int y) {
		danceTablePopupMenu.show(this, x, y);
	}

	private String createToolTipText() {
		final StringBuilder sb;
		final String result;

		sb = new StringBuilder();

		sb.append("<html><table border=0 cellspacing=0 cellpadding=1>");

		sb.append("<tr><td><b>ID: </b></td><td>");
		sb.append(seat.getId());
		sb.append("</td></tr>");

		sb.append("<tr><td><b>Name: </b></td><td>");
		sb.append(seat.getName());
		sb.append("</td></tr>");

		sb.append("<tr><td><b>IP: </b></td><td>");
		sb.append(seat.getIp());
		sb.append("</td></tr>");

		sb.append("<tr><td><b>Position: </b></td><td>");
		sb.append(this.currentPositionX);
		sb.append("/");
		sb.append(this.currentPositionY);
		sb.append("</td></tr>");

		sb.append("</table></html>");

		result = sb.toString();

		return result;
	}

	private void updateToolTipText() {
		this.setToolTipText(this.createToolTipText());
	}
	
	/**
	 * liefert das Bild des Tisches mit Stuhl
	 * 
	 * @param boolean printAngle wenn true, wird der aktuelle winkel
	 *        eingezeichnet
	 * 
	 * @return BufferedImage das bild des tisches
	 */
	private Image createImage(final boolean printAngle) {
		final int snapAngle;
		final Image tableImage;
		final Image chairImage;
		final BufferedImage tableAndChair;
		final Graphics2D tableAndChair2d;

		snapAngle = getSnapAngle();

		tableImage = danceFloor.getImageHandler().getTableImage(snapAngle, seat.isEnabled());
		chairImage = danceFloor.getImageHandler().getChairImage(snapAngle);
		tableAndChair = new BufferedImage(tableImage.getWidth(null), tableImage.getHeight(null)
				+ chairImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		tableAndChair2d = tableAndChair.createGraphics();
		tableAndChair2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

		tableAndChair2d.drawImage(tableImage, 0, 0, this);

		tableAndChair2d.drawImage(chairImage, (tableImage.getWidth(null) / 2) - (chairImage.getWidth(null) / 2),
				tableImage.getHeight(null), this);

		if (printAngle) {
			tableAndChair2d.setColor(CONFIG_TABLE_ANGLE_COLOR);
			tableAndChair2d.setFont(CONFIG_TABLE_ANGLE_FONT);
			tableAndChair2d.drawString(Integer.toString(getAngle()), 2,
					new Float(tableImage.getHeight(null) / 1.5).intValue());
		}

		return tableAndChair;
	}

	/**
	 * gibt an wo der Stuhl Positioniert werden muss
	 * 
	 * @author loom
	 * @since 2007-02-09
	 * @return Punkt mit X- und Y-Differenz zur oberen linken Ecke
	 */
	private Point getChairOffset() {
		final Image sizeimage;

		final int angle;
		final int stuhlHoehe;
		final int x;
		final int y;

		// calculates an absolute angle between 0 and 360 degrees
		angle = seat.getAngle() % 360;

		// Tisch verschieben
		sizeimage = danceFloor.getImageHandler().getChairImage(getSnapAngle());
		// sizeimage = createImage(false);
		stuhlHoehe = sizeimage.getHeight(null) / 2;

		x = (int) (Math.sin(Math.toRadians(angle)) * stuhlHoehe);
		y = (int) (Math.cos(Math.toRadians(angle)) * stuhlHoehe);

		return new Point(x, y);
	}

	/**
	 * returns the assigned seat object
	 * 
	 * @return Seat the seat object
	 */
	public DanceFloor getDanceFloor() {
		return danceFloor;
	}

	/**
	 * returns the assigned seat object
	 * 
	 * @return Seat the seat object
	 */
	public Seat getSeat() {
		return seat;
	}

	// --------------------------------------------------------------------------
	// SECTION: state
	// --------------------------------------------------------------------------

	public void setDragged(final boolean value) {
		isDragged = value;

		repaint();
	}

	public boolean isDragged() {
		return isDragged;
	}

	public void setSelected(final boolean selected) {
		isSelected = selected;

		repaint();
	}

	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * determines if the given point is inside the selected dot TODO rework this
	 * method using a glass pane
	 * 
	 * @param point
	 * @return
	 */
	public boolean canSelect(final Point point) {
		final Image sizeimage;
		final Point center;
		final boolean result;

		sizeimage = createImage(false);
		center = new Point(getWidth() / 2, getHeight() / 2);

		// DOT_SIZE/2 limits the hover radius too much, 1.5 is a good compromise
		// result = (center.distance(point) <= (float)(CONFIG_TABLE_DOT_SIZE /
		// 1.8));
		result = (center.distance(point) <= sizeimage.getWidth(null));

		return result;
	}

	// --------------------------------------------------------------------------
	// SECTION: appearance
	// --------------------------------------------------------------------------

	/**
	 * set the angle of the table
	 * 
	 * if the angle is negative and/or higher than 360 degrees, it will be
	 * converted to an absolute angle bewetween 0 and 360
	 * 
	 * @param int angle the new angle
	 */
	public void setAngle(int angle) {
		// calculate absolute angle
		angle = angle % 360;

		if (angle < 0) {
			angle = 360 + angle;
		}

		seat.setAngle(angle);

		this.adjustBoundingRect();
		
		repaint();
	}

	/**
	 * returns the angle of the table
	 * 
	 * @return int the angle
	 */
	public int getAngle() {
		return seat.getAngle();
	}

	/**
	 * returns the snap angle in relation to the current angle in 45 degree
	 * steps
	 * 
	 * @return int the 45�-angle
	 */
	public int getSnapAngle() {
		return (Math.round(getAngle() / 45f) * 45) % 360;
	}

	/**
	 * Checks and returns if the table can be dragged to the new position.
	 * @param positionDiff The position relative to the current location
	 * @return True, if the table can be dragged, else false
	 */
	public boolean getCanDragToPosition(Point positionDiff) {
		int bgHeight = getDanceFloor().getImageHandler().getBackgroundHeight();
		int bgWidth  = getDanceFloor().getImageHandler().getBackgroundWidth();
		
		return (
			((this.getPosition().getX() + positionDiff.getX() + this.getWidth()) <= bgWidth) && 
			((this.getPosition().getY() + positionDiff.getY() + this.getHeight()) <= bgHeight) &&
			((this.getPosition().getX() + positionDiff.getX()) >= 0) &&
			((this.getPosition().getY() + positionDiff.getY()) >= 0)
		);
	}
	
	public boolean getCanDragToPositionX(double pointX) {
		int bgWidth  = getDanceFloor().getImageHandler().getBackgroundWidth();
		
		return (
			((this.getPosition().getX() + pointX + this.getWidth()) <= bgWidth) && 
			((this.getPosition().getX() + pointX) >= 0)
		);
	}
	
	public boolean getCanDragToPositionY(double pointY) {
		int bgHeight = getDanceFloor().getImageHandler().getBackgroundHeight();
		
		return (
			((this.getPosition().getY() + pointY + this.getHeight()) <= bgHeight) &&
			((this.getPosition().getY() + pointY) >= 0)
		);
	}
	
	/**
	 * sets the position of the dancetable according to the seat position the
	 * center of the table is equal to the center of the seat
	 */
	public void moveToSeatPosition() {
		setPosition(seat.getCenterPosX() - getWidth() / 2, seat.getCenterPosY() - getHeight() / 2, false);
	}

	/**
	 * 
	 * @return a new Point object with the current seat position
	 */
	public Point getSeatPosition() {
		return new Point(seat.getCenterPosX(), seat.getCenterPosY());
	}

	/**
	 * Setzt den Sitzplatz entsprechend dem DanceTable dabei wird das Zentrum
	 * vom Seat auf das Zentrum des DanceTable gesetzt
	 * 
	 * @author loom
	 * @since 2007-02-09
	 */
	public void updateSeatPosition() {
		// topleft + width/2 (=center)
		seat.setCenterPosX(getX() + getWidth() / 2);
		seat.setCenterPosY(getY() + getHeight() / 2);
	}

	/**
	 * set the position to the drag position and updates the seat object
	 */
	public void moveToDragPosition() {
		setPosition(getX(), getY(), true);
	}

	/**
	 * sets the new position relative to the current position
	 * 
	 * @param int x new center x-position
	 * @param int y new center y-position
	 */
	public void setDragPosition(final int x, final int y) {
		setPosition(getPosition().x + x, getPosition().y + y, false);
	}

	/**
	 * sets the top left position of the object
	 * 
	 * @param int x new top left x-position
	 * @param int y new top left y-position
	 * @param boolean setSeat updates the associated seat object
	 */
	public void setPosition(final int x, final int y, final boolean setSeat) {
		final Point source;
		final Point result;

		source = new Point(x, y);

		if (danceFloor.isGridAlign() && isDragged()) {
			source.x = danceFloor.roundGridPositionX(source.x);
			source.y = danceFloor.roundGridPositionY(source.y);
		}

		// source.x -= getWidth();
		// source.y -= getHeight();

		result = danceFloor.getPositionWithinBackgroundImage(source, this.getSize());
		
		setLocation(result);

		if (setSeat) {
			updateSeatPosition();
		}
		
		this.currentPositionX = result.x;
		this.currentPositionY = result.y;
		
		this.updateToolTipText();
	}

	/**
	 * sets the top left positon to the x/y coordinates
	 * 
	 * @param int x the x position
	 * @param int y the y position
	 */
	public void setPosition(final int x, final int y) {
		setPosition(x, y, true);
		this.updateToolTipText();
	}

	/**
	 * 
	 * @return Point the position
	 */
	public Point getPosition() {
		return getLocation();
	}
	
	private void adjustBoundingRect() {
		Image i = this.createImage(false);
		
		// Get the image size and add an offset so it won't get
		// cropped when turned at some angles
		int imageWidth = i.getWidth(null);
		int imageHeight = i.getHeight(null);
		
		// Calc angles
		double angleRad = Math.toRadians(getAngle());
		double pitch = Math.toRadians(Math.abs(Math.cos(angleRad + (Math.PI / 2)) * 90));
		
		double alpha = pitch;
		double beta  = (Math.PI / 2) - pitch;
		
		// Calculate bounding height
		double a = Math.sin(beta) * imageHeight;
		double d = Math.sin(alpha) * imageWidth;
		double boundingHeight = a + d;
		
		// Calculate bounding width
		double e = Math.cos(alpha) * imageWidth;
		double boundingWidth = Math.sqrt(Math.pow(imageHeight, 2) - Math.pow(a, 2)) + e;
		
		this.setSize((int)Math.round(boundingWidth) + 2, (int)Math.round(boundingHeight) + 1);
	}
}
