/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancetable;

import static tabledancer.Constants.CONFIG_CURSOR_DEFAULT;
import static tabledancer.Constants.CONFIG_CURSOR_HAND;
import static tabledancer.Constants.CONFIG_CURSOR_MOVE;
import static tabledancer.Constants.CONFIG_TABLE_ROTATION_MULTIPLIER;
import static tabledancer.Constants.K_CTRL;
import static tabledancer.Constants.K_SHIFT;

import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import tabledancer.Util;
import tabledancer.gui.dancefloor.DanceFloor;

/**
 * DanceTableMouseListener handles all mouse events
 * 
 * @author jens
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class DanceTableMouseListener implements MouseListener, MouseMotionListener, MouseWheelListener {
	/**
	 * the dancetable
	 */
	private final DanceTable danceTable;

	/**
	 * Used to filter out the first drag message
	 */
	private boolean isFirstDragMessage = true;
	
	/**
	 * The position that was retrieved when the first
	 * drag event occurred.
	 */
	private Point firstDragPosition;
	
	public DanceTableMouseListener(final DanceTable danceTable) {
		this.danceTable = danceTable;

	}

	/**
	 * shows the popup menu
	 * 
	 * Note: Popup menus are triggered differently on different systems.
	 * Therefore, isPopupTrigger should be checked in both mousePressed and
	 * mouseReleased for proper cross-platform functionality.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	private void showPopupMenu(final MouseEvent event) {
		danceTable.showPopupMenu(event.getX(), event.getY());
	}

	// --------------------------------------------------------------------------
	// SECTION: MouseListener
	// --------------------------------------------------------------------------

	/**
	 * Invoked when the mouse button has been clicked (pressed and released) on
	 * a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseClicked(final MouseEvent event) {
	}

	/**
	 * Invoked when the mouse enters a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseEntered(final MouseEvent event) {
	}

	/**
	 * Invoked when the mouse exits a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseExited(final MouseEvent event) {
	}

	/**
	 * Invoked when a mouse button has been pressed on a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mousePressed(final MouseEvent event) {
		if (event.isPopupTrigger()) {
			showPopupMenu(event);
			return;
		}

		if (Util.hasModifier(event, InputEvent.BUTTON1_MASK)) {
			if (!danceTable.canSelect(event.getPoint())) {
				return;
			}

			final DanceFloor danceFloor;

			danceFloor = danceTable.getDanceFloor();

			danceFloor.setDragging(false);
			danceFloor.setDragCanceled(false);

			if ((event.getModifiers() & InputEvent.SHIFT_MASK) == 0 && danceFloor.getSelectedTables().size() <= 1) {
				danceFloor.deselectedAllTables();
			}

			danceTable.requestFocus();
			danceTable.setSelected(true);
			danceFloor.repaint();
		}
	}

	/**
	 * Invoked when a mouse button has been released on a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseReleased(final MouseEvent event) {
		if (event.isPopupTrigger()) {
			showPopupMenu(event);
			return;
		}

		if (Util.hasModifier(event, InputEvent.BUTTON1_MASK)) {
			final DanceFloor danceFloor;

			danceFloor = danceTable.getDanceFloor();

			if (danceFloor.isDragCanceled()) {
				return;
			}

			if (danceFloor.isDragging()) {
				danceFloor.updateDraggedTables();
				danceFloor.setCursor(CONFIG_CURSOR_HAND);
				this.isFirstDragMessage = true;
			} else if (danceTable.canSelect(event.getPoint())) {
				if (Util.hasModifier(event, K_SHIFT)) {
					danceTable.setSelected(!danceTable.isSelected());
				} else {
					danceTable.setSelected(true);
				}

				danceTable.requestFocus();
				danceFloor.repaint();
			}
		}
	}

	// --------------------------------------------------------------------------
	// SECTION: MouseMotionListener
	// --------------------------------------------------------------------------

	/**
	 * Invoked when the mouse cursor has been moved onto a component but no
	 * buttons have been pushed.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseMoved(final MouseEvent event) {
		if (danceTable.canSelect(event.getPoint())) {
			danceTable.getDanceFloor().setCursor(CONFIG_CURSOR_HAND);
		}
	}

	/**
	 * Invoked when a mouse button is pressed on a component and then dragged.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseDragged(final MouseEvent event) {
		if(this.isFirstDragMessage) {
			this.isFirstDragMessage = false;
			this.firstDragPosition = event.getPoint();
			return;
		}
		
		final DanceFloor danceFloor;

		danceFloor = danceTable.getDanceFloor();

		if (Util.hasModifier(event, InputEvent.BUTTON1_MASK)) {
			if (danceFloor.getX() < 0 || danceFloor.getY() < 0) {
				return;
			}

			if (danceFloor.isDragCanceled()) {
				return;
			}
			
			danceFloor.setDragging(true);
			danceFloor.setCursor(CONFIG_CURSOR_MOVE);
			
			// The first drag message contained the point relative to the
			// table, where the user clicked.
			// This offset has to be added to the new table postion each time the mouse
			// is moved or the dragged tables will "jump" to have their 0,0 edge aligned
			// to the pointer tip.
			Point newPosition = event.getPoint();
			
			newPosition.setLocation(
				newPosition.getX() - this.firstDragPosition.getX(), 
				newPosition.getY() - this.firstDragPosition.getY()
			);
			
			danceFloor.dragSelectedTables(newPosition);
		} else {
			danceFloor.setCursor(CONFIG_CURSOR_DEFAULT);
		}
	}

	// --------------------------------------------------------------------------
	// SECTION: MouseWheelListener
	// --------------------------------------------------------------------------

	/**
	 * Invoked when the mouse wheel is rotated.
	 * 
	 * @param MouseWheelEvent
	 *            event
	 */
	public void mouseWheelMoved(final MouseWheelEvent event) {
		int additionAngle;

		additionAngle = event.getWheelRotation();

		if (Util.hasModifier(event, K_CTRL)) {
			additionAngle *= CONFIG_TABLE_ROTATION_MULTIPLIER;
		}

		danceTable.setAngle(danceTable.getAngle() + additionAngle);
		danceTable.repaint();
	}
}
