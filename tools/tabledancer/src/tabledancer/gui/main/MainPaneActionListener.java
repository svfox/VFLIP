/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.main;

import static tabledancer.Constants.COMMAND_ABOUT;
import static tabledancer.Constants.COMMAND_BLOCK_DELETE;
import static tabledancer.Constants.COMMAND_BLOCK_EDIT;
import static tabledancer.Constants.COMMAND_BLOCK_NEW;
import static tabledancer.Constants.COMMAND_COPY;
import static tabledancer.Constants.COMMAND_CUT;
import static tabledancer.Constants.COMMAND_DELETE;
import static tabledancer.Constants.COMMAND_PASTE;
import static tabledancer.Constants.COMMAND_REDO;
import static tabledancer.Constants.COMMAND_RELOAD;
import static tabledancer.Constants.COMMAND_SAVE;
import static tabledancer.Constants.COMMAND_SEAT_NEW;
import static tabledancer.Constants.COMMAND_SELECT_ALL;
import static tabledancer.Constants.COMMAND_SETTINGS;
import static tabledancer.Constants.COMMAND_UNDO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import tabledancer.TableDancer;

/**
 * action listener for the mainpane
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class MainPaneActionListener implements ActionListener {
	/**
	 * the TableDancer instance
	 */
	private final TableDancer tableDancer;

	/**
	 * constructor
	 * 
	 * @param TableDancer
	 *            tableDancer the instance
	 */
	public MainPaneActionListener(final TableDancer tableDancer) {
		this.tableDancer = tableDancer;
	}

	/**
	 * Invoked when an action occurs.
	 * 
	 * @param ActionEvent
	 *            event the event
	 */
	public void actionPerformed(final ActionEvent event) {
		final String command;

		command = event.getActionCommand();

		if (COMMAND_RELOAD.equals(command)) {
			tableDancer.load(true);
		}

		if (COMMAND_SAVE.equals(command)) {
			tableDancer.save();
		}

		if (COMMAND_UNDO.equals(command)) {
			/*
			 * TODO undo action: new block, remove block, new dancetable(s),
			 * move dancetable(s), clone dancetable(s), delete dancetable(s)
			 */
		}

		if (COMMAND_REDO.equals(command)) {
			/*
			 * TODO redo action: new block, remove block, new dancetable(s),
			 * move dancetable(s), clone dancetable(s), delete dancetable(s)
			 */
		}

		if (COMMAND_CUT.equals(command)) {
			// TODO cut selected dancetables
		}

		if (COMMAND_COPY.equals(command)) {
			// TODO copy selected dancetables
		}

		if (COMMAND_PASTE.equals(command)) {
			// TODO paste selected dancetables
		}

		if (COMMAND_DELETE.equals(command)) {
			// TODO delete selected dancetables
		}

		if (COMMAND_SELECT_ALL.equals(command)) {
			tableDancer.getDanceFloorManager().getCurrent().selectAllTables();
		}

		if (COMMAND_BLOCK_NEW.equals(command)) {
			tableDancer.getDanceFloorManager().create();
		}

		if (COMMAND_BLOCK_EDIT.equals(command)) {
			tableDancer.getDanceFloorManager().getCurrent().showPropertyWindow();
		}

		if (COMMAND_BLOCK_DELETE.equals(command)) {
			tableDancer.getDanceFloorManager().removeCurrent();
		}

		if (COMMAND_SEAT_NEW.equals(command)) {
			tableDancer.getDanceFloorManager().getCurrent().createTable();
		}

		if (COMMAND_SETTINGS.equals(command)) {
			tableDancer.showPreferencesWindow();
		}

		if (COMMAND_ABOUT.equals(command)) {
			tableDancer.showAboutWindow();
		}
	}
}
