/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.main;

import static tabledancer.Constants.COMMAND_ABOUT;
import static tabledancer.Constants.COMMAND_BLOCK_DELETE;
import static tabledancer.Constants.COMMAND_BLOCK_EDIT;
import static tabledancer.Constants.COMMAND_BLOCK_NEW;
import static tabledancer.Constants.COMMAND_COPY;
import static tabledancer.Constants.COMMAND_CUT;
import static tabledancer.Constants.COMMAND_DELETE;
import static tabledancer.Constants.COMMAND_PASTE;
import static tabledancer.Constants.COMMAND_REDO;
import static tabledancer.Constants.COMMAND_RELOAD;
import static tabledancer.Constants.COMMAND_SAVE;
import static tabledancer.Constants.COMMAND_SELECT_ALL;
import static tabledancer.Constants.COMMAND_SETTINGS;
import static tabledancer.Constants.COMMAND_UNDO;
import static tabledancer.Constants.TEXT_COPYRIGHT;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;

import tabledancer.Constants;
import tabledancer.TableDancer;

/**
 * the MainPane contains all GUI elements
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class MainPane extends JScrollPane {
	/**
	 * the serial id
	 */
	private static final long serialVersionUID = 3032610778886086636L;

	/**
	 * the TableDancer instance
	 */
	private final TableDancer tableDancer;

	/**
	 * the container for all gui elements
	 */
	private final JPanel mainPanel;

	/**
	 * the tab pane for all dancefloors
	 */
	private final JTabbedPane workplace;

	/**
	 * constructor
	 */
	public MainPane(final TableDancer tableDancer) {
		this.tableDancer = tableDancer;
		this.tableDancer.setJMenuBar(MainPane.newMenuBar(tableDancer));

		this.workplace = new JTabbedPane();

		this.mainPanel = new JPanel();
		this.mainPanel.setLayout(new BorderLayout());
		this.mainPanel.add(MainPane.createHeadPanel(), BorderLayout.NORTH);
		this.mainPanel.add(getWorkplace(), BorderLayout.CENTER);
		this.mainPanel.add(MainPane.createFootPanel(), BorderLayout.SOUTH);

		this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.getViewport().add(mainPanel);
	}

	// --------------------------------------------------------------------------
	// SECTION: Labels & Buttons
	// --------------------------------------------------------------------------

	/**
	 * creates a label object with the given parameters
	 * 
	 * @param String
	 *            text the label text
	 * @param int x the x position
	 * @param int y the y position
	 * 
	 * @return JLabel the JLabel object
	 */
	private static JLabel newLabel(final String text, final int x, final int y) {
		final JLabel label;

		label = new JLabel();

		label.setText(text);
		label.setLocation(x, y);

		return label;
	}

	/**
	 * creates the application title label object
	 * 
	 * @return JLabel the JLabel object
	 */
	private static JLabel createTitleLabel() {
		String text = Constants.TITLE_APP + " Version " + Constants.TABLEDANCER_VERSION_STRING;

		return newLabel(text, 10, 10);
	}

	/**
	 * creates the copyright label object
	 * 
	 * @return JLabel the JLabel object
	 */
	private static JLabel createCopyrightLabel() {
		return newLabel(TEXT_COPYRIGHT, 10, 10);
	}

	/**
	 * creates a button object with the given parameters
	 * 
	 * @param String
	 *            text the label text
	 * @param String
	 *            actionCommand the ActionCommand text
	 * 
	 * @return JButton the JButton object
	 */
	/*
	 * private JButton newButton ( final String text, final String actionCommand
	 * ) { final JButton button;
	 * 
	 * button = new JButton();
	 * 
	 * button.setText(text); button.addActionListener(mainPaneActionListener);
	 * button.setActionCommand(actionCommand);
	 * 
	 * return button; }
	 */

	/**
	 * creates the save button object
	 * 
	 * @return JButton the JButton object
	 */
	/*
	 * private JButton createSaveButton() { return
	 * newButton(Constants.BUTTON_SAVE, Constants.COMMAND_SAVE); }
	 */

	// --------------------------------------------------------------------------
	// SECTION: main areas
	// --------------------------------------------------------------------------

	/**
	 * creates the menu
	 * 
	 * @param TableDancer
	 *            the TableDancer instance to use
	 * 
	 * @return JMenuBar the menu
	 */
	private static JMenuBar newMenuBar(final TableDancer tableDancer) {
		final JMenuBar menuBar;
		final ActionListener actionListener;
		final int acceleratorModifier;

		final JMenu menuFile;
		final JMenuItem menuItemReload;
		final JMenuItem menuItemSave;

		final JMenu menuEdit;
		final JMenuItem menuItemUndo;
		final JMenuItem menuItemRedo;
		final JMenuItem menuItemCut;
		final JMenuItem menuItemCopy;
		final JMenuItem menuItemPaste;
		final JMenuItem menuItemSelectAll;
		final JMenuItem menuItemDelete;

		final JMenu menuTableDancer;
		final JMenu menuTableDancerBlock;
		final JMenuItem menuItemBlockNew;
		final JMenuItem menuItemBlockEdit;
		final JMenuItem menuItemBlockRemove;
		final JMenuItem menuItemSettings;

		final JMenu menuHelp;
		final JMenuItem menuItemAbout;

		actionListener = new MainPaneActionListener(tableDancer);
		acceleratorModifier = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

		// file menu
		menuItemReload = new JMenuItem("Reload");
		menuItemReload.setActionCommand(COMMAND_RELOAD);
		menuItemReload.addActionListener(actionListener);

		menuItemSave = new JMenuItem("Save");
		menuItemSave.setActionCommand(COMMAND_SAVE);
		menuItemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, acceleratorModifier));
		menuItemSave.addActionListener(actionListener);

		// edit menu
		menuItemUndo = new JMenuItem("Undo");
		menuItemUndo.setActionCommand(COMMAND_UNDO);
		menuItemUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, acceleratorModifier));
		menuItemUndo.addActionListener(actionListener);

		menuItemRedo = new JMenuItem("Redo");
		menuItemRedo.setActionCommand(COMMAND_REDO);
		menuItemRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, acceleratorModifier));
		menuItemRedo.addActionListener(actionListener);

		menuItemCut = new JMenuItem("Cut");
		menuItemCut.setActionCommand(COMMAND_CUT);
		menuItemCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, acceleratorModifier));
		menuItemCut.addActionListener(actionListener);

		menuItemCopy = new JMenuItem("Copy");
		menuItemCopy.setActionCommand(COMMAND_COPY);
		menuItemCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, acceleratorModifier));
		menuItemCopy.addActionListener(actionListener);

		menuItemPaste = new JMenuItem("Paste");
		menuItemPaste.setActionCommand(COMMAND_PASTE);
		menuItemPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, acceleratorModifier));
		menuItemPaste.addActionListener(actionListener);

		menuItemSelectAll = new JMenuItem("Select All");
		menuItemSelectAll.setActionCommand(COMMAND_SELECT_ALL);
		menuItemSelectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, acceleratorModifier));
		menuItemSelectAll.addActionListener(actionListener);

		menuItemDelete = new JMenuItem("Delete");
		menuItemDelete.setActionCommand(COMMAND_DELETE);
		menuItemDelete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, acceleratorModifier));
		menuItemDelete.addActionListener(actionListener);

		// tabledancer menu
		menuItemBlockNew = new JMenuItem("New Block");
		menuItemBlockNew.setActionCommand(COMMAND_BLOCK_NEW);
		menuItemBlockNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, acceleratorModifier));
		menuItemBlockNew.addActionListener(actionListener);

		menuItemBlockEdit = new JMenuItem("Edit Block");
		menuItemBlockEdit.setActionCommand(COMMAND_BLOCK_EDIT);
		menuItemBlockEdit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, acceleratorModifier));
		menuItemBlockEdit.addActionListener(actionListener);

		menuItemBlockRemove = new JMenuItem("Remove Block");
		menuItemBlockRemove.setActionCommand(COMMAND_BLOCK_DELETE);
		menuItemBlockRemove.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, acceleratorModifier));
		menuItemBlockRemove.addActionListener(actionListener);

		menuItemSettings = new JMenuItem("Settings");
		menuItemSettings.setActionCommand(COMMAND_SETTINGS);
		menuItemSettings.addActionListener(actionListener);

		// help menu
		menuItemAbout = new JMenuItem("About");
		menuItemAbout.setActionCommand(COMMAND_ABOUT);
		menuItemAbout.addActionListener(actionListener);

		menuFile = new JMenu("File");
		menuFile.add(menuItemReload);
		menuFile.add(menuItemSave);

		menuEdit = new JMenu("Edit");
		menuEdit.add(menuItemUndo);
		menuEdit.add(menuItemRedo);
		menuEdit.addSeparator();
		menuEdit.add(menuItemCut);
		menuEdit.add(menuItemCopy);
		menuEdit.add(menuItemPaste);
		menuEdit.addSeparator();
		menuEdit.add(menuItemSelectAll);
		menuEdit.add(menuItemDelete);

		menuTableDancerBlock = new JMenu("Block");
		menuTableDancerBlock.add(menuItemBlockNew);
		menuTableDancerBlock.add(menuItemBlockEdit);
		menuTableDancerBlock.add(menuItemBlockRemove);

		menuTableDancer = new JMenu("Tabledancer");
		menuTableDancer.add(menuTableDancerBlock);
		menuTableDancer.addSeparator();
		menuTableDancer.add(menuItemSettings);

		menuHelp = new JMenu("Help");
		menuHelp.add(menuItemAbout);

		// create menubar
		menuBar = new JMenuBar();
		menuBar.add(menuFile);
		menuBar.add(menuEdit);
		menuBar.add(menuTableDancer);
		menuBar.add(menuHelp);

		return menuBar;
	}

	/**
	 * creates the head panel
	 * 
	 * @return JComponent the head panel
	 */
	private static JComponent createHeadPanel() {
		final JPanel panel;

		panel = new JPanel();

		panel.add(createTitleLabel());
		// panel.add(addSaveButton());

		return panel;
	}

	/**
	 * creates the foot panel
	 * 
	 * @return JComponent the foot panel
	 */
	private static JComponent createFootPanel() {
		final JPanel panel;

		panel = new JPanel();

		panel.add(createCopyrightLabel());

		return panel;
	}

	/**
	 * returns the current workplace inntance
	 * 
	 * @return JTabbedPane the instance
	 */
	public JTabbedPane getWorkplace() {
		return workplace;
	}
}
