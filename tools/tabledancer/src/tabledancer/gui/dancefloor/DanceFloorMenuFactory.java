/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * the dancefloor popup menu
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class DanceFloorMenuFactory {
	/**
	 * the singleton instance
	 */
	private static DanceFloorMenuFactory instance = new DanceFloorMenuFactory();

	/**
	 * the item list
	 */
	private final MenuItem items[] = { new MenuItem("New Seat", false, false),
			new MenuItem("Display grid", true, true), new MenuItem("Align to grid", true, false),
			new MenuItem("Background image", true, true), new MenuItem("Select all", false, false) };

	/**
	 * helper class
	 * 
	 */
	class MenuItem {
		private String name;
		private boolean isCheckbox;
		private boolean isDefault;

		/**
		 * instantiates a new menuitem object
		 * 
		 * @param String
		 *            name the name
		 * @param boolean isCheckbox if the item is a checkbox
		 * @param boolean isDefault if the checkbox is enabled by default
		 */
		public MenuItem(final String name, final boolean isCheckbox, final boolean isDefault) {
			this.name = name;
			this.isCheckbox = isCheckbox;
			this.isDefault = isDefault;
		}

		/**
		 * 
		 * @return String the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * 
		 * @return boolean if it the item is checked by default, otherwise false
		 */
		public boolean isDefault() {
			return isDefault;
		}

		/**
		 * creates a new JMenuItem from the current object values
		 * 
		 * @return JMenuItem the new JMenuItem object
		 */
		public JMenuItem getItem() {
			final JMenuItem menuItem;

			if (isCheckbox) {
				menuItem = new JCheckBoxMenuItem();
			} else {
				menuItem = new JMenuItem();
			}

			// TODO is it neccessary to call setAction()?
			menuItem.setAction(null);
			menuItem.setText(getName());
			menuItem.setSelected(isDefault());

			return menuItem;
		}
	}

	/**
	 * returns the factory instance
	 * 
	 * @return DanceFloorMenuFactory the factory instance
	 */
	public static DanceFloorMenuFactory getInstance() {
		return instance;
	}

	/**
	 * factory method for a popup menu
	 * 
	 * @param DanceFloor
	 *            danceFloor the dancefloor the menu should control
	 * 
	 * @return JPopupMenu the new JPopupMenu instance
	 */
	public JPopupMenu newMenu(final DanceFloor danceFloor) {
		final JPopupMenu popupMenu;
		final DanceFloorMenuActionListener actionListener;

		popupMenu = new JPopupMenu();
		actionListener = new DanceFloorMenuActionListener(danceFloor);

		for (int i = 0; i < items.length; i++) {
			final JMenuItem item;

			item = items[i].getItem();

			item.setName(Integer.toString(i + 1));
			item.addActionListener(actionListener);

			popupMenu.add(item);
		}

		return popupMenu;
	}
}
