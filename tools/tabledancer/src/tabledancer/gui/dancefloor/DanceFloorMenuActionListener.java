/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;

/**
 * action listener for the popup menu
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.0
 */
public class DanceFloorMenuActionListener implements ActionListener {
	/**
	 * the DanceFloor instance
	 */
	private DanceFloor danceFloor;

	/**
	 * constructor
	 * 
	 * @param DanceFloor
	 *            danceFloor the instance
	 */
	public DanceFloorMenuActionListener(final DanceFloor danceFloor) {
		this.danceFloor = danceFloor;
	}

	/**
	 * Invoked when an action occurs.
	 * 
	 * @param ActionEvent
	 *            event the event
	 */
	public void actionPerformed(final ActionEvent event) {
		final JComponent menuEntry;
		final int menuNumber;

		menuEntry = (JComponent) event.getSource();
		menuNumber = Integer.parseInt(menuEntry.getName());

		switch (menuNumber) {
		case 1: {
			danceFloor.createTable();
			break;
		}

		case 2: {
			danceFloor.setGridVisible(((JCheckBoxMenuItem) menuEntry).getState());
			break;
		}

		case 3: {
			danceFloor.setGridAlign(((JCheckBoxMenuItem) menuEntry).getState());
			break;
		}

		case 4: {
			danceFloor.setBackgroundVisible(((JCheckBoxMenuItem) menuEntry).getState());
			break;
		}

		case 5: {
			danceFloor.selectAllTables();
			break;
		}
		}
	}

}
