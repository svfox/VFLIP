/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import static tabledancer.Constants.CONFIG_CURSOR_DEFAULT;
import static tabledancer.Constants.CONFIG_TABLE_ROTATION_MULTIPLIER;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * DanceFloorMouseListener handles all mouse events
 * 
 * state diagram for selection:
 * 
 * <pre>
 * pressed
 *   - dancefloor focus
 *   - dancefloor selecting false
 *   - current position = mausposition
 *   - selection start = mausposition
 *   - selection end = mausposition
 * 
 * moved
 *   - current position = mausposition
 * 
 * dragged
 *   - current position = mausposition
 *   - selection end = mausposition
 *   - dancefloor selecting true
 *   - dancefloor selection = selected area
 *   - dancefloor select tables
 *   - dancefloor repaint
 * 
 * released
 *   - drag end = mausposition
 *   - dancefloor selection selected area
 *   - dancefloor select tables
 *   - dancefloor selecting = false
 *   - dancefloor repaint
 * </pre>
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class DanceFloorMouseListener implements MouseListener, MouseMotionListener, MouseWheelListener {
	/**
	 * the dancefloor
	 */
	private final DanceFloor danceFloor;

	/**
	 * the current mouse position
	 */
	private final Point currentPosition;

	/**
	 * the starting point of the selection
	 */
	private final Point selectionStart;

	/**
	 * the ending point of the selection
	 */
	private final Point selectionEnd;

	/**
	 * constructor
	 * 
	 * @param DanceFloor
	 *            danceFloor the dancefloor instance
	 */
	public DanceFloorMouseListener(final DanceFloor danceFloor) {
		this.danceFloor = danceFloor;

		this.currentPosition = new Point(0, 0);
		this.selectionStart = new Point(0, 0);
		this.selectionEnd = new Point(0, 0);
	}

	/**
	 * shows the popup menu
	 * 
	 * Note: Popup menus are triggered differently on different systems.
	 * Therefore, isPopupTrigger should be checked in both mousePressed and
	 * mouseReleased for proper cross-platform functionality.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	private void showPopupMenu(final MouseEvent event) {
		danceFloor.showPopupMenu(event.getX(), event.getY());
	}

	/**
	 * calculates the currently selected area
	 * 
	 * @return Rectangle a rectangle object with the position and size of the
	 *         selected area
	 */
	private Rectangle computeSelectedArea() {
		final int xStart;
		final int yStart;

		final int xStop;
		final int yStop;

		final int xDiff;
		final int yDiff;

		xStart = Math.min(selectionStart.x, selectionEnd.x);
		yStart = Math.min(selectionStart.y, selectionEnd.y);

		xStop = Math.max(selectionStart.x, selectionEnd.x);
		yStop = Math.max(selectionStart.y, selectionEnd.y);

		xDiff = xStop - xStart;
		yDiff = yStop - yStart;

		return new Rectangle(xStart, yStart, xDiff, yDiff);
	}

	/**
	 * returns the current x positon
	 * 
	 * @return int the x position
	 */
	public int getX() {
		return currentPosition.x;
	}

	/**
	 * returns the current y positon
	 * 
	 * @return int the y position
	 */
	public int getY() {
		return currentPosition.y;
	}

	// --------------------------------------------------------------------------
	// SECTION: MouseListener
	// --------------------------------------------------------------------------

	/**
	 * Invoked when the mouse button has been clicked (pressed and released) on
	 * a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseClicked(final MouseEvent event) {
	}

	/**
	 * Invoked when the mouse enters a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseEntered(final MouseEvent event) {
	}

	/**
	 * Invoked when the mouse exits a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseExited(final MouseEvent event) {
	}

	/**
	 * Invoked when a mouse button has been pressed on a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mousePressed(final MouseEvent event) {
		if (event.isPopupTrigger()) {
			showPopupMenu(event);
		}

		currentPosition.setLocation(event.getPoint());
		selectionStart.setLocation(currentPosition);
		selectionEnd.setLocation(currentPosition);

		danceFloor.setDragging(false);
		danceFloor.setDragCanceled(false);
		danceFloor.setSelecting(false);

		danceFloor.requestFocus();
	}

	/**
	 * Invoked when a mouse button has been released on a component.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseReleased(final MouseEvent event) {
		if (event.isPopupTrigger() && !danceFloor.isSelecting()) {
			showPopupMenu(event);
		}

		selectionEnd.setLocation(event.getPoint());

		danceFloor.setSelection(computeSelectedArea());
		danceFloor.setDragging(false);
		danceFloor.setDragCanceled(false);
		danceFloor.setSelecting(false);

		danceFloor.requestFocus();
		danceFloor.repaint();
	}

	// --------------------------------------------------------------------------
	// SECTION: MouseMotionListener
	// --------------------------------------------------------------------------

	/**
	 * Invoked when the mouse cursor has been moved onto a component but no
	 * buttons have been pushed.
	 * 
	 * track mouse position
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseMoved(final MouseEvent event) {
		currentPosition.setLocation(event.getPoint());

		danceFloor.setCursor(CONFIG_CURSOR_DEFAULT);
	}

	/**
	 * Invoked when a mouse button is pressed on a component and then dragged.
	 * 
	 * @param MouseEvent
	 *            event the event
	 */
	public void mouseDragged(final MouseEvent event) {
		final Point point;

		point = danceFloor.getPositionWithinVisibleBackgroundImage(event.getPoint());

		currentPosition.setLocation(point);
		selectionEnd.setLocation(currentPosition);

		danceFloor.setDragging(false);
		danceFloor.setDragCanceled(false);
		danceFloor.setSelecting(true);
		danceFloor.setSelection(computeSelectedArea());
	}

	// --------------------------------------------------------------------------
	// SECTION: MouseWheelListener
	// --------------------------------------------------------------------------

	/**
	 * Invoked when the mouse wheel is rotated.
	 * 
	 * @param MouseWheelEvent
	 *            event
	 */
	public void mouseWheelMoved(final MouseWheelEvent event) {
		int additionAngle;

		additionAngle = event.getWheelRotation();

		if ((event.getModifiers() & InputEvent.CTRL_MASK) != 0) {
			additionAngle *= CONFIG_TABLE_ROTATION_MULTIPLIER;
		}

		danceFloor.rotateSelectedTables(additionAngle);
	}
}
