/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import static tabledancer.Constants.CONFIG_DEFAULT_RIGHT;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class DanceFloorPropertyFrame extends JFrame // implements ActionListener
{
	// private DanceFloor parent;

	public DanceFloorPropertyFrame(final DanceFloor parent) {
		final Container contentPane;

		// this.parent = parent;

		setSize(400, 120);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		/*
		 * Titel [______] Leserecht [....] Koordinaten auf �bersicht: X: [0] Y:
		 * [0] Skalierung auf �bersicht: [0] Block-Tischbilder: [______]
		 * �bersichts-Tischbilder: [______] Link: [______] Link-Koordinaten:
		 * [______] �18-Block: [x] Beschreibung: [______] Hintergrund: (x) url
		 * [______] (x) datei [______]
		 */

		final JLabel labelCaption;
		final JLabel labelViewRight;
		final JLabel labelTopleft_x;
		final JLabel labelTopleft_y;
		final JLabel labelScale;
		final JLabel labelImagedir_block;
		final JLabel labelImagedir_overview;
		final JLabel labelLink;
		final JLabel labelCords;
		final JLabel labelIs_adult;
		final JLabel labelDescription;

		final JTextField inputCaption;
		final JComboBox inputViewRight;
		final JTextField inputTopleft_x;
		final JTextField inputTopleft_y;
		final JTextField inputScale;
		final JTextField inputImagedir_block;
		final JTextField inputImagedir_overview;
		final JTextField inputLink;
		final JTextField inputCords;
		final JCheckBox inputIs_adult;
		final JTextArea inputDescription;

		labelCaption = new JLabel("Titel");
		inputCaption = new JTextField(30);
		inputCaption.setText(parent.getBlock().getCaption());

		labelViewRight = new JLabel("Leserecht");
		inputViewRight = new JComboBox();
		inputViewRight.setEditable(false);
		inputViewRight.addItem(CONFIG_DEFAULT_RIGHT);

		labelTopleft_x = new JLabel("X");
		inputTopleft_x = new JTextField();
		inputTopleft_x.setText("0");
		inputTopleft_x.setEditable(false);

		labelTopleft_y = new JLabel("Y");
		inputTopleft_y = new JTextField();
		inputTopleft_y.setText("0");
		inputTopleft_y.setEditable(false);

		labelScale = new JLabel("Skalierung");
		inputScale = new JTextField();
		inputScale.setText("0");
		inputScale.setEditable(false);

		labelImagedir_block = new JLabel("Block-Tischbilder");
		inputImagedir_block = new JTextField();
		inputCaption.setText(parent.getBlock().getImageDirectory());

		labelImagedir_overview = new JLabel("Übersichts-Tischbilder");
		inputImagedir_overview = new JTextField();
		inputImagedir_overview.setText("");
		inputImagedir_overview.setEditable(false);

		labelLink = new JLabel("Link");
		inputLink = new JTextField();
		inputLink.setText(parent.getBlock().getLink());

		labelCords = new JLabel("Link-Koordinaten");
		inputCords = new JTextField();
		inputCords.setText("");
		inputCords.setEditable(false);

		labelIs_adult = new JLabel("ü18-Block");
		inputIs_adult = new JCheckBox("", false);
		inputIs_adult.setEnabled(false);

		labelDescription = new JLabel("Beschreibung");
		inputDescription = new JTextArea();
		inputDescription.setText(parent.getBlock().getDescription());

		contentPane = getContentPane();
		contentPane.setLayout(new GridLayout(11, 2));

		contentPane.add(labelCaption);
		contentPane.add(inputCaption);

		contentPane.add(labelViewRight);
		contentPane.add(inputViewRight);

		contentPane.add(labelTopleft_x);
		contentPane.add(inputTopleft_x);

		contentPane.add(labelTopleft_y);
		contentPane.add(inputTopleft_y);

		contentPane.add(labelScale);
		contentPane.add(inputScale);

		contentPane.add(labelImagedir_block);
		contentPane.add(inputImagedir_block);

		contentPane.add(labelImagedir_overview);
		contentPane.add(inputImagedir_overview);

		contentPane.add(labelLink);
		contentPane.add(inputLink);

		contentPane.add(labelCords);
		contentPane.add(inputCords);

		contentPane.add(labelIs_adult);
		contentPane.add(inputIs_adult);

		contentPane.add(labelDescription);
		contentPane.add(inputDescription);
	}
}

/*
 * public class MyFrame { JLabel LabelSize, LabelWeight, LabelResult; JTextField
 * TextSize, TextWeight; JButton OK;
 * 
 * public MyFrame() {
 * 
 * }
 * 
 * public void actionPerformed(ActionEvent ev) { if (TextSize.getText().length()
 * > 0 && TextWeight.getText().length() > 0) { try { int Size =
 * Integer.parseInt(TextSize.getText()); int Weight =
 * Integer.parseInt(TextWeight.getText()); double BMI = Weight / ((Size / 100.0)
 * * (Size / 100.0)); if (BMI < 18.5) {
 * LabelResult.setText("Sie haben m�glicherweise Untergewicht."); } else if (BMI
 * > 30) { LabelResult.setText("Sie haben m�glicherweise starkes �bergewicht.");
 * } else if (BMI > 25) {
 * LabelResult.setText("Sie haben m�glicherweise �bergewicht."); } else {
 * LabelResult.setText("Sie haben Normalgewicht."); } } catch
 * (NumberFormatException ex) { } } else {
 * LabelResult.setText("Geben Sie Gr��e und Gewicht an."); } } }
 */
