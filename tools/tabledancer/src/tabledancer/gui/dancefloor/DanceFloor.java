/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import static tabledancer.Constants.CONFIG_CURSOR_DEFAULT;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_SIZE;
import static tabledancer.Constants.CONFIG_FLOOR_SELECTION_BACKGROUND_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_SELECTION_BORDER_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_SELECTION_BORDER_STROKE;
import static tabledancer.Constants.CONFIG_ID_NEW_SEAT;
import static tabledancer.Constants.EMPTY_STRING;
import static tabledancer.Constants.LABEL_NEW_SEAT;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import tabledancer.Util;
import tabledancer.flip.bean.Block;
import tabledancer.flip.bean.Seat;
import tabledancer.gui.dancetable.DanceTable;

/**
 * this class represents a dancefloor
 * 
 * @author Kai 'Riffer' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class DanceFloor extends JPanel {
	/**
	 * the serial id
	 */
	private static final long serialVersionUID = -6894533268218762371L;

	/**
	 * overlay class for the selection rect
	 * 
	 */
	private class OverlayPanel extends JPanel {
		private static final long serialVersionUID = -6412339304991981250L;

		private final DanceFloor danceFloor;

		/**
		 * constructor
		 * 
		 * @param DanceFloor
		 *            danceFloor the danceFloor
		 */
		public OverlayPanel(final DanceFloor danceFloor) {
			this.danceFloor = danceFloor;

			this.setOpaque(false);
			this.setPreferredSize(danceFloor.getPreferredSize());
			this.setMaximumSize(danceFloor.getPreferredSize());
			this.setMinimumSize(danceFloor.getPreferredSize());
			this.setBounds(0, 0, danceFloor.getImageHandler().getBackgroundWidth(), danceFloor.getImageHandler()
					.getBackgroundHeight());

		}

		/**
		 * paint the component
		 */
		@Override
		public void paintComponent(final Graphics g) {
			super.paintComponent(g);

			if (!danceFloor.isSelecting()) {
				return;
			}

			final Graphics2D g2d;

			g2d = (Graphics2D) g;

			if (CONFIG_FLOOR_SELECTION_BORDER_STROKE != null) {
				g2d.setStroke(CONFIG_FLOOR_SELECTION_BORDER_STROKE);
			}

			g2d.setColor(CONFIG_FLOOR_SELECTION_BORDER_COLOR);
			g2d.drawRect(selectedArea.x, selectedArea.y, selectedArea.width, selectedArea.height);

			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.25f));

			g2d.setColor(CONFIG_FLOOR_SELECTION_BACKGROUND_COLOR);
			g2d.fillRect(selectedArea.x, selectedArea.y, selectedArea.width, selectedArea.height);
		}
	}

	/**
	 * the popup menu
	 */
	private final JPopupMenu danceFloorPopupMenu;

	/**
	 * the DanceFloorImages instance holds all image related code
	 */
	private final DanceFloorImageHandler danceFloorImageHandler;

	/**
	 * the MouseListener instance
	 */
	private final DanceFloorMouseListener danceFloorMouseListener;

	/**
	 * the associated block
	 */
	private final Block block;

	/**
	 * the list of dancetables
	 */
	private final List<DanceTable> tables;

	/**
	 * the selected area while selecting
	 */
	private Rectangle selectedArea;

	/**
	 * if the user selecting tables
	 */
	private boolean isSelecting;

	/**
	 * if the user is dragging tables
	 */
	private boolean isDragging;

	/**
	 * if the user canceled the dragging
	 */
	private boolean isDragCanceled;

	/**
	 * show background
	 */
	private boolean isBackgroundVisible;

	/**
	 * show grid
	 */
	private boolean isGridVisible;

	/**
	 * the grid size
	 */
	private final Dimension gridSize;

	/**
	 * align tables to grid
	 */
	private boolean gridAlign;

	/**
	 * factory method to create a new dancefloor instance
	 * 
	 * @param Block
	 *            block the data used to create the dancefloor
	 * 
	 * @return DanceFloor the new dancefloor instance
	 */
	public static DanceFloor newInstance(final Block block) {
		final DanceFloor danceFloor;

		danceFloor = new DanceFloor(block);

		for (final Seat seat : block.getSeats()) {
			final DanceTable danceTable;

			danceTable = new DanceTable(danceFloor, seat);

			danceFloor.addTable(danceTable);
		}

		return danceFloor;
	}

	/**
	 * Konstruktor
	 * 
	 * @param Block
	 *            block die Block-Bean fuer diesen DanceFloor
	 * @throws Exception
	 */
	public DanceFloor(final Block block) {
		this.setLayout(null);
		this.setBounds(0, 0, getWidth(), getHeight());

		this.block = block;
		this.tables = new ArrayList<DanceTable>();
		this.selectedArea = new Rectangle(0, 0, 0, 0);

		// state
		this.isSelecting = false;
		this.isDragging = false;
		this.isDragCanceled = false;

		// appearance
		this.isBackgroundVisible = true;
		this.isGridVisible = true;
		this.gridSize = CONFIG_FLOOR_GRID_SIZE;

		// behaviour
		this.gridAlign = false;

		this.danceFloorMouseListener = new DanceFloorMouseListener(this);
		this.danceFloorImageHandler = new DanceFloorImageHandler(this);

		this.danceFloorImageHandler.init();
		this.danceFloorPopupMenu = DanceFloorMenuFactory.getInstance().newMenu(this);

		// spezieller JPanel, der ?ber allen anderen liegt, damit die Tables von
		// der Linie �bermalt werden k�nnen und sie nicht darunter herumrutscht.
		// sieht einfach besser aus ;-)
		this.addMouseListener(danceFloorMouseListener);
		this.addMouseMotionListener(danceFloorMouseListener);
		this.addMouseWheelListener(danceFloorMouseListener);
		this.addKeyListener(new DanceFloorKeyListener(this));
		this.add(new OverlayPanel(this));
	}

	/**
	 * paints the component
	 */
	@Override
	public void paintComponent(final Graphics g) {
		super.paintComponent(g);

		final Image backgroundImage;

		backgroundImage = danceFloorImageHandler.getBackgroundImage();

		g.drawImage(backgroundImage, 0, 0, backgroundImage.getWidth(null), backgroundImage.getHeight(null), this);
	}

	/**
	 * shows the popup of this dancefloor
	 * 
	 * @param int x the x position
	 * @param int y the y position
	 */
	public void showPopupMenu(final int x, final int y) {
		danceFloorPopupMenu.show(this, x, y);
	}

	/**
	 * opens the window to edit block propertys
	 */
	public void showPropertyWindow() {
		JOptionPane.showMessageDialog(this, "NYI");
	}

	/**
	 * 
	 * @return the associated image handler
	 */
	public DanceFloorImageHandler getImageHandler() {
		return danceFloorImageHandler;
	}

	/**
	 * 
	 * @return the associated block
	 */
	public Block getBlock() {
		return this.block;
	}

	/**
	 * get all tables assigned to this dancefloor
	 * 
	 * @return
	 */
	public List<DanceTable> getTables() {
		return this.tables;
	}

	// --------------------------------------------------------------------------
	// SECTION: state
	// --------------------------------------------------------------------------

	/**
	 * if the user is doing a selection
	 * 
	 * @param boolean value
	 */
	public void setSelecting(final boolean value) {
		isSelecting = value;
	}

	/**
	 * if the user is doing a selection
	 * 
	 * @return boolean flag
	 */
	public boolean isSelecting() {
		return isSelecting;
	}

	/**
	 * Drueckt jemand mit der Maus auf einen Chair oder laesst los.
	 * 
	 * @param value
	 */
	public void setDragging(final boolean value) {
		isDragging = value;
	}

	/**
	 * ob grade jemand mit der maus auf einen chair drueckt.
	 * 
	 * @return boolean flag
	 */
	public boolean isDragging() {
		return isDragging;
	}

	/**
	 * drueckt jemand waehrend der auswahl die escape-taste
	 * 
	 * @param boolean value flag
	 */
	public void setDragCanceled(final boolean value) {
		isDragCanceled = value;
	}

	/**
	 * ob jemand waehrend der auswahl die escape-taste drueckt
	 * 
	 * @return boolean flag
	 */
	public boolean isDragCanceled() {
		return isDragCanceled;
	}

	// --------------------------------------------------------------------------
	// SECTION: appearance
	// --------------------------------------------------------------------------

	/**
	 * Hintergrundbild anyeigen
	 * 
	 * @return boolean flag
	 */
	public boolean isBackgroundVisible() {
		return isBackgroundVisible;
	}

	/**
	 * Hintergrundbild anzeigen
	 * 
	 * @param boolean value
	 */
	public void setBackgroundVisible(final boolean value) {
		isBackgroundVisible = value;

		danceFloorImageHandler.updateBackgroundImage();
		repaint();
	}

	/**
	 * Raster sichbar
	 * 
	 * @return boolean flag
	 */
	public boolean isGridVisible() {
		return isGridVisible;
	}

	/**
	 * Raster sichtbar
	 * 
	 * @param boolean value
	 */
	public void setGridVisible(final boolean value) {
		isGridVisible = value;

		danceFloorImageHandler.updateBackgroundImage();
		repaint();
	}

	/**
	 * returns the current grid size
	 * 
	 * @return Dimension for one grid box
	 */
	public Dimension getGridSize() {
		return gridSize;
	}

	/**
	 * set the grid size
	 * 
	 * @param int width the width of one grid box
	 * @param int height the height of one grid box
	 */
	public void setGridSize(final int width, final int height) {
		gridSize.setSize(width, height);

		danceFloorImageHandler.updateBackgroundImage();
		repaint();
	}

	/**
	 * the selected area
	 * 
	 * @return Rectangle area
	 */
	public Rectangle getSelection() {
		return selectedArea;
	}

	/**
	 * the selected area
	 * 
	 * @param Rectangle
	 *            area
	 */
	public void setSelection(final Rectangle area) {
		selectedArea.setLocation(area.getLocation());
		selectedArea.setSize(area.getSize());

		if (isSelecting()) {
			selectTablesByRectangle(area);
		} else {
			deselectedAllTables();
		}
	}

	// --------------------------------------------------------------------------
	// SECTION: behaviour
	// --------------------------------------------------------------------------

	/**
	 * Am Raster ausrichten
	 * 
	 * @return boolean flag
	 */
	public boolean isGridAlign() {
		return gridAlign;
	}

	/**
	 * Am Raster ausrichten
	 * 
	 * @param boolean value
	 */
	public void setGridAlign(final boolean value) {
		gridAlign = value;

		updateTableAlign();
		repaint();
	}

	// --------------------------------------------------------------------------
	// SECTION: business logic
	// --------------------------------------------------------------------------

	/**
	 * Rundet die Zahl auf ein ganzes Raster
	 * 
	 * @param int zahl Koordinatenwert, welcher ans Raster angepasst werden soll
	 * @param double raster Rasterabstand
	 * @return gerundeter Wert
	 */
	public int roundGridPositionX(final int position) {
		return (int) (Math.round(position / getGridSize().width) * getGridSize().width);
	}

	/**
	 * Rundet die Zahl auf ein ganzes Raster
	 * 
	 * @param int zahl Koordinatenwert, welcher ans Raster angepasst werden soll
	 * @param double raster Rasterabstand
	 * @return gerundeter Wert
	 */
	public int roundGridPositionY(final int position) {
		return (int) (Math.round(position / getGridSize().height) * getGridSize().height);
	}

	/**
	 * checks if the given point is within the coordinates of the visible
	 * background image and corrects it if necessary.
	 */
	public Point getPositionWithinVisibleBackgroundImage(final Point source) {
		final Point min;
		final Point max;
		final Point result;

		min = new Point();
		max = new Point();

		// -1 because otherwise the border wont be visible
		max.x = Math.min(getImageHandler().getBackgroundWidth(), getWidth()) - 1;
		max.y = Math.min(getImageHandler().getBackgroundHeight(), getHeight()) - 1;

		result = Util.getPositionWithinRange(source, min, max);

		return result;
	}

	/**
	 * checks if the given point is within the coordinates of the visible
	 * background image and corrects it if necessary.
	 */
	public Point getPositionWithinBackgroundImage(final Point source, final Dimension boundsRectDim) {
		final Point min;
		final Point max;
		final Point result;

		min = new Point();
		max = new Point();

		max.x = (int) (getImageHandler().getBackgroundWidth() - boundsRectDim.getWidth());
		max.y = (int) (getImageHandler().getBackgroundHeight() - boundsRectDim.getHeight());

		result = Util.getPositionWithinRange(source, min, max);

		return result;
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension preferred = new Dimension();

		preferred.setSize(this.danceFloorImageHandler.getBackgroundWidth(),
				this.danceFloorImageHandler.getBackgroundHeight());

		return preferred;
	}

	/**
	 * get the current selected tables
	 * 
	 * @return
	 */
	public List<DanceTable> getSelectedTables() {
		final List<DanceTable> result;

		result = new ArrayList<DanceTable>();

		for (final DanceTable table : this.tables) {
			if (!table.isSelected()) {
				continue;
			}

			result.add(table);
		}

		return result;
	}

	/**
	 * creates a new table at the current mouse position and adds it to this
	 * block
	 */
	public void createTable() {
		final Seat newSeat;
		final DanceTable newTable;

		newSeat = new Seat(block);
		newSeat.setId(CONFIG_ID_NEW_SEAT);
		newSeat.setName(LABEL_NEW_SEAT);
		newSeat.setIp(EMPTY_STRING);
		newSeat.setEnabled(true);
		newSeat.setAngle(0);
		newSeat.setCenterPosX(danceFloorMouseListener.getX());
		newSeat.setCenterPosY(danceFloorMouseListener.getY());

		block.addSeat(newSeat);

		newTable = new DanceTable(this, newSeat);

		addTable(newTable);
	}

	/**
	 * add a new table to this block
	 * 
	 * @param danceTable
	 */
	public void addTable(final DanceTable danceTable) {
		tables.add(danceTable);
		this.add(danceTable);

		danceTable.moveToSeatPosition();

		repaint();
	}

	/**
	 * remove a table from this block
	 * 
	 * @param danceTable
	 */
	public void removeTable(DanceTable danceTable) {
		this.remove(danceTable);
		tables.remove(danceTable);
		block.removeSeat(danceTable.getSeat());

		// gc does the rest
		danceTable = null;

		repaint();
	}

	/**
	 * wrapper for selectTablesByValue()
	 * 
	 * @see DanceFloor#selectTablesByValue(boolean)
	 */
	public void selectAllTables() {
		selectTablesByValue(true);
	}

	/**
	 * wrapper for selectTablesByValue()
	 * 
	 * @see DanceFloor#selectTablesByValue(boolean)
	 */
	public void deselectedAllTables() {
		selectTablesByValue(false);
	}

	/**
	 * selects all tables according to the given parameter
	 * 
	 * @param boolean value if true, all tables are selected, otherwise all
	 *        tables are deselected
	 */
	public void selectTablesByValue(final boolean value) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			danceTable.setSelected(value);
		}

		repaint();
	}

	/**
	 * selects all tables that are inside the boundaries of the given rectangle
	 * 
	 * @param Rectangle
	 *            selectedArea the selected area
	 */
	public void selectTablesByRectangle(final Rectangle selectedArea) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			/*
			 * we need to use the seat position here, because the seat could be
			 * positioned outside the range of the background image
			 */
			danceTable.setSelected(selectedArea.contains(danceTable.getSeatPosition()));
		}

		repaint();
	}

	/**
	 * removes all selected tables
	 */
	public void removeSelectedTables() {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			if (!danceTable.isSelected()) {
				continue;
			}

			removeTable(danceTable);
		}

		repaint();
	}

	/**
	 * moves all selected tables relative to the current position permanently
	 * 
	 * @param int xdiff the horizontal movement
	 * @param int ydiff the vertical movement
	 */
	public void moveSelectedTables(final Point location) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			if (!danceTable.isSelected()) {
				continue;
			}

			final int newx;
			final int newy;

			newx = danceTable.getPosition().x + location.x;
			newy = danceTable.getPosition().y + location.y;

			danceTable.setDragged(false);
			danceTable.setPosition(newx, newy);
		}
	}

	/**
	 * moves all selected tables relative to the current position permanently
	 * 
	 * @param int xdiff the horizontal movement
	 * @param int ydiff the vertical movement
	 */
	public void rotateSelectedTables(final int angle) {
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			if (!danceTable.isSelected()) {
				continue;
			}

			danceTable.setAngle(danceTable.getAngle() + angle);
		}
	}

	/**
	 * drags all selected tables relative to the current position
	 * 
	 * @param int xdiff the horizontal movement
	 * @param int ydiff the vertical movement
	 */
	public void dragSelectedTables(final Point location) {
		Point dragLocation = location;
		
		// First step - Check if we can drag to the new position, else do nothing
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable = (DanceTable) obj;
			
			if(danceTable.getCanDragToPositionX(dragLocation.getX()) == false) {
				dragLocation.setLocation(0, dragLocation.getY());
			}
			
			if(danceTable.getCanDragToPositionY(dragLocation.getY()) == false) {
				dragLocation.setLocation(dragLocation.getX(), 0);
			}
		}
		
		// Second step - now that every table can be dragged, drag the tables
		for (int i = 0; i < getComponentCount(); i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;
			
			if (!danceTable.isSelected()) {
				continue;
			}

			danceTable.setDragged(true);
			danceTable.setDragPosition(dragLocation.x, dragLocation.y);
		}
	}

	/**
	 * synchronizes the temporary drag positon with the actual table position
	 * for all dragged tables.
	 */
	public void updateDraggedTables() {
		if (!isDragging()) {
			return;
		}

		setDragging(false);
		setDragCanceled(false);
		
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			// only selected dragged dancetables
			if (!danceTable.isSelected() || !danceTable.isDragged()) {
				continue;
			}

			danceTable.setDragged(false);
			danceTable.moveToDragPosition();
		}
	}

	/**
	 * cancels the current drag action and resets drag state to default. moves
	 * all dragged tables to their original positions.
	 */
	public void cancelDragging() {
		if (!isDragging()) {
			return;
		}

		setDragging(false);
		setDragCanceled(true);

		for (int i = 0; i <= getComponentCount() - 1; i++) {
			final Object obj;

			obj = getComponent(i);

			if (!(obj instanceof DanceTable)) {
				continue;
			}

			final DanceTable danceTable;

			danceTable = (DanceTable) obj;

			// only selected dancetables
			if (!danceTable.isSelected() || !danceTable.isDragged()) {
				continue;
			}

			danceTable.setDragged(false);
			danceTable.moveToSeatPosition();
		}

		setCursor(CONFIG_CURSOR_DEFAULT);
		requestFocus();
		repaint();
	}

	/**
	 * aligns the tables to the grid
	 */
	public void updateTableAlign() {

	}
}
