/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import static tabledancer.Constants.CONFIG_FLOOR_BACKGROUND_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_BACKGROUND_HEIGHT;
import static tabledancer.Constants.CONFIG_FLOOR_BACKGROUND_WIDTH;
import static tabledancer.Constants.CONFIG_FLOOR_DEFAULT_ANGLE;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_COLOR;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_OPACITY;
import static tabledancer.Constants.CONFIG_FLOOR_GRID_STROKE;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;

import tabledancer.DataManager;
import tabledancer.ImageManager;
import tabledancer.TableDancer;
import tabledancer.flip.bean.IconList;

/**
 * DanceFloor image handling (background and icons)
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class DanceFloorImageHandler {
	/**
	 * the dancefloor instance
	 */
	private final DanceFloor danceFloor;

	/**
	 * map with images of enabled tables for this block
	 * 
	 * format: angle => image object
	 */
	private Map<Integer, Image> iconsTableEnabled;

	/**
	 * map with images of disabled tables for this block
	 * 
	 * format: angle => image object
	 */
	private Map<Integer, Image> iconsTableDisabled;

	/**
	 * map with chairs for this block
	 * 
	 * format: angle => image object
	 */
	private Map<Integer, Image> iconsChairs;

	/**
	 * the original background image
	 */
	private Image sourceBackgroundImage;

	/**
	 * the displayed background image (with grid etc) may change
	 */
	private Image currentBackgroundImage;

	/**
	 * ???
	 */
	private int maxTableWidth;

	/**
	 * ???
	 */
	private int maxTableHeight;

	/**
	 * ???
	 */
	private int maxChairWidth;

	/**
	 * ???
	 */
	private int maxChairHeight;

	/**
	 * constructor
	 * 
	 * @param DanceFloor
	 *            danceFloor the dancefloor instance
	 */
	public DanceFloorImageHandler(final DanceFloor danceFloor) {
		this.danceFloor = danceFloor;

		this.maxTableWidth = 0;
		this.maxTableHeight = 0;
		this.maxChairWidth = 0;
		this.maxChairHeight = 0;
	}

	/**
	 * liefert ein bild aus der uebergebenen bilder-map abhaengig vom snapwinkel
	 * 
	 * @param Map
	 *            <Integer,Image> tables die map mit dem winkel als key
	 * @param int snapAngle der gewuenschte snap-winkel
	 * 
	 * @return Image das bild
	 * 
	 * @throws NullPointerException
	 *             wenn der uebergebene winkel oder 0 nicht gefunden wurde
	 */
	private static Image getIconImage(final Map<Integer, Image> iconList, final int snapAngle)
			throws NullPointerException {
		if (iconList.containsKey(snapAngle)) {
			return iconList.get(snapAngle);
		} else if (iconList.containsKey(CONFIG_FLOOR_DEFAULT_ANGLE)) {
			return iconList.get(CONFIG_FLOOR_DEFAULT_ANGLE);
		} else {
			throw new NullPointerException("No tableimage for default angle or " + snapAngle);
		}
	}

	/**
	 * loads all needed images for this dancefloor
	 */
	private void loadImages() {
		final ImageManager imageManager;
		final DataManager dataManager;
		final int blockId;
		final List<IconList> iconList;

		imageManager = TableDancer.getInstance().getImageManager();
		dataManager = TableDancer.getInstance().getDataManager();

		sourceBackgroundImage = imageManager.getBackgroundImageByBlockId(danceFloor.getBlock().getId());

		blockId = danceFloor.getBlock().getId();

		if (blockId == 0) {
			iconList = dataManager.getIconUrlsDefault();
		} else {
			iconList = dataManager.getIconUrlsByBlockId(blockId);
		}

		if (iconList == null) {
			return;
		}

		this.iconsTableEnabled = imageManager.getImagesFromIconList(iconList, ImageManager.ICONS_TABLE_ENABLED);
		this.iconsTableDisabled = imageManager.getImagesFromIconList(iconList, ImageManager.ICONS_TABLE_DISABLED);
		this.iconsChairs = imageManager.getImagesFromIconList(iconList, ImageManager.ICONS_CHAIR);

		for (final Image image : iconsTableEnabled.values()) {
			setMaxTableHeight(image.getHeight(null));
			setMaxTableWidth(image.getWidth(null));
		}

		for (final Image image : iconsTableDisabled.values()) {
			setMaxTableHeight(image.getHeight(null));
			setMaxTableWidth(image.getWidth(null));
		}

		for (final Image image : iconsChairs.values()) {
			setMaxChairHeight(image.getHeight(null));
			setMaxChairWidth(image.getWidth(null));
		}
	}

	/**
	 * loads all images into the internal cache and creates the background
	 * image. this method must be called by the dancefloor instance after all
	 * state information has been initialized.
	 */
	public void init() {
		loadImages();
		updateBackgroundImage();
	}

	/**
	 * re-creates the background image for this dancefloor with current settings
	 * applied
	 */
	public void updateBackgroundImage() {
		final BufferedImage resultImage;
		final Graphics2D resultImage2d;

		final int backgroundImageWidth;
		final int backgroundImageHeight;

		// ----------------------------------------------------------------------
		// SECTION: create image objects
		// ----------------------------------------------------------------------

		if (sourceBackgroundImage == null) {
			backgroundImageWidth = CONFIG_FLOOR_BACKGROUND_WIDTH;
			backgroundImageHeight = CONFIG_FLOOR_BACKGROUND_HEIGHT;
		} else {
			backgroundImageWidth = sourceBackgroundImage.getWidth(null);
			backgroundImageHeight = sourceBackgroundImage.getHeight(null);
		}

		resultImage = new BufferedImage(backgroundImageWidth, backgroundImageHeight, BufferedImage.TYPE_INT_ARGB);

		resultImage2d = resultImage.createGraphics();

		// ----------------------------------------------------------------------
		// SECTION: create background
		// ----------------------------------------------------------------------

		resultImage2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));

		if (danceFloor.isBackgroundVisible() && sourceBackgroundImage != null) {
			// use the backgroundImage
			resultImage2d.drawImage(sourceBackgroundImage, 0, 0, backgroundImageWidth, backgroundImageHeight,
					danceFloor);
		} else {
			// draw an empty white rect
			resultImage2d.setColor(CONFIG_FLOOR_BACKGROUND_COLOR);
			resultImage2d.fillRect(0, 0, backgroundImageWidth, backgroundImageHeight);
		}

		// ----------------------------------------------------------------------
		// SECTION: create grid
		// ----------------------------------------------------------------------

		if (danceFloor.isGridVisible()) {
			resultImage2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, CONFIG_FLOOR_GRID_OPACITY));

			if (CONFIG_FLOOR_GRID_STROKE != null) {
				resultImage2d.setStroke(CONFIG_FLOOR_GRID_STROKE);
			}

			resultImage2d.setColor(CONFIG_FLOOR_GRID_COLOR);

			// horizontal lines
			for (int i = 0; i <= backgroundImageWidth; i += danceFloor.getGridSize().width) {
				resultImage2d.drawLine(i, 0, i, backgroundImageHeight);
			}

			// vertical lines
			for (int i = 0; i <= backgroundImageHeight; i += danceFloor.getGridSize().height) {
				resultImage2d.drawLine(0, i, backgroundImageWidth, i);
			}
		}

		currentBackgroundImage = resultImage;
	}

	/**
	 * returns the current background image
	 * 
	 * @return Image the background image
	 */
	public Image getBackgroundImage() {
		return currentBackgroundImage;
	}

	/**
	 * liefert ein bild eines tisches abhaengig vom winkel und dem enabled-flag
	 * 
	 * @param int SnapAngle der gewuenschte snap-winkel
	 * @param boolean enabled wenn true gruener tisch, sonst grau
	 * 
	 * @return Image das bild des tisches
	 */
	public Image getTableImage(final int snapAngle, final boolean enabled) {
		final Image table;

		if (enabled) {
			table = getIconImage(iconsTableEnabled, snapAngle);
		} else {
			table = getIconImage(iconsTableDisabled, snapAngle);
		}

		return table;
	}

	/**
	 * laedt das bild eines stuhles abhaengig vom snap-winkel
	 * 
	 * @param int snapAngle der gewuenschte snap-winkel
	 * 
	 * @return Image das bild des stuhls
	 */
	public Image getChairImage(final int snapAngle) {
		return getIconImage(iconsChairs, snapAngle);
	}

	/**
	 * liefert die Breite des Hintergrundbildes
	 * 
	 * @return int backgroundWidth
	 */
	public int getBackgroundWidth() {
		return currentBackgroundImage.getWidth(null);
	}

	/**
	 * lierfert die hoehe des Hintergrundbildes
	 * 
	 * @return int backgroundHeigth
	 */
	public int getBackgroundHeight() {
		return currentBackgroundImage.getHeight(null);
	}

	/**
	 * ???
	 * 
	 * @return int
	 */
	public int getMaxTableHeight() {
		return maxTableHeight;
	}

	/**
	 * ???
	 * 
	 * @param int maxSeatHeight
	 */
	private void setMaxTableHeight(final int maxTableHeight) {
		this.maxTableHeight = Math.max(this.maxTableHeight, maxTableHeight);
	}

	/**
	 * ???
	 * 
	 * @return int
	 */
	public int getMaxTableWidth() {
		return maxTableWidth;
	}

	/**
	 * ???
	 * 
	 * @param int maxSeatWidth
	 */
	private void setMaxTableWidth(final int maxTableWidth) {
		this.maxTableWidth = Math.max(this.maxTableWidth, maxTableWidth);
	}

	/**
	 * ???
	 * 
	 * @return int
	 */
	public int getMaxChairWidth() {
		return maxChairWidth;
	}

	/**
	 * ???
	 * 
	 * @param int maxChairWidth
	 */
	private void setMaxChairWidth(final int maxChairWidth) {
		this.maxChairWidth = Math.max(this.maxChairWidth, maxChairWidth);
	}

	/**
	 * ???
	 * 
	 * @return int
	 */
	public int getMaxChairHeight() {
		return maxChairHeight;
	}

	/**
	 * ???
	 * 
	 * @param int maxChairHeight
	 */
	private void setMaxChairHeight(final int maxChairHeight) {
		this.maxChairHeight = Math.max(this.maxChairHeight, maxChairHeight);
	}
}
