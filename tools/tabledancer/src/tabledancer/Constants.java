/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Stroke;
import java.awt.event.InputEvent;

import tabledancer.flip.bean.Right;

/**
 * Constants
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class Constants {
	// --------------------------------------------------------------------------
	// SECTION: application strings
	// --------------------------------------------------------------------------

	public static final String TITLE_APP = "FLIP - TableDancer Layout Tool";
	public static final String TITLE_MSG = "Message";
	public static final String TITLE_ERROR = "Error";
	public static final String TITLE_QUIT = "Quit";

	public static final String LABEL_NEW_BLOCK = "New Block";
	public static final String LABEL_NEW_SEAT = "New Seat";
	public static final String LABEL_CLONE = "Clone of ";

	public static final String BUTTON_SAVE = "Save";
	public static final String BUTTON_CANCEL = "Cancel";

	public static final String MSG_SAVE_SUCCESS = "Seating plan saved successfully.";
	public static final String MSG_SAVE_ERROR = "Error while saving data.";
	public static final String MSG_LOAD_ERROR = "Error while loading data.";
	public static final String MSG_UNCHANGED = "No changes were made.";
	public static final String MSG_CHANGED = "There are unsaved changes. Continue?";

	public static final String TEXT_COPYRIGHT = "(c) by flipdev.org - licenced under GNU GPL v2";

	public static final String ERROR_INCORRECT_CONNECTION_TYPE = "The connection is not an HTTP connection";
	public static final String ERROR_LOADING_IMAGES = "Error while loading image data.";

	/**
	 * The version variables of TableDancer
	 */
	public static final int TABLEDANCER_VERSION_MAJOR = 1;
	public static final int TABLEDANCER_VERSION_MINOR = 1;
	public static final int TABLEDANCER_VERSION_REVISION = 5;

	public static final String TABLEDANCER_VERSION_STRING = TABLEDANCER_VERSION_MAJOR + "." + TABLEDANCER_VERSION_MINOR
			+ "." + TABLEDANCER_VERSION_REVISION;

	// --------------------------------------------------------------------------
	// SECTION: application configuration - encoding
	// --------------------------------------------------------------------------

	public static final String CONFIG_ENCODING_OUTPUT = "UTF-8";

	// --------------------------------------------------------------------------
	// SECTION: application configuration - defaults
	// --------------------------------------------------------------------------

	/**
	 * id to create a new block
	 */
	public static final int CONFIG_ID_NEW_BLOCK = 0;

	/**
	 * id to create a new seat
	 */
	public static final int CONFIG_ID_NEW_SEAT = 0;

	/**
	 * the default viewRight for new blocks
	 */
	public static final Right CONFIG_DEFAULT_RIGHT = new Right(0, "<none>");

	/**
	 * the default user agent if no UA-parameter was found
	 */
	public static final String CONFIG_DEFAULT_USER_AGENT = "Tabledancer";

	// --------------------------------------------------------------------------
	// SECTION: application configuration - urls
	// --------------------------------------------------------------------------

	/**
	 * the url where the xml seat data can be updated
	 */
	public static final String CONFIG_URL_XML_SET_SEATS = "seatsadm.php?frame=setseats";

	/**
	 * the url from where the xml seat data can be retrieved
	 */
	public static final String CONFIG_URL_XML_GET_SEATS = "seatsadm.php?frame=getseats";

	/**
	 * the url from where the default xml icon-data (tables, chairs) can be
	 * retrieved
	 */
	public static final String CONFIG_URL_XML_GET_ICONLIST_DEFAULT = "seatsadm.php?frame=geticonsdefault";

	/**
	 * the url from where the block-specific xml icon-data (tables, chairs) can
	 * be retrieved
	 * 
	 * the first parameter is substituted by the block id
	 */
	public static final String CONFIG_URL_XML_GET_ICONLIST = "seatsadm.php?frame=geticons&blockid=%s";

	/**
	 * the url from where the overview background image can be retrieved
	 */
	public static final String CONFIG_URL_IMG_GET_BGIMAGE = "seatsadm.php?frame=getbgimage";

	/**
	 * the url from where the block-specific background image can be retrieved
	 * 
	 * the first parameter is substituted by the block id
	 */
	public static final String CONFIG_URL_IMG_GET_BLOCKIMAGE = "seatsadm.php?frame=getbgimage&blockid=%s";

	// --------------------------------------------------------------------------
	// SECTION: application configuration - cursors
	// --------------------------------------------------------------------------

	/**
	 * the default mouse cursor
	 */
	public static final Cursor CONFIG_CURSOR_DEFAULT = Cursor.getDefaultCursor();

	/**
	 * the mouse cursor while over a table
	 */
	public static final Cursor CONFIG_CURSOR_HAND = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

	/**
	 * the mouse cursor while moving a table
	 */
	public static final Cursor CONFIG_CURSOR_MOVE = Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);

	// --------------------------------------------------------------------------
	// SECTION: application configuration - dancefloor
	// --------------------------------------------------------------------------

	/**
	 * the default angle for icon images
	 */
	public static final Integer CONFIG_FLOOR_DEFAULT_ANGLE = 0;

	/**
	 * the default dancefloor background color if no background image was found
	 */
	public static final Color CONFIG_FLOOR_BACKGROUND_COLOR = Color.WHITE;

	/**
	 * the default dancefloor background width if no background image was found
	 */
	public static final int CONFIG_FLOOR_BACKGROUND_WIDTH = 800;

	/**
	 * the default dancefloor background height if no background image was found
	 */
	public static final int CONFIG_FLOOR_BACKGROUND_HEIGHT = 600;

	/**
	 * the default dancefloor grid size
	 */
	public static final Dimension CONFIG_FLOOR_GRID_SIZE = new Dimension(15, 15);

	/**
	 * the default dancefloor grid color
	 */
	public static final Color CONFIG_FLOOR_GRID_COLOR = Color.RED;

	/**
	 * the default dancefloor grid stroke
	 */
	public static final Stroke CONFIG_FLOOR_GRID_STROKE = new BasicStroke(1, BasicStroke.CAP_BUTT,
			BasicStroke.JOIN_BEVEL, 1, new float[] { 2, 1 }, 0);

	/**
	 * the default dancefloor grid opacity
	 */
	public static final float CONFIG_FLOOR_GRID_OPACITY = 0.15f;

	/**
	 * the default dancefloor selection rect border color
	 */
	public static final Color CONFIG_FLOOR_SELECTION_BACKGROUND_COLOR = Color.GREEN;

	/**
	 * the default dancefloor selection rect border color
	 */
	public static final Color CONFIG_FLOOR_SELECTION_BORDER_COLOR = Color.RED;

	/**
	 * the default dancefloor selection rect border stroke
	 */
	// public static final Stroke CONFIG_FLOOR_SELECTION_BORDER_STROKE = new
	// BasicStroke(0.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1, new
	// float[] { 2, 1 }, 0);
	public static final Stroke CONFIG_FLOOR_SELECTION_BORDER_STROKE = null;

	// --------------------------------------------------------------------------
	// SECTION: application configuration - dancetable
	// --------------------------------------------------------------------------

	public static final Font CONFIG_TABLE_ANGLE_FONT = new Font("Dialog", Font.PLAIN, 11);

	public static final Color CONFIG_TABLE_ANGLE_COLOR = Color.BLACK;

	/**
	 * the size of a dancetable drag-dot
	 */
	public static final int CONFIG_TABLE_DOT_SIZE = 10;
	/**
	 * the size of a dancetable drag-dot
	 */
	public static final Color CONFIG_TABLE_DOT_COLOR = Color.RED;

	/**
	 * the background color of a selected dancetable
	 */
	public static final Color CONFIG_TABLE_SELECTED_BACKGROUND_COLOR = Color.BLUE;

	/**
	 * the border color of a selected dancetable
	 */
	public static final Color CONFIG_TABLE_SELECTED_BORDER_COLOR = Color.RED;

	/**
	 * the stroke of a selected dancetable
	 */
	public static final Stroke CONFIG_TABLE_SELECTED_BORDER_STROKE = new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
			BasicStroke.JOIN_BEVEL, 1, new float[] { 2, 1 }, 0);

	/**
	 * distance when cloning a table, same value for X and Y
	 */
	public static final int CONFIG_TABLE_CLONE_DISTANCE = 30;

	/**
	 * rotation multiplier when ctrl is pressed while changing the angle of a
	 * table
	 */
	public static final int CONFIG_TABLE_ROTATION_MULTIPLIER = 5;

	// --------------------------------------------------------------------------
	// SECTION: miscellaneous
	// --------------------------------------------------------------------------

	public static final String EMPTY_STRING = "";
	public static final Object[] EMPTY_OBJECT_ARRAY = new Object[] {};

	// --------------------------------------------------------------------------
	// SECTION: internal data - menu commands
	// --------------------------------------------------------------------------

	// file
	public static final String COMMAND_RELOAD = "reload";
	public static final String COMMAND_SAVE = "save";

	// edit
	public static final String COMMAND_UNDO = "undo";
	public static final String COMMAND_REDO = "redo";
	public static final String COMMAND_CUT = "cut";
	public static final String COMMAND_COPY = "copy";
	public static final String COMMAND_PASTE = "paste";
	public static final String COMMAND_SELECT_ALL = "selectall";
	public static final String COMMAND_DELETE = "delete";

	// tabledancer
	public static final String COMMAND_BLOCK_NEW = "block_new";
	public static final String COMMAND_BLOCK_EDIT = "block_edit";
	public static final String COMMAND_BLOCK_DELETE = "block_delete";
	public static final String COMMAND_SEAT_NEW = "seat_new";
	public static final String COMMAND_SETTINGS = "settings";

	// help
	public static final String COMMAND_ABOUT = "about";

	// popup menu for a block

	// popup menu for a table
	public static final String COMMAND_CLONE = "clone";
	public static final String COMMAND_REMOVE = "remove";

	// --------------------------------------------------------------------------
	// SECTION: internal data - key modifiers
	// --------------------------------------------------------------------------

	public static int K_CTRL = InputEvent.CTRL_MASK;
	public static int K_SHIFT = InputEvent.SHIFT_MASK;
	public static int K_CTRL_SHIFT = K_CTRL | K_SHIFT;
}
