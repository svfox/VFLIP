/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import tabledancer.bean.Seat;
import tabledancer.dancefloor.DanceFloor;

/**
 * ein Tische
 */
public class DanceTable extends DanceTablePanel {
	private static final long serialVersionUID = 19685263610197526L;

	private static final int CLONE_DISTANCE = 30; // X = Y

	private static final String CLONE_NAME = "Clone of ";

	private DanceTable theDragInstance = null;

	private Seat theSeat = null;

	// sammelt Mausveränderunen für das Gitter
	private int diffx = 0, diffy = 0;

	public DanceTable(Seat mySeat, DanceFloor myDanceFloor) {
		super();
		theDanceFloor = myDanceFloor;
		theSeat = mySeat;

		theMouseHandler = new DanceTableMouseHandler(this, theDanceFloor);
		addMouseMotionListener(theMouseHandler);
		setOpaque(false);
		// ermitteln, wie breit und hoch das Fenster sein muss um immer die
		// komplette Diagonale des rechteckigen Bildes fassen zu können. Dazu
		// benutzen wir unsere lange verschüttet geglaubte Kentniss der
		// binomischen Formel:
		// [Anmerkung von Loom: das ist wohl eher Pythagoras, denn mit Binomen
		// hat das nicht viel zu tun ;)]
		int maxTableandchairWidth = theDanceFloor.getMaxSeatWidth() + theDanceFloor.getMaxChairWidth();
		int maxTableandchairHeight = theDanceFloor.getMaxSeatHeight() + theDanceFloor.getMaxChairHeight();
		double diagonale = Math.sqrt(Math.pow(maxTableandchairWidth, 2) + Math.pow(maxTableandchairHeight, 2));
		int quatersize = new Double(diagonale).intValue();
		Dimension qsize = new Dimension(quatersize, quatersize);
		setPreferredSize(qsize);
		setSize(qsize);
	}

	/**
	 * obere linke Ecke auf Punkt p setzen
	 * 
	 * @param p
	 */
	public void setTablePosition(Point p) {
		setTablePosition(p.x, p.y);
	}

	/**
	 * Positioniert den DanceTable anhand der Seat-Position Dabei ist das
	 * Zentrum von Seat gleich dem Zentrum von Dancetable
	 */
	public void setTablePositionFromSeat() {
		setTablePosition(theSeat.getCenterPosX() - getWidth() / 2, theSeat.getCenterPosY() - getHeight() / 2, false);
	}

	/**
	 * obere linke Ecke auf x,y setzen
	 * 
	 * @param x
	 * @param y
	 */
	public void setTablePosition(int x, int y) {
		setTablePosition(x, y, true);
	}

	/**
	 * Positioniert den DanceTable mit der linken oberen Ecke an x,y
	 * 
	 * @param x
	 *            X-Koordinate des Elternobjekts
	 * @param y
	 *            Y-Koordinate des Elternobjekts
	 * @param setSeat
	 *            soll die Position des Seats entsprechend dem DanceTable
	 *            gesetzt werden?
	 */
	public void setTablePosition(int x, int y, boolean setSeat) {
		// todo: das sollte hier auch nochmal entzerrt werden.
		int newx = x, newy = y;

		boolean setpos = true; // flag ob die Position gespeichert werden soll
		if (theDanceFloor.isRasterAusrichten() && isDragTable()) {
			// merken wie weit der Mauszeiger/Tisch verschoben wurde, bis er die
			// Weite eines Rasters erreicht hat
			diffx += x - rasterroundX(x);
			diffy += y - rasterroundY(y);
			if (Math.abs(diffx) > theDanceFloor.getRaster().x) {
				newx = rasterroundX(x + diffx);
				diffx = 0;
			}
			if (Math.abs(diffy) > theDanceFloor.getRaster().y) {
				newy = rasterroundY(y + diffy);
				diffy = 0;
			}
			if (diffx != 0 && diffy != 0) {
				setpos = false;
			}
		}

		if (setpos) {
			if (newx < 0) {
				newx = 0;
			} else if (newx > theDanceFloor.getMaxWidth() - getWidth()) {
				newx = theDanceFloor.getMaxWidth() - getWidth();
			}
			if (newy < 0) {
				newy = 0;
			} else if (newy > theDanceFloor.getMaxHeight() - getHeight()) {
				newy = theDanceFloor.getMaxHeight() - getHeight();
			}

			setLocation(newx, newy);

			if (setSeat) {
				setSeatPositionByTable();
			}
		}
	}

	private int rasterroundY(int zahl) {
		return rasterround(zahl, theDanceFloor.getRaster().y);
	}

	private int rasterroundX(int zahl) {
		return rasterround(zahl, theDanceFloor.getRaster().x);
	}

	/**
	 * Rundet die Zahl auf ein ganzes Raster
	 * 
	 * @param zahl
	 *            Koordinatenwert, welcher ans Raster angepasst werden soll
	 * @param achse
	 *            'x' oder 'y' -Rasterabstand benutzen
	 * @return gerundeter Wert
	 */
	private int rasterround(int zahl, double raster) {
		return (int) (Math.round(zahl / raster) * raster);
	}

	public void setTableAngle(int angle) {
		angle = Math.abs(angle) % 360;
		theSeat.setAngel(angle);
		setSeatPositionByTable();
		repaint();
	}

	/**
	 * Setzt den Sitzplatz entsprechend dem DanceTable dabei wird das Zentrum
	 * vom Seat auf das Zentrum des DanceTable gesetzt
	 * 
	 * @author loom
	 * @since 2007-02-09
	 */
	private void setSeatPositionByTable() {
		theSeat.setCenterPosX(getX() + getWidth() / 2);
		theSeat.setCenterPosY(getY() + getHeight() / 2);
	}

	public DanceTable getClone() {
		Seat aSeat = new Seat(theSeat.getBlock());
		aSeat.setAngel(theSeat.getAngle());
		aSeat.setEnabled(theSeat.isEnabled());
		aSeat.setCenterPosX(theSeat.getCenterPosX() + CLONE_DISTANCE);
		aSeat.setCenterPosY(theSeat.getCenterPosY() + CLONE_DISTANCE);
		aSeat.setName(CLONE_NAME + theSeat.getName());
		theSeat.getBlock().addSeat(aSeat);
		return new DanceTable(aSeat, theDanceFloor);
	}

	/**
	 * gibt an wo der Stuhl Positioniert werden muss
	 * 
	 * @author loom
	 * @since 2007-02-09
	 * @return Punkt mit X- und Y-Differenz zur oberen linken Ecke
	 */
	private Point getChairOffset() {
		int angle = (theSeat.getAngle() % 360); // sicher ist sicher
		// Tisch verschieben
		Image sizeimage = theDanceFloor.getTableChair(getSnapDegree());
		int stuhlHoehe = sizeimage.getHeight(null) / 2;
		int x = (int) (Math.sin(Math.toRadians(angle)) * stuhlHoehe);
		int y = (int) (Math.cos(Math.toRadians(angle)) * stuhlHoehe);
		return new Point(x, y);
	}

	/**
	 * liefert das Bild des Tisches
	 * 
	 * @param PrintAngle
	 * @return
	 */
	private BufferedImage getSnapImage(boolean PrintAngle) {
		int snapdegree = getSnapDegree();
		Image Table = theDanceFloor.getTable(snapdegree, theSeat.isEnabled());
		// Größe der Sitzplatzgraphik
		theSeat.setWidth(Table.getWidth(null));
		theSeat.setHeight(Table.getHeight(null));
		Image Chair = theDanceFloor.getTableChair(snapdegree);

		BufferedImage tableandchair = new BufferedImage(Table.getWidth(null), Table.getHeight(null)
				+ Chair.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = tableandchair.createGraphics();
		g.drawImage(Table, 0, 0, this);
		if (PrintAngle) {
			g.setColor(Color.black);
			g.drawString(Integer.toString(theSeat.getAngle()), 1, new Float(Table.getHeight(null) / 1.5).intValue());
		}
		g.drawImage(Chair, Table.getWidth(null) / 2 - Chair.getWidth(null) / 2, Table.getHeight(null), this);

		return tableandchair;
	}

	/**
	 * liefert abhängig vom Eingangswinkel den Snapwinkel für das richtige
	 * Bild...
	 * 
	 * @return 45°-Winkel für diesen Tisch
	 */
	public int getSnapDegree() {
		return (Math.round(theSeat.getAngle() / 45f) * 45) % 360;
	}

	@Override
	public void paintComponent(Graphics g) {
		// todo: aufraeumen
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		BufferedImage tableAndChair;
		tableAndChair = getSnapImage(true);
		// Bild mittig setzen (xdiff,ydiff = Rand)
		int xdiff = (getWidth() - tableAndChair.getWidth(null)) / 2;
		int ydiff = (getHeight() - tableAndChair.getHeight(null)) / 2;
		// Tisch in die Mitte rücken
		Point chairOffset = getChairOffset();
		g2d.translate(xdiff + chairOffset.x, ydiff + chairOffset.y);
		g2d.rotate(Math.toRadians(360 - theSeat.getAngle()), tableAndChair.getWidth(null) / 2,
				tableAndChair.getHeight(null) / 2);
		// DragTable wird 'duenner' dargestellt.
		if (isDragTable()) {
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.5));
		}
		g2d.drawImage(tableAndChair, 0, 0, this);
		Stroke stroke = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1, new float[] { 2, 1 }, 0);
		// selektierter Tisch wird blau eingefaerbt:
		if (isSelected()) {
			g2d.setColor(Color.BLUE);
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.15));
			g2d.fillRect(0, 0, tableAndChair.getWidth(null), tableAndChair.getHeight(null));
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 1.0));
		}

		g2d.setColor(Color.BLACK);
		g2d.setStroke(stroke);
		if (hasFocus()) {
			g2d.drawRect(0, 0, tableAndChair.getWidth(null), tableAndChair.getHeight(null));
		}
		// roter Punkt in der Mitte
		g2d.setColor(Color.red);
		g2d.fillOval(tableAndChair.getWidth(null) / 2 - 3, tableAndChair.getHeight(null) / 2 - 3, 6, 6);
	}

	public void keyTyped(KeyEvent event) {
		if ((new Integer(event.getKeyChar()).intValue() == KeyEvent.VK_ESCAPE) && isDragTable()) {
			theDanceFloor.setDragCanceled(true);
			theDanceFloor.setShiftGedrueckt(false);
			theDanceFloor.cancelSelectedPosition();
			event.consume();
			theDanceFloor.requestFocus();
			theDanceFloor.repaint();
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().length() <= 3) {
			setTableAngle(Integer.parseInt(e.getActionCommand()));
		}
		if (e.getActionCommand().equals(DanceTableMenu.CLONE)) {
			DanceTable clone = getClone();
			clone.setSelected(true);
			theDanceFloor.add(clone);
		}
		if (e.getActionCommand().equals(DanceTableMenu.REMOVE)) {
			System.out.println("removing?");
			setVisible(false);
			theDanceFloor.remove(this);
			return;
		}
		repaint();
	}

	public boolean isInnerBorder(Point p) {
		Image sizeimage = getSnapImage(false);
		Point2D pcenter = new Point(getWidth() / 2, getHeight() / 2);
		boolean result = (pcenter.distance(p) <= sizeimage.getWidth(null) / 2);
		if (result) {
			theDanceFloor.setCursor(new Cursor(Cursor.HAND_CURSOR));
			repaint();
		} else {
			theDanceFloor.setCursor(Cursor.getDefaultCursor());
			repaint();
		}
		return result;
	}

	public DanceTable getDragInstance() {
		// todo: warum 2 Instancen???
		if (theDragInstance == null) {
			theDragInstance = new DanceTable(theSeat, theDanceFloor);
			theDragInstance.setDragTable(true);
			theDanceFloor.add(theDragInstance);
			theDragInstance.setTablePosition(getLocation());
			theDragInstance.setTableAngle(theSeat.getAngle());
			theDragInstance.setVisible(true);
			setVisible(false);
		}
		return theDragInstance;
	}

	public void killDragInstance() {
		if (theDragInstance != null) {
			if (theDragInstance.getParent() != null) {
				theDragInstance.getParent().remove(theDragInstance);
			}
			theDragInstance.setVisible(false);
			theDragInstance = null;
			setVisible(true);
		}
	}

	public Point getTablePosition() {
		if (theDragInstance == null) {
			return getLocation();
		} else {
			return theDragInstance.getLocation();
		}
	}

	public Point getFlipPosition() {
		return (new Point(theSeat.getCenterPosX(), theSeat.getCenterPosY()));
	}

	public Seat getSeat() {
		return theSeat;
	}
}
