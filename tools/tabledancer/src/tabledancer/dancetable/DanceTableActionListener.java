/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Verarbeitung der Menuevents
 */
public class DanceTableActionListener implements ActionListener {
	DanceTable table;

	/**
	 * einfacher Konstruktor
	 * 
	 * @param table
	 */
	public DanceTableActionListener(DanceTable table) {
		this.table = table;
	}

	/**
	 * Verarbeitung der TischMenus
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		List<DanceTable> selectedTables = table.theDanceFloor.getSelectedTables();
		if (e.getActionCommand().equals(DanceTableMenu.CLONE)) {
			/* Markierung klonen */
			if (selectedTables.size() == 0) {
				// keine mehrfacheauswahl, element ist auch triggerelement
				cloneTable(table);
			} else {
				// mehrfachauswahl, tische über isSelected()
				for (int i = 0; i < selectedTables.size(); i++) {
					cloneTable(selectedTables.get(i));
				}
			}
		} else if (e.getActionCommand().equals(DanceTableMenu.REMOVE)) {
			/* Markierung löschen */
			if (selectedTables.size() == 0) {
				// keine mehrfacheauswahl, element ist auch triggerelement
				removeTable(table);
			} else {
				// mehrfachauswahl, tische über isSelected()
				for (int i = 0; i < selectedTables.size(); i++) {
					removeTable(selectedTables.get(i));
				}
			}
		} else if (e.getActionCommand().length() <= 3) {
			/* Markierung drehen */
			if (selectedTables.size() == 0) {
				// keine mehrfacheauswahl, element ist auch triggerelement
				table.setTableAngle(Integer.parseInt((e.getActionCommand())));
			} else {
				// mehrfachauswahl, tische über isSelected()
				for (int i = 0; i < selectedTables.size(); i++) {
					(selectedTables.get(i)).setTableAngle(Integer.parseInt((e.getActionCommand())));
				}
			}
		}
		table.theDanceFloor.repaint();
	}

	private void cloneTable(DanceTable table) {
		DanceTable clone = table.getClone();
		table.setSelected(false);
		clone.setSelected(true);
		clone.setVisible(true);
		clone.theDanceFloor.addTable(clone);
		clone.setTablePositionFromSeat();
		clone.repaint();
	}

	private void removeTable(DanceTable table) {
		table.setSelected(false);
		table.theDanceFloor.removeTable(table);
	}
}
