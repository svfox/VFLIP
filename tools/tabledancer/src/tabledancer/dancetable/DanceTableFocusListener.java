/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * FocusListener fuer die Tische
 */
public class DanceTableFocusListener implements FocusListener {
	private DanceTablePanel table;

	/**
	 * Konstruktor - tut aber nicht viel
	 * 
	 * @param table
	 */
	public DanceTableFocusListener(DanceTablePanel table) {
		this.table = table;
	}

	/**
	 * Focus erhalten
	 * 
	 * @param e
	 */
	public void focusGained(FocusEvent e) {
		table.repaint();
	}

	/**
	 * Verarbeitung, focus verloren
	 * 
	 * @param e
	 */
	public void focusLost(FocusEvent e) {
		if (table.theDanceFloor.isShiftGedrueckt()) {
			table.setSelected(!table.isSelected());
		}
		table.repaint();
	}

}
