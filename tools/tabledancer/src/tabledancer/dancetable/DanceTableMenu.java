/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Das Menu fuer die linke Maustaste
 */
public class DanceTableMenu {
	public static final String CLONE = "clone";

	public static final String REMOVE = "remove";

	private DanceTable table;

	private JPopupMenu dancePopUp;

	private final MenuItem[] MENU = { new MenuItem("Klonen", CLONE), new MenuItem("Loeschen", REMOVE),
			new MenuItem("0", null), new MenuItem("45", null), new MenuItem("90", null), new MenuItem("135", null),
			new MenuItem("180", null), new MenuItem("225", null), new MenuItem("270", null), new MenuItem("315", null) };

	/**
	 * Konstruktor, tut aber nicht viel :)
	 * 
	 * @param table
	 */
	public DanceTableMenu(DanceTable table) {
		this.table = table;
	}

	/**
	 * liefert das Menu fuer die Rechte Maustaste auf Stuehlen, mit Caching
	 * 
	 * @return Menu
	 */
	public JPopupMenu setUp() {
		if (dancePopUp != null) {
			return dancePopUp;
		}

		dancePopUp = new JPopupMenu();
		DanceTableActionListener aListener = new DanceTableActionListener(table);

		for (MenuItem element : MENU) {
			setUpMenuItem(element.getItem(), aListener);
		}

		return dancePopUp;
	}

	private void setUpMenuItem(JMenuItem item, ActionListener listener) {
		item.addActionListener(listener);
		dancePopUp.add(item);
	}

	/**
	 * kleine Hilfsklasse fuers Menu
	 */
	class MenuItem {
		private String name;

		private String wert;

		public MenuItem(String name, String wert) {
			this.name = name;
			this.wert = wert;
		}

		public JMenuItem getItem() {
			JMenuItem item = new JMenuItem();
			item.setText(name);
			if (wert != null) {
				item.setActionCommand(wert);
			}
			return item;
		}

		public String getWert() {
			return wert;
		}

		public String getName() {
			return name;
		}
	}
}
