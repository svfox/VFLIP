/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.AWTEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.JPanel;

import tabledancer.dancefloor.DanceFloor;

/**
 * PanelHandling fuer den Tisch
 */
public abstract class DanceTablePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6759346591215059939L;

	private boolean isSelected = false;

	private boolean isDragTable = false;

	protected boolean isTemporarySelected = false;

	protected DanceFloor theDanceFloor = null;

	protected DanceTableMouseHandler theMouseHandler;

	protected DanceTablePanel() {
		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
		enableEvents(AWTEvent.MOUSE_WHEEL_EVENT_MASK);
		enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
		enableEvents(AWTEvent.COMPONENT_EVENT_MASK);
		enableEvents(AWTEvent.FOCUS_EVENT_MASK);
		// enableEvents(AWTEvent.KEY_EVENT_MASK);
		addKeyListener(new DanceTableKeyListener());

		setFocusable(true);
		addFocusListener(new DanceTableFocusListener(this));
	}

	public void setDragTable(boolean value) {
		isDragTable = value;
		repaint();
	}

	public boolean isSelected() {
		return (isSelected || isTemporarySelected);
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
		if (isDragTable()) {
			isSelected = false;
		}
		repaint();
	}

	public boolean isDragTable() {
		return isDragTable;
	}

	/**
	 * Delegation zum DanceTableMouseHandler todo: aufheben der delegation und
	 * durch Listener ersetzen
	 */
	@Override
	public void processMouseEvent(MouseEvent event) {
		theMouseHandler.processMouseEvent(event);
	}

	@Override
	protected void processMouseMotionEvent(MouseEvent mouseEvent) {
		theMouseHandler.processMouseMotionEvent(mouseEvent);
	}

	@Override
	protected void processMouseWheelEvent(MouseWheelEvent mouseWheelEvent) {
		theMouseHandler.processMouseWheelEvent(mouseWheelEvent);
	}

}
