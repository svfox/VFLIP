/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import static tabledancer.Constants.CONFIG_URL_XML_GET_ICONLIST;
import static tabledancer.Constants.CONFIG_URL_XML_GET_ICONLIST_DEFAULT;
import static tabledancer.Constants.CONFIG_URL_XML_GET_SEATS;
import static tabledancer.Constants.CONFIG_URL_XML_SET_SEATS;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import tabledancer.flip.bean.Block;
import tabledancer.flip.bean.IconList;
import tabledancer.flip.bean.Right;
import tabledancer.flip.xml.XmlParser;
import tabledancer.flip.xml.XmlWriter;

/**
 * this class is responsible for all bean/xml data handling, processing and
 * conversion.
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class DataManager {
	/**
	 * string that indicates a flip error
	 */
	private static final String FLIP_ERROR_MESSAGE = "Fataler Fehler";

	/**
	 * xml mimetype
	 */
	private static final String MIME_XML = "text/xml";

	/**
	 * the TableDancer instance
	 * 
	 * this object is currently not used but here for compatibility reasons
	 */
	@SuppressWarnings("unused")
	private final TableDancer tableDancer;

	/**
	 * the XmlParser instance
	 */
	private final XmlParser xmlParser;

	/**
	 * the list with the backend beans for all rights
	 */
	private final List<Right> rights;

	/**
	 * the list with the backend beans for all blocks
	 */
	private final List<Block> blocks;

	/**
	 * constructor
	 * 
	 * @param TableDancer
	 *            tableDancer the TableDancer instance
	 */
	public DataManager(final TableDancer tableDancer) {
		this.tableDancer = tableDancer;
		this.xmlParser = new XmlParser();
		this.blocks = new ArrayList<Block>();
		this.rights = new ArrayList<Right>();
	}

	/**
	 * opens a connection to the given url and returns an InputStream object for
	 * the connection.
	 * 
	 * this method notifies the user about errors.
	 * 
	 * @param String
	 *            url the url string
	 * 
	 * @return InputStream the InputStream object for the url or null if an
	 *         error occured
	 * 
	 * @see HttpConnectionFactory#getInputStreamFromUrl(String)
	 */
	private InputStream getInputStreamFromUrl(final String url) {
		try {
			final InputStream inputStream;

			inputStream = HttpConnectionFactory.getInputStreamFromUrl(url);

			return inputStream;
		} catch (final MalformedURLException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden der daten ist ein fehler aufgetreten:
			 * ein url ist nicht korrekt + url string bitte korrgiere das in den
			 * settings blablabla
			 */
		} catch (final IOException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden der daten ist ein I/O-fehler
			 * aufgetreten textarea "daten fuer entwickler" mit stacktrace
			 */
		}

		return null;
	}

	/**
	 * returns the current block list
	 * 
	 * @return the block list
	 */
	public List<Block> getBlocks() {
		return blocks;
	}

	/**
	 * returns the current right list
	 * 
	 * @return the block list
	 */
	public List<Right> getRights() {
		return rights;
	}

	/**
	 * returns the right object with the given rightId, if no object was found a
	 * new object with the right id will be created
	 * 
	 * @param int rightId the right id
	 * @param String
	 *            name the name if no right was found
	 * 
	 * @return Right the right
	 */
	public Right requestRight(final int rightId, final String name) {

		for (final Right right : rights) {
			if (right.getId() == rightId) {
				continue;
			}

			return right;
		}

		final Right result;

		result = new Right(rightId, name);

		rights.add(result);

		return result;
	}

	/**
	 * compares the internal blocklist with the current backend data for
	 * changes.
	 * 
	 * @return boolean true if there are changes, false if there are no changes
	 *         or if an error occured
	 * 
	 * @throws Exception
	 *             if a parsing or i/o error occured
	 */
	public boolean hasChanged() throws Exception {
		final String url;
		final InputStream inputStream;

		url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_XML_GET_SEATS);

		inputStream = getInputStreamFromUrl(url);

		// if we cannot get an input stream we assume there is something
		// wrong with the backend and prevent further communication
		// with the backend
		if (inputStream == null) {
			return false;
		}

		try {
			final List<Block> currentBlocks;

			final String currentXML;
			final String newXML;

			final boolean hasChanged;

			// get data from backend
			currentBlocks = xmlParser.getBlocksFromInputStream(inputStream);

			/*
			 * es kann nicht direkt das XML vom FLIP genutzt werden, da nicht
			 * alle Daten vom Tabledancer gesetzt werden (z.B. UserID)
			 */
			newXML = XmlWriter.write(blocks);
			currentXML = XmlWriter.write(currentBlocks);

			hasChanged = !newXML.equals(currentXML);

			return hasChanged;
		} catch (SAXException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim parsen der xml-daten ist ein fehler
			 * aufgetreten
			 */
			throw new Exception(e);
		} catch (IOException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden der daten ist ein I/O-fehler
			 * aufgetreten textarea "daten fuer entwickler" mit stacktrace
			 */
			throw new Exception(e);
		} finally {
			inputStream.close();
		}
	}

	/**
	 * clears the internal block/seat list and fills it with the current data
	 * from the backend
	 * 
	 * @throws Exception
	 *             if an parsing or i/o error occured
	 */
	public void loadDataFromBackend() throws Exception {
		final String url;
		final InputStream inputStream;

		url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_XML_GET_SEATS);

		inputStream = getInputStreamFromUrl(url);

		if (inputStream == null) {
			return;
		}

		try {
			final List<Block> result;

			result = xmlParser.getBlocksFromInputStream(inputStream);

			// the data from flip could be empty (no existing blocks),
			// so we clear the internal list first
			if (result != null) {
				blocks.clear();
				blocks.addAll(result);
			}
		} catch (SAXException e) {
			TableDancer.showException("Parse-Error", "Could not parse the seats XML!", e.getStackTrace());
		} catch (IOException e) {
			TableDancer.showException("IO-Error", "Could not get the seats data!", e.getStackTrace());
		} finally {
			inputStream.close();
		}
	}

	/**
	 * converts the current internal data list to the flip xml format and writes
	 * it to the backend.
	 * 
	 * @throws Exception
	 *             if the configured url is incorrect or an i/o error occured
	 */
	public void saveDataToBackend() throws Exception {
		try {
			final String url;
			final URLConnection urlConnection;

			final OutputStream requestStream;
			final String requestData;

			final InputStream responseStream;
			final String responseData;

			url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_XML_SET_SEATS);

			// open HTTP POST connection for input and output
			urlConnection = HttpConnectionFactory.createURLConnection(url, HttpConnectionFactory.HTTP_METHOD_POST);
			urlConnection.setRequestProperty(HttpConnectionFactory.HTTP_REQUEST_CONTENT_TYPE, MIME_XML);
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.connect();

			requestData = XmlWriter.write(blocks);

			Charset outCharset = Charset.defaultCharset();

			// Encoding
			if (Charset.isSupported(Constants.CONFIG_ENCODING_OUTPUT)) {
				System.out.println("Using UTF-8 encoding...");
				outCharset = Charset.forName(Constants.CONFIG_ENCODING_OUTPUT);
			} else {
				System.out.println("UTF-8 Encoding not supported...");
			}

			byte encoded[] = requestData.getBytes(outCharset);

			requestStream = urlConnection.getOutputStream();
			requestStream.write(encoded);
			requestStream.flush();
			requestStream.close();

			responseStream = urlConnection.getInputStream();
			if (responseStream == null) {
				// we assume that there were no errors if the response is empty
				return;
			}

			// convert content from InputStream to String
			final int byteArraySize;
			final byte byteArray[];
			final ByteArrayOutputStream byteArrayOutputStream;

			// read data in 1024 byte chunks
			byteArraySize = 1024;
			byteArray = new byte[byteArraySize];
			byteArrayOutputStream = new ByteArrayOutputStream();

			while (responseStream.read(byteArray, 0, byteArraySize) > 0) {
				byteArrayOutputStream.write(byteArray, 0, byteArraySize);
			}

			byteArrayOutputStream.flush();
			responseStream.close();

			responseData = byteArrayOutputStream.toString();

			// TODO kann man das fehlerhandling nicht irgendwie verbessern?
			// (abgesehen davon die seite zu parsen)
			if (responseData.contains(FLIP_ERROR_MESSAGE)) {
				/*
				 * TODO fehlerpopup beim speichern der daten ist flip-fehler
				 * aufgetreten
				 */
				throw new Exception("there was an backend error while writing the data");
			}
		} catch (MalformedURLException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden der daten ist ein fehler aufgetreten:
			 * ein url ist nicht korrekt + url anzeige bitte korrgiere das in
			 * den settings blablabla
			 */
			throw new Exception(e);
		} catch (IOException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden der daten ist ein I/O-fehler
			 * aufgetreten textarea "daten fuer entwickler" mit stacktrace
			 */
			throw new Exception(e);
		}
	}

	/**
	 * returns a list of IconList objects with default icon urls
	 * 
	 * @return List<IconList> the IconList list, empty if an error occured
	 */
	public List<IconList> getIconUrlsDefault() {
		final String url;
		final InputStream inputStream;
		final List<IconList> result;

		url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_XML_GET_ICONLIST_DEFAULT);

		inputStream = getInputStreamFromUrl(url);

		result = xmlParser.getIconURLsFromInputStream(inputStream);

		try {
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (IOException e) {
			// nothing
		}

		return result;
	}

	/**
	 * returns a list of IconList objects with block-specific icon urls
	 * 
	 * @param int blockId the block id
	 * 
	 * @return List<IconList> the IconList list, empty if an error occured or no
	 *         data was found
	 */
	public List<IconList> getIconUrlsByBlockId(final int blockId) {
		final String url;
		final InputStream inputStream;
		final List<IconList> result;

		url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_XML_GET_ICONLIST, blockId);

		inputStream = getInputStreamFromUrl(url);

		result = xmlParser.getIconURLsFromInputStream(inputStream);

		try {
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (IOException e) {
			// nothing
		}

		return result;
	}
}
