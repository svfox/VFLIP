package tabledancer;

import java.applet.Applet;
import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import tabledancer.bean.Block;
import tabledancer.bean.Seat;
import tabledancer.xml.XmlParser;
import tabledancer.xml.XmlWriter;

/**
 * Handlerklasse zum holen aller Informationen aus dem FLIP-System
 */
public class FlipRessourcesHandler {
	private final static String PARAM_USERAGENT = "useragent", REGEX = "[^/]+$", LEER = "", XML = "text/xml",
			CONTENT_TYPE = "Content-Type", URL_GETBIGIMAGE = "seatsadm.php?frame=getbgimage&blockid=",
			URL_GETSEATS = "seatsadm.php?frame=getseats", URL_GETICONS = "seatsadm.php?frame=geticons&blockid=",
			URL_SETSEATS = "seatsadm.php?frame=setseats", URL_GET = "GET", URL_POST = "POST", HTTP = "http",
			FAIL_WRONG_TYPE = "this connection is NOT an HttpUrlConnection connection", ERROR_MSG = "Fataler Fehler";

	private Applet applet;

	private String baseUrl;

	// little cache
	private Map<String, Image> theImages;

	public FlipRessourcesHandler(Applet applet) {
		this.applet = applet;
		baseUrl = applet.getDocumentBase().toString().replaceFirst(REGEX, LEER);
		theImages = new HashMap<String, Image>();
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public Image getBackroundImage(int myBlockId) {
		return getURLImage(baseUrl + URL_GETBIGIMAGE + myBlockId);
	}

	public InputStream getXmlSeats() throws Exception {
		return getXmlStream(baseUrl + URL_GETSEATS);
	}

	public InputStream getXmlIconList(int blockId) throws Exception {
		return getXmlStream(baseUrl + URL_GETICONS + blockId);
	}

	public Image getURLImage(String sURL) {
		// get Image from URL
		if (!sURL.toLowerCase().startsWith(HTTP)) {
			sURL = baseUrl + sURL;
		}
		Image aImage = theImages.get(sURL);
		// Check auf Cache-Hit
		if (aImage == null) {
			aImage = getImage(sURL);
			theImages.put(sURL, aImage);
		}
		return aImage;
	}

	private Image getImage(String url) {
		Image image = null;
		try {
			image = ImageIO.read(new BufferedInputStream(getURLConnection(url).getInputStream()));
			if (image == null) {
				new Exception().printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}

	private URLConnection getURLConnection(String url) throws Exception {
		URLConnection urlConnection = prepareURLConnection(url);
		urlConnection.connect();
		return urlConnection;
	}

	private URLConnection prepareURLConnection(String url) throws Exception {
		return prepareURLConnection(url, URL_GET);
	}

	private URLConnection prepareURLConnection(String url, String method) throws Exception {
		URLConnection urlConnection = new URL(url).openConnection();
		if (urlConnection instanceof HttpURLConnection) {
			((HttpURLConnection) urlConnection).setRequestMethod(method);
		} else {
			throw new Exception(FAIL_WRONG_TYPE);
		}

		urlConnection.setUseCaches(false);
		urlConnection.setDefaultUseCaches(false);
		urlConnection.setRequestProperty("User-Agent", applet.getParameter(PARAM_USERAGENT));
		return urlConnection;
	}

	private InputStream getXmlStream(String url) throws Exception {
		URLConnection urlConnection = getURLConnection(url);
		return urlConnection.getInputStream();
	}

	// Obsolet da Funktion in DataManager ausgelagert (!?)
	// public boolean postXmlSeats(String data) throws Exception {
	// String url = baseUrl + URL_SETSEATS;
	// URLConnection urlConnection = prepareURLConnection(url, URL_POST);
	//
	// //Verbindung aufbauen
	// urlConnection.setDoInput(true);
	// urlConnection.setDoOutput(true);
	// urlConnection.setRequestProperty(CONTENT_TYPE, XML);
	//
	// urlConnection.connect();
	//
	// Charset outCharset = Charset.defaultCharset();
	//
	// // Encoding
	// if(Charset.isSupported(Constants.CONFIG_ENCODING_OUTPUT)) {
	// try {
	// outCharset = Charset.forName(Constants.CONFIG_ENCODING_OUTPUT);
	// } catch (Exception e) {
	// System.err.println(e.getLocalizedMessage());
	// }
	// } else {
	// System.out.println("UTF-8 Encoding not supported...");
	// }
	//
	// byte encodedData[] = data.getBytes(outCharset);
	//
	// //Daten senden
	// OutputStream os = urlConnection.getOutputStream();
	// os.write(encodedData);
	// os.flush();
	// os.close();
	//
	// //Antwort lesen
	// ByteArrayOutputStream baos = null;
	// InputStream is;
	// if ((is = urlConnection.getInputStream()) != null) {
	// baos = new ByteArrayOutputStream();
	// byte ba[] = new byte[1];
	// while ((is.read(ba, 0, 1)) != (-1)) {
	// baos.write(ba, 0, 1);
	// }
	// baos.flush();
	// is.close();
	// String response = baos.toString();
	// if(response.contains(ERROR_MSG)) {
	// return false;
	// } else {
	// return true;
	// }
	// }
	// //wenn keine Antwort ausgelesen werden kann, gehen wir mal vom Besten aus
	// return true;
	// }

	/**
	 * gibt an, ob Daten geändert wurden
	 * 
	 * @param blocks
	 * @return
	 */
	public boolean isChanged(List<Block> blocks) {
		try {
			XmlParser parser = new XmlParser();
			List<Block> flipBlocks = new ArrayList<Block>();
			List<Seat> flipSeats = new ArrayList<Seat>();
			parser.fillList(getXmlSeats(), flipBlocks, flipSeats);
			// es kann nicht direkt das XML vom FLIP genutzt werden, da nicht
			// alle Daten vom Tabledancer gesetzt werden (z.B. UserID)
			String flipXML = XmlWriter.write(flipBlocks);
			String thisXML = XmlWriter.write(blocks);
			return !thisXML.equals(flipXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
