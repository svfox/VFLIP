/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import static tabledancer.Constants.EMPTY_STRING;
import static tabledancer.Constants.MSG_CHANGED;
import static tabledancer.Constants.MSG_LOAD_ERROR;
import static tabledancer.Constants.MSG_SAVE_ERROR;
import static tabledancer.Constants.MSG_SAVE_SUCCESS;
import static tabledancer.Constants.MSG_UNCHANGED;
import static tabledancer.Constants.TITLE_APP;
import static tabledancer.Constants.TITLE_ERROR;
import static tabledancer.Constants.TITLE_MSG;

import java.awt.Container;

import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import tabledancer.gui.dialogs.ErrorDialog;
import tabledancer.gui.main.MainPane;

/**
 * The main applet class is the resource provider for all subsystems
 * 
 * @author Kai 'Riffler' Posadowsky
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.1
 * @since 1.0
 */
public class TableDancer extends JApplet {
	/**
	 * applet parameter from flip for the flip user id
	 */
	public static final String PARAM_USERID = "flipid";

	/**
	 * applet parameter from flip for the user agent
	 */
	public static final String PARAM_USERAGENT = "useragent";

	/**
	 * applet parameter from flip for the session cookie
	 */
	public static final String PARAM_COOKIENAME = "cookiename";

	/**
	 * the serial id
	 */
	private static final long serialVersionUID = -5784312311808084399L;

	/**
	 * the applet instance
	 */
	private static TableDancer instance;

	/**
	 * the image manager instance
	 */
	private ImageManager imageManager;

	/**
	 * the data manager instance
	 */
	private DataManager dataManager;

	/**
	 * the dancefloor manager instance
	 */
	private DanceFloorManager danceFloorManager;

	/**
	 * the GUI root of the application
	 */
	private MainPane mainPane;

	/**
	 * the base url of the backend
	 */
	private String baseUrl;

	/**
	 * The one and only handler for data from the FLIP system.
	 */
	private FlipRessourcesHandler theFlipHander = null;

	/**
	 * singleton for the tabledancer instance
	 * 
	 * @return TableDancer the instance of this class
	 * 
	 * @see TableDancer
	 */
	public synchronized static TableDancer getInstance() {
		if (instance == null) {
			instance = new TableDancer();
		}

		return instance;
	}

	// --------------------------------------------------------------------------
	// SECTION: management objects
	// --------------------------------------------------------------------------

	/**
	 * returns the data manager
	 * 
	 * @return DataManager the data manager associated to this instance
	 * 
	 * @see DataManager
	 */
	public DataManager getDataManager() {
		if (dataManager == null) {
			dataManager = new DataManager(this);
		}

		return dataManager;
	}

	/**
	 * returns the image manager
	 * 
	 * @return ImageManager the image manager associated to this instance
	 * 
	 * @see ImageManager
	 */
	public ImageManager getImageManager() {
		if (imageManager == null) {
			imageManager = new ImageManager(this);
		}

		return imageManager;
	}

	/**
	 * returns the dancefloor manager
	 * 
	 * @return DanceFloorManager the dancefloor manager associated to this
	 *         instance
	 * 
	 * @see DanceFloorManager
	 */
	public DanceFloorManager getDanceFloorManager() {
		if (danceFloorManager == null) {
			danceFloorManager = new DanceFloorManager(this);
		}

		return danceFloorManager;
	}

	/**
	 * Returns the resources handler that gets images and other resources from
	 * the FLIP.
	 * 
	 * @return The handler instance
	 */
	public FlipRessourcesHandler getRessourceHandler() {
		if (this.theFlipHander == null) {
			this.theFlipHander = new FlipRessourcesHandler(this);
		}

		return this.theFlipHander;
	}

	// --------------------------------------------------------------------------
	// SECTION: applet handling
	// --------------------------------------------------------------------------

	/**
	 * Returns information about this applet.
	 * 
	 * @return String a string containing information about the author, version,
	 *         and copyright of the applet.
	 */
	@Override
	public String getAppletInfo() {
		return TITLE_APP;
	}

	/**
	 * returns the base url the applet runs at
	 * 
	 * @return String the base url
	 */
	public String getBaseUrl() {
		return this.baseUrl;
	}

	/**
	 * sets the look and feel of the gui
	 */
	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (final UnsupportedLookAndFeelException e) {
			// handle exception
		} catch (final ClassNotFoundException e) {
			// handle exception
		} catch (final InstantiationException e) {
			// handle exception
		} catch (final IllegalAccessException e) {
			// handle exception
		}
	}

	/**
	 * constructor
	 * 
	 * Called by the browser or applet viewer to inform this applet that it has
	 * been loaded into the system. It is always called before the first time
	 * that the start method is called.
	 */
	@Override
	public void init() {
		final Container rootPane;

		// ----------------------------------------------------------------------
		// SECTION: setup applet and gui
		// ----------------------------------------------------------------------

		setLookAndFeel();
		instance = this;

		// Strip off trailer
		// http://localhost/flip/web/foo -> http://localhost/flip/web/
		String currentURL = getDocumentBase().toString().replaceFirst("[^/]+$", EMPTY_STRING);

		// Handle the case this applet is run in stand-alone mode
		if (currentURL.matches("^(http|https).+$")) {
			this.baseUrl = currentURL;
		} else {
			this.baseUrl = JOptionPane.showInputDialog("Please give an URL to the FLIP you want to work with!",
					"http://localhost/");
		}

		imageManager = new ImageManager(this);
		dataManager = new DataManager(this);
		danceFloorManager = new DanceFloorManager(this);

		mainPane = new MainPane(this);

		rootPane = getContentPane();
		rootPane.add(mainPane);
		rootPane.validate();

		// ----------------------------------------------------------------------
		// SECTION: initialize system
		// ----------------------------------------------------------------------

		getDanceFloorManager().setContainer(mainPane.getWorkplace());

		// Sync the new TableDancer app with the FLIP
		load(false);
	}

	/**
	 * loads data from the backend
	 * 
	 * if there are unsaved changes, the user is prompted with a confirmation
	 * dialog. when the data should be loaded, the current data is resetted
	 * first.
	 * 
	 * TODO implement SwingWorker thread usage
	 * 
	 * @param boolean prompt if the user should be prompted
	 * 
	 *        if there was an error while communicating with the backend, the
	 *        user is notified.
	 */
	public void load(final boolean prompt) {
		try {
			final DataManager dataManager = getDataManager();

			if (prompt && dataManager.hasChanged()) {
				final int answer = showConfirmBox(MSG_CHANGED, JOptionPane.YES_NO_OPTION);

				if (answer == JOptionPane.NO_OPTION) {
					return;
				}
			}

			getDanceFloorManager().removeAll();
			getDanceFloorManager().load();
		} catch (Exception e) {
			showErrorBox(MSG_LOAD_ERROR);
		}
	}

	/**
	 * saves data to the backend.
	 * 
	 * if there was an error while communicating with the backend or there were
	 * no changes made, the user is notified.
	 */
	public void save() {
		try {
			final DataManager dataManager;

			dataManager = getDataManager();

			if (!dataManager.hasChanged()) {
				showMessageBox(MSG_UNCHANGED);
				return;
			}

			dataManager.saveDataToBackend();
		} catch (Exception e) {
			showErrorBox(MSG_SAVE_ERROR);
			return;
		}

		// no exception occured, so everything went fine
		showMessageBox(MSG_SAVE_SUCCESS);
	}

	/**
	 * shows the about window with the applet-name, author, version and
	 * copyright information.
	 */
	public void showAboutWindow() {
		// do nothing :p
	}

	/**
	 * shows the preferences window to modify properies like grid size, colors,
	 * etc.
	 */
	public void showPreferencesWindow() {
		// do nothing :p
	}

	/**
	 * shows a scrolled messagebox with the given text
	 * 
	 * @param String
	 *            text the text
	 */
	public void showScrolledMessageBox(final String text) {
		showScrolledMessageBox(text, TITLE_MSG);
	}

	/**
	 * shows a scrolled mesagebox with the given text and title
	 * 
	 * @param String
	 *            text the text
	 * @param String
	 *            title the title
	 */
	public void showScrolledMessageBox(final String text, final String title) {
		final JScrollPane scrollText;

		if (text == null || title == null) {
			return;
		}

		scrollText = new JScrollPane(new JTextArea(text, 30, 60));

		JOptionPane.showMessageDialog(mainPane, scrollText, title, JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * shows a confirm box with the given text
	 * 
	 * @param String
	 *            text the text
	 * @param int the available options
	 */
	public int showConfirmBox(final String text, final int optionType) {
		return JOptionPane.showConfirmDialog(mainPane, text, TITLE_MSG, optionType);
	}

	/**
	 * shows a message box with the given text
	 * 
	 * @param String
	 *            text the text
	 */
	public void showMessageBox(final String text) {
		JOptionPane.showMessageDialog(mainPane, text, TITLE_MSG, JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * shows an error box with the given text
	 * 
	 * @param String
	 *            text the text
	 */
	public void showErrorBox(final String text) {
		JOptionPane.showMessageDialog(mainPane, text, TITLE_ERROR, JOptionPane.ERROR_MESSAGE);
	}

	public static void printException(final Exception e) {
		System.err.println("printException():" + e.getMessage());
		System.err.println("Stacktrace:");
		e.printStackTrace(System.err);
	}

	public static void showException(String title, String message, StackTraceElement trace[]) {
		ErrorDialog.showException(title, message, trace);
	}

	public void setWorkplace(JTabbedPane aWorkplace) {
		// TODO Auto-generated method stub

	}
}
