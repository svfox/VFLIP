/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancefloor;

import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Das Menu fuer die linke Maustaste
 */
public class DanceFloorMenu {
	private DanceFloor floor;

	private JPopupMenu dancePopUp;

	private final MenuItem[] MENU = { new MenuItem("neuer Tisch", false, false), new MenuItem("Raster", true, true),
			new MenuItem("am Raster ausrichten", true, false), new MenuItem("Raum", true, true),
			new MenuItem("makiere Alle", false, false), new MenuItem("speichern", false, false) };

	public DanceFloorMenu(DanceFloor floor) {
		this.floor = floor;
	}

	/**
	 * Liefert das Menu. Caching vorhanden.
	 * 
	 * @return
	 */
	public JPopupMenu setUp() {
		if (dancePopUp != null) {
			return dancePopUp;
		}

		dancePopUp = new JPopupMenu();
		DanceFloorActionListener aListener = new DanceFloorActionListener(floor);

		for (int i = 0; i < MENU.length; i++) {
			setUpMenuItem(MENU[i].getItem(), null, MENU[i].getName(), Integer.toString(i + 1), aListener,
					MENU[i].isDefault());
		}

		return dancePopUp;
	}

	/**
	 * Setzen des Menu-Eintrags
	 * 
	 * @param item
	 * @param action
	 * @param text
	 * @param name
	 * @param listener
	 * @param selected
	 */
	private void setUpMenuItem(JMenuItem item, Action action, String text, String name, ActionListener listener,
			boolean selected) {
		item.setAction(action);
		item.setText(text);
		item.setName(name);
		item.addActionListener(listener);
		item.setSelected(selected);
		dancePopUp.add(item);
	}

	/**
	 * kleine Hilfsklasse fuers Menu
	 */
	class MenuItem {
		private String name;

		private boolean checkBox;

		private boolean isDefault;

		public MenuItem(String name, boolean checkBox, boolean isDefault) {
			this.name = name;
			this.checkBox = checkBox;
			this.isDefault = isDefault;
		}

		public JMenuItem getItem() {
			if (checkBox) {
				return new JCheckBoxMenuItem();
			} else {
				return new JMenuItem();
			}
		}

		public boolean isDefault() {
			return isDefault;
		}

		public String getName() {
			return name;
		}
	}
}
