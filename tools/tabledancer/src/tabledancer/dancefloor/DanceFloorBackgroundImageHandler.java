/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancefloor;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

import tabledancer.FlipRessourcesHandler;

/**
 * Created by IntelliJ IDEA. User: jens Date: Jun 13, 2005 Time: 2:22:41 PM To
 * change this template use File | Settings | File Templates.
 */
public class DanceFloorBackgroundImageHandler {
	private DanceFloorPanel panel;

	private FlipRessourcesHandler flipHandler;

	public DanceFloorBackgroundImageHandler(DanceFloorPanel panel, FlipRessourcesHandler flipHandler) {
		this.panel = panel;
		this.flipHandler = flipHandler;
	}

	public BufferedImage getRasterPic(int myBlockId) {
		Image plainImage = flipHandler.getBackroundImage(myBlockId);
		BufferedImage newImage = new BufferedImage(plainImage.getWidth(null), plainImage.getHeight(null),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = newImage.createGraphics();
		g2d.setComposite(AlphaComposite.SrcOver);
		if (panel.isRaumSichtbar()) {
			g2d.drawImage(plainImage, 0, 0, plainImage.getWidth(null), plainImage.getHeight(null), panel);
		} else {
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, plainImage.getWidth(null), plainImage.getHeight(null));
		}
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.15));
		g2d.setColor(Color.red);
		if (panel.isRasterSichtbar()) {
			Stroke stroke = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1, new float[] { 2, 1 }, 0);
			g2d.setStroke(stroke);
			// Senkrechte
			for (int i = 0; i <= newImage.getWidth(); i += panel.getRaster().getX()) {
				g2d.drawLine(i, 0, i, newImage.getHeight());
			}
			// Vertikale
			for (int i = 0; i <= newImage.getHeight(); i += panel.getRaster().getY()) {
				g2d.drawLine(0, i, newImage.getWidth(), i);
			}
		}
		return newImage;
	}

}
