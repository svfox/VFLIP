/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancefloor;

import java.awt.AWTEvent;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import tabledancer.FlipRessourcesHandler;
import tabledancer.TableDancer;
import tabledancer.bean.Block;
import tabledancer.dancetable.DanceTable;

/**
 * Kuemmer sich um dan Handling des Halle als Panel
 */
public abstract class DanceFloorPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7675164801264825667L;

	private JPanel slopePanel;

	protected JPopupMenu dancePopUp;

	protected Block theBlock;

	private boolean rasterSichtbar = true;

	private boolean raumSichtbar = true;

	private boolean rasterAusrichten = false;

	protected int rasterX = 10, rasterY = 10;

	protected BufferedImage rasterImage;

	private boolean shiftGedrueckt = false;

	protected DanceFloorMouseHandler mHandler;

	private DanceFloorBackgroundImageHandler bgHandler;

	/**
	 * Konstruktor
	 * 
	 * @param theTableDancer
	 */
	protected DanceFloorPanel(Block myBlock) {
		theBlock = myBlock;
		setUpPanel();

		mHandler = new DanceFloorMouseHandler((DanceFloor) this);
		addMouseMotionListener(mHandler);
		setBounds(0, 0, getWidth(), getHeight());

		MediaTracker tracker = new MediaTracker(this);

		FlipRessourcesHandler flipHandler = TableDancer.getInstance().getRessourceHandler();

		tracker.addImage(flipHandler.getBackroundImage(theBlock.getId()), 0);

		try {
			tracker.waitForAll();
		} catch (InterruptedException e) {
		}

		bgHandler = new DanceFloorBackgroundImageHandler(this, flipHandler);

		rasterImage = bgHandler.getRasterPic(theBlock.getId());
		add(slopePanel);
		slopePanel.setBounds(0, 0, getMaxWidth(), getMaxHeight());
		setLayout(null);

		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
		enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}

	private void setUpPanel() {
		// spezieller JPanel, der ?ber allen anderen liegt, damit die Tables von
		// der Linie ?bermalt werden k?nnen und sie nicht darunter herumrutscht.
		// sieht einfach besser aus ;-)
		slopePanel = getSlopePanel();
		slopePanel.setPreferredSize(getPreferredSize());
		slopePanel.setMaximumSize(getPreferredSize());
		slopePanel.setMinimumSize(getPreferredSize());
	}

	/**
	 * Wrapper fuer die Mausposition
	 * 
	 * @return x-wert
	 */
	public int getAbsoluteMouseX() {
		return mHandler.getMouseX();
	}

	/**
	 * Wraper fuer die Mausposition
	 * 
	 * @return y-wert
	 */
	public int getAbsoluteMouseY() {
		return mHandler.getMouseY();
	}

	/**
	 * Ja, was macht diese Methode (JM: ich hasse solche verschachtelungen.
	 * Diese sind total unuebersichtlich) todo jm verschachtelung aufl?sen.
	 * 
	 * @return JPanel
	 */

	private JPanel getSlopePanel() {
		JPanel result = new JPanel() {
			private static final long serialVersionUID = -6412339304991981250L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (mHandler.isMakieren()) {
					Graphics2D g2d = (Graphics2D) g;
					Stroke stroke = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1, new float[] {
							2, 1 }, 0);
					g2d.setStroke(stroke);
					g2d.setColor(Color.RED);
					Rectangle Slope = mHandler.getSlopeRectangle();
					g2d.drawRect(Slope.x, Slope.y, Slope.width, Slope.height);
				}
			}
		}; // JPannel
		result.setOpaque(false);

		return result;
	}

	/**
	 * waehlt die Tische im gezogenden Rechteck aus
	 */
	public void selectRectChairs() {
		Rectangle select = mHandler.getSlopeRectangle();
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			Object obj = getComponent(i);
			if (obj instanceof DanceTable) {
				if (select.contains(((DanceTable) obj).getFlipPosition())) {
					((DanceTable) obj).setSelected(true);
				} else {
					((DanceTable) obj).setSelected(false);
				}
			}
		}

		repaint();
	}

	/**
	 * wrapper fuer alle Mausevents
	 * 
	 * Beinhaltet bewegen der Maus innerhalb des Applets,
	 * 
	 * @param event
	 */
	@Override
	public void processMouseEvent(MouseEvent event) {
		// delegation
		mHandler.processMouseEvent(event);
	}

	/**
	 * Raster sichbar?
	 * 
	 * @return ja/nein
	 */
	public boolean isRasterSichtbar() {
		return rasterSichtbar;
	}

	/**
	 * Am Raster ausrichten - Flag
	 * 
	 * @return rasterflag
	 */
	public boolean isRasterAusrichten() {
		return rasterAusrichten;
	}

	/**
	 * Am-Raster-Ausrichten-Flag
	 * 
	 * @param value
	 */
	public void setRasterAusrichten(boolean value) {
		rasterAusrichten = value;
	}

	/**
	 * Raster-Sichtbar-Flag
	 * 
	 * @param value
	 */
	public void setRasterSichtbar(boolean value) {
		rasterSichtbar = value;
		rasterImage = bgHandler.getRasterPic(theBlock.getId());
		repaint();
	}

	/**
	 * Hintergrundbild anzeigen?
	 * 
	 * @return ja/nein
	 */
	public boolean isRaumSichtbar() {
		return raumSichtbar;
	}

	/**
	 * Flag fuers Hintergrundbild
	 * 
	 * @param value
	 */
	public void setRaumSichtbar(boolean value) {
		raumSichtbar = value;
		rasterImage = bgHandler.getRasterPic(theBlock.getId());
		repaint();
	}

	/**
	 * liefert die Rastergroesse
	 * 
	 * @return 0/0 zu x/y - Punkt
	 */
	public Point getRaster() {
		return (new Point(rasterX, rasterY));
	}

	/**
	 * Flag fuer die SHIFT-Taste
	 * 
	 * @return ShiftFlag
	 */
	public boolean isShiftGedrueckt() {
		return shiftGedrueckt;
	}

	/**
	 * set das Flag fuer die SHIFT-Taste
	 * 
	 * @param value
	 */
	public void setShiftGedrueckt(boolean value) {
		shiftGedrueckt = value;
	}

	/**
	 * Setzt das Makiert-Flag fuer alle Tische
	 * 
	 * @param value
	 */
	public void setSelectedAll(boolean value) {
		repaint();
		Object comp;
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			comp = getComponent(i);
			if (comp instanceof DanceTable) {
				((DanceTable) comp).setSelected(value);
			}
		}
	}

	/**
	 * liefert die Breite des Hintergrundbildes
	 * 
	 * @return maxWidth
	 */
	public int getMaxWidth() {
		return rasterImage.getWidth();
	}

	/**
	 * lierfert die hoehe des Hintergrundbildes
	 * 
	 * @return maxHeigth
	 */
	public int getMaxHeight() {
		return rasterImage.getHeight();
	}

}
