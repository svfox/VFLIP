/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancefloor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;

import tabledancer.TableDancer;
import tabledancer.bean.Seat;
import tabledancer.dancetable.DanceTable;

/**
 * Verarbeitung den JPopupMenu
 */
public class DanceFloorActionListener implements ActionListener {

	private DanceFloor panel;

	public DanceFloorActionListener(DanceFloor panel) {
		this.panel = panel;
	}

	/***************************************************************************
	 * Verarbeitung des Menuaufrufs
	 * 
	 * @param event
	 */
	public void actionPerformed(ActionEvent event) {
		JComponent menuentry = (JComponent) event.getSource();
		switch (Integer.parseInt(menuentry.getName())) {
		// Neuen Tisch erzeugen
		case 1:
			Seat mySeat = new Seat(panel.theBlock);
			mySeat.setCenterPosX(panel.getAbsoluteMouseX());
			mySeat.setCenterPosY(panel.getAbsoluteMouseY());
			mySeat.setName("Neuer Sitzplatz");
			mySeat.setEnabled(true);
			panel.theBlock.addSeat(mySeat);
			DanceTable neuerTisch = new DanceTable(mySeat, panel);
			panel.addTable(neuerTisch);
			panel.repaint();
			break;

		// Rastersichtbarkeit
		case 2:
			panel.setRasterSichtbar(((JCheckBoxMenuItem) menuentry).getState());
			break;

		// Raster einrasten...
		case 3:
			panel.setRasterAusrichten(((JCheckBoxMenuItem) menuentry).getState());
			break;

		// Raumsichtbarkeit
		case 4:
			panel.setRaumSichtbar(((JCheckBoxMenuItem) menuentry).getState());
			break;

		// Alle Tische selektieren
		case 5:
			panel.setSelectedAll(true);
			break;

		// speichern
		case 6:
			try {
				TableDancer.getInstance().save();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
			break;
		}
	}

}
