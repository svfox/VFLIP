package tabledancer.dancefloor;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tabledancer.FlipRessourcesHandler;
import tabledancer.TableDancer;
import tabledancer.bean.Block;
import tabledancer.dancetable.DanceTable;
import tabledancer.xml.XmlParserKonstanten;

/**
 * <p>
 * Description: Applet for table positioning in the FLIP intranet system
 * </p>
 * 
 * @author Kai 'Riffer' Posadowsky
 * @version 1.0
 */
public class DanceFloor extends DanceFloorPanel {
	private static final long serialVersionUID = -6894533268218762371L;

	private int maxSeatWidth = 0;

	private int maxSeatHeight = 0;

	private int maxChairWidth = 0;

	private int maxChairHeight = 0;

	private Map<String, Image> tableGreyImages = new HashMap<String, Image>(),
			tableGreenImages = new HashMap<String, Image>(), tableChairImages = new HashMap<String, Image>();

	private boolean isGettingDragged = false, isDragCanceled = false;

	private List<DanceTable> tables;

	public DanceFloor(Block myBlock) throws Exception {
		super(myBlock);

		setVisible(false);

		tableGreenImages = loadSnapImages(theBlock.getTableAngleUrls(XmlParserKonstanten.TABLEGREEN)); // todo:
																										// Mal
																										// schön
																										// machen
		tableGreyImages = loadSnapImages(theBlock.getTableAngleUrls(XmlParserKonstanten.TABLEGREY)); // (ohne
																										// Konstanten)
		tableChairImages = loadSnapImages(theBlock.getTableAngleUrls(XmlParserKonstanten.CHAIR)); // direkt
																									// beim
																									// parsen
																									// "ordentlich"
																									// zuweisen
		// Größten finden
		for (Image i : tableGreenImages.values()) {
			setMaxSeatHeight(i.getHeight(null));
			setMaxSeatWidth(i.getWidth(null));
		}
		for (Image i : tableGreyImages.values()) {
			setMaxSeatHeight(i.getHeight(null));
			setMaxSeatWidth(i.getWidth(null));
		}
		for (Image i : tableChairImages.values()) {
			setMaxChairHeight(i.getHeight(null));
			setMaxChairWidth(i.getWidth(null));
		}

		tables = new ArrayList<DanceTable>();
	}

	public void setDragPosition(int xdiff, int ydiff) {
		DanceTable dragTable;
		DanceTable selTable;
		for (int i = 0; i < getComponentCount(); i++) {
			if (getComponent(i) instanceof DanceTable && ((DanceTable) getComponent(i)).isSelected()
					&& !((DanceTable) getComponent(i)).isDragTable()) {
				selTable = (DanceTable) getComponent(i);
				dragTable = selTable.getDragInstance();
				dragTable
						.setTablePosition(selTable.getTablePosition().x + xdiff, selTable.getTablePosition().y + ydiff);
			}
		}
	}

	/**
	 * Drueckt jemand mit der Maus auf einen Chair oder laesst los.
	 * 
	 * @param value
	 */
	public void setGettingDragged(boolean value) {
		isGettingDragged = value;
	}

	public boolean isGettingDragged() {
		return isGettingDragged;
	}

	public void setDragCanceled(boolean value) {
		isDragCanceled = value;
	}

	public boolean isDragCanceled() {
		return isDragCanceled;
	}

	public void cancelSelectedPosition() {
		DanceTable selTable;
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			if (getComponent(i) instanceof DanceTable && ((DanceTable) getComponent(i)).isSelected()
					&& !((DanceTable) getComponent(i)).isDragTable()) {
				selTable = (DanceTable) getComponent(i);
				selTable.killDragInstance();
			}
		}
		setCursor(Cursor.getDefaultCursor());
	}

	/**
	 * Wird ausgefuehrt, wenn min. ein Chair bewegt wurde.
	 */
	public void fixSelectedPosition() {
		DanceTable dragTable;
		DanceTable selTable;
		for (int i = 0; i <= getComponentCount() - 1; i++) {
			if (getComponent(i) instanceof DanceTable && ((DanceTable) getComponent(i)).isSelected()
					&& !((DanceTable) getComponent(i)).isDragTable()) {
				selTable = (DanceTable) getComponent(i);
				dragTable = selTable.getDragInstance();
				selTable.setTablePosition(dragTable.getTablePosition());
				selTable.killDragInstance();
			}
		}
	}

	private Map<String, Image> loadSnapImages(Map<String, String> ImageUrls) throws InterruptedException {
		Image img;
		FlipRessourcesHandler flipHandler = TableDancer.getInstance().getRessourceHandler();
		Map<String, Image> Imagelist = new HashMap<String, Image>();
		MediaTracker tracker = new MediaTracker(this);

		for (Object element : ImageUrls.keySet()) {
			String angle = (String) element;
			try {
				img = flipHandler.getURLImage(ImageUrls.get(angle));
				tracker.addImage(img, Integer.valueOf(angle).intValue());
				Imagelist.put(angle, img);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		tracker.waitForAll();

		return Imagelist;
	}

	public int getMaxSeatHeight() {
		return maxSeatHeight;
	}

	private void setMaxSeatHeight(int maxSeatHeight) {
		this.maxSeatHeight = Math.max(this.maxSeatHeight, maxSeatHeight);
	}

	public int getMaxSeatWidth() {
		return maxSeatWidth;
	}

	private void setMaxChairWidth(int maxChairWidth) {
		this.maxChairWidth = Math.max(this.maxChairWidth, maxChairWidth);
	}

	public int getMaxChairHeight() {
		return maxChairHeight;
	}

	private void setMaxChairHeight(int maxChairHeight) {
		this.maxChairHeight = Math.max(this.maxChairHeight, maxChairHeight);
	}

	public int getMaxChairWidth() {
		return maxChairWidth;
	}

	private void setMaxSeatWidth(int maxSeatWidth) {
		this.maxSeatWidth = Math.max(this.maxSeatWidth, maxSeatWidth);
	}

	public Image getTable(int SnapAngle, boolean enabled) {
		return enabled ? getTableGreen(SnapAngle) : getTableGrey(SnapAngle);
	}

	private Image getTableGreen(int snapAngle) {
		return getTable(tableGreenImages, snapAngle);
	}

	private Image getTableGrey(int snapAngle) {
		return getTable(tableGreyImages, snapAngle);
	}

	public Image getTableChair(int snapAngle) {
		return getTable(tableChairImages, snapAngle);
	}

	private Image getTable(Map<String, Image> tables, int snapAngle) throws NullPointerException {
		if (tables.containsKey(String.valueOf(snapAngle))) {
			return tables.get(Integer.toString(snapAngle));
		} else if (tables.containsKey("0")) {
			return tables.get("0");
		} else {
			throw new NullPointerException("No tableimage for angle 0 or " + snapAngle);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// Raster
		g.drawImage(rasterImage, 0, 0, rasterImage.getWidth(null), rasterImage.getHeight(null), this);
	}

	public void addTable(DanceTable table) {
		tables.add(table);
		super.add(table);
		table.setTablePositionFromSeat();
	}

	public Collection<DanceTable> getAllTables() {
		return tables;
	}

	public List<DanceTable> getSelectedTables() {
		List<DanceTable> list = new ArrayList<DanceTable>();
		DanceTable table;
		for (int i = 0; i < tables.size(); i++) {
			table = tables.get(i);
			if (table.isSelected() && table.isVisible()) {
				list.add(table);
			}
		}
		return list;
	}

	public void removeTable(DanceTable danceTable) {
		remove(danceTable);
		tables.remove(danceTable);
		theBlock.removeSeat(danceTable.getSeat());
	}
}
