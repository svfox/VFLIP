#!/usr/local/bin/php -q
<?php

require_once("inc.lockarray.php");
require_once("cfg.netlog.php");

Main();

function Main()
{
  global $cfg,$queue,$OS;
  ob_implicit_flush();
  set_time_limit(0);
  $OS = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? "win" : "other";
  $queue = new LockArray($cfg["queuename"],$cfg["tempdir"]);
  doLog("Starte Endlosschleife, um Lookups aus der Queue $cfg[tempdir]$cfg[queuename] abzuarbeiten. (OS:$OS)");
  while(true) 
  {
    if(!LookupOnce()) sleep(1);
  }
//  SetStatus(array("ip" => "11.11.11.11"),"host_offline");
}

function doLog($Msg)
{
  echo date("m.d.y G:i:s")." $Msg\n";
  return false;
}

function LookupOnce()
{
  global $queue,$cfg;
  $Item = $queue->shift();
  
  if(!is_array($Item)) return false;
  doLog("Looking up $Item[ipv4]...");  

    // lookup data  
  if($Item["action"] != "host_offline")
  {
    LookupNetBIOS($Item["ipv4"],$Item["nbname"],$Item["nbgroup"],$Item["nbunames"]);
    $Item["dnsname"] = LookupDNS($Item["ipv4"]);
    if(empty($Item["mac"])) $Item["mac"] = LookupMAC($Item["ipv4"]);
  }
  $Item["nic_vendor"] = LookupVendor($Item["mac"]);
  
    // write data
  if(!empty($cfg["dblog"]) or !empty($cfg["dbserver"]))  
  {
    $i = array();
    foreach($Item as $k => $v) $i[] = "`$k` = '".addslashes($v)."'";
    $i = "INSERT `flip_netlog` SET ".implode(", ",$i).";";
    
    if(!SubmitDBServer($i)) doLog("Fehler");
    if(!SubmitDBLog($i)) doLog("Fehler");
  }
  if(!SubmitFlipUrl($Item)) doLog("Fehler");
  doLog("done.");
  return true;
}

function LookupNetBIOS($IP,&$NBName,&$NBGroup,&$NBUNames)
{
  global $OS;
  $NBGroup = "";
  $NBName = "";
  $uname = array();
  $name = "\w\d\.\-";
  $a = array();
  if($OS == "win")
  {
    $p = popen("nbtstat -A \"".addslashes($IP)."\"","r");
    // nach "------------" suchen
    while(!feof($p)) if(substr(trim(fgets($p)),0,3) == "---") break;
    while(!feof($p)) 
    { 
      $s = trim(fgets($p));
      if(preg_match("/^([$name]+)\s*<00>\s*(EINDEUTIG|UNIQUE)/i",$s,$a)) $NBName = $a[1];
      if(preg_match("/^([$name]+)\s*<00>\s*(GRUPPE|GROUP)/i",$s,$a)) $NBGroup = $a[1];
      if(preg_match("/^([$name]+)\s*<03>\s*(EINDEUTIG|UNIQUE)/i",$s,$a)) $uname[] = $a[1];
    }    
    pclose($p);
  }  
  else 
  {
    $p = popen("nmblookup -A \"".addslashes($IP)."\"","r");
    while(!feof($p)) 
    { 
      $s = trim(fgets($p));
      if(preg_match("/^([$name]+)\s*<00> -    /i",$s,$a)) $NBName = $a[1];
      if(preg_match("/^([$name]+)\s*<00> - <GROUP>/i",$s,$a)) $NBGroup = $a[1];
      if(preg_match("/^([$name]+)\s*<03> -    /i",$s,$a)) $uname[] = $a[1];
    }    
    pclose($p);
  }
  foreach($uname as $k => $v) if($v == $NBName) unset($uname[$k]);
  $NBUNames = implode(", ",$uname);
  return (empty($NBGroup) and empty($NBName)) ? false : true;
}

function _FormatMAC($mac,$delim)
{
  $a = explode(":",strtr($mac,$delim,":"));
  return sprintf("%x:%x:%x:%x:%x:%x",hexdec($a[0]),hexdec($a[1]),hexdec($a[2])
                                    ,hexdec($a[3]),hexdec($a[4]),hexdec($a[5]));
}

function _ParseArp($IP)
{
  global $OS;
  $a = array();
  if($OS == "win")
  {
    $p = popen("arp -a","r");
    while(!feof($p)) 
      if(preg_match("/^$IP\s+([a-f\d\-]+)/i",trim(fgets($p)),$a))
      {
        pclose($p);
        return _FormatMAC($a[1],"-");
      }
  }
  else
  {
    $p = popen("arp -n","r");
    while(!feof($p))
      if(preg_match("/^$IP.+([a-f\d:]{17})/i",trim(fgets($p)),$a))
      {
        pclose($p);
        return _FormatMAC($a[1],":");
      }
      print_r($a);
  }
  pclose($p);
  return false;
}

function _Ping($IP)
{
  global $OS;
  $ip = addslashes($IP);
  if($OS == "win") exec("ping -n 1 \"$ip\"");
  else exec("ping -c1 \"$ip\"");
}

function LookupMAC($IP)
{
  if($mac = _ParseArp($IP)) return $mac;
  _Ping($IP);
  if($mac = _ParseArp($IP)) return $mac;
  return false;
}

function LookupDNS($IP)
{
  return gethostbyaddr($IP);
}

function LookupVendor($mac)
{
  $db = "ethercodes.dat";
  
  // db erstellen
  // BUG: Datei existiert nicht?!
  // if(!is_file($db)) include("updatevendors.php");
  static $cache = false;
  
  // db laden
  if(!is_array($cache))
  {
    $cache = array();
    foreach(file($db) as $line)
    {
      list($oui,$vendor) = explode("\t",trim($line),2);
      $cache[$oui] = $vendor;
    }
  }
  
  // mac suchen
  $a = explode(":",$mac,4);
  $oui = "$a[0]:$a[1]:$a[2]";
  $vendor = $cache[$oui];
  return (empty($vendor)) ? false : trim($vendor);
}

function SubmitDBServer($Query = "")
{
  global $cfg;
  static $mysql;
  if(empty($cfg["dbserver"])) return true;
  doLog("DB..");
  if(!$mysql)
  {
    $mysql = mysql_connect($cfg["dbserver"],$cfg["dbuser"],$cfg["dbpass"]);
    if(!$mysql) return doLog("ERROR: Die Verbindung zur Datenbank ($cfg[dbuser]@$cfg[dbserver]) konnte nicht hergestellt werden. (".mysql_error().")");
    if(!mysql_select_db($cfg["dbname"],$mysql))
      return doLog("ERROR: Die Datenbank $DName konnte nicht ausgewählt werden. (".mysql_error().")");
  }
  if(!empty($Query))
    if(!mysql_query($Query,$mysql))
      return doLog("WARNING: Eine Datenbank-Abfrage konnte nicht fehlerfrei ausgefuehrt werden. (".mysql_error($mysql).") \"$i\"");
  return true;
}

function SubmitDBLog($Line)
{
  global $cfg;
  if(empty($cfg["dblog"])) return true;
  doLog("Log..");
  $f = fopen($cfg["dblog"],"a");
  if(!$f) return doLog("Die Datei $cfg[dblog] konnte nicht zum Schreiben geoeffnet werden.");
  if(!fputs($f,"$Line\n")) 
  {
    fclose($f);
    return doLog("In die Datei $cfg[dblog] konnte nicht geschrieben werden.");
  }
  fclose($f);
  return true;
}

function SubmitFlipUrl($Items)
{
  global $cfg;
  if(empty($cfg["flipurl"])) return true;
  doLog("FLIP..");
  include_once("mod.netlog.protocol.php");
  $p = new NetLogPacket();
  $p->Command = "submit_data";
  $p->Keys = $Items;
  $r = $p->sendHTTP("$cfg[flipurl]netlog.php?action=exec");  
  if(is_object($r)) $r->handleResponse();
  return is_object($r);
}  
  /*
  $Items["netlog_action"] = $Items["action"];
  $Items["action"] = "writenetlog";
  $url = "$cfg[flipurl]netlog.php";
  return HttpPostRequest($url,$Items,array("Cookie" => "disable-flip-cookie=1;"));
}

function HttpPostRequest($URL,$Data=array(),$Header=array(),$Timeout = 30)
{
  foreach($Data as $k => $v) $Data[$k] = "$k=".urlencode($v);
  $Data = implode("&",$Data);
  $u = parse_url($URL);
  if(!isset($Header["User-Agent"])) $Header["User-Agent"] = "FLIP Webclient 0.1";
  if(!isset($Header["Connection"])) $Header["Connection"] = "Keep-Alive";
  if(!isset($Header["Cache-Control"])) $Header["Cache-Control"] = "no-cache";
  $Header["Content-Length"] = strlen($Data);
  $Header["Content-Type"] = "application/x-www-form-urlencoded";
  $Header["Host"] = $u["host"];
  
  $dir = (empty($u["query"])) ? $u["path"] : "$u[path]?$u[query]";
  $dat = "POST $dir HTTP/1.1\r\n";
  foreach($Header as $k => $v) $dat .= "$k: $v\r\n";
  $dat .= "\r\n$Data";
  $port = (empty($u["port"])) ? 80 : $u["port"];
  return SocketPutContents($u["host"],$dat,$port,$Timeout);
}

// geklaut aus core.utils.php
function SocketPutContents($Address,$Data,$DefaultPort = 80,$Timeout = 30)
{
  list($host,$port) = array_pad(explode(":",$Address,2),2,$DefaultPort);
  $f = fsockopen($host,$port,$errno,$errstr,$Timeout);
  if(!$f) return doLog("Eine Socketverbindung konnte nicht hergestellt werden.|Host:$host:$port Err: $errno:$errstr");
  if(!fwrite($f,$Data))
  {
    fclose($f);
    return doLog("In eine Socketverbindung konnte nicht geschrieben werden.|Host:$host:$port Err: $errno:$errstr");
  }
  fclose($f);
  return true;
}
*/

?>
