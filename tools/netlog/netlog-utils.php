#!/usr/local/bin/php -q
<?php

require_once("inc.lockarray.php");

$cfg["tempdir"] = "./";
$cfg["queuename"] = "netlog";

$queue = new LockArray($cfg["queuename"],$cfg["tempname"]);

print_r($queue->_getArray());
//echo "\x0C\r";
//echo "test\n";
//for($i = 0; $i < 20; $i++) echo chr($i)." [$i]\n";


if($argv[1] == "-u") UpdateEthercodes();

function doLog($Msg,$Die = false)
{
  echo date("m.d.y G:i:s")." $Msg\n";
  return !$Die;
}

function UpdateEthercodes()
{
  $source = "http://standards.ieee.org/regauth/oui/oui.txt";
  
  doLog("Lade Quelldatei vom IEEE...\n");
  $a = file($source);
  if(!is_array($a)) return doLog("ERROR: Die Datei $source konnte nicht gelesen werden.");
  
  $f = @fopen("ethercodes.dat","w");
  if(!$f) die("ERROR: Die Datei ethercodes.dat konnte nicht zum schreiben geöffnet werden.");
  
  $b = array();
  foreach($a as $l)
   if(preg_match("/^([\da-f]+)\-([\da-f]+)\-([\da-f]+)\s+\(hex\)\s+(.*)$/i",trim($l),$b))
     fwrite($f,sprintf("%x:%x:%x\t%s\r\n",hexdec($b[1]),hexdec($b[2]),hexdec($b[3]),$b[4]));
     
  fclose($f);
  doLog("Fertig. Die Datei ethercodes.dat wurde geupdated.");
}

?>