<?php
if (!function_exists("file_put_contents")) {
	function file_put_contents($Filename, $Data) {
		$f = fopen($Filename, "wb");
		$r = fwrite($f, $Data);
		fclose($f);
		return $r;
	}
}

if (!function_exists("file_get_contents")) {
	function file_get_contents($FileName) {
		$f = fopen($FileName, "rb");
		$r = fread($f, filesize($FileName));
		fclose($f);
		return $r;
	}
}

class LockFile {
	var $Name;
	var $Dir;
	var $Lock = 0;
	var $_FileName;
	var $_LockName;
	var $_Data;

	function LockFile($aName, $aDir = "./") {
		$this->Name = $aName;
		$this->Dir = $aDir;
		$this->_LockName = "{$this->Dir}{$this->Name}.lock";
		$this->_FileName = "{$this->Dir}{$this->Name}.dat";
		if (!is_file($this->_LockName))
			$this->_lockFile(0);
		if (!is_file($this->_FileName))
			file_put_contents($this->_FileName, "");
	}

	function _lockFile($newVal = null) {
		if ($newVal === null)
			return strval(file_get_contents($this->_LockName));
		$s = strval($newVal);
		file_put_contents($this->_LockName, $s);
		return $s;
	}

	function lock() {
		$this->_waitForLock();
		$this->Lock++;
		if ($this->Lock == 1)
			$this->_lockFile(1);
	}

	function unlock() {
		$this->Lock--;
		if ($this->Lock == 0) {
			file_put_contents($this->_FileName, $this->_Data);
			$this->_lockFile(0);
		}
		if ($this->Lock < 0)
			trigger_error("LockFile: unlock() kann nicht oefter aufgerufen werden als Lock.", E_USER_WARNING);
	}

	function _wait() {
		if (function_exists("usleep"))
			usleep(10); // usleep nicht auf win32 verfuegbar -> win sux.
		sleep(1);
	}

	function _waitForLock() {
		if ($this->Lock > 0)
			return; // eigener lock
		$this->_Data = null;
		for ($i = 0; $i < 100; $i++)
			if ($this->_lockFile() != 0)
				$this->_wait();
			else
				return $this->Lock = 0;
		trigger_error("Timeout beim warten auf Lock {$this->Name}. Moeglicher Deadlock. Lock aufgebrochen.", E_USER_WARNING);
		$this->_lockFile(0);
		return $this->Lock = 0;
	}

	function getContents() {
		$this->_waitForLock();
		if ($this->_Data !== null)
			return $this->_Data;
		return $this->_Data = file_get_contents($this->_FileName);
	}

	function setContents($Data) {
		$this->lock();
		$this->_Data = $Data;
		$this->unlock();
	}
}

class LockArray extends LockFile {
	function LockArray($aName, $aDir = "./") {
		parent :: LockFile($aName, $aDir);
	}

	function _getArray() {
		$a = $this->getContents();
		if (empty ($a))
			return array ();
		$r = unserialize($a);
		if (!is_array($r))
			return array ();
		return $r;
	}

	function _setArray($Array) {
		$this->setContents(serialize($Array));
	}

	// liefert das letzte Element
	function pop() {
		$a = $this->_getArray();
		$r = array_pop($a);
		$this->_setArray($a);
		return $r;
	}

	// fuegt ein Element an das Ende des Arrays
	function push($Item) {
		$a = $this->_getArray();
		$r = array_push($a, $Item);
		$this->_setArray($a);
		return $r;
	}

	// liefert das erste Element des Arrays
	function shift() {
		$a = $this->_getArray();
		$r = array_shift($a);
		$this->_setArray($a);
		return $r;
	}

	// fuegt ein Element am Anfang des Arrays ein
	function unshift($Item) {
		$a = $this->_getArray();
		$r = array_unshift($a, $Item);
		$this->_setArray($a);
		return $r;
	}

}
?>