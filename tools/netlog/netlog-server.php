#!/usr/local/bin/php -q
<?php

require_once("inc.lockarray.php");
require_once("cfg.netlog.php");
require_once("mod.netlog.protocol.php");

Main();

function Main()
{
  global $cfg,$queue;
  ob_implicit_flush();
  set_time_limit(0);
  $queue = new LockArray($cfg["queuename"],$cfg["tempdir"]);
  RunLookupServer();
}

function doLog($Msg,$Die = false)
{
  echo date("m.d.y G:i:s")." $Msg\n";
  return !$Die;
}

function _SockErr($Msg,$Code = 0,$Sock = null)
{
  doLog($Msg.(($Code != 0) ? ": ".socket_strerror($Code) : ""));
  if(is_resource($Sock)) socket_close($Sock);
  return false;
}

function ServeClient($msgsock)
{
  global $cfg,$queue;
  $remoteip = $remoteport = null;
  if(!socket_getpeername($msgsock,$remoteip,$remoteport)) return _SockErr("Konnte fuer Socket $cfg[lsip]:$cfg[lsport] die Remote-IP nicht auslesen.",socket_last_error($msgsock));
  $log = "Verbindung von $remoteip:$remoteport entgegengenommen";
  if(!in_array($remoteip,$cfg["lsclients"])) return _SockErr("$log -> Zugriff verweigert, da nicht authorisierte IP-Adresse.");
  $query = "";
  while(true)
  {
    if (false === ($buf = socket_read($msgsock, 2048, PHP_BINARY_READ))) return _SockErr("$log -> Konnte nicht aus Verbindung lesen.",socket_last_error($msgsock));
    if($buf == "") return _SockErr("$log -> Verbindung wurde verzeitig beendet."); // connection closed
    else $query .= $buf;
    foreach(array_reverse(explode("\n",$query)) as $line) if(trim($line) == "") break 2; // bis zur leerzeile lesen.
  }
  
  /*
  $a = explode("\n",trim($query));
  $command = trim(array_shift($a));
  $data = array();
  foreach($a as $l) if(trim($l) != "")
  {
    list($k,$v) = explode(":",$l,2);
    $data[trim($k)] = trim($v);
  }
  if(!preg_match("/^[\d\w]+$/i",$command)) return _SockErr("$log -> Der Command \"$command\" ist ungueltig.");   

  if($command == "lookup") 
  {
    _SockErr("$log -> bearbeite Lookup: ip: $data[ip]");
    $queue->push($data);
  }
  else return _SockErr("$log -> Der Command \"$command\" ist unbekannt.");*/
  
  $p = new NetLogPacket();
  $r = new NetLogPacket();
  $p->parse($query);
  socket_write($msgsock,$r->buildResponse());
  switch($p->Command)  
  {
    case("submit_ipv4"):
      $queue->push($p->Keys);
      _SockErr("$log -> bearbeite Lookup: ipv4: {$p->Keys[ipv4]}");
      break;
    default:    
     return _SockErr("$log -> Unbekannter Befehl:{$p->Command}.");
  }
}

function RunLookupServer()
{
  global $cfg, $Version;
  doLog("Starte LookupServer...");
  
  if(!function_exists("socket_create")) return doLog("um den LookupServer nutzen zu koennen muss das php-modul \"sockets\" geladen sein.",true);
  
  if(($sock = socket_create(AF_INET,SOCK_STREAM,SOL_TCP)) < 0) return _SockErr("Kann Socket nicht erstellen.",$sock);
  if(($ret = socket_bind($sock,$cfg["lsip"],$cfg["lsport"])) < 0) return _SockErr("Kann Socket nicht an $cfg[lsip]:$cfg[lsport] binden.",$ret,$sock); 
  if(($ret = socket_listen($sock,50)) < 0) return _SockErr("Kann mit Socket $cfg[lsip]:$cfg[lsport] nicht in Listen-Modi wechseln.",$ret,$sock);
  
  //$sock = socket_create_listen($cfg["lsport"],SOMAXCONN);
  //if(!$sock) return _SockErr("Konnte kein Socket auf Port $cfg[lsport] oeffnen.",socket_last_error());
  
  $localip = $localport = null;
  if(!socket_getsockname($sock,$localip,$localport)) return _SockErr("Konnte fuer Socket nicht die lokale Adresse auslesen.",socket_last_error($sock),$sock);
  doLog("LookupServer laeuft auf $localip:$localport. Authorisierte Clients: ".implode(",",$cfg["lsclients"]));
  while(true)
  {
    if(($msgsock = socket_accept($sock)) < 0)
    {
      _SockErr("Konnte mit Socket $cfg[lsip]:$cfg[lsport] eine eingehende Verbindung nicht entgegen nehmen.",$msgsock);
      sleep(1);
      continue;
    }
    ServeClient($msgsock);
    socket_close($msgsock);
  } 
  socket_close($sock);
}

function GetErrors()  // wird von mod.netlog.protocol.php aufgerufen
{
  return false;
}


?>