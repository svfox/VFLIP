/* $Id: arpwatch.c 404 2004-08-25 21:20:48Z docfx $ */

/* Copyright (C) 2004 by Soeren Bleikertz <soeren@openfoo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <pcap.h>
#include <unistd.h>
#include <pthread.h>
#include <syslog.h>
#include <netinet/in_systm.h>
#include <sys/queue.h>
#include <sys/socket.h> 
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "arpwatch.h"
#include "error.h"
#include "ethcodes.h"
#include "lookup.h"
#include "queue.h"


GHashTable *arp_hash;
extern option_t opt;


int pcap_init(pcap_t **pcap)
{
    int snaplen = sizeof(struct ether_header)+1;
    char err_buf[PCAP_ERRBUF_SIZE];
    uint32_t localnet, netmask;

    if (!opt.interface) {
	if (!(opt.interface = pcap_lookupdev(err_buf))) {
	    Perror("can not lookup interface");
	    return -1;
	}
    }	

    if (!(*pcap = pcap_open_live(opt.interface, snaplen, 1, TIMEOUT, err_buf))) {
	Perror("can not open pcap");
	return -1;
    }

    if (pcap_lookupnet(opt.interface, &localnet, &netmask, err_buf) <0) {
	Perror("can not lookup net");
	return -1;
    }		

    return 0;
}	


void *arpwatch(void *foo)
{
    pcap_t *pcap;
    struct pcap_pkthdr hdr;
    struct ether_header *eptr;
    struct ether_arp *aptr;
    char *ptr;
    gpointer hash_arp_tmp;
    unsigned char mac_tmp[18];
    unsigned char ipv4_tmp[16];
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    hash_value_arp_t *value_arp_tmp;
    hash_value_ip_t *value_ipv4_tmp;
    time_t time_tmp, time_diff;
    job_t job;
    /* queue_t qfoo, *qpfoo; */


    if (!opt.daemon)	
        printf("arpwatch running\n");

    
    /* init stuff */
    if (pcap_init(&pcap) != 0) {
	Perror("can not init pcap");
	pthread_exit(NULL);
    }
   	
    arp_hash = g_hash_table_new(g_str_hash, g_str_equal);
	
    pthread_mutex_init(&mutex, NULL);


    /* waiting for packets */
    while (1) {
	while ((ptr = (char*) pcap_next(pcap, &hdr)) == NULL) ;
		
	eptr = (struct ether_header*) ptr;
	aptr = (struct ether_arp*) (eptr+1);
		
	if (ntohs(eptr->ether_type) != ETHERTYPE_IP)
	    continue;

	snprintf(mac_tmp, 18, "%x:%x:%x:%x:%x:%x",
	    eptr->ether_shost[0], eptr->ether_shost[1], eptr->ether_shost[2],
	    eptr->ether_shost[3], eptr->ether_shost[4], eptr->ether_shost[5]);

	/* probs with endian? */
	snprintf(ipv4_tmp, 16, "%d.%d.%d.%d", aptr->arp_spa[2], aptr->arp_spa[3],
	    aptr->arp_spa[0], aptr->arp_spa[1]);

	
	/* new MAC? */
	if (!(hash_arp_tmp = g_hash_table_lookup(arp_hash, mac_tmp))) {
	    /* new MAC -> creating mutex */
	    /* pth_mutex_acquire(&mutex, 0, NULL); */
	    
	    if (!opt.daemon)
		printf("new MAC (%s) (IP=%s)\n", mac_tmp, ipv4_tmp);

	    /* creating mem for new hash-entry */ 
	    if (!(value_arp_tmp = malloc(sizeof(hash_value_arp_t)))) {
		Perror("can not alloc mem");
		continue; /* skip MAC */	
	    }		
			
	    /* copy mac_tmp to new entry */
	    if (!(value_arp_tmp->mac = strdup(mac_tmp))) {
		Perror("can not alloc mem");
		continue; /* skip MAC */	
	    }

	    /* create ipv4-hash-table */
	    value_arp_tmp->ipv4_hash = g_hash_table_new(g_str_hash, g_str_equal); 
	    
	    if (!(value_ipv4_tmp = malloc(sizeof(hash_value_ip_t)))) {
		Perror("can not alloc mem");
		continue; /* skip MAC */	
	    }			    
	    
	    inet_aton(ipv4_tmp, &(value_ipv4_tmp->ipv4));
	    value_ipv4_tmp->arp = value_arp_tmp;
	    
	    value_arp_tmp->last_seen = value_ipv4_tmp->last_seen = time(NULL);

	    /* insert into ip-hash */
	    g_hash_table_insert(value_arp_tmp->ipv4_hash, ipv4_tmp, value_ipv4_tmp);
	    
	    /* insert into arp-hash */
	    g_hash_table_insert(arp_hash, value_arp_tmp->mac, value_arp_tmp);
	   
	    
	    /* TEST LOOKUP */
	    job.mac = mac_tmp;
	    job.ipv4 = value_ipv4_tmp->ipv4;
	    lookup(&job);
	    
		    
	    /* release mutex */
	    /* pth_mutex_release(&mutex); */
	} else {
	    /* pth_mutex_acquire(&mutex, 0, NULL); */
	
	    hash_arp_tmp = g_hash_table_lookup(arp_hash, mac_tmp);
	    time_tmp = time(NULL);
	    
	    /* update last_seen? */
	    time_diff = time_tmp - ((hash_value_arp_t*)hash_arp_tmp)->last_seen;
	    if (time_diff >= LAST_SEEN_UP) {
		if (!opt.daemon)
		    printf("MAC in hash. Updating time\n");
		((hash_value_arp_t*)hash_arp_tmp)->last_seen = time_tmp;
		
		g_hash_table_insert(arp_hash, mac_tmp, hash_arp_tmp);
	    }
			
	    /* pth_mutex_release(&mutex); */
	}
    }

    pcap_close(pcap);
    pthread_exit(NULL);
}	
