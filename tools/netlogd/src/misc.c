/* $Id: misc.c 404 2004-08-25 21:20:48Z docfx $ */

/* Copyright (C) 2004 by Soeren Bleikertz <soeren@openfoo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <pcap.h>

#include "misc.h"

extern pcap_t *pcap;

void usage(void)
{
    printf("Usage: netlogd [option(s)]\n" \
	    " The options are:\n" \
	    "  -f\t\tDo not go into background\n" \
	    "  -d [dir]\tChange CWD to [dir]\n" \
	    "  -c [file]\tConfig-file\n" \
	    "  -i [int]\tInterface to listen for MACs\n" \
	    "  -n [nr]\tMaximal count of lookup-threads\n" \
	    "  -p [port]\tPort to liste for FLIP\n" \
	    "  -a [ip]\tAccept data from [ip]\n" \
	    "  -h\t\tDisplay this information\n" \
	    "  -V\t\tDisplay version number\n\n" \
	    "http://flip.sf.net - soeren[at]openfoo.org\n"
    );
}


void sig_clean(int sig)
{
    /*
    struct pcap_stat stat;
    pcap_stats(pcap, &stat);
	
    printf("\nstats: %d, %d\n", stat.ps_recv, stat.ps_drop);

    pcap_close(pcap);
    */
    exit(0);
    // do cleanup
}
