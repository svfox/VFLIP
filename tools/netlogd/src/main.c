/* $Id: main.c 404 2004-08-25 21:20:48Z docfx $ */

/* Copyright (C) 2004 by Soeren Bleikertz <soeren@openfoo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pthread.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <string.h>

#include "main.h"
#include "misc.h"
#include "arpwatch.h"
#include "server.h"


extern char *optarg;
extern int optind;


option_t opt;


int main(int argc, char **argv)
{
    char c;
    pid_t pid;
    pthread_t arpw_thread[MAX_ARPW];
    int i;
	

    /* set default */
    opt.dir = ".";
    opt.conf = "netlogd.conf";
    opt.interface = NULL;
    opt.daemon = 1;
    opt.max_th = 10;
    opt.port = 1234;
    memset(&(opt.ipv4), 0, sizeof(struct in_addr));


    /* init */
    signal(SIGINT, sig_clean);
    signal(SIGTERM, sig_clean);
    signal(SIGKILL, sig_clean);	

	
    /* Parsing Options */
    while ((c = getopt(argc, argv, "fd:c:i:n:p:a:hV")) != -1) {
	switch(c) {
	    /* dont daemonize */
	    case 'f':
		opt.daemon = 0;
		break;
	    /* change dir */	
	    case 'd':	
		printf("DEBUG: dir: %s\n", optarg);
		opt.dir = optarg;
		break;
	    /* config-file */	
	    case 'c':
		printf("DEBUG: config: %s\n", optarg);
		opt.conf = optarg;
	    break;
	    /* interface */
	    case 'i':
		printf("DEBUG: interface: %s\n", optarg);
		opt.interface = optarg;
		break;
	    /* max threads */
	    case 'n':
		printf("DEBUG: max threads: %i\n", atoi(optarg));
		opt.max_th = atoi(optarg);
		break;
	    /* port for server */
	    case 'p':
		printf("DEBUG: port: %i\n", atoi(optarg));
		opt.port = atoi(optarg);
		break;
	    /* port for server */
	    case 'a':
		printf("DEBUG: accept IP: %s\n", optarg);
		inet_aton(optarg, &(opt.ipv4));
		break;
	    /* print version */	
	    case 'V':
		printf("netlogd "VERSION"\n" \
			"http://flip.sf.net - soeren[at]openfoo.org\n");
		exit (0);
	    break;
	    /* unknown option, print usage */	
	    case '?':
	    case 'h':
	    default:
		usage();
		exit(0);
	}
    }


    /* Going into Daemon-Mode */
    if (opt.daemon) {
	if ((pid = fork()) < 0)
	    return -1;
	else if (pid != 0) {
	    printf("netlogd running in daemon-mode (pid: %i).\n", pid);
	    exit(0);
	}	

	setsid();
	chdir(opt.dir);
	umask(0);
    }	


    /* creating arpwatch-threads */
    if (!opt.daemon)
        printf("creating arpwatch-thread(s):");
	
    for (i=0; i<MAX_ARPW; i++) {
	pthread_create(&arpw_thread[i], NULL, arpwatch, NULL);
	if (!opt.daemon)
	    printf(" %i", i+1);
    }
    if (!opt.daemon)
	printf("\n");

    
    /* start server */
    srv_start();  
   

    /* exit */
    pthread_exit(NULL);
	
    return 0;
}
