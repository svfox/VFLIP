/* $Id: ethcodes.c 404 2004-08-25 21:20:48Z docfx $ */

/* Copyright (C) 2004 by Soeren Bleikertz <soeren@openfoo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "ethcodes.h"
#include "main.h"
#include "error.h"


extern option_t opt;
size_t mmap_len;


char *ec_open(char *file)
{
    int fd;
    char *fmap;
    struct stat st;

    /* open file */
    if ((fd = open(file, O_RDONLY, 0)) == -1) {
	Perror("can not open eth-file");
	return NULL;
    }
	    
    /* stat file */
    if (fstat(fd, &st) == -1) {
	Perror("can not stat file");
	return NULL;
    }

    mmap_len = st.st_size;

    /* map file */
    if ((fmap = mmap(NULL, mmap_len, PROT_READ, MAP_PRIVATE, fd, 0)) == (caddr_t)-1) {
	Perror("can not map file");
	return NULL;
    }	

    close(fd);
    
    return fmap;
}


char *ec_lookup(char *mac)
{
    char *file, *tmp, *tmp2, vendor_mac[9], *vendor;
    int i=0, size;
    
    
    /* map file */
    if (!(file = ec_open(ETHFILE)))
	return NULL;
    
    
    /* extract vendor-type from MAC */
    tmp = mac;
    
    while (i < 3) {
	if (*tmp++ == ':')
	    i++;
    }
    
    size = (tmp-mac);
    
    if (size > sizeof(vendor_mac))
	return NULL;
    
    memcpy(vendor_mac, mac, size-1);
    vendor_mac[size-1] = 0;
    

    /* lookup .dat-file */
    tmp = strstr(file, vendor_mac)+strlen(vendor_mac)+1;
    tmp2 = tmp;
    i = 0;
    
    while (*tmp2++ != '\n')
	i++;
    
    if ((vendor = malloc(i+1)) == NULL) {
	Perror("can not alloc mem");
	return NULL;
    }
    
    memcpy(vendor, tmp, i);
    vendor[i] = 0;

    munmap(file, mmap_len);
    
    return vendor;
}
