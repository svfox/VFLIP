/* $Id: queue.c 404 2004-08-25 21:20:48Z docfx $ */

/* Copyright (C) 2004 by Soeren Bleikertz <soeren@openfoo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <glib.h>
#include <stdlib.h>
#include <string.h>

#include "queue.h"
#include "error.h"


GSList *job_queue = NULL;


void q_push(queue_t *entry)
{
    job_queue = g_slist_append(job_queue, entry);
}


queue_t *q_pop(void)
{
    queue_t *entry, *tmp;

    if ((entry = malloc(sizeof(queue_t))) == NULL) {
	Perror("can not alloc mem");
	return NULL;
    }
    
    tmp = g_slist_nth_data(job_queue, 0);

    memcpy(entry, tmp, sizeof(queue_t));
    
    job_queue = g_slist_remove(job_queue, tmp);
    
    return entry;
}


int q_size(void)
{
    return g_slist_length(job_queue);
}
