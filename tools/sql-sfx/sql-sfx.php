<?php

/**
* dieser script erlaubt es, aus einem datenbankdump einen ausführbaren 
* php-script zu machen, der diesen in eine datenbank einspielt. außerdem 
* kann das php-script in einzelne teile mit einer maximalen dateigröße 
* zerlegt werden.
*/

$MaxFileSize = 2048 * 1024; 
$Input = "db.sql";
$FileName = "sql%02d.php";

set_time_limit(0);
GenConfig();
ExecFile($Input);
Query("",true);

function ExecFile($File)
{
  $cmds = array();
  $i = 0;
  foreach(file($File) as $line)
  {
    $line = trim($line);
    if($line{0} == "#") continue;
    
    $c .= "$line ";
    if($line{strlen($line)-1} == ";")
    {
      Query($c);
      if(($i++ % 20) == 0) echo ".";
      $c = "";
    }  
  }
  echo "\nEs wurden $i Befehle erfolgreich ausgefuehrt.\n";
}

function GenConfig()
{
  $f = fopen("sql.php","w");
  fputs($f,"<?php\n");
  fputs($f,"mysql_connect(\"localhost\",\"root\",\"\");\n");
  fputs($f,"mysql_select_db(\"flip\");\n");
  fputs($f,"?>\n");
  fclose($f);
}

function Query($q,$close = false)
{
  global $MaxFileSize,$FileName;
  static $file;
  static $index, $pos = 0;  
  
  $x = addcslashes(trim($q),"'\\");
  $x = "mysql_query('$x');\n";

  if(($pos + strlen($q) > $MaxFileSize) or $close)
  {
    fputs($file,"\n?>");
    fclose($file);
    $file = false;
    $pos = 0;
  } 

  if(!empty($q))
  {  
    if(!$file)
    {
      $file = fopen(sprintf($FileName,$index),"w");
      fputs($file,"<?php\n");
      fputs($file,"include(\"sql.php\");\n");
      $index++;
    }
    fputs($file,$x);
    $pos += strlen($x);
  }
}

?>