<?php

$CfgLogFile = "./access.log";

$CfgMysqlServer = "localhost";
$CfgMysqlUser   = "root";
$CfgMysqlPass   = "";
$CfgMysqlDB     = "flip";

main();

function main()
{
  global $argv;
  set_error_handler("errorHandler");
  $stdin = fopen('php://stdin', 'r'); 
  $user = trim(fgets($stdin));
  $pass = trim(fgets($stdin));
  authUser($user,$pass,$argv[1]);
  break;
}

function checkPass($Password,$Crypted)
{
  if($Crypted == md5($Password)) return true;
  if($Crypted == crc32($Password)) return true;
  if($Crypted == crypt($Password)) return true;
  $q = mysqlQuery("SELECT PASSWORD('".addslashes($Password)."')");
  if($Crypted == array_pop($q[0])) return true;
  return false;
}

function authUser($Ident,$Password,$Right)
{
  $i = addslashes($Ident);
  $r = mysqlQuery("
    SELECT d.val,u.name,u.id FROM flip_user_subject u, flip_user_column c, flip_user_data d
      WHERE (
        ((u.id = '$i') OR (u.name = '$i') OR (u.email = '$i')) AND
        (c.name = 'password') AND (c.type = 'user') AND
        (u.id = d.subject_id) AND
        (d.column_id = c.id)
      );
  ");
  $id = $r[0]["id"];
  $n = $r[0]["name"]." [$id]";
  if(!checkPass($Password,$r[0]["val"])) return doLog("Login fehlgeschlagen für $Ident",1);
  if(empty($Right)) return doLog("Login erfolgreich für $n",0);
  
  $q = mysqlQuery("
    SELECT `parent_id` FROM `flip_user_groups` WHERE (`child_id` = '$id');
  ");
  $ids = array($id);
  foreach($q as $v) $ids[] = $v["parent_id"];
  
  $r = addslashes($Right);
  $q = mysqlQuery("
    SELECT COUNT(s.id) AS `count` FROM flip_user_rights s, flip_user_right r
      WHERE (
        (r.right = '$r') AND 
        (r.id = s.right_id) AND
        (s.owner_id IN (".implode(",",$ids)."))
      )
  ");
  if($q[0]["count"] > 0) doLog("Authentifizierung von $n erfolgreich für Recht $Right",0);
  else doLog("Authentifizierung von $n fehlgeschlagen für Recht $Right",1);
}

function mysqlQuery($Query)
{
  static $db;
  if(!$db)
  {
    global $CfgMysqlServer, $CfgMysqlUser, $CfgMysqlPass, $CfgMysqlDB;
    $db = mysql_connect($CfgMysqlServer,$CfgMysqlUser,$CfgMysqlPass);
    mysql_select_db($CfgMysqlDB);
  }
  $q = mysql_query(trim($Query),$db);
  if(!$q) doLog("MysqlFehler: ".mysql_error($db),1);
  $r = array();
  while($i = mysql_fetch_assoc($q)) $r[] = $i;
  return $r;
}

function doLog($aMsg,$Exit = NULL)
{
  global $CfgLogFile;
  static $f;
  $s = date("r")." $aMsg (IP:$_ENV[IP] Host:$_ENV[HTTP_HOST] URI:$_ENV[URI])\n";
  echo $s;
  if(!$f) $f = fopen($CfgLogFile,"a");
  fputs($f,$s);  
  if(!is_null($Exit)) exit($Exit);
}

function errorHandler($no)
{
   if($no != E_NOTICE) doLog(var_export(array_splice(func_get_args(),0,4),true),1);
}

?>