Diese Datei gibt wieder, wie weit sich die einzelnen Pakete bereits an die Konventionen halten. Alle Pakete, die hier eingetragen werden, wurden Zeile für Zeile überprüft.

(Die Ausgabekonventionen sind an dieser Stelle noch nicht relevant)


Paket           Datenbank-  Pageklassen-      paketextern    externe namen       logging-  config-
                konform     Name ist korrekt  namenskonform  in membervariablen  konform   konform

archiv.php         X            X                 X                 X               X         X
barcode.php        X            X                 X                 X               X         X
calender.php       X            X                 X                 X               X         X
catering.php       X            X                 X                 X               X         X
checkininfo.php    X            X                 X                 X               X         X
config.php         X            X                 X                 X               X         X 
content.php        X            X                 X                 X               X         X
dbxfer.php         X            X                 X                 X               X         X
forum.php          X            X                 X                 X               X         X
image.php          X            X                 X                 X               X         X
konten.php         X            X                 X                 X               X         X
lanparty.php       X            X                 X                 X               X         X
log.php            X            X                 X                 X               X         X
menu.php           X            X                 X                 X               X         X
netlog.php         X            X                 X                 X               X         X
news.php           X            X                 X                 X               X         X
poll.php           X            X                 X                 X               X         X
seats.php          X            X                 X                 X               X         X
sendmessage.php    X            X                 X                 X               X         X
server.php         X            X                 X                 X               X         X
sponsor.php        X            X                 X                 X               X         X
text.php           X            X                 X                 X               X         X
ticket.php         X            X                 X                 X               X         X
tournament.php     X            X                 X                 X               X         X
turnieradm.php     X            X                 X                 X               X         X
user.php           X            X                 X                 X               X         X
webmessage.php     X            X                 X                 X               X         X

dns.php          [! legacy / unsupported !]
mfzpage.php      [! legacy / unsupported !]
rawdata.php      [! legacy / unsupported !]


countdown.php    [! verschoben nach lanparty.php?frame=countdown !]
dbimport.php     [! verschoben nach dbxfer.php !]
messaging.php    [! umbenannt in webmessage.php !]
sidebar.php      [! ist veraltet, aktuell in lanparty.php?frame=sidebar !]
xml.dbexport.php [! verschoben nach dbxfer.php !]
viewlog.php      [! verschoben nach log.php !]
ulticket.php     [! verschoben nach ticket.php !]
