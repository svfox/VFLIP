<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: log.php 1451 2007-08-16 11:58:10Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class LogPage extends Page {
	var $ViewRight = "log_view";

	function frameViewDBLog($get) {
		include_once ("mod/mod.lastvisit.php");
		global $User;
		
		$User->requireRight($this->ViewRight);
		
		$this->ShowContentCell = false;

		$types = MysqlReadCol("SELECT `type` FROM `" . TblPrefix() . "flip_log_log` GROUP BY `type`;", "type");
		
		if (!in_array('change', $types)) {
			$types[] = 'change';
		}

		$type = (empty ($get["type"])) ? "change" : addslashes($get["type"]);
		$orderby = (empty ($get["orderby"])) ? "`time` DESC" : addslashes((($get["orderby"] == "user") ? "u.name" : "l.$get[orderby]") . " " . $get["order"]);
		$limit = (empty ($get["limit"])) ? "0,100" : addslashes($get["limit"]);
		
		$items = MysqlReadArea(
			"SELECT l.*, u.name AS `user` " 
				. "FROM (`" . TblPrefix() . "flip_log_log` l) " 
				. "LEFT JOIN `" . TblPrefix() . "flip_user_subject` u " 
				. "ON (u.id = l.user_id) " 
				. "WHERE (l.type = '$type') " 
				. "ORDER BY $orderby " 
				. "LIMIT $limit;"
		);
		
		$this->Caption = ucfirst($type) . 'log';
		return array (
			'type' => $type, 
			'foundrows' => MysqlReadField(
				"SELECT COUNT(`id`) FROM `" . TblPrefix() . "flip_log_log` " 
					. "WHERE (`type` = \"$type\")"
			), 
			'types' => $types, 
			'items' => $items, 
			'last_visit' => doVisit('log_' . $type)
		);
	}

	function frameBackTrace($get, $post) {
		$this->ShowContentCell = false;
		$this->Caption = 'BackTrace';
		ArrayWithKeys($post, array('data'));
		$msg = uncondense($post['data']);
		if(is_object($msg)) {
			foreach ($msg->BackTrace as $k => $v)
				$msg->BackTrace[$k]['args'] = wordwrap(var_export($v['args'], true), 110);
			//print_r($msg->Args);
			return array (
				'trace' => array_reverse($msg->BackTrace),
				'error' => $msg->Error,
				'file' => $msg->File,
				'line' => $msg->Line,
				'message' => $msg->Message,
				'debug' => wordwrap($msg->Debug, 120),
				'args' => $msg->Args //wordwrap(var_export($msg->Args,true),120)
			);
		} else {
			trigger_error_text('Keine g&uuml;ltigen Daten', E_USER_ERROR, __FILE__,__LINE__);
		}
	}

}

RunPage("LogPage");
?>
