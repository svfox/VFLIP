<?php

/**
 * @author Daniel Raap
 * @version $Id: turnieradm.php 1499 2018-05-08 12:18:40Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ('inc/inc.text.php');

class StatisticPage extends Page {
	var $onlineconfig = "statistic_numberonlineusers";
	var $lastseenconfig = "statistic_onlinemaxlastseen";
	var $showidleconfig = "statistic_usersonline_show_idle";

	function frameonline() {
		$this->Caption = "User online";
		$limit = (integer) ConfigGet($this->onlineconfig);
		$maxlastseen = ConfigGet($this->lastseenconfig);
		$showidle = (ConfigGet($this->showidleconfig) == "Y") ? true : false;
		$maxage = time() - ($maxlastseen * 60);

		/**$count = MysqlReadField("SELECT COUNT(*) FROM `".TblPrefix()."flip_session_data`
		                         WHERE time_lastseen > '".(time()-$maxlastseen*60)."'", "COUNT(*)");*/
		$lasttime = (ConfigGet($this->showidleconfig) == "Y") ? "MAX(d.time_lastseen)" : "MAX(d.time_create)";
		$users = MysqlReadArea("
		      SELECT s.name, d.user_id, COUNT(d.user_id) AS count, $lasttime AS `last`
		        FROM " . TblPrefix() . "flip_session_data d LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON s.id=d.user_id
		          WHERE d.time_lastseen > '$maxage'
		          GROUP BY d.user_id
		          ORDER BY s.name;
		      ", "user_id");

		// wenn niemand online ist, wird nichts ausgegeben.
		if (empty ($users))
			return true;

		// user rausfiltern, die in ihrem profil das anzeigen in der onlineliste deaktiviert haben
		$ids = implode_sqlIn(array_keys($users));
		$hiddencount = 0;
		$hidden = GetSubjects("user", array (
			"id",
			"statistic_hide_status"
		), "s.id IN ($ids)");
		foreach ($hidden as $id => $dat) {
			if ($dat["statistic_hide_status"] == 'Y') {
				unset ($users[$id]);
				$hiddencount++;
			}
		}

		// wenn es zu viele User sind, wird der Rest abgeschnitten und darauf hingewiesen.
		if (count($users) > $limit) {
			$islimit = true;
			$users = array_slice($users, 0, $limit);
		} else
			$islimit = false;

		$guests = 0;
		foreach ($users as $k => $u)
			if (empty ($u["user_id"]) or ($u["name"] == "Anonymous")) {
				$guests += $u["count"];
				unset ($users[$k]);
			} else
				$users[$k]["time"] = round((time() - $u["last"]) / 60);

		return array (
			"islimit" => $islimit,
			"users" => $users,
			"guests" => $guests,
			"maxlastseen" => $maxlastseen,
			"hiddencount" => $hiddencount,
			
		);
	}

	function frameRegisteredUsers($get) {
		global $User, $CoreConfig;
		$User->requireRight('user_admin_subjects');
		
		$this->Caption = 'neue Benutzer';
		
		// wonach wird gesucht?
		// TODO &uuml;bersetzungen ber&uuml;cksichtigen
		$newPrefix = 'neuer Account erstellt f&uuml;r ';
		$activPrefix = 'Der Account von ';
		$activPostfix = ' wurde aktiviert.';
		
		//wo wird das ActionLog gespeichert?
		$log = explode('|',$CoreConfig['log_action']);
		$created = array();
		$activated = array();
		if(in_array("db", $log)) {
			// Datenbank abfragen
			$created = MysqlReadArea("SELECT `message`, `time` FROM `".TblPrefix()."flip_log_log` WHERE `message` LIKE '".escape_sqlData_without_quotes($newPrefix)."%' AND type='action'");
			$activated = MysqlReadArea("SELECT `message`, `time` FROM `".TblPrefix()."flip_log_log` WHERE `message` LIKE '%".escape_sqlData_without_quotes($activPostfix)."' AND type='action'");
			if(!is_array($created)) $created = array();
			if(!is_array($activated)) $activated = array();
		} else {
			// Datei parsen
			$filename = array_shift($log);
			if(empty($filename) || !is_file($filename))
			{
				trigger_error_text(text_translate('Es gibt kein Action-Log!'), E_USER_ERROR);
				return false;
			}
			
			$file = fopen($filename, 'r');
			$maxsize = 10*1024*1024; // Bytes
			if(filesize($filename) > $maxsize)
			{
				// nur die letzten $maxsize Bytes auslesen
				fseek($file, -$maxsize, SEEK_END);
			}
			while(!feof($file))
			{
				$line = fgets($file);
				if(strpos($line, $newPrefix) !== false)
				{
					$created[] = $this->_actionLogMessage($line);
				} else if(strpos($line, $activPostfix) !== false)
				{
					$activated[] = $this->_actionLogMessage($line);
				}
			}
		}
		
		$created = array_reverse($created);
		$user = array();
		foreach($created AS $entry)
		{
			$line = $entry['message'];
			$name = str_replace($newPrefix, '', $line);
			$user[$name] = array('name'=>$name, 'time'=>$entry['time']);
		}
		foreach($activated AS $entry)
		{
			$line = $entry['message'];
			$name = str_replace($activPostfix, '', $line);
			$name = str_replace($activPrefix, '', $name);
			$user[$name]['aktiv'] = 1;
		}
		return array('user'=>$user);
	}
	
	/**
	 * Liefert den Text einer Zeile aus dem ActionLog
	 * @param String $line Logzeile
	 * @return String
	 */
	function _actionLogMessage($line) {
					$msg = preg_split('/ action/', $line);
					$dateAndSID = substr(array_shift($msg), 0, 17); // Datum und Session vorne abschneiden
					return array('message'=>trim(implode(' action', $msg)), 'time'=>strtotime($dateAndSID));
	}
}

RunPage("StatisticPage");
