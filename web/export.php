<?php
/**
 * 
 * @author Moritz Eysholdt
 * @version $Id: calender.php 1702 2019-01-09 09:01:12Z docfx $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

	
class ExportPage extends Page {
	
	//php 7 public function __construct()
	//php 5 original function ExportPage() {
	function __construct() {

		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		
		global $User;
		$User->requireRight('admin');
	}
	
	function frameDefault($GetVars, $PostVars) {
		return array ();
	}

	function frameCSV($get) {
		$g = new Group("status_paid");
		$ids = implode_sqlIn(array_keys($g->getChilds()));
		$cols = array ("id", "name", "givenname", "familyname", "clan");
		$users = GetSubjects("user", $cols, "s.id in ($ids)");
		$seats = MysqlReadArea("SELECT `user_id`,`name`,`ip` FROM `".TblPrefix()."flip_seats_seats` WHERE (`reserved` = 'Y');", "user_id");

		header("Content-Type: text/comma-separated-values"); //alternativ: "text/plain"
		header("Content-Disposition: attachment; filename=flip_userexport_".count($users)."_users.csv");

		// echo "# Userexport\r\n";
		// echo "# ".count($users)." User\r\n";
		echo implode(", ", $cols).", sitz, ip, block, platz\r\n";
		foreach ($users as $x => $v) {
			$a = array();
			$v["seat"] = $seats[$v["id"]]["name"];
			$v["ip"] = $seats[$v["id"]]["ip"];
			preg_match("/Block (\d) (\w\d{1,2})/i", $v["seat"], $a);
			$v["block"] = $a[1];
			$v["platz"] = $a[2];
			$users[$x] = $v;
		}

		function cmp($a, $b) {
			return strnatcasecmp($a["seat"], $b["seat"]);
		}

		usort($users, "cmp");

		foreach ($users as $v) {
			foreach ($v as $k => $w)
				$v[$k] = addcslashes($w, "\"\r\n");
			echo "\"".implode("\",\"", $v)."\"\r\n";
		}
		//DisplayErrors();
		return true;
	}
	
	function frameCSVGroup($getVars) {
		$groupname = $getVars['name'];
		
		$g = new Group("status_" . $groupname);
		
		$ids = implode_sqlIn(array_keys($g->getChilds()));
		// Read the required cols		
		$cols = MysqlReadCol('SELECT `name`, `val_type` FROM `flip_user_column` WHERE type = \'user\' AND `required` = \'Y\'', 'val_type', 'name');
		
		// Add the id col
		$cols = array_merge(array('id' => 'integer'), $cols, array('status' => 'string'));
		
		// Get the user values
		$users = GetSubjects("user", array_keys($cols), "s.id in ($ids)");

		header("Content-Type: text/comma-separated-values"); //alternativ: "text/plain"
		header("Content-Disposition: attachment; filename=flip_userexport_".count($users)."_users.csv");

		// echo "# Userexport\r\n";
		// echo "# ".count($users)." User\r\n";
		echo implode(",", array_keys($cols))."\r\n";

		foreach ($users as $user) {
			$line = array();
			
			foreach ($cols as $colName => $colType) {
				$value = $user[$colName];
				
				if($colType == 'Date') {
					$value = date('d.m.Y h:i', $value);
				}
				
				$value = '"' . addcslashes($value, "\"\r\n") . '"';				
				$line[] = $value;
			}
			
			echo implode(',', $line) . "\r\n";
		}
		
		return true;		
	}
	
	/**
	 * Exportiert alle Gruppenmitglieder der Gruppe $get['id']
	 * im Outlook/Adressbuch-Format.
	 * @param mixed $get Frame-Vars
	 */
	
	// Vorname;Nachname;Rufname;Geburtstag;E-Mail-Adresse;Stra&szlig;e (privat);
	// Ort (privat);Postleitzahl (privat);Rufnummer (privat);Mobiltelefon;
	// Webseite (privat);Kommentare
	
	function frameGroup2Outlook($get) {
		global $User;
		//$User->requireRight($this->_ExportGroupsRight);
		$g = CreateSubjectInstance($get['id'], 'group');
		// Darf der User das?
		if(!$User->hasRightOver('user_view_members',$g)) {
			trigger_error_text('Du bist nicht berechtigt, die Gruppenmitglieder einzusehen und kannst daher auch keinen CSV-Export machen!',E_USER_ERROR);
			return '';			
		}
		// Nur Name, Vorname, Nick und die &ouml;ff. Spalten exportieren
		$g_pub = $g->getProperty('public_member_columns');
		if(empty($g_pub)) { // Keine &ouml;ff. Spalten = kein Export
			trigger_error_text('Diese Gruppe hat keine &ouml;ffentlichen Daten, die exportiert werden k&ouml;nnten!',E_USER_ERROR);
			return '';
		}
		$p = array_merge(explode(';', $g_pub),array('id','email','name','type'));
		$p = array_unique($p); // Evtl. Duplikate entfernen
		$ids = implode_sqlIn(array_keys($g->getChilds()));
		// Export-Format (Win-Adressbuch-Bezeichnungen)
		$format = array('givenname' => 'Vorname','familyname' => 'Nachname','name' => 'Rufname',
					  'birthday' => 'Geburtstag','email' => 'E-Mail-Adresse','street' => 'Stra&szlig;e (privat)',
					  'city' => 'Ort (privat)','plz' => 'Postleitzahl (privat)','tele' => 'Rufnummer (privat)',
					  'mobil' => 'Mobiltelefon','homepage' => 'Webseite (privat)','description' => 'Kommentare',
					  'type' => 'FLIP-Subjekttyp', 'id' => 'FLIP-User-ID');
		$details = GetColumnDeteils('user');
		foreach($p as $col) {
			$col = trim($col);
			if(!empty($col)) {
				$csvheader[] = isset($format[$col]) ? '"'.$format[$col].'"' : '"'.$details[$col]['caption'].'"';
				$cols[] = $col;
			}
		}
		$users = GetSubjects('user', $cols, 's.id in ('.$ids.')');
		header('Content-Type: text/comma-separated-values'); //alternativ: 'text/plain'
		header('Content-Disposition: attachment; filename=flip_groupexport_'.$g->name.'.csv');
		echo implode(';',$csvheader)."\r\n";
		foreach($users as $k => $v) {
			if(isset($users[$k]['birthday']))
				$users[$k]['birthday'] = date('d.m.Y', $users[$k]['birthday']);
		}
		echo formatCSVdata($users,';');
		return true;
	}

	function _mergeSQL($prefix, $cols) {
		$r = array ();
		foreach ($cols as $k => $v) {
			if (is_numeric($k))
				$r[] = "$prefix.$v";
			else
				$r[] = "$prefix.$k AS `$v`";
		}
		return implode(",", $r);
	}

	function _exportCSV($List) {
		echo "\"".implode("\",\"",array_keys($List[0]))."\"\r\n";
		foreach ($List as $v) {
			foreach ($v as $k => $w)
				$v[$k] = addcslashes($w, "\"\r\n");
			echo "\"".implode("\",\"", $v)."\"\r\n";
		}
	}
	
	function _addTshGateway($List) {
		foreach($List as $k => $v) {
			$a = explode(".",$v["ip"]);
			$a[3] = 254;
			$List[$k]["gateway"] = implode(".",$a);			
		}		
		return $List;
	}

	function frameSeats($get) {
		$seat_cols = array ("name" => "seat", "ip");
		$block_cols = array ("caption" => "seat_block");
		$user_cols = array ("name", "familyname", "givenname", "clan"); // gateway

		$this->saveAs("flip_seats_export-".date("YmdHis").".csv");
		$scols = $this->_mergeSQL("s", $seat_cols);
		$bcols = $this->_mergeSQL("b", $block_cols);
		$from = "`".TblPrefix()."flip_seats_seats` s, `".TblPrefix()."flip_seats_blocks` b WHERE (s.block_id=b.id) ORDER BY b.caption, s.name;";
		$seats = MysqlReadArea("SELECT $scols,$bcols,IF(s.reserved='Y',s.user_id,0) AS user_id FROM $from");
		$seats = AddSubjectProperties("user", $seats, "user_id", $user_cols);
		$seats = $this->_addTshGateway($seats);
		$this->_exportCSV($seats);
		return true;
	}

}

RunPage("ExportPage");
?>