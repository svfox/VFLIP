<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: ticket.php 1702 2019-01-09 09:01:12Z docfx $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once('core/core.php');
require_once('inc/inc.page.php');
require_once('inc/inc.text.php');

class SysinfoPage extends Page {
	// Rights
	var $ViewRight = "sysinfo_view";
	
	// Texts
	var $textModulesDescription = 'sysinfo_modules';
	
	// Modules
	var $flipRequiredMods = array('gd');
	var $flipOptionalMods = array('mcrypt');

	//php 7 public function __construct()
	//php 5 original function SysinfoPage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$User->requireRight($this->ViewRight);
	}

	function frameDefault($get, $post) {
		$this->Caption = 'Systeminfos';
		ob_start();
		phpinfo();
		$r = ob_get_contents();
		ob_end_clean();
		list (, $r) = explode("<body>", $r, 2);
		list ($r) = explode("</body>", $r);
		return array (
			"phpinfo" => $r
		);
	}
	
	function frameExtensions($get, $post) {
		$ex = get_loaded_extensions();
		$r = array();
		
		//Sortieren
		sort($ex);
		
		foreach($ex as $name) {
			$ex_name = strtolower($name);
			
			if(in_array($ex_name, $this->flipRequiredMods)) {
				$r['extensions'][] = array(
					'required' => true,
					'name' => $ex_name,
					'present' => 'Vorhanden'
				);				
			} elseif(in_array($ex_name, $this->flipOptionalMods)) {
				$r['extensions'][] = array(
					'required' => false,
					'name' => $ex_name,
					'present' => 'Vorhanden'
				);				
			} else {
				$r['extensions_else'][] = array(
					'name' => $ex_name
				);
			}
		}
		
		$r['description'] = LoadText($this->textModulesDescription, $this->Caption);
		
		return $r;
	}
}

RunPage("SysinfoPage");
?>