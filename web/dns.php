<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: dns.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class DnsPage extends Page {
	var $Cfg;

	//php 7 public function __construct()
	//php 5 original function DnsPage()
	function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		global $User;
		$User->requireRight("admin");
		/*$this->Cfg = ConfigRead("dns");
		if(!$this->Cfg) $this->Cfg = ConfigCreate("dns",array(
		  array("key" => "user_domain",     "val" => "xxx.de", "desc" => "Die Domain-Zone f&uuml;r die User-Domains."),
		  array("key" => "server_domain",     "val" => "xxx.de", "desc" => "Die Domain-Zone f&uuml;r die Server-Domains."),
		));*/
		$this->Cfg['user_domain'] = ConfigGet('dns_user_domain');
		$this->Cfg['server_domain'] = ConfigGet('dns_server_domain');
	}

	function getNames() {
		$dns = array ();
		include_once ("mod/mod.user.php");
		
		if(empty($this->Cfg['user_domain'])) {
			trigger_error('Es wurde keine Domain f&uuml;r die User in der <a href="config.php#dns">Config</a> angegeben!<br/>\nVerwende statt dessen "example.com"',E_USER_WARNING);
			$this->Cfg['user_domain'] = 'example.com';
		}
		if(empty($this->Cfg['server_domain'])) {
			trigger_error('Es wurde keine Domain f&uuml;r die Server in der <a href="config.php#dns">Config</a> angegeben!<br/>\nVerwende statt dessen "example.com"',E_USER_WARNING);
			$this->Cfg['server_domain'] = 'example.com';
		}
		
		//alle SitzPl&auml;tze
		foreach (MysqlReadArea("SELECT `user_id`,`name` FROM `" . TblPrefix() . "flip_seats_seats` WHERE ((`reserved` = 'Y') AND (`user_id` != 0));") as $dat)
			$dns[] = array (
				"owner_id" => $dat["user_id"],
				"ip" => UserGetIP($dat["user_id"]
			), "name" => preg_replace("/[^0-9a-z\-]/i", "-", strtolower($dat["name"])) . "." . $this->Cfg["user_domain"]);

		foreach (GetSubjects("server", array (
				"id",
				"name",
				"owner_id",
				"ip"
			)) as $dat) {
			$dat["name"] = preg_replace("/[^0-9a-z\-]/i", "-", strtolower($dat["name"])) . "." . $this->Cfg["server_domain"];
			$dns[] = $dat; 
		}
			
		// pruefen auf leer -> abbruch
		if (empty ($dns))
			return array ();
		$ids = array ();
		foreach ($dns as $v)
			$ids[] = $v["owner_id"];
		$ids = MysqlReadCol("SELECT `id`,`name` FROM `" . TblPrefix() . "flip_user_subject` WHERE (`id` IN (" . implode_sqlIn($ids) . "));", "name", "id");
		foreach ($dns as $k => $v)
			$dns[$k]["owner"] = isset($ids[$v["owner_id"]]) ? $ids[$v["owner_id"]] : text_translate('fehlender User').' (ID '. $v['owner_id'] .')';

		return $dns;
	}

	function frameDefault($get, $post) {
		$this->Caption = "DNS-Config";
		return array (
		"dns" => $this->getNames());
	}

	function frameDnsUpdate() {
		header("Content-Type: plain/text");
		header("Content-Disposition: attachment; filename=dnsconfig.txt");
		$this->ShowMenu = false;
		return array (
		"dns" => $this->getNames());
	}
}

RunPage("DnsPage")
?>
