<?php

/**
 * @author Matthias Gro&szlig;
 * @version $Id: paypal.php 1284 2006-07-22 20:31:57Z scope $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/
 

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class PayPalPage extends Page {

	var $PayPalViewRight = "paypal_view";
	var $PayPalAdminRight = "paypal_admin";

	function frameDefault($a, $post) {
		$this->ShowMenu = false;
		if ($a["mode"] == "ipn") {
			include_once ("mod/mod.paypal.ipn.php");
			
			if(!empty($_POST)) 
				LogAction("PayPal-IPN abgefangen!");

			// Variablen &uuml;bergeben
			$handler = new FLIPPayPalHandler($_POST);
			
			// IPN Verarbeiten...
			$handler->ProcessIPN();
			
			return array (); // n&ouml;tig, damit das FLIP sauber 'beendet'
		} else {
			$prot = ($_SERVER["HTTPS"] == "on") ? "https" : "http";
			$port = ($_SERVER["SERVER_PORT"] != 80) ? ":$_SERVER[SERVER_PORT]" : "";
			$sname = $_SERVER["SERVER_NAME"];
			$script = $_SERVER["PHP_SELF"];
			header("Location: $prot://$sname$port$script?frame=pay");
			return array ();
		}
	}

	function framePay($g) {
		global $User;

		$this->ShowMenu = ($g["type"] == "embedded") ? false : true;
		$r["embedded"] = !$this->ShowMenu;
		$r["embed_href"] = ($r["embedded"]) ? "&type=embedded" : "";

		if (!$User->hasRight($this->PayPalAdminRight)) {
			if (!$User->hasRight($this->PayPalViewRight) && $r["embedded"]) {
				$r["err_noview"] = LoadText("paypal_no_access", $this->Caption);
				return $r;
			} else {
				$User->requireRight($this->PayPalViewRight);
			}
		}

		$r["text"] = LoadText("paypal_pay", $this->Caption);
		$r["userid"] = $User->id;
		$r["usernick"] = $User->name;
		$r["edit_right"] = ($User->hasRight($this->PayPalAdminRight)) ? $this->PayPalAdminRight : "";

		$buttons = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_paypal_buttons`;", "id");

		if (!empty ($buttons)) {
			foreach ($buttons as $b) {
				foreach ($b as $f => $v) {
					$r["buttons"][$b["id"]][$f] = $v;
					if ($f == "amount") {
						$r["buttons"][$b["id"]]["amount_f"] = sprintf("%01.2f", $v);
					}
				}
			}
		}
		return $r;
	}

	function frameEdit($g) {
		global $User;
		$User->requireRight($this->PayPalAdminRight);

		if (empty ($g["id"])) {
			$g["id"] = "new";
		}
		$this->Caption = ($g["id"] == "new") ? "Button hinzuf&uuml;gen" : "Button bearbeiten";
		$r["embedded"] = ($g["type"] == "embedded") ? true : false;
		if ($g["id"] == "new") {
			$prot = ($_SERVER["HTTPS"] == "on") ? "https" : "http";
			$port = ($_SERVER["SERVER_PORT"] != 80) ? ":$_SERVER[SERVER_PORT]" : "";
			$sname = $_SERVER["SERVER_NAME"];
			$script = $_SERVER["PHP_SELF"];
			$r["notify_url"] = "$prot://$sname$port$script?mode=ipn";
			$script = str_replace("paypal.php", "text.php", $script);
			$r["return"] = "$prot://$sname$port$script?name=paypal_paid";
			$r["cancel_return"] = "$prot://$sname$port$script?name=paypal_cancelled";
			$r["ppurl"] = "https://www.paypal.com/cgi-bin/webscr";
			$r["amount"] = "1.00";
			$r["business"] = "<Die bei PayPal registrierte Adresse des Geldempf&auml;ngers>";
			$r["item_name"] = "Neuer Button";
		} else {
			$button = MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_paypal_buttons` WHERE `id`='$g[id]';", true);
			if (!empty ($button)) {
				foreach ($button as $f => $v) {
					$r[$f] = $v;
				}
				$r["amount"] = sprintf("%01.2f", $r["amount"]);
			} else {
				trigger_error_text("Der Button mit der ID $g[id] existiert nicht!", E_USER_ERROR);
			}
		}
		return $r;
	}

	function submitButton($g) {
		global $User;
		$User->RequireRight($this->PayPalAdminRight);
		srand((double) microtime() * 1000);
		$g["item_number"] = rand(); // = Zufallszahl, da unwichtig
		$g["quantity"] = 1;         // 1, niemand bestellt 2 Tickets auf einmal...
		$g["amount"] = str_replace(",", ".", $g["amount"]); // 35,50 -> 35.50

		if (MysqlWriteByID(TblPrefix() . "flip_paypal_buttons", $g, $g["id"])) {
			LogAction("Der PayPal-Button '$g[item_name]' wurde hinzugef&uuml;gt!");
			return true;
		} else {
			trigger_error_text("Ein PayPal-Button konnte nicht erstellt werden!", E_USER_ERROR);
			return false;
		}
	}

	function actionDeleteButton($a) {
		if (MysqlDeleteByID(TblPrefix() . "flip_paypal_buttons", $a["buttonid"])) {
			trigger_error_text("Der Button wurde erfolgreich gel&ouml;scht!", E_USER_NOTICE);
			$this->NextPage = "paypal.php?frame=pay";
			return true;
		} else {
			trigger_error_text("Der Button konnte nicht gel&ouml;scht werden!", E_USER_WARNING);
			$this->NextPage = "paypal.php?frame=pay";
			return false;
		}
	}


}

RunPage("PayPalPage");
?>
