<?php

/**
 * @author Daniel Raap
 * @version $Id: konten.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class KontenPage extends Page {
	var $EditRight = "konten_admin";
	var $ViewRight = "konten_view";
	var $Eintrittconfig = "banktransfer_money_amount";
	var $Eintrittswaehrungsconfig = "banktransfer_currency";

	//php 7 public function __construct()
	//php 5 original function KontenPage()
	function __construct() {
		$this->Caption = "Konten&uuml;bersicht";
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
	}

	function framedefault($get, $post) {
		global $User;
		if (!$User->hasRight($this->EditRight))
			$User->requireRight($this->ViewRight);
		ArrayWithKeys($get, array("kid"));
		$i = 0;

		$tpl["list"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_konten_list`", "id");
		if (!empty ($tpl["list"])) {
			if (empty ($get["kid"])) {
				//standardm&auml;&szlig;ig erstes Konto anzeigen
				$listcopy = $tpl["list"];
				$firstelement = array_shift($listcopy);
				unset ($listcopy);
				$get["kid"] = $firstelement["id"];
				$this->Caption = $firstelement["title"];
			} else {
				$this->Caption = $tpl["list"][$get["kid"]]["title"];
			}
			$tpl["list_id"] = $get["kid"];
			$tpl["title"] = $this->Caption;
			$kosten = array();
			$summe = 0;
			foreach (MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_konten_data` WHERE list_id=".escape_sqlData($get["kid"])." ORDER BY date DESC") AS $auszug) {
				$kosten[$i]["date"] = date("d.m.y", $auszug["date"]);
				$kosten[$i]["text"] = $auszug["text"];
				$kosten[$i]["id"] = $auszug["id"];
				$kosten[$i]["betrag"] = number_format($auszug["value"], 2, ",", ".") . "&nbsp;&euro;";
				$summe += $auszug["value"];
				$i++;
			}
			$tpl["sum"] = number_format($summe, 2, ",", ".") . "&nbsp;&euro;";
			$tpl["price"] = ConfigGet($this->Eintrittconfig);
			$tpl["users"] = round($summe / $tpl["price"]);
			$tpl["currency"] = ConfigGet($this->Eintrittswaehrungsconfig);
			$saldo = MysqlReadField("SELECT SUM(value) FROM " . TblPrefix() . "flip_konten_data", "SUM(value)");
			$tpl["saldo"] = number_format($saldo, 2, ",", ".") . "&nbsp;&euro;";
		} else {
			$kosten[$i]["date"] = "Kein Konto ausgew&auml;hlt.";
			$kosten[$i]["betrag"] = "Bitte Konto anlegen.";
		}
		$tpl["kosten"] = $kosten;

		if ($User->hasRight($this->EditRight)) {
			$tpl["edit"] = "true";
		}

		return $tpl;
	}

	function actioninsert($post) {
		global $User;
		$User->requireRight($this->EditRight);
		if (empty ($post["kid"]) || empty ($post["text"]))
			return trigger_error_text("Die &uuml;bermittelten Daten waren unvollst&auml;ndig.", E_USER_WARNING);
		if (empty ($post["value"]))
			$post["value"] = 0;
		$post["value"] = str_replace(",", ".", $post["value"]);
		if (empty ($post["date"]) || $post["date"] == "yyyy-mm-dd") {
			$post["date"] = time();
		} else {
			if (($post["date"] = strtotime($post["date"])) === -1)
				return trigger_error_text("Falsches Datumsformat.", E_USER_WARNING);
		}
		if (empty ($post["id"]))
			$post["id"] = 0;
		if ($post["id"] == 0)
			$actiontext = "erstellt";
		else
			$actiontext = "bearbeitet";
		if (MysqlWriteByID(TblPrefix() . "flip_konten_data", array (
				"list_id" => $post["kid"],
				"date" => $post["date"],
				"value" => $post["value"],
				"text" => $post["text"]
			), $post["id"], "Kontoeintrag konnte nicht in die DB eingetragen werden."))
			return LogAction("Im Konto " . MysqlReadFieldByID(TblPrefix() . "flip_konten_list", "title", $post["kid"]) . " (ID " . $post["kid"] . ") wurde \"" . $post["text"] . "\" $actiontext.");
		else
			return trigger_error_text("Fehler beim Einf&uuml;gen der Daten.|ID=" . $post["id"], E_USER_WARNING);
	}

	function actionadd($post) {
		global $User;
		$User->requireRight($this->EditRight);
		if (MysqlWriteByID(TblPrefix() . "flip_konten_list", array (
				"title" => escape_sqlData($post["ktitle"]
			))))
			return LogChange("Das <b>Konto</b> " . escapeHtml($post["ktitle"]) . " wurde erstellt.");
		else
			return trigger_error_text("Konto wurde nicht erstellt.", E_USER_WARNING);
	}

	function submitdel($post) {
		global $User;
		$User->requireRight($this->EditRight);
		if (MysqlDeleteByID(TblPrefix() . "flip_konten_data", $post["id"]))
			return LogChange("Der <b>Kontoeintrag</b> \"" . escapeHtml($post["ktext"]) . "\" wurde gel&ouml;scht.");
		else
			return trigger_error_text("Kontoeintrag konnte nicht gel&ouml;scht werden.", E_USER_WARNING);
	}

	function submitremkonto($post) {
		global $User;
		$User->requireRight($this->EditRight);
		if (MysqlExecQuery("DELETE FROM " . TblPrefix() . "flip_konten_data WHERE list_id='" .escape_sqlData_without_quotes($post["kid"]) . "'")) {
			unset ($this->FrameVars["kid"]);
			if (MysqlDeleteByID(TblPrefix() . "flip_konten_list", $post["kid"]))
				return LogChange("Das <b>Konto</b> \"" . escapeHtml($post["ktitle"]) . "\" wurde gel&ouml;scht.");
		}
		return trigger_error_text("Kontoeintrag konnte nicht gel&ouml;scht werden.", E_USER_WARNING);
	}

	function frameedit($get) {
		global $User;
		$User->requireRight($this->EditRight);
		$this->Caption = "Eintrag bearbeiten";
		$data = MysqlReadRowByID(TblPrefix() . "flip_konten_data", $get["id"]);
		$data["date"] = date("Y-m-d", $data["date"]);
		return $data;
	}
}

RunPage("KontenPage");
?>
