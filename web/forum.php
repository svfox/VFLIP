<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: forum.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.page.php');
require_once ('mod/mod.lastvisit.php');
require_once ('mod/mod.forum.watches.php');

class ForumPage extends Page {
	// normale rechte
	var $AdminForumRight = 'forum_admin';

	// rechte &uuml;ber ein forum
	var $ViewRight = 'forum_view';
	var $PostRight = 'forum_post';
	var $ModRight = 'forum_moderate';
	var $AllowHtml = 'forum_post_html';
	var $AllowFlip = 'forum_post_fliptags';

	var $UserAdminRight = 'user_admin_rights';
	var $EditPageRight = 'edit_page_rights';

	// rechte &uuml;ber einen user
	var $EditRight = 'forum_edit_own_posts';

	// config
	var $MaxPostLengthConfig = 'forum_max_post_length';
	var $PostsPerPageConfig = 'forum_postsperpage';
	var $AdditionalColumnsConfig = 'forum_additionaluserdata';
	var $AddinfoSpacer = 'forum_additionaluserdata_spacer';
	var $HTMLSignaturesConfig = 'forum_signatures_html_for';
	
	// Watches
	
	var $WatchesHandler;
	
	//php 7 public function __construct()
	//php 5 original function ForumPage()
	function __construct() {
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		$this->WatchesHandler = new ForumWatchesHandler();
	}

	function getForumID($ID, $Type = 'forum') {
		$id = addslashes($ID);
		switch ($Type) {
			case ('forum') :
				return $ID;
			case ('thread') :
				return MysqlReadField('SELECT `forum_id` FROM `'.TblPrefix()."flip_forum_threads` WHERE (`id` = '$id');");
			case ('post') :
				return MysqlReadField('SELECT t.forum_id FROM `'.TblPrefix().'flip_forum_threads` t, `'.TblPrefix()."flip_forum_posts` p WHERE ((p.id = '$id') AND (p.thread_id = t.id));");
			default :
				return trigger_error_text("Der Type ist ung&uuml;ltig.|Type:$Type (erlaubt:forum,thread,post)", E_USER_ERROR);
		}
	}

	function getForum($ID, $Type = 'forum', $Right = '') {
		global $User;
		$id = $this->getForumID($ID, $Type);
		$f = CreateSubjectInstance($id, 'forum');
		if (!empty ($Right))
			$User->requireRightOver($Right, $f);
		return $f;
	}

	function getPostsPerPage() {
		// wenn empty($r) kommt es im template zu einer division by zero.
		$r = ConfigGet($this->PostsPerPageConfig);
		if (empty ($r))
			return 20;
		return $r;
	}

	function frameDefault($get, $post) {
		global $User;
		$this->Caption = 'Foren';
		$this->ShowContentCell = false;
		$g = MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_forum_groups` ORDER BY `sort_index`', 'id');

		$user = $lastvisit = array ();
		foreach (GetSubjects('forum', array (), '', 'sort_index') as $f)
			if ($User->hasRightOver($this->ViewRight, $f['id'])) {
				$g[$f['group_id']]['forums'][] = $f;
				if (!empty ($f['last_post_user']))
					$user[$f['last_post_user']] = 1;
				$lastvisit[$f['id']] = "forum_$f[id]";
			}
		$lastvisit = getVisits($lastvisit);
		if (count($user) > 0)
			$user = MysqlReadCol('SELECT `name`,`id` FROM `'.TblPrefix().'flip_user_subject` WHERE (`id` IN ('.implode_sqlIn(array_keys($user)).'));', 'name', 'id');
		$hasnew = false;
		
		foreach ($g as $k => $v)
			if (!(isset($v['forums']) && is_array($v['forums']) ))
				unset ($g[$k]);
			else
				foreach ($v['forums'] as $f => $h) {
					$g[$k]['forums'][$f]['last_poster'] = (isset($user[$h['last_post_user']])) ? $user[$h['last_post_user']] : 0;
					$g[$k]['forums'][$f]['last_visit'] = (isset($lastvisit[$h['id']])) ? $lastvisit[$h['id']] : 0;
					$g[$k]['forums'][$f]['img'] = (!isset($lastvisit["forum_$h[id]"]) || $lastvisit["forum_$h[id]"] < intval($h['last_post_time'])) ? $hasnew = 'images/balloons_new.png' : 'images/balloons_old.png';
				}
		include_once ('mod/mod.config.php');
		return array ('groups' => $g, 'admin' => $User->hasRight($this->AdminForumRight), 'hasnew' => (empty ($hasnew)) ? false : true, 'configurl' => (ConfigCanEdit()) ? 'config.php?category=forum' : '');
	}

	function frameViewForum($get) {
		global $User;
		$f = $this->getForum($get['id'], 'forum', $this->ViewRight);
		$r = $f->getProperties();
		doVisit("forum_$r[id]");
		$this->Caption = 'Forum -> '.$r['name'];
		$r['right_post'] = $User->hasRightOver($this->PostRight, $f);
		$r['right_loggedin'] = $User->hasRight('logged_in');

		$limit = (empty ($get['limit'])) ? '0,50' : addslashes($get['limit']);
		$r['threads'] = MysqlReadArea('
		      SELECT t.*, s.name AS `last`, s.id AS `last_id`, g.name AS `poster`, g.id AS `poster_id`, IF('.$User->sqlHasRight('t.post_right').",'unlocked','locked') AS `locked`
		        FROM (`".TblPrefix().'flip_forum_threads` t)
		          LEFT JOIN `'.TblPrefix().'flip_user_subject` s ON (s.id = t.last_poster)
		          LEFT JOIN `'.TblPrefix().'flip_user_subject` g ON (t.poster_id = g.id)
		          WHERE (t.forum_id = \''.$f->id."')
		          ORDER BY t.sticky DESC,t.last_change DESC
		          LIMIT $limit;
		    ");
		$r['foundrows'] = MysqlReadField('SELECT COUNT(`id`) FROM `'.TblPrefix()."flip_forum_threads` WHERE (`forum_id` = '{$f->id}');");

		// letzte besuchszeiten auslesen 
		$visits = array ();
		foreach ($r['threads'] as $k => $v)
			$visits[] = 'thread_'.$v['id'];
		$visits = getVisits($visits);

		$ppp = $this->getPostsPerPage();
		$hasnew = false;
		foreach ($r['threads'] as $k => $v) {
			$v['last_visit'] = $visits['thread_'.$v['id']];
			if ($v['posts'] > $ppp) {
				$a = array ();
				$max = ceil($v['posts'] / $ppp) - 1;
				for ($i = 1; $i < $max; $i ++)
					$a[$i +1] = 'limit='. ($i * $ppp).",$ppp";
				$a['end'] = 'limit='. ($max * $ppp).",$ppp";
				$r['threads'][$k]['pages'] = $a;
			}
			$r['threads'][$k]['img'] = 'images/balloon_'. (($v['sticky']) ? 'sticky' : 'unsticky')."_$v[locked]_". (($v['last_visit'] >= $v['last_change']) ? 'old' : $hasnew = 'new').'.png';
		}
		if (!empty ($hasnew)) {
			$i = 'images/forum_markread24.png';
			$i = getTplFileDir($i).$i;
			$lnk = EditURL(array ('action' => 'setread', 'forum_id' => $f->id));
			$r['newlink'] = "<a href=\"$lnk\" title=\"".text_translate('als gelesen markieren')."\"><img src=\"$i\" width=\"24\" height=\"14\" alt=\"read\" border=\"0\" /></a>";
		} else
			$r['newlink'] = '';
			
		if(ConfigGet('watches_enabled') == 'Y') {
			$r['watching'] = $this->WatchesHandler->IsWatching('forum', $get['id']);
			$r['watches_enabled'] = true;
		} else
			$r['watches_enabled'] = false;
		
		return $r;
	}

	function framePost($get) {
		global $User;
		$this->Caption = 'Forum -> Beitrag erstellen';
		if (!empty ($get['id'])) {
			$r = MysqlReadRow('
			        SELECT p.*, s.name AS `user` FROM (`'.TblPrefix().'flip_forum_posts` p)
			          LEFT JOIN `'.TblPrefix().'flip_user_subject` s ON (p.poster_id = s.id) 
			          WHERE (p.id = \''.addslashes($get['id']).'\');
			      ');
			$get['thread_id'] = $r['thread_id'];
		} else {
			$r = array ('allow_nl2br' => 1, 'allow_fliptags' => 0);
			$r['user'] = $User->name;
		}

		if (empty ($get['thread_id'])) {
			$r['title'] = '';
			$r['forum_id'] = $get['forum_id'];
			$r['edittitle'] = true;
			$f = CreateSubjectInstance($get['forum_id'], 'forum');
		} else {
			$id = addslashes($get['thread_id']);
			$i = MysqlReadRow('SELECT `title`,`forum_id`,`post_right` FROM `'.TblPrefix()."flip_forum_threads` WHERE (`id` = '$id');");
			$f = CreateSubjectInstance($i['forum_id'], 'forum');
			if (!$User->hasRightOver($this->ModRight, $f))
				$User->requireRight($i['post_right']);
			$r['title'] = $i['title'];
			$r['thread_id'] = $get['thread_id'];
			if (MysqlReadField('SELECT COUNT(*) FROM '.TblPrefix()."flip_forum_posts WHERE thread_id='$id'", 'COUNT(*)') === '1')
				$r['edittitle'] = true;
			else
				$r['edittitle'] = false;
		}

		$User->requireRightOver($this->PostRight, $f);
		if (!empty ($get['id']))
			if (!$User->hasRightOver($this->EditRight, $r['poster_id']) and !$User->hasRightOver($this->ModRight, $f))
				trigger_error_text('Es ist nicht erlaubt ohne Sonderrechte fremde Posts zu editieren.', E_USER_ERROR);

		$r['right_fliptags'] = $User->hasRightOver($this->AllowFlip, $f);
		$r['right_html'] = $r['allow_html'] = $User->hasRightOver($this->AllowHtml, $f);
		return $r;
	}

	function isMineAndLatest($PostID) {
		global $User;
		$id = addslashes($PostID);
		$dat = MysqlReadRow('SELECT `poster_id`, `post_time`, `thread_id` FROM `'.TblPrefix()."flip_forum_posts` WHERE (`id` = '$id');");
		if ($dat['poster_id'] != $User->id)
			return false;
		$latest = MysqlReadField('SELECT MAX(`post_time`) FROM `'.TblPrefix()."flip_forum_posts` WHERE (`thread_id` = '$dat[thread_id]');");
		if ($latest != $dat['post_time'])
			return false;
		return true;
	}

	function submitPost($data) {
		global $User;
		if (empty ($data['thread_id']))
			$f = $this->getForum($data['forum_id'], 'forum', $this->PostRight);
		else
			$f = $this->getForum($data['thread_id'], 'thread', $this->PostRight);

		if (!$User->hasRightOver($this->AllowFlip, $f))
			$data['allow_fliptags'] = 0;
		if (!$User->hasRightOver($this->AllowHtml, $f))
			$data['allow_html'] = 0;

		if (empty ($data['id'])) {
			$data['poster_id'] = $User->id;
			$data['post_time'] = time();
			$do = 'erstellt';
		} else {
			if ($this->isMineAndLatest($data['id'])) {
				$data['poster_id'] = $User->id;
				$data['post_time'] = time();
			} else {
				$data['changer_id'] = $User->id;
				$data['change_time'] = time();
			}
			$do = 'bearbeitet';
			if (!$User->hasRightOver($this->EditRight, MysqlReadField('SELECT `poster_id` FROM `'.TblPrefix()."flip_forum_posts` WHERE (`id` = '".addslashes($data['id'])."')")) and !$User->hasRightOver($this->ModRight, $f))
				trigger_error_text('Es ist nicht erlaubt ohne Sonderrechte fremde Posts zu editieren.', E_USER_ERROR);
		}
		$data['text'] = trim($data['text']);
		$maxlen = ConfigGet($this->MaxPostLengthConfig);
		if (strlen($data['text']) > $maxlen) {
			trigger_error_text("Der von dir eingegebene Text &uuml;berschreitet die maximale L&auml;nge von $maxlen Zeichen. Bitte k&uuml;rze deinen Text oder teile ihn in mehrere Posts auf.", E_USER_WARNING);
			return false;
		}

		if (empty ($data['thread_id'])) {
			$data['thread_id'] = MysqlWriteByID(TblPrefix().'flip_forum_threads', array ('title' => $data['title'], 'forum_id' => $data['forum_id'], 'poster_id' => $User->id));
			
			// Watches handeln...
			$this->WatchesHandler->CheckWatches('forum',$data['forum_id'],'threadcreate',array('forum' => $f,'newthread' => $data['title']));
			
			$this->NextPage = 'forum.php?frame=viewthread&id='.$data['thread_id'];
		} else {
			// Watches handeln...
			$ac = ($do == 'erstellt') ? 'postadd' : 'postmodify';
			$this->WatchesHandler->CheckWatches('thread',$data['thread_id'],$ac,array('forum' => $f));
			$User->requireRight(MysqlReadField('SELECT `post_right` FROM `'.TblPrefix()."flip_forum_threads` WHERE (`id` = '".addslashes($data['thread_id'])."');"));
		}

		doVisit('thread_'.$data['thread_id']);
		doVisit('forum_'.$f->id);
		
		unset ($data['user']);
		unset ($data['title']);
		unset ($data['forum_id']);

		$id = MysqlWriteByID(TblPrefix().'flip_forum_posts', $data, $data['id']);
		LogAction("der Post mit der ID $id wurde im Forum {$f->name} $do.");

		$this->threadSetLastPost($data['thread_id']);
		$f->setLastPost();
		return true;
	}

	function actionSetRead($post) {
		global $User;
		if (empty ($post['forum_id'])) {
			$fids = array ();
			foreach (GetSubjects('forum', array ('id')) as $f)
				if ($User->hasRightOver($this->ViewRight, $f['id']))
					$fids[] = $f['id'];
		} else
			$fids = array ($post['forum_id']);

		// alle foren als gelesen markieren.
		foreach ($fids as $f)
			forceVisit("forum_$f");

		// alle threads als gelesen markieren.
		$oldest = LastvisitOldest();
		$fids = implode_sqlIn($fids);
		$threads = MysqlReadCol('SELECT `id` FROM `'.TblPrefix()."flip_forum_threads` WHERE (`last_change` > $oldest) AND (`forum_id` IN ($fids));", 'id');
		foreach ($threads as $t)
			forceVisit("thread_$t");

		// user zur letzten seite zur&uuml;ckschicken.
		Redirect($_SERVER[HTTP_REFERER]);
		$this->_ShowFrame = false;
	}

	function actionGetNew($post) {
		$tid = addslashes($post['thread_id']);
		$visit = getVisits(array ("thread_$tid"));
		$visit = $visit["thread_$tid"];
		$posttimeandcount = MysqlReadRow('
		      SELECT MIN(`post_time`) as `time`, COUNT(*) as `count`
		        FROM `'.TblPrefix()."flip_forum_posts`
		        WHERE ((`thread_id` = '$tid') AND (`post_time` > '$visit'));
		    ");
		$posttime = $posttimeandcount['time'];
		if (empty ($posttime))
			$anch = 'post_last';
		else
			$anch = 'post_'.MysqlReadField('SELECT `id` FROM `'.TblPrefix()."flip_forum_posts` WHERE ((`thread_id` = '$tid') AND (`post_time` = '$posttime'));");
		//get page
		$followingpostcount = $posttimeandcount['count'];
		$postcount = MysqlReadField('
		      SELECT COUNT(*)
		      FROM `'.TblPrefix()."flip_forum_posts`
		      WHERE `thread_id` = '$tid'
		    ", 'COUNT(*)');
		$postbefore = $postcount - $followingpostcount;
		$ppp = $this->getPostsPerPage();
		$last = ceil(($postbefore) / $ppp) - 1;
		if($last < 0) $last = 0;
		$limit = ($last * $ppp).",$ppp";
		
		header('Location: '.EditURL(array ('frame' => 'viewthread', 'id' => $tid, 'limit' => $limit, 'action' => '', 'thread_id' => ''), '', false)."#$anch");
		$this->_ShowFrame = false;
	}

	function threadSetLastPost($id) {
		$id = addslashes($id);
		$time = MysqlReadField('SELECT MAX(`post_time`) FROM `'.TblPrefix()."flip_forum_posts` WHERE (`thread_id` = '$id');");
		if (empty ($time))
			$poster = '';
		else
			$poster = MysqlReadField('SELECT `poster_id` FROM `'.TblPrefix()."flip_forum_posts` WHERE ((`post_time` = '$time') AND (`thread_id` = '$id'));");
		MysqlWriteByID(TblPrefix().'flip_forum_threads', array ('posts' => MysqlReadField('SELECT COUNT(`id`) FROM `'.TblPrefix()."flip_forum_posts` WHERE (`thread_id` = '$id');"), 'last_change' => $time, 'last_poster' => $poster), $id);

	}

	function actionDeleteThread($post) {
		$f = $this->getForum($post['id'], 'thread', $this->ModRight);
		$id = addslashes($post['id']);
		
		// Watches handeln...
		$this->WatchesHandler->CheckWatches('thread',$post['id'],'threaddelete',array('forum' => $f));
		
		MysqlWrite('DELETE FROM `'.TblPrefix()."flip_forum_threads` WHERE (`id` = '$id');");
		MysqlWrite('DELETE FROM `'.TblPrefix()."flip_forum_posts` WHERE (`thread_id` = '$id');");
		LogAction("Der Thread mit der ID $id wurde gel&ouml;scht.");
		$f->setLastPost();
	}

	function actionDeletePost($post) {
		$id = addslashes($post['id']);
		$f = $this->getForum($post['id'], 'post', $this->ModRight);
		$t = MysqlReadField('SELECT `thread_id` FROM `'.TblPrefix()."flip_forum_posts` WHERE (`id` = '$id')");
		
		// Watches handeln...
		$this->WatchesHandler->CheckWatches('thread',$t,'postdelete',array('forum' => $f));
		
		MysqlWrite('DELETE FROM `'.TblPrefix()."flip_forum_posts` WHERE (`id` = '$id');");
		LogAction("Der Post mit der ID $id wurde gel&ouml;scht.");
		$this->threadSetLastPost($t);
		$f->setLastPost();
	}

	function actionSticky($post) {
		$f = $this->getForum($post['id'], 'thread', $this->ModRight);

		// Watches handeln...
		$s = ($post['val']) ? 'sticky' : 'unsticky';
		$this->WatchesHandler->CheckWatches('thread',$post['id'],'sticky',array('forum' => $f, 'sticky' => $s));

		MysqlWriteByID(TblPrefix().'flip_forum_threads', array ('sticky' => $post['val']), $post['id']);
		LogAction('Der Thread mit der ID '.$post['id'].' wurde als '. (($post['val']) ? 'sticky' : 'unsticky').' markiert.');
	}

	function submitPostRight($post) {
		$f = $this->getForum($post['id'], 'thread', $this->ModRight);

		// Watches handeln...
		if($post['right'] > 0)
			$this->WatchesHandler->CheckWatches('thread',$post['id'],'right',array('forum' => $f, 'right' => GetRightName($post['right'])));
		elseif($post['right'] < 0)
			$this->WatchesHandler->CheckWatches('thread',$post['id'],'lock',array('forum' => $f, 'lock' => true));
		elseif($post['right'] == 0)
			$this->WatchesHandler->CheckWatches('thread',$post['id'],'lock',array('forum' => $f, 'lock' => false));

		if (MysqlWriteByID(TblPrefix().'flip_forum_threads', array ('post_right' => $post['right']), $post['id']))
			return LogAction('Dem Thread mit der ID '.$post['id'].' wurde das Postrecht '.$post['right'].' gegeben.');
		return false;
	}

	function submitMoveThread($post) {
		global $User;

		if (empty ($post['forum']))
			trigger_error_text('Die Forum ID wurde nicht angegeben.', E_USER_ERROR);
		$source = $this->getForum($post['id'], 'thread', $this->ModRight);
		$target = $this->getForum($post['forum'], 'forum', $this->ViewRight);
		$this->SubmitMessage = 'Der Thread wurde verschoben.';
		
		// Watches handeln...
		$this->WatchesHandler->CheckWatches('thread',$post['id'],'threadmove',array('targetforum' => $target));
		
		if (MysqlWriteByID(TblPrefix().'flip_forum_threads', array ('forum_id' => $post['forum']), $post['id'])) {
			$source->setLastPost();
			$target->setLastPost();
			return LogAction('Der Thread mit der ID '.$post['id'].' wurde in das Forum mit der ID '.$post['forum'].' verschoben.');
		}
		return false;
	}

	function submitRenameThread($post) {
		$f = $this->getForum($post['id'], 'thread', $this->ModRight);
		
		// Watches handeln...
		$this->WatchesHandler->CheckWatches('thread',$post['id'],'threadrename',array('forum' => $f, 'newname' => $post['title']));

		if (MysqlWriteByID(TblPrefix().'flip_forum_threads', array ('title' => $post['title']), $post['id']))
			return LogAction('Dem Thread mit der ID '.$post['id'].' wurde in "'.$post['title'].'" umbenannt.');
			
		return false;
	}

	function getUserProperties($list, $idkey, $cols) {
		$ids = array ();
		foreach ($list as $v)
			$ids[$v[$idkey]] = 1;
		if (empty ($ids))
			return array ();
		$ids = implode_sqlIn(array_keys($ids));
		$where = "s.id IN ($ids)";
		array_unshift($cols, 'id');
		return GetSubjects('user', $cols, $where);
	}

	function interpret_bb($text) {
	   // MySQL-Table mit den BB-Codes f&uuml;r freiz&uuml;giges ersetzen?
	   
		$text = str_replace('[b]', '<b>', $text);
		$text = str_replace('[/b]', '</b>', $text);
		
		$text = str_replace('[i]', '<i>', $text);
		$text = str_replace('[/i]', '</i>', $text);
		
		$text = str_replace('[u]', '<u>', $text);
		$text = str_replace('[/u]', '</u>', $text);
		
		$text = str_replace('[quote]', '<div class="quote">', $text);
		$text = str_replace('[/quote]', '</div>', $text);
		
		return $text;
	}

	function frameViewThread($get) {
		global $User;
		$this->ShowContentCell = false;
		$r = MysqlReadRowByID(TblPrefix().'flip_forum_threads', $get['id']);
		$f = $this->getForum($r['forum_id'], 'forum', $this->ViewRight);
		$r['last_visit'] = doVisit('thread_'.$r['id']);
		doVisit('forum_'.$f->id);
		$this->Caption = 'Forum -> '.$f->name.' -> '.$r['title'];
		$r['forum_name'] = $f->name;
		$r['posts_per_page'] = $this->getPostsPerPage();
		$r['right_post'] = ($User->hasRightOver($this->PostRight, $f) and $User->hasRight($r['post_right'])) ? true : false;
		$r['right_mod'] = $User->hasRightOver($this->ModRight, $f);
		if ($r['right_mod'])
			//make switching between locked/unlocked easier with opposite default value
			switch ($r['post_right']) {
				case '0' :
					$r['post_setright'] = -1;
					break;
				case '-1' :
					$r['post_setright'] = 0;
					break;
				default :
					$r['post_setright'] = $r['post_right'];
			}

		$limit = (empty ($get['limit'])) ? '0,'.$r['posts_per_page'] : addslashes($get['limit']);
		$id = addslashes($get['id']);
		$posts = MysqlReadArea('
		      SELECT p.*,s.name AS `poster`, t.name AS `changer` FROM (`'.TblPrefix().'flip_forum_posts` p)
		        LEFT JOIN `'.TblPrefix().'flip_user_subject` s ON (p.poster_id = s.id)
		        LEFT JOIN `'.TblPrefix()."flip_user_subject` t ON (p.changer_id = t.id)
		        WHERE (p.thread_id = '$id')
		        ORDER BY p.post_time
		        LIMIT $limit;
		    ");
		$r['foundrows'] = MysqlReadField('SELECT COUNT(`id`) FROM `'.TblPrefix()."flip_forum_posts` WHERE (`thread_id` = '$id');");

		$r['maxwidth']  = ConfigGet('forum_avatars_maxwidth');
		$r['maxheight'] = ConfigGet('forum_avatars_maxheight');
		$allow_avatars  = ConfigGet('forum_avatars_allow');

		$addinfocols = explode(';', ConfigGet($this->AdditionalColumnsConfig));
		$userprops = $this->getUserProperties($posts, 'poster_id', array_merge($addinfocols, array ('forum_signature')));
		$showsignature = ((ConfigGet('forum_signature_show') == 'Y') and ($f->getProperty('signature_allow') == 'Y')) ? true : false;
		$htmlsigs = ConfigGet($this->HTMLSignaturesConfig);
		foreach ($posts as $k => $v) {
			$addinfo = array ();
			foreach ($addinfocols as $col)
				if (!empty ($userprops[$v['poster_id']][$col]))
					$addinfo[] = escapeHtml($userprops[$v['poster_id']][$col]);

			$posts[$k]['addposterinfo'] = implode(ConfigGet($this->AddinfoSpacer), $addinfo);
			$posts[$k]['editable'] = $User->hasRightOver($this->EditRight, $v['poster_id']);
			
			if($allow_avatars == 'Y') {
				$u_subj = CreateSubjectInstance($v['poster_id'],'user');
				if(!isset($u_imgs[$v['poster_id']]))
					$u_imgs[$v['poster_id']]  = $u_subj->getProperties(array('user_image'));
				$posts[$k]['user_img'] = (!empty($u_imgs[$v['poster_id']]['user_image'])) ? $u_imgs[$v['poster_id']]['user_image'] : '';
			}

			$txt = $v['text'];
			if (!$v['allow_html'])
				$txt = escapeHtml($txt);
			if ($v['allow_nl2br'])
				$txt = nl2br($txt);

			$txt = $this->interpret_bb($txt);

			$posts[$k]['text'] = $txt;

			if ($showsignature) {
				$sig = $userprops[$v['poster_id']]['forum_signature'];
				if (!empty ($sig))
					switch ($htmlsigs) {
						case 'alle' :
							break;
						case 'html-poster' :
							$u = new User(array ('id' => $v['poster_id'], 'name' => $v['poster'], 'type' => 'user'));
							if (!$u->hasRightOver($this->AllowHtml, $f))
								$sig = escapeHtml($sig);
							break;
						case 'niemand' :
						default :
							$sig = escapeHtml($sig);
					}
				$posts[$k]['signature'] = $sig;
			}
		}
		$r['posts'] = $posts;
		if ($r['right_mod']) {
			$r['stickyval'] = ($r['sticky']) ? '0' : '1';
			$r['stickycapt'] = ($r['sticky']) ? 'unsticky' : 'sticky';
			$r['right'] = 'right';
			$r['right_rights'] = ($User->hasRight($this->UserAdminRight) or $edit = $User->hasRight($this->EditPageRight)) ? true : false;
			$r['foren'] = array ();
			$groups = MysqlReadCol('SELECT `id`,`caption` FROM `'.TblPrefix().'flip_forum_groups`;', 'caption', 'id');
			foreach (GetSubjects('forum', array ('id', 'name', 'group_id')) as $id => $f)
				if ($User->hasRightOver($this->ViewRight, $id))
					$r['foren'][$id] = $groups[$f['group_id']] .'->'. $f['name'];
			uasort($r['foren'], 'strnatcasecmp');
		}
		
		if(ConfigGet('watches_enabled') == 'Y') {
			$r['watches_enabled'] = true;
			if(!$this->WatchesHandler->IsWatching('forum', $r['forum_id']))
				$r['watching'] = $this->WatchesHandler->IsWatching('thread', $get['id']);
			else
				$r['watching'] = true;	
		} else
			$r['watches_enabled'] = false;
			
		return $r;
	}

	/*****************************************************************************/
	/*                       Admin-Bereich                                       */
	/*****************************************************************************/

	function frameViewGroups() {
		global $User;
		$this->Caption = "Forengruppen";
		$User->requireRight($this->AdminForumRight);
		include_once ('inc/inc.text.php');
		$g = MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_forum_groups` ORDER BY `sort_index`', 'id');

		$last = 0;
		foreach ($g as $k => $v) {
			$g[$k]['sort_index2'] = $last;
			$last = $v['sort_index'];
		}

		foreach (GetSubjects('forum', array ('group_id')) as $f)
			if (!empty ($f['group_id']))
				$g[$f['group_id']]['notdeletable'] = true;

		return array ('groups' => $g);
	}

	function actionDeleteGroup($post) {
		global $User;
		$User->requireRight($this->AdminForumRight);
		$capt = MysqlReadFieldByID(TblPrefix().'flip_forum_groups', 'caption', $post['id']);
		if (MysqlDeleteByID(TblPrefix().'flip_forum_groups', $post['id']))
			return LogChange("Im <b>Forum</b> wurde die Gruppe <b>$capt</b> gel&ouml;scht.");
	}

	function frameEditGroup($get) {
		global $User;
		$User->requireRight($this->AdminForumRight);
		$this->Caption = 'ForumAdmin -> Gruppe bearbeiten';
		if (empty ($get['id']))
			return array ('caption' => 'neue Gruppe', 'sort_index' => time());
		return MysqlReadRowByID(TblPrefix().'flip_forum_groups', $get['id']);
	}

	function submitGroup($data) {
		global $User;
		$User->requireRight($this->AdminForumRight);
		$r = MysqlWriteByID(TblPrefix().'flip_forum_groups', $data, $data['id']);
		if ($r)
			LogChange("Im <b>Forum</b> wurde die Gruppe <a href=\"forum.php?frame=viewgroups\"><b>$data[caption]</b></a> ". ((empty ($data['id'])) ? 'erstellt.' : 'bearbeitet.'));
		return $r;
	}

	function actionSwitchGroups($post) {
		global $User;
		$User->requireRight($this->AdminForumRight);
		$id = addslashes($post['id']);
		$si = addslashes($post['sort_index']);
		$si2 = addslashes($post['sort_index2']);
		MysqlWrite('UPDATE `'.TblPrefix()."flip_forum_groups` SET `sort_index` = '$si' WHERE (`sort_index` = '$si2');");
		MysqlWrite('UPDATE `'.TblPrefix()."flip_forum_groups` SET `sort_index` = '$si2' WHERE (`id` = '$id');");
	}

	function frameViewGroup($get) {
		global $User;
		$User->requireRight($this->AdminForumRight);
		$groups = MysqlReadRowByID(TblPrefix().'flip_forum_groups', $get['id']);
		$this->Caption = "ForumAdmin -> $groups[caption]";
		$forums = GetSubjects('forum', array (), '', 'sort_index');
		$last = 0;
		foreach ($forums as $k => $v)
			if ($v['group_id'] != $get['id'])
				unset ($forums[$k]);
		foreach ($forums as $k => $v) {
			$forums[$k]['last'] = $last;
			$last = $v['id'];
		}
		return array ('group' => $groups, 'forums' => $forums, 'sortindex' => time());
	}

/*	function actionFixSortIndexes($get) {
		include_once('mod/mod.forum.php');
		ForumFixSortIndexes();
		return true;
	}
*/
	function actionSwitchForums($post) {
		global $User;
		$User->requireRight($this->AdminForumRight);
		$f1 = CreateSubjectInstance($post['id'], 'forum');
		$f2 = CreateSubjectInstance($post['last'], 'forum');
		$s1 = $f1->getProperty('sort_index');
		$s2 = $f2->getProperty('sort_index');
		$f1->setProperty('sort_index', $s2);
		$f2->setProperty('sort_index', $s1);
	}

	function actionEmptyForum($post, $doupdate = true) {
		global $User;
		$User->requireRight($this->AdminForumRight);

		$id = addslashes($post['id']);
		$ids = MysqlReadCol('SELECT `id` FROM `'.TblPrefix()."flip_forum_threads` WHERE (`forum_id` = '$id');", 'id');
		if (count($ids) > 0)
			if (!MysqlWrite('DELETE FROM `'.TblPrefix().'flip_forum_posts` WHERE (`thread_id` IN ('.implode_sqlIn($ids).'));'))
				return false;
		if (!MysqlWrite('DELETE FROM `'.TblPrefix()."flip_forum_threads` WHERE (`forum_id` = '$id');"))
			return false;

		$f = CreateSubjectInstance($post['id'], 'forum');
		if ($doupdate) {
			$f->setLastPost();
			trigger_error_text('Alle Threads und Posts wurden aus dem Forum gel&ouml;scht.');
		}
		
		// Watches handeln...
		$this->WatchesHandler->CheckWatches('forum',$post['id'],'empty',array('forum' => $f, 'forumids' => $ids));
		
		LogChange("Das <b>Forum <a href=\"forum.php?frame=viewforum&amp;id={$f->id}\">{$f->name}</a></b> wurde geleert.");
		return true;
	}

	function actionDeleteForum($post) {
		global $User;
		$User->requireRight($this->AdminForumRight);

		$this->actionEmptyForum($post, false);
		$f = CreateSubjectInstance($post['id'], 'forum');
		
		// Watches handeln...
		$this->WatchesHandler->CheckWatches('forum',$post['id'],'forumdelete',array('forum' => $f));
		
		LogChange("Das <b>Forum {$f->name}</b> wurde gel&ouml;scht.");
		DeleteSubject($f);
	}

	function actionCountPosts() {
		$threads = MysqlReadCol('SELECT `id` FROM `'.TblPrefix().'flip_forum_threads`;', 'id');
		foreach ($threads as $id)
			$this->threadSetLastPost($id);

		foreach (GetSubjects('forum', array ('id')) as $v) {
			$f = CreateSubjectInstance($v['id']);
			$f->setLastPost();
		}
		trigger_error_text('Die letzten Posts der Foren wurden neu berechnet.');
	}

	function framestats() {
		$this->Caption = text_translate('Forumstatistik');
		return array ('users' => MysqlReadArea('SELECT s.name,
		                                  COUNT(f.poster_id) AS summe,
		                                  MAX(f.post_time)-MIN(f.post_time) AS zeit,
		                                  COUNT( DISTINCT thread_id ) AS threads,
		                                  COUNT(f.poster_id)/COUNT( DISTINCT thread_id ) AS avg
		                           FROM (`'.TblPrefix().'flip_forum_posts` f) LEFT JOIN `'.TblPrefix().'flip_user_subject` s ON f.poster_id = s.id
		                           WHERE f.poster_id > 0
		                           GROUP BY f.poster_id
		                           ORDER BY summe DESC
		                           LIMIT 10 '));
	}

	function framesmalllastposts() {
		$sql = 'SELECT t.*, LEFT(t.title, 14) AS title, t.id AS tid, p.*, p.id AS pid' .
				' FROM `'.TblPrefix().'flip_forum_posts` p' .
				'   LEFT JOIN `'.TblPrefix().'flip_forum_threads` t ON p.thread_id=t.id' .
				' GROUP BY t.id' .
				' ORDER BY post_time DESC LIMIT 10';
		return array('posts' => MysqlReadArea($sql));
	}
}

RunPage('ForumPage');
?>
