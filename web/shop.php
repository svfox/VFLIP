<?php 
/**
 * The shop module is a complete reimplementation of 
 * the FLIP catering system.
 * It is designed to give users a better shopping experience while
 * helping the catering staff to keep track of orders.
 * 
 * @author Matthias Gro&szlig;
 * @version $Id$ 1702 2019-01-09 09:01:12Z
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

// FLIP Core components
require_once 'core/core.php';
require_once 'inc/inc.page.php';
require_once 'inc/inc.text.php';

// Shop backend
require_once 'mod/mod.shop.backend.php';

/**
 * This class represents the front end of the shopping system.
 * It communicates with the ShopBackend instance which manages
 * the tasks concerning the the shop's logic.
 * 
 * @author Scope
 */
class ShopPage extends Page {
	
	/** Texts **/
	var $txtDefaultPage	 		= 'shop_default_page';
	var $txtCartPage			= 'shop_shoppingcart_page';
	var $txtMyBalance			= 'shop_mybalance_page';
	var $txtAdminPage			= 'shop_admin_default_page';
	var $txtAdminPageGroups 	= 'shop_admin_groups_page';
	var $txtAdminPageItems		= 'shop_admin_items_page';
	var $txtAdminUsers	    	= 'shop_admin_users_page';
	var $txtAdminUser	    	= 'shop_admin_user_page';
	var $txtAdminOrdersByItem	= 'shop_admin_order_by_item';
	var $txtAdminBalanceAcc		= 'shop_admin_balance_accounts';
		
	/**
	 * An instance of the shop backend.
	 * 
	 * @var ShopBackend
	 */
	var $shopBackendInstance = null;
	
	//php 7 public function __construct()
	//php 5 original function ShopPage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:		
		parent::__construct();
		$this->shopBackendInstance = new ShopBackend($User);
	}
	
	/**
	 * Provides the front page which contains an overview of 
	 * the enabled food groups like beverages or snacks.
	 * 
	 * @see web/inc/Page::frameDefault()
	 */
	function frameDefault($GetVars, $PostVars) {
		$r = array();
		
		$r['text'] = LoadText($this->txtDefaultPage, $this->Caption);
		$r['groups'] = $this->shopBackendInstance->getItemGroups(FLIP_SHOP_GROUP_ENABLED_ONLY);
		
		return $r;
	}
	
	/**
	 * Displays the contents of a single item group.
	 * 
	 * @param $GetVars Contains the id of the group to display
	 */
	function frameShowGroup($GetVars) {
		$groupID = $GetVars['id'];
		
		$this->Caption = $this->shopBackendInstance->getGroupName($groupID);
		
		$r = array();
		$r['items'] = $this->shopBackendInstance->getItems($groupID);
		
		$item_amount = $this->shopBackendInstance->getItemStockAmounts();
		
		foreach($r['items'] as $key => $item) {
			$stockEnabled = $item['stockenabled']; 
			$remaining = $item_amount[$item['id']];
			$r['items'][$key]['remaining_amount'] = $remaining;
			
			if($stockEnabled && ($remaining < 1)) {
				$r['items'][$key]['enabled'] = 0;
			}
		}
		
		return $r;
	}
	
	/**
	 * Displays the shopping cart of the current user
	 * 
	 * @param $GetVars Are not used here
	 */
	function frameShowShoppingCart($GetVars) {
		$r = array();
		
		$r['text'] = LoadText($this->txtCartPage, $this->Caption);
		$r['contents'] = $this->shopBackendInstance->getShoppingCartContents();
		$r['balance'] = $this->shopBackendInstance->getUserBalance();
		$r['total_sum'] = 0.0;
		
		foreach ($r['contents'] as $key => $value) {
			$r['contents'][$key]['item_sum'] = $value['amount'] * $value['price'];
			$r['total_sum'] += $r['contents'][$key]['item_sum'];
		}
		
		return $r;
	}
	
	/**
	 * Displays compact information about the current state of the shopping cart.
	 * This is especially useful for the side menu in the LAN-Party section.
	 * 
	 * @param $GetVars
	 */
	function frameMenuShoppingCart($GetVars) {
		$r = array();
		
		$contents = $this->shopBackendInstance->getShoppingCartContents();
		
		$r['num_items'] = 0;
		$r['total_sum'] = 0.0;
		$r['balance'] = $this->shopBackendInstance->getUserBalance();
		
		foreach ($contents as $key => $value) {
			$r['num_items'] += $value['amount'];
			$r['total_sum'] += $value['amount'] * $value['price'];
		}
		
		return $r;		
	}
	
	function frameShowMyBalance() {
		$r = array();
		
		$r['text'] = LoadText($this->txtMyBalance, $this->Caption);
		$r['balance'] = $this->shopBackendInstance->getUserBalance();
		$r['complete'] = $this->shopBackendInstance->getCompleteOrders();
		$r['incomplete'] = $this->shopBackendInstance->getIncompleteOrders();
		
		return $r;
	}
	
	/**
	 * Adds a new item to the current shopping cart.
	 * If no cart can be found in the database, a new one will be created first.
	 * 
	 * @param $GetVars Parameters and arguments which describe the new item
	 */
	function actionCartAddItem($GetVars) {
		$addOK = false;		
		
		if(!isset($GetVars['amount']) || empty($GetVars['amount'])) {
			$addOK = $this->shopBackendInstance->addItemToCart($GetVars['itemid'], 1);
		} else {
			$addOK = $this->shopBackendInstance->addItemToCart($GetVars['itemid'], $GetVars['amount']);	
		}
		
		if($addOK) {
			trigger_error('Der Artikel wurde dem Warenkorb hinzugef&uuml;gt!', E_USER_NOTICE);
		}
		
		return $addOK;
	}
	
	/**
	 * Removes an item from the cart completely.
	 * 
	 * @param $GetVars
	 */
	function actionCartDeleteItems($GetVars) {
		if(empty($GetVars['ids'])) {
			return false;
		}
		
		foreach ($GetVars['ids'] as $id) {
			if($this->shopBackendInstance->removeItemFromCart($id)) {
				trigger_error('Ein Artikel wurde aus dem Warenkorb entfernt!', E_USER_NOTICE);
			}
		}
		
		return true;
	}
	
	/**
	 * Changes the amount value of the specified order articles
	 * 
	 * @param $getVars
	 */
	function actionCartChangeAmount($getVars) {
		if(!isset($getVars['ids']) || !isset($getVars['newamount'])) {
			return false;
		}
		
		foreach ($getVars['ids'] as $id) {
			if($this->shopBackendInstance->changeItemAmountInCart($id, $getVars['newamount'])) {
				trigger_error('Die Menge eines Artikels wurde ver&auml;ndert!', E_USER_NOTICE);
			}
		}
		
		return true;
	}
	
	/**
	 * Checks out the contents of the current shopping cart.
	 *
	 */
	function actionCartCheckout() {
		$checkoutOK = $this->shopBackendInstance->checkoutCurrentCart(FLIP_SHOP_CHECKOUT_BALANCE);
		
		if($checkoutOK) {
			trigger_error_text('Der Warenkorb wurde erfolgreich verarbeitet!');
		}
		
		return $checkoutOK;
	}
	
	/**
	 * Checks out the current shopping cart but does not trigger payment.
	 * The user will have to pay the items when he or she picks them up.
	 * 
	 */
	function actionCartOrder() {
		$checkoutOK = $this->shopBackendInstance->checkoutCurrentCart(FLIP_SHOP_CHECKOUT_PLACEORDER);
		
		if($checkoutOK) {
			trigger_error_text('Der Warenkorb wurde erfolgreich verarbeitet!');
		}
		
		return $checkoutOK;
	}
	
	/* ********************************************************************************
	 * Admin frames
	 * ********************************************************************************/
	
	/**
	 * Displays the available admin options.
	 * 
	 * @param $GetVars Has no effect on the page. 
	 */
	function frameAdminOverview($GetVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$r = array();
		$r['text'] = LoadText($this->txtAdminPage, $this->Caption);
		
		return $r;
	}
	
	/**
	 * Displays the groups defined in the system and
	 * lets the user edit them.
	 * 
	 */
	function frameAdminItemGroups($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$r = array();
		$r['groups'] = $this->shopBackendInstance->getItemGroups(FLIP_SHOP_GROUP_ALL);
		$r['text'] = LoadText($this->txtAdminPageGroups, $this->Caption);
		
		return $r;
	}
	
	function frameAdminEditItemGroup($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$r = array();
		$isNewGroup = (empty($getVars['id']) || ($getVars['id'] == 'new'));
		$newGroup = array();
		
		if($isNewGroup) {
			$this->Caption = 'Neue Gruppe anlegen';
			
			$newGroup['id'] = -1;
			$newGroup['name'] = 'Neue Gruppe';
			$newGroup['description'] = '';
			$newGroup['enabled'] = 1;
			$newGroup['imageid'] = -1;
			
			$r = $newGroup;
		} else {
			$group = $this->shopBackendInstance->getGroup($getVars['id']);
			
			if($group === false) {
				trigger_error_text(
					'Die Artikelgruppe mit der Nummer ' . $getVars['id'] . ' wurde nicht gefunden!', 
					E_USER_ERROR
				);
			}
			
			$r = $group;
			
			$this->Caption = 'Gruppe "' . $group['name'] . '" bearbeiten';
		}
		
		return $r;
	}
	
	/**
	 * Is called by the edit group form's save button
	 * and triggers the saving of the (new) group's data.
	 * 
	 * @param $getVars The data required to save the group
	 */
	function submitAdminEditItemGroup($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$recordID = $this->shopBackendInstance->writeGroup($getVars); 
		
		return is_numeric($recordID) ? true : false;
	}
	
	/**
	 * Deletes an item group from the page
	 * 
	 * @param unknown_type $getVars
	 */
	function actionDeleteItemGroup($getVars) {
		if(!is_array($getVars['ids'])) {
			trigger_error_text('Die Gruppen konnten wegen unbekannten IDs nicht gel&ouml;scht werden!', E_USER_ERROR);
			return false;
		}
		
		$deleteOk = $this->shopBackendInstance->deleteGroups($getVars['ids']);
		
		if($deleteOk) {
			trigger_error_text('L&ouml;schen erfolgreich!');
		}
		
		return $deleteOk;
	}
	
	function frameAdminItems() {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$r = array();
		$r['text'] = LoadText($this->txtAdminPageItems, $this->Caption);
		$r['items'] = $this->shopBackendInstance->getItems(FLIP_SHOP_ITEMS_ALL);
		
		$groups = $this->shopBackendInstance->getItemGroups(FLIP_SHOP_GROUP_ALL);

		foreach ($r['items'] as $index => $item) {
			$r['items'][$index]['groupname'] = $groups[$item['groupid']]['name'];
		}
		
		return $r;
	}
	
	/**
	 * Displays the item editing page.
	 * 
	 * @param unknown_type $getVars Parameters of the item
	 */
	function frameAdminEditItem($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$r = array();
		$isNewItem = (empty($getVars['id']) || ($getVars['id'] == 'new'));
		$newItem = array();
		
		if($isNewItem) {
			$this->Caption = 'Neuer Artikel anlegen';
			
			$newItem['id'] = -1;
			$newItem['name'] = 'Neuer Artikel';
			$newItem['price'] = 0.0;
			$newItem['description'] = '';
			$newItem['enabled'] = 1;
			$newItem['stockenabled'] = 0;
			$newItem['stockamount'] = 0;
			$newItem['queued'] = 0;
			$newItem['imageid'] = -1;
			
			$r = $newItem;
		} else {
			$item = $this->shopBackendInstance->getItem($getVars['id']);
			
			if($item === false) {
				trigger_error_text(
					'Die Artikelgruppe mit der Nummer ' . $getVars['id'] . ' wurde nicht gefunden!', 
					E_USER_ERROR
				);
			}
			
			$r = $item;
			
			$this->Caption = 'Artikel "' . $item['name'] . '" bearbeiten';
		}
		
		$groups = $this->shopBackendInstance->getItemGroups(FLIP_SHOP_GROUP_ALL);
		
		foreach ($groups as $group) {
			$r['groups'][$group['id']] = $group['name'];
		}
		
		return $r;		
	}
	
	/**
	 * Executes the storage of the new or edited item.
	 * 
	 * @param $formData The submitted item data.
	 */
	function submitAdminEditItem($formData) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		return $this->shopBackendInstance->writeItem($formData);
	}
	
	/**
	 * Deletes the specified items from the database.
	 * 
	 * @param array $formData The ids to delete.
	 */
	function actionDeleteItems($formData) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		return $this->shopBackendInstance->deleteItems($formData['ids']);
	}
	
	/**
	 * Displays the users who have ordered something along with their
	 * current balance and number of orders.
	 * Also gives an admin the opportunity to change the state of an order
	 * or update an user's balance value.
	 * 
	 * @param mixed $getVars The data about how to display the page
	 */
	function frameAdminUsers($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		$r = array();
		
		if(empty($getVars['userid'])) {
			$r['text'] = LoadText($this->txtAdminUsers, $this->Caption);
			$r['users'] = $this->shopBackendInstance->getTransactionUsers();
		} else {
			$target = CreateSubjectInstance($getVars['userid']);
			$forUser = new ShopBackend($target);
			
			$r['showstates'] = array(
				'ordered','waiting','readyforpickup','completed'
			);
			
			$r['changestates'] = array(
				'ordered' => 'Bestellt', 
				'waiting' => 'Wartet',
				'readyforpickup' => 'Abholbereit',
				'completed'=>'Abgeschlossen'
			);
			
			$r['username'] = $target->getProperty('name');
			$r['text'] = LoadText($this->txtAdminUser, $this->Caption);
			$r['transactions'] = $forUser->getTransactionWithStatus($r['showstates']);
		}

		return $r;
	}
	
	/**
	 * Changes the transaction state of multiple transactions
	 * 
	 * @param $getVars The ids of the transaction
	 */
	function actionAdminChangeTransactionState($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		return $this->shopBackendInstance->setTransactionStates($getVars['ids'], $getVars['state']);
	}
	
	/**
	 * Deletes a transaction from the system and triggers refund.
	 * 
	 * @param $getVars
	 */
	function actionAdminDeleteTransactionWithRefund($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		return $this->shopBackendInstance->deleteTransactions($getVars['ids'], true);
	}
	
	/**
	 * Deletes a transaction from the system.
	 * 
	 * @param $getVars
	 */
	function actionAdminDeleteTransaction($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		return $this->shopBackendInstance->deleteTransactions($getVars['ids'], false);
	}
	
	/**
	 * Shows stats about how often an item has been bought and
	 * calculates/displays the total sales.
	 * 
	 * @param $getVars -
	 */
	function frameAdminTransactionsByItem($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		$r = array();
		
		$r['text'] = LoadText($this->txtAdminOrdersByItem, $this->Caption);
		$r['items'] = $this->shopBackendInstance->getItemStats();
		$r['sales'] = $this->shopBackendInstance->getSales();
		
		return $r;
	}
	
	/**
	 * Displays the existing balance accounts and offers an admin
	 * to modify them or create a new balance account for a user.
	 * 
	 * @param $getVars -
	 */
	function frameAdminBalanceAccounts($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$r = array();
		$r['text'] = LoadText($this->txtAdminBalanceAcc, $this->Caption);
		$r['accounts'] = $this->shopBackendInstance->getBalanceAccounts();
		
		return $r;
	}
	
	/**
	 * Displays the interface for:
	 * 
	 * 1. Searching for a user and then adding a new balance account
	 * 2. Modifiying an existing balance account for an user
	 * 
	 * @param $getVars Contains $getVars['userid'] for displaying the account
	 */
	function frameAdminBalanceAccount($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$this->Caption = 'Neues Guthabenkonto anlegen';
		
		$r = array();
		$r['nextframelink'] = base64_encode('shop.php?frame=adminBalanceAccount&userid={userid}');
		
		$userid = isset($getVars['userid']) ? $getVars['userid'] : 0;
		
		if($userid != 0) {
			$uSubject = CreateSubjectInstance($userid);
			$uShopInstance = new ShopBackend($uSubject);
			
			$this->Caption = 'Guthaben des Users "' . $uSubject->getProperty('name') . '"';
			
			$r['balance'] = $uShopInstance->getUserBalance();
		}
		
		$r['userid'] = $userid;
		
		return $r;
	}
	
	/**
	 * Is triggered by the form on the user transaction admin page and
	 * changes the users balance to another value.
	 * 
	 * @param $getVars
	 */
	function submitChangeUserBalance($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		if(isset($getVars['userid']) && isset($getVars['newbalance'])) {
			$userID = $getVars['userid'];
			$newBalace = $getVars['newbalance'];
			
			$uShopInstance = new ShopBackend(CreateSubjectInstance($userID));
			$uShopInstance->setUserBalance($newBalace);
			
			return true;
		}
		
		trigger_error_text('Das Guthaben des Users konnte nicht ge&auml;ndert werden!', E_USER_ERROR);
		
		return false; 
	}
	
	/**
	 * Is triggered by the red button on the user transaction admin page.
	 * 
	 * @param $getVars
	 */
	function actionClearUserBalance($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();

		
		if(isset($getVars['userid'])) {
			$userID = $getVars['userid'];
			
			$uShopInstance = new ShopBackend(CreateSubjectInstance($userID));
			$delete = $uShopInstance->deleteUserBalanceAccount();
			
			if($delete == false) {
				trigger_error_text('Konnte das Guthaben des Users #' . $userID . ' nicht aufl&ouml;sen!', E_USER_ERROR);
			} else {
				trigger_error_text('Das Guthaben wurde aufgel&ouml;st!');
			}
			
			return $delete;
		}
		
		trigger_error_text('Das Guthaben des Users konnte nicht ge&auml;ndert werden!', E_USER_ERROR);
		
		return false; 		
	}
	
	/**
	 * Shows the items of a single transaction.
	 * 
	 * @param $getVars
	 */
	function frameadminTransaction($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$this->Caption = 'Transaktion betrachten';
		
		$r = array();
		$r = $this->shopBackendInstance->getTransactionWithID($getVars['id']);
		$r['userid'] = $this->shopBackendInstance->getCurrentUserID();
		
		return $r;
	}
	
	/**
	 * Prints the production queue view.
	 *
	 * @param $getVars
	 */
	function frameAdminProductionQueue($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();
		
		$this->Caption = 'Produktionswarteschlange';
		
		$r = array();
		$r['waiting'] = $this->shopBackendInstance->getTransactionWithStatusAndUser(
			array('waiting'), 
			FLIP_SHOP_USERS_ALL
		);
			
		$r['times'] = array(
			1 => 1, 
			2 => 2, 
			5 => 5, 
			10 => 10, 
			20 => 20, 
			60 => 60, 
			120 => 120
		);
			
		return $r;
	}
	
	/**
	 * Sets a transaction to readyforpickup
	 * 
	 * @param $getVars
	 */
	function actionSetTransactionReady($getVars) {
		$this->shopBackendInstance->requireAdminPrivileges();

		if(!isset($getVars['id']) || empty($getVars['id'])) {
			trigger_error_text('Die ID einer Transaktion ist ung&uuml;ltig!', E_USER_ERROR);
			return false;
		}

		$transactionIDs = array($getVars['id']);
		
		return $this->shopBackendInstance->setTransactionStates($transactionIDs, 'readyforpickup');
	}
}

RunPage('ShopPage');
?>