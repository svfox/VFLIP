<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: ticket.php 536 2004-10-12 22:38:17Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 **/

/**
 * Liefert alle Werte einer Tabelle
 * 
 * @param String $TableName Name der Tabelle
 * @return Array Array mit allen Werten
 */
function TableGetTableValues($TableName) {
	$r = MysqlReadCol("
		    SELECT e.key, e.value FROM `".TblPrefix()."flip_table_entry` e, `".TblPrefix()."flip_table_tables` t
		    WHERE (e.table_id = t.id) AND (t.name = '".addslashes($TableName)."');
		  ", "value", "key");
	uasort($r, 'strnatcasecmp');
	return $r;
}

function TableGetDisplay($Table, $Key, $Vars = array ()) {
	$k = explode(",", $Key);
	$r = array ();
	foreach ($k as $v)
		$r[] = TableGetDisplayItem($Table, $v, $Vars);
	return implode(", ", $r);
}

function TableGetDisplayItem($Table, $Key, $Vars = array ()) {
	include_once ("mod/mod.template.php");
	static $cache = array ();
	if (!isset ($cache[$Table]))
		$cache[$Table] = MysqlReadArea("
				    SELECT e.key, e.display_code, e.code_exec FROM `".TblPrefix()."flip_table_entry` e, `".TblPrefix()."flip_table_tables` t
				    WHERE (e.table_id = t.id) AND (t.name = '".addslashes($Table)."');
				  ", "key");
	$t = $cache[$Table];
	if (!isset ($t[$Key])) {
		if (!empty ($Key))
			trigger_error_text("Ein Key einer Tabelle Existiert nicht.|Table:$Table Key:$Key", E_USER_WARNING);
		return "";
	} else
		$t = $t[$Key];
	if ($t["code_exec"])
		return TemplateExecute($t["display_code"], $Vars);
	else
		return $t["display_code"];
}

function TableCanEdit($Table) {
	global $User;
	$r = MysqlReadField("SELECT `edit_right` FROM `".TblPrefix()."flip_table_tables` WHERE (`name` = '".addslashes($Table)."');");
	return ($User->hasRight($r)) ? true : false;
}
?>