<?php

/**
 * Implementierung von RFC4287 [The Atom Syndication Format] (December 2005)
 * 
 * nicht implementiert:
 * - xml:base attribute [W3C.REC-xmlbase-20010627]
 * - xml:lang attribute for elements NOT declared to be "Language-Sensitive"
 * - Extending Atom
 * 
 * @author Daniel Raap
 * @version $Id: inc.form.php 1702 2019-01-09 09:01:12Z VulkanLAN $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 * @since 1376 - 27.02.2007
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.FEED.ATOM.PHP"))
	return 0;
define("MOD.FEED.ATOM.PHP", 1);

/** FLIP-Kern */
require_once ("core/core.php");

/**
 * Basiselemente
 * "Many of Atom's elements share a few common structures. This section defines
 * those structures and their requirements for convenient reference by the
 * appropriate element definitions."
 * quote of RFC4287
 */
class atomConstructs {
	/** xml:lang specified in XML 1.0 [W3C.REC-xml-20040204], Section 2.12 */
	var $lang;

	/**
	 * Konstruktor
	 */
	 //php 7 public function __construct()
	//php 5 original function atomConstructs()
	function __construct($lang = null) {
		if(is_null($lang)) {
			$lang = ConfigGet("translate_language");
		}
		$this->setLang($lang);
	}

	function setLang($aLanguageTag) {
		if ($aLanguageTag === "" || $this->isLang($aLanguageTag)) {
			$this->lang = $aLanguageTag;
		}
	}

	/**
	 * TODO: "MUST be an IRI reference [RFC3987]"
	 */
	function isIRI($text) {
		return (strpos($text, ":") > 1);
	}

	/**
	 * Gibt an ob es sich um einen Mediatypen nach RFC4288 handelt
	 * @see Section 4.2. Naming Requirements in RFC4288 (Media Type
	 * Specifications and Registration Procedures)
	 * @param String $aText mediatype
	 * @return boolean
	 */
	function isMediatype($aText) {
		//"MUST conform to the syntax of a MIME media type" (RFC4287)
		return preg_match("/^[\w!#$&.+\-^]{1,127}\/[\w!#$&.+-^_]{1,127}$/i", $aText); //i suppose '/' is the concatenation string of 'type' and 'subtype' like RFC2045 section 5.1. [loom]
	}

	function isContentMediatype($aText) {
		//"MUST NOT be a composite type (see Section 4.2.6 of [RFC 4288])" (RFC4287)
		if (preg_match("/^(multipart|message)/i", $aText)) {
			return false;
		}
		return $this->isMediatype($aText);
	}
	
	/**
	 * Der Medientyp gibt an, dass es sich um Text handelt
	 * 
	 * @param String $aText Medientyp (MIME-type)
	 * @return boolean
	 */
	function isTextMediatype($aText) {
		if($this->isMediatype($aText)) {
			if(preg_match("/(^text\/)|(\/xml$)|(\+xml$)|(\/xml-)/", $aText)) { //FIXME: may not be complete
				return true;
			}
		}
		return false;
	}
	
	/**
	 * the value MUST be one of "text", "html", or "xhtml"
	 * (RFC4287, 3.1.1 The "type" Attribute)
	 */
	function isTextType($aType) {
		switch($aType) {
			case "text":
			case "html":
			case "xhtml":
				return true;
			default:
				return false;
		}
	}

	/**
	 * Gibt an ob es sich nach RFC3066 um ein'language tag' handelt
	 * "value MUST be a language tag [RFC3066]"
	 */
	function isLang($aLanguageTag) {
		//HACK: only syntax check
		return preg_match("/^[a-z]{1,8}(-[a-z0-9]{1,8})?$/i", $aLanguageTag);
	}

	/**
	 * Language-Sensitive
	 * 
	 * @param Array $array
	 * @return Array Array ggf. mit Schl&uuml;ssel 'lang' 
	 */
	function lang($array) {
		if ($this->isLang($this->lang)) {
			$array["lang"] = $this->lang;
		}
		return $array;
	}

	/**
	 * "A Text construct contains human-readable text, usually in small
	 * quantities. The content of Text constructs is Language- Sensitive."
	 * quote of RFC4287
	 * 
	 * @param StringOrArray $text Entweder String oder ein Array mit den
	 * Schl&uuml;sseln 'type' und 'text'
	 * @return Array atom-data
	 */
	function text($text) {
		$r = array ();
		if (is_object($text)) {
			//Objekt
			$r = array (
				"type" => "text",
				"text" => gettype($text
			));
		}
		elseif (!is_array($text)) {
			//Array
			$r = array (
				"type" => "text",
				"text" => $text
			);
		} else {
			//Rest
			if (isset ($text["text"]) && !is_array($text["text"]) && !is_object($text["text"])) {
				if(!$this->isTextType($text["type"])) {
					$text["type"] = "text";
				}
				$r = $text;
			} else {
				$r = null;
			}
		}
		if(is_array($r)) {
			$r = $this->lang($r);
		}
		return $r;
	}

	/**
	 * "A Person construct is an element that describes a person, corporation,
	 * or similar entity"
	 * quote of RFC4287
	 * 
	 * Childs: name, uri (optional), email (optional)
	 * 
	 * @param mixed $data Personendaten (Array oder String)
	 * @return Array Personendaten (Name [, URL][, E-Mail])
	 */
	function person($data) {
		if(is_null($data)) {
			return null;
		}
		
		$r = array ();
		if(!is_array($data)) {
			$name = $data;
			$data = array();
			$data["name"] = $name;
		}
		
		//a human-readable name for the person
		if (is_array($data["name"]) || is_object($data["name"])) {
			$data["name"] = gettype($data["name"]);
		}
		$r["name"] = $data["name"];
		if ($this->isIRI($data["uri"])) {
			$r["uri"] = $data["uri"];
		}
		if (IsValidEmail($data["email"])) {
			$r["email"] = $data["email"];
		}
		return $this->lang($r);
	}
	
	function isPerson($aPerson) {
		if(is_array($aPerson)) {
			if(!empty($aPerson["name"]) && !is_array($aPerson["name"]) && !is_object($aPerson["name"])) {
				return true;
			}	
		}
		return false;
	}

	/**
	 * "A Date construct is an element whose content MUST conform to the "date-
	 * time" production in [RFC3339]. In addition, an uppercase "T" character
	 * MUST be used to separate date and time, and an uppercase "Z" character
	 * MUST be present in the absence of a numeric time zone offset."
	 * quote of RFC4287
	 * 
	 * @param String $string Zeitangabe
	 * @return String
	 */
	function date($string) {
		if (is_posDigit($string)) {
			//Timestamp
			$timestamp = $string;
		}
		elseif (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(Z|[+-][0-9]{2}:[0-9]{2})$/", $string)) {
			//already correct format
			return $string;
		}
		elseif ($timestamp = strtotime($string) != -1) {
			//PHP can get a timestamp out of it
			if (!is_posDigit($timestamp)) {
				$timestamp = time();
			}
		}
		elseif ($timestamp = toTimestamp($string) != $string) {
			//FLIP can get a timestamp out of it
			if (!is_posDigit($timestamp)) {
				$timestamp = time();
			}
		} else {
			//no date
			$timestamp = time();
		}

		return gmdate("Y-m-d\TH:m:i\Z", $timestamp);
	}
}

/**
 * Ein einzelner Eintrag bzw. ein Atom-Eintrag-Dokument (Atom Entry Document)
 * "The "atom:entry" element represents an individual entry, acting as a
 * container for metadata and data associated with the entry."
 * "An Atom Entry Document represents exactly one Atom entry, outside of the
 * context of an Atom feed."
 * quotes of RFC4287
 */
class atomEntry extends atomConstructs {
	//required elements
	var $title;
	var $id;
	var $updated;
	var $authors; //only if document doesn't contain an author
	var $errCodes = array(
	 "ok"		=> array("no"=> 0, "text"=>"OK"),
	 "noId"		=> array("no"=> 1, "text"=>"keine g&uuml;ltige ID"),
	 "noTitle"	=> array("no"=> 2, "text"=>"kein Titel"),
	 "noDate"	=> array("no"=> 4, "text"=>"kein Aktualisierungsdatum"),
	 "noAuthor"	=> array("no"=> 8, "text"=>"kein Author"),
	 "noContent"=> array("no"=>16, "text"=>"kein content und kein alternate-link"),
	 "noSummary"=> array("no"=>32, "text"=>"keine Summary bei Nicht- Text-Content")
	);

	/**
	 * Konstruktor
	 * 
	 * @param String $title Titel
	 * @param String $updated Zeitangabe der letzten &Auml;nderung
	 * @param String $id eindeutige IRI (URL)
	 * @param Array $autors Die Autoren des Eintrages (darf auch nur eine Person
	 * sein)
	 */
	 //php 7 public function __construct()
	//php 5 original function atomEntry()
	function __construct($title, $updated, $id, $authors = null, $lang = null) {
		//parent costructor
		$this->atomConstructs($lang);

		$this->setTitle($title);
		$this->setUpdated($updated);
		$this->setId($id);
		if (!is_array($authors) || isset ($authors["name"])) {
			$this->addAuthor($authors);
		} else {
			foreach ($authors AS $aAuthor) {
				$this->addAuthor($aAuthor);
			}
		}
	}

	/**
	 * Setzt die ID
	 * 
	 * @param String $aIRI muss ein IRI nach RFC3987 sein
	 * @return boolean
	 */
	function setId($aIRI) {
		if ($this->isIRI($aIRI)) {
			$this->id = $aIRI;
			return true;
		} else {
			trigger_error_text("atomEntry::setId(): " . text_translate("Keine g&uuml;ltige ID!") . "|\$aIRI='$aIRI'", E_USER_WARNING);
			return false;
		}
	}

	function setTitle($aText) {
		$this->title = $this->text($aText);
	}

	function setUpdated($aDate) {
		$this->updated = $this->date($aDate);
	}

	function addAuthor($aPerson) {
		if(!is_null($aPerson)) {
			$p = $this->person($aPerson);
			if (!empty ($p)) {
				$this->authors[] = $p;
			}
		}
	}

	/**
	 * Setzt die Daten f&uuml;r das content-Tag
	 * 
	 * @param mixed $data Kontent (Text oder Array mit den Schl&uuml;sseln text und
	 * type)
	 * @return boolean true, falls Daten die Spezifikation erf&uuml;llen
	 */
	function setContent($data) {
		$r = false;

		if (!is_array($data)) {
			$data = $this->text($data);
		}
		if(!isset($data["isBase64"])) {
			$data["isBase64"] = false;
		}

		/* "If neither the type attribute nor the src attribute is provided,
		 * Atom Processors MUST behave as though the type attribute were present
		 * with a value of "text"."
		 * (RFC 4287, 4.1.3.1 The "type" Attribute)
		 */
		if (!isset ($data["type"]) && !isset ($data["src"])) {
			$data["type"] = "text";
		}

		if ($this->isIRI($data["src"])) {
			//"the "type" attribute SHOULD be provided and MUST be a MIME media type" (RFC 4287, 4.1.3.2 The "src" Attribute)
			if (isset ($data["type"]) && !$this->isMediatype($data["type"])) {
				trigger_error_text("atomEntry::setContent(): " . text_translate("Kein g&uuml;ltiger Mediatype!") . "|type='" . $data["type"] . "'", E_USER_WARNING);
			}
			//"If the "src" attribute is present, atom:content MUST be empty" (RFC 4287, 4.1.3.2 The "src" Attribute)
			$data["text"] = "";
			$this->content = $data;
			$r = true;
		}
		elseif (isset ($data["text"])) {
			/* "the value of the "type" attribute MAY be one of "text", "html", or "xhtml".
			 * Failing that, it MUST conform to the syntax of a MIME media type"
			 * (RFC 4287, 4.1.3.1 The "type" Attribute)
			 */
			if ($this->isTextType($data["type"]) || $this->isContentMediatype($data["type"])) {
				if(!($this->isTextMediatype($data["type"]) || $this->isTextType($data["type"]))) {
					$data["isBase64"] = true;
				}
				$this->content = $data;
				$r = true;
			}
		}
		if(is_array($this->content)) {
			$this->content = $this->lang($this->content);
		}
		return $r;
	}
	
	function setContentIsBase64() {
		$this->content["isBase64"] = true;
	}
	
	function setSummary($aText) {
		$this->summary = $this->text($aText);
	}

	/**
	 * F&uuml;gt einen Verweis hinzu
	 * 
	 * @param mixed $aLink URL (IRI) oder Array mit den Schl&uuml;sseln href[, rel][,
	 * type] [, hreflang] [, title][, length]
	 */
	function addLink($aLink) {
		if (!is_array($aLink)) {
			$aLink = array (
				"href" => $aLink
			);
		}

		/* "atom:link elements MUST have an href attribute, whose value MUST be
		 * a IRI reference [RFC3987]"
		 * (RFC 4287, 4.2.7.1 The "href" Attribute)
		 */
		if ($this->isIRI($aLink["href"])) {
			$myLink = array ();
			foreach ($aLink AS $key => $value) {
				//$use = in_array($key, array("href","rel","type","hreflang","title","length"));
				$use = false;
				switch ($key) {
					case "href" :
						$use = true;
						break;
					case "rel" :
						$use = true;
						if (!$this->isIRI($value)) {
							$value = (string) $value;
							if (empty ($value) || strpos($value, ":") !== false) { //HACK: not as precise as RFC3987 about isegment-nz-nc
								$use = false;
							}
						}
						break;
					case "type" :
						$use = $this->isMediatype($value);
						break;
					case "hreflang" :
						$use = $this->isLang($value);
						break;
					case "title" :
						$use = true;
						//"The content of the "title" attribute is Language-Sensitive."
						$myLink = $this->lang($myLink);
						break;
					case "length" :
						$use = is_posDigit($value);
					default :
						}
				if ($use) {
					$myLink[$key] = $value;
				}
			}
			/**
			 * "If the "rel" attribute is not present, the link element MUST be
			 * interpreted as if the link relation type is "alternate"."
			 * (RFC 4287, 4.2.7.2 The "rel" Attribute)
			 */
			if (!isset ($myLink["rel"])) {
				$myLink["rel"] = "alternate";
			}

			if (!empty ($myLink)) {
				$this->links[] = $myLink;
			}
		}
	}

	function addCategory($aCategory) {
		if (!is_array($aCategory)) {
			$aCategory = array (
				"name" => $aCategory
			);
		}

		if (isset ($aCategory["name"])) {
			$myCat = array ();
			$myCat["name"] = (string) $aCategory["name"];
			if ($this->isIRI($aCategory["scheme"])) {
				$myCat["scheme"] = $aCategory["scheme"];
			}
			if (isset ($aCategory["label"])) {
				$myCat["label"] = (string) $aCategory["label"];
				//"The content of the "label" attribute is Language-Sensitive." (RC4287, 4.2.2.3 The "label" Attribute)
				$myCat = $this->lang($myCat);
			}
			$this->categories[] = $myCat;
		}
	}

	function addContributor($aPerson) {
		$p = $this->person($aPerson);
		if (!empty ($p)) {
			$this->contributors[] = $p;
		}
	}

	function setPublished($aDate) {
		$this->published = $this->date($aDate);
	}

	function setRights($aString) {
		$this->rights = $this->text($aString);
	}

	/**
	 * "If an atom:entry is copied from one feed into another feed, then the
	 * source atom:feed's metadata [...] MAY be preserved within the copied
	 * entry by adding an atom:source child element"
	 * (RFC4287, 4.2.11 The "atom:source" Element)
	 * 
	 * @param object $aFeed ein atomFeed-Objekt
	 * @return boolean
	 */
	function setSource($aFeed) {
		if (is_a($aFeed, "atomFeed")) {
			unset($aFeed->entries);
			$this->source = $aEntry->toArray();
			return true;
		}
		return false;
	}

	/**
	 * Pr&uuml;ft ob alle ben&ouml;tigten Elemente mit korrekten Daten gef&uuml;llt sind
	 * 
	 * @see $this->errCodes
	 * @return int Bitmuster mit Fehlermeldung
	 */
	function isValid() {
		$r = 0;
		
		//MUST contain exactly one atom:id element
		if(!$this->isIRI($this->id)) {
			$r += $this->errCodes["noId"]["no"];
		}
		
		//MUST contain exactly one atom:title element.
		if($this->title == "") {
			$r += $this->errCodes["noTitle"]["no"];
		}
		
		//MUST contain exactly one atom:updated element.
		if($this->updated == "") {
			$r += $this->errCodes["noDate"]["no"];
		}
		
		/* "atom:entry elements MUST contain one or more atom:author elements, unless the
		 * atom:entry contains an atom:source element that contains an
		 * atom:author element or, in an Atom Feed Document, the atom:
		 * feed element contains an atom:author element itself."
		 * (RFC4287, 4.1.2 The "atom:entry" Element)
		 */
		if (!is_array($this->authors)) {
			$fail = true;
			if(isset($this->src) && is_array($this->src->authors)) {
				foreach($this->src->authors AS $author) {
					if($this->isPerson($author)) {
						$fail = false;
						break;
					}
				}
			}
			if($fail) {
				$r += $this->errCodes["noAuthor"]["no"];
			}
		}

		/* elements that contain no child atom:content element MUST contain at
		 * least one atom:link element with a rel attribute value of "alternate"
		 */
		if (!isset ($this->content)) {
			$haslink = false;
			if(is_array($this->links)) {
				foreach ($this->links AS $link) {
					if ($link["rel"] == "alternate") {
						$haslink = true;
						break;
					}
				}
			}
			if (!$haslink) {
				$r += $this->errCodes["noContent"]["no"];
			}
		}

		/* elements MUST contain an atom:summary element in either of the
		 * following cases
		 * - the atom:entry contains an atom:content that has a "src" attribute
		 * (and is thus empty)
		 * - the atom:entry contains content that is encoded in Base64; i.e.,
		 * the "type" attribute of atom:content is a MIME media type [MIMEREG],
		 * but is not an XML media type [RFC3023], does not begin with "text/",
		 * and does not end with "/xml" or "+xml".
		 */
		if (isset ($this->content["src"]) || $this->content["isBase64"]) {
			if (empty($this->summary)) {
				$r += $this->errCodes["noSummary"]["no"];
			}
		}

		return $r;
	}

	function toArray() {
		return (array) $this;
	}
}

/**
 * Ein kompletter Feed (Atom Feed Document)
 * "An Atom Feed Document is a representation of an Atom feed, including
 * metadata about the feed, and some or all of the entries associated with it."
 * quote of RFC4287
 */
class atomFeed extends atomEntry {
	var $entries = array ();
	var $encoding = "iso-8859-1";
	var $tplFile = "inc.atom.tpl";
	var $tplSub = "feed";

	/**
	 * Konstruktor
	 */
	//php 7 public function __construct()
	//php 5 original function atomFeed()
	function __construct($title, $updated, $id, $authors = null, $lang = null) {
		//parent constructor
		$this->atomEntry($title, $updated, $id, $authors, $lang);
		$this->addLink(array("href"=>ServerURL().dirname(GetRequestUri()), "rel"=>"alternate", "title"=>ConfigGet("page_title")));
	}

	function add($entry) {
		if (is_a($entry, "atomEntry")) {
			$this->entries[] = $entry;
			return true;
		}
		return false;
	}

	function setEncoding($anEncoding) {
		//TODO: unchecked!
		$this->encoding = $anEncoding;
	}
	
	function create() {
		$err = $this->isValid();
		if($err == 0) {
			$this->header();
			return $this->toArray();
		} else {
			$errors = $this->parseErrNo($err);
			//atom error message
			$errorFeed = new atomFeed(text_translate("Fehler"), time(), $this->id, GetSubjectName(SYSTEM_USER_ID));
			//log error message
			$errString = "<ul>\n";
			foreach($errors AS $childNo=>$err) {
				foreach($err AS $no=>$text) {
					$errString .= "    <li>($childNo. Element) $text [$no]</li>\n";
					$e = new atomEntry(text_translate("Fehler in Element ").$childNo, time(), $this->id.((strpos($this->id,"?")) ? "&" : "?")."error=$childNo-$no");
					$e->setContent($text);
					$errorFeed->add($e);
				}
			} 
			$errString .= "</ul>\n";
			trigger_error_text("atomFeed::create(): ".text_translate("Feed ist nicht g&uuml;ltig!")."<br/>\n$errString|errno=$err, childBase=$base", E_USER_WARNING);
			return $errorFeed->toArray();
		}
	}
	
	/**
	 * get errorstring from errornumber
	 * returns: array(<childNo>=>array(<errNo>=><errText>))
	 * 
	 * @param int $err kombinierte Fehlernummern
	 * @return Array assoziatives Array (child=>array(no=>text))
	 */
	function parseErrNo($err) {
		$base = $this->childErrorBase();
		$codes = array();
		foreach($this->errCodes AS $aCode) {
			$codes[$aCode["no"]] = $aCode["text"];
		}
		$errors = array();
		$i = 0;
		while($err > 0) {
			$errNo = $err % $base;
			if($errNo > 0) {
				$errors[$i] = array($errNo=>$codes[$errNo]);
			}
			$err = floor(($err - $errNo) / $base);
			$i++;
		}
		return $errors;
	}
	
	/**
	 * gibt eine Fehlernummer zur&uuml;ck
	 * In Abst&auml;nden von childErrorBase() werden die Elemente von rechts
	 * nach links aufgelistet.
	 * z. B.: childErrorBase() = 100
	 * 23545 => 2 zweites Kind, 35 erstes Kind, 45 dieses Element
	 * 
	 * @return int
	 */
	function isValid() {
		$err = parent::isValid();
		
		$allvalid = false;
		if(count($this->entries) > 0) {
			$allvalid = true;
			$base = $this->childErrorBase();
			$childNo = 1;
			foreach($this->entries AS $entry) {
				$childErr = $entry->isValid(); 
				if($childErr) {
					if(!($err == 0 && $childErr == $this->errCodes["noAuthor"]["no"])) {
						$allvalid = false;
						$err += $childErr * pow($base, $childNo);
					}
				}
				$childNo++;
			}
		}
		/** "MUST contain one or more atom:author elements, unless all of
		 * the atom:feed element's child atom:entry elements contain at
		 * least one atom:author element."
		 * 
		 * versch&auml;rft: fehlerfreie Kinder (muss nicht nur Author enthalten)
		 */
		if($allvalid && $err == $this->errCodes["noAuthor"]["no"]) {
			$err = 0;
		}
		
		return $err;
	}
	
	/**
	 * gibt den Faktor an, um welche die Errorcodes der Kindelemente verschoben
	 * werden
	 * Zehnerpotenz, welche gr&ouml;&szlig;er ist, als zweimal der gr&ouml;&szlig;te Fehlercode
	 * z.B.:
	 * gr&ouml;&szlig;ter Fehlercode = 512, dann liefert childErrorBase() 10000
	 * f&uuml;r Fehlercodes von: Elternelement = 36, Kind1 = 8, Kind2 = 1022
	 * ergibt sich: 36 + 10000^1*8 + 10000^2*144 = 102200080036
	 * 
	 * @access private
	 * @return int
	 */
	function childErrorBase() {
		$base = 10;
		foreach($this->errCodes AS $err) {
			while($err["no"]*2-1 > $base) { //HACK: should be possible without loop
				$base *= 10;
			} 
		} 
		return $base;
	}
	
	function header() {
		header("content-type: application/atom+xml; charset=" . $this->encoding);
	}

	function toArray() {
		$r = (array) $this;
		//child objects to arrays
		foreach ($r["entries"] AS $key => $entry) {
			$r["entries"][$key] = $entry->toArray();
		}
		return $r;
	}
}
?>
