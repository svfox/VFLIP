<?php
/**
 * Ein Objekt welches eine Zeile aus der Datenbank wiederspiegelt
 * @author loom
 * @version $Id$ 1702 2019-01-09 09:01:12Z VulkanLAN $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 * @since 1398 25.05.2007 */

/** Die Datei nur einmal includen */
if (defined("INC.SQLROWOBJECT.PHP"))
	return 0;
define("INC.SQLROWOBJECT.PHP", 1);

/** FLIP-Kern */
require_once("core/core.php");

/**
 * Einfache Klasse welche Daten eines Arrays als Attribute/Felder zuweist
 */
class SqlRowObject {
	
	
	
	/**
	 * Konstruktor
	 * Die ersten beiden Parameter d&uuml;rfen null sein, damit DB-Queries verhindert
	 * werden k&ouml;nnen!
	 * @param String $table Tabellenname
	 * @param int $id ID der Zeile
	 * @param Array $data Daten die als Felder/Attribute zugewiesen werden
	 * sollen
	 */
	//php 7 public function __construct()
	//php 5 original function SqlRowObject()
	function __construct($table = null, $id = null, $data = null) {
		if(!is_null($table) && !is_null($id)) {
			$data = array_merge($data, MysqlReadRowByID($table, $id));
		}
		if(is_array($data)) {
			foreach($data AS $key=>$value) {
				$this->{$key} = $value;
			}
		}
	}
}
?>
