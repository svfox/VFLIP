<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: ticket.php 536 2004-10-12 22:38:17Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 **/

/**
 * L&auml;dt einen zuf&auml;lligen Banner
 */
function SponsorLoadRandomBanner() {
	global $Session, $User;
	$Location = "bannerrotation";

	if ($User->hasRight("sponsor_no_banner"))
		return array ();

	// bannerdaten auslesen
	$banner = CacheGet("sponsor_banners");
	if (!is_array($banner)) {
		$banner = MysqlReadArea("
		      SELECT `image_mtime`, `id`, `priority`, `enable_view_log`, `enable_session_log`, `enable_counter`, `html`, `link`
		        FROM `".TblPrefix()."flip_sponsor_ads` 
		          WHERE ((`show_as_ad` = '1') AND (`type` = 'banner'));
		    ");
		CacheSet("sponsor_banners", $banner, array ("flip_sponsor_ads"));
	}
	if (empty ($banner))
		return array ();

	// einen zuf&auml;lligen banner mit ber&uuml;cksichtigung der Priorit&auml;ten bestimmen
	srand(time());

	$max = 0;
	foreach ($banner as $v)
		$max += $v["priority"];
	if (empty ($max))
		return array ();

	$i = $r = 0;
	$rnd = rand(0, $max);
	foreach ($banner as $v) {
		$i += $v["priority"];
		if ($i >= $rnd) {
			$r = $v;
			break;
		}
	}
	if (empty ($r)) {
		trigger_error_text("Zufallszahl au&szlig;erhalb des definierten Bereichs.", E_USER_WARNING);
		return array ();
	}

	// in den log schreiben
	SponsorLogAndCount($Location, $r);
	$r["location"] = $Location;
	return $r;
}

function SponsorLogAndCount($Location, $dat) {
	global $Session;
	SponsorDoLog("view", $Location, $dat);

	$shown = (isset($Session->Data["sponsor_view"])) ? $Session->Data["sponsor_view"] : null;
	if (!is_array($shown))
		$shown = array ();
	if (!in_array($dat["id"], $shown)) {
		SponsorDoLog("session", $Location, $dat);
		SponsorCounterInc(array ("views", "sessions"), $dat);
		$shown[] = $dat["id"];
	} else
		SponsorCounterInc(array ("views"), $dat);
	$Session->Data["sponsor_view"] = $shown;
	$dat["location"] = $Location;
	return $dat;

}

function SponsorDoLog($Reason, $Location, $dat) {
	global $Session;
	if (!$dat["enable_{$Reason}_log"])
		return null;
	return MysqlWriteByID(TblPrefix()."flip_sponsor_log", array ("reason" => $Reason, "ad_id" => $dat["id"], "ip" => $_SERVER["REMOTE_ADDR"], "session_id" => $Session->id, "location" => $Location));
}

function SponsorCounterInc($Cols, $dat) {
	if (!$dat["enable_counter"])
		return;
	$id = addslashes($dat["id"]);
	$r = array ();
	foreach ($Cols as $c)
		$r[] = "`$c` = `$c` + 1";
	return MysqlWrite("UPDATE `".TblPrefix()."flip_sponsor_ads` SET ".implode(", ", $r)." WHERE (`id` = '$id');");
}
?>