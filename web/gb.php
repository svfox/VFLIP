<?php


/**
 * @author Andreas G. Ver. 1.1
 * @author Daniel Raap
 * @version $Id: gb.php 1702 2019-01-09 09:01:12Z tazzweasel $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class GBPage extends Page {
	//Config
	var $gbperpageconfig = "gb_perpage";

	//Rechte
	var $right_post = "gb_add";
	var $right_admin = "gb_admin";
	
	//Pfad
	var $iconpath = "images/smilies/"; //Unterhalb vom Template

	//php 7 public function __construct()
	//php 5 original function GBPage()
	function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$this->Caption = text_translate("G&auml;stebuch");
	}

	function framedefault($get, $post) {
		global $User;

		//seitenweise
		$gbperpage = (int) ConfigGet($this->gbperpageconfig);
		if (empty ($get["more"])) {
			$limit = 0;
		} else {
			$limit = (integer) $get["more"];
		}

		//nur ein G&auml;stebucheintrag?
		$w = "";
		if (!empty ($get["gbid"]))
			$w = "WHERE id='" . addslashes($get["gbid"]) . "'";

		//G&auml;stebuch auslesen
		include_once("mod/mod.template.php");
		foreach (MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_gb_entry` $w ORDER BY date DESC LIMIT $limit," . $gbperpage) AS $data) {
			// Umwandlung von Teiltexten in Smilies
			foreach ($this->getCodes() AS $icontext => $imagepath) {
				$data["text"] = str_replace($icontext, "<img src=\"".getTplFileDir($imagepath)."$imagepath\" alt=\"$icontext\" border=\"0\"/>", $data["text"]);
			}
			$r["entries"][] = $data;
		}

		//Default wenn keine G&auml;stebucheintr&auml;ge vorhanden sind
		if (empty ($r))
			$r = array (
				"noentry" => true,
				"entries" => array (
					array (
					"date" => time(),
					"title" => text_translate("Noch kein G&auml;stebucheintrag vorhanden."
				),
				"author" => GetSubjectName(SYSTEM_USER_ID
			), "text" => text_translate("Bisher wurde noch kein G&auml;stebucheintrag erstellt."))));
		
		//RECHTE
		if ($User->hasRight($this->right_admin)) {
			$r["edit"] = "true";
		}

		//Links zu n&auml;chster/vorheriger Seite
		if (MysqlReadField("SELECT COUNT(*) FROM `" . TblPrefix() . "flip_gb_entry`", "COUNT(*)") - $limit > $gbperpage)
			$r["more"] = $limit + $gbperpage;
		if ($limit < $gbperpage && $limit > 0) {
			$r["less"] = 0;
		} elseif ($limit >= $gbperpage) {
			$r["less"] = $limit - $gbperpage;
		}
		if (!empty ($get["gbid"]))
			unset ($r["more"]);
		
		return $r;
	}

	function frameadd($get) {
		$this->requireAdd();
		ArrayWithKeys($get, array('id'));
		//Icons
		$r = array();
		$r["codes"] = $this->getCodes();
		
		if (is_posDigit ($get["id"]) && $data = MysqlReadRowByID(TblPrefix() . "flip_gb_entry", $get["id"])) {
			return array_merge($data, $r);
		} else {
			return $r;;
		}
	}

	function submitentry($post) {
		global $User;
		$this->requireAdd();
		
		//Captcha vorhanden?
		if(!isset($post["Sicherheitsabfrage"])) {
			return false;
		}
		unset($post["Sicherheitsabfrage"]);
		
		//fill empty fields
		if (!is_posDigit ($post["id"]))
			$post["id"] = 0;
		if (empty ($post["author"]))
			$post["author"] = $User->name;
		
		$post["date"] = time();
		
		//escape text
		$post["text"] = nl2br(escapeHtml($post["text"]));

		//save and log
		if ($writeid = MysqlWriteByID(TblPrefix() . "flip_gb_entry", $post, $post["id"])) {
			if ($writeid == $post["id"]) {
				$text = text_translate("Der G&auml;stebucheintrag") . ' "'.$post["title"].'" ' . text_translate("wurde ge&auml;ndert.");
			} else {
				$text = text_translate("Der G&auml;stebucheintrag") . ' "'.$post["title"].'" ' . text_translate("wurde eingetragen."); 
			}
			LogAction($text);
			return $text;
		} else {
			trigger_error_text(text_translate("Fehler beim Schreiben des G&auml;stebucheintrages."), E_USER_ERROR);
		}
		return false;
	}
	
	function getCodes() {
		return MysqlReadCol("SELECT text, icon FROM `".TblPrefix()."flip_gb_icons`","icon","text");
	}
	
	function requireAdd() {
		global $User;
		if(!$User->hasRight($this->right_admin)) {
			$User->requireRight($this->right_post);
		}
	}

	/****************************************************
	 * admin stuff
	 ****************************************************/

	function framedel($get) {
		$this->Caption = text_translate("Eintrag l&ouml;schen");
		$this->requireAdmin();
		$title = MysqlReadFieldByID(TblPrefix() . "flip_gb_entry", "title", $get["id"]);
		return array (
			"title" => text_translate($title
		), "id" => $get["id"]);
	}

	function submitdel($post) {
		$this->requireAdmin();
		$title = MysqlReadFieldByID(TblPrefix() . "flip_gb_entry", "title", $post["id"]);
		if (MysqlDeleteByID(TblPrefix() . "flip_gb_entry", $post["id"])) {
			$text = text_translate("Der Eintrag") . " \"$title\" " . text_translate("wurde gel&ouml;scht.");
			LogAction($text);
			return $text;
		}
		return false;
	}
	
	function frameicons() {
		$this->requireAdmin();
		
		return array("icons"=>MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_gb_icons`"));
	}
	
	function frameediticon($get) {
		$this->requireAdmin();
		
		$r = array();
		if(is_posDigit($get["id"])) {
			$r = MysqlReadRowByID(TblPrefix()."flip_gb_icons", $get["id"]);
		}
		
		$r["icons"] = $this->getIcons();
		
		return $r;
	}
	
	function submiticon($post) {
		$this->requireAdmin();
		if(!is_posDigit($post["id"]))
			$post["id"] = 0;
		if(MysqlWriteByID(TblPrefix()."flip_gb_icons", $post, $post["id"]))
			return true;
		else
			return false;
	}
	
	function frameEdit($g) {
		$this->requireAdmin();
		$this->Caption = 'Eintrag bearbeiten';
		if(!empty($g['id'])) {
			$id = escape_sqlData_without_quotes($g['id']);
			$r = MysqlReadRow("SELECT * FROM `".TblPrefix()."flip_gb_entry` WHERE `id` = '$id';");
			return !empty($r) ? $r : array();
		}
		trigger_error_text('Die Eintrags-ID wurde nicht angegeben!',E_USER_ERROR);
	}
	
	function submitEdit($g) {
		$this->requireAdmin();
		if(!empty($g) && is_array($g)) {
			if(MysqlWriteByID(TblPrefix().'flip_gb_entry',$g,$g['id']))
				return true;
		}
		return trigger_error_text('Der Eintrag konnte nicht gespeichert werden!',E_USER_ERROR);
	}
	
	function getIcons() {
		static $files = array();
		if(empty($files)) {
			//Pfade
			$tplPath = ConfigGet("template_dir").ConfigGet("template_customname");
			$iconpath = $this->iconpath;
			if(substr($iconpath, -1) != "/") {
				$iconpath .= "/";
			}
			$fullpath = $tplPath ."/". $iconpath;
			//Dateien
			$dir = opendir($fullpath);
			while(false !== ($entry = readdir($dir))) {
				if(is_file($fullpath.$entry)) {
					$files[$iconpath.$entry] = $entry;
				}
			}
		}
		return $files;
	}
	
	function requireAdmin() {
		global $User;
		$User->requireRight($this->right_admin);
	}
}

RunPage("GBPage");
?>