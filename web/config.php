<?php
/**
 * 
 * @author Moritz Eysholdt
 * @version $Id: config.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class ConfigPage extends Page {

	//php 7 public function __construct()
	//php 5 original function ConfigPage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		require_once ("mod/mod.config.php");
		$User->requireRight(ConfigEditRight());
	}

	function frameDefault($get, $post) {
		ArrayWithKeys($get, array("category"));
		$this->ShowContentCell = false;
		$this->Caption = "Config";
		$items = ConfigGetAll();
		$cols = GetColumns("user");
		$groups = array();

		//multiple choice
		$rawvalues = MysqlReadArea("SELECT config_id, value FROM ".TblPrefix()."flip_config_values");
		if (empty ($rawvalues))
			$rawvalues = array ();
		foreach ($rawvalues AS $avalue)
			$values[$avalue["config_id"]][$avalue["value"]] = $avalue["value"];
		
		// Kategorien raussortieren
		$cat = $get['category'];
		
		if(!empty($cat)) {
			$this->Caption .= " ($cat)";
			foreach($items as $i => $k) {
				$k_cat = preg_split('/_/', $k['key']);
				$k_cat = $k_cat[0];
				if($k_cat != $cat)
					unset($items[$i]);
			}
		}
		
		foreach ($items as $k => $v) {
			if (array_key_exists($k, $cols))
				$items[$k]["usercfg"] = true;
			$items[$k]["values"] = (isset($values[$v["id"]])) ? $values[$v["id"]] : null;
			//use first word in configname to group entries
			$tmp = preg_split('/_/', $v["key"]);
			$items[$k]["modul"] = $tmp[0];
		}
		
		$qs = empty($_SERVER['QUERY_STRING']) ? "" : "?".$_SERVER['QUERY_STRING'];
		
		return array ("items" => $items, "rparams" => $qs);
	}
	
	function frameCategories($g) {
    	$this->Caption = "Config (Kategorien)";
    	$vals = ConfigGetAll();
    	$c    = array();
    	$c_description = $c_customname = $c_id = array();
    	$prev = "";
    	
    	$categories = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_config_categories`;");
    	
    	foreach($categories as $category) {
    		$c_description[$category['name']] = htmlentities_single($category['description']);
    		$c_customname[$category['name']]  = htmlentities_single($category['custom_name']);
    		$c_id[$category['name']]		  = $category['id'];
    	}

	    foreach($vals as $k => $v) {
	    	$cat_name = preg_split('/_/', $k);
	    	$cat_name = $cat_name[0];
	    	if($cat_name != $prev) {
	    		$c[] = array("name" => $cat_name, 
							 "description" => isset($c_description[$cat_name]) ? $c_description[$cat_name] : '', 
							 "custom_name" => isset($c_customname[$cat_name]) ? $c_customname[$cat_name] : $cat_name,
							 "id" => (!isset($c_id[$cat_name]) ? "new" : $c_id[$cat_name]));
	    		$prev = $cat_name;
	      	}
		}
		
		return array("categories" => $c, "edit_right" => "config_edit");
	}
	
	// TODO
	function frameEditCategory($g) {
		$this->Caption = "Kategorie bearbeiten";
		if($g['id'] == "new") {
			$this->Caption = "Kategorie ($g[name]) neu benennen";
			$r['name'] = $g['name'];
			$r['custom_name'] = "Ersatzname";
			$r['description'] = "";
		} else {
			$id = escape_sqlData_without_quotes($g['id']);
			$r = MysqlReadRow("SELECT * FROM `".TblPrefix()."flip_config_categories` WHERE `id` = '$id';");
			unset($r['mtime']);
		}
		return $r;
	}
	
	function submitEditCategory($g) {
		if($g['id'] == "new") unset($g['id']);
		if(MysqlWriteByID(TblPrefix()."flip_config_categories",$g,$g['id'])) {
			LogAction("Die Kategorie '$g[custom_name]' wurde erfolgreich bearbeitet!");
			return true;
		} else {
			trigger_error_text("Die Kategorie '$g[custom_name]' konnte nicht bearbeitet werden!",E_USER_ERROR);
			return false;
		}
	}

	function frameEdit($get) {
		ArrayWithKeys($get, array("id","newfield"));
		if (empty ($get["id"]))
			return array ("key" => "new_key", "value" => "", "default_value" => "", "description" => "", "type" => "string");
		$cfg = ConfigGetLong($get["id"]);
		if (!$cfg["editable"])
			trigger_error_text("Der Config-Eintrag kann nicht bearbeitet werden.", E_USER_ERROR);

		$this->Caption = "Config -> edit $cfg[key]";

		$values = MysqlReadCol("SELECT value FROM ".TblPrefix()."flip_config_values WHERE config_id = '".escape_sqlData_without_quotes($get["id"])."'", "value", "value");
		if (empty ($values) && $get["newfield"] > 0)
			$cfg["values"] = array_merge(array ($cfg["value"]), $values);
		else
			$cfg["values"] = $values;
		if ($get["newfield"] > 0)
			for ($i = 0; $i < (int) $get["newfield"]; $i ++)
				$cfg["values"][] = "";

		return $cfg;
	}

	function submitItem($data) {
		if (!empty ($data["values"])) {
			//remove dropdown if only one option
			if(count($data["values"]) == 1
			   || (count($data["values"]) == 2
			     && ($data["values"][0] == ""
			         || $data["values"][1] == ""
			        )
			    )
			  )
			{
				MysqlWrite("DELETE FROM ".TblPrefix()."flip_config_values WHERE config_id = '".escape_sqlData_without_quotes($data["id"])."'");
				$data["values"] = $data["oldvalues"] = array ();
			}

			$currentvalue = MysqlReadFieldByID(TblPrefix()."flip_config", "value", $data["id"], true);
			if (!in_array($currentvalue, $data["values"]) || $currentvalue == "")
				$currentvalue = $data["default_value"];

			//modify config
			$r = ConfigSetByID($data["id"], $data["key"], $currentvalue, $data["default_value"], $data["description"], $data["type"]);

			//modify values (add/update/delete)
			$valueids = MysqlReadCol("SELECT `id`, `value` FROM `".TblPrefix()."flip_config_values`
			                                 WHERE `config_id` = '".escape_sqlData_without_quotes($data["id"])."'
			                                   AND `value` IN (".implode_sqlIn($data["oldvalues"]).")
			                               ", "id", "value");
			$delvalues = array ();
			foreach ($data["values"] AS $key => $value) {
				if (isset ($data["oldvalues"][$key]) && $value == "") //values and values must have the same keys/order!
					{
					$delvalues[] = escape_sqlData_without_quotes($data["oldvalues"][$key]);
				} else {
					$valueid = $valueids[$data["oldvalues"][$key]];
					MysqlWriteByID(TblPrefix()."flip_config_values", array ("config_id" => $data["id"], "value" => $value), $valueid);
					//2do: error handling and logging
				}
			}
			MysqlWrite("DELETE FROM ".TblPrefix()."flip_config_values
			                  WHERE config_id = '".escape_sqlData_without_quotes($data["id"])."'
			                    AND value IN (".implode_sqlIn($delvalues).")
			                 ");
		} else {
			$r = ConfigSetByID($data["id"], $data["key"], $data["value"], $data["default_value"], $data["description"], $data["type"]);
		}
		if ($r)
			LogChange("Der Config-Eintrag <a href=\"config.php?frame=edit&amp;id=$r\"><b>$data[key]</b></a> wurde ". ((empty ($data["id"])) ? "erstellt." : "bearbeitet."));
		return $r;
	}

	function submitConfig($data) {
		$r = true;
		foreach ($data as $k => $v) {
			$old = ConfigGet($k, NULL, "", false);
			if ($old != $v) {
				if (!ConfigSet($k, $v))
					$r = false;
				else
					LogChange("Der Wert des Config-Eintrages <a href=\"config.php\"><b>$k</b></a> wurde von <b>".htmlspecialchars($old)."</b> auf <b>".htmlspecialchars($v)."</b> ge&auml;ndert.");
			}
		}
		return $r;
	}

	function actionDelete($data) {
		$i = addslashes($data["id"]);
		$c = MysqlReadRow("SELECT `key`,`value` FROM `".TblPrefix()."flip_config` WHERE (`id` = '$i');");
		$r = ConfigDelete($data["id"]);
		if ($r)
			LogChange("Der Config-Eintrag <b>".escapeHtml("$c[key] ($c[value])")."</b> wurde gel&ouml;scht.");
		return $r;
	}
}

RunPage("ConfigPage");
?>
