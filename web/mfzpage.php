<?php

/**
 * @author Daniel Raap
 * @version $Id: mfzpage.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class mfzpage extends Page {
	function Page() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
	}

	function framedefault($get, $post) {
		$text = LoadText("lanparty_mitfahrzentrale", $this->Caption);
		$partyid = 10; // eure lanparty.de PartyID
		$data = array ();
		include ("ext/mfz.php");

		return array (
			"text" => $text,
			"data" => $data,
			"partyid" => $partyid
		);
	}
}

RunPage("mfzpage");
?>