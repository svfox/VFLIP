<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: netlog.php 1441 2007-07-30 10:19:14Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class NetLogPage extends Page {
	var $ViewNetlogRight = "netlog_view";

	function getHeader() {
		global $User;
		include_once ("mod/mod.lastvisit.php");
		$User->requireRight($this->ViewNetlogRight);
		$this->ShowContentCell = false;
		$this->Caption = "NetLog";
		return array (
			"items" => array (
				array (
					"default" => "Log",
					//            "alerts" => "Warnungen",
					"search" => "Suche",
					
				),
				array (
					"macs" => "MACs",
					"ips" => "IPs",
					
				),
				array (
					"nbnames" => "NBNames",
					"dnsnames" => "DNSNames",
					
				),
				array (
					"user" => "User",
					
				),
				array (
					"conflicts" => "IP-Konflikte",
					"wrong" => "falsche IPs",
					"unknownmacs" => "unbekannte MACs",
					//            "unknownmacs" => "unbekannte IPs",
					
				),
				array (
					//            "tools" => "Tools",
					"info" => "Info"
				)
			),
			"current" => $this->Frame,
			"last_visit" => doVisit("netlog_{$this->Frame}"
		));
	}

	function frameInfo() {
		$r = array (
		"header" => $this->getHeader());
		;
		$this->ShowContentCell = true;
		return $r;
	}

	function frameDefault($get, $post) {
		$orderby = (empty ($get["orderby"])) ? "time DESC" : addslashes((($get["orderby"] == "user") ? "u.name" : "l.$get[orderby]") . " " . $get["order"]);
		$limit = (empty ($get["limit"])) ? "0,100" : addslashes($get["limit"]);
		$items = MysqlReadArea("
		      SELECT l.*, u.name FROM (`" . TblPrefix() . "flip_netlog_log` l)
		        LEFT JOIN `" . TblPrefix() . "flip_user_subject` u ON (u.id = l.user_id) 
		        ORDER BY $orderby
		        LIMIT $limit;
		    ");
		return array (
		"header" => $this->getHeader(), "items" => $items, "foundrows" => MysqlReadField("SELECT COUNT(`id`) FROM `" . TblPrefix() . "flip_netlog_log`;"),);
	}

	function getItemsByColumn($ColName) {
		$get = $this->FrameVars;
		$orderby = (empty ($get["orderby"])) ? "$ColName DESC" : addslashes($get["orderby"] . " " . $get["order"]);
		$limit = (empty ($get["limit"])) ? "0,100" : addslashes($get["limit"]);
		$c = addslashes($ColName);

		return MysqlReadArea("
		      SELECT l.*, u.name FROM (`" . TblPrefix() . "flip_netlog_status` s)
		        LEFT JOIN `" . TblPrefix() . "flip_netlog_log` l ON (s.log_id = l.id)
		        LEFT JOIN `" . TblPrefix() . "flip_user_subject` u ON (u.id = l.user_id)
		        WHERE (s.key = '$c')
		        ORDER BY $orderby
		        LIMIT $limit;    
		    ", $ColName);
	}

	function getListByColumn($ColName) {
		$c = addslashes($ColName);
		return array (
		"header" => $this->getHeader(), "items" => $this->getItemsByColumn($ColName), "foundrows" => MysqlReadField("SELECT COUNT(`id`) FROM `" . TblPrefix() . "flip_netlog_status` WHERE (`key` = '$c');"));
	}

	function frameMACs($get) {
		return $this->getListByColumn("mac");
	}

	function frameIPs($get) {
		return $this->getListByColumn("ipv4");
	}

	function frameNBNames($get) {
		return $this->getListByColumn("nbname");
	}

	function frameDNSNames($get) {
		return $this->getListByColumn("dnsname");
	}

	function frameUser($get) {
		return $this->getListByColumn("user_id");
	}

	function frameConflicts($get) {
		$Items = $this->getItemsByColumn("mac");
		$Tmp = $Conf = array ();
		foreach ($Items as $i)
			if (isset ($Tmp[$i["ipv4"]]))
				$Tmp[$i["ipv4"]] = true;
			else
				$Tmp[$i["ipv4"]] = false;
		foreach ($Items as $i)
			if ($Tmp[$i["ipv4"]])
				$Conf[] = $i;
		return array (
		"header" => $this->getHeader(), "items" => $Conf);
	}

	function frameWrong($get) {
		$ips = $this->getItemsByColumn("user_ip");
		$macs = $this->getItemsByColumn("mac");
		$items = $tmp = array ();

		// alle ips raussuchen, die aktuell falsch sind und deren mac noch immer die falcshe adresse inne hat.
		foreach ($ips as $ip => $dat)
			if (($ip != $dat["ipv4"]) and ($macs[$dat["mac"]]["ipv4"] != $ip))
				$tmp[$ip] = $dat;

		// &uuml;berpr&uuml;fen, ob dem subject nicht inzwischen eine neue IP zugewiesen wurde    
		include_once ("mod/mod.user.php");
		foreach ($tmp as $userip => $dat) {
			$ip = UserGetIP($dat["user_id"]);
			if (($ip != $dat["ipv4"]) and ($macs[$dat["mac"]]["ipv4"] != $ip))
				$items[$ip] = array (
					"user_ip" => $ip
				) + $dat;
		}

		// pr&uuml;fen, ob vllt gerade ein andere Rechner mit der richitgen MAC online ist.      
		foreach ($items as $userip => $v) {
			$mac = MysqlReadField("SELECT `mac` FROM `" . TblPrefix() . "flip_netlog_log` WHERE ((`ipv4` = '$userip') AND (`user_ip` = '$userip')) ORDER BY `time` DESC;", "mac", true);
			if ($mac)
				if ($macs[$mac]["ipv4"] == $userip)
					$items[$userip]["correct"] = $macs[$mac];
		}
		return array (
		"header" => $this->getHeader(), "items" => $items,);
	}

	function frameUnknownmacs($get) {
		$macs = $this->getItemsByColumn("mac");
		$items = array ();
		foreach ($macs as $v)
			if (empty ($v["nic_vendor"]))
				$items[] = $v;
		return array (
		"header" => $this->getHeader(), "items" => $items,);
	}

	function frameSearch($get) {
		$Columns = array (
			"all" => "Alle",
			"mac" => "MAC",
			"ipv4" => "IPv4",
			"hostname" => "Hostname",
			"dnsname" => "Domainname",
			"nbname" => "NetBIOS-Name",
			"nbunames" => "NetBIOS-UserName",
			"nbgroup" => "NetBIOS-Group",
			"user" => "User",
			"action" => "Action",
			"user_ip" => "Zugewiesene IP",
			"user_seat" => "Sitzplatz",
			"useragent" => "UserAgent",
			"nic_vendor" => "NIC-Hersteller"
		);
		ArrayWithKeys($get, array("search","type","column","orderby","limit"));
		$val = addslashes($get["search"]);
		if (empty ($val)) {
			$Items = array ();
			$count = 0;
		} else {
			if (empty ($get["type"]))
				$get["type"] = "contains";
			switch ($get["type"]) {
				case ("contains") :
					$type = "<col> LIKE '%$val%'";
					break;
				case ("matches") :
					$type = "STRCMP(<col>,'$val') = 0";
					break;
				case ("regex") :
					$type = "<col> REGEXP '$val'";
					break;
				default :
					trigger_error_text("Der Such-Typ ist ung&uuml;ltig|Type:$get[type]", E_USER_ERROR);
			}
			if (empty ($get["column"]))
				$get["column"] = "all";
			switch ($get["column"]) {
				case ("all") :
					$a = array ();
					$c = $Columns;
					unset ($c["all"]);
					unset ($c["user"]);
					foreach ($c as $k => $v)
						$a[] = strtr($type, array (
							"<col>" => "n.$k"
						));
					if (is_posDigit($val))
						$a[] = "n.user_id = $val";
					$a[] = strtr($type, array (
						"<col>" => "u.name"
					));
					$where = "(" . implode(") OR (", $a) . ")";
					break;
				case ("user") :
					$a = array ();
					if (is_posDigit($val))
						$a[] = "n.user_id = $val";
					$a[] = strtr($type, array (
						"<col>" => "u.name"
					));
					$where = "(" . implode(") OR (", $a) . ")";
					break;
				default :
					if (!isset ($Columns[$get["column"]]))
						trigger_error_text("Die Spalte ist ung&uuml;ltig|Column:$get[column]", E_USER_ERROR);
					$where = strtr($type, array (
						"<col>" => "n.$get[column]"
					));
			}
			$orderby = (empty ($get["orderby"])) ? "time DESC" : addslashes((($get["orderby"] == "user") ? "u.name" : "l.$get[orderby]") . " " . $get["order"]);
			$limit = (empty ($get["limit"])) ? "0,100" : addslashes($get["limit"]);
			$Items = MysqlReadArea("
			        SELECT n.*, u.name FROM (`" . TblPrefix() . "flip_netlog_log` n)
			          LEFT JOIN `" . TblPrefix() . "flip_user_subject` u ON (u.id = n.user_id)
			          WHERE ($where)
			          ORDER BY $orderby
			          LIMIT $limit;
			      ");
			$count = MysqlReadField("
			        SELECT COUNT(n.id) FROM (`" . TblPrefix() . "flip_netlog_log` n)
			          LEFT JOIN `" . TblPrefix() . "flip_user_subject` u ON (u.id = n.user_id) 
			          WHERE ($where);
			      ");
		}
		return array (
		"header" => $this->getHeader(), "items" => $Items, "types" => array (
			"contains" => "Enh&auml;lt",
			"matches" => "Entspricht",
			"regex" => "RegExp"
		), "columns" => $Columns, "foundrows" => $count);
	}

	function actionExec() {
		include_once ("mod/mod.netlog.php");
		include_once ("mod/mod.netlog.protocol.php");

		// client authentifizieren
		$this->_ShowFrame = false;
		$p = new NetLogPacket();
		if (NetLogIsAuthorizedClient()) {
			// Befehl bearbeiten      
			if ($p->parse(GetRawPostData()))
				switch ($cmd = $p->Command) {
					case ("submit_data") :
						NetLogSaveData($p->Keys);
						$ip = $p->Keys["ipv4"];
						break;
					default :
						trigger_error_text("Unbekannter netlog-Befehl|cmd:{$p->Command}", E_USER_WARNING);
				}
		} else
			trigger_error_text("Der Client ist nicht authorisiert in den NetLog zu schreiben.|IP:$_SERVER[REMOTE_ADDR]", E_USER_WARNING);

		NetLogCheckTime($p->Keys["time"]);

		$r = $p->buildResponse();
		if ($r)
			LogAction("Netlogbefehl $cmd mit $ip von $_SERVER[REMOTE_ADDR] entgegengenommen. Antwort: $r");

		header("Content-Type: application/x-netlog");
		header("Content-Length: " . strlen($r));
		echo $r;
	}

	function submitclearlog() {
		global $User;

		$User->requireRight($this->ViewNetlogRight);

		if (!MysqlExecQuery("TRUNCATE TABLE `" . TblPrefix() . "flip_netlog_log`"))
			trigger_error_text("Fehler beim Bereiningen des NetLogs", E_USER_WARNING);
		if (!MysqlExecQuery("TRUNCATE TABLE `" . TblPrefix() . "flip_netlog_status`"))
			trigger_error_text("Fehler beim Bereiningen des Status-NetLogs", E_USER_WARNING);
		else
			trigger_error_text("Log bereinigt!", E_USER_NOTICE);
	}
}

RunPage("NetLogPage");
?>
