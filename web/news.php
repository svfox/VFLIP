<?php


/**
 * @author Daniel Raap
 * @version $Id: news.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class NewsPage extends Page {
	//RSS Modul
	var $rsslib = "ext/rss10.inc.php";
	var $atomlib = "inc/inc.feed.atom.php";
	var $feedTextLength = 500;
	//Config
	var $pagetitleconfig = "page_title";
	var $lanpartytitleconfig = "lanparty_fulltitle";
	var $newsperpageconfig = "news_perpage";
	var $usecategoriesconfig = "news_use_categories";
	//Rechte
	var $editright = "news_post";
	var $postright = "news_public";
	var $internright = "view_internal_informations";
	var $commentright = "news_comments";
	var $commenteditright = "news_editcomments";
	//texte
	var $archivtext = "news_archiv";

	//php 7 public function __construct()
	//php 5 original function NewsPage()
	function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$this->Caption = text_translate("Neuigkeiten");
	}

	function framedefault($get, $post) {
		global $User;

		//seitenweise
		$newsperpage = ConfigGet($this->newsperpageconfig);
		if (!is_posDigit($newsperpage))
			$newsperpage = 10;
		if (empty ($get["more"]))
			$limit = 0;
		else
			$limit = (integer) $get["more"];

		$w = "";
		if (!empty ($get["newsid"])) {
			//nur eine News
			$w = "WHERE id='" . addslashes($get["newsid"]) . "'";
		}
		elseif (!empty ($get["category"])) {
			//nur eine Kategorie
			$w = "WHERE category='" . addslashes($get["category"]) . "'";
		}

		//Anzahl der Kommentare
		$commentcount = MysqlReadCol("SELECT news_id, COUNT(news_id) FROM `" . TblPrefix() . "flip_news_comments` GROUP BY news_id", "COUNT(news_id)", "news_id");

		//News auslesen
		foreach (MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_news_news` $w ORDER BY on_top DESC, date DESC LIMIT $limit," . $newsperpage) AS $data) {
			$data["commentcount"] = isset($commentcount[$data["id"]]) ? $commentcount[$data["id"]] : 0;
			if ($data["view_right"] == GetRightID($this->internright)) {
				//Intern
				$data["intern"] = true;
				if ($User->hasRight($this->editright))
					$data["edit"] = "true";
			} else {
				$data["intern"] = false;
			}
			if ($User->hasRight($data["view_right"]))
				$r["news"][] = $data;
		}

		//Default wenn keine News vorhanden sind
		if (empty ($r))
			$r = array (
				"news" => array (
					array (
					"id" => 0,
					"date" => time(),
					"title" => text_translate("Noch kein Newseintrag vorhanden."),
					"author" => GetSubjectName(SYSTEM_USER_ID),
					"text" => text_translate("Bisher wurde noch kein Newseintrag erstellt."),
					"intern" => false,
					"category" => isset($get["category"]) ? $get["category"] : null)));

		//Links:
		//hinzuf&uuml;gen Link
		if ($User->hasRight($this->editright) || $User->hasRight($this->postright))
			$r["admin"] = text_translate("Newseintrag erstellen");
		//public Recht
		if ($User->hasRight($this->postright))
			$r["spellchecker"] = "true";
		//Links zu n&auml;chster/vorheriger Seite
		if (MysqlReadField("SELECT COUNT(*) FROM `" . TblPrefix() . "flip_news_news`", "COUNT(*)") - $limit > $newsperpage)
			$r["more"] = $limit + $newsperpage;
		if ($limit < $newsperpage && $limit > 0)
			$r["less"] = 0;
		elseif ($limit >= $newsperpage) $r["less"] = $limit - $newsperpage;
		if (!empty ($get["newsid"]))
			unset ($r["more"]);

		//Kategorien
		$r["usecat"] = (ConfigGet($this->usecategoriesconfig) == 'Y');
		$r["category"] = isset($get["category"]) ? $get["category"] : "";

		return $r;
	}

	function frameedit($get) {
		global $User;
		$User->requireRight($this->editright);
		if ($User->hasRight($this->postright))
			$newsposter = "true";
		else
			$newsposter = "";
		if (!empty ($get["id"]) && $data = MysqlReadRowByID(TblPrefix() . "flip_news_news", $get["id"])) {
			$data["newsposter"] = $newsposter;
			if ($data["view_right"] != GetRightID($this->internright))
				$data["public"] = "1";
			return $data;
		} else {
			return array (
				"view_right" => $this->internright,
				"newsposter" => $newsposter
			);
		}
	}

	function submitnews($action) {
		global $User;
		$User->requireRight($this->editright);
		if (empty ($action["id"]))
			$action["id"] = 0;

		if (empty ($action["date"])) {
			$action["date"] = time();
		}

		if (empty ($action["author"]))
			$action["author"] = $User->name;
		if ($action["public"] == "1" && $User->hasRight($this->postright))
			$action["view_right"] = 0;
		else
			$action["view_right"] = GetRightID($this->internright);
		if (empty ($action["view_right"]))
			$action["view_right"] = 0;
		unset ($action["public"]);
		if (!stristr($action["text"], "<br"))
			$action["text"] = nl2br($action["text"]);
		if ($writeid = MysqlWriteByID(TblPrefix() . "flip_news_news", $action, $action["id"]))
			if ($writeid == $action["id"])
				return LogChange(text_translate("Der <strong>Newseintrag</strong> \"?1?\" wurde ge&auml;ndert.", $action["title"]));
			else
				return LogChange(text_translate("Der <strong>Newseintrag</strong> \"?1?\" wurde eingetragen.",$action["title"]));
		else
			trigger_error_text(text_translate("Fehler beim schreiben."), E_USER_ERROR);
		return false;
	}

	function framecomments($get) {
		global $User;

		//Newsdata
		$r = MysqlReadRowByID(TblPrefix() . "flip_news_news", $get["id"]);

		//Commentdata
		$r["comments"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_news_comments` WHERE news_id='" .escape_sqlData_without_quotes($get["id"]) . "' ORDER BY `timestamp` ASC;");
		$users = MysqlReadCol("SELECT s.id, s.name FROM `" . TblPrefix() . "flip_news_comments` c LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON c.user_id=s.id WHERE c.news_id='" .escape_sqlData_without_quotes($get["id"]) . "'", "name", "id");
		foreach ($r["comments"] AS $key => $comment)
			$r["comments"][$key]["username"] = $users[$comment["user_id"]];

		//Rights
		if ($User->hasRight($this->commentright))
			$r["cancomment"] = "true";
		$r["editright"] = $this->commenteditright;
		$r["usecat"] = (ConfigGet($this->usecategoriesconfig) == 'Y');

		return $r;
	}

	function actiondelcomment($post) {
		global $User;
		$User->requireRight($this->commenteditright);
		foreach ($post["ids"] AS $id)
			MysqlDeleteByID(TblPrefix() . "flip_news_comments", $id);
		return true;
	}

	function frameaddcomment($get) {
		global $User;
		$User->requireRight($this->commentright);

		$title = MysqlReadFieldByID(TblPrefix() . "flip_news_news", "title", $get["id"]); //also a check for valid ID ;-)

		$this->Caption = text_translate("Kommentar zu \"?1?\"", $title);
		return array (
			"id" => $get["id"],
			"title" => $title
		);
	}

	function submitcomment($post) {
		global $User;
		$User->requireRight($this->commentright);

		$post["news_id"] = $post["id"];
		unset ($post["id"]);
		$post["timestamp"] = time();
		$post["user_id"] = $User->id;
		//HTML escapen und <br>s einf&uuml;gen (HTML wird dargestellt)
		$post["text"] = escapeHtml($post["text"]);
		$post["text"] = nl2br($post["text"]);
		MysqlWriteByID(TblPrefix() . "flip_news_comments", $post);
	}

	function framedel($get) {
		$this->Caption = text_translate("Newseintrag l&ouml;schen");
		global $User;
		$User->requireRight($this->editright);
		$title = MysqlReadFieldByID(TblPrefix() . "flip_news_news", "title", $get["id"]);
		return array (
			"title" => text_translate($title
		), "id" => $get["id"]);
	}

	function submitdel($post) {
		global $User;
		$User->requireRight($this->editright);
		$title = MysqlReadFieldByID(TblPrefix() . "flip_news_news", "title", $post["id"]);
		if (MysqlDeleteByID(TblPrefix() . "flip_news_news", $post["id"]))
			return LogChange(text_translate("Der <strong>Newseintrag</strong> \"?1?\" wurde gel&ouml;scht.", $title));
		return false;
	}

	function framearchiv($get) {
		global $User;
		ArrayWithKeys($get, array("category")); 

		$r["text"] = LoadText($this->archivtext, $this->Caption);

		//Kategorie-Filter
		if (!empty ($get["category"]))
			$w = "WHERE category='" .escape_sqlData_without_quotes($get["category"]) . "'";
		else
			$w = "";

		foreach (MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_news_news` $w ORDER BY date DESC") AS $data) {
			if ($data["view_right"] == GetRightID($this->internright))
				$data["title"] = text_translate("Intern") . ": " . $data["title"];
			if ($User->hasRight($data["view_right"]))
				$r["news"][] = $data;
		}
		//Kategorie
		$r["usecat"] = (ConfigGet($this->usecategoriesconfig) == 'Y');
		$r["category"] = $get["category"];
		return $r;
	}

	function framerss() {
		global $User;
		$this->ShowMenu = false;
		include_once ($this->rsslib);

		/* Global */
		$textlen = $this->feedTextLength;
		$rssfeeds = (int) ConfigGet($this->newsperpageconfig);
		$rss = new RSSWriter("http://" . $_SERVER["HTTP_HOST"], ConfigGet($this->pagetitleconfig) . " - " . text_translate("News"), text_translate("News von ?1?", ConfigGet($this->lanpartytitleconfig)), array (
			"dc:creator" => "http://usefulinc.com/rss/rsswriter/"
		));

		/* News */
		$url = ServerUrl().array_shift(explode("?", GetRequestUri()));
		foreach (MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_news_news` WHERE " . $User->sqlHasRight("view_right") . " ORDER BY date DESC LIMIT " . $rssfeeds) AS $data) {
			$data["text"] = $rss->deTag($data["text"]);
			if (strlen($data["text"]) > $textlen -3)
				$data["text"] = substr($data["text"], 0, $textlen -3) . "...";
			$rss->addItem("$url?newsid=".$data["id"], date("d.m.Y", $data["date"]) . " " . $data["title"], array (
				"description" => $data["text"]
			));
		}
		$rss->serialize();
		return true;
	}

	function frameatom() {
		global $User;
		
		/* Global */
		$rssfeeds = (int) ConfigGet($this->newsperpageconfig);
		$newsurl = ServerUrl().array_shift(explode("?", GetRequestUri()));
		
		include_once($this->atomlib);
		$atom = new atomFeed(ConfigGet($this->pagetitleconfig) . " - " . text_translate("News"), time(), ServerURL().GetRequestUri());
		
		foreach (MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_news_news` WHERE " . $User->sqlHasRight("view_right") . " ORDER BY date DESC LIMIT " . $rssfeeds) AS $data) {
			$data["text"] = $this->shortText($data["text"]);
			$entry = new atomEntry($data["title"], $data["mtime"], $newsurl."?newsid=".$data["id"], $data["author"]);
			$entry->setContent(array("type"=>"html", "text"=>$this->shortText($data["text"])));
			$entry->setPublished($data["date"]);
			if(ConfigGet($this->usecategoriesconfig) == 'Y') {
				include_once("inc/inc.table.php");
				$cat = TableGetDisplay("news_category", $data["category"]);
				$entry->addCategory($cat);
			}
			$atom->add($entry);
		}
		
		$r = $atom->create();
		
		//atom-template
		$this->ShowMenu = false;
		$this->TplFile = $atom->tplFile;
		$this->TplSub = $atom->tplSub;
		return $r;
	}
	
	/**
	 * K&uuml;rzt einen Text auf eine festgelegte Anzahl von Zeichen
	 * 
	 * TODO: kein ungeschlossenes HTML-Tag &uuml;briglassen
	 */
	function shortText($text) {
		$textlen = $this->feedTextLength;
		if (strlen($text) > $textlen -3) {
				return substr($text, 0, $textlen -3) . "...";
		} else {
			return $text;
		}
	}

	function frameSmallNews($get) {
		global $User;
		$count = (empty ($get["count"])) ? 5 : addslashes($get["count"]);

		//News auslesen
		$news = MysqlReadArea("SELECT `id`,`author`,`date`,`title`,`view_right` FROM `" . TblPrefix() . "flip_news_news` ORDER BY `date` DESC LIMIT $count;");

		//keine News
		if (empty ($news))
			$news = array (
				array (
					"title" => text_translate("keine News"
				)
			));

		//Rechtecheck
		$intern = GetRightID($this->internright);
		foreach ($news as $k => $v) {
			if (!$User->hasRight($v["view_right"]))
				unset ($news[$k]);
			if ($v["view_right"] == $intern)
				$news[$k]["intern"] = true;
			else
				$news[$k]["intern"] = false;
		}
		return array (
			"items" => $news
		);
	}
}

RunPage("NewsPage");
?>