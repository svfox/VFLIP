<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: sendmessage.php 1702 2019-01-09 09:01:12Z loom$ - edit by Tominator und naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");
require_once ("inc/inc.form.php");
require_once ("mod/mod.sendmessage.php");

class SendMessagePage extends Page {
	var $SendRight = "sendmessage_use";

	var $SysText = "sendmessage_sys";
	var $UserText = "sendmessage_user";
	var $EditMessageText = "sendmessage_edit_message";
	var $ViewMessageText = "sendmessage_view_message";
	var $SendMessageText = "sendmessage_send_message";

	//php 7 public function __construct()
	//php 5 original function SendMessagePage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$User->requireRight($this->SendRight);
	}

	function frameDefault($get, $post) {
		global $User;
		ArrayWithKeys($get, array("type"));
		$type = ($get["type"] == "sys") ? "sys" : "user";
		$text = ($get["type"] == "sys") ? $this->SysText : $this->UserText;
		return array (
			"text" => LoadText($text,
			$this->Caption
		), "type" => $type, "mails" => MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_sendmessage_message` WHERE (`type` = '$type') ORDER BY `name`;"), "user" => $User->id);
	}

	function frameEditMessage($get) {
		$this->Caption = "Nachricht bearbeiten";
		ArrayWithKeys($get, array("id"));
		if (empty ($get["id"]))
			$r = array (
				"name" => "new_message"
			);
		else
			$r = LoadMessageFromDB($get["id"]);
		$r["text"] = LoadText($this->EditMessageText, $this->Caption);
		return $r;
	}

	function submitMessage($data) {
		include_once ("mod/mod.template.php");
		ArrayWithKeys($data, array("message","subject","id"));
		if (TemplateCompile($data["message"], "Nachricht") and TemplateCompile($data["subject"], "Betreff")) {
			if ($newid = MysqlWriteByID(TblPrefix() . "flip_sendmessage_message", $data, $data["id"]))
				return LogChange("Die <b>Nachricht <a href=\"sendmessage.php?frame=editmessage&amp;id=$newid\">$data[name]</a></b> wurde " . ((empty ($data["id"])) ? "erstellt." : "bearbeitet."));
		}
		return false;
	}

	function actionDeleteMessage($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$name = MysqlReadFieldByID(TblPrefix() . "flip_sendmessage_message", "name", $id);
				if (MysqlDeleteByID(TblPrefix() . "flip_sendmessage_message", $id))
					LogChange("Die <b>Nachricht \"$name\"</b> wurde gel&ouml;scht.");
			}
	}

	function frameViewMessage($get) {
		$r = array (
			"id" => $get["id"],
			"text" => LoadText($this->ViewMessageText,
			$this->Caption
		), "type" => MysqlReadFieldByID(TblPrefix() . "flip_sendmessage_message", "type", $get["id"]));

		$m = new ExecMessage();
		$m->messageFromDB($get["id"]);
		$m->Receiver = new User($get["user"]);
		if (!$m->getMessage($r["subject"], $r["message"]))
			trigger_error("Fehler beim generieren der Nachricht.", E_USER_WARNING);
		return $r;
	}

	function frameSend($get) {
		if (empty ($get["id"]))
			$r = array (
				"vars" => implode(", ",
			GetMessageVars()), "subject" => "", "message" => "");
		else
			$r = LoadMessageFromDB($get["id"]);
		$r["to"] = ($get["to"] == "Group") ? "group" : "user";
		$r["changeto"] = ($r["to"] == "group") ? "User" : "Group";
		$r["text"] = LoadText($this->SendMessageText, $this->Caption);
		$r["messagetypes"] = GetMessageTypes();
		return $r;
	}

	function frameSendcheckin($get) {		
		if (empty ($get["id"]))
			$r = array (
				"vars" => implode(", ",
			GetMessageVars()), "subject" => "", "message" => "");
		else
			$r = LoadMessageFromDB($get["id"]);
		$r["to"] = ($get["to"] == "Group") ? "group" : "user";
		$r["changeto"] = ($r["to"] == "group") ? "User" : "Group";
		$r["text"] = LoadText($this->SendMessageText, $this->Caption);
		$r["messagetypes"] = GetMessageTypes();
				
		$userId = $get["userid"];
		if ($userId != NULL) {
			$UserSubject = CreateSubjectInstance($userId, "user");
		}
		$SelectedUsers = array();
		if ($UserSubject != NULL)
		{		
		  $SelectedUsers += array ($userId => $UserSubject->name);
		  $r["userid"] = $SelectedUsers;
		}
		
		return $r;
	}

	function frameSending($g, $a) {
		global $Session;
		$this->Caption = "Sende...";
		$rec = CreateSubjectInstance($a["receiver"]);
		if (!is_object($rec))
			return trigger_error("Die ID des Subjekts ist ung&uuml;ltig|ID:$a[receiver]", E_USER_ERROR);

		if (is_a($rec, "ParentSubject")) {
			$users = array ();
			$childs = $rec->getChilds();
			if ($a["forceenabled"]) {
				$enabled = GetSubjects("user", array (
					"id",
					"enabled"
				));
				foreach ($childs as $id => $name)
					if ($enabled[$id]["enabled"] == "N")
						unset ($childs[$id]);
			}
			foreach (array_chunk($childs, 5, true) as $chunk) {
				$c = array ();
				foreach (array_keys($chunk) as $id)
					$c[] = "ids[]=$id";
				$users[] = array (
					"ids" => implode("&",
					$c
				), "names" => implode(", ", $chunk));
			}
		} else
			$users = array (
				array (
					"ids" => "ids[]={$rec->id}",
					"names" => $rec->name
				)
			);

		$m = new ExecMessage();
		$m->Type = $a["messagetype"];
		$m->setMessage($a["subject"], $a["message"]);
		$m->IsHtml = ($a["sendhtml"] > 0) ? true : false;
		$Session->Data["sendmessage"] = serialize($m);
		LogChange("Die <b>Nachricht</b> \"" . $a["subject"] . "\" wurde an <b>{$rec->name}</b> versandt.");
		
		return array (
			"subject" => $a["subject"],
			"users" => $users,
		"time" => time());
	}

	function frameSendto($a) {
		global $Session;
		LogCacheActivate();
		$err = false;
		$m = unserialize($Session->Data["sendmessage"]);

		if (is_array($a["ids"]))
			foreach ($a["ids"] as $id) {
				$u = CreateSubjectInstance($id, "user");
				if (is_object($u)) {
					if (!$m->sendMessage($u)) {
						trigger_error("Beim Senden einer Nachricht an {$u->name}[{$u->id}] ist ein Fehler aufgetreten.", E_USER_WARNING);
						$err = true;
					}
				} else {
					trigger_error("Eine SubjectID ist nicht die eines Users.|ID:$id", E_USER_WARNING);
					$err = true;
				}
			}
		$im = ImageCreate(8, 8);
		if ($err)
			$color = ImageColorAllocate($im, 255, 0, 0);
		else
			$color = ImageColorAllocate($im, 0, 255, 0);
		imagefill($im, 2, 2, $color);
		
		return $im;
	}

	function frameResult() {
		global $Session;
		$this->Caption = "Sendereport";
		unset ($Session->Data["sendmessage"]);
		LogCacheFlush();
		return array (
		"errors" => (is_array(getErrors())) ? true : false);
	}
}

RunPage("SendMessagePage");
?>
