<?php
/**
 * @author Daniel Raap
 * @version $Id: poll.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once("inc/inc.page.php");
require_once("inc/inc.text.php");

class pollPage extends Page {
	//Config
	var $durationconfig = "poll_duration";
	//Rechte
	var $adminright = "poll_admin";
	var $loggedin = "logged_in";
	//Texte
	var $loggedouttext = "user_logged_out";
	var $text = "poll_main";
	//intern Config
	var $pollduration;
	var $barwidth = 500; //Pixel

	//php 7 public function __construct()
	//php 5 original function pollPage()
	function __construct() {
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		$this->pollduration = ConfigGet($this->durationconfig);
		$this->Caption = "Umfrage";
	}

	function hasvoted4($pollid) {
		global $User;
		$hasvoted = MysqlReadField("SELECT COUNT(id) FROM ".TblPrefix()."flip_poll_users WHERE user_id='".$User->id."' AND poll_id='".escape_sqlData_without_quotes($pollid)."'", "COUNT(id)");
		if ($hasvoted > 0)
			return true;
		else
			return false;
	}

	function framedefault($get, $post) {
		global $User;

		$tpl["text"] = LoadText($this->text, $this->Caption);
		if (!$User->hasRight($this->loggedin)) {
			$tmp = null;
			$tpl["loggedout"] = LoadText($this->loggedouttext, $tmp);
		}

		$i = (integer) 0;
		$polllist = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_list ORDER BY time DESC");
		foreach ($polllist AS $poll) {
			if (!$User->hasRight($poll["view_right"]) && !$User->hasRight($this->adminright))
				continue;
			$tpl["polls"][$i] = $poll;
			$tpl["polls"][$i]["class"] = ($i % 2 == 1) ? "row1" : "row2";
			$endtime = $poll["time"] + $this->pollduration * 3600 * 24;
			if (time() <= $endtime) {
				$endtime = floor(($endtime -time()) / 3600 / 24)." Tage";
				if ($User->hasRight($this->loggedin) && $User->hasRight($poll["view_right"]) && !$this->hasvoted4($poll["id"]))
					$tpl["polls"][$i]["can_vote"] = "true";
			} else {
				$endtime = date("d.m.Y", $endtime);
			}
			$tpl["polls"][$i]["date"] = $endtime;
			$tpl["polls"][$i]["votes"] = MysqlReadField("SELECT sum(count) FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($poll["id"])."'", "sum(count)");
			$i ++;
		}

		if ($User->hasRight($this->adminright)) {
			$tpl["admin"] = "true";
		}

		$tpl["editright"] = $this->adminright;

		return $tpl;
	}

	function framepoll($get) {
		global $User;
		if (!$get["id"] > 0)
			return trigger_error("Poll nicht vorhanden. <a href=\"poll.php\">zur Poll-Liste</a>", E_USER_ERROR);

		$poll = MysqlReadRowByID(TblPrefix()."flip_poll_list", $get["id"]);
		$User->requireRight($poll["view_right"]);
		// Ergebnisse
		$stimmen = MysqlReadField("SELECT sum(count) FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($poll["id"])."'", "sum(count)");
		$poll["stimmen"] = $stimmen;
		$votes = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($poll["id"])."' ORDER BY id");
		$i = 0;
		foreach ($votes AS $vote) {
			if ($stimmen > 0) {
				$percent = round($vote["count"] / $stimmen * 100, 1)."%";
				$breite = floor($vote["count"] / $stimmen * $this->barwidth);
			} else {
				$breite = floor($this->barwidth / 2);
			}
			$poll["results"][$i]["txt"] = $vote["text"];
			$poll["results"][$i]["breite"] = $breite;
			$poll["results"][$i]["percent"] = $percent;
			$i ++;
		}
		// Zeit
		$endtime = $poll["time"] + $this->pollduration * 3600 * 24;
		if (time() <= $endtime) { //Wenn Zeit noch nicht abgelaufen ist: Link
			$day2end = floor(($endtime -time()) / 3600 / 24);
			$poll["votetext"] = "Poll l&auml;uft noch bis zum ".date("d.m.Y", $endtime)." ($day2end Tage).";
			if ($User->hasRight($this->loggedin) && !$this->hasvoted4($poll["id"])) {
				$poll["logged_in"] = "true";
			}
		} else {
			$poll["votetext"] = "Umfrage ist beendet.";
		}

		return $poll;
	}

	function actionvote($post) {
		global $PHP_SELF, $User;
		$pollid = $this->FrameVars["id"];
		if ($pollid == "")
			trigger_error("Es wurde kein Poll angegeben. <a href=\"poll.php\">zur Poll-Liste</a>", E_USER_ERROR);
		$User->requireRight(MysqlReadFieldByID(TblPrefix()."flip_poll_list", "view_right", $pollid));
		if ($this->hasvoted4($pollid))
			return trigger_error("Du hast schon gew&auml;hlt. <a href=\"poll.php\">zur Poll-Liste</a>", E_USER_WARNING);

		MysqlWrite("UPDATE ".TblPrefix()."flip_poll_votes SET count=count+1 WHERE id='".escape_sqlData_without_quotes($post["vote"])."'");
		MysqlWrite("INSERT INTO ".TblPrefix()."flip_poll_users (user_id, poll_id) VALUES ('".$User->id."', '".escape_sqlData_without_quotes($pollid)."')");
	}

	function framevote() {
		global $User;
		$id = $this->FrameVars["votefor"];
		if ($this->hasvoted4($id))
			return trigger_error("Du hast schon gew&auml;hlt. <a href=\"poll.php\">zur Poll-Liste</a>", E_USER_WARNING);
		if ($id > 0 && $poll = MysqlReadRowByID(TblPrefix()."flip_poll_list", $id)) {
			$this->Caption = "Stimmenabgabe";
			$j = (integer) 0; // ???
			$User->requireRight($poll["view_right"]);
			// Ergebnisse
			$votes = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($poll["id"])."' ORDER BY id");
			$i = 0;
			foreach ($votes AS $vote) {
				$poll["results"][$i]["txt"] = $vote["text"];
				$poll["results"][$i]["vote"] = $vote["id"];
				$i ++;
			}
			//Zeit
			$endtime = $poll["time"] + $this->pollduration * 3600 * 24;
			if ($endtime < time())
				return trigger_error_text("Die Umfrage ist bereits beendet.", E_USER_WARNING);
			$day2end = floor(($endtime -time()) / 3600 / 24);
			$poll["votetext"] = "Poll l&auml;uft noch bis zum ".date("d.m.Y", $endtime)." ($day2end Tage).";
		} else {
			trigger_error("Poll nicht vorhanden. <a href=\"poll.php\">zur Poll-Liste</a>", E_USER_ERROR);
		}
		return $poll;
	}

	function frameadd($get) {
		global $User;
		$User->requireRight($this->adminright);
		$this->Caption = "Umfrage erstellen";

		$tpl = array ();
		if (empty ($get["id"]))
			$votes = 0;
		else
			$votes = MysqlReadField("SELECT COUNT(id) FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($get["id"])."'", "COUNT(id)");
		$plus = $votes +1;
		if ($plus > 1) {
			LogNotice("poll.php?frame=add: id=".$get["id"].", \$plus='$plus'");
			$tpl = MysqlReadRowByID(TblPrefix()."flip_poll_list", $get["id"]);
		}
		for ($i = 0; $i <= 9; $i ++) {
			$tpl["votes"][$i]["no"] = $i + $plus;
			$tpl["votes"][$i]["voteno"] = $i;
		}
		return $tpl;
	}

	/**
	 * Umfrage wird erstellt oder weitere Auswahlm&ouml;glichkeiten hinzugef&uuml;gt
	 **/
	function submitadd($post) {
		global $User;
		$User->requireRight($this->adminright);
		ArrayWithKeys($post, array("id", "10"));
		
		// Auf vorhandene Umfrage pr&uuml;fen...
		if (!empty ($post["id"]))
			$pollid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_poll_list WHERE id='".escape_sqlData_without_quotes($post["id"])."'", "id", true);
		if (!empty ($pollid)) {
			$isset = true;
		} else {
			// ...oder neue Umfrage anlegen
			$isset = false;
			ArrayWithKeys($post, array("0", "view_right", "title", "text"));
			if ($post["0"] != "") {
				$pollid = MysqlWriteByID(TblPrefix()."flip_poll_list", array ("view_right" => $post["view_right"], "time" => time(), "titel" => $post["titel"], "text" => $post["text"]));
				LogChange("Der <b>Poll</b> \"".escapeHtml($post["titel"])."\" wurde angelegt.");
			} else {
				return trigger_error_text("Es wurde keine 1. Wahlm&ouml;glichkeit angegeben.", E_USER_WARNING);
			}
		}
		// Weitere Wahlm&ouml;glichkeiten hinzuf&uuml;gen?
		if ($post["10"] != "") {
			$this->FrameVars["frame"] = "add";
			$this->NextPage = "poll.php?frame=add&id=$pollid";
		}
		// Daten in DB schreiben
		for ($i = 0; $i < 10; $i ++) {
			if (!isset($post["$i"]) || $post["$i"] == "")
				break;
			MysqlWriteByID(TblPrefix()."flip_poll_votes", array ("poll_id" => $pollid, "text" => $post["$i"]));
		}
		if ($isset)
			$this->SubmitMessage = "Es wurden $i M&ouml;glichkeiten hinzugef&uuml;gt";
		else
			$this->SubmitMessage = "Die Umfrage wurde erstellt.";
		return true;
	}

	function frameedit($get) {
		$tpl = MysqlReadRowByID(TblPrefix()."flip_poll_list", $get["id"]);
		$tpl["votes"] = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($get["id"])."'");
		$tpl["uri"] = GetRequestURI();
		return $tpl;
	}

	function submitedit($post) {
		global $User;
		$User->requireRight($this->adminright);
		MysqlWriteByID(TblPrefix()."flip_poll_list", $post, $post["id"]);
	}

	function submiteditvote($post) {
		global $User;
		$User->requireRight($this->adminright);
		if (!empty ($post["id"]))
			$id = MysqlWriteByID(TblPrefix()."flip_poll_votes", array ("text" => $post["text"]), $post["id"]);
		if (empty ($id))
			return false;
		else
			return true;
	}

	function framedel() {
		global $User;
		$User->requireRight($this->adminright);

		$this->Caption = "Umfrage w&auml;hlen";
		$list = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_list ORDER BY time");
		$i = 0;
		foreach ($list AS $poll) {
			$tpl["votes"][$i]["poll_id"] = $poll["id"];
			$tpl["votes"][$i]["title"] = $poll["titel"]." bis ".date("d.m.y", $poll["time"] + $this->pollduration * 3600 * 24);
			$i ++;
		}
		return $tpl;
	}

	function submitdel($post) {
		global $User;
		$User->requireRight($this->adminright);
		$titel = htmlentities_single(MysqlReadFieldByID(TblPrefix()."flip_poll_list", "titel", $post["poll_id"]));
		if (!MysqlDeleteByID(TblPrefix()."flip_poll_list", $post["poll_id"]))
			return trigger_error_text("Fehler beim l&ouml;schen der Umfrage.", E_USER_WARNING);
		if (!MysqlWrite("DELETE FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($post["poll_id"])."'"))
			trigger_error_text("Fehler beim l&ouml;schen der Wahlm&ouml;glichkeiten.");
		if (!MysqlWrite("DELETE FROM ".TblPrefix()."flip_poll_users WHERE poll_id='".escape_sqlData_without_quotes($post["poll_id"])."'"))
			trigger_error_text("Fehler beim l&ouml;schen der Umfrageteilnehmer.");
		LogChange("Der <b>Poll</b> \"$titel\" wurde gel&ouml;scht.");
		return true;
	}

	function framemenuvote($get) //Der f&uuml;r das Menue modifizierte Frame 'vote'
	{
		global $User;
		$votefor = empty($get["votefor"]) ? 'newest' : $get["votefor"];
		
		if ($votefor == "newest") {
			$id = MysqlReadCol("SELECT `id` FROM `".TblPrefix()."flip_poll_list` WHERE ".$User->sqlHasRight("view_right")." ORDER BY mtime DESC LIMIT 1", "id");
			if (!$id) {
				$poll["warning"] = "Keine Umfrage vorhanden";
				return $poll;
			}
			$id = $id[0];
		}
		
		elseif (is_posDigit($votefor)) //Numerischer Wert f&uuml;r votefor -> ID
		{
			$id = $get["votefor"];
		} else //votefor = Ungueltig
			{
			$poll["warning"] = "Ung&uuml;ltiger Parameter f&uuml;r votefor";
			return $poll;
		}

		//Variablen schaffen Ordnung :)
		$linkpolls = "<a style=\"text-decoration:none;\" href=\"poll.php\">Zu den Umfragen</a>";
		$linkvotes = "<a style=\"text-decoration:none;\" href=\"poll.php?frame=poll&amp;id=$id\">Ergebnis</a>";

		$poll = MysqlReadRowByID(TblPrefix()."flip_poll_list", $id, true);
		if ($poll == false) {
			$poll["warning"] = "Poll existiert nicht";
			return $poll;
		}

		if ($this->hasvoted4($id) || $User->name == "Anonymous") {
			//$poll["warning"] = "Stimme abgegeben!<hr noshade size=\"1\">".$linkvotes."<br>".$linkpolls; return $poll;
			$poll["warning"] = "results";
			$stimmen = MysqlReadField("SELECT sum(count) FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($id)."'", "sum(count)");
			$votes = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($id)."' ORDER BY id");
			foreach ($votes AS $vote) {
				$poll["results"][] = array ("txt" => $vote["text"], "percent" => ($stimmen < 1) ? 0 : round($vote["count"] / $stimmen * 100, 1));
			}
			return $poll;
		}

		//Berechtigung
		if (!$User->hasRight($poll["view_right"])) {
			$poll["warning"] = "Keine Berechtigung!";
			return $poll;
		}

		// Ergebnisse
		$votes = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_poll_votes WHERE poll_id='".escape_sqlData_without_quotes($poll["id"])."' ORDER BY id");
		$i = 0;
		foreach ($votes AS $vote) {
			$poll["results"][$i]["txt"] = $vote["text"];
			$poll["results"][$i]["vote"] = $vote["id"];
			$i ++;
		}
		//Zeit
		$endtime = $poll["time"] + $this->pollduration * 3600 * 24;
		if ($endtime < time()) {
			$poll["warning"] = "Poll beendet<hr noshade size=\"1\">".$linkvotes;
			return $poll;
		}
		//$day2end = floor(($endtime-time())/3600/24);
		//$poll["votetext"] = "Poll l&auml;uft noch bis zum ".date("d.m.Y",$endtime)." ($day2end Tage).";

		return $poll;
	}
}

RunPage("pollPage");
?>


