<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: dbxfer.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("mod/mod.dbxfer.php");
require_once ("mod/mod.rpc.php");

class DBXFerSrvPage extends Page {
	var $Right = "dbxfer_dbtransfer";

	
	//php 7 public function __construct()
	//php 5 original function DBXFerSrvPage()
	function __construct() {
		global $User;
		$User->requireRight($this->Right);
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
	}

	function frameTableList($get) {
		return new DBXFerDBInfo($get["names"]);
	}

	function frameTableStatus($get) {
		return new DBXFerTableStatus();
	}

	function frameBlockdata($get) {
		return new DBXFerBlockData($get["tablename"], $get["firstid"], $get["lastid"]);
	}
}

RPCSetErrorHandler();
RunPage("DBXFerSrvPage");
?>