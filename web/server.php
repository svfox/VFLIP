<?php
/**
 * @author Daniel Raap
 * @version $Id: server.php 1395 2007-06-15 13:59:13Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");
require_once ("mod/mod.subject.server.php");

class ServerPage extends Page {
	//Rechte
	var $adminright = "server_admin";
	var $editright = "server_edit_own";
	//Texte
	var $netzwerktext = "lanparty_netzwerk";
	var $myservertext = "server_myserver";

	function frameDefault($get, $post) {
		global $User;
		$r = array ();
		$r["text"] = LoadText($this->netzwerktext, $this->Caption);
		$servers = GetSubjects("server");

		$ids = array ();
		foreach ($servers as $k => $s) {
			if ($s["atparty"] != "Y")
				unset ($servers[$k]);
			else
				$ids[$s["owner_id"]] = 1;
		}
		$ids = implode_sqlIn(array_keys($ids));
		$users = array();
		if (!empty ($ids))
			$users = MysqlReadCol("SELECT `id`,`name` FROM ".TblPrefix()."flip_user_subject WHERE (`id` IN ($ids))", "name", "id");
		foreach ($servers as $k => $s) {
			$servers[$k]["owner"] = (isset($users[$s["owner_id"]])) ? $users[$s["owner_id"]] : null;
			$servers[$k]["canedit"] = $User->hasRightOver($this->editright, $s["id"]);
		}
		$r["serverlist"] = $servers;
		return $r;
	}

	function frameadd($get) {
		global $User;
		$User->requireRight($this->editright);
		$r = array ();
		$r["text"] = LoadText($this->myservertext, $this->Caption);
		$r["serverlist"] = GetSubjects("server");

		foreach ($r["serverlist"] as $k => $v)
			if ($v["owner_id"] != $User->id)
				unset ($r["serverlist"][$k]);

		return $r;
	}

	function actionAtParty($post, $val = "Y") {
		global $User;
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$s = CreateSubjectInstance($id);
				$User->requireRightOver($this->editright, $s);
				$s->setProperty("atparty", $val);
			}
	}

	function actionNotAtParty($post) {
		$this->actionAtParty($post, "N");
	}

	function actionDel($post) {
		global $User;
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$s = CreateSubjectInstance($id);
				$User->requireRightOver($this->editright, $s);
				$name = $s->name;
				if (DeleteSubject($s))
					LogAction("Der Server \"$name\" wurde von {$User->name} gel&ouml;scht.");
			}
	}
}

RunPage("ServerPage");
?>