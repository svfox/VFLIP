{#SUB head}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{$metarefresh}
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="alternate" type="application/rss+xml" title="RFD-Datei" href="news.php?frame=rss" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
  <link rel="shortcut icon" href="{#STATICFILE favicon.ico}" />
  <title>{%title} - {%caption}</title>
</head>
<body>
{#END}

{#SUB foot}
<br /><br />
</body>
</html>
{#END}

{#SUB page}
{#VAR sub=head}
<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td style="background-image:url({#STATICFILE images/head_r.png});" align="left">
      <div style="position:absolute; top: 20px; right: 90px;">{#VAR sub=banner file=inc.sponsor.tpl}</div>
      <img alt="FLIP Logo" src="{#STATICFILE images/flip.png}" height="45" width="66" style="position:absolute; top:80px; right:8px;" />
      <div style="height:133px; width:850px; background-image:url({#STATICFILE images/head.png});">
        <div style="position:absolute; top: 108px; right: 90px;">
        {#LOAD MenuLoad() $level0 inc/inc.menu.php}
        {#FOREACH $level0.menu}{#FOREACH $items}
          <a class="{#WHEN $active menulinksel0 menulink0}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{§ %caption}</a>
        {#MID} | {#END}
        {#END}
        </div>
      </div>
    </td>
  </tr>
  <tr>
    <td>
    <table cellpadding="0" cellspacing="0" width="100%">
      <tr>
        {#LOAD MenuLoad($level0.activedir,1) $level1 inc/inc.menu.php}
          {#IF !empty($level1.menu)}
          <td  width="1%" valign="top">
            {#FOREACH $level1.menu}{#VAR sub=menubox header=1}{#MID}<br />{#END}
          </td>
        	{#END}
        
        <td width="98%" valign="top" style="padding:4px 0px 0px 0px;">
          <!-- ERRORCELL -->
          {#VAR sub=content_header style="height:400px"}
          {#TPL $page}
          {#VAR sub=content_footer}
        </td>
        {#LOAD '' $caption}
        {#LOAD MenuLoad($level1.activedir,2) $level2 inc/inc.menu.php}
          {#IF !empty($level2.menu)}
          <td width="1%" valign="top">
            {#FOREACH $level2.menu}{#VAR sub=menubox header=1}{#MID}<br />{#END}
          </td>
        {#END}
      </tr>
    </table>
    </td>
  </tr>
</table>
<div style="padding-left: 160px; color: #BBBBBB">
	<a href="http://www.flipdev.de" target="_blank" style="color: #BBBBBB">Powered by FLIP {$flipversion}</a>
	&nbsp;-&nbsp;
	Page created in <!-- {creation_time} --> ms using <!-- {query_count} --> queries
</div>
{#VAR sub=foot}
{#END}

{#SUB menubox}
<table width="160" cellpadding="0" cellspacing="0">
  <tr>
    {#IF !empty($caption)}
    <td colspan="3">
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="background-image:url({#STATICFILE images/nav_t.png}); height:32px;">
            <div style="background-image:url({#STATICFILE images/nav_tl.png}); height:26px; background-repeat:no-repeat; padding: 6px 0px 0px 12px; font-size:12px; color:#666666; font-weight:bold;">
            {§ %caption}
            </div>
          </td>
          <td style="background-image:url({#STATICFILE images/nav_tr.png}); height:32px; width:20px;"></td>
        </tr>
      </table>
    </td>
    {#ELSE}
    <td style="background-image:url({#STATICFILE images/nav2_tl.png}); width:9px; height:8px;"></td>
    <td style="background-image:url({#STATICFILE images/nav2_t.png}); width:7px; height:8px;"></td>
    <td style="background-image:url({#STATICFILE images/nav2_tr.png}); width:7px; height:8px;"></td>
    {#END}
  </tr>
  <tr>
    <td style="background-image:url({#STATICFILE images/nav_l.png}); width:9px;">&nbsp;</td>
    <td style="background-color:#F1F1F1; padding:0px 0px 8px 8px; width: 144px">
    {#FOREACH $items}
      {#IF is_object($frame)}{#TPL $frame}
      {#ELSEIF !empty($text)}{$text}
      {#ELSE}{#DBIMAGE $image}<a class="{#WHEN $active menulinksel1 menulink1}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{§ %caption}</a><br />
      {#END}
    {#END}
    </td>
    <td style="background-image:url({#STATICFILE images/nav_r.png}); width:7px;">&nbsp;</td>
  </tr>
  <tr>
    <td style="background-image:url({#STATICFILE images/nav_bl.png}); width:9px; height:7px;"></td>
    <td style="background-image:url({#STATICFILE images/nav_b.png}); width:7px; height:7px;"></td>
    <td style="background-image:url({#STATICFILE images/nav_br.png}); width:7px; height:7px;"></td>
  </tr>
</table>
{#END}

{#SUB content_header}
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="background-image:url({#STATICFILE images/cnt_tl.png}); width:21px; height:28px;">&nbsp;</td>
          <td style="background-image:url({#STATICFILE images/cnt_t.png}); height:28px; font-size:14px; color:#666666; font-weight:bold;">{%caption}</td>
          <td style="background-image:url({#STATICFILE images/cnt_tr.png}); width:134px; height:28px;">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="background-image:url({#STATICFILE images/cnt_l.png}); width:8px;">&nbsp;</td>
          <td style="background-color:#F1F1F1; padding:8px; {$style}" align="center" valign="top">
{#END}
{#SUB content_footer}
          </td>
          <td style="background-image:url({#STATICFILE images/cnt_r.png}); width:13px;" valign="top">
            <img src="{#STATICFILE images/cnt_rt.png}" width="13" height="26" alt="Border" border="0" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="background-image:url({#STATICFILE images/cnt_bl.png}); width:21px; height:20px;">&nbsp;</td>
          <td style="background-image:url({#STATICFILE images/cnt_b.png}); height:20px;">&nbsp;</td>
          <td style="background-image:url({#STATICFILE images/cnt_br.png}); width:27px; height:20px;">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
{#END}

{#SUB errorcell}
{#IF count($errors) > 0}
  {#VAR sub=content_header caption=Fehler style="color:#990000;"}
  <ul>
  {#FOREACH $errors $err}
    <li>{$err}</li>
  {#END}
  </ul>              
  {#VAR sub=content_footer}
{#END}
{#IF count($notices) > 0}
  {#VAR sub=content_header caption=Nachricht style="color:#BBBB00;"}
  <ul>
  {#FOREACH $notices $msg}
    <li>{$msg}</li>
  {#END}
  </ul>              
{#VAR sub=content_footer}
{#END}
{#END}