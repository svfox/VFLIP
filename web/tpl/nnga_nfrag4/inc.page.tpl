{#SUB page}<html>
<head>
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
  <link rel="shortcut icon" href="{#STATICFILE favicon.ico}" />
  <title>{%title} - {%caption}</title>
  <style>
  <!--
  ul { list-style-image: url({#STATICFILE images/dot2.png}); }
  -->
  </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td style="height:10px; background-color:#00000B;"></td></tr>
  <tr> 
    <td background="{#STATICFILE images/headhg.gif}">
      {!----------- head (logo/login/logount) -----------}
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="10%"><img src="{#STATICFILE images/head.gif}" width="600" height="120"></td>
          <td width="90%" valign="top" align="right" style="padding-top:12px;">
{#VAR sub=login file=inc.page.login.tpl}
          </td>
        </tr>
      </table>
      {! ----------- /head ----------------------------}
    </td>
  </tr>
  <tr> 
    <td>
      {!------------ top-menu -------------------------}
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="10%" height="13"><img src="{#STATICFILE images/headsub.gif}" width="417" height="13"></td>
          <td width="90%" height="13" style="background-color:#B7B7BA;" class="menulink">
            {#LOAD MenuLoad() $level0 inc/inc.menu.php}
            {#FOREACH $level0.menu}{#FOREACH $items}
              <a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{%caption}</a>
            {#MID} | {#END}
            {#END}
          </td>
        </tr>
      </table>
      {!------------ /top-menu -------------------------}
    </td>
  </tr>
  <tr> 
    <td height="18" background="{#STATICFILE images/shadow.gif}">&nbsp;</td>
  </tr>
  <tr>
    <td> 
      <table width="100%" border="0" cellpadding="3" cellspacing="3">
        <tr>
        {!---------- left-menu (wird ausgeblendet, wenn leer) ---}
        {#LOAD MenuLoad($level0.activedir) $level1 inc/inc.menu.php}
        {#IF !empty($level1.menu)}
          <td width="2%" valign="top"> 
            {#FOREACH $level1.menu}
            <table width="121" border="0" cellspacing="0" cellpadding="0">
              <tr><td class="cellcaptionl" background="{#STATICFILE images/menutop.gif}">{%caption}</td></tr>
              {#FOREACH $items}
                {#IF is_object($frame)}<tr><td>{#TPL $frame}</td></tr>
                {#ELSEIF !empty($text)}<tr><td>{$text}</td></tr>{#ELSE}
                <tr> 
                  <td>
                    <table width="121" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="11"><img src="{#IF $active}{#STATICFILE images/pfeilov.gif}{#ELSE}{#STATICFILE images/pfeil.gif}{#END}" width="11" height="20"></td>
                        <td width="110" background="{#STATICFILE images/menuhg.gif}" style="padding:3px;"><a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{%caption}</a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                {#END}
              {#MID} 
              <tr><td style="height:1px; background-color:E7E7E7;"></td></tr>
              {#END}
              <tr> 
                <td><img src="{#STATICFILE images/menubottom.gif}" width="121" height="12"></td>
              </tr>
            </table>
            {#MID}
            <br>
            {#END}
          </td>
          {#END}
          {!--------- /left-menu --------------------------------}
          <td width="85%" valign=top> 
            {!--------- error & notice-boxes ----------------------}
            <!-- ERRORCELL -->
            {!--------- /error & notice-boxes ---------------------}
            {!--------- content-box -------------------------------}
{#VAR sub=cellheader}
{#TPL $page}
{#VAR sub=cellfooter}
            {!-------------- /content-box -----------------------}
          </td>
          <td width="9%" valign=top> 
            {!-------------- right-cells ------------------------}
            <table width="100" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td>
                  <table width="100" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5%"><img src="{#STATICFILE images/menukantel.gif}" width="8" height="20"></td>
                      <td width="90%" background="{#STATICFILE images/menutophg.gif}" class="cellcaptionr">partner</td>
                      <td width="5%"><img src="{#STATICFILE images/menukanter.gif}" width="39" height="20"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td bgcolor="#E7E7E7" style="padding:4px;" align="center"><img src="{#STATICFILE images/pixelcorebutton.gif}" width="88" height="31"></td>
              </tr>
              <tr>
                <td><img src="{#STATICFILE images/smenbottom.gif}" width="100" height="12"></td>
              </tr>
            </table>
            {!-------------- /right-cells -----------------------}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p align="center" style="font-size:10px; color: #AAAAAA;">
  Copyright &copy; 2001-2004:&nbsp;&nbsp;&nbsp;
  Design & Content: <a href="http://nnga.info" target="_blank" style="color: #6666BB;">NNGA e.V.</a>&nbsp;&nbsp;&nbsp;
  Code: The <a href="http://flip.sourceforge.net" target="_blank" style="color: #6666BB;">FLIP</a> Project Team.
</p>
</body>          
</html>
{#END}

{#SUB errorcell}
{#IF count($errors) > 0}
  {#VAR sub=cellheader contenttype=error caption=Fehler}
  <ul>
  {#FOREACH $errors $err}
    <li>{$err}</li>
  {#END}
  </ul>
  {#VAR sub=cellfooter}
  <br>
{#END}
{#IF count($notices) > 0}
  {#VAR sub=cellheader contenttype=notice caption=Nachricht}
      <ul>
      {#FOREACH $notices $msg}
        <li>{$msg}</li>
      {#END}
      </ul>              
  {#VAR sub=cellfooter}
  <br>
{#END}
{#END}

{#SUB cellheader}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>              
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="1%" bgcolor="#EBEBEB"><img src="{#STATICFILE images/ckl.gif}" width="9" height="24"></td>
          <td width="98%" bgcolor="#EBEBEB" class="{$contenttype}caption">{%caption}</td>
          <td width="1%"><img src="{#STATICFILE images/ckr.gif}" width="40" height="24"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="{$contenttype}cell" align="center" valign="top">
{#END}
{#SUB cellfooter}
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="1%"><img src="{#STATICFILE images/cskl.gif}" width="90" height="12"></td>
          <td width="98%" background="{#STATICFILE images/cshg.gif}"></td>
          <td width="1%"><img src="{#STATICFILE images/cskr.gif}" width="100" height="12"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
{#END}