{#SUB page}{$xmltag}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
    <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
    <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
    <link rel="shortcut icon" href="{#STATICFILE favicon.ico}" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>{%title} - {%caption}</title>
    <style>
    <!--
    ul { list-style-image: url({#STATICFILE images/dot2.png}); }
    -->
    </style>
  </head>
  <body>
  <table cellpadding="0" cellspacing="0" width="100%" border="0">
  <tr>
    <td>
      <br />
      <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
          <td><img src="{#STATICFILE images/banner.png}" alt="" width="468" height="60" /></td>
          <td align="right">
            <table cellspacing="0" cellpadding="0" border="0" class="menu">
              <tr>
                <td style="background-image:url({#STATICFILE images/border-nw.png}); width:8px; height:8px;"></td>
                <td style="background-image:url({#STATICFILE images/border-n.png}); height:8px;" colspan="3"></td>
              </tr>
              <tr>
                <td style="background-image:url({#STATICFILE images/border-w.png}); width:8px;"></td>
                {#IF $isloggedin}
                <td class="menu" valign="middle" style="padding-right:4px;padding-top:4px;">
				  {#IF $unread_messages}
                    <a href="webmessage.php?frame=owner"><img src="{#STATICFILE images/mail_icon.png}" width="32" height="16" alt="" border="0"></a>
				  {#END}
                </td>
                <td class="menu">
                  <b>{%user}</b>&nbsp;&middot;&nbsp;
                </td>
                <form method="post" name="frm">
                <td class="menu" style="padding-right: 3px;">
                  <input type="hidden" name="ident" value="Anonymous" />
                  <input type="hidden" name="password" value="x" />
                  <input type="submit" class="loginbtn" value="logout" />
                </td>
                </form>
                {#ELSE}
                <form method="post" name="frm">
                <td class="menu">
                  <input type="text" class="logined" name="ident" title="Deine UserID, dein Nickname oder deine Email-Adresse" onFocus="if(this.value=='Nickname') this.value='';" />
                  <input type="password" class="logined" name="password" title="Dein Password" onFocus="this.value='';" />
                </td>
                <td class="menu">
                  &nbsp;<input type="submit" class="loginbtn" value="login" />
                </td>
                <td style="padding-right: 8px;" class="menu">
                  &nbsp;&nbsp;<a href="text.php?name=user_account_trouble"><b>?</b></a>
                </td>
                </form>
                <script> document.frm.ident.value='Nickname'; document.frm.password.value='Password';</script>
                {#END}
              </tr>
              <tr>
                <td style="background-image:url({#STATICFILE images/border-sw.png}); width:8px; height:8px;"></td>
                <td style="background-image:url({#STATICFILE images/border-s.png}); height:8px;" colspan="3"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="right">      
      <br />
      <table cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td style="background-image:url({#STATICFILE images/bar_fader_left.png}); width:260px;"></td>
          <td style="background-image:url({#STATICFILE images/bar_spacer.png});">
            <table cellpadding="2" cellspacing="0" border="0" style="height:18px;">
              <tr class="menu">
                {#LOAD MenuLoad() $level0 inc/inc.menu.php}
                {#FOREACH $level0.menu}{#FOREACH $items}
                  <td nowrap="1"><a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN $use_new_window " target=\"_blank\""}>{%caption}</a></td>
                {#MID}
                <td>&middot;</td>
                {#END}
                {#END}
              </tr>
            </table>
          </td>
          <td style="background-image:url({#STATICFILE images/bar_fader_right.png}); width:260px;"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr valign="top">
          {!------------------------- left menu ---------------------------}
          {#LOAD MenuLoad($level0.activedir) $level1 inc/inc.menu.php}
          {#IF !empty($level1.menu)}
          <td style="width:118px;">
            <br />
            <br />
            <br />
            {#FOREACH $level1.menu}
            <table cellspacing="0" cellpadding="0" border="0" class="menu">
              <tr>
                <td style="background-image:url({#STATICFILE images/border-n.png}); width:110px; height:8px;"></td>
                <td style="background-image:url({#STATICFILE images/border-ne.png}); width:8px; height:8px;"></td>
              </tr>
              <tr>
                <td style="padding-right: 8px; text-align: right;" class="menu">
                  {#FOREACH $items}
                    {#IF is_object($frame)}{#TPL $frame}
                    {#ELSEIF !empty($text)}{$text}
                    {#ELSE}{#DBIMAGE $image}<a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{%caption}</a><br />
                    {#END}
                  {#END}
                </td>
                <td style="background-image:url({#STATICFILE images/border-e.png});"></td>
              </tr>
              <tr>
                <td style="background-image:url({#STATICFILE images/border-s.png}); width:110px; height:8px;"></td>
                <td style="background-image:url({#STATICFILE images/border-se.png}); width:8px; height:8px;"></td>
              </tr>
            </table>
            <br />
            {#END}
            <br />
            <br />
          </td>
          {#END} 
          {!---------------------- end left menu --------------------------}
          <td style="padding-top:10px;padding-bottom:10px;" align="center">
            <br />
<!-- ERRORCELL -->
            <div class="{$contenttype}cell">
              {#IF !empty($caption)}<h1 align="center">{%caption}</h1>{#END}
{#TPL $page}
              <br />&nbsp;
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <br />
      <table cellspacing="0" cellpadding="0" border="0" align="right">
        <tr>
          <td style="height:18px; background-image:url({#STATICFILE images/bar_border_left.png}); width:12px;"></td>
          <td style="height:18px; background-image:url({#STATICFILE images/bar_spacer.png});" class="footer">&nbsp;{$copyright}&nbsp;</td>
          <td style="height:18px; background-image:url({#STATICFILE images/bar_fader_right.png}); width:260px;"></td>
        </tr>
      </table>
    </td>
  </tr>
  </table>
  <p>
    <br />
    <br />
    <br />
  </p>
  </body>
</html>
{#END}

{#SUB errorcell}
{#IF count($errors) > 0}
<div class="errorcell">
  <h1 align="center">Fehler</h1>
  <ul>
  {#FOREACH $errors $err}
    <li>{$err}</li>
  {#END}
  </ul>              
</div>
<br />
{#END}
{#IF count($notices) > 0}
<div class="noticecell">
  <h1 align="center">Nachricht</h1>
  <ul>
  {#FOREACH $notices $msg}
    <li>{$msg}</li>
  {#END}
  </ul>              
</div>
<br />
{#END}
{#END}
