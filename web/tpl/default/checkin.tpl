{! $Id$ }

{#SUB default}
{#IF !empty($current_step)}
<h3 style="border: 1px solid black; width: 20%; padding: 5px;">Schritt {$current_step} von {$final_step}</h3>
<br />
{#END}
<table>
	<tr>
		<td>
			<form name="f" action="checkin.php" method="get">
				<input name="search" type="text" value="{%search}" class="edit" title="Irgend ein im Profil enthaltener Wert"/>
				<input value="User suchen" type="submit" class="button"/>
			</form>
		</td>
		<td style="width: 100px; text-align: center;"><span style="font-size: 18px;">- oder -</span></td>
		<td>
			
			<a style="font-size:18px;" href="user.php?frame=editproperties&amp;id=create_new_subject&amp;type=user&amp;enabled=Y">
				Neuen User erstellen
			</a>
		</td>
	</tr>
</table>
<br />
<hr />
<h3><u>Suchergebnisse:</u></h3>
{#IF count($users) > 0}
  {#TABLE $users}
    {#COL En $enabled}
    {#COL Nick $name}<b><a href="checkin.php?search={$id}">{%name}</a></b>
    {#COL Name $givenname}{%givenname} {%familyname}
    {#COL Email %email}
  {#END}
{#END}
{#IF count($user) > 0}{#WITH $user}
  <style type="text/css">
	.b{font-size:18px;}
    .button{font-size:16px; font-weight: bold;}
  </style>
  <span class="b" style="color:#008800"><b class="b">{%nick}</b> ({%name})</span><br /><br />
  <p class="b">
    <a class="b" href="user.php?frame=viewsubject&amp;id={$id}">&Uuml;bersicht</a>&nbsp;&middot;&nbsp;
    <a class="b" href="user.php?frame=editproperties&amp;id={$id}">Profil</a>&nbsp;&middot;&nbsp;
    <a class="b" href="seats.php?userid={$id}">Sitzplatz</a>
  </p>
  <p class="b">
    {#FOREACH $warnings $w}<span style="color:#E60000">{$w}</span><br />{#END}
    <br />
    {#FOREACH $buttons}
      {#FORM item action="checkin.php?search=$id" keepvals=0}
        {#INPUT action_type hidden $type}
        {#INPUT action_name hidden $name}
        {#IF $input_type == "edit"}
          {#INPUT action_value $action_val_type $value}
        {#ELSE}
          {#INPUT action_value hidden $value}
        {#END}
        {#INPUT id hidden $id}
        <input type="submit" class="button" style="background-color:{$button_color}" value="{$text}" />
      {#END}<br />
    {#END}
    <table>
      {#FOREACH $props}<tr><td align="right" class="b">{%name}:</td><td class="b" style="font-weight: bold;">{#IF $type == "date"}{#DATE $value}{#ELSE}{#IF $type=="image"}{#DBIMAGE $value process=createthumbnail(100,100)}{#ELSE}{%value}{#END}{#END}</td></tr>{#END}
    </table>
  </p>    
{#END}{#END}
{#IF (count($users) == 0) and (count($user) == 0)}
  <p>
    Keine User gefunden :-(
  </p>
{#END}
<script>
  {#IF $user.barcode}
    document.b.hw_barcode.focus();
  {#ELSE}
    document.f.search.focus();
    document.f.search.select();
  {#END}
</script>
{#IF $editright}
<div align="right" class="textfooter">
<a href="checkin.php?frame=editlist">Pr&uuml;fungen bearbeiten</a><br />
<a href="config.php?category=checkininfo">Systemeinstellungen bearbeiten</a><br />
<a href="checkin.php?frame=doku">Dokumentation</a>
</div>
{#END}
{#END}

{#SUB editlist}
<a href="checkin.php">{§zum Checkin}</a><br /><br />
{#TABLE $list}
<a href="checkin.php?frame=edititem">{§hinzuf&uuml;gen}</a>
  {#COL "Schritt #" $order align="center"}{#WHEN "$input_type == \"text\"" ">="}{$order}
  {#COL Status $status}
  {#COL Pr&uuml;fung $check_name}{$check_text}
  {#COL Anzeige $input_type}
  {#COL Aktion $action_name}{$action_text}
  {#OPTION entfernen delitems}
  {#FRAME edit edititem}
{#END}
<div align="right" class="textfooter">
<a href="checkin.php?frame=doku">Dokumentation</a>
</div>
{#END}

{#SUB edititem}
<a href="checkin.php?frame=editlist">{§zur&uuml;ck zur Liste}</a><br />
<br />
{#FORM customize}
{#INPUT id hidden $id}
Status: {#INPUT status dropdown $status param=$statuses}<br />
<h3>{§Pr&uuml;fung}</h3>
<table>
<tr><td>{§Typ}:</td><td>{#INPUT check_type dropdown $check_type $checktypes}</td></tr>
<tr><td>{§Name}:</td><td>{#INPUT check_name string $check_name}</td></tr>
<tr><td>{§Wert}:</td><td>{#INPUT check_not checkbox $check_not} {§nicht}{#INPUT check_value string $check_value}</td></tr>
</table>
<h3>{§Eingabe/Anzeige}</h3>
<table>
<tr><td>{§Typ}:</td><td>{#INPUT input_type dropdown $input_type $inputtypes}</td></tr>
<tr><td>{§Buttonfarbe}:</td><td>{#INPUT button_color string $button_color}</td></tr>
</table>
<h3>{§Aktion}</h3>
<table>
<tr><td>{§Typ}:</td><td>{#INPUT action_type dropdown $action_type $actiontypes}</td></tr>
<tr><td>{§Name}:</td><td>{#INPUT action_name string $action_name}</td></tr>
<tr><td>{§Wert}:</td><td>{#INPUT action_value string $action_value}</td></tr>
</table>
<br />
{§Reihenfolge}: {#INPUT order integer $order}<br />
{#SUBMIT speichern}
{#END}
<div align="right" class="textfooter">
<a href="checkin.php?frame=doku">Dokumentation</a>
</div>
{#END}

{#SUB doku}
<h1>Checkindokumentation</h1>
Stand: 2005-09-17<br />
<a href="checkin.php">zum Checkin</a><br />
<div align="left">
<h2>Aufbau</h2>
<p>Der Checkin besteht aus einer Pr&uuml;fung, Darstellung und Aktion. Wenn die Pr&uuml;fung fehlschl&auml;gt, wird dies
dargestellt. Die Pr&uuml;fung gibt an, wie der Zustand des Users sein soll, z.B. das er ein bestimmtes Recht hat.
Ist dies nicht der Fall, wird eine Aktion dargestellt. Die Darstellung kann ein Hinweis sein oder ein Button der direkt eine Aktion ausf&uuml;hrt. Ausserdem
k&ouml;nnen Informationen zum Benutzer angezeigt werden.<br />
Es wird eine Liste mit allen Pr&uuml;fungen und Aktionen angelegt. Diese wird nacheinander abgearbeitet. Schl&auml;gt eine
Pr&uuml;fung fehl, wird die Abarbeitung abgebrochen. Die Darstellung wird angezeigt, damit das "Problem" behoben werden kann.
Ist dies ein Button kann direkt die Aktion durchgef&uuml;hrt werden. Z.B. bei der Auswahl eines Sitzplatzes ist dies nicht so
einfach m&ouml;glich und es wird ein Hinweis angezeigt, dass ein Sitzplatz ausgew&auml;hlt werden muss.<br />
Die Aktionen k&ouml;nnen das setzen einer Eigenschaft, das Zuweisen eines Rechtes oder das Verschieben in eine Gruppe sein.
</p>
<h2>Konfiguration</h2>
<p>F&uuml;r jeden Eintrag der abgearbeitet wird, werden folgende Daten festgelegt:<br />
<dt>Status</dt>
<dd>Gibt an ob der Eintrag <em>aktiv</em>, <em>optional</em> oder <em>inaktiv</em> ist.</dd>
<dt>Pr&uuml;fung</dt>
<dd>Die Pr&uuml;fung die durchgef&uuml;hrt wird. Der <em>Typ</em> bestimmt wie <em>Name</em> und <em>Wert</em> genutzt werden.
Oft ist Wert nicht notwendig, z.B. wenn eine Gruppenzugeh&ouml;rigkeit gepr&uuml;ft wird, gibt <em>Name</em> die Gruppe an.</dd>
<dt>Eingabe/Anzeige</dt>
<dd>Gibt an was angezeigt wird, wenn die Pr&uuml;fung fehl schl&auml;gt.Eine Ausnahme ist <em>Infotext</em>: Er wird immer angezeigt (unabh&auml;ngig von der Pr&uuml;fung).<br />
Die Anzeige bezieht sich auf die durchzuf&uuml;hrende Aktion:
Soll ein Button etwas setzen, ein Wert eingegeben werden oder ein Hinweis angezeigt werden?</dd>
<dt>Aktion</dt>
<dd>Die Aktion die durchgef&uuml;hrt wird. Der <em>Typ</em> bestimmt wie <em>Name</em> und <em>Wert</em> genutzt werden.
Oft ist Wert nicht notwendig, z.B. wenn der Status gesetzt wird, gibt <em>Name</em> den Status an.</dd>
<dl><dt>Reihenfolge</dt>
<dd>Die Reihenfolge in der die Eintr&auml;ge abgearbeitet werden.</dd>
</dl>
</p>
</div>
{#END}