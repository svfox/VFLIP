{! benenne diesen sub in "menu" um, damit der Turnierbaum in einem neuen Fenster angezeigt wird.}
{#SUB menu_mit_baum_und_neuen_fenster}
<h3>
  <a href="{$file}?frame=overview&amp;id={$tournamentid}"{#WHEN "$curframe == 'overview'" " class='important'"}>&Uuml;bersicht</a> &middot; 
  <a target="_blank" href="{$file}?frame=tree&amp;id={$tournamentid}&amp;nomenu=1">Turnierbaum</a> &middot;
  <a href="{$file}?frame=results&amp;id={$tournamentid}"{#WHEN "$curframe == 'results'" " class='important'"}>Platzierung</a>
</h3>
{#END}

{! Dies ist das default-Men&uuml;, in ihm wird der Turnierbaum nicht in einem neuen Fenster ge&ouml;ffnet.}
{#SUB menu}
<h3>
  <a href="{$file}?frame=overview&amp;id={$tournamentid}"{#WHEN "$curframe == 'overview'" " class='important'"}>&Uuml;bersicht</a> &middot; 
  <a href="{$file}?frame=tree&amp;id={$tournamentid}"{#WHEN "$curframe == 'tree'" " class='important'"}>Turnierbaum</a> &middot;
  <a href="{$file}?frame=results&amp;id={$tournamentid}"{#WHEN "$curframe == 'results'" " class='important'"}>Platzierung</a>
</h3>
{#END}

{#SUB editteamstyles}
<style type="text/css">
.edittable
{
  margin:10px;
  border-spacing:0px;
  border:1px solid black;
  padding:10px;
}
.thcell
{
  border-bottom:1px solid gray;
  padding-top:12px;
  padding-right:20px;
  padding-bottom:12px;
  margin:0px;
}
.tdcell
{
  border-bottom:1px solid gray;
  padding-bottom:12px;
  padding-top:12px;
  margin:0px;
}
</style>
{#END}

{#SUB rankingstyles}
<style type="text/css">
 .podest{background-color:#CCCCCC;}
</style>
{#END}

{! Eine Liste mit den Platzierungen aller Turniere die unter 'Export' ertellt wird }
{#SUB resultexport}
<style type="text/css">
body
{
  font-family:arial,sans-serif;
}
.tournamenttitle
{
  text-align:center;padding-top:20px;
}
.rankbox
{
  border:1px solid black;padding:6px;
}
</style>
<div align="center">
<h1>{$caption}</h1>
<h2>am {$partydate}</h2>
<table>
{#FOREACH $tournaments}
<tr><td class="tournamenttitle" colspan="3">
<h2><a name="{$title}"></a>{$title}</h2>
</td></tr>
<tr><td width="20%">&nbsp;</td><td class="rankbox">
<table>
{#FOREACH $ranking}
<tr>
  <td align="right">{#IF $rank==="1"}<strong>{$rank}.</strong>{#ELSE}{$rank}.{#END}</td><td>{#IF $rank=="1"}<strong>{$teamname}</strong>{#ELSE}{$teamname}{#END}</td>
</tr>
{#END}
</table>
</td><td width="20%">&nbsp;</td></tr>
{#END}
</table>
<br />
<small>generiert am {#DATE} von FLIP {$flipversion}</small>
</div>
{#END}
