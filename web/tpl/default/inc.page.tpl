{#SUB head}<<?php echo"?"?>xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE
 html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
 "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
  <title>{%title} - {%caption}</title>
	{$metarefresh}
  <link rel="alternate" type="application/rss+xml" title="RFD-Datei" href="news.php?frame=rss"/>
  <link rel="alternate" type="application/atom+xml" title="Atom-Feed" href="news.php?frame=atom"/>
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
  <link rel="shortcut icon" href="{#STATICFILE favicon.ico}" />
  <style type="text/css">
    ul { list-style-image: url({#STATICFILE images/dot2.png}); }
  </style>
</head>
<body>
{#END}

{#SUB foot}
</body>
</html>
{#END}

{#SUB page}{#VAR sub=head}
<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td style="width:32px; height:84px;"></td>
    <td class="toplogo" rowspan="2" style="background-image:url({#STATICFILE images/logo_flip.png});"></td>
    <td style="height:84px;" align="center" valign="middle">
      {#VAR sub=banner file=inc.sponsor.tpl}
    </td>
    <td style="height:84px;" align="right" valign="middle">
      {#VAR sub=login file=inc.page.login.tpl}
    </td>
    <td style="width:32px; height:84px;"></td>
  </tr>
  <tr>
    <td class="bartl" style="background-image:url({#STATICFILE images/diag_topleft.png});"></td>
    <td class="bartm" style="white-space:nowrap;" colspan="2">
      {#LOAD MenuLoad() $level0 inc/inc.menu.php}
      {#FOREACH $level0.menu}{#FOREACH $items}
        <a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN "isset($use_new_wnd) && $use_new_wnd" " target=\"_blank\""}>{%caption}</a>
      {#MID} | {#END}
      {#END}
    </td>
    <td class="bartr" style="background-image:url({#STATICFILE images/diag_topright.png});"></td>
  </tr>
</table>
  
  <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
      {!------------------------- left menu ---------------------------}
      {#LOAD MenuLoad($level0.activedir,1) $level1 inc/inc.menu.php}
      {#IF !empty($level1.menu)}
      <td valign="top" style="padding-right:8px; padding-top:8px; padding-bottom:8px;width:100px">
        <table cellpadding="0" cellspacing="0" width="100%">
          {#FOREACH $level1.menu}
          <tr>
            <td class="menuleft">
              <div class="menutitle">{#DBIMAGE $image_title}{%caption}</div>
              <div style="padding: 4px 0px 4px 2px;{#IF $image_bg}background-image:url({#DBIMAGE $image_bg fileonly=1});{#END}">
                {#FOREACH $items}
                  {#IF is_object($frame)}{#TPL $frame}
                  {#ELSEIF !empty($text)}{$text}
                  {#ELSE}{#DBIMAGE $image}<a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN "isset($use_new_wnd) && $use_new_wnd" " target=\"_blank\""}>{%caption}</a><br />
                  {#END}
                {#END}
              </div>
            </td>
          </tr>
          {#MID}
          <tr><td style="height:8px;"></td></tr>
          {#END}
        </table>
      </td>
      {#END} 
      {!---------------------- end left menu --------------------------}
      <td align="center" valign="top">
<!-- ERRORCELL -->
        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 8px;">
          <tr>
            <td class="{$contenttype}cell" align="center" valign="top">
              {#WHEN !empty($caption) <h1>%caption</h1> }
{#TPL $page}
            </td>
          </tr>
        </table>
      </td>
      <td style="width:32px;"></td>
    </tr>
  </table>
<table cellpadding="0" cellspacing="0" width="100%" style="margin-top:8px;">
  <tr>
    <td class="barbl" style="background-image:url({#STATICFILE images/diag_bottomleft.png});"></td>
    <td class="barbm" colspan="2">{$copyright}</td>
    <td class="barbr" style="background-image:url({#STATICFILE images/diag_bottomright.png});"></td>
  </tr>
</table>
{#VAR sub=foot}
{#END}

{#SUB errorcell}
<table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 8px;">
{#IF count($errors) > 0}
<tr><td class="errorcell">
  <h1 align="center">{§Fehler}</h1>
  <ul>
  {#FOREACH $errors $err}
    <li>{$err}</li>
  {#END}
  </ul>              
</td></tr>
<tr><td style="height:8px;"></td></tr>
{#END}
{#IF count($notices) > 0}
<tr><td class="noticecell">
  <h1 align="center">{§Nachricht}</h1>
  <ul>
  {#FOREACH $notices $msg}
    <li>{$msg}</li>
  {#END}
  </ul>              
</td></tr>
<tr><td style="height:8px;"></td></tr>
{#END}
</table>
{#END}
