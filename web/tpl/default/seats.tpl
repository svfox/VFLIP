{! $Id: seats.tpl 1441 2007-07-30 10:19:14Z loom $ }

{#SUB blockgraph}
  {#VAR sub=header file=inc.seats.tpl}
  {#TPL $text}  
  {#VAR sub=searchform}
  <map name="seats" id="seats">
  {#FOREACH $blocks}
    <area {$cords} href="{%href}{#WHEN "!empty($userid)" "&amp;userid=%userid"}" title="{$caption}" alt="{$caption}" />
  {#END}
  </map>
  <p>
    <img src="seats.php?frame=overviewimage" width="{$img.width}" height="{$img.height}" border="0" alt="" usemap="#seats" />
  </p>  
{#END}

{#SUB blocklist}
  {#VAR sub=header file=inc.seats.tpl}
  {#TPL $text}  
  {#TABLE $blocks}
    {#COL style="padding:0px"}<a href="{%href}">{#DBIMAGE $background_tmp process="createThumbnail(200,128)" border=0}</a>
    {#COL Sitzplan}<b><a href="{%href}">{%caption}</a></b><br />{%description}
  {#END}
{#END}

{#SUB block}
  {#TPL $text}
  <p align="center"><b>{$stat}</b>
  <table cellpadding="16"><tr>
    <td><!--
      <form>
        Sitzplatzsuche:&nbsp;&nbsp;
        <input class="edit" type="text" name="search" value="{$search}">&nbsp;&nbsp;
        <input class="button" type="submit" value="suchen">
  	    {#IF !empty($found)}&nbsp;&nbsp;&nbsp;{$found.count} Sitzpl&auml;tze gefunden.{#END}
      </form>-->
    </td>
    <td>
      {#FORM action="seats.php" method="get"}
        {#INPUT frame hidden block}
	<select name="blockid" onchange="submit()">
		{#FOREACH $blocks $blockname $blockid}
		<option value="{$blockid}"{#WHEN "$blockid==$block_id" " selected=\"1\""}>{$blockname}</option>
		{#END}
	</select>
        <noscript>{#SUBMIT anzeigen shortcut=0}</noscript>
      {#END}
    </td>
    <td><b><a href="seats.php?frame=blockgraph">&Uuml;bersicht</a></b>{#IF $admin} - <a href="seatsedit.php?frame=editblock&amp;id={$id}">{§Block bearbeiten}</a>{#END}</td>
  </td></table>
  {#IF !empty($found)}
  <table cellspacing="0" cellpadding="1">
	{#FOREACH $found.result}
	<tr>
	  <td style="padding-right:8px;"><b>{$name}</b></td><td>{$places}</td>
	</tr>
	{#END}  
  </table>
  {#END}
  <map name="seats" id="seats">
  {#FOREACH $seats}
{#IF $shape == "rectangle"}
<area shape="rect" coords="{$left},{$top},{$right},{$bottom}" href="{$url}" title="{$name} - {$title}" alt="{$name} - {$title}" />
{#ELSEIF $shape == "polygon"}
<area shape="poly" coords="{$x1.x},{$x1.y},{$x2.x},{$x2.y},{$x3.x},{$x3.y},{$x4.x},{$x4.y}" href="{$url}" title="{$name} - {$title}" alt="{$name} - {$title}" />
{#END}
  {#END}
  </map>
  <img src="{$img}" width="{$width}" height="{$height}" border="0" alt="" usemap="#seats">
  <br />
  <h3>Legende:</h3>
  <table cellpadding="4" align="center" border="0">
	{#FOREACH $legend}
	<tr>
	  <td><img src="{$img1.imgname}" border="0" alt="" width="{$img1.width}" height="{$img1.height}"></td>
	  <td>{$desc1}</td>
	  <td><img src="{$img2.imgname}" border="0" alt="" width="{$img2.width}" height="{$img2.height}"></td>
	  <td>{$desc2}</td>
	</tr>  
	{#END}
  </table>
  {#TPL $afterlegend}
  {#IF $admin}
    <p align="right"><i><a href="seatsedit.php?frame=editblock&amp;id={$block_id}">Admin</a></i></p>
  {#END}
{#END}

{#SUB seat}
  <table cellpadding="6" width="400">
    <tr><td align="center">Name: <b>{$name}</b></td></tr>
    {#IF !empty($ip)}<tr><td align="center">IP-Adresse: {$ip}</td></tr>{#END}
  </table>
  <table cellpadding="10" width="400">
	<tr>
	  <td><img src="{$imgname}" width="{$width}" height="{$height}" border="0" alt="" align="center"><br /></td>
	  <td>{$stat}</td>
	</tr>  
  </table>
  <br />
  {#IF !empty($actions)}
  <table cellpadding="4">
	{#FOREACH $actions}
	<tr>
	  <td><img src="{$img.imgname}" border="0" alt="" width="{$img.width}" height="{$img.height}"></td>
	  <td>{$msg}</td>
	  <form method="post">	    
	    <td>
		  <input type="hidden" name="action" value="{$action}"/>
		  <input type="hidden" name="id" value="{$id}"/>
		  <input type="submit" class="button" value="{$button}"/>
		</td>
	  </form>
	</tr>  
	{#END}
	{#IF !empty($exchange)}
	<tr>
	  {#FORM seatexchange}
	  <td></td>
	  <td>
	    Admin: Sitzplatz tauschen mit:<br /> 
	    {#INPUT exchange dropdown param=$exchange}{#INPUT id hidden $id}
	  </td>
	  <td>{#SUBMIT tauschen shortcut=0}</td>
	  {#END}	  
	</tr> 	
	{#END}
  </table>
  {#END}
  <br />
  <br />
  <p align="center">
    Zur&uuml;ck zum <a href="seats.php?frame=block&amp;blockid={$block_id}{$linkpost}">Sitzplan</a>
  </p>
{#END}

{#SUB searchform}
{#FORM search}
  Spielername: {#INPUT search string $search} {#SUBMIT suchen}<br/>
{#END}
<br/>
{#END}

{#SUB search}
<h3><a href="seats.php">Sitzplan&uuml;bersicht</a></h3>
{#VAR sub=searchform}
{#IF empty($blocks)}
Es wurden keine Sitzpl&auml;tze gefunden.<br/>
<a href="seats.php">zum Sitzplan</a>
{#ELSE}
{#FOREACH $blocks $ablock $block_caption}
<h3>{$block_caption}</h3>
{#TABLE $ablock}
	{#COL User %username}
	{#COL Sitzplatz $name}<a href="seats.php?frame=block&amp;blockid={$block_id}&amp;ids[]={$user_id}">{$name}</a>
{#END}
{#END}
{#END}
{#END}
