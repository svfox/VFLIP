{! $Id: lanparty.tpl 1415 2007-06-18 13:56:00Z loom $ }

{#SUB default}
  {#TPL $text}
  <style type="text/css">
    .lanparty_statusbar_normal { font-weight: normal; }
  </style>
  <table width="90%">
    {#IF !empty($check)}
    <tr><td align="center">
      {§Das Konto wurde zu letzt &uuml;berpr&uuml;ft am}: {#DATE $check}
    </td></tr>
    {#END}
    <tr><td align="center">
      <b>{§Dein Status}: <span class="important">{$statuscaption}</span></b><br />{#TPL $status}
    </td></tr><tr><td>        
      {#VAR $bar statusbar textclass=lanparty_statusbar_normal}<br/>
    </td></tr><tr><td align="center" style="padding: 10px 0px 0px 0px;">
      {#FORM}
        {§Suche}: {#INPUT searchtext string $searchtext}
      {#END}
    </td></tr><tr><td align="center">
      {#TABLE $user maxrows=100}
        {#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a> 
        {#COL Clan %clan}{#IF empty($clan_url)}{%clan}{#ELSE}<a href="{%clan_url}" target="_blank">{%clan}</a>{#END}
        {#COL Status $status}{#TABLEDISPLAY lanparty_status $status}{#IF !empty($seat_id)} (<a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{%seat}</a>){#END}
        {#OPTION "auf bezahlt setzen" setpaid right=$setpaidright}
        {#OPTION "auf angemeldet setzen<br/>und reservierten Sitzplatz vormerken" unsetpaid right=$setpaidright}
      {#END}
      {#IF $can_config}<a href="config.php#{$configlistgroupname}">{§zus&auml;tzliche Gruppen in der Teilnehmerliste bearbeiten}</a>{#END}
    </td></tr>
  </table>
{#END}

{#SUB statusbar}
<table style="width:98%; position: relative; top:4px; margin: 0px 0px -16px 0px;" cellspacing="0" border="0" cellpadding="0">
  <tr>
    <td style="width:{$paid.width}%; color:#000000; font-weight:bold;" align="center">{$paid.count}</td>
    <td style="width:{$reg.width}%; color:#000000; font-weight:bold;" align="center">{#IF !$hidereg}{$reg.count}{#END}</td>
    <td style="width:{$free.width}%; color:#000000; font-weight:bold;" align="center">{$free.count}</td>
  </tr>
</table>
<table style="width:98%; border:1px solid black; height: 24px;" cellspacing="0">
  <tr>
    <td style="width:{$paid.width}%; background-color:#FF0000;"></td>
    <td style="width:{$reg.width}%; background-color:#FFFF00;"></td>
    <td style="width:{$free.width}%; background-color:#00FF00;"></td>
  </tr>
</table>
<table style="width:98%;" cellspacing="0" border="0" cellpadding="0">
  <tr>
    <td style="color:#770000;" class="{$textclass}" align="left">{§Bezahlungen}</td>
    <td style="color:#777700;" class="{$textclass}" align="center">{#IF !$hidereg}{§Anmeldungen}{#END}</td>
    <td style="color:#007700;" class="{$textclass}" align="right">{§freie Pl&auml;tze}</td>
  </tr>
</table>
{#END}

{#SUB smallstatusbar}
  <style type="text/css">
    .lanparty_statusbar_small { font-size:11px; font-weight: normal; }
  </style>
  {#VAR sub=statusbar hidereg=1 textclass=lanparty_statusbar_small}
{#END}

{#SUB smallstatusbartext}
<div align="{$align}">
<span class="paid">{$paid.count} {§bezahlt}</span><br />
<span class="registered">{$reg.count} {§angemeldet}</span><br />
<span class="checked_in">{$max.count} {§maximal}</span><br />
</div>
{#END}

{#SUB userstatus}
<div align="left">
<small>{§Status}:<br /></small>
<strong>{$statuscaption}</strong><br />
<small><br />
{§n&auml;chster Schritt}:<br /></small>
{#TPL $status}
</div>
{#END}

{#SUB register}
{#TPL $text}
{#FORM register}
<table border="0" cellpadding="2" cellspacing="0" width="90%"><tr><td>
  {#INPUT lanparty_participate checkbox}
</td><td>
  {#TPL $agbtext}
</td></tr>
</table>	
{#IF count($clans) > 0}
<table border="0" cellpadding="2" cellspacing="0"><tr><td>
<tr><td colspan="2" align="center"><h3>Clan anmelden</h3></td></tr>
{#FOREACH $clans $name $id}
  <tr><td>
  {#INPUT clan_$id checkbox}
  </td><td>
  {%name}
  </td></tr>
{#END}
</table>	
{#END}
{#SUBMIT $action}
{#END}
{#END}

{#SUB showpaid}
  <a href="lanparty.php?frame=paidloggraph&amp;x=1000&amp;y=500"><img src="lanparty.php?frame=paidloggraph&amp;x=600&amp;y=300" alt="Graph"/></a><br/>
  <br/>
  {#TABLE $entrys maxrows=25}
    {#COL Zeitpunkt %date}
    {#COL Username %user_name}
    {#COL Orga %orga_name}
  {#END}
  <br />
  {#Action "Die Liste leeren" emptylog confirmation="Soll der Log wirklich geleert werden?"}
{#END}

{#SUB sidebar}
<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <th colspan="3" align="left">{§Teilnehmer}</th>
  </tr>
  <tr>
    <td colspan="3"><hr style="height:1px;border:0px;border-top:1px solid grey;"></td>
  </tr>
  <tr>
    <td colspan="3" align="right">{$users_max}</td>
  </tr>
  <!-- space between -->
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <!-- space between -->
  <tr>
    <th colspan="3" align="left"><a href="lanparty.php?menudir=27-59" target="_blank">{§Anmeldungen}</a></th>
  </tr>
  <tr>
    <td colspan="3"><hr style="height:1px;border:0px;border-top:1px solid grey;"></td>
  </tr>
  <tr>
    <td width="12">&nbsp;</td>
    <td align="left">{§insgesamt}</td>
    <td align="right">{$users_all}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="registered" align="left">{§angemeldet}</td>
    <td class="registered" align="right">{$users_registered}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="paid" align="left">{§bezahlt}</td>
    <td class="paid" align="right">{$users_paid}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="checked_in" align="left">{§eingechecked}</td>
    <td class="checked_in" align="right">{$users_checked_in}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="online" align="left">{§online}</td>
    <td class="online" align="right">{$users_online}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="online" align="left">{§offline}</td>
    <td class="online" align="right">{$users_offline}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="checked_out" align="left">{§ausgechecked}</td>
    <td class="checked_out" align="right">{$users_checked_out}</td>
  </tr>
  <!-- space between -->
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <!-- space between -->
  <tr>
    <th colspan="3" align="left"><a href="seats.php?menudir=27-61" target="_blank">{§Sitzplan}</a></th>
  </tr>
  <tr>
    <td colspan="3"><hr style="height:1px;border:0px;border-top:1px solid grey;"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="stat_red" align="left">{§reserviert}</td>
    <td class="stat_red" align="right">{$seats_taken}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="stat_yellow" align="left">{§vorgemerkt}</td>
    <td class="stat_yellow" align="right">{$seats_marked}</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="stat_green" align="left">{§frei}</td>
    <td class="stat_green" align="right">{$seats_free}</td>
  </tr>
  <!-- space between -->
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <!-- space between -->
  <tr>
    <th colspan="3" align="left">{§So, und jetzt kommen die wichtigen Daten}</th>
  </tr>
  <tr>
    <td colspan="3"><hr style="height:1px;border:0px;border-top:1px solid grey;"></td>
  </tr>
</table>
{! textclass auf nicht bewirkt die nichtbenutzung der kleinen }
{! Schriftart, welche fuers menu benutzt wird. }
{#VAR sub=smallcounter textclass="nicht_" showdesc=1}
{#END}

{#SUB smallcounter}
<style type="text/css">
.small {
  font-size: 11px;
  font-weight: normal;
}
</style>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td class="stat_green" align="left"><span class="{$textclass}small">{§Abendkasse}</span></td>
    <td class="stat_green" align="right"><span class="{$textclass}small">{$abendkasse}</span></td>
  </tr>
  <tr>
    <td class="stat_yellow" align="left"><span class="{$textclass}small">{§Bezahlt}</span></td>
    <td class="stat_yellow" align="right"><span class="{$textclass}small">{$users_paid}</span></td>
  </tr>
  <tr>
    <td class="stat_red" align="left"><span class="{$textclass}small">{§Besucher}</span></td>
    <td class="stat_red" align="right"><span class="{$textclass}small">{$users_on_party}</span></td>
  </tr>
  <tr>
  	<td class="stat_red" colspan="2">
      <table style="width:100%; position: relative; top:4px; margin: 0px 0px -16px 0px;" cellspacing="0" border="0" cellpadding="0">
        <tr>
          <td style="width:{$users_on_party_width}%; color:#000000; font-weight:bold;" align="center">{$users_on_party}</td>
          <td style="width:{$users_paid_width}%; color:#000000; font-weight:bold;" align="center">{$users_paid}</td>
          <td style="width:{$abendkasse_width}%; color:#000000; font-weight:bold;" align="center">{$abendkasse}</td>
        </tr>
      </table>
      <table style="width:100%; border:1px solid black; height: 24px;" cellspacing="0">
        <tr>
          <td style="width:{$users_on_party_width}%; background-color:#FF0000;"></td>
          <td style="width:{$users_paid_width}%; background-color:#FFFF00;"></td>
          <td style="width:{$abendkasse_width}%; background-color:#00FF00;"></td>
        </tr>
      </table>
      {#IF $showdesc }
      <table style="width:100%;" cellspacing="0" border="0" cellpadding="0">
        <tr>
          <td style="color:#770000;" class="{$textclass}small" align="left">{§Besucher}</td>
          <td style="color:#777700;" class="{$textclass}small" align="center">{§Bezahlt}</td>
          <td style="color:#007700;" class="{$textclass}small" align="right">{§Abendkasse}</td>
        </tr>
      </table>
      {#END}
  	</td>
  </tr>
</table>
{#END}

{#SUB countdown}
{! es stehen auch zur Verf&uuml;gung: $days, $hours, $minutes, $seconds }
<h1>{%msg}</h1>
{#END}

{#SUB countdown_small}
{%msg}
{#END}

{#SUB smalluserstatus}
<style type="text/css">
  .lanparty_check { font-size:14px; font-weight: bold; color:green; }
  .lanparty_small { font-size:11px; font-weight: normal; padding: 0px 2px 0px 2px; margin:0px; }
</style>
<p class="lanparty_small" align="left">
  {#IF $acc == "active"}
    1. <a class="important" href="user.php?frame=register">{§Page Account}</a><br />
  {#ELSEIF $acc == "done"}
    <nobr>1. {§Page Account} <span class="lanparty_check">&radic;</span></nobr><br />
  {#END}
  
  {#IF $reg == "active"}
    2. <a class="important" href="lanparty.php?frame=register">{§Anmeldung}</a><br />
  {#ELSEIF $reg == "done"}
    <nobr>2. {§Anmeldung} <span class="lanparty_check">&radic;</span></nobr><br />
  {#ELSE}
    2. {§Anmeldung}<br />
  {#END}
  
  {#IF $pay == "active"}
    3. <a class="important" href="banktransfer.php">{§&Uuml;berweisung}</a><br />
  {#ELSEIF $pay == "done"}
    <nobr>3. {§&Uuml;berweisung} <span class="lanparty_check">&radic;</span></nobr><br />
  {#ELSE}
    3. {§&Uuml;berweisung}<br />
  {#END}

  {#IF $seat == "active"}
    4. <a class="important" href="seats.php">{§Sitzplatz}</a><br />
  {#ELSEIF $seat == "done"}
    4. {§Sitzplatz}:<br />
    <nobr><a href="seats.php">{$seatname}</a><span class="lanparty_check">&radic;</span></nobr><br />
  {#ELSE}
    4. {§Sitzplatz}<br />
  {#END}
</p>
{#END}

{#SUB kontocheck}
 {#IF !empty($check)}
    {§Letzter Kontocheck}:<br /> {#DATE $check}
  {#END} 
{#END}
