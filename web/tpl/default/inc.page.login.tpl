{#SUB login}
{#IF $isloggedin}
  <form method="post" id="frm" action="">
  <div>
    {#IF $unread_messages}
      <a href="webmessage.php?frame=owner"><img src="{#STATICFILE images/mail_icon.png}" width="32" height="16" alt="" border="0"></a>&nbsp;&middot;&nbsp;
    {#END}        
    <strong>{%user}</strong>&nbsp;&middot;&nbsp;
    <input type="hidden" name="ident" value="Anonymous" />
    <input type="hidden" name="password" value="x" />
    <input type="submit" class="loginbtn" value="logout" />
  </div>
  </form>
{#ELSE}
  <form method="post" id="frm" action="">
  <div>
    <input type="text" class="logined" name="ident" title="Deine UserID, dein Nickname oder deine Email-Adresse" onFocus="if(this.value=='Nickname') this.value='';" {#WHEN "!empty($loginident) and ($loginident != \"Anonymous\")" "value=\"%loginident\" "}/>
    <input type="password" class="logined" name="password" title="Dein Password" onclick="this.value='';" />
    <input type="submit" class="loginbtn" value="login" />
    &nbsp;&nbsp;<a href="text.php?name=user_account_trouble"><strong>?</strong></a>
  </div>
  </form>
  <script type="text/javascript">
  var loginform = document.getElementById('frm');
  {#IF empty($loginident) or ($loginident == "Anonymous")}
    loginform.ident.value='Nickname';	
	loginform.password.value='Password';
  {#ELSE}
	loginform.password.focus();
  {#END}
  </script>
{#END}    
{#END}