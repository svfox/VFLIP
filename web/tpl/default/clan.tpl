{! $Id$ }

{#SUB default}
{#IF $create}<a href="clan.php?frame=editclan">{§neuen Clan erstellen}</a><br/>{#END}
<br/>
{#TABLE $clans}
  {#COL Name %name}<a href="clan.php?frame=viewclan&amp;id={$id}">{%name}</a>
  {#COL Website %homepage}{$homepageLink}
  {#COL Bild $image}<img src="{$image}" alt=""/>
{#END}
{#END}

{#SUB viewclan}
<a href="clan.php">{§zur&uuml;ck zur Liste}</a><br/>
<!-- Claninfos -->
<div>
<h2>{§Infos}</h2>
<table>
{#FOREACH $props}
{#IF $value != ""}
<tr><th valign="top">{$caption}:</th><td>{#IF $val_type == "Image"}{#DBIMAGE $value}{#ELSE}{#IF $val_type == "Text" || substr($val_type, 0, 8) == "Document"}<pre>{$value}</pre>{#ELSE}{$value}{#END}{#END}</td></tr>
{#END}
{#END}
{#IF $leader}
<tr><td colspan="2"><a href="clan.php?frame=editclan&amp;id={$id}">{§Daten bearbeiten}</a></td></tr>
{#END}
</table>
</div>
<br/>
<!-- Clanmitglieder -->
<div>
<h2>{§Mitglieder}</h2>
{#LOAD $id $clan_id}
{#TABLE $members}
	{#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{#IF $isleader}<span class="important">{%name}</span>{#ELSE}{%name}{#END}</a>
	{#ACTION entfernen delmember condition=$leader params="array(clan_id=$clan_id)"}
	{#OPTION "Leader ernennen" nominate condition=$leader}
	{#OPTION "Leader aberkennen" deprive condition=$leader}
{#END}
{#IF $join}<a href="clan.php?frame=join&amp;id={$id}">{§teilnehmen}</a>{#END}
</div>
{#ACTION "Clan l&ouml;schen" del condition=$leader params="array(id=$id)" confirmation="Soll der Clan wirklich gel&ouml;scht werden?" buttonattrs="array(style=\"background-color:#ff8888\")"}
{#IF $leader}
<!-- Anfragen -->
<div>
<h2>{§Anfragen}</h2>
{#TABLE $asking}
	{#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
	{#ACTION hinzuf&uuml;gen confirm params="array(clan_id=$clan_id)"}
{#END}
</div>
{#END}
{#END}

{#SUB editclan}
{#FORM clan}
{#INPUT id hidden $id}
{#INPUT type hidden clan}
<table>
{#FOREACH $items}
	<tr><th>{#WHEN "$required==\"Y\"" "<span class=\"important\">$caption</span>" $caption}</th>
	<td>{#INPUT $name $val_type $val allowempty=$allowempty caption=$caption enabled=$enabled} {$hint}</td></tr>
{#END}
{!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
{#IF false}{#INPUT justtomaketheformanuploadform file enabled=0}{#END}
<tr><td colspan="2" align="center">{#SUBMIT}</td></tr>
</table>
{#END}
<a href="clan.php{#IF !(empty($id) || $id == "create_new_subject")}?frame=viewclan&amp;id={$id}{#END}">{§zur&uuml;ck}</a>
{#END}

{#SUB join}
{#TPL $jointext}
<table><tr><td>
{#FORM join}
{#INPUT yes checkbox $yes}{§Ja, ich will.}<br/>
{#INPUT id hidden $id}
{#INPUT captcha captcha $captcha}<br/>
{#SUBMIT anfragen a}<br/>
<a href="clan.php?frame=viewclan&amp;id={$id}">{§zur&uuml;ck}</a>
{#END}
</td></tr></table>
{#END}