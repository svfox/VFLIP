{! $Id$ }

{! this subs should not be used standalone !}

{#SUB atree}
  <td valign="middle" height="100%">
    <table height="100%" width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr height="20">
        <th class="treehead" style="border-bottom:1px solid black;{#WHEN $isfirstcol "border-left:1px solid black;"}{#WHEN $islastcol "border-right:1px solid black;"}" align="center" valign="middle"><a name="{$roundtitle}">{$roundtitle}</a></th>
      </tr>
      <tr height="10"><td></td></tr>
      {#FOREACH $row index=$i}
        {#LOAD "1*100/count($row)" $matchcellheight}
      <tr>
        <td height="{$matchcellheight}%" align="center" valign="middle" style="white-space:nowrap;padding:0px;margin:0px;border-spacing:0px;">
          <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
          <tr height="50%">
          <td style="white-space:nowrap;" height="100%" class="treecell">
          <div class="{$color1}" style="margin:0px;margin-bottom:-1px;">
          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
          <tr>
          <td>{#IF !empty($team1) && empty($points1) && empty($points2)}{#LOAD $ready1 $ready}{#LOAD $readylink1 $readylink}{#VAR sub=_ready}{#END}{$team1html}</td>
          <td align="right"><small>{$score}</small></td>
          </tr></table>
          </div>
          </td>
          {#LOAD "($color1 == \"winner\") ? \"_atree_winnerlineborderstyle\" : \"_atree_loserlineborderstyle\"" $linestylesub}
          {#LOAD "($color2 == \"winner\") ? \"_atree_winnerlineborderstyle\" : \"_atree_loserlineborderstyle\"" $notlinestylesub}
          {#LOAD "\"bottom\"" $lineposition}
          {#LOAD "\"top\"" $linevalign}
          {#VAR sub=_treelinealgorithm file=$treetypefile}
          </tr>
          <tr height="50%">
          <td style="white-space:nowrap;" height="100%" class="treecell">
          <div class="{$color2}" style="margin:0px;margin-bottom:-1px;">
          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
          <tr>
          <td>{#IF !empty($team2) && empty($points1) && empty($points2)}{#LOAD $ready2 $ready}{#LOAD $readylink2 $readylink}{#VAR sub=_ready}{$team2html}</span>{#ELSE}{$team2html}{#END}</td>
          <td align="right"><small>{$score}</small></td>
          </tr></table>
          </div>
          </td>
          {#LOAD "($color2 == \"winner\") ? \"_atree_winnerlineborderstyle\" : \"_atree_loserlineborderstyle\"" $linestylesub}
          {#LOAD "\"top\"" $lineposition}
          {#LOAD "\"bottom\"" $linevalign}
          {#VAR sub=_treelinealgorithm file=$treetypefile}
          </tr></table>
        </td>
      </tr>
      {#END}
    </table>
  </td>
{#END}

{#SUB _ready}
{#IF empty($ready)}
	{#IF $readylink}<a href="{$readylink}" title="ready"><span style="color:red;">&radic;</a>
	{#ELSE}<span style="color:red;">
	{#END}
{#ELSE}
	<span style="color:green;">
{#END}
{#END}

{#SUB _treelinealgorithm}
          <td valign="{$lineposition}" style="width:12px;height:50%;vertical-align:{$lineposition}">
          {! The smart way, but only Mozilla can draw 50% height and valign correctly :-( }
          {#IF (stristr($browser, "gecko") && !stristr($browser, "konqueror"))}
          <div style="height:50%;overflow:hidden;{#IF ($no*100) % 100/100 == 0 && $no > 1}{#VAR sub=_atree_winnerlinealgorithm file=$treetypefile}{#ELSE}{#VAR sub=_atree_loserlinealgorithm file=$treetypefile}{#END}">
          </div>
          {#ELSE}
          {! so we have to calculate a fix height for all other browsers }
          <div style="width:90%;height:23px{!=23height (class=treecell)};overflow:hidden;{#IF ($no*100) % 100/100 == 0 && $no > 1}{#VAR sub=_atree_winnerlinealgorithm file=$treetypefile}{#ELSE}{#VAR sub=_atree_loserlinealgorithm file=$treetypefile}{#END}">
          </div>
          {#END}</td>
{#END}

{#SUB _atree_winnerlinealgorithm}
{#IF $no>0}border-{$linevalign}:{#VAR sub=$linestylesub file=$treetypestyles};{#END}{#IF $no>1}border-right:{#VAR sub=$linestylesub file=$treetypestyles};{#END}
{#END}

{#SUB _atree_loserlinealgorithm}
{#IF ($no*100) % 100/100==0.75 && $no>1}border-{$linevalign}:{#VAR sub=$linestylesub file=$treetypestyles};border-right:{#VAR sub=$linestylesub file=$treetypestyles};{#ELSE}{#IF $linevalign=="top"}border-top:{#VAR sub=$linestylesub file=$treetypestyles};border-bottom:{#VAR sub=$notlinestylesub file=$treetypestyles};border-right:{#VAR sub=$notlinestylesub file=$treetypestyles};{#ELSE}border-left:{#VAR sub=$linestylesub file=$treetypestyles};{#END}{#END}
{#END}

