{#SUB default}
{#TPL $text}
{#TABLE $events}
  {#IF $edit}<a href="archiv.php?frame=editevent">{§hinzuf&uuml;gen}</a> / <a href="archiv.php?frame=savecurrent">{§aktuelle LAN hinzuf&uuml;gen}</a>{#END}
  {#COL Wann? $time_start}{#DATE $time_start d.m.y} - {#DATE $time_end d.m.y}
  {#COL Was?}<a href="archiv.php?frame=event&amp;id={$id}"><b>{$caption}</b></a>
  {#COL Wo? %location}
{#END}
{#END}

{!************************ Event anzeigen/bearbeiten ********************************}

{#SUB event}
  <table width="420" cellspacing="0" cellpadding="2" border="0" align="center">
    <tr>
      <td>Zeitraum: <b>{#DATE $time_start d.m.y} - {#DATE $time_end d.m.y}</b></td>
      <td align="right">Ort: <b>{%location}</b></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2">{#TPL $text}</td></tr>
  </table>
  <br />
  <table width="320" cellspacing="0" cellpadding="2" border="0" align="center">
    {#FOREACH $groups $article $groupname}
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td class="headline" colspan="2">{%groupname}</td></tr>
    {#FOREACH $article}
    <tr>
      <td>{%caption}</td>
      <td align="right">
        <a href="{%url}" target="{%target}">view</a>
        {#IF $edit}<i><a href="archiv.php?frame=editarticle&amp;event_id={$event}&amp;id={$id}">edit</a></i>{#END}
      </td>
    </tr>
    {#END}
    {#END}
    <tr><td colspan="2">&nbsp;</td></tr>
  </table>
  {#IF $edit}
  <div class="textfooter" style="width:10em;border:1px solid silver;text-align:left;">
    {§Verwaltung}:<br/>
    <i><a href="archiv.php?frame=editarticle&amp;event_id={$id}">{§Artikel hinzuf&uuml;gen}</a></i><br/>
    <i><a href="archiv.php?frame=addtree&amp;event_id={$id}">{§Turnier hinzuf&uuml;gen}</a></i><br/>
    <i><a href="archiv.php?frame=editevent&amp;id={$id}">{§bearbeiten}</a></i><br/>
    <i><a href="archiv.php?action=deleteevent&amp;id={$id}&amp;confirmation={#URL Soll das Ereignis incl. aller Artikel gel&ouml;scht werden? Content wird nicht gel&ouml;scht.}&amp;confirmation_uri=archiv.php">{§l&ouml;schen}</a></i><br/>
  </div>
  {#END}
  <p align="center"><a href="archiv.php">Zur&uuml;ck</a></p>
{#END}

{#SUB editevent}
{#FORM event}
<table>
  <tr><td align="right">Caption:</td><td>{#INPUT caption string $caption}</td></tr>
  <tr><td align="right">Ort:</td><td>{#INPUT location string $location}</td></tr>
  <tr><td align="right">Von:</td><td>{#INPUT time_start date $time_start}</td></tr>
  <tr><td align="right">Bis:</td><td>{#INPUT time_end date $time_end}</td></tr>
  <tr><td align="right">Teilnehmergruppe:</td><td>{#INPUT participant_group_id subjects $participant_group_id param=group}</td></tr>
  <tr><td align="right">Leserecht:</td><td>{#INPUT view_right rights $view_right}</td></tr>
  <tr><td align="right">Schreibrecht:</td><td>{#INPUT edit_right rights $edit_right}</td></tr>
  <tr><td align="right">Text:</td><td>{#INPUT text content $text allowempty=0}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#BACKLINK Zur&uuml;ck}</td></tr>    
</table>
{#INPUT id hidden $id}
{#END}
{#END}

{#SUB savecurrent}
{#FORM current}
<table>
<tr>
  <th>Teilnehmer:</th><td>{#INPUT participant_group_id dropdown $participant_group_id param=$groups}</td>
</tr>
<tr>
  <th>Ort:</th><td>{#INPUT location string $location}</td>
</tr>
</table>
{#SUBMIT}<br/>
{#BACKLINK zur&uuml;ck}
{#END}
{#END}

{!************************ Artikel anzeigen/bearbeiten ********************************}

{#SUB article}
  <p>{#TPL $text}</p>  
  {#IF is_array($url)}<p>Quelle: <a href="{%url.url}" target="_blank">{%url.caption}</a></p>{#END}
  {#IF $edit}<p><i><a href="archiv.php?frame=editarticle&amp;event_id={$event_id}&amp;id={$id}">edit</a></i></p>{#END}
  <br />
  <br />
  <p align="center"><a href="archiv.php?frame=event&amp;id={$event_id}">Zur&uuml;ck</a></p>
{#END}

{#SUB editarticle}
{#FORM article}
<table width="70%">
  <tr><td colspan="2" align="center">
    {§Wenn kein Text angegeben wird, verweist der Link<br />aus der Artikel&uuml;bersicht direkt auf die angegebene URL.}<br />
	<br />
  </td></tr>
  <tr><td align="right">Titel:</td><td>{#INPUT caption string $caption}</td></tr>
  <tr><td align="right">Gruppe:</td><td>{#INPUT group dropdownedit $group param=$groups}</td></tr>
  <tr><td align="right">URL:</td><td>{#INPUT url longstring $url}</td></tr>
  <tr><td align="right">Text:</td><td>{#INPUT text content $text}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#BACKLINK Zur&uuml;ck}</td></tr>    
</table>
{#INPUT id hidden $id}
{#INPUT event_id hidden $event_id}
{#END}
{#END}

{!****************************** Turnierb&auml;ume ******************************************}

{#SUB addtree}
{#FORM tournament}
{#BACKLINK zur&uuml;ck}<br/>
Turnier: {#INPUT tournamentid dropdown $tournamentid param=$tournaments}<br/>
{#INPUT event_id hidden $event_id}
{#SUBMIT}
{#END}
{#END}

{!****************************** Teilnehmer anzeigen ***********************************}

{#SUB participants}
  <table>
  <tr><td><b>Teilnehmer ({$count})</b><br /><br /></td></tr>
  {#FOREACH $users $u}
  <tr><td>{%u}</td></tr>
  {#END}
  </table>
{#END}



