﻿{#SUB default}
	<div class="errorcell" style="color: red; font-weight: bold">
		TableDancer befindet sich zur Zeit in der Beta-Phase!
		Bitte nicht im Produktiveinsatz verwenden.<br/>
		Es wird Java 5.0 (1.5.0) ben&ouml;tigt!
	</div>
	<br />
	<object codetype="application/java-vm" 
			classid="java:tabledancer/TableDancer.class" 
			archive="ext/tabledancer.jar" 
			width="100%"
			height="{$height}">
		<param name="flipid" value="{%ID}"/>
		<param name="useragent" value="{%AGENT}"/>
		<param name="cookiename" value="{%NAME}"/>
	</object>
	<br />
{#END}
