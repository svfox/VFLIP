{#SUB adminbox}
  <a href="tinfo.php?frame=editnews&id=new">News hinzuf&uuml;gen</a>
{#END}

{#SUB default}
  <script language="javascript" type="text/javascript">
    function OpenDlWindow(tid) {
      DlWin = window.open("tinfo.php?frame=showdownloads&tid="+tid+"&nomenu=1", "Downloads", "width=800,height=500");
      DlWin.focus();
    }
    function OpenRulesWindow(tid) {
      RulesWin = window.open("tournament.php?frame=showdescription&nomenu=1&id="+tid, "Turnierregeln", "scrollbars=yes,width=600,height=500,resizable=yes");
      RulesWin.focus();
    }
    function open_info(target_id) {
      if(document.getElementById('l'+target_id).style.display == 'none') {
        document.getElementById('l'+target_id).style.display = 'block';
        document.getElementById('b'+target_id).src = '{#STATICFILE images/minus.gif}';
      }
      else {
        document.getElementById('l'+target_id).style.display = 'none';
        document.getElementById('b'+target_id).src = '{#STATICFILE images/plus.gif}';
      }
    }
    function Nav2TPage(tid) {
      window.location.href = 'tournament.php?frame=overview&id='+tid;
    }
  </script>
  <style type="text/css">
    .title {
    	background: #E3E3E3;
    	border: 1px solid #888888;
    	color: rgb(0,119,204);
    	padding: 3px;
    	font-size: 12px;
    	font-weight: bold;
    	width: 90%;
    }
    
    .info {
    	background-color: #EEEEEE;
    	padding: 3px;
    	width: 90%;
    	border-bottom: 1px solid #888888;
    	border-left: 1px solid #888888;
    	border-right: 1px solid #888888;
    }
    
    .info_text {
    	font-size: 12px;
    	font-weight: bold;
    }
    
    .news_title {
      font-size: 12px;
      background-color: #e3e3e3;
      border: 1px solid #888888;
      padding: 3px;
    }
    
    .news_content {
      font-size: 12px;
      background-color: rgb(238,238,238);
      border: 1px solid #888888;
      border-top: 0px;
      padding: 3px;
      color: black;
    }
  </style>

  <div style="float: left; padding-top: 2px;">
    <h3 align="center">Turniernews</h3>
    {#IF $isadmin}{#VAR sub=adminbox}{#END}
    {#FOREACH $news}
      <table cellspacing="0" cellpadding="5" border="0" width="600px">
       <tr>
        <td>
        
          <table cellspacing="0" cellpadding="0" border="0" width="100%">
           <tr>
            <td class="news_title">
              <b>
                {#DATE $date} - {$caption}&nbsp;
                {#IF $isadmin && !empty($id)}
                  <a href="tinfo.php?frame=editnews&id={$id}">edit</a>&nbsp;
                  <a href="tinfo.php?action=deletenews&id={$id}&confirmation={$conf}">del</a>
                {#END}
              </b>
            </td>
           </tr>
           
           <tr>
            <td class="news_content">
              {$content}
            </td>
           </tr>
          </table>
             
        </td>
       </tr>
      </table>
    {#END}
  </div>

  <div style="margin-left: 605px; padding-top: 1px;">
    <h3 align="center">Turnierliste</h3>
    {#FOREACH $tlist}
      <table cellspacing="5" width="100%">
        <tr>
          <td>
            <div class="title">
              <a href="javascript:open_info('{$id}')">
                <img border="0" src="{#STATICFILE images/plus.gif}" alt="&ouml;ffnen" id="b{$id}">
              </a>
              &nbsp;&nbsp;[ {#TABLEDISPLAY tournament_games $game} - {$teamsize}on{$teamsize} ]
            </div>
            <div class="info" id="l{$id}" style="display: none;">
              <table class="info_text">
                <tr>
                  <td>Typ:&nbsp;&nbsp;</td>
                  <td>{$teamsize}on{$teamsize}</td>
                </tr>
                <tr>
                  <td>Angemeldet:&nbsp;&nbsp;</td>
                  <td>{$participants} von {$maximum}</td>
                </tr>
                <tr>
                  <td>Offizieller Start:&nbsp;&nbsp;</td>
                  <td>{#DATE $start}</td>
                </tr>
                <tr>
                  <td>Kosten:&nbsp;&nbsp;</td>
                  <td>{$coins} {$currency}</td>
                </tr>
                <tr>
                  <td style="padding-top: 10px;" colspan="2">
                    <form action="">
                      <input type="button" style="width:120px;" class="button" value="Downloads" onclick="OpenDlWindow({$id})"><br />
                      <input type="button" style="width:120px;margin-top: 2px;" class="button" value="Regeln" onclick="OpenRulesWindow({$id})"><br />
                      <input type="button" style="width:120px;margin-top: 2px;" class="button" value="Zur Turnierpage" onclick="Nav2TPage({$id})">
                    </form>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
      </table>
    {#END}
  </div>
{#END}

{#SUB editnews}
  {#FORM editnews}
    <table>
      {#INPUT id hidden $id}
      <tr>
        <td>Titel:</td>
        <td>{#INPUT caption string $caption}</td>
      </tr>
      <tr>
        <td>Newstext:</td>
        <td>{#INPUT content text $content}</td>
      </tr>
      <tr align="center">
        <td colspan="2"><br />{#SUBMIT Speichern}</td>
      </tr>
    </table>
  {#END}
  <br />
  {#FORM name}
    {#BACKLINK Zur&uuml;ck}
  {#END}
{#END}

{#SUB downloadsadmin}
  <br />
  <div align="right">
    <div style="border: 1px solid grey;font-size: 10px; width: 120px; text-align: left; padding: 4px;">
      <i>Admin:<br />
      <a href="tinfo.php?frame=editdownload&id=new&tid={$tid}&nomenu=1">Download hinzuf&uuml;gen</a>
      </i>
    </div>
  </div>
{#END}

{#SUB showdownloads}
  <center>
    Hier habt ihr Zugriff auf alle dem Turnier bezogenen Downloads.<br />
    <br />
    {#TABLE $downloads} 
      {#COL Beschreibung $description}{#IF $isadmin}<a href="tinfo.php?frame=editdownload&id={$id}&nomenu=1">{$description}</a>{#ELSE}{$description}{#END}
      {#COL Link $link}<a target="_blank" href="{$link}">{$link}</a>
    {#END}
  </center>
  {#IF $isadmin}
    {#VAR sub=downloadsadmin}
  {#END}
{#END}

{#SUB editdownload}
  <center>
  {#FORM editdownload}
    <table>
      {#INPUT id hidden $id}
      {#INPUT tournament_id hidden $tournament_id}
      <tr>
        <td>Beschreibung:</td>
        <td>{#INPUT description string $description}</td>
      </tr>
      <tr>
        <td>Link:</td>
        <td>{#INPUT link string $link}</td>
      </tr>
      <tr align="center">
        <td colspan="2"><br />{#SUBMIT Speichern}</td>
      </tr>
    </table>
  {#END}
  <br />
  {#ACTION l&ouml;schen deletedownload params=array("id=$id tid=$tournament_id") confirmation="Soll der Download wirklich gel&ouml;scht werden?"}
  <br />
  {#FORM}{#BACKLINK Zur&uuml;ck}{#END}
  </center>
{#END}
