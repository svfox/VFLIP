{#SUB default_alt}
{#TABLE $items}
  <b><a href="config.php?frame=edit">add</a></b>
  {#COL Key %key}
  {#COL Value %value}
  {#COL Beschreibung %description}
  {#OPTION L&ouml;schen delete}  
{#END}
{#END}


{#SUB default}
  {#IF !empty($items)}
	<div class="wide_topdiv">
	<div align="left" style="width:70%">
	<p align="justify">
	  Es gibt drei verschiedene Orte, an denen Konfigurationen vorgenommen werden k&ouml;nnen:<br />
	  <ul>
	    <li>
	      <b>/core/flipconfig.php</b><br />
	      Dies ist die zentrale Konfigurationsdatei. Es besteht kein Webinterface, um sie zu bearbeiten. 
	      Der Zugriff muss also &uuml;ber das Dateisystem erfolgen. In ihr finden sich beispielsweise die Einstellungen
	      f&uuml;r die Mysql-Datenbankanbindung, den Cache und die Logfiles.
	    </li>
	    <li>
	      <b>admin -> config</b><br />
	      Genau, hier an diesem Ort. Diese Einstellungen werden in der Datenbank gespeichert. Sie gelten global
	      f&uuml;r alle User, alle Templates usw. Bestimmt Einstellungen (die <span class="important">rot</span> gekennzeichneten)
	      k&ouml;nnen von den Usern in ihrem Benutzerprofil angepasst werden. Voraussetzung daf&uuml;r ist, dass der 
	      Konfigurationseintrag und die User-Property ein und denselben Key-Namen haben.
	    </li>
	    <li>
	      <b>das Benutzerprofil</b><br />
	      Dort kann der User f&uuml;r ihn pers&ouml;nlich geltende Einstellungen vornehmen. Beispielsweise, wie viele Posts im Forum
	      auf einer Seite angezeigt werden sollen.
	    </li>
	  </ul>
	</p></div></div>
	<table class="wide_table" cellspacing="1" cellpadding="1">
	<tr>
	  <th class="tdedit">Key</th>
	  <th class="tdedit">Value</th>
	  <th class="tdedit" colspan="2"></th>
	</tr>
	{#FORM config action="config.php$rparams"}
	  {#FOREACH $items}
	    {#IF !empty($description)}
	      <tr><td class="tdcont" colspan="4" style="padding-top:12px;">{%description}</td></tr>
	    {#END}
	    <tr>
	      <td class="tdcont" align="right"><a name="{%modul}">{! <--TODO nur beim ersten eintrag von Modul den Anker setzen}<a name="{%key}"></a><b{#WHEN $usercfg " class=\"important\""}>{%key}:</b></td>
	      <td class="tdaction" align="left">
	        {#IF $editable}
	          {#IF empty($values)}
	            {#INPUT $key $type $value}
	          {#ELSE}
	            {#INPUT $key dropdown $value param=$values}
	          {#END}
	        {#ELSE}
	          <div class="edit">{%value}</div>
	        {#END}
	        {#IF ($default_value != $value) and (!empty($default_value))}<i>&nbsp;&nbsp;{%default_value}</i>{#END}
	      </td>
	      <td class="tdedit">{#IF $editable}<a href="config.php?frame=edit&amp;id={$id}">edit</a>{#END}</td>
	      <td class="tdedit">{#IF $editable}<a href="config.php?action=delete&amp;id={$id}&amp;confirmation={#URL Soll der Config-Eintrag wirklich gel&ouml;scht werden?}">del</a>{#END}</td>
	    </tr>
	  {#END}
	  <tr>
	    <td class="tdcont" colspan="3" align="center"><br /><br />{#SUBMIT Speichern}<br /><br /></td>
	    <td class="tdedit"><a href="config.php?frame=edit"><b>add</b></a></td>
	  </tr>
	{#END}
	</table>
{#ELSE}
<b>Zur angegebenen Kategorie gibt es keine Config-Eintr&auml;ge!</b>
<br />
<br />
Alle vorhandenen Kategorien findest du <a href="config.php?frame=categories">hier</a>!
{#END}
{#END}

{#SUB categories}
An dieser Stelle sind alle Config-Kategorien zu sehen, die es gibt.<br />
Um die Variablen einer Kategorie zu betrachten/zu bearbeiten, einfach auf den Kategoriennamen klicken!<br/>
<br />
  {#TABLE $categories}
   {#COL Name $custom_name}<a href="config.php?category={$name}">{#IF empty($custom_name)}{$name}{#ELSE}{$custom_name}{#END}</a>
   {#COL Beschreibung $description}{#IF empty($description)}<i>Keine Beschreibung vorhanden!</i>{#ELSE}{$description}{#END}
   {#FRAME edit "editcategory&name=$name" right=$edit_right}
  {#END}
{#END}

{#SUB editcategory}
 {#FORM editcategory}
  {#INPUT id hidden $id}
  {#INPUT name hidden $name}
  <table>
   <tr>
    <td>Name:</td>
    <td>{#INPUT custom_name string $custom_name}</td>
   </tr>
   <tr>
    <td>Beschreibung:</td>
    <td>{#INPUT description text $description}</td>
   </tr>
   <tr align="center">
    <td colspan="2"><br /><br />{#SUBMIT Speichern}</td>
   </tr>
   <tr align="center">
    <td colspan="2"><br /><br />{#BACKLINK zur&uuml;ck}</td>
   </tr>
  </table>
 {#END}
{#END}

{#SUB edit}
{#FORM item}
<table>
  <tr><td align="right">Key:</td><td>{#INPUT key string $key}</td></tr>
  <tr><td align="right">Typ:</td><td>{#INPUT type inputtypes $type}</td></tr>
  <tr><td align="right" valign="top">
    {#IF empty($values)}
      Value:</td><td>{#INPUT value $type $value}
    {#ELSE}
      Values:</td><td>{#FOREACH $values $value}{#INPUT values[] longstring $value}{#INPUT oldvalues[] hidden $value}<br />{#END}
    {#END}
    <a href="config.php?frame=edit&amp;id={$id}&amp;newfield=1">einen weiteren Wert hinzuf&uuml;gen</a></td></tr>
  <tr><td align="right">Default:</td><td>{#INPUT default_value $type $default_value $values}</td></tr>
  <tr><td align="right">Beschreibung:</td><td>{#INPUT description text $description}</td></tr>
  <tr><td colspan="2" align="center"><br /><br />{#SUBMIT Speichern}</td></tr>
  <tr><td colspan="2" align="center"><br /><br /><a href="config.php">Zur&uuml;ck</a></td></tr>
</table>
{#INPUT id hidden $id}
{#END}
{#END}
