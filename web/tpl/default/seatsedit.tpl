{#SUB blocktools}
<h3 align="center">
  <a href="seatsedit.php?frame=editblocks">Blockliste</a> &middot;
  <a href="seats.php?frame=block&amp;blockid={$id}">Reservierungen</a>
</h3>
<table width="80%" cellpadding=10>
  <tr>
    <td>{#ACTION "neu zeichnen" redraw params="array(ids[]=$id)"}</td>
	<td>
	  Der unten angezeigte Sitzplan wird neu gezeichnet und Werte in der
	  Sitzplan-Tabelle, die von anderen anderen abh&auml;ngig sind,
	  werden neu berechnet.
	</td>
  </tr>
  <tr>
    <td>{#ACTION "Demo erstellen" demo params="array(blockid=$id type=demo)" confirmation="Soll der Sitzplan wirklich neu gezeichnet werden? (Der alte Sitzplan wird dabei gel&ouml;scht!)"}</td>
	<td>
	  Erstellt einen Demo-Sitzplan.<br />
	  <span class="important">Aber Vorsicht</span>, der alte Sitzplan wird dabei gel&ouml;scht!
	</td>
  </tr>
  <tr>
    <td><a href="seatsedit.php?frame=edit&amp;id={$id}">bearbeiten</a></td>
	<td>
	  Es k&ouml;nnen einzelne Sitzpl&auml;tze erstellt und bearbeitet werden oder ganze Reihen hinzugef&uuml;gt werden.
	</td>
  </tr>
</table>
{#DBIMAGE $background_tmp}
{#END}

{#SUB editblocks}
{#VAR sub=header file=inc.seats.tpl}
<p>{§Hier k&ouml;nnen Segmente erstellt, bearbeitet und gel&ouml;scht werden.}</p>
{#TABLE $blocks}
  <a href="seatsedit.php?frame=editblock">{§neu}</a>
  {#COL }<a href="{%href}">{#DBIMAGE $background_tmp process="createThumbnail(100,64)" border=0}</a>
  {#COL "Titel / Beschreibung / Link-Koordinaten" %caption}
    <b><a href="{%href}">{%caption}</a></b><br />
    {%description}<br />
    <i>{%cords}</i>{#IF !empty($link)} (<a href="{%link}">{%link}</a>){#END}
  {#COL "Position<br />Skalierung" align="center"}{$topleft_x}/{$topleft_y}<br />{$scale}
  {#COL bearbeiten align="center"}
    <a href="seatsedit.php?frame=editblock&amp;id={$id}">daten</a><br />
    <a href="seatsedit.php?frame=blocktools&amp;id={$id}">grafisch</a>
  {#OPTION "L&ouml;schen" deleteblock}
  {#OPTION "Neu zeichnen" redraw}
  {#OPTION "Link-Koordinaten neu berechnen" calccords}
{#END}
{#END}

{#SUB editblock}
{#FORM block}
{#INPUT id hidden $id}
<table width="80%">
  <tr><td align="right">{§Titel}:</td><td>{#INPUT caption string $caption}</td></tr>
  <tr><td align="right">{§Leserecht}:</td><td>{#INPUT view_right rights $view_right}</td></tr>
  <tr><td align="right">{§Koordinaten auf &Uuml;bersicht}<sup>1</sup>:</td><td>X:{#INPUT topleft_x integer $topleft_x} Y:{#INPUT topleft_y integer $topleft_y}</td></tr>
  <tr><td align="right">{§Skalierung auf &Uuml;bersicht}<sup>2</sup>:</td><td>{#INPUT scale decimal $scale} (0 => keine Anzeige in &Uuml;bersicht)</td></tr>
  <tr><td align="right">{§Block-Tischbilder}:</td><td>{#INPUT imagedir_block longstring $imagedir_block allowempty=0}</td></tr>
  <tr><td align="right">{§&Uuml;bersichts-Tischbilder}:</td><td>{#INPUT imagedir_overview longstring $imagedir_overview allowempty=0}</td></tr>
  <tr><td align="right">{§Link}<sup>3</sup>:</td><td>{#INPUT link longstring $link}</td></tr>
  <tr><td align="right">{§Link-Koordinaten}<sup>4</sup>:</td><td>{#INPUT cords longstring $cords}</td></tr>
  <tr><td align="right">{§&uuml;18-Block}<sup>5</sup>:</td><td>{#INPUT is_adult checkbox $is_adult}</td></tr>
  <tr><td align="right">{§Beschreibung}:</td><td>{#INPUT description text $description}</td></tr>
  <tr><td align="right">{§Hintergrund}:</td><td>{#INPUT background image $background}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#BACKLINK Zur&uuml;ck}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">
    <p align="justify">
	    <span class="important">Wichtig:</span> Viele &Auml;nderungen an diesen Daten zeigen sich erst, 
	    nachdem der Sitzplan neu gezeichnet wurde!<br />
	    <br />
	    <sup>1</sup> Die Koordinaten auf der &Uuml;bersicht, geben an, an welcher Stelle die 
	    Tische dieses Sitzblockes auf der &Uuml;bersichtsgrafik gezeichnet werden sollen.<br />
	    <sup>2</sup> Die Skalierung auf der &Uuml;bersicht, ist ein Faktor, mit dem alle 
	    Tischkoordinaten multipliziert werden, bevor sie auf der &Uuml;bersicht eingezeichnet werden. 
	    Mit einem Skalierungs-Faktor von <i>0.5</i> ist Sitzblock auf der &Uuml;bersicht ale genau halb so gro&szlig;,
	    wie in der Blockansicht. Achtung: Ist der Skalierungs-Faktor 0, werden die Tische nicht in 
	    die &Uuml;bersicht eingezeichnet.<br />
	    <sup>3</sup> Ein Sitzblock muss nicht notwendiger weise Tische enthalten. Ist dieser 
	    Link angegeben, so verweist die &Uuml;bersicht nicht auf eine Blockansicht, sondern auf 
	    eben diesen Link. Beispielsweise kann ein User, der auf den Stand des Caterers clickt, 
	    so direkt aus die Speisekarte gef&uuml;hrt werden.<br />
	    <sup>4</sup> Die Link-Koordinaten werden direkt in die Imagemap in der &Uuml;bersicht eingetragen.
	    Wenn der Sitzblock Tische enth&auml;lt, k&ouml;nnen (und sollten) sie in der Blockliste automatisch 
	    generiert werden. Beispiel: <i>shape="rect" coords="149,211,351,404"</i><br />
	    <sup>5</sup> Markiert diesen Block explizit f&uuml;r &uuml;ber-18-J&auml;hrige. Damit k&ouml;nnen f&uuml;r diesen Block
	    im Checkin besondere abfragen get&auml;tigt oder im Turniersystem besondere Spiele 
	    freigeschaltet werden.
    </p>
  </td></tr>
</table>
{#END}

{#END}

{#SUB edit}
<a href="seatsedit.php?frame=blocktools&amp;id={$block.id}">zur&uuml;ck zur Blockseite</a>
<h3>Sitzpl&auml;tze:</h3>
<a href="seatsedit.php?frame=editseat&amp;blockid={$block.id}&amp;id=new">hinzuf&uuml;gen</a><br />
<br />
<form method="post">
  <input type="hidden" name="action" value="deleteall"/>
  <input type="hidden" name="blockid" value="{$block.id}"/>
  <input type="hidden" name="confirmation" value="Soll der Sitzplan '{%block.caption}' wirklich komplett <span class=important>gel&ouml;scht</span> werden?"/>
  <input type="submit" value="Sitzplan l&ouml;schen"/>
</form>  
<table border="1">
{#FOREACH $rows}
<tr>
{#FOREACH $cols}
<td>{#IF isset($id)}<a href="seatsedit.php?frame=editseat&amp;blockid={$block.id}&amp;id={$id}">{$name}</a>{#ELSE}&nbsp;{#END}</td>
{#END}
</tr>
{#END}
</table>
{#END}

{#SUB editseat}
<a href="seatsedit.php?frame=edit&amp;id={$block_id}">{§zur&uuml;ck}</a><br />
Hinweis: Eine Standard-Tischgrafik ist {$width0}x{$height0} Pixel gross.<br />
<table style="margin:10px;border-collapse:collapse;">
<tr><th style="padding:10px;border:1px dashed gray;">einzelner Sitzplatz:</th>
<td style="padding:10px;border:1px dashed gray;">
{#FORM editseat}
{#INPUT id hidden $id}
{#INPUT block_id hidden $block_id}
<table>
<tr>
<th>Zentrum X/Y:</th>
<td>{#INPUT center_x integer $center_x allowempty=0}/{#INPUT center_y integer $center_y allowempty=0}</td>
</tr>
<tr>
<th>Winkel:</th>
<td>{#INPUT angle integer $angle}</td>
</tr>
<tr>
<th>Name:</th>
<td>{#INPUT name string $name}</td>
</tr>
<tr>
<th>IP:</th>
<td>{#INPUT ip IP $ip}</td>
</tr>
</table>
{#SUBMIT}<br />
{#END}
{#IF $id!="new"}
<br />
{#FORM delseat}{#SUBMIT l&ouml;schen shortcut=l}{#INPUT seatid hidden $id}{#END}
{#END}
</td></tr>
<tr><th style="padding:10px;border:1px dashed gray;">Sitzplatzreihe:</th>
<td style="padding:10px;border:1px dashed gray;">
{#FORM addrow}
{#INPUT id hidden $id}
{#INPUT block_id hidden $block_id}
<table>
<tr>
<th>Start Zentrum X/Y:</th>
<td>{#INPUT center_x integer $center_x allowempty=0}/{#INPUT center_y integer $center_y allowempty=0}</td>
</tr>
<tr>
<th>Abstand X/Y:</th>
<td>{#INPUT spacex integer $spacex}/{#INPUT spacey integer $spacey}</td>
</tr>
<tr>
<th>Anzahl:</th>
<td>{#INPUT seatno integer $seatno} Sitzpl&auml;tze</td>
</tr>
<tr>
<th>Winkel:</th>
<td>{#INPUT angle dropdown $angle param=$angles}</td>
</tr>
<tr>
<th valign="top">Namen:</th>
<td>
  <table><tr><td>Zeilenenpr&auml;fix</td><td>{#INPUT namerowprefix string $namerowprefix}</td></tr>
  <tr><td>Beschriftungsanfang</td><td>{#INPUT namerowstart string $namerowstart}</td></tr>
  <tr><td>Zeilenensuffix</td><td>{#INPUT namerowsuffix string $namerowsuffix}</td></tr>
  <tr><td>Spaltenpr&auml;fix</td><td>{#INPUT namecolprefix string $namecolprefix}</td></tr>
  <tr><td>Beschriftungsanfang</td><td>{#INPUT namecolstart string $namecolstart}</td></tr>
  <tr><td>Spaltensuffix</td><td>{#INPUT namecolsuffix string $namecolsuffix}</td></tr>
  </table>
</td>
</tr>
<tr>
 <th>Benennungsmodus:</th>
 <td>{#INPUT namingmode dropdown $namingmode param=$namingmodes}</td>
</tr>
<tr>
 <th>erste IP:</th>
 <td>
   {#INPUT ipa integer $ipa}.{#INPUT ipb integer $ipb}.{#INPUT ipc integer $ipc}.{#INPUT ipd integer $ipd}<br />
 </td>
</tr>
<tr>
  <th>IPschema:</th>
  <td>{#INPUT xy dropdown $xy param=$xyarr}</td>
</tr>
</table>
{#SUBMIT shortcut=r}<br />
{#END}
</td></tr>
</table>
{#END}
