{! $Id: sendmessage.tpl 1330 2006-11-26 01:20:57Z scope $ }

{#SUB default}
{#TPL $text}
<h3><a href="sendmessage.php">Rundbriefe</a> &middot; <a href="sendmessage.php?type=sys">System</a></h3>
{#TABLE $mails actions=$action}
  <a href="sendmessage.php?frame=editmessage&amp;type={$type}">add</a> &nbsp;
  <a href="sendmessage.php?frame=send">send</a>
  {#COL Name $name}<a href="sendmessage.php?frame=viewmessage&amp;id={$id}&amp;user={$user}">{$name}</a>
  {#COL Betreff %subject}
  {#COL Beschreibung %description}
  {#FRAME edit editmessage}
  {#FRAME send send}
  {#OPTION L&ouml;schen deletemessage condition="$type!='sys'"}
{#END}
{#END}

{#SUB editmessage}
{#FORM message}
<table width="100%">
  <tr><td>{#TPL $text}</td></tr>
  <tr><td>G&uuml;ltige Variablen: {%vars}<br /><br /></td></tr>
  <tr><td>Name: {#INPUT name string $name}</td></tr>
  <tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="85">Beschreibung:</td><td>{#INPUT description longstring $description}</td></tr></table></td></tr>
  <tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="43">Betreff:</td><td>{#INPUT subject longstring $subject allowempty=0}</td></tr></table></td></tr>
  <tr><td>Nachricht:<br />{#INPUT message documentwrap $message allowempty=0}</td></tr>
  <tr><td align="center"><br /><br />{#SUBMIT Speichern}</td></tr>
  <tr><td align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT id hidden $id}
{#INPUT type hidden $type}
{#END}
{#END}

{#SUB viewmessage}
<style type="text/css">
  .mailcell{ 
    background-color: white;
	border-color:black;
	border-width:1px;
	border-style:solid;
	padding: 5px;
	margin-left: 0px;
	margin-top: 3px;
	margin-right: 0px;
	margin-bottom: 6px;
  }
</style>
<table width="600">
  <tr><td colspan="3">{#TPL $text}<br /></td></tr>
  <tr>
    {#FORM method=get action=sendmessage.php}<td>
	  {#INPUT frame hidden viewmessage}
      {#INPUT id hidden $id}
	  {#INPUT user userfromgroup $user param=registered}
	  {#SUBMIT Wechseln}
    </td>{#END}
	<td align="center"><b><a href="sendmessage.php?type={$type}">&Uuml;bersicht</a></b></td>
	<td align="right"><b><a href="sendmessage.php?frame=editmessage&amp;id={$id}">Bearbeiten</a></b></td>
  </tr>
  <tr><td colspan="3"><br /><br />Betreff: <pre class="mailcell">{%subject}</pre></td></tr>
  <tr><td colspan="3">Nachricht: <pre class="mailcell">{%message}</pre></td></tr>
</table>
{#END}

{#SUB send}
{#FORM action="sendmessage.php?frame=sending"}
<table width="100%">
  <tr><td>{#TPL $text}</td></tr>
  <tr><td>G&uuml;ltige Variablen: {%vars}<br /><br /></td></tr>
  <tr><td>
    <table cellpadding="2" cellspacing="0" width="100%">
      <tr><td width="80">Empf&auml;nger:</td><td>{#INPUT receiver subjects param=$to allowempty=0} <a href="{#EDITURL to=$changeto}">list {$changeto}s</a></td></tr>
	  <tr><td width="80">NachrichtenTyp:</td><td>{#INPUT messagetype dropdown email param=$messagetypes}</td></tr>
	  <tr><td width="80">Betreff:</td><td>{#INPUT subject longstring $subject allowempty=0}</td></tr>
	  <tr><td colspan="2">{#INPUT forceenabled checkbox 1} Die Nachrichten nur an aktivierte Accounts in Gruppen senden.</td></tr>
	  <tr><td colspan="2">{#INPUT sendhtml checkbox 1} Die Nachrichten als HTML-Mail versenden.</td></tr>
    </table>
  </td></tr>
  <tr><td>Nachricht:<br />{#INPUT message documentwrap $message allowempty=0}</td></tr>
  <tr><td align="center"><br /><br />{#SUBMIT Senden}</td></tr>
  <tr><td align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT id hidden $id}
{#INPUT type hidden $type}
{#END}
{#END}

{#SUB sending}
<b>{%subject}</b> wird versendet:
<br />
<br />
{#TABLE $users}
  {#COL User}{%names}
  {#COL status style="background-image:url(sendmessage.php?frame=sendto&amp;$ids&amp;anticache=$time);"}
{#END}
<p>
  <span class="important">Achtung:</span><br />
  Erst wenn alle Statusfelder ein farbiges Bild geladen haben wurden alle Nachrichten gesendet und du kannst auf "Weiter" klicken!
</p>
<p>
  {#FORM action="sendmessage.php?frame=result"}{#SUBMIT Weiter}{#END}
</p>
{#END}

{#SUB result}
<p>
{#IF $errors}
  Es wurde versucht die Nachrichten zu versenden, allerdings sind dabei Fehler aufgetreten. 
  Bitte entnimm selbst den Fehlermeldungen an welchen Stellen etwas Schiefgelaufen ist.
{#ELSE}
  Alle Nachrichten wurden erfolgreich versandt.
{#END}
</p>
<p>
  <a href="sendmessage.php">Zur&uuml;ck</a>
</p>
{#END}
