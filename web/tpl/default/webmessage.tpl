{#SUB header}
<h3>
  <a href="webmessage.php?frame=owner&amp;id={$user_id}"{#WHEN "$frame == 'owner'" " class=\"important\""}>Posteingang</a> &middot;
  <a href="webmessage.php?frame=sender&amp;id={$user_id}"{#WHEN "$frame == 'sender'" " class=\"important\""}>Postausgang</a> &middot;
  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}"{#WHEN "$frame == 'sendmessage'" " class=\"important\""}>neue Nachricht</a>
</h3>
{#END}

{#SUB default}
{#VAR sub=header}
{#END}

{#SUB owner}
{#VAR sub=header}
<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr><td valign="top" width="20%">
{§Ordner} (<a href="webmessage.php?frame=editfolder&amp;id={$user_id}">{§verwalten}</a>):<br />
<br />
<a href="webmessage.php?frame=owner{#IF !empty($user_id)}&amp;id={$user_id}{#END}">{§Posteingang}</a>
<ul>
{#FOREACH $folders $afoldername $afolderid}
<li><a href="webmessage.php?frame=owner&amp;folder_id={$afolderid}{#IF !empty($user_id)}&amp;id={$user_id}{#END}">{%afoldername}</a></li>
{#END}
</ul>
</td><td>
{#TABLE $messages width="90%" maxrows=20}
  {#COL Sender $sender width="120" align="center"}<a href="user.php?frame=viewsubject&amp;id={$sender_id}">{%sender}</a>
  {#COL Gesendet $date width="100" align="center"}{#DATE $date}
  {#COL Betreff %subject}
    <a {#WHEN "$status == 'unread'" "class=\"important\" "}{#WHEN "$status != 'processed'" "style=\"font-weight:bold;\" "}href="webmessage.php?frame=viewmessage&amp;id={$user_id}&amp;message={$id}&amp;box=owner">{%subject}</a>
  {#COL "" title="Antworten" width="16" align="center"}
    <b><a {#WHEN $is_replay "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=replay&amp;message_id={$id}">&lt;&lt;</a></b>
  {#COL "" title="Weiterleiten" width="16" align="center"}
    <b><a {#WHEN $is_oforward "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=oforward&amp;message_id={$id}">&gt;&gt;</a></b>  
  {#OPTION L&ouml;schen delete}{#INPUT type hidden owner}
  {#OPTION Als setstatus}
  {#INPUT status dropdown param="array(read=gelesen unread=ungelesen processed=bearbeitet)"} &nbsp;markieren<br />
  {#OPTION Nach movetofolder}
  {#INPUT folder_id dropdown param=$allfolders} &nbsp;verschieben<br />
{#END}
</td></tr>
</table>
{#END}

{#SUB sender}
{#VAR sub=header}
{#TABLE $messages width="90%" maxrows=20}
  {#COL Empf&auml;nger %owner width="120" align="center"}<a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>
  {#COL Gesendet $date width="100" align="center"}{#DATE $date}
  {#COL Betreff %subject}    
    <b><a href="webmessage.php?frame=viewmessage&amp;id={$user_id}&amp;message={$id}&amp;box=sender">{%subject}</a></b>
    {#IF !empty($processor_id) and ($sender_id != $processor_id)}<span style="color:gray">von</span> <a href="user.php?frame=viewsubject&amp;id={$processor_id}">{%processor}</a>{#END}
  {#COL "" title="Noch einmal senden" width="16" align="center"}
    <b><a {#WHEN $is_resend "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=resend&amp;message_id={$id}">&gt;</a></b>
  {#COL "" title="Weiterleiten" width="16" align="center"}
    <b><a {#WHEN $is_sforward "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=sforward&amp;message_id={$id}">&gt;&gt;</a></b>
  {#OPTION L&ouml;schen delete}{#INPUT type hidden sender}
{#END}
{#END}

{#SUB editfolder}
<a href="webmessage.php?frame=owner{#IF !empty($user_id)}&amp;id={$user_id}{#END}">{§zur&uuml;ck}</a>
<h2>Ordner {§hinzuf&uuml;gen}/{§bearbeiten}</h2>
{#FORM folder}
{#INPUT folder_id hidden $folder_id}
Name: {#INPUT foldername string}<br />
{#SUBMIT anlegen a}
{#END}
{#IF !empty($folders)}
<h2>{§Ordner} {§l&ouml;schen}</h2>
{#FORM delfolder}
{#INPUT folder_id dropdown $folder_id param=$folders}
{#SUBMIT l&ouml;schen l}
{#END}
{#END}
{#END}

{#SUB sendmessage}
{#VAR sub=header}
{#FORM message}
<table width="100%">
  <tr><td width="70">Empf&auml;nger:</td><td>{#INPUT receiver string $receiver} <i>(UserID, Nickname oder Email-Addresse)</i></td></tr>
  <tr><td width="70">Betreff:</td><td>{#INPUT subject longstring $subject allowempty=0}</td></tr>
  <tr><td colspan="2" style="padding-left: 70px;">{#INPUT append_signature checkbox $append_signature} Signatur anf&uuml;gen</td></tr>
  <tr><td colspan="2">Nachricht:<br />{#INPUT message documentwrap $message allowempty=0}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2" align="center">{#SUBMIT Senden}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT source_id hidden $source_id}
{#INPUT source_type hidden $source_type}
{#END}
{#END}

{#SUB viewmessage}
{#VAR sub=header}
<style type="text/css">
  .mailcell{ 
    background-color: white;
    border-color:black;
    border-width:1px;
    border-style:solid;
    padding: 3px;
    margin-left: 0px;
    margin-top: 3px;
    margin-right: 0px;
    margin-bottom: 6px;
  }
</style>
<br />
<table cellpadding="0" cellspacing="0" width="90%">
  <tr><td><table width="100%">
    <tr><td width="70">Optionen:</td>
      <td><b>
        {#IF $box == "owner"}
          <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=replay&amp;message_id={$id}">Antworten</a> &middot;
          <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=oforward&amp;message_id={$id}">Weiterleiten</a>&middot;
          {#ACTION L&ouml;schen delete "webmessage.php?frame=owner&id=$user_id" params="array(ids[]=$id type=owner)" formattrs="array(style=display:inline;)"}
          &nbsp;&middot;&nbsp;
          {#IF $status != "processed"}
            {#ACTION bearbeitet setstatus params="array(ids[]=$id status=processed)" formattrs="array(style=display:inline;)"}
          {#ELSE}
            {#ACTION unbearbeitet setstatus params="array(ids[]=$id status=read)" formattrs="array(style=display:inline;)"}          
          {#END}
        {#ELSEIF $box == "sender"}
          <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=resend&amp;message_id={$id}">Nochmals senden</a> &middot;
          <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=sforward&amp;message_id={$id}">Weiterleiten</a>&middot;
          {#ACTION L&ouml;schen delete "webmessage.php?frame=sender" params="array(ids[]=$id type=sender)" formattrs="array(style=display:inline;)"}
          </b></td>
        </tr>
        <tr><td width="70">Status:</td>
          <td><b>
          {#IF $status == "processed"}
            bearbeitet
          {#ELSEIF $status == "unread"}
            ungelesen
          {#ELSEIF $status == "read"}
            gelesen
          {#ELSE}
            unbekannt
          {#END}
        {#END}
      </b></td>
    </tr>
    <tr><td width="70">Absender:</td><td class="mailcell">
      <a href="user.php?frame=viewsubject&amp;id={$sender_id}">{%sender}</a>
      {#IF $processor_id}(<span style="color:gray">bearbeitet von</span> <a href="user.php?frame=viewsubject&amp;id={$processor_id}">{%processor}</a>){#END}
    </td></tr>
    <tr><td width="70">Empf&auml;nger:</td><td class="mailcell">
      <a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>
    </td></tr>
    <tr><td width="70">Betreff:</td><td class="mailcell"><b>{%subject}</b></td></tr>
  </table></td></tr>
  <tr><td colspan="2" height="300">
    <table height="300" width="100%"><tr><td valign="top" class="mailcell" style="padding: 8px;">{$text}</td></tr></table>
  </td></tr>
  {#IF !empty($related)}
  <tr><td colspan="2">
    <br />
    <br />
    Zugeh&ouml;rige Nachrichten:
    <ul>{#FOREACH $related}
      <li>
        {#IF $source_type == "replay"}Antwort{#ELSEIF $source_type == "resend"}Nochmals gesendet{#ELSE}Weitergeleitet{#END}
        an <a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>:
        <b><a href="webmessage.php?frame=viewmessage&amp;message={$id}&amp;box=sender&amp;id={$user_id}">{%subject}</a></b>
      </li>  
    {#END}</ul>
  </td></tr>
  {#END}
</table>
{#END}

{#SUB contact}
{#FORM contact}
<table>
  <tr>
    <td colspan="2">{#TPL $text}</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  {#IF !$loggedin}
  <tr>
    <td colspan="2">
      <span class="important">Wichtig:</span> Wenn du auf dieser Seite bereits einen 
      Account hast, dann logge dicht mit diesem ein, bevor du die Nachricht schreibst.<br />
      <br />
    </td>
  </tr>
  {#END}
  <tr>
    <td align="right" width="10%">Name:</td>
    <td>{#IF $loggedin}<div class="edit">{%name}</div>{#ELSE}{#INPUT name string allowempty=0}{#END}</td>
  </tr>
  <tr>
    <td align="right" width="10%">EMail:</td>
    <td>{#IF $loggedin}<div class="edit">{%email}</div>{#ELSE}{#INPUT email email allowempty=0}{#END}</td>
  </tr>
  <tr>
    <td align="right" width="10%">Betreff:</td>
    <td>{#INPUT subject longstring $test}</td>
  </tr>
  <tr>
    <td colspan="2">Nachricht: {#INPUT message document allowempty=0}</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
    <td colspan="2" align="center">{#INPUT Sicherheitsabfrage captcha}{#SUBMIT Absenden}</td>
  </tr>
</table>
{#INPUT prefix hidden $prefix}
{#END}
{#END}

{#SUB smallnewmessages}
<div align="left" style="overlay:none; overflow: hidden;">
{#FOREACH $items}
  <nobr>
  {#IF $status=='unread'}   
    <img src="{#STATICFILE images/webmessage/message_unread.png}" width="15" height="10" alt=""/>
    <a class="important" href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">{%subject}</a><br />
  {#ELSE}
    <img src="{#STATICFILE images/webmessage/message_read.png}" width="15" height="10" alt=""/>
    <a href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">{%subject}</a><br /> 
  {#END}
  </nobr>
{#END}
</div>
{#END}
