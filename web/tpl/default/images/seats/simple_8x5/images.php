<?php
$ImageClass = "Simple8x5SeatIcons";

class Simple8x5SeatIcons extends AbstractSeatImages {

	function DrawSeatUnselected($Image, $Seat) {
        $img = false;
        if($Seat["user_status"]) $img = $this->loadImage($Seat["user_status"].".png");
        if(!$img) $img = $this->loadImage(GetSeatColor($Seat).".png");
        if(!$img) return DrawDummySeat($Image, $Seat);
        else return $Image->drawRotated($img, $Seat["center_x"], $Seat["center_y"], $Seat["angle"], true);
	}
	
	function getLegend() {
		return array (array ("img1" => null, "desc1" => "not yet implemented"));
	}
}
?>