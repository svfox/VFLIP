{#SUB online}
  <style type="text/css">
    .statistics_small { font-size: 11px; }
  </style>
  <table width="97%" title="Besucher der letzten {$maxlastseen} Min">
    {#FOREACH $users}
    <tr>
      <td class="statistics_small">
        <div style="overflow:hidden; width:100%;">
          <a href="user.php?frame=viewsubject&amp;id={$user_id}">{%name}</a>
          {#WHEN "$count > 1" "($count)"}
        </div>
      </td>
      <td align="right" class="statistics_small" style="white-space:nowrap;">
        {$time} Min
      </td>
    </tr>
    {#END}
    {#IF $islimit}
    <tr><td colspan="2" class="statistics_small">(...)</td></tr>
    {#END}
    {#IF $guests > 0}
    <tr><td colspan="2" class="statistics_small">
      {#WHEN "$guests == 1" "ein Gast" "$guests G&auml;ste"}
    </td></tr>
    {#END}
    {#IF $hiddencount > 0}
    <tr><td colspan="2" class="statistics_small">
      {#WHEN "$hiddencount == 1" "ein Versteckter" "$hiddencount Versteckte"}
    </td></tr>
    {#END}
  </table>
{#END}

{#SUB registeredusers}
<p>Hier werden alle Benutzer aufgelistet, welche im ActionLog stehen. Die neuesten stehen oben.</p>
<ul>
{#FOREACH $user}
  <li><span style="color:{#WHEN $aktiv green red};">#</span> {%name} ({#DATE $time})</li>
{#END}
</ul>
{#END}