{! $Id$ }

{#SUB default}
<!-- Suchformular -->
{#FORM method=get action=search.php?frame=result}
<table border="0" cellspacing="1" cellpaddig="1" class="table">
{#IF isset($nextframelink)}
	{#INPUT nextframelink hidden $nextframelink}
{#END}
<tr>
  <th class="tdedit" style="text-align:right;">Suchtext:</th>
  <td class="tdcont">{#INPUT searchstring string $searchstring}</td>
</tr>
<tr>
  <th class="tdedit" style="text-align:right;">Nur Titel durchsuchen:</th>
  <td class="tdcont">{#INPUT titleonly checkbox $titleonly}</td>
</tr>
<tr>
  <th class="tdedit" style="text-align:right;">Modus:</th>
  <td class="tdcont">{#INPUT mode dropdown $mode param=$modes}</td>
</tr>
<tr><th class="tdedit" colspan="2">Module:</th></tr>
<tr><td class="tdcont" colspan="2">{#FOREACH $modules}{#INPUT usedmodules[$name] checkbox $checked}{$name} {#END}</td></tr>
</table>
<br />
{#SUBMIT Suchen}
{#END}
<br />
<!-- Ergebnisse -->
<h2>Suchergebnisse</h2>
<u>{$count} Eintr&auml;ge in {$time} Sekunden gefunden</u>
<br />
<br />
{#IF $usenextlink}
<h3 style="color: #008800;">Klicke auf den Titel (Usernamen) um fortzufahren!</h3>
{#END}
{#TABLE $results maxrows=50}
  {#COL Titel %title}<a href="{$link}"{#WHEN $link_new " target=_blank"}>{%title}</a>
  {#COL Text $text}
  {#COL Modul $mod}
{#END}
{#END}