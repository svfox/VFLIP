{#SUB default}
{#TABLE $items}
  <a href="table.php?frame=edittable">add</a>
  {#COL Name %name}<a href="table.php?frame=viewtable&amp;id={$id}">{%name}</a>
  {#COL Schreibrecht}{#RIGHT $edit_right edit}
  {#COL Beschreibung %description}
  {#FRAME edit edittable right=$edit_right}
  {#OPTION L&ouml;schen deletetable}
{#END}
{#END}

{#SUB edittable}
{#FORM table}
<table width="70%">
  <tr><td align="right">Name:</td><td>{#INPUT name name $name allowempty=0}</td></tr>
  <tr><td align="right">Schreibrecht:</td><td>{#INPUT edit_right rights $edit_right}</td></tr>
  <tr><td align="right">Beschreibung:</td><td>{#INPUT description longstring $description}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT id hidden $id}
{#END}
{#END}

{#SUB viewtable}
<h3><a href="table.php">&Uuml;bersicht</a></h3>
{#TABLE $items}
  {#IF $allowedit}<a href="table.php?frame=edititem&amp;table_id={$table_id}">add</a>{#END}
  {#COL Key $key}
  {#COL Value %value}
  {#COL Display %display}
  {#FRAME edit edititem condition=$allowedit}
  {#OPTION L&ouml;schen deleteitem condition=$allowedit}
{#END}
<p style="width:60%">
  &Uuml;ber den <i>Key</i> wird ein Datensatz intern angesprochen, die <i>Value</i> sieht der 
  User in Dropdownboxen und den Wert von <i>Display</i> wann immer ein einzelner Wert angezeigt wird.
  <i>Display</i> darf sowohl HTML als auch Templatecode enthalten.
</p>
{#END}

{#SUB edititem}
{#FORM item}
<table>
  <tr><td align="right">Key:</td><td>{#INPUT key name $key}</td></tr>
  <tr><td align="right">Value:</td><td>{#INPUT value longstring $value allowempty=0}</td></tr>
  <tr><td colspan="2">Display: <i>(darf HTML und Templatecode enthalten)</i></td></tr>
  <tr><td colspan="2">{#INPUT display text $display}</td></tr>
  <tr><td colspan="2">
    Wenn das Feld <i>Display</i> leer gelassen wird,<br />
    wird automatisch der Wert des Feldes <i>Value</i> verwendet.
  </td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT id hidden $id}
{#INPUT table_id hidden $table_id}
{#END}
{#END}