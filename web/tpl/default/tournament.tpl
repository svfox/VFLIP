{! 
  $Id: tournament.tpl 1466 2007-09-17 13:40:59Z loom $
}

{#SUB defaultlink}
<h3><a href="{$file}?frame=default"{#WHEN "empty($curframe) || $curframe == 'default'" " class='important'"}>Turnierliste</a>
{#IF $logged_in=="true"} &middot; <a href="{$file}?frame=myinfo"{#WHEN "$curframe == 'myinfo'" " class='important'"}>Meine pers&ouml;nliche Seite</a>{#END}
</h3>
{#END}

{#SUB tournamenthead}
<div align="center">
{#VAR sub=defaultlink}
<h3>
{#VAR sub=menu file=inc.tournament.tpl}
</h3>
</div>
{#END}

{#SUB AdminLink}
<div align="right">
<table>
{#IF !empty($adminlinks)}
<tr><td class="textfooter" style="border:1px solid gray;padding:4px;">Admin:<br />
  {#FOREACH $adminlinks}
  <div class="textfooter"><a class="textfooter" href="turnieradm.php?frame={$frame}&amp;id={$tournamentid}">{$linktext}</a></div>
  {#END}
</td></tr>
{#END}
{#IF !empty($tournamentfooter)}
<tr><td class="textfooter" style="border:1px solid gray;padding:4px;">Sonstiges:<br />
  {#FOREACH $tournamentfooter}
  <div class="textfooter"><a class="textfooter" href="{$url}">{$linktext}</a></div>
  {#END}
</td></tr>
{#END}
</table>
</div>
{#END}

{#SUB Coins}
{#IF isset($coins)}Dein aktuelles Guthaben betr&auml;gt{#IF !is_array($coins)} <strong>{$coins} {$currency}</strong>.<br />{#ELSE}:<br/>
<table>
{#FOREACH $coins}
<tr>
  <td>{#WHEN empty($currency) "Unbekannt" $currency}</td><td><strong>{$coins}</strong></td>
</tr>
{#END}
</table>
{#END}
{#END}
{#END}

{#SUB default}
{#VAR sub=defaultlink}
{#IF $coins > 0}<a href="tournament.php?frame=coinoverview">Kosten&uuml;bersicht der Turniere</a><br />{#END}
<br />
{#IF count($turniertypen) > 1}
<h3>
{#FOREACH $turniertypen}
<a href="#{$caption}">{$caption}</a>
{#MID}
 &middot; 
{#END}
</h3>
{#END}
{#VAR sub=_tournament_list}
<br />
{#IF !empty($tournamentfooter)}{#VAR sub=AdminLink}{#END}
{#END}

{#SUB _tournament_list}
<div align="center">
{#FOREACH $turniertypen}
<h1><a name="{$caption}"></a>{#IF $image}{#DBIMAGE $image alt=$caption}{#ELSE}{$caption}{#END}</h1>
{#TABLE $turniere}
  {#COL Spiel $game height=32 style="padding:1px 6px"}{#DBIMAGE flip_tournament_tournaments icon_small $id $mtime align=middle hspace=4px}{$gamelink}
  {#COL Typ $teamsize align=center}{$teamsize}on{$teamsize}
  {#COL Teams $count align=right}{$count}/{$maximum}
  {#COL Status $status}
  {#COL Start $start}{$timestr}
  {#COL Kosten $coins align=right style="padding:1px 6px"}{$coins}&nbsp;{$currency}
{#END}
{#END}
</div>
{#END}

{#SUB myinfo}
{#VAR sub=defaultlink}
{#VAR sub=Coins}
<h2>Meine Teams</h2>
{#TABLE $teams}
  {#COL Name $teamname}
  {#COL Mitglieder $count align=right}
  {#COL Turniere $tcount align=center}{#IF $tcount == 1}{$tournament}{#ELSE}{$tcount} {§Turniere}{#END}
{#END}
<h2>Meine Matches</h2>
{#TABLE $matches}
  {#COL Turnier $tournament}
  {#COL Runde $level}<a href="{$file}?frame=tree&amp;id={$tournament_id}#{$level}">{$level}</a>
  {#COL Gegner $vs}
  {#COL Status $status align=center}
  {#COL Zeit $endtime}{#IF $endtime>0}{#IF $status=="offen"}<strong>{#IF $endtime>time() && $endtime < time()+30*60}<span class="important">{#DATE $endtime}</span>{#ELSE}{#DATE $endtime}{#END}</strong>{#ELSE}{#DATE $endtime}{#END}{#ELSE}{§unbegrenzt}{#END}
  {#COL Server %server}
{#END}
<h2>Meine Turniere</h2>
{#FOREACH $turniere $turniertyp $caption}
<h3>{#WHEN empty($caption) "Sonstige" %caption}</h3>
{#TABLE $turniertyp}
  {#COL Platz $rank align=right}{#IF !empty($rank)}{$rank}.{#END}
  {#COL Spiel $game}<a href="tournament.php?frame=overview&amp;id={$id}">{$game}</a>
  {#COL Typ $teamsize align=center}{$teamsize}on{$teamsize}
  {#COL Teams $count align=right}{$count}/{$maximum}
  {#COL Status $status}
  {#COL Start $start}{$timestr}
  {#COL Kosten $coins align=right}{$coins} {$currency}
{#END}
{#END}
{#END}

{#SUB coinoverview}
{#VAR sub=defaultlink}
{#VAR sub=Coins}
{#FOREACH $groups key=$currency}
<h3>{#WHEN empty($currency) "Unbekannt" $currency}</h3>
{#TABLE $turniere}
  {#COL Turnier $name}<a href="tournament.php?frame=overview&amp;id={$id}">{%name}</a>
  {#COL Kosten $coins}{$coins} {$currency}
  {#ACTION + addcoins right=$adminright}
  {#ACTION - remcoins right=$adminright}
{#END}
<br /><br />
{#END}
{#END}

{#SUB screenshots}
  <div align="center">
{#IF empty($screens)}
    Keine Screenshots vorhanden.
{#ELSE}
  {#FOREACH $screens var=$name}
    <img src="image.php?name={$name}" alt="{%name}"/><br />
    <br />
  {#END}
{#END}
  </div>
  <a href="tournament.php?frame=tree&id={$tournamentid}">{§zum Turnierbaum}</a>
{#END}

{#SUB Overview}
{#VAR sub=tournamenthead}
{#DBIMAGE flip_tournament_tournaments icon $id $mtime}<br />
<br />
<table border="0" class="table" cellspacing="0" cellpadding="3">
<tr>
<td colspan="4" class="tdedit"><h3 align="center">Info</h3></td>
</tr><tr>
<th class="tdcont">Orga:</th><td class="tdcont">{#FOREACH $orgas $organame $orgaid}<a href="user.php?frame=viewsubject&amp;id={$orgaid}">{%organame}</a>{#MID}, {#END}</td>
<th class="tdcont">&nbsp;&nbsp;&nbsp; Kosten:</th><td class="tdcont">{$coins} {$currency}</td>
</tr><tr>
<th class="tdcont">Beginn:</th><td class="tdcont">{$begin}</td>
<th class="tdcont">&nbsp;&nbsp;&nbsp; Teams:</th><td class="tdcont">{$numteams}</td>
</tr><tr>
<th class="tdcont">Status:</th><td class="tdcont">{$status}</td>
<th class="tdcont">&nbsp;&nbsp;&nbsp; Spieler:</th><td class="tdcont">{$numplayers}</td>
</tr><tr>
<th class="tdcont">Modus:</th><td class="tdcont">{$mode}</td>
<th class="tdcont">&nbsp;&nbsp;&nbsp; Rundendauer:</th><td class="tdcont">{$round}</td>
</tr>
</table>
<table widht="90%" border="0" cellspacing="0" cellpadding="8">
<tr><td valign="top">
<h3 align="center">Beschreibung</h3>
{$TDescription}
</td><td valign="top">
<h3 align="center">Teilnehmer</h3>
{#IF is_numeric($anmeldeid)}
<a href="{$file}?frame=getTeam&amp;id={$anmeldeid}">Anmeldung</a><br />
{#ELSE}
{%anmeldeid}<br />
{#END}
<br />
<table cellspacing="0" border="1" cellpadding="2">
  <tr class="hdr">
    <th>Team</th>{#IF $teamsize > 1}{#IF $anmeldestatus}<th>Aktion</th>{#END}{#END}<th>Nickname</th>
  </tr>
{#FOREACH $teams index=$i}
  <tr class="{#IF ($i % 2 == 1)}row2{#ELSE}row1{#END}">
    <td>{$team}</td>
    {#IF $teamsize > 1}
    {#IF $anmeldestatus}<td><i>{$action}</i></td>{#END}
    {#END}
    <td>
      {#FOREACH $players index=$i}<a href="user.php?frame=viewsubject&amp;id={$userid}">{$nick}</a>{#MID}, {#END}
    </td>
  </tr>
{#END}
</table>
</td></tr>
</table>
{#VAR sub=AdminLink}
{#END}

{#SUB Results}
{#VAR sub=tournamenthead}
{#VAR sub=rankingstyles file=inc.tournament.tpl}
<table border="0" cellspacing="0" cellpadding="4">
<tr>
  <td width="33%">&nbsp;</td><td align="center">{#IF empty($isFirst)}{$first}{#ELSE}<strong>{$first}</strong>{#END}</td><td width="33%">&nbsp;</td>
</tr>
<tr>
  <td align="center">{#IF empty($isSecond)}{#IF empty($second)}&nbsp;{#ELSE}{$second}{#END}{#ELSE}<strong>{$second}</strong>{#END}</td><td align="center" class="podest">1</td><td align="center">{#IF empty($isThird)}{#IF empty($third)}&nbsp;{#ELSE}{$third}{#END}{#ELSE}<strong>{$third}</strong>{#END}</td>
</tr>
<tr>
  <td align="center" class="podest">2</td><td align="center" class="podest">&nbsp;</td><td align="center" class="podest">3</td>
</tr>
</table>
<br />
<table border="0" cellspacing="0" cellpadding="4">
  {#FOREACH $rankings}
  <tr>
    <td align="right">{$rank}.</td>
    <td>{#IF empty($isInTeam)}{$name}{#ELSE}<strong>{$name}</strong>{#END}</td>
  </tr>
  {#END}
</table>
{#END}

{#SUB getTeam}
{#IF isset($coins)}Dieses Turnier kostet <span class="important">{$tcoins} {$currency}</span>.<br />
{#VAR sub=Coins}
<br />{#END}
{#IF !$oldsingleplayer}
<strong>Neues</strong> Turnierteam:
{#FORM addTeam action=$action}
{#IF $newsingleplayer}
  <h3>{%nickname}</h3>
{#END}
  {#INPUT teamname $nametype $nickname allowempty=0}<br />
  {#IF !empty($ligaid)}
    {$liga}-ID ("none"=>noch keine ID):<br />
    {#INPUT ligaid string $ligaid}<br />
  {#END}
  {#SUBMIT Anmelden}
{#END}
{#END}
{#IF !$newsingleplayer}
{#FOREACH $oldteam}
<br />
<strong>Altes</strong> Turnierteam:
{#FORM fixsize action="$file?frame=$frame&amp;id=$tournamentid"}
{#IF $oldsingleplayer}
  {#FOREACH $teams $teamname $teamid}
  <h3>{%teamname}</h3>
  {#END}
{#END}
  {#INPUT teamid $idtype $teamid param=$teams allowempty=0}<br />
  {#IF !empty($newligaid)}
    {$liga}-ID ("none"=>noch keine ID):<br />
    {#INPUT ligaid string $ligaid}<br />
  {#END}
  {#SUBMIT Anmelden}
{#END}
{#END}
{#END}
<br />
{#END}

{#SUB editTeam}
<a href="{$file}?frame=overview&amp;id={$id}">zur&uuml;ck</a><br />
{#VAR file=inc.tournament.tpl sub=editteamstyles}
<table class="edittable">
<tr>
  <th class="thcell">Weitere Seiten:</th>
  <td class="tdcell">
  {#FOREACH $links}
    <a href="{$file}?frame={$frame}&amp;id={$id}&amp;newteamid={$newteamid}">{$text}</a><br />
  {#END}
  </td>
</tr>
{#IF !$singleplayer}
<tr>
  <th class="thcell">Team umbenennen:</th>
  <td class="tdcell">
  {#FORM}
    {#INPUT action hidden renTeam}
    {#INPUT teamname string $teamname}
    {#SUBMIT &auml;ndern shortcut=e}
  {#END}
  </td>
</tr>
{#END}
{#IF !empty($liga)}
<tr>
  <th class="thcell">{$liga}-ID &auml;ndern:</th>
  <td class="tdcell">
  {#FORM}
    {#INPUT action hidden changeliga}
      {$liga}-ID ("none"=>noch keine ID):<br />
      {#INPUT ligaid string $ligaid}<br />
    {#SUBMIT &auml;ndern shortcut=l}
  {#END}
  </td>
</tr>
{#END}
<tr>
  <th class="thcell">Turnier verlassen:</th>
  <td class="tdcell">
  {#FORM action="$file?frame=overview&amp;id=$tournamentid"}
    {#INPUT action hidden remTeam}
    {#INPUT id hidden $id}
    {#INPUT newteamid hidden $newteamid}
    {#SUBMIT "Team aus dem Turnier nehmen" shortcut=r}
  {#END}
  </td>
</tr>
{#IF $deleteable=="true"}
<tr>
  <th class="thcell">Team aufl&ouml;sen:</th>
  <td class="tdcell">
  {#FORM action="$file?frame=overview&amp;id=$tournamentid"}
    {#INPUT action hidden delteam}
    {#INPUT id hidden $id}
    {#INPUT newteamid hidden $newteamid}
    <input type="submit" class="button" style="background-color:#FFD0D0;border-color:red;" value="Team l&ouml;schen (aus dem System)"/>
  {#END}
  </td>
</tr>
{#END}
</table>
{#END}

{#SUB Playerlist}
{#TABLE $candidates maxrows=50}
  {#COL Nickname %nick}
  {#OPTION %submitval $action}
{#END}
{#END}

{#SUB Playerlist_bak}
<form method="post" action="{$file}?frame={$frame}&amp;id={$tournamentid}">
<select class="edit" name="playerid[]"{$select}>
  {#FOREACH $candidates $nick $id}
  <option value="{$id}">{%nick}</option>
  {#END}
</select>
<br />
<input type="hidden" name="action" value="{$action}">
<input class="button" type="submit" value="{$submitval}">
</form>
{#END}

{#SUB AddPlayer}
<a href="{$file}?frame=overview&amp;id={$tournamentid}">zur&uuml;ck zur Turnierseite</a><br />
Es k&ouml;nnen noch <strong>{$free}</strong> Spieler in diesem Team teilnehmen.<br />
<br />
{#IF !empty($invited)}
<h3>Eingeladen wurden:</h3>
{#FOREACH $invited index=$i}
{#IF $i>1}, {#END}{%nickname}{#IF ($i % 3 == 0)}<br />{#END}
{#END}
{#END}
{#IF !empty($join)}
<h3>Anfragen von:</h3>
{#TABLE $join}
	{#COL Nickname %nickname}
	{#OPTION hinzuf&uuml;gen addPlayer}
	{#OPTION l&ouml;schen reminvite}
{#END}
{#END}
<h3>Einladen:</h3>
Spieler aus Team<br />
{#FOREACH $teamlinks}
<a href="{$file}?frame=saveTeam&amp;id={$tournamentid}&amp;newteamid={$newteamid}&amp;teamid={$teamid}">{%teamname}</a><br />
{#END}
{#FOREACH $searchform}
{#FORM action="$file?frame=saveTeam&amp;id=$tournamentid&amp;newteamid=$newteamid&amp;teamid=$teamid"}
  {#INPUT search string $search}
  {#SUBMIT suchen}
{#END}
{#END}
{#VAR sub=Playerlist}
{#END}

{#SUB RemPlayer}
<a href="{$file}?frame=overview&amp;id={$tournamentid}">zur&uuml;ck zur Turnierseite</a><br />
{#VAR sub=Playerlist}
{#END}

{#SUB PointForm}
{#WITH $pointinput}
<div align="center">
{#IF !empty($servername)}<h3>Server: {%servername} ({$ip})</h3>{#END}
{#FORM addpoints action="$file?frame=tree&amp;id=$tournamentid"}
{#IF $type == 'dm'}
  {§Gewinner}: {#INPUT points1 dropdown $points1 param=$teams}<br/>
  {#INPUT points2 hidden 1}
{#ELSE}
<table rules="none" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td width="45%" align="center">{#IF $isadmin=="true"}{#INPUT team1 dropdown $team1id param=$combatants}{#ELSE}{$team1}<br/>{#IF $ready1}{§bereit seit} {#DATE $ready1}{#ELSE}{§nicht bereit}{#END}{#END}</td><td width="10%" align="center" rowspan="2">vs.</td><td width="45%" align="center">{#IF $isadmin=="true"}{#INPUT team2 dropdown $team2id param=$combatants}{#ELSE}{$team2}<br/>{#IF $ready2}{§bereit seit} {#DATE $ready2}{#ELSE}{§nicht bereit}{#END}{#END}</td>
  </tr>
  <tr>
    <td align="center">{#INPUT points1 integer $points1}</td><td align="center">{#INPUT points2 integer $points2}</td>
  </tr>
</table>
{#END}{!if $type != dm}
{#IF $cancomment=="true"}
  Kommentar: {#INPUT comment string $comment}<br/>
  <br/>
{#END}
  letzter Bearbeiter: {%editor}<br/>
  <br/>
{#IF $enabled1 || $enabled2}
<h2>Screenshot Upload</h2>
({%types})<br />
<table rules="none" border="0" cellspacing="0" cellpadding="3">
<tr><td>Screenshot1: {#INPUT screen1 file $screen1 enabled=$enabled1}{$screen1}</td></tr>
<tr><td>Screenshot2: {#INPUT screen2 file $screen2 enabled=$enabled2}{$screen2}</td></tr>
</table>
<br />
{#END}
{#INPUT matchid hidden $matchid allowempty=0}
{#SUBMIT Eintragen}<br />
<br />
<a href="{$file}?frame=tree&amp;id={$tournament_id}">zur&uuml;ck</a>
{#END}
{#FOREACH $admin}
<br /><br />
{#FORM confirmation="$confirm"}{!removed for icon uplod: action="$file?frame=$frame&amp;id=$tournament_id&amp;matchid=$id"}
{#INPUT  confirmation_uri hidden $confirm_uri}
{#INPUT action hidden $action}
{#INPUT matchid hidden $matchid}
{#SUBMIT $text}
{#END}
{#END}
</div>
{#END}
{#END}

{#SUB edittime}
{#FORM action="$file?frame=tree&amp;id=$tournamentid"}
{#INPUT action hidden settime}
{#INPUT matchid hidden $matchid}
Zeit: {#INPUT endtime date $endtime}<br />
{#INPUT nextround checkbox $nextround} nachfolgende Runden entsprechend &auml;ndern (Winner- und Loserbracket!)<br />
{#SUBMIT setzen}
{#END}
{#END}

{#SUB tree}
{#VAR sub=tournamenthead}
<br />
{#IF !$showgames}
	Bisher hat das Turnier noch nicht begonnen.<br />
{#ELSE}
	{#IF true}
		{#FORM jumpto}Teamname: {#INPUT teamname string $teamname}{#SUBMIT finden}{#END}
		{#FOREACH $teamids $id $name index=$i}<a href="#{$id}">{%name}</a>{#MID}, {#END}
		<h3><a href="#winner">Winnerbracket</a>{#IF !empty($lcoldef)} &middot; <a href="#loser">Loserbracket</a>{#END}</h3>
		{#WHEN "!empty($groups)" "<h3>"}
		
		{#FOREACH $groups}<a href="#{$title}">{$title}</a>{#MID}&middot;{#END}
		
		{#WHEN "!empty($groups)" "</h3>"}
		<br />
		{#VAR sub=treestyles file=$treetypestyles}
		
		<table border="0" cellspacing="0" cellpadding="0" align="center">
		{#WITH $coldef}
			<colgroup width="{$width}" span="{$span}" valign="middle"></colgroup>
			<tr>
			  <td colspan="{$span}" class="treehead" align="center" style="border:1px solid black;border-bottom:0px;">
			    <h2><a name="winner"></a>Winnerbracket</h2>
			  </td>
			</tr>
		{#END}
		<tr valign="middle" height="100%">
		  {#FOREACH $col index=$currentcol}
		  {#LOAD "($currentcol == 1) ? true : false" $isfirstcol}
		  {#LOAD "($currentcol == count($col)) ? true : false" $islastcol}
		    {#VAR sub=_atree}
		  {#END}
		</tr>
		</table>
	{#END}

	{#IF true}
		<br />
		{#IF !empty($lcoldef)}
			<table border="0" cellspacing="0" cellpadding="0" align="center">
			{#WITH $lcoldef}
			<colgroup width="{$width}" span="{$span}" valign="middle"></colgroup>
			<tr>
			  <td colspan="{$span}" class="treehead" align="center" style="border:1px solid black;border-bottom:0px;">
			    <h2><a name="loser"></a>Loserbracket</h2>
			  </td>
			</tr>
			{#END}
			<tr valign="middle" height="100%">
			  {#FOREACH $lcol index=$currentcol}
			  {#LOAD "($currentcol == 1) ? true : false" $isfirstcol}
			  {#LOAD "($currentcol == count($lcol)) ? true : false" $islastcol}
			    {#VAR sub=_atree}
			  {#END}
			</tr>
			</table>
			<br />
		{#END}{!if !empty(lcoldef)}
		{#FOREACH $groups}
			  <h2><a name="{$title}">{$title}</a></h2>
			  <h3>Tabelle</h3>
			  {#TABLE $ergebnis}
			    {#COL "Team" $name}
			    {#COL "Spiele" $matchcount}
			    {#COL "+" $won}
			    {#COL "=" $equal}
			    {#COL "-" $lost}
			    {#COL "Punkte" $points}
			    {#COL "Ergebnis" $pluspunkte}<div style="height:1em;position:relative;"><div style="text-align:right;width:2em;position:absolute;display:inline;">{$pluspunkte}</div><div style="left:2em;position:absolute;display:inline;">: {$minuspunkte}</div></div>
			  {#END}
			  <br />
			  <h3>Paarungen</h3>
			  <table class="table" cellpadding="0" cellspacing="1">
			  {#FOREACH $rounds index=$j}
				  <tr>
				    <td class="tdedit">{$j}. Runde</td>
				    {#FOREACH $matches}
				    <td class="tdcont">
				      <table width="100%" cellspacing="0" cellpadding="0" border="0">
				      <tr>
				        <td class="tdcont" width="40%"><span class="{$color1}">{#IF !empty($team1) && empty($points1) && empty($points2)}{#LOAD $ready1 $ready}{#LOAD $readylink1 $readylink}{#VAR sub=_ready file=$treetypefile}&nbsp;{#END}{$team1html}</span></td>
				        <td class="tdcont" width="20%" align="center">{$score}<br /><small class="comment">{$comment}</small></td>
				        <td class="tdcont" width="40%" align="right"><span class="{$color2}">{$team2html}{#IF !empty($team2) && empty($points1) && empty($points2)}{#LOAD $ready2 $ready}{#LOAD $readylink2 $readylink}&nbsp;{#VAR sub=_ready file=$treetypefile}{#END}</span></td>
				      </tr>
				      </table>
				    </td>
					{#END}
				</tr>
			{#END}
		</table>
		{#END}{!foreach groups}
		{#IF $toolate=="true"}* Die Spielzeit f&uuml;r diese Runde ist bereits abgelaufen: Ergebnismeldung bzw. R&uuml;cksprache halten beim zust&auml;ndigen Turnierorga.{#END}
	{#END}{!group}
{#END}{!if empty}
{#VAR sub=AdminLink}
{#END}

{#SUB showdescription}
  <style type="text/css">
    body {
      background-color: white;
      background-image: none;
      padding-top: 10px;
    }
  </style>
  <div style="border: 2px solid #d8d8d8; background-color: #f8f8f8;">
    {$description}
  </div>
{#END}

{! subs below this line should not be used standalone }

{#SUB _atree}
{#VAR sub=atree file=$treetypefile}
{#END}