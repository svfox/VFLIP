{#SUB header}
		<div class="row">
			<div class="col-12">
			<a href="seats.php?frame=blockgraph"{#WHEN "$frame=='blockgraph' or empty($frame)" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="far fa-file-image"></i>&nbsp;Grafisch</a> 
			<a href="seats.php?frame=blocklist"{#WHEN "$frame=='blocklist'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-list"></i>&nbsp;Liste</a>
			</div>
		</div>
{#END}

{#SUB seatadminheader}
	{#IF $showadmin}
		<div class="row">
			<div class="col-12">
					<div><b>Admin Sitzplatzverwaltung:</b></div>
						&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$block_id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; Block Daten editieren</a>
						&nbsp;<a href="seatsedit.php?frame=blocktools&amp;id={$block_id}" {#WHEN "$frame=='blocktools'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; Block Grafisch editieren</a>
						&nbsp;<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp;Bl&ouml;cke Bearbeiten</a> 
			</div>
		</div>
	{#END}
{#END}