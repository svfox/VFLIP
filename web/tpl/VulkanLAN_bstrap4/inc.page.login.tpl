{#SUB login}

{#IF $isloggedin}

<!--VulkanLAN neuer logout in der Hauptmenue leiste - edit naaux -->
<form class="form-inline my-2 my-lg-0 d-none d-lg-block" method="post" id="frm" action="" style="margin-right: 10px">
	<div class="form-control mr-sm-2">
		

		  <span cla	ss="label label-default" style="font-size: 1.2em;">
			 <strong>{%user}</strong>
			 {#IF $unread_messages}
					<a href="webmessage.php?frame=owner" class="badge badge-warning" style="font-size: 1em;">
					  <i class="far fa-envelope"></i>
					</a>
			{#END}
		  </span>
		  <input type="hidden" name="ident" value="Anonymous" />
		  <input type="hidden" name="password" value="x" />
	</div>
	  <button type="submit"  class="btn btn-primary my-2 my-sm-0" value="logout"> Logout &nbsp;
		<i class="fas fa-sign-out-alt"></i>
	  </button>
</form>

{#ELSE}

<!--VulkanLAN neuer login in der Hauptmenue leiste - edit naaux -->
<form class="form-inline my-2 my-lg-0 d-none d-lg-block" method="post" id="frm" action="" style="margin-right: 10px">
  <div class="form-row align-items-center">
      <div class="col-auto">
	<div class="input-group">
		<div class="input-group-prepend">
			<div class="input-group-text"><i class="far fa-user"></i></div>
		</div>
		<input type="text"
		  class="form-control"
		  name="ident"
		  title="Deine UserID, dein Nickname oder deine Email-Adresse"
		  placeholder="UserID, Nickname oder Email-Adresse"
		  onFocus="if(this.value=='Nickname casesensitive') this.value='';" {#WHEN "!empty($loginident) and ($loginident != \"Anonymous\")" "value=\"%loginident\" "}/>
	</div>
	<div class="input-group">
		<div class="input-group-prepend">
			<div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
		</div>
		<input type="password"
			id="password"
		  class="form-control"
		  name="password"
		  title="Passwort"
		  placeholder="Password"
		  onclick="this.value='';" />
	</div>
	</div>
   <div class="col-auto">
  <button type="submit" class="btn btn-success my-2 my-sm-3"><i class="fas fa-sign-in-alt"></i>&nbsp; Sign In </button>
  <!--<a href="text.php?name=user_account_trouble">
    <font color="#aaff00">
      <strong>
        <i class="fas fa-magic"></i>
        ?
        <i class="fas fa-user-md"></i>
      </strong>
    </font>
  </a>-->
  </div>
  </div>
</form>

<script type="text/javascript">
  var loginform = document.getElementById('frm');
  {#IF empty($loginident) or ($loginident == "Anonymous")}
    loginform.ident.value='Nickname casesensitive';	
    loginform.password.value='Password';
  {#ELSE}
    loginform.password.focus();
  {#END}
</script>

{#END}   

{#END}