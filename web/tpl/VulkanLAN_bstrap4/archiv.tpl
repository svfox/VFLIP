{#SUB default}
{#TPL $text}

{#IF $edit}
  <div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
	<div class="btn-group" aria-label="button group" role="group">
		<a href="archiv.php?frame=editevent" class="btn btn-secondary btn-sm" role="button"> <i class="fas fa-plus"></i>{§hinzuf&uuml;gen}</a>
	</div>
	<div class="btn-group" aria-label="button group" role="group">
		<a href="archiv.php?frame=savecurrent" class="btn btn-secondary btn-sm" role="button"> <i class="fas fa-plus"></i>{§aktuelle LAN hinzuf&uuml;gen}</a>
	</div>
  </div>
   <div style="clear:both">&nbsp;</div>
{#END}
<div class="card-body">
{#TABLE $events}
  {#COL Wann? $time_start}{#DATE $time_start d.m.y} - {#DATE $time_end d.m.y}
  {#COL Was?}<a href="archiv.php?frame=event&amp;id={$id}"><b>{$caption}</b></a>
  {#COL Wo? %location}
{#END}
</div>
{#END}

{!************************ Event anzeigen/bearbeiten ********************************}

{#SUB event}

  {#IF $edit}
	  <div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
	  <div><b>{§Verwaltung}:</b></div>
		<div class="btn-group" aria-label="button group" role="group">
			<a href="archiv.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>Zur&uuml;ck</a>  &nbsp;
		</div>
		<div class="btn-group" aria-label="button group" role="group">
			<a href="archiv.php?frame=editarticle&amp;event_id={$id} class="btn btn-secondary btn-sm" role="button"> <i class="fas fa-plus"></i> {§Artikel hinzuf&uuml;gen}</a> &nbsp;
		</div>
		<div class="btn-group" aria-label="button group" role="group">
			<a href="archiv.php?frame=addtree&amp;event_id={$id} class="btn btn-secondary btn-sm" role="button"> <i class="fas fa-plus"></i>{§Turnier hinzuf&uuml;gen}</a> &nbsp;
		</div>
		<div class="btn-group" aria-label="button group" role="group">
			<!-- /*start neu Archivierung aller Turnierdaten */ -->
			<a href="archiv.php?frame=addtournamentdata&amp;event_id={$id} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-plus"></i>{§Archivierung aller Turnierdaten}</a> &nbsp;
			<!-- /*end*/ -->
		</div>
		<div class="btn-group" aria-label="button group" role="group">
			<a href="archiv.php?frame=editevent&amp;id={$id} class="btn btn-secondary btn-sm" role="button"><i class="far fa-edit"></i>{§bearbeiten}</a> &nbsp;
		</div>
		<div class="btn-group" aria-label="button group" role="group">
			<a href="archiv.php?action=deleteevent&amp;id={$id}&amp;confirmation={#URL Soll das Ereignis incl. aller Artikel gel&ouml;scht werden? Content wird nicht gel&ouml;scht.}&amp;confirmation_uri=archiv.php class="btn btn-secondary btn-sm" role="button"><i class="far fa-minus-square"></i>{§l&ouml;schen}</a>
		</div>
	  </div>
	  <div style="clear:both">&nbsp;</div>
  {#END}

	<div class="card ">
			<div class="card-header">
				<div class="row">
					Zeitraum: {#DATE $time_start d.m.y} - {#DATE $time_end d.m.y}
				</div>
				<div class="row">
					Ort: {%location}
				</div>
			</div>
			
			<div class="card-body">
				<div class="row" align="center">
					<div class="col-sm-12">{#TPL $text}</div>
				</div>
			</div>
	</div>
	<div style="clear:both">&nbsp;</div>
	<div class="card ">
			{#FOREACH $groups $article $groupname}
			<div class="card-header">
				<h3 class="card-title">{%groupname}</h3>
			</div>
			{#FOREACH $article}
			
				<div class="row" align="left">
					<div class="col-6 col-sm-5 col-md-4 col-lg-3" align="right">{%caption}</div>
					<div class="col-2 col-sm-2 col-md-2 col-lg-2"><a href="{%url}" target="{%target}">view</a>{#IF $edit} / <i><a href="archiv.php?frame=editarticle&amp;event_id={$event}&amp;id={$id}">edit</a></i>{#END}</div>
					<div class="col-4 col-sm-5 col-md-6 col-lg-7">&nbsp;</div>
				</div>
			
			{#END}
			<div style="clear:both">&nbsp;</div>
			{#END}
	</div>

	<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
		<div class="btn-group" aria-label="button group" role="group">
			<a href="archiv.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>Zur&uuml;ck</a>
		</div>
	</div>
{#END}

{#SUB editevent}
{#FORM event}
<table>
  <tr><td align="right">Caption:</td><td>{#INPUT caption string $caption}</td></tr>
  <tr><td align="right">Ort:</td><td>{#INPUT location string $location}</td></tr>
  <tr><td align="right">Von:</td><td>{#INPUT time_start date $time_start}</td></tr>
  <tr><td align="right">Bis:</td><td>{#INPUT time_end date $time_end}</td></tr>
  <tr><td align="right">Teilnehmergruppe:</td><td>{#INPUT participant_group_id subjects $participant_group_id param=group}</td></tr>
  <tr><td align="right">Leserecht:</td><td>{#INPUT view_right rights $view_right}</td></tr>
  <tr><td align="right">Schreibrecht:</td><td>{#INPUT edit_right rights $edit_right}</td></tr>
  <tr><td align="right">Text:</td><td>{#INPUT text content $text allowempty=0}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
  <tr><td align="center" colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#BACKLINK Zur&uuml;ck}</td></tr>    
</table>
{#INPUT id hidden $id}
{#END}
{#END}

{#SUB savecurrent}
{#FORM current}
<table>
<tr>
  <th>Teilnehmer:</th><td>{#INPUT participant_group_id dropdown $participant_group_id param=$groups}</td>
</tr>
<tr>
  <th>Ort:</th><td>{#INPUT location string $location}</td>
</tr>
</table>
{#SUBMIT}<br/>
{#BACKLINK zur&uuml;ck}
{#END}
{#END}

{!************************ Artikel anzeigen/bearbeiten ********************************}

{#SUB article}
  <p>{#TPL $text}</p>  
  {#IF is_array($url)}<p>Quelle: <a href="{%url.url}" target="_blank">{%url.caption}</a></p>{#END}
  {#IF $edit}<p><i><a href="archiv.php?frame=editarticle&amp;event_id={$event_id}&amp;id={$id}">edit</a></i></p>{#END}
  <br />
  <br />
  <p align="center"><a href="archiv.php?frame=event&amp;id={$event_id}">Zur&uuml;ck</a></p>
{#END}

{#SUB editarticle}
{#FORM article}
<table width="70%">
  <tr><td colspan="2" align="center">
    {§Wenn kein Text angegeben wird, verweist der Link<br />aus der Artikel&uuml;bersicht direkt auf die angegebene URL.}<br />
	<br />
  </td></tr>
  <tr><td align="right">Titel:</td><td>{#INPUT caption string $caption}</td></tr>
  <tr><td align="right">Gruppe:</td><td>{#INPUT group dropdownedit $group param=$groups}</td></tr>
  <tr><td align="right">URL:</td><td>{#INPUT url longstring $url}</td></tr>
  <tr><td align="right">Text:</td><td>{#INPUT text content $text}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td align="center" colspan="2">{#BACKLINK Zur&uuml;ck}</td></tr>    
</table>
{#INPUT id hidden $id}
{#INPUT event_id hidden $event_id}
{#END}
{#END}

{!****************************** Turnierb&auml;ume ******************************************}

{#SUB addtree}
	{#FORM tournament}
		{#BACKLINK zur&uuml;ck}<br/>
		Turnier: {#INPUT tournamentid dropdown $tournamentid param=$tournaments}<br/>
		{#INPUT event_id hidden $event_id}
		{#SUBMIT}
	{#END}
{#END}

{!****************************** Teilnehmer anzeigen ***********************************}

{#SUB participants}
  <table>
  <tr><td><b>Teilnehmer ({$count})</b><br /><br /></td></tr>
  {#FOREACH $users $u}
  <tr><td>{%u}</td></tr>
  {#END}
  </table>
{#END}



