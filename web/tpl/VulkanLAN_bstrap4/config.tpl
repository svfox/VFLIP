{#SUB default_alt}
{#TABLE $items}
  <b><a href="config.php?frame=edit">add</a></b>
  {#COL Key %key}
  {#COL Value %value}
  {#COL Beschreibung %description}
  {#OPTION L&ouml;schen delete}  
{#END}
{#END}


{#SUB default}
  {#IF !empty($items)}
	<div class="wide_topdiv">
	<div align="left" style="width:70%">
	<p align="justify">
	  Es gibt drei verschiedene Orte, an denen Konfigurationen vorgenommen werden k&ouml;nnen:<br />
	  <ul>
	    <li>
	      <b>/core/flipconfig.php</b><br />
	      Dies ist die zentrale Konfigurationsdatei. Es besteht kein Webinterface, um sie zu bearbeiten. 
	      Der Zugriff muss also &uuml;ber das Dateisystem erfolgen. In ihr finden sich beispielsweise die Einstellungen
	      f&uuml;r die Mysql-Datenbankanbindung, den Cache und die Logfiles.
	    </li>
	    <li>
	      <b>admin -> config</b><br />
	      Genau, hier an diesem Ort. Diese Einstellungen werden in der Datenbank gespeichert. Sie gelten global
	      f&uuml;r alle User, alle Templates usw. Bestimmt Einstellungen (die <span class="important">rot</span> gekennzeichneten)
	      k&ouml;nnen von den Usern in ihrem Benutzerprofil angepasst werden. Voraussetzung daf&uuml;r ist, dass der 
	      Konfigurationseintrag und die User-Property ein und denselben Key-Namen haben.
	    </li>
	    <li>
	      <b>das Benutzerprofil</b><br />
	      Dort kann der User f&uuml;r ihn pers&ouml;nlich geltende Einstellungen vornehmen. Beispielsweise, wie viele Posts im Forum
	      auf einer Seite angezeigt werden sollen.
	    </li>
	  </ul>
	</p></div></div>
	<table class="wide_table" cellspacing="1" cellpadding="1">
	<tr>
	  <th class="tdedit">Key</th>
	  <th class="tdedit">Value</th>
	  <th class="tdedit" colspan="2"></th>
	</tr>
	 <tr>
		<td class="tdcont" colspan="3" align="center"><br /></td>
		<td class="tdedit"><a href="config.php?frame=edit" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a></td>
	 </tr>
	{#FORM config action="config.php$rparams"}
	  {#FOREACH $items}
	    {#IF !empty($description)}
	      <tr><td class="tdcont" colspan="4" style="padding-top:12px;">{%description}</td></tr>
	    {#END}
	    <tr>
	      <td class="tdcont" align="right"><a name="{%modul}">{! <--TODO nur beim ersten eintrag von Modul den Anker setzen}<a name="{%key}"></a><b{#WHEN $usercfg " class=\"important\""}>{%key}:</b></td>
	      <td class="tdaction" align="left">
	        {#IF $editable}
	          {#IF empty($values)}
	            {#INPUT $key $type $value}
	          {#ELSE}
	            {#INPUT $key dropdown $value param=$values}
	          {#END}
	        {#ELSE}
	          <div class="edit">{%value}</div>
	        {#END}
	        {#IF ($default_value != $value) and (!empty($default_value))}<i>&nbsp;&nbsp;{%default_value}</i>{#END}
	      </td>
	      <td class="tdedit">{#IF $editable}<a href="config.php?frame=edit&amp;id={$id}" class="btn btn-secondary btn-md" role="button">edit</a>{#END}</td>
	      <td class="tdedit">{#IF $editable}<a href="config.php?action=delete&amp;id={$id}&amp;confirmation={#URL Soll der Config-Eintrag wirklich gel&ouml;scht werden?}" class="btn btn-danger ml-1" role="button"><i class="far fa-minus-square"></i>&nbsp;del</a>{#END}</td>
	    </tr>
	  {#END}
	  <tr>
	    <td class="tdcont" colspan="3" align="center"><br /><br />{#SUBMIT Speichern}<br /><br /></td>
	    <td class="tdedit"><a href="config.php?frame=edit" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a></td>
	  </tr>
	{#END}
	</table>
{#ELSE}
<b>Zur angegebenen Kategorie gibt es keine Config-Eintr&auml;ge!</b>
<br />
<br />
Alle vorhandenen Kategorien findest du <a href="config.php?frame=categories">hier</a>!
{#END}
{#END}

{#SUB categories}
An dieser Stelle sind alle Config-Kategorien zu sehen, die es gibt.<br />
Um die Variablen einer Kategorie zu betrachten/zu bearbeiten, einfach auf den Kategoriennamen klicken!<br/>
<br />
  {#TABLE $categories}
   {#COL Name $custom_name}<a href="config.php?category={$name}">{#IF empty($custom_name)}{$name}{#ELSE}{$custom_name}{#END}</a>
   {#COL Beschreibung $description}{#IF empty($description)}<i>Keine Beschreibung vorhanden!</i>{#ELSE}{$description}{#END}
   {#FRAME edit "editcategory&name=$name" right=$edit_right}
  {#END}
{#END}

{#SUB editcategory}
	{#FORM editcategory}
		{#INPUT id hidden $id}
		{#INPUT name hidden $name}
		  <table>
		   <tr>
			<td>Name:</td>
			<td>{#INPUT custom_name string $custom_name}</td>
		   </tr>
		   <tr>
			<td>Beschreibung:</td>
			<td>{#INPUT description text $description}</td>
		   </tr>
		   <tr align="center">
			<td colspan="2"><br /><br />{#SUBMIT Speichern}</td>
		   </tr>
		   <tr align="center">
			<td colspan="2"><br /><br />{#BACKLINK zur&uuml;ck}</td>
		   </tr>
		  </table>
	{#END}
{#END}

{#SUB edit}
<div class="card">
	{#FORM item}
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
				<a href="config.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-reply"></i>&nbsp;{§zur&uuml;ck}</a>
				{#SUBMIT Speichern}
			</div>
		</div>
	</div>
	<form>
	<div class="card-body">
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Key:</label>
				<div class="col-sm-10">
					{#INPUT key string $key}  <i>(Uniqe Database Key)</i>
				</div>
			</div>
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Typ:</label>
				<div class="col-sm-10">
					{#INPUT type inputtypes $type}  <i>(Typ u. Parameter)</i>
				</div>
			</div>
				  
		{#IF empty($values)}
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Value:</label>
				<div class="col-sm-10">
					{#INPUT value $type $value}  <i>(Eintrag)</i>
				</div>
			</div>
		{#ELSE}
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Values:</label>
				<div class="col-sm-10">
					{#FOREACH $values $value}
						{#INPUT values[] longstring $value}
						{#INPUT oldvalues[] hidden $value}<br />
					{#END}  
					<i>(Einträge)</i>
				</div>
			</div>
		{#END}
			<div class="form-group row">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
				<a href="config.php?frame=edit&amp;id={$id}&amp;newfield=1" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;einen weiteren Wert (Value) hinzuf&uuml;gen</a></td></tr>
				</div>
			</div>
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Default:</label>
				<div class="col-sm-10">
					{#INPUT default_value $type $default_value $values}  <i>(Standardwert. Wird verwendet wenn kein Eintrag "Value" vorhanden ist.)</i>
				</div>
			</div>
			
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Beschreibung:</label>
				<div class="col-sm-10">
					{#INPUT description text $description}  <i>(Beschreibung)</i>
				</div>
			</div>	
	</div>	
	<div class="card-footer">
		<a href="config.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-reply"></i>&nbsp;{§zur&uuml;ck}</a>
		{#SUBMIT Speichern}
		{#INPUT id hidden $id}
	</div>
	</form>
	{#END}
</div>
{#END}
