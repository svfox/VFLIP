{! $Id$ }

{#SUB default}

	<!-- Suchformular -->
	<div class="card ">
		<div class="card-header">	
			<div class="row">
				<div class="col-12">
				</div>
			</div>
		</div>
		<div class="card-body">
			{#FORM method=get action=search.php?frame=result}
			<div class="row">
				<div class="col-6">
					
						{#IF isset($nextframelink)}
						{#INPUT nextframelink hidden $nextframelink}
						{#END}
					<div class="input-group">
						<span class="input-group-addon"><i class="fas fa-search"></i></span>
						
						{#INPUT searchstring string $searchstring "type=\"text\" class=\"form-control\" placeholder=\"Suchtext\""}
						<div class="input-group-btn">
						<button class="btn btn-secondary" type="submit"><i class="fas fa-search"></i></button>
						</div>
					</div>
				</div>
				<div class="col-5">
				Modus:
				{#INPUT mode dropdown $mode param=$modes}
				</div>
				<div class="col-1"></div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="form-check">
						<label class="form-check-inline">{#INPUT titleonly checkbox $titleonly} Nur Titel durchsuchen</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					Module:
				</div>
				<div class="col-12">
					<div class="form-check">
						{#FOREACH $modules}
						<label class="form-check-inline">{#INPUT usedmodules[$name] checkbox $checked}{$name}</label>
						{#END}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
				{#SUBMIT Suchen}
				</div>
			</div>
			{#END}
		</div>
	</div>
		<!-- Ergebnisse -->
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col-12">
					<h3>Suchergebnisse</h3>
				</div>
				<div class="col-12">
					<u>{$count} Eintr&auml;ge in {$time} Sekunden gefunden</u>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				{#IF $usenextlink}
				<div class="col-12">
				<h3 style="color: #008800;">Klicke auf den Titel (Usernamen) um fortzufahren!</h3>
				</div>
				{#END}
				<div class="col-12">
				{#TABLE $results maxrows=50 class="table table-sm table-hover table-striped"}
				  {#COL Titel %title}<a href="{$link}"{#WHEN $link_new " target=_blank"}>{%title}</a>
				  {#COL Text $text}
				  {#COL Modul $mod}
				{#END}
				</div>
			</div>
		</div>
	</div>

{#END}