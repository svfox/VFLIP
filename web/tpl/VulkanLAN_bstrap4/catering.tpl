{! $Id: catering.tpl 1337 2006-12-19 13:22:07Z loom $ }

{#SUB adminlink}

<br />
<a href="catering.php?frame=cashdesk">Kasse</a><br />
<a href="catering.php?frame=doku">Dokumentation</a><br />
<a href="catering.php?frame=ordermanager">Bestellverwaltung</a><br />
<a href="catering.php?frame=showgroups">Angebot bearbeiten</a><br />
<a href="catering.php?frame=showstock">Lagerbestand einsehen</a><br />

{#END}

{#SUB backlink}

		<a href="catering.php" class="btn btn-secondary btn-sm">&larr; zur &Uuml;bersicht</a><br />
		<a href="catering.php?frame=showgroups" class="btn btn-secondary btn-sm">&larr; zur Kategorienverwaltung</a><br />
	
{#END}

{#SUB default}
{#TPL $text}
<br />
{#IF $logged_in=="true"}<strong><a href="catering.php?frame=korb">Warenkorb</a> &middot; <a href="catering.php?frame=mybill">meine Bestellungen</a></strong>{#END}
<h2>Kategorie{#WHEN "$count>1" n}</h2>
<div align=center>
<table border="0">
{#FOREACH $kategorien}
<tr>
<td>
  {#IF !empty($image_id)}<img src="image.php?name={$image_id}" alt="Bild{$image_id}" />{#ELSE}&nbsp;{#END}
</td><td>
<ul>
  <li>
  {#IF $aktiv=="1"}
  <a href="catering.php?frame=list&amp;kategorie={$id}"><h3>{%name}</h3></a>{#IF !empty($bestellzeit)}<small> (n&auml;chste Bestellung {%bestellzeit})</small>{#END}
  {#ELSE}
  <h3 class="stat_gray">{%name}</h3>{#IF !empty($bestellzeit)}<small> (zur Zeit nicht verf&uuml;gbar)</small>{#END}
  {#END}
  {%beschreibung}
  </li>
</ul>
</td>
</tr>{#END}
</table></div>
{#IF !empty($admin)}
<div class="textfooter" align="right">
Admin<br />
{#VAR sub=adminlink}
</div>
{#END}
{#END}

{#SUB list}
{#IF !empty($image_id)}<img src="image.php?name={$image_id}" alt="Bild{$image_id}" /><br />{#END}
{%beschreibung}<br />
<br />
{#VAR sub=backlink}
{#IF $logged_in=="true"}<br />
<strong><a href="catering.php?frame=korb">Warenkorb</a> &middot; <a href="catering.php?frame=mybill">meine Bestellungen</a></strong>
{#END}<br />
<br />
Es befinde{#WHEN "$warenkorbcount!=1" n t} sich <strong>{$warenkorbcount} Artikel</strong> in deinem Warenkorb.<br />
{#IF $warenkorbcount>0}Um deine Bestellung abzuschicken musst du deinen <a href="catering.php?frame=korb">Warenkorb</a> &uuml;berpr&uuml;fen und abschicken.{#END}
<h3>
{#FOREACH $kategorien}
<a href="catering.php?frame=list&amp;kategorie={$id}">{%name}</a>
{#MID} &middot; 
{#END}
</h3>
{#TABLE $artikel}
  {#COL Titel $titel}
  {#COL Beschreibung $beschreibung}
  {#COL Preis $preis align=right}{#NUMBER $preis prec=2}&nbsp;&euro;
  {#COL "Von dir bestellt" $myorder align=right}
  {#COL bestellen}{#IF $logged_in=="true"}{#IF $aktiv=="1"}{#FORM bestellung keepvals=0}{#INPUT id hidden $id}{#INPUT anzahl integer}x&nbsp;&nbsp;{#SUBMIT "in den Warenkorb"}{#END}{#ELSE}<span class="stat_gray">deaktiviert</span>{#END}{#ELSE}keine Berechtigung{#END}
{#END}
{#END}

{#SUB korb}
{#VAR sub=backlink}
<br />
<strong><a href="catering.php?frame=korb">Warenkorb</a> &middot; <a href="catering.php?frame=mybill">meine Bestellungen</a></strong><br />
<br />
  {#TABLE $bestellungen}
  <strong>Summe:&nbsp;{#NUMBER $sum 2}&nbsp;&euro;</strong>
    {#COL Titel %titel}
    {#COL Anzahl $anzahl}
    {#COL Preis $preis align=right}{#NUMBER $preis 2}&nbsp;&euro;
    {#OPTION entfernen RemFromWarenkorb}
  {#END}
  <div align="right">
  {#ACTION "Bestellung abschicken" status_bestellen buttonattrs="array(style=background-color:#66FF66;)"}
  </div>
{#END}

{#SUB mybill}
{#VAR sub=backlink}
<br />
<strong><a href="catering.php?frame=korb">Warenkorb</a> &middot; <a href="catering.php?frame=mybill">meine Bestellungen</a></strong><br />
<h3>
{#FOREACH $statuses index=$i var=$stat}
<a href="catering.php?frame=mybill&amp;id={$id}&amp;status={%stat}">{%stat}</a>
{#MID} &middot; 
{#END}
</h3>
Noch zu zahlen: <strong>{#NUMBER $sum 2} &euro;</strong> (Warteliste + bestellt)<br />
Sitzplatz: <a href="seats.php?frame=search&amp;ids[]={$id}">{$seatname}</a>
{#FOREACH $status}
<h2>Status: <a name="{%status}">{%status}</a></h2>
  {#VAR sub=Artikelliste}
{#END}
{#IF $admin=="true"}
{#VAR sub=adminlink}<br />
{#END}
{#END}

{#SUB showgroups}
{#TPL $text}<br />
{#TABLE $kategorien}
	<a href="catering.php?frame=editgroup">add</a>
  {#COL Kategorie %name}
  {#COL "# Artikel" $count align=right}
  {#COL Aktiv $aktiv align=center}{#IF $aktiv=="1"}aktiv{#ELSE}<span class="stat_gray">inaktiv</span>{#END}
  {#COL Sichtbar $sichtbar align=center}{#IF $sichtbar=="1"}sichtbar{#ELSE}<span class="stat_gray">versteckt</span>{#END}
  {#COL Warten $warteliste align=center}
  {#FRAME "Artikel bearbeiten" link="catering.php?frame=showitems&amp;catid=$id"}
  {#FRAME bearbeiten link="catering.php?frame=editgroup&amp;id=$id"}
  {#OPTION "de-/aktivieren" kataktiv}
  {#OPTION "un-/sichtbar" katsichtbar}
  {#OPTION l&ouml;schen delgroup}
{#END}<br />
{#VAR sub=backlink}
{#END}

{#SUB editgroup}
	{#FORM editgroup}
	<table border="0" cellspacing="2" cellpadding="2">
	<tr>
	  <td valign="top">Name:</td>
	  <td valign="top">{#INPUT name string $name}</td>
	</tr><tr>
	  <td valign="top">n&auml;chste Bestellung:</td>
	  <td valign="top">{#INPUT bestellzeit string $bestellzeit}</td>
	</tr><tr>
	  <td valign="top">Aktiv:</td>
	  <td valign="top">{#INPUT aktiv checkbox $aktiv}</td>
	</tr><tr>
	  <td valign="top">Sichtbar:</td>
	  <td valign="top">{#INPUT sichtbar checkbox $sichtbar}</td>
	</tr><tr>
	  <td valign="top">Warteliste:</td>
	  <td valign="top">{#INPUT warteliste checkbox $warteliste} (zum Drucken ben&ouml;tigt! -> "../data/tools/catering-server/")</td>
	</tr><tr>
	  <td valign="top">Bild:<br />(<a href="content.php">Admin->Content</a>)</td>
	  <td valign="top">{#INPUT image_id content $image_id param=images}</td>
	</tr><tr>
	  <td valign="top">Beschreibung:</td>
	  <td valign="top">{#INPUT beschreibung text $beschreibung}</td>
	</tr>
	</table>
	{#INPUT id hidden $id}
	{#SUBMIT Speichern}
	{#END}
{#END}

{#SUB showitems}
{#TPL $text}
<h3>Aktuelle Kategorie: {$category}</h3>
{#TABLE $artikel}
  <a href="catering.php?frame=edititem&amp;catid={$catid}">add</a>
  {#COL Titel $titel}
  {#COL Beschreibung $beschreibung}
  {#COL Aktiv $aktiv align=center}{#IF $aktiv=="1"}aktiv{#ELSE}<span class="stat_gray">inaktiv</span>{#END}
  {#COL Sichtbar $sichtbar align=center}{#IF $sichtbar=="1"}sichtbar{#ELSE}<span class="stat_gray">versteckt</span>{#END}
  {#COL Preis $preis align=right}{#NUMBER $preis 2}&nbsp;&euro;
  {#COL Lagermenge $amount align=center}
  {#FRAME bearbeiten link="catering.php?frame=edititem&amp;id=$id&amp;catid=$catid"}
  {#OPTION "de-/aktivieren" aktivartikel}
  {#OPTION "un-/sichtbar" sichtbarartikel}
  {#OPTION l&ouml;schen delartikel}
{#END}
<br />
{#VAR sub=backlink}
{#END}

{#SUB edititem}
	{#FORM edititem keepvals=0}
		<table border="0">
		<tr><td>Titel:</td><td>{#INPUT titel string $titel}</td></tr>
		<tr><td>Beschreibung:<br />(HTML)</td><td>{#INPUT beschreibung text $beschreibung}</td></tr>
		<tr><td>Preis:</td><td>{#INPUT preis decimal $preis}</td></tr>
		<tr><td>Lagermenge:</td><td>{#INPUT amount integer $amount}</td></tr>
		<tr><td valign="top">Aktiv:</td><td valign="top">{#INPUT aktiv checkbox $aktiv}</td></tr>
		<tr><td valign="top">Sichtbar:</td><td valign="top">{#INPUT sichtbar checkbox $sichtbar}</td></tr>
		</table>
		{#INPUT catid hidden $catid}
		{#INPUT id hidden $id}
		{#SUBMIT Speichern}<br /><br />
		{#BACKLINK Zur&uuml;ck}
	{#END}
{#END}

{#SUB verwaltung_kopf}
{#TPL $text}
<br />
{#VAR sub=backlink}<br />
{#FORM}
	{#INPUT id dropdown $kategorie $kategorien}&nbsp;&middot;&nbsp;
	{#INPUT Warteliste checkbox $warteliste} Warteliste&nbsp;
	{#INPUT Bestellt checkbox $bestellt} bestellt&nbsp;
	{#INPUT Bezahlt checkbox $bezahlt} bezahlt&nbsp;
	{#INPUT Geliefert checkbox $geliefert} geliefert&nbsp;
	{#INPUT Abgeschlossen checkbox $abgeschlossen} weitergeleitet&nbsp;
	{#SUBMIT "Filtern"}
{#END}
<h3>
	Gruppierkriterium: 
	{#IF $frame=="ordermanager"}
		<span style="color: #FF0000;">User</span>&nbsp;<a href="catering.php?frame=itemmanager">Artikel</a>
	{#ELSE}
		<a href="catering.php?frame=ordermanager">User</a>&nbsp;<span style="color: #FF0000;">Artikel</span>
	{#END}
</h3>
<br />
{#END}

{#SUB ordermanager}
{#VAR sub=verwaltung_kopf}
<style type="text/css">
.warteliste {color:#bb880b;}
.bestellt {color:#bb0000;}
.bezahlt {color:#007700;}
.geliefert {color:#0000bb;}
</style>
{#TABLE $bestellungen maxrows=30 style="width: 70%"}
	<strong>Summe aller Einnahmen: {$total} &euro;</strong>
	{#COL Status %status}<span class="{%status}">{%status}</span>
	{#COL User %nickname}<a href="catering.php?frame=mybill&amp;id={$subject_id}&amp;status={$status}"><strong>{%nickname}</strong></a>
	{#COL User-ID $subject_id}
	{#COL "Anzahl" $count align=center}
	{#COL Preis $preis align=right}<strong><span class="{%status}">{#NUMBER $preis 2}&nbsp;&euro;</span></strong>
{#END}
<br />
{#IF !$printing}<a href="catering.php?frame=viewlist&amp;id={$id}&amp;status={$print}" target="_blank">Druckansicht</a>{#END}
{#END}

{#SUB itemmanager}
{#VAR sub=verwaltung_kopf}
{#VAR sub=Artikelliste}
<br />
{#IF !$printing}<a href="catering.php?frame=printlist&amp;id={$id}&amp;status={$print}">Druckansicht</a>{#END}
{#END}

{#SUB Artikelliste}
{#IF $printing}<h1>{%kategorie} Bestellungen</h1>Status: {%statustext}{#END}
{#TABLE $bestellungen style="width: 70%"}
  <strong>Summe:&nbsp;{#NUMBER $sum 2}&nbsp;&euro;</strong>
  {#COL Kategorie $name}
  {#COL Artikel %titel}
  {#COL Anzahl $anzahl}
  {#COL Preis $preis align=right}{#NUMBER $preis 2}&nbsp;&euro;
  {#OPTION weitergeleitet artikelweitergeleitet condition="$thisframe==\"Artikelverwaltung\""}
  {#OPTION geliefert artikelgeliefert condition="$thisframe==\"Artikelverwaltung\""}
  {#OPTION %newstatus status_$newstatus right=$nextright condition="$thisframe==\"mybill\""}
  {#OPTION l&ouml;schen delbestellung right=$delright condition="($thisframe==\"mybill\")"}
{#END}
{#END}

{#SUB viewlist}
{! Begin HEAD}
{! copy of inc.page.tpl 2005-09-29 Loom }
<<?php echo"?"?>xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE
 html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
 "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
  <title>{%title} - {%caption}</title>
  <meta http-equiv="refresh" content="15;"/>
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
  <link rel="shortcut icon" href="{#STATICFILE favicon.ico}" />
  <style type="text/css">
    ul { list-style-image: url({#STATICFILE images/dot2.png}); }
  </style>
</head>
<body>
{! End HEAD}
<h1>Alle Bestellungen</h1>
	Status: {%statustext}
{#TABLE $bestellungen}
  {#COL Anzahl $anzahl align=right}
  {#COL Artikel %titel}
  {#COL User $name style="padding-left:12px;"}
  {#COL Sitzplatz $seat}
  {#COL Preis $preis align=right}{#NUMBER $preis 2}&nbsp;&euro;
  {#OPTION weitergeleitet artikelweitergeleitet condition="$thisframe==\"Artikelverwaltung\""}
  {#OPTION geliefert artikelgeliefert condition="$thisframe==\"Artikelverwaltung\""}
  {#OPTION %newstatus status_$newstatus right=$nextright condition="$thisframe==\"mybill\""}
  {#OPTION l&ouml;schen delbestellung right=$delright condition="$thisframe==\"mybill\""}
  {#ACTION geliefert status_geliefert}
{#END}
{#END}

{#SUB doku}
<pre>
Das Catering wird folgenderma&szlig;en abgewickelt:
Kurzfassung:
Warenkorb->(evtl. Warteliste)->bestellt->bezahlt->geliefert(->extra: weitergeleitet)

Langfassung:
1. (orga) erstellt Kategorien nach denen das Angebot gegliedert werden soll (die Warteliste dient der Best&auml;tigung von Bestellungen)
2. (orga) tr&auml;gt f&uuml;r jede Kategorie Artikel ein
3. (user) w&auml;hlt eine Kategorie
4. (user) w&auml;hlt die Anzahl der Artikel die er bestellen m&ouml;chte,
          diese werden im Warenkorb abgelegt
5. (user) schickt den Warenkorb ab, nicht gew&uuml;nschte Artikel k&ouml;nnen vorher entfernt werden
6. (user/orga) Artikel werden bezahlt, Orga setzt Status auf bezahlt
7. (orga) setzt Status auf geliefert, wenn die Artikel den User erreicht haben.
</pre>
<br />
<a href="catering.php">zur Cateringstartseite</a>
{#END}

{#SUB showStock}
	<form action="catering.php" method="get">
		<input type="hidden" name="frame" value="showstock" />
		<input class="button" value="Reload page" type="submit" /> 
	</form>
	<br />
	{#TABLE $stockamount}
		{#COL Name $name}
		{#COL Restbestand $remaining_amount align=center}
	{#END}
{#END}

{#SUB cashDesk}
	<div style="width: 50%; border: 2px solid grey; padding: 5px">
		{#IF $current_user==0}
			<h3>Bitte den User-Barcode einscannen</h3>
			<form name="barcode" method="get">
				<input type="hidden" name="frame" value="cashdesk" />
				<input type="text" name="current_user" value="" /> 
			</form>
			<script>
	    		document.barcode.current_user.focus();
	    		document.barcode.current_user.select();
			</script>
			<br />
		{#ELSE}
			<h3>Angemeldet als User #{$current_user} ({$current_user_name})</h3>
			{#TABLE $current_user_items}
				{#COL "Name" $titel}
				{#COL "Menge" $anzahl}
				{#COL "Einzelpreis" $preis}{#NUMBER $preis 2}&nbsp;&euro;
				{#COL "Zwischensumme" $sub_summe}{#NUMBER $sub_summe 2}&nbsp;&euro;
			{#END}
			<br />
			<h3>Summe: {$summe} &euro;</h3>
		{#END}
	</div>
	<br />
	{#FOREACH array=$category_list}
		<h3>{$name}</h3>
		<table cellspacing="5" cellpadding="5" border="0">
			<tr>
				{#FOREACH $items index=$itemindex}
					{#IF ($itemindex % 5) == 0}
						<td>{#ACTION "$label Euro" cashdesk_additem buttonattrs="array(style=\"height:100px; width:100px\")" params="array(id=$id user=$current_user)"}</td>
						</tr>
						<tr>
					{#ELSE}
						<td>{#ACTION "$label Euro" cashdesk_additem buttonattrs="array(style=\"height:100px; width:100px\")" params="array(id=$id user=$current_user)"}</td>
					{#END}
				{#END}
			</tr>
		</table>
	{#END}
	<br />
	<div style="width: 50%; border: 2px solid grey; padding: 5px">
		<table>
			<tr>
				<td>{#ACTION "Cancel" cashdesk_cancel uri="catering.php?frame=cashdesk" buttonattrs="array(style=\"height:100px; width:150px\; background-color: #DD0000; font-weight: bold\")" params="array(user=$current_user)"}</td>
				<td>{#ACTION "Paid" cashdesk_checkout confirmation="Das macht dann $summe Euro" uri="catering.php?frame=cashdesk" buttonattrs="array(style=\"height:100px; width:150px\; background-color: #00FF00; font-weight: bold\")" params="array(user=$current_user state=bezahlt)"}</td>
				<td>{#ACTION "Delivered" cashdesk_checkout confirmation="Das macht dann $summe Euro" uri="catering.php?frame=cashdesk" buttonattrs="array(style=\"height:100px; width:150px\; background-color: #00AAFF; font-weight: bold\")" params="array(user=$current_user state=geliefert)"}</td>
			</tr>
		</table>
	</div>
{#END}
