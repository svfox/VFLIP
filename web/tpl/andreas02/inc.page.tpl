{#SUB head}<<?php echo"?"?>xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="author" content="ported by Loom  / Original design: Andreas Viklund - http://andreasviklund.com/" />
<link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
<link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
<link rel="stylesheet" type="text/css" href="{#STATICFILE andreas02.css}" media="screen" title="andreas02 (screen)" />
<link rel="stylesheet" type="text/css" href="{#STATICFILE print.css}" media="print" />
<link rel="alternate" type="application/rss+xml" title="RFD-Datei" href="news.php?frame=rss"/>
<link rel="alternate" type="application/atom+xml" title="Atom-Feed" href="news.php?frame=atom"/>
<style type="text/css">
input.logined {
	background-color : #F0F0F0;
	font-family : arial;
	font-size : 10px;
	border-style : solid;
	border-width : 1px;
	width : 64px;
	border-color : gray;
}

input.loginbtn {
	font-family : arial;
	font-size : 10px;
	border-width : 0px;
	background-color : silver;
}
</style>
<title>{%title} - {%caption}</title>
</head>
{#END}

{#SUB page}{#VAR sub=head}
<body>
<div id="toptabs">
<p>Site Network: 
<a class="activetoptab" href="">diese Seite</a><span class="hide"> | </span>
<a class="toptab" href="index.php">andere Seite</a>
</p>
</div>

<div id="container">
<div id="logo">
<h1><a href="index.php">{%title}</a></h1>
</div>

<div id="navitabs">
<h2 class="hide">Site menu:</h2>
{#LOAD MenuLoad() $level0 inc/inc.menu.php}
{#FOREACH $level0.menu}{#FOREACH $items}
<a class="{#WHEN $active activenavitab navitab}" href="{$link}" title="{%description}"{#WHEN "$use_new_wnd" " target=\"_blank\""}>{%caption}</a>
{#MID}<span class="hide"> | </span>{#END}
{#END}
</div>
	
<div id="desc">
<h2>{#VAR sub=login file=inc.page.login.tpl}</h2>
{!divs um die Fehlermeldung vor das Banner zu legen}
<div style="padding:10px;z-index=0;position:absolute;">{#VAR sub=banner file=inc.sponsor.tpl}</div>
<div style="z-index=10;position:absolute;"><!-- ERRORCELL --></div>
</div>
    
<div id="main">
{#WHEN "$caption != \"\"" <h1>%caption</h1> ""}
{#TPL $page}
</div>

<div id="sidebar">
{#LOAD MenuLoad($level0.activedir,1) $level1 inc/inc.menu.php}
{#IF !empty($level1.menu)}
  {#FOREACH $level1.menu}
    <h3>{#DBIMAGE $image_title}{%caption}</h3>
    <p>
      {#FOREACH $items}
        {#IF is_object($frame)}{#TPL $frame}
        {#ELSEIF !empty($text)}{$text}
        {#ELSE}{#DBIMAGE $image}<a class="sidelink" href="{$link}" title="{%description}"{#WHEN "$use_new_wnd" " target=\"_blank\""}>{%caption}</a>
        {#END}
      {#MID}<span class="hide"> | </span>{#END}
    </p>
  {#END}
{#END} 
</div>
    
<div id="footer">
{$copyright} | Design by <a href="http://andreasviklund.com">Andreas Viklund</a>.
</div>

</div>
</body>
</html>
{#END}

{#SUB errorcell}
{#IF count($errors) > 0}
<h2>{§Fehler}</h2>
<p class="errorcell">
{#FOREACH $errors $err}
- {$err}<br/>
{#END}
</p>
{#END}
{#IF count($notices) > 0}
<h2>{§Nachricht}</h2>
<p class="noticecell">
{#FOREACH $notices $msg}
- {$msg}<br/>
{#END}
</p>
{#END}
{#END}
