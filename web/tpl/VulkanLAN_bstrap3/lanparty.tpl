{! $Id: lanparty.tpl 1415 20015-01-29 13:56:00Z loom edit by naaux $ }

{#SUB default}
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-sm-12">
				<h3>Status</h3>
			</div>
		</div>
		{#IF $can_config}
		<div class="row">
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
				<a href="config.php#{$configlistgroupname}" class="btn btn-default" role="button">
				<i class="fa fa-group"></i>&nbsp;{§zus&auml;tzliche Gruppen in der Teilnehmerliste bearbeiten}</a>
			</div>
		</div>
		{#END}
	</div>
	<div class="panel-body">
		<div class="row"><div class="col-sm-12">
				{#IF !empty($check)}
				 {§Das Konto wurde zu letzt &uuml;berpr&uuml;ft am}: {#DATE $check}
				{#END}
		</div></div>
	</div>
	<div class="panel panel-warning">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-3" style="border-bottom:1px solid #ddd;"><b>{§Dein Status}:</b></div>
				<div class="col-sm-9" style="border-bottom:1px solid #ddd;"><b>{$statuscaption}</b></div>
				<div class="col-sm-3"><b>{§n&auml;chster Schritt}:</b></div>
				<div class="col-sm-9">{#TPL $status}</div>
			</div>
		</div>
	</div>
	
	{#VAR $bar statusbar textclass=lanparty_statusbar_normal}
	
</div>
<div class="panel panel-default">
	<div class="panel-heading">
	{#TPL $text}
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
			 {#FORM class="form-horizontal"}
				 <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i>&nbsp;{§Suche}:</span>
					 {#INPUT searchtext string $searchtext }
					</div>
			</div>
			 {#END}
			</form>
		</div>
		<div>&nbsp;</div>
		<div class="row">
			<div class="col-sm-12">
			{#IF $ViewInternalProfile}
				{#TABLE $user maxrows=200 class="table table-striped table-bordered table-hover dataTable no-footer" role="grid"}
					{#COL Name %name} {#IF $ViewInternalProfile}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a> {#ELSE}{%name}{#END}
					{#COL Vorname %givenname}{%givenname}
					{#COL Nachname %familyname}{%familyname}
					{#COL Clan %clan}{#IF empty($clan_url)}{%clan}{#ELSE}<a href="{%clan_url}" target="_blank">{%clan}</a>{#END}
					{#COL Status $status}{#TABLEDISPLAY lanparty_status $status}{#IF !empty($seat_id)} (<a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{%seat}</a>){#END}
					{#COL Age %age}{%age}
					{#COL PLZ %plz}{%plz}
					{#OPTION "auf bezahlt setzen" setpaid right=$setpaidright}
					{#OPTION "auf angemeldet setzen<br/>und reservierten Sitzplatz vormerken" unsetpaid right=$setpaidright}
				{#END}
			{#ELSE}
			 {#TABLE $user maxrows=100 class="table table-striped table-bordered table-hover dataTable no-footer" role="grid"}
				{#COL Name %name} {#IF $ViewUserProfile}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a> {#ELSE}{%name}{#END}
				{#COL Clan %clan}{#IF empty($clan_url)}{%clan}{#ELSE}<a href="{%clan_url}" target="_blank">{%clan}</a>{#END}
				{#COL Status $status}{#TABLEDISPLAY lanparty_status $status}{#IF !empty($seat_id)} (<a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{%seat}</a>){#END}
				{#OPTION "auf bezahlt setzen" setpaid right=$setpaidright}
				{#OPTION "auf angemeldet setzen<br/>und reservierten Sitzplatz vormerken" unsetpaid right=$setpaidright}
			  {#END}
			 {#END}
			  </div>
		</div>
	</div>
</div>
{#END}

{#SUB statusbar}
<!-- Menue smallstatusbar as Frame-->
<div class="panel panel-success">
  <div class="panel-heading">
	<h3 class="panel-title">Teilnehmer</h3>
	 {§maximal} {$max.count}
  </div>
	<div class="panel-body">
			<div class="progress" style="margin-bottom: 2px;">
			  <div class="progress-bar progress-bar-info" style="min-width: 1em; width: {$paid.width}%;">
				{$paid.count} <span class="sr-only">{§bezahlt}</span>
			  </div>
			  <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: {$reg.width}%;">
			   {#IF !$hidereg}{$reg.count} {§angemeldet}{#END}  <span class="sr-only">{$reg.count} {§angemeldet}</span>
			  </div>
			  <div class="progress-bar progress-bar-success" style="width: {$free.width}%;">
				{$free.count}<span class="sr-only">{$max.count} {§maximal}</span>
			  </div>
			</div>
			<div style="font-size:9px;">
				<div style="float: left;">
					{§Bezahlungen}
				</div>
				<div style="float: right;">
					{§freie Pl&auml;tze}
				</div>
			</div>
	</div>
</div>
{#END}

{#SUB smallstatusbar}
  {#VAR sub=statusbar hidereg=1 textclass=lanparty_statusbar_small}
{#END}

{#SUB smallstatusbartext}
<div align="{$align}">
<ul class="list-group">
  <li class="list-group-item"><span class="badge">{$max.count}</span>{§maximal}</li>
  <li class="list-group-item list-group-item-info"><span class="badge">{$paid.count}</span>{§bezahlt}</li>
  <li class="list-group-item list-group-item-warning"><span class="badge">{$reg.count}</span>{§angemeldet}</li>
  <li class="list-group-item list-group-item-success"><span class="badge">{$free.count}</span>{§freie Pl&auml;tze}</li>
</ul>
</div>
{#END}

{#SUB userstatus}
<div class="panel panel-default">
	<div class="panel-heading">
		<small>{§Status}:<b>{$statuscaption}</b></small>
	</div>
	<div class="panel-body">
		<small>{§n&auml;chster Schritt}:{#TPL $status}</small>
	</div>
</div>
{#END}

{#SUB register}
<div class="panel panel-default">
	<div class="panel-heading">
		Anmelden
	</div>
	<div class="panel-body">
			{#TPL $text}
	</div>
		{#FORM register}
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-1">{#INPUT lanparty_participate checkbox}</div>
				<div class="col-xs-11">{#TPL $agbtext}</div>
			</div>
		</div>
			{#IF count($clans) > 0}
				<div class="panel-footer">
					zus&auml;tzlich deinen Clan anmelden
				</div>
				<div class="panel-body">
				{#FOREACH $clans $name $id}
					<div class="checkbox">
					  <label>
					  {#INPUT clan_$id checkbox}&nbsp;{%name}
					  </label>
					</div>	
				{#END}
				</div>
			{#END}
	<div class="panel-body">
			{#SUBMIT $action}
		{#END}
	</div>
</div>
{#END}

{#SUB showpaid}
<div class="panel panel-default">
  <div class="panel-heading">
	<a href="lanparty.php?frame=paidloggraph&amp;x=1000&amp;y=500"><img src="lanparty.php?frame=paidloggraph&amp;x=600&amp;y=300" alt="Graph"/></a>
  </div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
			  {#TABLE $entrys class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" maxrows=100}
				{#COL Zeitpunkt %date}
				{#COL Username %user_name}
				{#COL Orga %orga_name}
			  {#END}
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align='left';>
				{#Action "Die Liste leeren" emptylog confirmation="Soll der Log wirklich geleert werden?"}
			</div>
		</div>	
	</div>	
</div>
{#END}

{#SUB sidebar}
<div class="panel panel-default">
  <div class="panel-heading">
	<h3 class="panel-title">{§Teilnehmer}&nbsp;<span class="badge">{$users_max}</span></h3>
  </div>
  <div class="panel-heading">
	<a href="lanparty.php?menudir=27-59" target="_blank">{§Anmeldungen}</a>
  </div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-3 col-sm-2 col-md-1">{§insgesamt}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="{$users_all}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_all}%;">{$users_all}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§angemeldet}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{$users_registered}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_registered}%;">{$users_registered}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§bezahlt}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$users_paid}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_paid}%;">{$users_paid}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§eingechecked}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$users_checked_in}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_checked_in}%;">{$users_checked_in}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§online}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$users_online}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_online}%;">{$users_online}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§offline}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$users_offline}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_offline}%;">{$users_offline}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§ausgechecked}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$users_checked_out}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_checked_out}%;">{$users_checked_out}</div>
				</div>
			</div>
		</div>
	</div>
  <div class="panel-heading">
	<a href="seats.php?menudir=27-61" target="_blank">{§Sitzplan}</a>
  </div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-3 col-sm-2 col-md-1">{§reserviert}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{$seats_taken}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$seats_taken}%;">{$seats_taken}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§vorgemerkt}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{$seats_marked}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$seats_marked}%;">{$seats_marked}</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-2 col-md-1">{§frei}</div>
			<div class="col-xs-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{$seats_free}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$seats_free}%;">{$seats_free}</div>
				</div>
			</div>
		</div>
	</div>
  <div class="panel-heading">
	{§So, und jetzt kommen die wichtigen Daten}
  </div>
	<div class="panel-body">
		{! textclass auf nicht bewirkt die nichtbenutzung der kleinen }
		{! Schriftart, welche fuers menu benutzt wird. }
		{#VAR sub=smallcounter textclass="nicht_" showdesc=1}
	</div>
</div>
{#END}

{#SUB smallcounter}
<div class="panel panel-success">
  <div class="panel-heading">
	<h3 class="panel-title">Status Counter</h3>
  </div>
	<div class="panel-body">
			<div class="panel panel-default">
				<div style="float: left;">
					{§Bezahlt}
				</div>
				<div style="float: right;">
					{$users_paid}
				</div>
			</div>
			<div class="panel panel-default">
				<div style="float: left;">
					{§Besucher}
				</div>
				<div style="float: right;">
					{$users_on_party}
				</div>
			</div>
			<div class="panel panel-default">
				<div style="float: left;">
					{§Abendkasse}
				</div>
				<div style="float: right;">
					{$abendkasse}
				</div>
			</div>
			<div class="progress">
			  <div class="progress-bar progress-bar-info" style="min-width: 2em; width:{$users_paid_width}%;">
			   {$users_paid} <span class="sr-only">{#IF $showdesc }{§Bezahlt}{#END}</span>
			  </div>
			  <div class="progress-bar progress-bar-warning progress-bar-striped" style="min-width: 2em; width:{$users_on_party_width}%;">
				{$users_on_party} <span class="sr-only"> {#IF $showdesc }{§Besucher}{#END}</span>
			  </div>
			  <div class="progress-bar progress-bar-danger progress-bar-striped" style="min-width: 2em; width:{$abendkasse_width}%;">
				{$abendkasse}<span class="sr-only">{#IF $showdesc }{§Abendkasse}{#END}</span>
			  </div>
			</div>
	</div>
</div>

{#END}

{#SUB countdown}
	{! es stehen auch zur Verf&uuml;gung: $days, $hours, $minutes, $seconds }
	<!-- Beispiel {%days} -->
	<div id="countdown" class="panel panel-info">
	{%msg}
	</div>
{#END}

{#SUB countdown_small}
	<div id="countdown" class="panel panel-info">
		<p> <br> &nbsp; &nbsp; {%msg} &nbsp; &nbsp;  <br>
		</p>
	</div>
{#END}

{#SUB smalluserstatus}
{#IF $seat == "done"}<div class="panel panel-success">
{#ELSE}<div class="panel panel-warning">
{#END}
  <div class="panel-heading">
	<h3 class="panel-title"><i class="fa fa-tasks fa-1x"></i>Checklist</h3>
  </div>
	<div class="list-group">
		<ul class="list-group">
		  {#IF $acc == "active"}
			<li class="list-group-item list-group-item-warning">1. <a href="user.php?frame=register" class="alert-link">{§Registrieren} <i class="fa fa-fw fa-plus"></i> </a></li>
		  {#ELSEIF $acc == "done"}
			<nobr><li class="list-group-item list-group-item-success">1. {§Page Account} <i class="fa fa-fw fa-check"></i></li></nobr>
		  {#END}
		  
		  {#IF $reg == "active"}
			<li class="list-group-item list-group-item-warning">2. <a href="lanparty.php?frame=register">{§Anmeldung} <i class="fa fa-fw fa-sign-in"></i></a></li>
		  {#ELSEIF $reg == "done"}
			<nobr><li class="list-group-item list-group-item-success">2. {§Anmeldung} <i class="fa fa-fw fa-check"></i></li></nobr>
		  {#ELSE}
			<li class="list-group-item">2. {§Anmeldung}</li>
		  {#END}
		  
		  {#IF $pay == "active"}
			<li class="list-group-item list-group-item-warning">3. <a href="banktransfer.php">{§&Uuml;berweisung} <i class="fa fa-fw fa-money"></i></a></li>
		  {#ELSEIF $pay == "done"}
			<nobr><li class="list-group-item list-group-item-success">3. {§&Uuml;berweisung} <i class="fa fa-fw fa-check"></i></li></nobr>
		  {#ELSE}
			<li class="list-group-item">3. {§&Uuml;berweisung}</li>
		  {#END}

		  {#IF $seat == "active"}
			<li class="list-group-item list-group-item-warning">4. <a href="seats.php">{§Sitzplatz} <i class="fa fa-fw fa-map-marker"></i></a></li>
		  {#ELSEIF $seat == "done"}
			<li class="list-group-item list-group-item-success">4. {§Sitzplatz}:<br />
			<nobr><a href="seats.php">{$seatname}</a><i class="fa fa-fw fa-check"></i></li></nobr>
		  {#ELSE}
			<li class="list-group-item">4. {§Sitzplatz}</li>
		  {#END}
		  </ul>
	</div>
</div>
{#END}

{#SUB kontocheck}
	{#IF !empty($check)}
		<div class="panel panel-default">
		  <div class="panel-heading">{§Letzter Kontocheck}:</div>
		  <div class="panel-body">
			{#DATE $check}
		  </div>
		</div>
	{#END} 
{#END}
