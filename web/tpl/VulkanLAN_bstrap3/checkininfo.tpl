{#SUB default}
{#IF $printing}
<style type="text/css">
td,b{
	font-family: Arial, Helvetica, sans-serif;
}
</style>
{#END}
<table width="650" align="center">
  {#IF $printing}
    <tr><td colspan="3"><h2 align="center">{$caption}</h2></td></tr>
  {#END}
  <tr><td colspan="3">{#TPL $text}</td></tr>
  <tr><td><hr /></td><td align="center"><b>{%givenname} {%familyname}</b></td><td><hr /></td></tr>
  <tr><td align="right" valign="top">
    <table>      
      <tr><td align="right">Nickname:</td><td><b>{%name}</b></td></tr>
      <tr><td align="right">Sitzplatz:</td><td><b>{$seat}</b></td></tr>
      {#IF $showip}<tr><td align="right">IP-Adresse:</td><td><b>{$ip}</b></td></tr>{#END}
      {#IF !empty($mask)}<tr><td align="right">SubnetMask:</td><td><b>{$mask}</b></td></tr>{#END}
      {#IF !empty($dns)}<tr><td align="right">DNS-Sever:</td><td><b>{$dns}</b></td></tr>{#END}
      {#IF !empty($wins)}<tr><td align="right">WINS-Sever:</td><td><b>{$wins}</b></td></tr>{#END}
      {#IF !empty($http)}<tr><td align="right">HTTP-Sever:</td><td><b>{$http}</b></td></tr>{#END}
      {#IF !empty($ftp)}<tr><td align="right">FTP-Sever:</td><td><b>{$ftp}</b></td></tr>{#END}
    </table>
  </td><td>
    &nbsp;
  </td><td valign="top" align="left">
    <table>      
      <tr><td colspan="2"><img src="barcode.php?code={$id}&amp;width=200&amp;height=100" height="100" width="200" alt="user-id:{$id}" /></td></tr>
      <tr><td align="right" width="100">ID:</td><td width="100"><b>{$id}</b></td></tr>
    </table>
  </td></tr>
  {#IF !$printing}
  <tr><td colspan="3" align="center">&nbsp;</td></tr>
  <tr><td colspan="3" align="center"><b><a href="checkininfo.php?frame=print&amp;id={$id}">Druckversion</a></b></td></tr>
  {#IF !empty($configurl)}<tr><td colspan="3" align="center"><b><a href="{$configurl}">{§vorgegebene Daten bearbeiten}</a></td></tr>{#END}
  {#END}
</table>
{#END}
{#SUB ausweis}
{#IF $printing}
<style type="text/css">
td,b{
	font-family: Arial, Helvetica, sans-serif;
}
</style>
{#END}
<table width="450" align="center">
  {#IF $printing}
    <tr><td colspan="3"><h2 align="center">Ausweis</h2></td></tr>
  {#END}
  <tr><td colspan="3"><hr /></td></tr>
  <tr><td align="left" valign="top">
		<table>
			<tr><td align="left" colspan="2"><b>{%givenname} {%familyname}</b></td>
			<tr><td align="left">Nickname:</td><td><b>{%name}</b></td></tr>
		</table>
	  </td>
	  <td align="center">
		<table>
			<tr><td >{#DBIMAGE $userimg process=createthumbnail(120,120)}</td></tr>
		</table>
	  </td>
	</tr>
 
  <tr><td align="left" valign="top">
    <table>
	  <tr><td align="left">BDay:</td><td><b>{#DATE $birthday}</b></td></tr>
      <tr><td align="left">Sitzplatz:</td><td><b>{$seat}</b></td></tr>
      {#IF $showip}<tr><td align="left">IP-Adresse:</td><td><b>{$ip}</b></td></tr>{#END}
      {#IF !empty($lantitle)}<tr><td align="left">Title:</td><td><b>{$lantitle}</b></td></tr>{#END}
      {#IF !empty($lanstartdate)}<tr><td align="left">Start:</td><td><b>{$lanstartdate}</b></td></tr>{#END}
      {#IF !empty($lanenddate)}<tr><td align="left">End:</td><td><b>{$lanenddate}</b></td></tr>{#END}
    </table>
  </td><td valign="top" align="left">
    <table>      
      <tr><td colspan="2"><img src="barcode.php?code={$id}&amp;width=200&amp;height=100" height="100" width="200" alt="user-id:{$id}" /></td></tr>
      <tr><td align="right" width="100">ID:</td><td width="100"><b>{$id}</b></td></tr>
    </table>
  </td></tr>
  {#IF !$printing}
  <tr><td colspan="3" align="center">&nbsp;</td></tr>
  <tr><td colspan="3" align="center"><b><a href="checkininfo.php?frame=ausweis&amp;id={$id}">Druckversion</a></b></td></tr>
  {#IF !empty($configurl)}<tr><td colspan="3" align="center"><b><a href="{$configurl}">{§vorgegebene Daten bearbeiten}</a></td></tr>{#END}
  {#END}
</table>
{#END}