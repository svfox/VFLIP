{#SUB default}
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="btn-group">
			{#FOREACH $types $t}
				{#IF $t == $type}<a href="#" class="btn btn-primary btn-sm" role="button">&nbsp;{#UCFIRST $t}</a>
				{#ELSE}<a href="user.php?type={$t}" class="btn btn-default btn-sm" role="button">&nbsp;{#UCFIRST $t}</a>{#END}{#MID}&nbsp;
			{#END}
			<a href="user.php?frame=editproperties&amp;id=create_new_subject&amp;type={$type}" class="btn btn-info btn-sm" role="button"><i class="fa fa-user-plus"></i>&nbsp;{§add User}</a>
			</div>
		</div>
		<div class="row">
		<div class="col-xs-12">
		<p>{§"Rechte &uuml;ber" f&uuml;r Eigenschaften verwenden}: {$userightsover}</p>
		</div>
		</div>
	</div>
</div>
<div class="panel panel-info">
	<div class="panel-heading">
			<div class="row">
				{#TPL $text}
			</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-8">
				<b>Subjektsuche</b>
				<!-- 
				{#FORM subjectsearch method=get}
				 {#INPUT type hidden $type}
				 {#INPUT search string} 
				 {#SUBMIT Los}
				{#END}
				 -->
				<form action="search.php" accept-charset="utf-8">
					<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
						<input type="hidden" name="mode" value="default" class="form-control"/>
						<input type="hidden" name="usedmodules[user]" value="1" class="form-control"/>
						<input type="text" name="searchstring" value="" class="form-control"/>
						<input type="submit" class="button" value="suchen (Alt+S)" accesskey="s">
					</div>
				</form>
				</div>
				<div class="col-xs-4"></div>
			</div>
		</div>
		<div class="row">
		<div class="col-xs-12">
		{#TABLE $items maxrows=100 class="table table-condensed table-hover table-striped"}
			{#IF $itemcount < 2}
				<b>{$itemcount} Eintrag</b>
			{#ELSE}
				<b>{$itemcount} Eintr&auml;ge</b>
			{#END}
			{#COL Name $name}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
			{#COL EMail %email}{#IF empty($email)}<i>Keine</i>{#ELSE}{%email}{#END}
			{! #ACTION login changeuser uri=user.php right=admin condition="$type==user"}
			{#OPTION L&ouml;schen deletesubjects condition="$type!='systemuser'" confirmation="Die gew&auml;hlten Subjekte werden aus dem System gel&ouml;scht - fortfahren?"}
		{#END}
		</div>
	</div>
</div>
{#END}

{#SUB frames}
<div class="panel-heading">	
	<div class="row">
		<div class="col-xs-12">
			{#IF $showback}<a href="user.php?type={$type}" class="btn btn-default btn-md" role="button"><i class="fa fa-reply"></i>&nbsp;{§Zur&uuml;ck}</a>
			{#END}
			{#FOREACH $frames $v $k}<a {#WHEN "$frame == $k" "class=\"btn btn-primary btn-md\"" "role=\"button\""} href="user.php?frame={$k}&amp;id={$id}" class="btn btn-default btn-md" role="button">{$v}</a>{#MID}&nbsp;{#END}
			&nbsp;<a href="lanparty.php" class="btn btn-default btn-md" role="button"><i class="fa fa-users"></i>&nbsp;{§Teilnehmer}</a>
		</div>
	</div>
</div>

{#END}

{#SUB viewsubject}
<div class="panel-group">
	<div class="panel panel-default">
		{#VAR sub=frames}
	</div>
	{#WITH $user}
	<div class="panel panel-default">
		<div class="panel-heading">
			{#IF $allowchange}
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px; margin-top: 5px;">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-2">
					{#IF $checkin}<a href="checkin.php?search={$id}" class="btn btn-default btn-md" role="button"><i class="fa fa-check-square-o"></i>&nbsp;{§Zum Checkin}</a>{#END}
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3">
					{#IF $allowchange}{#ACTION "Als %name einloggen" changeuser params="array(id=$id)" buttonclass="btn btn-warning btn-md"}{#END}
					</div>
					<div class="col-xs-12 col-sm-4 col-md-7">
					{#IF $logged_in=="true"}<a href="webmessage.php?frame=sendmessage&amp;receiver={#URL $name}" class="btn btn-default btn-md" role="button"><i class="fa fa-comments"></i>&nbsp;{§Nachricht senden}</a>{#END}
					{#IF $catering}<a href="shop.php?frame=adminusers&userid={$id}" class="btn btn-default btn-md" role="button"><i class="fa fa-shopping-basket"></i>&nbsp;{§Bestellungen einsehen}</a>{#END}
					</div>
				</div>
			</div>
			{#END}
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-6 col-sm-2 col-md-2">
					{#IF $type == "user"}
					{#IF !empty($user_img)}
					{#DBIMAGE $user_img process=createthumbnail(150,150)}
					{#END}
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 table-responsive">
						<table class="table table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th>Firstname</th>
								<th>Lastname</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{§Nickname}:</td><td>{%name}</td>
							</tr>
							<tr>
								<td>{§Homepage}:</td><td><a href="{%homepage}" target="_blank">{%homepage}</a></td>
							</tr>
							<tr>
								<td>{§Status}:</td><td>{$status_name}</td>
							</tr>
							<tr>
								<td>{§Sitzplatz}:</td><td>{#IF empty($seat)}<i>{§nicht reserviert}</i>{#ELSE}<b><a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{$seat}</a></b>{#END}</td>
							</tr>
							{#IF !empty($ip)}
							<tr>
								<td>{§IP-Adresse}:</td><td>{$ip}</td>
							</tr>
							{#END}
							<tr>
								<td>{§Beschreibung}:</td><td>{$description}</td>
							</tr>
						</table>
					</div>
					<div class="col-sm-6 col-md-6"></div>
				</div>
			</div>
		</div>
	</div>
	{#ELSEIF $type == "group"}
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12">
					<table cellpadding="10">
					  <tr>
						<td>{%description}</td>
					  </tr>
					  {#IF isset($colvals) and isset($cols)}
					  <tr>
						<td>
						  <table class="table" class="table table-condensed table-hover table-striped">
							<tr>
							  <th class="tdedit">Name</th>
							  {#FOREACH $cols $c}<th class="tdedit">{$c}</th>{#END}
							</tr>
							{#FOREACH $colvals}
							<tr>
							  <td class="tdcont"><b><a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a></b></td>
							  {#FOREACH $cols $c $k}
								{#IF is_array($colvals[$name][$k])}
								  {#LOAD $colvals[$name][$k]['data'] $imgpath}
								  <td class="tdcont"><img src="{$imgpath}"></td>
								{#ELSE}
								  <td class="tdcont">{%$k}</td>
								{#END}
							  {#END}
							</tr>
							{#END}
						  </table>
						</td>
						{#END}
					  </tr>    
					</table>
				
				</div>
			</div>
		</div>
	</div>
	{#END}
	{#END}
</div>
{#END}

{#SUB editproperties}
<div class="panel panel-default">
{#VAR sub=frames}
<div class="panel-body">
	{#FORM properties}
	<table>
	  {#FOREACH $items}
		<tr>
		  <td align="right">{%caption}:</td>
		  {#IF $val_type=="Dropdown"}
			<td>{#INPUT $name $val_type $val allowempty=$allowempty enabled=$enabled param=$vals} {$hint}</td>
		  {#ELSE}
			<td>{#INPUT $name $val_type $val allowempty=$allowempty caption=$caption enabled=$enabled} {$hint}</td>
		  {#END}
		</tr>	
	  {#END}
	  {!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
	  {#IF false}{#INPUT justtomaketheformanuploadform file enabled=0}{#END}
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
	  </table>
	{#INPUT subject_id hidden $id}
	{#IF $id=="create_new_subject"}{#INPUT type hidden $type}{#END}
	{#END}
	</div>
</div>
{#END}

{#SUB editrights}
<div class="panel panel-default">
{#VAR sub=frames}
<div class="panel-body">
	{#TABLE $items}
	  {#COL Recht $right}<a href="user.php?frame=viewright&amp;id={$id}">{%right}</a>
	  {#COL "Geerbt von" $owner_name}{#IF $subject_id != $owner_id}<a href="user.php?frame=editrights&amp;id={$owner_id}">{%owner_name}</a>{#END}
	  {#COL "Kontrolle &uuml;ber" $controled_name}{#IF $controled_id}<a href="user.php?frame=editrights&amp;id={$controled_id}">{%controled_name}</a>{#END}
	  {#ACTION del deletesubjectright params="array(controled_id=$controled_id subject_id=$subject_id)" condition="$subject_id==$owner_id" uri="user.php?frame=editrights&amp;id=$subject_id"}
	{#END}
	{#FORM addright action="user.php?frame=editrights&amp;id=$subject_id"}
	<table class="table" cellspacing="1">
	  <tr>
		<th class="tdedit">{§Recht}:</td>
		<th class="tdedit">{§Kontrolle &uuml;ber}:</td>
		<th class="tdedit">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="tdaction">{#INPUT right rights allowempty=0}</td>
		<td class="tdaction">{#INPUT subject subjects}</td>
		<td class="tdaction">{#SUBMIT Hinzuf&uuml;gen}</td>
	  </tr>
	</table>
	{#INPUT id hidden $subject_id}
	{#END}
	</div>
</div>
{#END}

{#SUB editchilds}
<div class="panel panel-default">
{#VAR sub=frames}
<div class="panel-body">
	{#TABLE $childs}
	  {#COL Mitglieder $name width="150"}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
	  {! #ACTION del deletechild params="array(subject_id=$subject_id)" uri="user.php?frame=editchilds&amp;id=$subject_id"}
	  {#OPTION "Aus Gruppe entfernen" deletechilds}
	  {#OPTION "Verschieben nach: " movechilds}<br />{#INPUT group_id subjects param=group allowempty=0}
	  {#INPUT subject_id hidden $subject_id}
	{#END}
	{#FORM child action="user.php?frame=editchilds&amp;id=$subject_id"}
	<table class="table" cellspacing="1">
	  <tr>
		<th class="tdedit">{§neues Mitglied}:</td>
		<th class="tdedit">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="tdaction">{#INPUT child_id subjects param=user allowempty=0}</td>
		<td class="tdaction">{#SUBMIT Hinzuf&uuml;gen}</td>
	  </tr>
	</table>
	{#INPUT subject_id hidden $subject_id}
	{#END}
	</div>
</div>
{#END}

{#SUB editparents}
<div class="panel panel-default">
{#VAR sub=frames}
<div class="panel-body">
	{#TABLE $parents}
	  {#COL Gruppen $name width="150"}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
	  {#ACTION del deleteparent params="array(subject_id=$subject_id)" uri="user.php?frame=editparents&amp;id=$subject_id"}
	{#END}
	{#FORM parent action="user.php?frame=editparents&amp;id=$subject_id"}
	<table class="table" cellspacing="1">
	  <tr>
		<th class="tdedit">{§neue Gruppe}:</td>
		<th class="tdedit">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="tdaction">{#INPUT parent_id subjects param=group}</td>
		<td class="tdaction">{#SUBMIT Hinzuf&uuml;gen}</td>
	  </tr>
	</table>
	{#INPUT subject_id hidden $subject_id}
	{#END}
	</div>
</div>
{#END}


{#SUB viewcolumns}
<div class="panel panel-default">
	<div class="panel-heading">
	{#TPL $text}
	<h3>{#FOREACH $types $t}<a {#WHEN "$t==$type" "class=\"important\""} href="user.php?frame=viewcolumns&amp;type={$t}">{$t}</a>{#MID} &middot; {#END}</h3>
	</div>
	<div class="panel-body">
	{#TABLE $items class="table table-condensed table-hover table-striped"}
	  <a href="user.php?frame=editcolumn&amp;type={$type}">{§add}</a>
	  {#COL Name<br/>Bemerkung}<b>{#WHEN "$required=='Y'" "<span class=\"important\">%name</span>" %name}</b><br />{%comment}
	  {#COL Titel<br/>Hinweis}<b>{%caption}</b><br />{$hint}
	  {#COL Leserecht<br/>Schreibrecht}{#RIGHT $required_view_right view}<br />{#RIGHT $required_edit_right edit}
	  {#COL Typ}{$val_type}{#IF !empty($default_val)}<br /><i>{%default_val}</i>{#END}
	  {#FRAME edit editcolumn}
	  {#ACTION /\ switchcolumns params="array(this=$sort_index last=$last_index)" uri="user.php?frame=viewcolumns&amp;type=$type" condition=$last_index}
	  {#ACTION del delcolumn uri="user.php?frame=viewcolumns&amp;type=$type" confirmation="Soll die Spalte <span style=text-decoration:underline>$name</span> wirklich <strong>gel&ouml;scht</strong> werden?"}
	{#END}
	</div>
</div>
{#END}

{#SUB editcolumn}
<div class="panel panel-default">
	<div class="panel-body">
	{#FORM column}
	<table width="80%">
	  <tr><td align="right">{§Name}:</td><td>{#INPUT name name $name}</td></tr>
	  <tr><td align="right">{§Bemerkung}:</td><td>{#INPUT comment longstring $comment}</td></tr>
	  <tr><td align="right">{§Titel}:</td><td>{#INPUT caption string $caption}</td></tr>
	  <tr><td align="right">{§Hinweis}:</td><td>{#INPUT hint longstring $hint}</td></tr>
	  <tr><td align="right">{§Erforderlich}:</td><td>{#INPUT required yesno $required}</td></tr>
	  <tr><td align="right">{§Leserecht}:</td><td>{#INPUT required_view_right rights $required_view_right param=infinite}</td></tr>
	  <tr><td align="right">{§Schreibrecht}:</td><td>{#INPUT required_edit_right rights $required_edit_right param=infinite}</td></tr>
	  <tr><td align="right">{§Typ}:</td><td>{#INPUT val_type inputtypes $val_type}</td></tr>
	  <tr><td align="right">{§min. L&auml;nge}:</td><td>{#INPUT min_length integer $min_length} (0 = {§unbegrenzt})</td></tr>
	  <tr><td align="right">{§max. L&auml;nge}:</td><td>{#INPUT max_length integer $max_length} (0 = {§unbegrenzt})</td></tr>
	  <tr><td align="right">{§Defaultwert}:</td><td><span class="edit" style="height:100%;">{%default_val}</span> ....
		<a href="config.php?frame=edit&amp;{#IF is_null($default_val)}key={$name}">{§Erstellen}{#ELSE}id={$name}">{§&auml;ndern}{#END}</a>
	  </td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" align="center"><br />{#SUBMIT Speichern}</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" align="center"><br />{#BACKLINK Zur&uuml;ck}</td></tr>
	</table>
	{#INPUT id hidden $id}
	{#INPUT type hidden $type}
	{#END}
	</div>
</div>
{#END}

{#SUB viewrights}
<div class="panel panel-default">
	<div class="panel-body">
	{#TPL $text}
	{#TABLE $items}
	  <a href="user.php?frame=editright">{§add}</a>
	  {#COL verg $count align=right}
	  {#COL Recht $right}{#IF $id != -1}<b><a href="user.php?frame=viewright&amp;id={$id}">{$right}</a></b>{#ELSE}<b>{$right}</b>{#END}
	  {#COL Beschreibung %description}
	  {#FRAME edit editright}
	  {#ACTION del deleteright condition="empty($count) and ($id != -1)" uri="user.php?frame=viewrights"}
	{#END}
	</div>
</div>
{#END}

{#SUB editright}
<div class="panel panel-default">
	<div class="panel-body">
	{#FORM right}
	<table width="80%">
	  <tr><td align="right">{§Recht}:</td><td>{#INPUT right name $right}</td></tr>
	  <tr><td align="right">{§Beschreibung}:</td><td>{#INPUT description longstring $description}</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" align="center"><br />{#SUBMIT Speichern}</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" align="center"><br />{#BACKLINK Zur&uuml;ck}</td></tr>
	</table>
	{#INPUT id hidden $id}
	{#END}
	</div>
</div>
{#END}

{#SUB viewright}
<div class="panel panel-default">
	<div class="panel-body">
	{#TPL $text}
	<p><a href="user.php?frame=viewrights">{§Zur&uuml;ck}</a></p>
	<p>{%description}</p>
	{#TABLE $subjects}
	  {#COL Besitzer $owner}<a href="user.php?frame=editrights&amp;id={$owner_id}">{%owner}</a>
	  {#COL Kontrollierter $controled}<a href="user.php?frame=editrights&amp;id={$controled_id}">{%controled}</a>
	{#END}
	</div>
</div>
{#END}

{#SUB checkin}
User Checkin Frame ?
<!--
	<form name="f" action="user.php" method="get">
	  <input name="search" type="text" value="{%search}" class="edit" title="{§Irgend ein im Profil enthaltener Wert}"/>
	  <input name="frame" type="hidden" value="checkin"/>
	  <input value="Suchen" type="submit" class="button"/>
	  &nbsp;&nbsp;&nbsp;<a style="font-size:18px;" href="user.php?frame=editproperties&amp;id=create_new_subject&amp;type=user&amp;enabled=Y">{§neu}</a>
	</form>	
	<br />
	{#IF count($users) > 0}
	  {#TABLE $users}
		{#COL En $enabled}
		{#COL Nick $name}<b><a href="user.php?frame=checkin&amp;search={$id}">{%name}</a></b>
		{#COL Name $givenname}{%givenname} {%familyname}
		{#COL Email %email}
	  {#END}
	{#END}
	{#IF count($user) > 0}{#WITH $user}
	  <style type="text/css">
		.b{font-size:18px;}
		.button{font-size:16px; font-weight: bold;}
	  </style>
	  <span class="b" style="color:#008800"><b class="b">{%nick}</b> ({%name})</span><br /><br />
	  <p class="b">
		<a class="b" href="user.php?frame=viewsubject&amp;id={$id}">{§&Uuml;bersicht}</a>&nbsp;&middot;&nbsp;
		<a class="b" href="user.php?frame=editproperties&amp;id={$id}">{§Profil}</a>&nbsp;&middot;&nbsp;
		<a class="b" href="seats.php?userid={$id}">{§Sitzplatz}</a>
	  </p>
	  <p class="b">
		{#FOREACH $warnings $w}<span style="color:#E60000">{$w}</span><br />{#END}
		<br />
		{#FOREACH $buttons $capt $action}
		  {#FORM action="user.php?frame=checkin&amp;search=$id" keepvals=0}
			{#INPUT action hidden $action}
			{#INPUT id hidden $id}
			<input type="submit" class="button" style="{#WHEN "$action=='setcheckedout'" background-color:#FF8888}{#WHEN "$action=='setcheckedin'" background-color:#88FF88}" value="{$capt}" />
		  {#END}<br />
		{#END}
		{#IF $barcode}
		  <form name="b" action="user.php?frame=checkin&amp;search={$id}" method="post">
			<input name="hw_barcode" type="text" value="" class="edit" />
			<input name="id" type="hidden" value="{$id}" />
			<input name="action" type="hidden" value="setbarcode" />
			<input value="Speichern und Einchecken" type="submit" class="button" style="background-color:#88FF88" />
		  </form>	
		{#END}
		{#IF $signoffbutton}
		  {#ACTION "Benutzer abmelden" SignOff confirmation="Wollen Sie den Benutzer wirklich von der Party abmelden und seinen Sitzplatz wieder frei geben? Sein 'Bezahltstatus' wird dann auch aufgehoben." params="array(id=$id)" buttonattrs="array(style=background-color:yellow;)"}
		{#END}
		<table>
		  {#FOREACH $props $val $key}<tr><td align="right" class="b">{%key}:</td><td class="b" style="font-weight: bold;">{%val}</td></tr>{#END}
		</table>
	  </p>    
	{#END}{#END}
	{#IF (count($users) == 0) and (count($user) == 0)}
	  <p>
		{§keine User gefunden} :-(
	  </p>
	{#END}
	<script>
	  {#IF $user.barcode}
		document.b.hw_barcode.focus();
	  {#ELSE}
		document.f.search.focus();
		document.f.search.select();
	  {#END}
	</script>
-->
{#END}

{#SUB register}
	{#FORM register action="user.php?frame=register"}
	<div class="panel-group">
		<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12">
				{#TPL $text}
				</div>
			</div>
		</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-8">
					
					  {#FOREACH $items}
					   <div class="row">
							<label class="col-sm-2 control-label">{%caption}:</label>
							<div class="col-sm-10">
							{#INPUT $name $val_type allowempty=0 caption=$caption}{$hint}
							</div>
						</div>
						
					  {#END} 
					  {#IF $password == 'Y'} 
						<div class="row">
							<label class="col-sm-2 control-label">Passwort für die Anmeldung</label>
							<div class="col-sm-10">
							{#INPUT lanparty_passwordd passwordquery}
							</div>
						</div>
						{#END}
				
						{#IF !empty($lanparty)}
						<div class="row">
						<div class="col-xs-12">
							<div class="col-sm-2" style="margin-top:5%; padding-left:10%;">
								{#INPUT lanparty_participate checkbox}</div>
							<div class="col-sm-10">
								{#TPL $lanparty}
								</div>
						</div>
						</div>
						{#END}
						{#IF !empty($siteKey)}
						<div class="row">
						<div class="col-xs-12">
						<!-- include recaptcha alra -->
						<script src="https://www.google.com/recaptcha/api.js" async defer></script>
						<?php
						if ($resp != null && $resp->success) {
							echo "You got it!";
						}
						?>
						<form action="/" method="post">
								<fieldset>
									<legend>Recaptcha </legend>
								<div class="g-recaptcha" data-sitekey={$siteKey}></div>
									<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=de"></script>
									<br/>
									{#SUBMIT Registrieren}
								</fieldset>
							</form>
						<!-- recaptcha ende -->
						</div>
						</div>
						{#END}
				</div>
				<div class="col-sm-12 col-md-3 col-lg-4"></div>
			</div>
			</div>
		</div>
	</div>
	{#END}
	{#IF $lansurferimport == "Y"}
	<hr/>
	<h1>{§Account von Lansurfer &uuml;bernehmen}</h1>
	{#FORM lansurfer_import keepvals=1}
	<table>
	  <tr><td colspan="2">{§Daten von Lansurfer importieren (EMail-Adresse und Passwort von Lansurfer ben&ouml;tigt).}</td></tr>
	  <tr>
		<td align="right">Lansurfer-{§Email}:</td>
		<td>{#INPUT ls_email email allowempty=0}</td>
	  </tr>	
	  <tr>
		<td align="right">Lansurfer-{§Passwort}:</td>
		<td>{#INPUT ls_password passwordquery allowempty=0}</td>
	  </tr>
	  {#IF !empty($lanparty)}
	  <tr><td colspan="2">
		<table cellpadding="2" cellspacing="0"><tr><td>    
		  {#INPUT lanparty_participate checkbox}
		</td><td>
		  {#TPL $lanparty}
		</td></tr></table>	
	  </td></tr>
	  {#END}
	  <tr><td colspan="2" align="center">{#SUBMIT importieren i}</td></tr>
	  {#IF count($ls_items) > 0}
	  <tr><td colspan="2"><br/>{§manuelle Eingabe, falls nicht von Lansurfer geliefert}:</td></tr>
	  {#FOREACH $ls_items}
		{#IF $name != "password"}
		<tr>
		  <td align="right">{%caption}:</td>
		  {#IF $val_type == "sex"} {! einige Inputs brauchen vorgegebene Werte (z.B. aus Dropdowns) }
		  <td>{#INPUT $name $val_type allowempty=0 caption=$caption} {$hint}</td>
		  {#ELSE}
		  <td>{#INPUT $name $val_type caption=$caption} {$hint}</td>
		  {#END}
		</tr>
		{#END}
	  {#END}  
	  <tr><td colspan="2" align="center">{#SUBMIT importieren i}</td></tr>
	  {#END}
	</table>
	{#END}
	{#END}
{#END}

{#SUB resetpwd}
	{#TPL $text}
	<br />
	{#FORM reset}
	{§Ident}: {#INPUT email string}<br />
	<br />
	{#SUBMIT "Passwort zur&uuml;cksetzen"}
	{#END}
{#END}

{#SUB setpwd}
	{#FORM setpwd}
	{§Passwort}: {#INPUT password passwordedit}<br />
	<br />
	{#SUBMIT setzen}
	{#INPUT id hidden $id}
	{#INPUT code hidden $code}
	{#END}
{#END}

{#SUB resendactivation}
	{#TPL $text}
	<br />
	{#FORM resend}
	{§Ident}: {#INPUT ident string}<br />
	<br />
	{#SUBMIT "Email senden"}
	{#END}
{#END}

{#SUB changeemail}
	{#TPL $text}
	<br />
	{#FORM changeemail}
	<table>
	  <tr><td align="right">{§Ident}:</td><td>{#INPUT ident string}</td></tr>
	  <tr><td align="right">{§Password}:</td><td>{#INPUT pass passwordquery}</td></tr>
	  <tr><td align="right">{§neue Email-Adresse}:</td><td>{#INPUT email email}</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td align="center" colspan="2">{#SUBMIT "Email &auml;ndern"}</td></tr>
	</table>
	{#END}
{#END}

{#SUB user4turnier}
	{#FORM user4turnier action="user.php?frame=user4turnier"}
	<p>{§Hier kann ein User erstellt werden, der sofort das Recht hat um an Turnieren teilzunehmen.}</p>
	<table>
	  {#FOREACH $items}
		<tr>
		  <td align="right">{%caption}:</td>
		  <td>{#INPUT $name $val_type allowempty=0 caption=$caption} {$hint}</td>
		</tr>	
	  {#END}  
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td align="center" colspan="2">{#SUBMIT Registrieren}</td></tr>
	  </table>
	{#END}
{#END}

{-------------------------- login ---------------------------------------}

{#SUB login}
<div class="panel panel-default">
	<div class="panel-heading">	
		<div class="row">
			{#IF $isloggedin}
				<div class="col-xs-12">
				<h3>{%user}</h3>
				</div>
			{#END}
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				{#IF $isloggedin}
				<form method="post" id="frm2">
					<input type="hidden" name="ident" value="Anonymous" />
					<input type="hidden" name="password" value="x" />
					<button type="submit" class="btn btn-primary btn-md" value="logout">Logout&nbsp; <i class="fa fa-sign-out"></i>
					</button>
				</form>
				{#ELSE}
					<form method="post" id="frm2">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input id="ident" type="text" class="form-control" name="ident" title="Deine UserID, dein Nickname oder deine Email-Adresse" placeholder="UserID, Nickname oder Email-Adresse" 
							onclick="this.value='';" onFocus="if(this.value=='Nickname casesensitive') this.value='';" {#WHEN "!empty($loginident) and ($loginident != \"Anonymous\")" "value=\"%loginident\" "}/>
						</div>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="password" type="password" class="form-control" name="password" placeholder="Password" title="Passwort" onclick="this.value='';" />
						</div>
						<div class="input-group-btn" style="padding-top: 10px;">
						<button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i>&nbsp; Sign In </button>
						<a href="text.php?name=user_account_trouble" class="btn btn-default btn-md" role="button"><i class="fa fa-magic"></i>&nbsp;<i class="fa fa-user-md"></i>&nbsp;Account Wiederherstellung</a>
						</div>
					</form>
					<script>
					var loginform = document.getElementById('frm2');
					{#IF empty($loginident) or ($loginident == "Anonymous")}
					loginform.ident.value='Nickname';	
					loginform.password.value='Password';
					{#ELSE}
					loginform.password.focus();
					{#END}
					</script>          
				{#END}
			</div>
			<div class="col-sm-6 col-md-6"></div>
		</div>
	</div>
</div>
{#END}
