{! 
  $Id: tournament.tpl 1500 2015-02-12 13:40:59Z loom $ edit by naaux
}
{#SUB archivtournamenthead}
<!-- erweitertes Turnier Menue, wird mit inc.tournament.tpl ergaenst -->
		<div class="btn-group" role="group">
		  <a href="{$file}?frame=archivoverview&amp;id={$tournamentid}"{#WHEN "$curframe == 'overview'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-eye"></i>&nbsp;&Uuml;bersicht</a>
		  <a href="{$file}?frame=archivtree&amp;id={$tournamentid}"{#WHEN "$curframe == 'tree'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-sitemap"></i>&nbsp;Turnierbaum</a>
		  <a href="{$file}?frame=archivresults&amp;id={$tournamentid}"{#WHEN "$curframe == 'results'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-trophy"></i>&nbsp;Platzierung</a>
		</div>
{#END}

{#SUB ArchivOverview}
<!-- Turnier Uebersicht -->
<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				{#VAR sub=archivtournamenthead}
			</div>
		</div>
		
		<div class="panel-body">
			<div class="row" align="center">
				<div class="col-sm-12">{#DBIMAGE flip_tournament_tournaments icon $id $mtime}</div>
				<div class="col-sm-12"><h3>Info</h3></div>
			</div>
			<div class="row" align="left">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Orga:</b></div>
				<div class="col-xs-4 col-sm-3 col-md-2 col-lg-3">{#FOREACH $orgas $organame $orgaid}<a href="user.php?frame=viewsubject&amp;id={$orgaid}">{%organame}</a>{#MID}, {#END}</div>
				<div class="col-sm-2 col-md-4 col-lg-2"></div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Kosten:</b></div>
				<div class="col-xs-4 col-sm-3 col-md-2 col-lg-3">{$coins} {$currency}</div>
			</div>
			<div class="row" align="left">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Beginn:</b></div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">{$begin}</div>
				<div class="		 col-sm-1 col-md-2 col-lg-2"></div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Teams:</b></div>
				<div class="col-xs-4 col-sm-3 col-md-2 col-lg-3">{$numteams}</div>
			</div>
			<div class="row" align="left">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Status:</b></div>
				<div class="col-xs-4 col-sm-3 col-md-2 col-lg-3">{$status}</div>
				<div class="col-sm-2 col-md-4 col-lg-2"></div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Spieler:</b></div>
				<div class="col-xs-4 col-sm-3 col-md-2 col-lg-3">{$numplayers}</div>
			</div>	
			<div class="row" align="left">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><b>Modus:</b></div>
				<div class="col-xs-4 col-sm-3 col-md-2 col-lg-3">{$mode}</div>
				<div class="col-sm-1 col-md-4 col-lg-2"></div>
				<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2"><b>Rundendauer:</b></div>
				<div class="col-xs-3 col-sm-3 col-md-2 col-lg-3">{$round}</div>
			</div>					
		</div>
		<div class="panel-body">
			<div class="row" align="left">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">Beschreibung</div>
						<div class="panel-body">{$TDescription}</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">Teilnehmer</div>
						<div class="panel-body">
							{#IF is_numeric($anmeldeid)}
							<a href="{$file}?frame=getTeam&amp;id={$anmeldeid}" class="btn btn-md btn-default" role="button"><i class="fa fa-sign-in"></i>&nbsp;Anmelden</a><br />
							{#ELSE}
							{%anmeldeid}<br />
							{#END}
						</div>
						<table class="table table-bordered table-condensed table-hover table-striped">
							<thead>
							  <tr>
								<th>Team</th>
								{#IF $teamsize > 1}
									{#IF $anmeldestatus}
									<th>Aktion</th>
									{#END}
								{#END}
								<th>Nickname</th>
								<th>Ingame Nickname</th>
							  </tr>
							 </thead>
							 <tbody>
								{#FOREACH $teams index=$i}
								  <tr class="{#IF ($i % 2 == 1)}row2{#ELSE}row1{#END}">
									<td>{$team}</td>
									{#IF $teamsize > 1}
									{#IF $anmeldestatus}<td><i>{$action}</i></td>{#END}
									{#END}
									<td>
									  {#FOREACH $players index=$i}<a href="user.php?frame=viewsubject&amp;id={$userid}">{$nick}</a>{#MID}, {#END}
									</td>
									 <td>
										<!-- alra Anpassung - ingame nickname Anzeige Turnier Teilnehmerliste -->
									  {#FOREACH $players index=$i}<a href="user.php?frame=viewsubject&amp;id={$userid}">{$ingame_nick_name}</a>{#MID}, {#END}
									</td>
								  </tr>
								{#END}
							</tbody>
						</table>
						
					</div>
				</div>
			</div>	
		</div>
</div>
{#END}

{#SUB ArchivResults}
<!-- Turnier Plazierung -->
<div class="panel panel-default">
	<div class="panel-heading">
			{#VAR sub=tournamenthead}
	</div>
	<div class="panel-body" align="center">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Plazierung</h3>
			</div>
			<div class="panel-body" align="center">
				<div class="row" align="center">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isFirst)}{$first}{#ELSE}<strong>{$first}</strong>{#END}</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				</div>
				<div class="row" align="center">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isSecond)}{#IF empty($second)}&nbsp;{#ELSE}{$second}{#END}{#ELSE}<strong>{$second}</strong>{#END}</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;"><strong>1</strong></div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isThird)}{#IF empty($third)}&nbsp;{#ELSE}{$third}{#END}{#ELSE}<strong>{$third}</strong>{#END}</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				</div>
				<div class="row" align="center">
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;"><strong>2</strong></div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;">&nbsp;</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;"><strong>3</strong></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				</div>
				<div class="row" align="left">
					{#FOREACH $rankings}
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="right">{$rank}.</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isInTeam)}{$name}{#ELSE}<strong>{$name}</strong>{#END}</div>
					<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">&nbsp;</div>
					{#END}
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB Archivtree}
<!-- Turnierbaum -->
<!-- siehe tounament.php (frametree)
	"treetypefile" = "inc.tree.default.tpl";
	"treetypestyles" = "inc.tree.default.styles.tpl";
 -->
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			{#VAR sub=archivtournamenthead}
		</div>
	</div>
	<div class="panel-body">
			{#IF !$showgames}
			<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h3>Bisher hat das Turnier noch nicht begonnen.</h3></div>
			</div>
			{#ELSE}
				{#IF $toolate=="true"}
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<ul class="list-group">
							<li class="list-group-item list-group-item-danger">
								<i class="fa glyphicon-warning-sign"></i> Die Spielzeit f&uuml;r diese Runde ist bereits abgelaufen: <br>
																		Ergebnismeldung bzw. R&uuml;cksprache halten beim zust&auml;ndigen Turnierorga.
							</li>
						</ul>
					</div>
				</div>
				{#END}
				{#IF true}<!-- Tournament Tree Winnerbracket -->
				<div class="row">	
					{#FORM jumpto}<!-- Suche-->
					<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">Teamname:</div>
					<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">{#INPUT teamname string $teamname}</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">{#SUBMIT finden}</div>
					{#END}
					<!-- Suche ausgabe-->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">{#FOREACH $teamids $id $name index=$i}<a href="#{$id}">{%name}</a>{#MID}, {#END}</div>
					<!--Jump to -->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<a href="#winner" class="btn btn-md btn-default" role="button"><i class="fa fa-arrow-right "></i>&nbsp;Winnerbracket</a>
						{#IF !empty($lcoldef)}<a href="#loser" class="btn btn-md btn-default" role="button"><i class="fa fa-arrow-right"></i>&nbsp;Loserbracket</a>{#END}
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
						{#WHEN "!empty($groups)" "<h3>"}<!-- muss noch angepasst werden. nur fuer Gruppen Turnier? wtf... -->
							{#FOREACH $groups}<a href="#{$title}">{$title}</a>{#MID}&middot;{#END}
						{#WHEN "!empty($groups)" "</h3>"}
					</div>
				</div>	
				<div class="row">	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div align="center" valign="middle">
							{#WITH $coldef}<!-- Ueberschrift incl. jump to -->
								<div>
									<h2><a name="winner"></a>Winnerbracket</h2>
								</div>
							{#END}
							<!-- hack edit by naaux, counter fuer variable breite des turnierbaums -->
							<div style="clear:both"></div>
							<?php $counter = 0; ?>
							{#FOREACH $col}<?php $counter++; ?>{#END} <!-- hack ende -->
							<div style="overflow-x:scroll; vertical-align: middle; height:100%; width:<?php echo $pixwidth=300*$counter; ?>px;">
							  {#FOREACH $col index=$currentcol}<!-- je Runde 1x frametree - inc.tree.default.style -->
								  
								  <!-- hack edit by naaux, es soll ein zusaetliches diff fuer jede col eingefügt werden auuser round 1 ... arghhhhh -->
									{#LOAD "($currentcol == 1) ? true : false" $isfirstcol}
									{#LOAD "($currentcol == count($col)) ? true : false" $islastcol}
										<?php
											if($isfirstcol==true)
											{ 
											print("<div style=\"width:200px; height:60px;\"> Test currentcol:$currentcol and col:$col and counter: $counter2 and rowcounter:$i and $isfirstcol and $islastcol
												</div>");
											}
											else
											{
											// nothing to do
											}
											?>
									<!-- hack ende -->
									
								{#VAR sub=_atree}
							  {#END}
							  <div style="clear:both"></div> <!-- clean up. evt andere Position -->
							</div>
						</div>
					</div>
				</div>	
				{#END} <!-- ende Tournament Tree winnerbracket -->
				{#IF true}<!-- Tournament Tree Looserbracket -->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						{#IF !empty($lcoldef)}
							<div align="center" valign="middle">
								{#WITH $lcoldef}<!-- Ueberschrift incl. jump to -->
									<div>
										<h2><a name="loser"></a>Loserbracket</h2>
									</div>
								{#END}
								<!-- hack edit by naaux, counter fuer variable breite des turnierbaums -->
								<div style="clear:both"></div>
								<?php $counterl = 0; ?>
								{#FOREACH $lcol}<?php $counterl++; ?>{#END} <!-- hack ende -->
								<div style="overflow-x:scroll; vertical-align: middle; height:100%; width:<?php echo $pixwidth=300*$counterl; ?>px;">
								  {#FOREACH $lcol index=$currentcol}<!-- je Runde 1x frametree - inc.tree.default.style -->
									{#VAR sub=_atree} 
								  {#END}
								  <div style="clear:both"></div> <!-- clean up. evt andere Position -->
								</div>
							</div>
						{#END}{!if !empty(lcoldef)} <!-- ende Tournament Tree Looserbracket -->
					</div>	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						{#FOREACH $groups}<!-- Tournament Tree Groupgames -->
							  <h2><a name="{$title}">{$title}</a></h2>
							  <h3>Tabelle</h3>
							  {#TABLE $ergebnis class="table"}
								{#COL "Team" $name}
								{#COL "Spiele" $matchcount}
								{#COL "+" $won}
								{#COL "=" $equal}
								{#COL "-" $lost}
								{#COL "Punkte" $points}
								{#COL "Ergebnis" $pluspunkte}
									<div style="height:1em;position:relative;">
										<div style="text-align:right;width:2em;position:absolute;display:inline;">{$pluspunkte}</div>
										<div style="left:2em;position:absolute;display:inline;">: {$minuspunkte}</div>
									</div>
							  {#END}
							  <br />
							  <h3>Paarungen</h3>
							  <table class="table" cellpadding="0" cellspacing="1">
							  {#FOREACH $rounds index=$j}
								  <tr>
									<td class="tdedit">{$j}. Runde</td>
									{#FOREACH $matches}
									<td class="tdcont">
									  <table width="100%" cellspacing="0" cellpadding="0" border="0">
									  <tr>
										<td class="tdcont" width="40%"><span class="{$color1}">{#IF !empty($team1) && empty($points1) && empty($points2)}{#LOAD $ready1 $ready}{#LOAD $readylink1 $readylink}{#VAR sub=_ready file=$treetypefile}&nbsp;{#END}{$team1html}</span></td>
										<td class="tdcont" width="20%" align="center">{$score}<br /><small class="comment">{$comment}</small></td>
										<td class="tdcont" width="40%" align="right"><span class="{$color2}">{$team2html}{#IF !empty($team2) && empty($points1) && empty($points2)}{#LOAD $ready2 $ready}{#LOAD $readylink2 $readylink}&nbsp;{#VAR sub=_ready file=$treetypefile}{#END}</span></td>
									  </tr>
									  </table>
									</td>
									{#END}
								</tr>
							  {#END}
							  </table>
						{#END}{!foreach groups}
						<!-- Vorbereitung fuer Punkte System
						/**#FOREACH $points}
						//	 <h2><a name="$title}">$title}</a></h2>
						//  <h3>Tabelle</h3>
						//  #TABLE $ergebnis class="table"}
						//		#COL "Teilnehmer" $name}
						//			#FOREACH $rounds index=$j}
						//				<tr>
						//					<td class="tdedit">$j}. Runde</td>
						//				#FOREACH $matches}
						//				<td class="tdcont">
						//					#COL "Punkte" $points}
						//				</td>
						//				</tr>
						//		#COL "Ergebnis" $pluspunkte}<div style="height:1em;position:relative;">
						//			<div style="text-align:right;width:2em;position:absolute;display:inline;">{$pluspunkte}</div>
						//			<div style="left:2em;position:absolute;display:inline;">: {$minuspunkte}</div></div>
						//	  #END}
						//	  <br />
						//#END}**/ --> <!-- ende Tournament Tree Pointsgame -->
					</div>
				</div>
				{#END}{!group} <!-- ende Tournament Tree Groupegames -->
			{#END}{!if empty} <!-- close else -->
	</div>
</div>
{#END}
{#SUB showdescription}
<!-- Turnierbeschreibung -->
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		{$description}
	</div>
{#END}

{! subs below this line should not be used standalone }

{#SUB _atree}
	<!-- siehe tounament.php (frametree)
		"treetypefile" = "inc.tree.default.tpl";
		"treetypestyles" = "inc.tree.default.styles.tpl";
	 -->
	{#VAR sub=atree file=$treetypefile}
{#END}
