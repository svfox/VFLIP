
{#SUB _input_string}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_longstring}
  <input style="width:100%;" type="text" class="form-control" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_yesno}
  <input type="radio" class="checkbox" name="{%name}" value="Y"{$sely}{$enabled} />{§Ja}&nbsp;&nbsp;&nbsp;
  <input type="radio" class="checkbox" name="{%name}" value="N"{$seln}{$enabled} />{§Nein}
{#END}

{#SUB _input_radio}
  <input type="radio" class="checkbox" name="{%name}" value="{%val}"{$sel}{$enabled} />
{#END}

{#SUB _input_checkbox}
  <input type="checkbox" class="checkbox" name="{%name}" value="{%param}"{$sel}{$enabled} />
{#END}

{#SUB _input_integer}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_decimal}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_ip}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_dropdown}
  <select name="{%name}"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>
{#END}

{#SUB _input_inputtypes}
  <select name="{%name}_type"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>
  {§Parameter}:
  <input type="text" class="form-control" name="{%name}_param" value="{%param}" size="32"{$enabled} />
{#END}

{#SUB _input_multisel}
  {#FOREACH $items}
    <input type="checkbox" class="checkbox" name="{%name}_{%key}" value="1"{$sel}{$enabled} />{%caption}
  {#MID}
    <br />
  {#END}
{#END}

{#SUB _input_tabledropdown}
  <select name="{%name}"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>{#IF $canedit}&nbsp;&nbsp;&nbsp;<i><a href="table.php?frame=viewtable&amp;name={$table}">({§edit})</a></i>{#END}
{#END}

{#SUB _input_tablemultisel}
  {#FOREACH $items}
    <input type="checkbox" class="checkbox" name="{%name}_{%key}" value="1"{$sel}{$enabled} />{%caption}
  {#MID}
    <br />
  {#END}
  {#IF $canedit}<br/><i><a href="table.php?frame=viewtable&amp;name={$table}">({§edit})</a></i>{#END}
{#END}

{#SUB _input_text}
<!-- new editor ckeditor edit by naaux -->
 <form method="post">
	<textarea rows="10" cols="48" name="{%name}" class="form-control"{$enabled}>{%val}</textarea>
  </form>
{#END}

{#SUB _input_document}
<!-- new editor ckeditor edit by naaux -->
 <form method="post">
	<textarea style="width:100%;" cols="90" rows="30" name="{%name}" class="form-control"{$enabled}>{%val}</textarea>
  </form>
{#END}

{#SUB _input_documentwrap}
<!-- new editor ckeditor edit by naaux -->
 <form method="post">
  <textarea style="width:100%;" cols="90" rows="30" wrap="virtual" name="{%name}" class="form-control"{$enabled}>{%val}</textarea>
  </form>
{#END}

{#SUB _input_phone}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="15"{$enabled} />
{#END}

{#SUB _input_captcha}
	<div class="form-group col-sm-12">
		<label for="Bildtext" class="col-sm-2 control-label">{§Bild}:</label>
		<div class="col-sm-10">
		<img src="ext/captcha/captcha_image.php?img={$url}" alt="{§Bild mit Text fehlt!}"/>
		</div>
	</div>
	<div class="form-group col-sm-12">
		<label for="Bildtext" class="col-sm-2 control-label">{§Bildtext}:</label>
		<div class="col-sm-5">
		<input type="text" class="form-control" placeholder="{%name}" name="{%name}" value="{%val}"{$enabled} />
		</div><div class="col-sm-5"></div>
	</div>
{#END}

{#SUB _input_recaptcha}
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
			<div class="g-recaptcha" data-sitekey="6LcYWgATAAAAADTZeT1ipOn0dz8NxwgILH8G4q0L"></div>
			<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en">
	</script>
{#END}

{#SUB _input_passwordedit}
  <div class="form-group col-sm-12">
  <div>
  Passwort:
  <input type="password" class="form-control" name="{%name}2" value="" size="24"{$enabled} />
  </div>
  <div>
  Passwort {§Wiederholung}:
  <input type="password" class="form-control" name="{%name}1" value="" size="24"{$enabled} />
  </div>
  </div>
{#END}

{#SUB _input_passwordquery}
  <input type="password" class="form-control" name="{%name}" value="" size="24"{$enabled} />
{#END}

{#SUB _input_hidden}
  <input type="hidden" name="{%name}" value="{%val}" />
{#END}

{#SUB _input_file}
<!-- muss noch angepasst werden -->
	<form class="form-inline">
	  <div class="form-group">
		<div class="input-group">
		  <div class="input-group-addon"><i class="fa fa-upload"></i></div>
		  <input type="file" name="{%name}" class="btn btn-default" type="button"{$enabled} />
		  <div class="input-group-addon" align="left">
			<div class="col-xs-12 col-sm-12 col-md-12">
			<small>{§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=1}MB</small></div>
			<div class="col-xs-12 col-sm-12 col-md-12">
		    <small>({$maxreason})</small></div>
			</div>
		</div>
	  </div>
	</form>

{#END}

{#SUB _input_image}
<!-- muss noch angepasst werden -->
	<table>
	  {#IF $allowurl}
	  <tr>
		<td><input type="radio" class="checkbox" name="{%name}_sel" value="url"{$enabled} />URL:</td>
		<td colspan="3"><input type="text" class="form-control" name="{%name}_url" value="" size="69"{$enabled} /></td>
	  </tr>
	  {#END}
	  <tr>
		<td><input type="radio" class="checkbox" name="{%name}_sel" value="file"{$enabled} />{§Datei}:</td>
		<td colspan="3"><input type="file" name="{%name}_file" class="form-control" size="40"{$enabled} /></td>
		<td rowspan="2" style="padding-left:16px;">{#DBIMAGE $image process=createThumbnail(160,64)}</td>
	  </tr>
	  <tr>
		<td align="left" colspan="2"><input type="radio" class="checkbox" name="{%name}_sel" value="del"{$enabled} />{§altes Bild l&ouml;schen}</td>
		<td  colspan="2" align="right" valign="top">({§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=1}MB, <span style="color:grey;">{$maxreason}</span>)</td>
	  </tr>
	</table>
{#END}

{#SUB _input_date}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="17"{$enabled} /> (d.m.y H:i)
{#END}

{#SUB _input_email}
  <input type="text" class="form-control" name="{%name}" value="{%val}" {$enabled} />
{#END}

{#SUB _input_dropdownedit}
  <select name="drop_{$name}"{$enabled} onchange="{$name}_change()">
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>
  <input type="text" class="form-control" name="edit_{$name}" value="{%val}" size="32"{$enabled} />
  <script>
    function {$name}_change()
    {
      var i = -1;
      while(document.forms[++i])
        if(document.forms[i].edit_{$name}) with(document.forms[i])
        {
          edit_{$name}.disabled = (drop_{$name}.value == '') ? false : true;
          if(drop_{$name}.value != '') edit_{$name}.value = drop_{$name}.value;        
        }
    }
  {$name}_change()
  </script>
  <noscript></noscript>
{#END}
