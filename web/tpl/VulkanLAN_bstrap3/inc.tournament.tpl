{! benenne diesen sub in "menu" um, damit der Turnierbaum in einem neuen Fenster angezeigt wird.}
{#SUB menu_mit_baum_und_neuen_fenster}
  <a href="{$file}?frame=overview&amp;id={$tournamentid}"{#WHEN "$curframe == 'overview'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-eye"></i>&nbsp;&Uuml;bersicht</a> 
  <a target="_blank" href="{$file}?frame=tree&amp;id={$tournamentid}&amp;nomenu=1">Turnierbaum</a> 
  <a href="{$file}?frame=results&amp;id={$tournamentid}"{#WHEN "$curframe == 'results'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-trophy"></i>&nbsp;Platzierung</a>
{#END}

{! Dies ist das default-Men&uuml;, in ihm wird der Turnierbaum nicht in einem neuen Fenster ge&ouml;ffnet.}
{#SUB menu}
  <a href="{$file}?frame=overview&amp;id={$tournamentid}"{#WHEN "$curframe == 'overview'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-eye"></i>&nbsp;&Uuml;bersicht</a>
  <a href="{$file}?frame=tree&amp;id={$tournamentid}"{#WHEN "$curframe == 'tree'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-sitemap"></i>&nbsp;Turnierbaum</a>
  <a href="{$file}?frame=results&amp;id={$tournamentid}"{#WHEN "$curframe == 'results'" " class='btn btn-md btn-primary'"}class="btn btn-md btn-default" role="button"><i class="fa fa-trophy"></i>&nbsp;Platzierung</a>
  <a target="_blank" href="{$file}?frame=tree&amp;id={$tournamentid}&amp;nomenu=1" class="btn btn-md btn-default" role="button"><i class="fa fa-sitemap"></i>&nbsp;Turnierbaum only</a> 
{#END}

{! Eine Liste mit den Platzierungen aller Turniere die unter 'Export' erstellt wird }
{#SUB resultexport}
	<style type="text/css">
	body
	{
	  font-family:arial,sans-serif;
	}
	.tournamenttitle
	{
	  text-align:center;padding-top:20px;
	}
	.rankbox
	{
	  border:1px solid black;padding:6px;
	}
	</style>
	<div align="center">
		<h1>{$caption}</h1>
		<h2>am {$partydate}</h2>
			<table>
				{#FOREACH $tournaments}
				<tr><td class="tournamenttitle" colspan="3">
				<h2><a name="{$title}"></a>{$title}</h2>
				</td></tr>
				<tr><td width="20%">&nbsp;</td><td class="rankbox">
				<table>
				{#FOREACH $ranking}
				<tr>
				  <td align="right">{#IF $rank==="1"}<strong>{$rank}.</strong>{#ELSE}{$rank}.{#END}</td><td>{#IF $rank=="1"}<strong>{$teamname}</strong>{#ELSE}{$teamname}{#END}</td>
				</tr>
				{#END}
				</table>
				</td><td width="20%">&nbsp;</td></tr>
				{#END}
			</table>
		<br />
		<small>generiert am {#DATE} von FLIP {$flipversion}</small>
	</div>
{#END}
