{! $Id$ }

{#SUB default}
<div class="panel panel-default">
		<div class="panel-heading">	
			<div class="row">
				{#IF $create}&nbsp;<a href="clan.php?frame=editclan" class="btn btn-default btn-sm" role="button"><i class="fa fa-crosshairs"></i>&nbsp;{§neuen Clan erstellen}</a>&nbsp;{#END}
			</div>
		</div>
		<div class="panel-body">
			<div class="row" align="left">
			<div class="col-xs-12 col-sm-12 col-md-11">
				{#TABLE $clans class="table table-condensed table-hover table-striped"}
				  {#COL Name %name}<a href="clan.php?frame=viewclan&amp;id={$id}">{%name}</a>
				  {#COL Website %homepage}{$homepageLink}
				  {#COL Bild $image}<img src="{$image}" height="80" alt=""/>
				{#END}
			</div>
			<div class="col-md-1"></div>
		</div>
</div>
{#END}

{#SUB viewclan}
<div class="panel-group">
	<div class="panel panel-default">
		<div class="panel-heading">	
			<div class="row">
				<div class="col-xs-4">
				<a href="clan.php" class="btn btn-default btn-sm" role="button"><i class="fa fa-reply"></i>&nbsp;{§zur&uuml;ck zur Liste}</a>&nbsp;
				{#IF $join}<a href="clan.php?frame=join&amp;id={$id}" class="btn btn-success btn-sm" role="button"><i class="fa fa-question-circle-o"></i>&nbsp;Clan beitreten</a>&nbsp;{#END}
				{#IF $leader}&nbsp;<a href="clan.php?frame=editclan&amp;id={$id}" class="btn btn-default btn-sm" role="button"><i class="fa fa-pencil-square-o"></i>&nbsp; Clan {§Daten bearbeiten}</a>{#END}
				</div>
				<div class="col-xs-3">
				{#IF $leader}{#ACTION "Clan l&ouml;schen" del condition=$leader params="array(id=$id)" confirmation="Soll der Clan wirklich gel&ouml;scht werden?" buttonattrs="array(style=\"background-color:#c9302c\")" buttonclass="btn btn-danger btn-sm"}{#END}
				</div>
				<div class="col-xs-5"></div>
			</div>
			{#IF $admin}
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px; margin-top: 5px;">
				<div class="row">
					<div class="col-xs-12">
					<a href="user.php?frame=editchilds&amp;id={$id}" class="btn btn-default btn-sm" role="button"><i class="fa fa-address-card"></i>&nbsp;Clan Mitglieder Verwalten</a>&nbsp;
					</div>
				</div>
			</div>
			{#END}
		</div>
	</div>
	{#LOAD $id $clan_id}
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="col-xs-12">
					<!-- Claninfos -->
					<h2>CLAN {§Infos}</h2>
					</div>
					<div class="col-xs-12">
						<table class="table table-condensed table-hover table-striped">
						{#FOREACH $props}
							{#IF $value != ""}
							<tr><th valign="top">{$caption}:</th><td>{#IF $val_type == "Image"}{#DBIMAGE $value process=createthumbnail(150,150)}{#ELSE}{#IF $val_type == "Text" || substr($val_type, 0, 8) == "Document"}<pre>{$value}</pre>{#ELSE}{$value}{#END}{#END}</td></tr>
							{#END}
						{#END}
						
						</table>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6">
					{#IF $leader}
					<!-- Anfragen -->
					<div class="col-xs-12">
					<h2>{§Anfragen}</h2>
					</div>
					<div class="col-xs-12">
					{#TABLE $asking class="table table-condensed table-hover table-striped"}
						{#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
						{#ACTION hinzuf&uuml;gen confirm params="array(clan_id=$clan_id)"}
					{#END}
					</div>
					{#END}
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">		
		<div class="panel-body">
				<!-- Clanmitglieder -->
				<div class="col-xs-12">
				<h2>{§Mitglieder}</h2>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6">
				{#LOAD $id $clan_id}
				{#TABLE $members class="table table-condensed table-hover table-striped"}
					{#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{#IF $isleader}<i class="fa fa-user-circle-o"></i>&nbsp;{%name}&nbsp;(Leader){#ELSE}{%name}{#END}</a>
					{#ACTION entfernen delmember condition=$leader params="array(clan_id=$clan_id)"}
					{#OPTION "Leader ernennen" nominate condition=$leader}
					{#OPTION "Leader aberkennen" deprive condition=$leader}
				{#END}
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6"></div>
		</div>
	</div>
</div>
{#END}

{#SUB editclan}
<div class="panel panel-default">
	<div class="panel-heading">	
		<div class="row">
			<div class="col-xs-12">
				<a href="clan.php{#IF !(empty($id) || $id == "create_new_subject")}?frame=viewclan&amp;id={$id}{#END}"" class="btn btn-default btn-sm" role="button"><i class="fa fa-reply"></i>&nbsp;{§zur&uuml;ck}</a>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12">
				{#FORM clan}
				{#INPUT id hidden $id}
				{#INPUT type hidden clan}
				<table>
				{#FOREACH $items}
					<tr><th>{#WHEN "$required==\"Y\"" "<span class=\"important\">$caption</span>" $caption}</th>
					<td>{#INPUT $name $val_type $val allowempty=$allowempty caption=$caption enabled=$enabled} {$hint}</td></tr>
				{#END}
				{!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
				{#IF false}{#INPUT justtomaketheformanuploadform file enabled=0}{#END}
				<tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
				<td></td>
				<td>{#SUBMIT}</td></tr>
				</table>
			</div>
				{#END}
		</div>
	</div>
</div>
{#END}

{#SUB join}
<div class="panel panel-default">
	<div class="panel-heading">	
		<div class="row">
			<div class="col-xs-12">
			<a href="clan.php?frame=viewclan&amp;id={$id}" class="btn btn-default btn-sm" role="button"><i class="fa fa-reply"></i>&nbsp;{§zur&uuml;ck}</a>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12">
				{#TPL $jointext}
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
					{#FORM join}
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Checkbox</th>
							<th>Info</th>
						</tr>
						</thead>
						<tbody>
						<tr class="success">
							<td align="center">{#INPUT yes checkbox $yes}</td>
							<td><i class="fa fa-handshake-o" ></i>&nbsp;{§Ja, ich will.}</td>
						</tr>
						<tr>
							<td colspan="2">
							{#INPUT id hidden $id}
							{#INPUT captcha captcha $captcha}
							</td>
						</tr>
						<tr>
							<td colspan="2">
							{#SUBMIT anfragen a}
							</td>
						</tr>
					 </table>
					{#END}
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6"></div>
		</div>
	</div>
</div>
{#END}
