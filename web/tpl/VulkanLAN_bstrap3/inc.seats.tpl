{#SUB header}
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-12">
			<a href="seats.php?frame=blockgraph"{#WHEN "$frame=='blockgraph' or empty($frame)" " class='btn btn-md btn-primary'"} class="btn btn-md btn-default" role="button"><i class="fa fa-file-image-o"></i>&nbsp;Grafisch</a> 
			<a href="seats.php?frame=blocklist"{#WHEN "$frame=='blocklist'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-default" role="button"><i class="fa fa-list-ul"></i>&nbsp;Liste</a>
			</div>
		</div>
		{#IF $showadmin}
		<div class="row">
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div><b>Admin Sitzplatzverwaltung:</b></div>
			<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-default" role="button"><i class="fa fa-pencil-square-o"></i>&nbsp;Bearbeiten</a> 
			<a href="seats.php?frame=FreeSeats"{#WHEN "$frame=='FreeSeats'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-default" role="button"><i class="fa fa-recycle"></i>&nbsp;alle Sitzplštze Freigeben</a>
			</div>
		</div>
		{#END}
	</div>
</div>
{#END}