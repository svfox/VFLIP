{#SUB head}<<?php echo"?"?>xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	 
    <link rel="icon" href="./tpl/VulkanLAN_bstrap3/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="./tpl/VulkanLAN_bstrap3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="./tpl/VulkanLAN_bstrap3/dist/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./tpl/VulkanLAN_bstrap3/css/custom.css" rel="stylesheet">
	<link href="./tpl/VulkanLAN_bstrap3/css/bootstrap_custom.css" rel="stylesheet">

	<!-- Font Awesome Module -->
	<link href="./tpl/VulkanLAN_bstrap3/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="./tpl/VulkanLAN_bstrap3/css/jquery.bracket.min.css" rel="stylesheet">
	 
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./tpl/VulkanLAN_bstrap3/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		
	<!-- include tinymce edit by naaux-->
	<script type="text/javascript" src="./ext/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
		    selector: "textarea",
		    theme: "modern",
			forced_root_block : false,
		    force_p_newlines : true,
		    remove_linebreaks : false,
		    force_br_newlines : false,
		    verify_html : false,
			cleanup: false,
			cleanup_on_startup: false,
			browser_spellcheck : true,
			theme_advanced_resizing: true,
			theme_advanced_resize_horizontal: false,
			auto_resize: false,
			trim_span_elements: false,
			remove_trailing_nbsp : false,
			fontsize_formats: "0.5em 0.8em 0.9em 1em 1.2em 1.5em 1.8em 2em",
			
		    height: 300,
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		         "save table contextmenu directionality emoticons template paste textcolor"
		   ],
		   
		   toolbar: "insertfile undo redo | styleselect | fontsizeselect | fontselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
		   style_formats: [
		        {title: 'Bold text', inline: 'b'},
		        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
		        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
		        {title: 'Example 1', inline: 'span', classes: 'example1'},
		        {title: 'Example 2', inline: 'span', classes: 'example2'},
		        {title: 'Table styles'},
		        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		    ]
		 }); 
	</script>
	<title>{%title} - {%caption}</title>
</head>
	{#END}
	{#SUB page}{#VAR sub=head}
	<body role="document">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		{#LOAD MenuLoad() $level0 inc/inc.menu.php}		
		<div class="navbar-header">
							<a class="navbar-brand" href="#" rel="home">						
				 	 <img 	alt="VulkanLAN"
			 				src="./tpl/VulkanLAN_bstrap3/images/VulkanLAN_logo.jpg">
		 			</a>
			{#FOREACH $level0.menu}
			{#FOREACH $items}			
			{#IF $id==8}
				{#LOAD LoadMenu(%id,1) $sub inc/inc.menu.php}
				{#FOREACH $sub.menu}
				{#FOREACH $items}
				{#IF $id==93}
				<!--Get the link from the mainpage and set it on the logo to get back to the start page-->
				<!--id 8 -> menu lanparty-->
				<!--id 93 -> submenu lan-info-->

				{#END}
				{#END}
				{#END}
			{#END}						
			{#END}
			{#END}			
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" onclick="javascript:void(0);">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbar">
			<ul class="nav navbar-nav pull-right">
				<li>
					{#VAR sub=login file=inc.page.login.tpl}
				</li>
			</ul>
			<ul class="nav navbar-nav">				
				{#FOREACH $level0.menu}
				{#FOREACH $items}
				<li class="dropdown">
					<a 	href="{$link}"
						class="dropdown-toggle"
						data-toggle="dropdown"
						title="{%description}">
						<span class="{$font_image}"></span>					
						{%caption}
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
					{#LOAD LoadMenu(%id,1) $sub inc/inc.menu.php}
					{#FOREACH $sub.menu}
					{#FOREACH $items}
							{#IF is_object($frame)}<li role="presentation">{#TPL $frame}</li>
							{#ELSEIF !empty($text)}<li role="presentation">{$text}</li>
							{#ELSE}{#DBIMAGE $image}
							<li role="presentation">
							<a class="{#WHEN $active activenavitab navitab}" data-toggle="collapse" data-target="#second" href="{$link}" title="{%description}"{#WHEN "isset($use_new_wnd) && $use_new_wnd" " target=\"_blank\""}>
							<span class="{$font_image}"></span>&nbsp;{%caption}</a></li>
							{#END}
					  
					{#END}
					{#END}
					</ul>
				</li>
				{#END}
				{#END}
			</ul>			
		</div>
	</div>
</nav>

<div class="container-fluid">
	<div class="row">
	<div class="col-sm-3 col-md-2 col-lg-2 sidebar">	
		{#LOAD PageLoadFrame("'lanparty.php?frame=countdown'") $temp3}
		{#TPL $temp3}
		{#LOAD PageLoadFrame("'webmessage.php?frame=smallnewmessages'") $temp1}
		{#TPL $temp1}
		{#LOAD PageLoadFrame("'lanparty.php?frame=smalluserstatus'") $temp2}
		{#TPL $temp2}
		{#LOAD PageLoadFrame("'lanparty.php?frame=smallstatusbar'") $temp}
		{#TPL $temp}
	</div>

	<div class=".col-xs-12 col-sm-9 col-md-10 col-lg-10">
		<div class="panel panel-default"><!-- ERRORCELL --></div>
			{#WHEN "$caption != \"\"" <h1>%caption</h1> ""}							
			{#TPL $page}
		</div>
	</div>
</div>

<!-- Sponoren Anzeige - Position - Vorschlaege-->
<div class="navbar  navbar-static-bottom" id="footer">
	<div class="list-group">
		<ul class="list-group">
			<li class="list-group-item list-group-item-info"> Sponsor</li>
			<li class="list-group-item">{#VAR sub=banner file=inc.sponsor.tpl}</li>
		</ul>
	</div>
</div>
<!--Ende Sponoren Anzeige-->

<div class="well" align="center">
	<footer>
		{$copyright} | Design by <a href="https://www.vulkanlan.at">VulkanLAN</a>.
	</footer>
</div>
			<!-- Bootstrap core JavaScript
		    ================================================== -->
		    <!-- Placed at the end of the document so the pages load faster -->
		    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
			<script src="./tpl/VulkanLAN_bstrap3/jquery_bracket/jquery-3.2.1.min.js" type="text/javascript"></script>
		    <script src="./tpl/VulkanLAN_bstrap3/dist/js/bootstrap.min.js"></script>
		   <!--  <script src="./tpl/VulkanLAN_bstrap3/assets/js/docs.min.js"></script> -->
		    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		    <script src="./tpl/VulkanLAN_bstrap3/assets/js/ie10-viewport-bug-workaround.js"></script>
			<!-- jquery bracket tournament plugin -->
			<script src="./tpl/VulkanLAN_bstrap3/jquery_bracket/jquery.bracket.min.js" type="text/javascript"></script>

	</body>
</html>
{#END}
<!-- Error Ausgabe - fuer Troubleshooting -->
{#SUB errorcell}
{#IF count($errors) > 0}
<h2>{§Fehler}</h2>
<p class="errorcell">
{#FOREACH $errors $err}
- {$err}<br/>
{#END}
</p>
{#END}
{#IF count($notices) > 0}
<h2>{§Nachricht}</h2>
<p class="noticecell">
{#FOREACH $notices $msg}
- {$msg}<br/>
{#END}
</p>
{#END}
{#END}
