{! $Id: webmessage.tpl 1415 20015-01-29 13:56:00Z edit by naaux $ }

{#SUB header}
<div class="panel panel-default">
  <a href="webmessage.php?frame=owner&amp;id={$user_id}"{#WHEN "$frame == 'owner'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-default" role="button"><i class="fa fa-inbox"></i>&nbsp;Posteingang</a> 
  <a href="webmessage.php?frame=sender&amp;id={$user_id}"{#WHEN "$frame == 'sender'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-default" role="button"><i class="fa fa-upload"></i>&nbsp;Postausgang</a> 
  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}"{#WHEN "$frame == 'sendmessage'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-default" role="button"><i class="fa fa-pencil"></i>&nbsp;neue Nachricht</a>
</div>
 {#END}

{#SUB default}
{#VAR sub=header}
{#END}

{#SUB owner}
{#VAR sub=header}
<div class="panel panel-default">
	<div class="row">
		<div class="visible-md visible-lg col-md-2 col-lg-2" style="padding-right: 5px;">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa  fa-folder-o fa-1x"></i>&nbsp;{§Ordner}</div>
					<div class="list-group">
						<a href="webmessage.php?frame=owner{#IF !empty($user_id)}&amp;id={$user_id}{#END}" class="list-group-item"><i class="fa  fa-folder fa-1x"></i>&nbsp; {§Posteingang}</a>
						{#FOREACH $folders $afoldername $afolderid}
						<a href="webmessage.php?frame=owner&amp;folder_id={$afolderid}{#IF !empty($user_id)}&amp;id={$user_id}{#END}" class="list-group-item"><i class="fa  fa-folder fa-1x"></i>&nbsp; {%afoldername}</a>
						{#END}
						<p align="center"><span style="vertical-align:middle;">
						<a href="webmessage.php?frame=editfolder&amp;id={$user_id}" class="btn btn-xs btn-default" role="button">{§Ordner}{§verwalten}</a>
						</span></p>
					</div>
			</div>
		</div>
		<div class=".col-xs-12 col-sm-12 col-md-10 col-lg-10" style="padding-left: 5px;">
			<!-- Default panel contents -->
			<!-- Posteingang -->
			<div class="panel panel-warning">
				<div class="panel-heading">Nachrichten</div>
				<div class="panel-body">
				<!-- Table -->
				{#TABLE $messages width="100%" maxrows=20 class="table"}
				  {#COL Sender $sender width="120" align="center"}<a href="user.php?frame=viewsubject&amp;id={$sender_id}">{%sender}</a>
				  {#COL Gesendet $date width="100" align="center"}{#DATE $date}
				  {#COL Betreff %subject}
					<a {#WHEN "$status == 'unread'" "class=\"important\" "}{#WHEN "$status != 'processed'" "style=\"font-weight:bold;\" "}href="webmessage.php?frame=viewmessage&amp;id={$user_id}&amp;message={$id}&amp;box=owner">{%subject}</a>
				  {#COL "" title="Antworten" width="20" align="center"}
					<b><a {#WHEN $is_replay "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=replay&amp;message_id={$id}"><i class="fa fa-mail-reply fa-2x"></i></a></b>
				  {#COL "" title="Weiterleiten" width="20" align="center"}
					<b><a {#WHEN $is_oforward "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=oforward&amp;message_id={$id}"><i class="fa fa-mail-forward fa-2x"></i></a></b>  
				 {#OPTION L&ouml;schen delete}
				 {#INPUT type hidden owner}
				  {#OPTION Als setstatus}
				  {#INPUT status dropdown param="array(read=gelesen unread=ungelesen processed=bearbeitet)"} &nbsp;markieren<br />
				  {#OPTION Nach movetofolder}
				  {#INPUT folder_id dropdown param=$allfolders} &nbsp;verschieben
				 {#END}
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB sender}
{#VAR sub=header}
<div class="panel panel-primary">
		<!-- Postausgang -->
		<div class="panel-heading">Nachrichten</div>
		<div class="panel-body">
			<!-- Table -->
			{#TABLE $messages width="100%" maxrows=20 class="table"}
				  {#COL Empf&auml;nger %owner width="120" align="center"}<a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>
				  {#COL Gesendet $date width="100" align="center"}{#DATE $date}
				  {#COL Betreff %subject}    
					<b><a href="webmessage.php?frame=viewmessage&amp;id={$user_id}&amp;message={$id}&amp;box=sender">{%subject}</a></b>
					{#IF !empty($processor_id) and ($sender_id != $processor_id)}<span style="color:gray">von</span> <a href="user.php?frame=viewsubject&amp;id={$processor_id}">{%processor}</a>{#END}
				  {#COL "" title="Noch einmal senden" width="16" align="center"}
					<b><a {#WHEN $is_resend "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=resend&amp;message_id={$id}"><i class="fa fa-repeat fa-2x"></i></a></b>
				  {#COL "" title="Weiterleiten" width="16" align="center"}
					<b><a {#WHEN $is_sforward "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=sforward&amp;message_id={$id}"><i class="fa fa-mail-forward fa-2x"></i></a></b>
				  {#OPTION L&ouml;schen delete}{#INPUT type hidden sender} 
			{#END}
		</div>
</div>
{#END}

{#SUB editfolder}
<div class="panel panel-info">
	<div class="panel-heading">
		<div class="row">
				<div class="col-xs-6" align="left"><i class="fa  fa-folder-o fa-1x"></i>&nbsp; Ordner {§hinzuf&uuml;gen}/{§bearbeiten}</div>
				<div class="col-xs-6" align="right">
				  <a href="webmessage.php?frame=owner{#IF !empty($user_id)}&amp;id={$user_id}{#END}"class="btn btn-default"><i class="fa fa-arrow-circle-left fa-1x"></i>{§zur&uuml;ck}</a>
				</div>
			</div>
	</div>
	{#FORM folder}
	{#INPUT folder_id hidden $folder_id}
	<div class="panel-body">
		<form class="form-horizontal">
			  <div class="form-group">
				<label for="Folder" class="col-xs-2 control-label">Name:</label>
				<div class="col-xs-10">
					{#INPUT foldername string}
				</div>
			  </div>
		</form>
		<div class="col-xs-12" align="left">
		{#SUBMIT anlegen a}
		{#END}
		</div>
	</div>
</div>
	{#IF !empty($folders)}
	{#FORM delfolder}
<div class="panel panel-danger">
	<div class="panel-heading">
		<div class="row">
				<div class="col-xs-12" align="left"><i class="fa  fa-folder-o fa-1x"></i>&nbsp; {§Ordner}{§l&ouml;schen}</div>
			</div>
	</div>
	<div class="panel-body">
		<form class="form-horizontal">
			<div class="form-group">
				<label for="Folder" class="col-xs-3 control-label">Ordner ausw&auml;hlen</label>
				<div class="col-xs-9">
					{#INPUT folder_id dropdown $folder_id param=$folders}
				</div>
			</div>
		</form>
		<div class="col-xs-12" align="left">
		{#SUBMIT l&ouml;schen l}
		{#END}
		</div>
	</div>
	{#END}	
</div>
{#END}

{#SUB sendmessage}
{#VAR sub=header}
{#FORM message}
<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-6" align="left">Nachrichten</div>
				<div class="col-xs-6" align="right">
				  <button type="submit" class="btn btn-default">Senden</button>
				  <span class="btn btn-default">{#BACKLINK Zur&uuml;ck}</span>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Empf&auml;nger:</label>
				<div class="col-sm-10">
					{#INPUT receiver string $receiver}  <i>(UserID, Nickname oder Email-Addresse)</i>
				</div>
			  </div>
			  <div class="form-group">
				<label for="inputSubject3" class="col-sm-2 control-label">Betreff</label>
				<div class="col-sm-10">
					{#INPUT subject longstring $subject allowempty=0}
				</div>
			  </div>
			  <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				  <div class="checkbox">
					<label>
					  {#INPUT append_signature checkbox $append_signature} Signatur anf&uuml;gen
					</label>
				  </div>
				</div>
			  </div>
			</form>
		</div>
		<div class="panel-body">
			<textarea class="form-control" rows="3"></textarea>
		</div>
</div>
{#INPUT source_id hidden $source_id}
{#INPUT source_type hidden $source_type}
{#END}
{#END}

{#SUB viewmessage}
{#VAR sub=header}
<!-- Nachricht -->
<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-2" align="left">Nachrichten</div>
				<div class="col-xs-10" align="right">
				  {#IF $box == "owner"}
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=replay&amp;message_id={$id}" class="btn btn-default" role="button"><i class="fa fa-mail-reply"></i>Antworten</a>
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=oforward&amp;message_id={$id}"  class="btn btn-default" role="button"><i class="fa fa-mail-forward"></i>Weiterleiten</a>
					  <span class="btn btn-default btn-sm">{#ACTION L&ouml;schen delete "webmessage.php?frame=owner&id=$user_id" params="array(ids[]=$id type=owner)" formattrs="array(style=display:inline;)"}</span>
					  {#IF $status != "processed"}
						<span class="btn btn-default btn-sm">{#ACTION bearbeitet setstatus params="array(ids[]=$id status=processed)" formattrs="array(style=display:inline;)"}</span>
					  {#ELSE}
						<span class="btn btn-default btn-sm">{#ACTION unbearbeitet setstatus params="array(ids[]=$id status=read)" formattrs="array(style=display:inline;)"}</span>
					  {#END}
					{#ELSEIF $box == "sender"}
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=resend&amp;message_id={$id}" class="btn btn-default" role="button"><i class="fa fa-repeat"></i>Nochmals senden</a>
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=sforward&amp;message_id={$id}" class="btn btn-default" role="button"><i class="fa fa-mail-forward"></i>Weiterleiten</a>
					  <span class="btn btn-default btn-sm">{#ACTION L&ouml;schen delete "webmessage.php?frame=sender" params="array(ids[]=$id type=sender)" formattrs="array(style=display:inline;)"}</span>
					{#END}
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				 <div class="col-xs-2" align="left">Status:</div>
				 <div class="col-xs-10">
									{#IF $status == "processed"}
										bearbeitet
									{#ELSEIF $status == "unread"}
										ungelesen
									{#ELSEIF $status == "read"}
										gelesen
									{#ELSE}
										unbekannt
									{#END}
								
						</div>
			</div>
			<div class="row">
				<div class="col-xs-2" align="left">Absender:</div>
					<div class="col-xs-10">
						<a href="user.php?frame=viewsubject&amp;id={$sender_id}">{%sender}</a>
							{#IF $processor_id}(<span style="color:gray">bearbeitet von</span> 
						<a href="user.php?frame=viewsubject&amp;id={$processor_id}">{%processor}</a>){#END}
					</div>
			</div>	
			<div class="row">
				<div class="col-xs-2" align="left">Empf&auml;nger:</div>
					<div class="col-xs-10">
						 <a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>
					</div>
			</div>
			<div class="row">
				<div class="col-xs-2" align="left">Betreff</div>
					<div class="col-xs-10">
						{%subject}
					</div>
			</div>
		</div>
		<div class="panel-footer">
			<span>{$text}</span>
		</div>
		<div class="panel panel-default">
		{#IF !empty($related)}
			<div class="panel-heading">Zugeh&ouml;rige Nachrichten:</div>
			 <ul class="list-group">{#FOREACH $related}
			  <li class="list-group-item">
				{#IF $source_type == "replay"}Antwort{#ELSEIF $source_type == "resend"}Nochmals gesendet{#ELSE}Weitergeleitet{#END}
				an <a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>:
				<b><a href="webmessage.php?frame=viewmessage&amp;message={$id}&amp;box=sender&amp;id={$user_id}">{%subject}</a></b>
			  </li>  
			{#END}</ul>
		  {#END}
		</div>
</div>
{#END}

{#SUB contact}
{#FORM contact}
<div class="panel panel-primary">
		<div class="panel-body">
		{#TPL $text}
		</div>
		{#IF !$loggedin}
		<div class="alert alert-warning">
		<b>Wichtig:</b> Wenn du auf dieser Seite bereits einen 
		Account hast, dann logge dicht mit diesem ein, bevor du die Nachricht schreibst.
		</div>
		{#END}
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-12" align="left">Nachricht</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-2" align="left">Name:</div>
					<div class="col-xs-10">
						{#IF $loggedin}{%name}{#ELSE}{#INPUT name string allowempty=0}{#END}
					</div>
			</div>
			<div class="row">
				<div class="col-xs-2" align="left">EMail:</div>
					<div class="col-xs-10">
						{#IF $loggedin}{%email}{#ELSE}{#INPUT email email allowempty=0}{#END}
					</div>
			</div>	
			<div class="row">
				<div class="col-xs-2" align="left">Betreff:</div>
					<div class="col-xs-10">
						{#INPUT subject longstring $test}
					</div>
			</div>
		</div>
		<div class="panel-body">
			 {#INPUT message document allowempty=0}
		</div>
		<div class="panel-footer">
			<div class="row">
					{#INPUT Sicherheitsabfrage captcha}
				<div class="form-group col-sm-12">
					<label for="Bildtext" class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
						{#SUBMIT Absenden}
					</div>
				</div>
			</div>
		</div>
		
</div>
{#INPUT prefix hidden $prefix}
{#END}
{#END}

{#SUB smallnewmessages}
<!-- in ein Div verpackt - edit naaux -->
 <?php 
   $counter = 0;
 ?>
 {#FOREACH $items}<?php $counter++; ?>{#END}
<div class="panel panel-yellow">
  <div class="panel-heading">
     <div class="row">
        <div class="col-xs-3">
            <i class="fa fa-comments fa-5x"></i>
        </div>
          <div class="col-xs-9 text-right">
            <div class="huge"><?php echo $counter; ?></div>
                 <div>New Messages!</div>
           </div>
     </div>
    </div>
    
     <div class="panel-footer">
        <span class="pull-left">
		{#FOREACH $items}
			<nobr>
		  {#IF $status=='unread'}
			<span class="text-warning"><i class="fa fa-envelope"></i></span>
			<a class="important" href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">{%subject} &nbsp;<i class="fa fa-arrow-circle-right"></i></a><br />
		  {#ELSE}
		  <!-- old <img src="{#STATICFILE images/webmessage/message_read.png}" width="15" height="10" alt=""/> -->
			<span class="text-warning"><i class="fa fa-envelope-o"></i></span>
			<a href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">{%subject} &nbsp;<i class="fa fa-arrow-circle-right"></i></a><br /> 
		  {#END}
		  </nobr>
		{#END}</span>
          <span class="pull-right"></span>
        <div class="clearfix"></div>
     </div>
</div>
{#END}
