<?php

/**
 * @author Matthias Gro&szlig;
 * @version $Id: fsys.php 1311 2006-09-18 16:36:27Z scope $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class FeatureAccountSys extends Page {

	var $Right_FeatureSysView = "featuresystem_view";
	var $Right_AdminView = "featuresystem_admin";

	function frameDefault($get, $post) {
		global $User;

		$User->requireRight($this->Right_FeatureSysView);
		$this->Caption = "Feature-Anfragen";

		$types = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_featuresys_types`;", "id");
		$orders = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE `user_id` = '{$User->id}';", "id");

		foreach ($orders as $o) {
			$account["id"] = $types[$o["accounttype"]]["id"];
			$account["name"] = $types[$o["accounttype"]]["name"];
			$account["description"] = $types[$o["accounttype"]]["description"];
			$account["free"] = $this->getAvailable($o["accounttype"]);
			$account["status"]["class"] = $o["status"];
			$account["status"]["name"] = ($o["status"] == "accepted") ? "Freigeschalten" : (($o["status"] == "denied") ? "Abgelehnt" : "Angefragt");
			unset ($types[$o["accounttype"]]); // Feature entfernen
			$r["accounts"][] = $account;
		}

		// Restliche (nicht angefragte Features) hinzuf&uuml;gen

		foreach ($types as $t) {
			$account["id"] = $t["id"];
			$account["name"] = $t["name"];
			$account["description"] = $t["description"];
			$account["free"] = $this->getAvailable($t["id"]);
			$account["status"]["class"] = "requestable";
			$account["status"]["name"] = "Kein Account";
			$r["accounts"][] = $account;
		}

		if (!empty ($r["accounts"]))
			asort($r["accounts"]); // Sonst wechseln die Eintr&auml;ge beim Statuswechsel die Position

		$r["admin"] = $User->hasRight($this->Right_AdminView);

		return empty ($r) ? array () : $r;
	}

	function actionRequestFeature($a) {
		global $User;
		$User->requireRight($this->Right_FeatureSysView);

		if (empty ($a["ids"]))
			return true;

		$reqs = MysqlReadCol("SELECT `id`, `accounttype` FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE `user_id` = '{$User->id}';", "accounttype", "id");

		foreach ($a["ids"] as $k => $v) {
			if (array_search($v, $reqs))
				unset ($a["ids"][$k]);
			elseif ($this->getAvailable($v) < 1) {
				unset ($a["ids"][$k]);
				$key = MysqlReadField("SELECT `name` FROM `" . TblPrefix() . "flip_featuresys_types` WHERE `id` = '$v';");
				trigger_error_text("Von '$key' sind nicht mehr genug verf&uuml;gbar!", E_USER_WARNING);
			}
		}

		if (empty ($a["ids"]))
			return true;

		$insert = "";

		foreach ($a["ids"] as $id) {
			$sep = empty ($insert) ? "" : ",";
			$insert .= "$sep('{$User->id}', 'requested', '$id')";
		}

		if (MysqlWrite("INSERT INTO `" . TblPrefix() . "flip_featuresys_accounts` (user_id, status, accounttype) VALUES $insert;")) {
			include_once ("mod/mod.sendmessage.php");
			$types = MysqlReadCol("SELECT `id`,`msg_requested` FROM `" . TblPrefix() . "flip_featuresys_types`;", "msg_requested", "id");
			foreach ($a["ids"] as $id)
				if (!empty ($types[$id]))
					SendSysMessage($types[$id], $User, "webmessage");
		}
	}

	function actionUnRequestFeature($g) {
		global $User;
		$User->requireRight($this->Right_FeatureSysView);
		if (!empty ($g["ids"])) {
			$del = implode_sqlIn($g["ids"]);
			MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE `accounttype` IN ($del) AND `user_id` = '{$User->id}';");
		}
	}

	function frameAccountsAdmin($g) {
		global $User;

		$User->requireRight($this->Right_AdminView);
		$this->Caption = "Accountadministration";

		switch (strtolower($g["showonly"])) {
			case "requested" :
				$where = "WHERE `status` = 'requested'";
				break;
			case "accepted" :
				$where = "WHERE `status` = 'accepted'";
				break;
			case "denied" :
				$where = "WHERE `status` = 'denied'";
				break;
			default :
				$where = "";
		}

		if (!empty ($g["ftype"]) && ($g["ftype"] != -1)) {
			if (empty ($where))
				$where = "WHERE `accounttype` = '$g[ftype]'";
			else
				$where .= " AND `accounttype` = '$g[ftype]'";
		}

		$r["showonly"] = !empty ($g["showonly"]) ? strtolower($g["showonly"]) : "";

		$types = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_featuresys_types`;", "id");
		$orders = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_featuresys_accounts` $where;", "id");
		$users = array ();

		foreach ($orders as $o) {
			if (!in_array($o["user_id"], $users)) {
				$u_subject = CreateSubjectInstance($o["user_id"]);
				$users[$o["user_id"]] = $u_subject->name;
			}

			$request["id"] = $o["id"];
			$request["owner"] = $users[$o["user_id"]];
			$request["name"] = $types[$o["accounttype"]]["name"];
			$request["status"]["class"] = $o["status"];
			$request["status"]["name"] = ($o["status"] == "accepted") ? "Freigeschaltet" : (($o["status"] == "denied") ? "Abgelehnt" : "Angefragt");
			$r["accounts"][] = $request;
		}

		$r["filtertypes"] = array (
			"Alle" => "-1"
		);
		$r["ftype_f"] = !empty ($g["ftype"]) ? "&ftype=$g[ftype]" : "";

		foreach ($types as $t)
			$r["filtertypes"] = array_merge($r["filtertypes"], array (
				$t["name"] => $t["id"]
			));

		$r["filtertypes"] = array_flip($r["filtertypes"]); // Workaround f&uuml;r array_merge...

		return empty ($r) ? array () : $r;
	}

	function actionAcceptRequest($g) {
		global $User;
		$User->requireRight($this->Right_AdminView);
		$this->changeStatus($g["ids"], "accepted");
	}

	function actionDenyRequest($g) {
		global $User;
		$User->requireRight($this->Right_AdminView);
		$this->changeStatus($g["ids"], "denied");
	}

	function actionDeleteRequest($g) {
		global $User;
		$User->requireRight($this->Right_AdminView);
		if (!empty ($g["ids"])) {
			$del = implode_sqlIn($g["ids"]);
			MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE `id` IN($del);");
		}
	}

	function frameEditFeature($g) {
		global $User;
		$User->requireRight($this->Right_AdminView);
		if ($g["id"] == "new") {
			$this->Caption = "Neues Feature erstellen";
			$r["name"] = "Neues Feature";
			$r["description"] = "";
			$r["maxcount"] = "0";
			$r["id"] = 0;
			$r["msg_denied"] = "-1";
			$r["msg_accepted"] = "-1";
			$r["msg_requested"] = "-1";
		} else {
			$r = MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_featuresys_types` WHERE `id` = '$g[id]';");
			$this->Caption = "Feature bearbeiten";
		}
		$r["msgs"] = MysqlReadCol("SELECT `name`,`subject` FROM `".TblPrefix()."flip_sendmessage_message` WHERE `type` = 'sys';", "subject", "name");
		$r["msgs"] = array_merge(array (
			"-1" => "Keine"
		), $r["msgs"]);
		return $r;
	}

	function submitEditFeature($g) {
		global $User;
		$User->requireRight($this->Right_AdminView);
		if (MysqlWriteByID(TblPrefix() . "flip_featuresys_types", $g, $g["id"])) {
			LogAction("Das Feature '$g[name]' wurde dem Featuresystem hinzugef&uuml;gt!");
			return true;
		} else {
			trigger_error_text("Ein Feature konnte dem Featuresystem nicht hinzugef&uuml;gt werden!", E_USER_ERROR);
			return false;
		}
	}

	function actionDeleteType($g) {
		global $User;
		$User->requireRight($this->Right_AdminView);
		$this->NextPage = "fsys.php";
		if (MysqlDeleteByID(TblPrefix() . "flip_featuresys_types", $g["id"])) {
			LogAction("Ein Featuretyp wurde gel&ouml;scht!");
			MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE `accounttype` = '$g[id]';");
		} else
			trigger_error_text("Ein Feature konnte aus dem Featuresystem nicht entfernt werden!", E_USER_ERROR);
	}

	function changeStatus($ids, $status) {
		if (!empty ($ids)) {
			$in = implode_sqlIn($ids);
			if (MysqlWrite("UPDATE `" . TblPrefix() . "flip_featuresys_accounts` SET `status` = '$status' WHERE `id` IN($in);")) {
				$accounts = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE `id` IN ($in);", "user_id");
				$types = MysqlReadCol("SELECT `id`,`msg_$status` FROM `" . TblPrefix() . "flip_featuresys_types`;", "msg_$status", "id");
				include_once ("mod/mod.sendmessage.php");
				foreach ($accounts as $a) {
					if (!empty ($types[$a["accounttype"]]))
						SendSysMessage($types[$a["accounttype"]], CreateSubjectInstance($a["user_id"]), "webmessage");
				}
			} else {
				trigger_error_text("Das setzten der IDs \"$in\" auf $status schlug fehl!");
			}
		}
	}

	function getAvailable($tid) {
		$cnt = MysqlReadField("SELECT COUNT(`id`) AS `cnt` FROM `" . TblPrefix() . "flip_featuresys_accounts` WHERE accounttype='$tid' AND `status` = 'accepted';");
		$max = MysqlReadField("SELECT `maxcount` FROM `" . TblPrefix() . "flip_featuresys_types` WHERE id='$tid';");
		return $max - $cnt;
	}
}

RunPage("FeatureAccountSys");
?>
