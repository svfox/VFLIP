<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: rawdata.php 1351 2007-01-30 13:36:09Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");

function EnabledChildCount($Group) {
	static $disabled;
	if (!isset ($disabled)) {
		$disabled = array ();
		foreach (GetSubjects("user", array (
				"id",
				"enabled",
				"name"
			)) as $id => $v)
			if ($v["enabled"] == "N")
				$disabled[$id] = $v["name"];
	}
	$i = 0;
	foreach ($Group->getChilds() as $id => $name)
		if (!isset ($disabled[$id]))
			$i++;
	return $i++;
}

/*
Ausgabe: aaa/bbb/ccc/ddd
    aaa: geplante Teilnehmerzahl
    bbb: Zahl der Teilnehmer, die bezahlt haben
    ccc: Zahl der Teilnehmer, die angemeldet sind
    ddd: Zahl der noch verf&uuml;gbaren Pl&auml;tze
*/
function FrameStatus() {
	$max = MysqlReadField("SELECT COUNT(*) FROM `" . TblPrefix() . "flip_seats_seats`;");
	$signed = EnabledChildCount(new Group("status_registered"));
	$paid = EnabledChildCount(new Group("status_paid"));

	echo implode('/', array (
		$max,
		$paid,
		$signed + $paid,
		$max - $signed - $paid
	));
}

/*
Ausgabe: aaa/bbb/ccc/ddd
    aaa: gesamt
    bbb: reserviert
    ccc: vorgemerkt
    ddd: frei
*/
function FrameSeats() {
	$max = MysqlReadField("SELECT COUNT(*) FROM `" . TblPrefix() . "flip_seats_seats`;");
	$taken = MysqlReadField("SELECT COUNT(*) FROM `" . TblPrefix() . "flip_seats_seats` WHERE (reserved = 'Y');");
	$nonfree = MysqlReadField("SELECT COUNT(*) FROM `" . TblPrefix() . "flip_seats_seats` WHERE (user_id <> 0);");

	echo join('/', array (
		$max,
		$taken,
		$nonfree - $taken,
		$max - $nonfree
	));
}

/*
Ausgabe: aaa
    aaa: der aktuellste Newstitel  
*/

function FrameLatestNews() {
	echo MysqlReadField("SELECT `title` FROM `" . TblPrefix() . "flip_news` WHERE (view_right = 0) ORDER BY `date` DESC LIMIT 1;");
}

/*
Ausgabe: aaa
    aaa: der aktuellste Newstitel  
*/

function FrameAuth() {
	$ident = (empty ($_GET["ident"])) ? $_POST["ident"] : $_GET["ident"];
	$pass = (empty ($_GET["pass"])) ? $_POST["pass"] : $_GET["pass"];
	$right = (empty ($_GET["right"])) ? $_POST["right"] : $_GET["right"];

	$err = "0\n\nnone\n";
	if (empty ($ident) or empty ($pass))
		return $err;
	if (!SubjectExists($ident, "user"))
		return $err;
	$u = new User($ident);
	if ($u->getProperty("password") == $pass) {
		if (empty ($right))
			return "{$u->id}\n{$u->name}\nnone\n";
		elseif ($u->hasRight($right)) return "{$u->id}\n{$u->name}\n{$right}\n";
	}
	return $err;
}

switch ($_GET["frame"]) {
	case ("status") :
		FrameStatus();
		break;
	case ("seats") :
		FrameSeats();
		break;
	case ("latestnews") :
		FrameLatestNews();
		break;
	case ("latestnews") :
		FrameLatestNews();
		break;
	case ("auth") :
		echo FrameAuth();
		break;
	default :
		echo "g&uuml;ltige Frames: status, seats, latestnews, auth";
}
?>
