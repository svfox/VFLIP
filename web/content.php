<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: content.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.form.php");

class ContentPage extends Page {
	var $EditRight = "content_edit";

	//php 7 public function __construct()
	//php 5 original function ContentPage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$User->requireRight($this->EditRight);
	}

	function frameDefault($get, $post) {
		global $User;
		$this->Caption = "Content";
		$dat = MysqlReadArea("
		      SELECT g.* FROM `" . TblPrefix() . "flip_content_groups` g
		        WHERE " . $User->sqlHasRight("g.view_right") . "
		        ORDER BY g.name;
		    ", "name");
		$size = 0;
		$images = MysqlReadArea("
		      SELECT g.name, COUNT(i.id) AS `images`, SUM(LENGTH(i.data)) AS `imagesize`
		        FROM `" . TblPrefix() . "flip_content_groups` g, `" . TblPrefix() . "flip_content_image` i
		        WHERE ((i.group_id = g.id) AND " . $User->sqlHasRight("g.view_right") . ")
		        GROUP BY g.id;
		    ", "name");
		$texts = MysqlReadArea("
		      SELECT g.name, COUNT(t.id) AS `texts`, SUM(LENGTH(t.text)) AS `textsize`
		        FROM `" . TblPrefix() . "flip_content_groups` g, `" . TblPrefix() . "flip_content_text` t
		        WHERE ((t.group_id = g.id) AND " . $User->sqlHasRight("g.view_right") . ")
		        GROUP BY g.id;
		    ", "name");
		foreach ($dat as $k => $v) {
			$dat[$k]["images"] = isset($images[$k]) ? (int) $images[$k]["images"] : 0;
			$dat[$k]["texts"] =  isset($texts[$k])  ? (int) $texts[$k]["texts"]   : 0;
			$size += $dat[$k]["size"] = ($dat[$k]["imagesize"] = isset($images[$k]) ? $images[$k]["imagesize"] : 0) + ($dat[$k]["textsize"] = isset($texts[$k]) ? $texts[$k]["textsize"] : 0);
			$dat[$k]["editable"] = $x = $User->hasRight($v["edit_right"]);
			$dat[$k]["deletable"] = ($x and ($dat[$k]["texts"] == 0) and ($dat[$k]["images"] == 0)) ? true : false;
		}
		return array (
			"items" => $dat,
			"groupcount" => count($dat
		), "size" => $size,);
	}

	function frameEditGroup($get) {
		$this->Caption = "Content -> Gruppe Bearbeiten";
		if (empty ($get["id"]))
			return array (
				"name" => "Neu"
			);
		return MysqlReadRowByRight(TblPrefix() . "flip_content_groups", $get["id"], array (
			"view_right",
			"edit_right"
		));
	}

	function submitGroup($sub) {
		$id = MysqlWriteByRight(TblPrefix() . "flip_content_groups", $sub, array (
			"view_right",
			"edit_right"
		), $sub["id"]);
		if ($id)
			LogChange("Die <b>Kontent-Gruppe <a href=\"content.php?frame=viewgroup&amp;id=$id\">$sub[name]</a></b> wurde " . ((empty ($sub["id"])) ? "erstellt." : "bearbeitet."));
		return $id;
	}

	function actionDeleteGroup($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$id = addslashes($id);
				$name = MysqlReadField("SELECT `name` FROM `" . TblPrefix() . "flip_content_groups` WHERE (`id` = '$id');");
				if ((MysqlReadField("SELECT COUNT(`id`) FROM `" . TblPrefix() . "flip_content_image` WHERE (`group_id` = '$id');") == 0) AND (MysqlReadField("SELECT COUNT(`id`) FROM `" .
					TblPrefix() . "flip_content_text` WHERE (`group_id` = '$id');") == 0)) {
					if (MysqlDeleteByRight(TblPrefix() . "flip_content_groups", $id, array (
							"view_right",
							"edit_right"
						)))
						LogChange("Die Kontent-Gruppe <b>$name</b> wurde gel&ouml;scht.");
				} else
					trigger_error_text("Die Gruppe \"$name\" kann erst gel&ouml;scht werden, wenn sich in ihr weder Bilder noch Texte befinden.", E_USER_WARNING);
			}
	}

	function _sortCallback($a, $b) {
		return strnatcasecmp($a['name'], $b['name']);
	}

	function frameViewGroup($get) {
		global $User;
		if (empty ($get["id"]))
			trigger_error_text('Es wurde keine Gruppe angegeben', E_USER_ERROR);
		$r = array (
		'group' => MysqlReadRowByRight(TblPrefix() . 'flip_content_groups', $get['id'], 'view_right'));
		$this->Caption = 'Content -> '.$r['group']['name'];
		$id = addslashes($get['id']);

		// texte auslesen
		$r['texts'] = MysqlReadArea('
		      SELECT t.name,t.id,t.caption,t.edit_time,t.description,
		        t.edit_right, t.view_right, a.name AS author, LENGTH(t.text) AS `size`
		        FROM (`' . TblPrefix() . 'flip_content_text` t)
		          LEFT JOIN `' . TblPrefix() . "flip_user_subject` a ON (t.edit_user_id = a.id)
		        WHERE ((t.group_id = '$id') AND " . $User->sqlHasRight("t.view_right") . ')
		        ORDER BY t.name;
		    ');
		$r['textsize'] = 0;
		foreach ($r['texts'] as $k => $v)
			$r['textsize'] += $v['size'];

		// bilder auslesen
		$r['images'] = MysqlReadArea('
		      SELECT i.name,i.id,i.caption,i.edit_time,i.tn_width,i.tn_height,i.description,
		        i.view_right, i.edit_right, a.name AS author, LENGTH(i.data) AS `size` 
		        FROM (`' . TblPrefix() . 'flip_content_image` i)
		          LEFT JOIN `' . TblPrefix() . "flip_user_subject` a ON (i.edit_user_id = a.id)
		        WHERE ((i.group_id = '$id') AND " . $User->sqlHasRight("i.view_right") . ')
		        ORDER BY i.name;
		    ');
		$r['imagesize'] = 0;
		foreach ($r['images'] as $k => $v)
			$r['imagesize'] += $v['size'];

		$r['textcount'] = count($r['texts']);
		$r['imagecount'] = count($r['images']);
		$r['groups'] = MysqlReadCol('SELECT `id`,`name` FROM `' . TblPrefix() . 'flip_content_groups` WHERE ' . $User->sqlHasRight('view_right') . ' ORDER BY `name`;', 'name', 'id');
		unset ($r['groups'][$r['group']['id']]);
		return $r;
	}

	// *************** Text functions **************************  

	function frameEditText($get) {
		ArrayWithKeys($get, array('id','name','group_id'));
		$this->Caption = "Content -> Text " . ((empty ($get["id"])) ? "Erstellen" : "Bearbeiten");
		if (empty ($get["id"]))
			$r = array (
				"name" => (empty ($get["name"]
			)) ? "new" : $get["name"], "group_id" => $get["group_id"], "edit_right" => "edit_public_text");
		else
			$r = MysqlReadRowByRight(TblPrefix() . "flip_content_text", $get["id"], array (
				"view_right",
				"edit_right"
			));
		$r["groups"] = MysqlReadCol("SELECT `name`,`id` FROM `" . TblPrefix() . "flip_content_groups`;", "name", "id");
		//no text replacement, damit der Text editier bar ist mit einem WYSIWYG Editor, edit naaux
		//$r['text'] = str_replace('<br />', "\n", $r['text']);
		
		return $r;
	}

	function submitText($sub) {
		global $User;
		$sub["edit_user_id"] = $User->id;
		$sub["edit_time"] = time();
		
		//no text replacement, damit der Text editier bar ist mit einem WYSIWYG Editor, edit naaux
		//$sub['text'] = str_replace("\n", '<br />', $sub['text']);
		
		if (MysqlWriteByRight(TblPrefix() . "flip_content_text", $sub, array (
				"view_right",
				"edit_right"
			), $sub["id"]))
			return LogChange("Der <b>Content-Text <a href=\"text.php?name=$sub[name]\">$sub[name]</a></b> wurde " . ((empty ($sub["id"])) ? "erstellt." : "bearbeitet."));
		return false;
	}

	function actionDeleteText($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$name = MysqlReadFieldByID(TblPrefix() . "flip_content_text", "name", $id);
				if (MysqlDeleteByRight(TblPrefix() . "flip_content_text", $id, array (
						"view_right",
						"edit_right"
					)))
					LogChange("Der Content-Text <b>$name</b> wurde gel&ouml;scht.");
			}
	}

	function actionMoveText($post) {
		$ids = array ();
		if (is_array($post["ids"]))
			$ids = $post["ids"];
		$g = addslashes($post["group"]);
		MysqlWrite(
			"UPDATE `" . TblPrefix() . "flip_content_text` " . 
			"SET `group_id` = '$g' " . 
			"WHERE (`id` IN (" . implode_sql(',', $ids) . "))"
		);
	}

	// *************** Image functions **************************  

	function frameEditImage($get) {
		include_once ("mod/mod.image.php");
		$this->Caption = "Content -> Bild " . ((empty ($get["id"])) ? "Erstellen" : "Bearbeiten");
		if (empty ($get["id"]))
			$r = array (
				"name" => (empty ($get["name"]
			)) ? "new" : $get["name"], "group_id" => $get["group_id"], "edit_right" => "edit_public_text");
		else
			$r = MysqlReadRowByRight(TblPrefix() . "flip_content_image", $get["id"], array (
				"view_right",
				"edit_right"
			));
		;
		$r["types"] = GetSupportedImageTypes();
		$r["groups"] = MysqlReadCol("SELECT `name`,`id` FROM `" . TblPrefix() . "flip_content_groups`;", "name", "id");
		return $r;
	}

	function actionDeleteImage($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$name = MysqlReadFieldByID(TblPrefix() . "flip_content_image", "name", $id);
				if (MysqlDeleteByRight(TblPrefix() . "flip_content_image", $id, array (
						"view_right",
						"edit_right"
					)))
					LogChange("Das Content-Bild <b>$name</b> wurde gel&ouml;scht.");
			}
	}

	function actionMoveImage($post) {
		$ids = array ();
		if (is_array($post["ids"]))
			$ids = $post["ids"];
		$g = addslashes($post["group"]);
		MysqlWrite("UPDATE `" . TblPrefix() . "flip_content_image` SET `group_id` = '$g' WHERE (`id` IN (" . implode_sql($ids) . "))");
	}

	function submitImage($sub) {
		include_once ("mod/mod.imageedit.php");
		global $User;
		$r = $sub;
		unset ($r["image"]);
		$r["edit_user_id"] = $User->id;
		$r["edit_time"] = time();

		if (is_array($sub["image"])) {
			$i = new DataImage($sub["image"]["tmp_name"]);
			$inf = $i->getInfo();
			if (isset ($inf["error"])) {
				trigger_error_text("Die Daten wurden nicht gespeichert. Vermutlich ist das Bild besch&auml;digt oder in einem nicht unterst&uuml;tzten Format.", E_USER_WARNING);
				return false;
			}
			$id = $i->saveToDB($r["id"], $r, true);
		} else
			$id = MysqlWriteByRight(TblPrefix() . "flip_content_image", $r, array (
				"view_right",
				"edit_right"
			), $r["id"]);

		if ($id)
			return LogChange("Das <b>Content-Bild <a href=\"content.php?frame=editimage&amp;id=$id\">$r[name]</a></b> wurde " . ((empty ($sub["id"])) ? "erstellt." : "bearbeitet."));
		else
			return false;
	}

}

RunPage("ContentPage");
?>
