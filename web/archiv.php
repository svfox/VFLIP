<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: archiv.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class ArchivPage extends Page {
	var $EventListText = "archiv_eventlist";
	var $CreateEventsRight = "archiv_createevents";
	// Turnierb&auml;ume
	var $mod_tournament = "mod/mod.tournament.php";
	var $page_tournament = "tournament.php";
	var $groupname_for_trees = "Turniere";
	
	//php 7 public function __construct()
	//php 5 original function ArchivPage()
	function __construct() {
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		$this->groupname_for_trees = text_translate("Turniere");
	}
	
	function frameDefault($get, $post) {
		include_once ("inc/inc.text.php");
		global $User;
		return array (
			"text" => LoadText($this->EventListText,
			$this->Caption
		), "events" => MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_archiv_events WHERE " . $User->sqlHasRight("view_right") . " ORDER BY `time_start`;"), "edit" => $User->hasRight($this->CreateEventsRight));
	}

	/****************************** Event anzeigen/bearbeiten ***************************/

	function frameEvent($get) {
		include_once ("inc/inc.text.php");
		global $User;
		$id = escape_sqlData_without_quotes($get["id"]);
		$r = MysqlReadRowByRight(TblPrefix() . "flip_archiv_events", $id, "view_right");
		$this->Caption = "Archiv: $r[caption]";
		$r["event"] = $r["id"];
		$r["edit"] = $User->hasRight($r["edit_right"]);
		$x = null;
		$r["text"] = LoadText($r["text"], $x);
		//Artikel
		$r["groups"] = array ();
		foreach (MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_archiv_articles WHERE (`event_id` = '$id') ORDER BY `group`,`caption`;") as $v) {
			$g = $v["group"];
			if (!empty ($v["text"])) {
				$v["url"] = "archiv.php?frame=article&id=$v[id]&event=$id";
				$v["target"] = "";
			} else {
				$v["target"] = "_blank";
			}
			if (is_array($r["groups"][$g]))
				$r["groups"][$g][] = $v;
			else
				$r["groups"][$g] = array (
					$v
				);
		}
		//Teilnehmergruppe
		if (!empty ($r["participant_group_id"]))
			$r["groups"]["Facts"][] = array (
				"target" => "",
				"url" => "archiv.php?frame=participants&id=$r[id]&event=$id",
				"caption" => "Teilnehmer",
				"edit" => false
			);
		//Turnierb&auml;ume
		foreach(MysqlReadCol("SELECT id, caption FROM `".TblPrefix()."flip_archiv_trees` WHERE event_id='$id'", "caption", "id") AS $tid => $caption) {
			$r["groups"][$this->groupname_for_trees][] = array (
				"target" => "_blank",
				"url" => "archiv.php?frame=viewtree&id=$tid",
				"caption" => $caption,
				"edit" => false
			);
		}
		return $r;
	}

	function frameEditEvent($g) {
		global $User;
		$this->Caption = "Event bearbeiten";
		if (empty ($g["id"])) {
			$User->requireRight($this->CreateEventsRight);
			return $this->_defaultData();
		} else
			return MysqlReadRowByRight(TblPrefix() . "flip_archiv_events", $g["id"], array (
				"view_right",
				"edit_right"
			));
	}
	
	/**
	 * @access private
	 */
	function _defaultData() {
		return array (
				"edit_right" => GetRightID("admin"),
				"caption" => ConfigGet("lanparty_fulltitle"),
				"time_start" => ConfigGet("lanparty_date"),
				"time_end" => ConfigGet("lanparty_date") + 2*24*3600,
				"participant_group_id" => GetSubjectId("status_checked_out"),
				"text" => MysqlReadField("SELECT id FROM `".TblPrefix()."flip_content_text` WHERE name = 'menu_lanparty'", "id")
			);
	}
	
	function frameSaveCurrent($get) {
		global $User;
		$User->requireRight($this->CreateEventsRight);
		
		$r = $this->_defaultData();
		
		$this->Caption = $r["caption"];
		
		//participant group
		//status_* with most members
		$max = array("count"=>0, "id"=>0);
		$group_sizes = MysqlReadCol("SELECT parent_id, COUNT(*) FROM `".TblPrefix()."flip_user_groups` GROUP BY parent_id", "COUNT(*)", "parent_id");
		foreach(GetSubjects("group", array("id", "name"), "s.name LIKE 'status_%'") AS $group) {
			$member_count = $group_sizes[$group["id"]];
			$group["name"] .= " ($member_count)";
			if($member_count >= $max["count"]) {
				$max["count"] = $member_count;
				$max["id"] = $group["id"];
			}
			if($member_count > 0) {
				$r["groups"][$group["id"]] = $group["name"];
			}
		}
		if(!$max["id"] > 0) {
			//no users?
			trigger_error_text("Es gibt keine Status-Gruppe mit Mitgliedern!",E_USER_ERROR);
		}
		$r["participant_group_id"] = GetSubjectID("status_checked_out");
		
		return $r;
	}
	
	function submitCurrent($data) {
		global $User;
		$User->requireRight($this->CreateEventsRight);
		
		//defaults
		$data = array_merge($this->_defaultData(), $data);
		
		$archive_name = "archive_".date("Y-m-d");
		
		//participant group
		if($groupid = MysqlReadField("SELECT id FROM `".TblPrefix()."flip_user_subject` WHERE name=".escape_sqlData($archive_name)." AND type='group'", "id", true)) {
			//group already exists
			if($groupid != $data["participant_group_id"]) {
				trigger_error_text("Es gibt bereits eine Gruppe '$archive_name'. Diese wird benutzt!", E_USER_WARNING);
			}
			$data["participant_group_id"] = $groupid;
		} else {
			//copy group
			$group = CreateSubjectInstance($data["participant_group_id"], "group");
			if(!($newgroup = CreateSubject("group", $archive_name))) {
				trigger_error_text("Die Gruppe '$archive_name' konnte nicht erstellt werden!", E_USER_ERROR);
			}
			$newgroup->setProperty("description", text_translate("Kopie von ?1?", $group->name));
			foreach($group->GetChilds() AS $child_id=>$child_name) {
				$newgroup->addChild($child_id);
			}
			$data["participant_group_id"] = $newgroup->id;
		}
		
		//text
		$lanparty_text_name = $archive_name;
		$lanparty_text_new = MysqlReadRow("SELECT * FROM `".TblPrefix()."flip_content_text` WHERE name = ".escape_sqlData($lanparty_text_name), true);
		$lanparty_text = MysqlReadRow("SELECT * FROM `".TblPrefix()."flip_content_text` WHERE name = 'menu_lanparty'");
		//copy 'menu_lanparty'
		if(!is_array($lanparty_text_new)) {
			$lanparty_text["name"] = $lanparty_text_name;
			unset($lanparty_text["id"]);
			$textid = MysqlWriteByID(TblPrefix()."flip_content_text", $lanparty_text);
		} else {
			$textid = $lanparty_text_new["id"];
			//another one exists
			if($lanparty_text["text"] != $lanparty_text_new["text"]) {
				trigger_error_text("Es gibt bereits einen Archivtext Names '".$lanparty_text_new["name"]."'! Dieser wird verwendet.",E_USER_WARNING);
			}
		}
		$data["text"] = $textid; 
		
		//create event
		$newEventID = $this->submitEvent($data);
		
		//Turniere
		include_once($this->mod_tournament);
		foreach(TournamentGetTournaments() AS $aTournament) {
			if($aTournament["status"] == "end") {
				$this->submitTournament(array("tournamentid"=>$aTournament["id"], "event_id"=>$newEventID));
			}
		}
		return $newEventID;
	}

	function submitEvent($data) {
		global $User;
		if (empty ($data["id"])) {
			$User->requireRight($this->CreateEventsRight);
		}
		$r = MysqlWriteByRight(TblPrefix() . "flip_archiv_events", $data, array (
			"view_right",
			"edit_right"
		), $data["id"]);
		if ($r)
			LogChange("Im <b>Archiv</b> wurde das <b>Ereignis <a href=\"archiv.php?frame=event&amp;id=$r\">$data[caption]</a></b> " . ((empty ($data["id"])) ? "erstellt." : "bearbeitet."));
		return $r;
	}

	function actionDeleteEvent($post) {
		global $User;
		$e = MysqlReadRowByID(TblPrefix() . "flip_archiv_events", $post["id"]);
		$User->requireRight($e["edit_right"]);

		if (MysqlDeleteByID(TblPrefix() . "flip_archiv_events", $post["id"])) {
			MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_archiv_articles` WHERE (`event_id` = '$e[id]')");
			LogChange("Im <b>Archiv</b> wurde das Ereignis $e[caption] gel&ouml;scht.");
		}
	}

	/************************** Artikel anzeigen/bearbeiten ************************/

	function requireEventRight($id, $edit = false) {
		global $User;
		$right = ($edit) ? array (
			"view_right",
			"edit_right"
		) : "view_right";
		$r = MysqlReadRowByRight(TblPrefix() . "flip_archiv_events", $id, $right);
		if (!is_array($r))
			trigger_error_text("Das Event mit der ID $id existiert nicht, oder der aktuelle User (" . $User->name . ") hat nicht gen&uuml;gend Rechte es anzuzeigen.", E_USER_ERROR);
		return $r;
	}

	function frameArticle($get) {
		include_once ("inc/inc.text.php");
		global $User;
		$event = $this->requireEventRight($get["event"]);
		$r = MysqlReadRowByID(TblPrefix() . "flip_archiv_articles", $get["id"]);
		$this->Caption = "Archiv: $r[caption]";
		$x = null;
		$r["text"] = LoadText($r["text"], $x);
		if (!empty ($r["url"])) {
			$u = parse_url($r["url"]);
			$r["url"] = array (
				"caption" => ($u["host"] != "") ? $u["host"] : $r["url"],
				"url" => $r["url"]
			);
		}
		$r["edit"] = $User->hasRight($event["edit_right"]);
		return $r;
	}

	function frameEditArticle($g) {
		global $User;
		$this->Caption = "Artikel bearbeiten";
		$this->requireEventRight($g["event_id"], true);
		if (empty ($g["id"])) { 
			$r = array (
				"caption" => "neuer Artikel"
			);
			$r["groups"] = array();
		} else {
			$r = MysqlreadRowByID(TblPrefix() . "flip_archiv_articles", $g["id"]);
			$r["groups"] = MysqlReadCol("SELECT DISTINCT `group` FROM `".TblPrefix()."flip_archiv_articles` WHERE `event_id`=".escape_sqlData($g["id"]), "group", "group");
		}
		$r["groups"]["Facts"] = "Facts";
		return $r;
	}

	function submitArticle($data) {
		$event = $this->requireEventRight($data["event_id"]);
		$r = MysqlWriteByID(TblPrefix() . "flip_archiv_articles", $data, $data["id"]);
		if ($r)
			LogChange("Im <b>Archiv</b> von <b><a href=\"archiv.php?frame=event&amp;id=$data[event_id]\">$event[caption]</a></b> wurde der <b>Artikel <a href=\"archiv.php?frame=editarticle&amp;event_id=$data[event_id]&amp;id=$r\">$data[caption]</a></b> " . ((empty ($data["id"])) ? "erstellt." : "bearbeitet."));
		return $r;
	}

	/*********************************************** Turnierb&auml;ume **********************************/
	
	function frameViewTree($get) {
		$data = MysqlReadRowByID(TblPrefix()."flip_archiv_trees", $get["id"]);
		
		$this->requireEventRight($data["event_id"]);
		
		$this->Caption = $data["caption"];
		/*aenderung auf archivtree, edit by naaux */
		$this->TplFile = "tournament_archiv.tpl";
		$this->TplSub  = "archivtree";
		$r = Uncondense($data["data"]);
		$r["orga"] = false;
		unset($r["adminlinks"]);
		return $r;
	}
	
	/**
	 * F&uuml;gt dem Archiv einen Turnierbaum hinzu
	 * 
	 * Es werden die Daten welche f&uuml;r die Darstellung n&ouml;tig sind archiviert.
	 * Die Darstellung &uuml;bernimmt das Turniersystem.
	 * 
	 * @author loom
	 * @since 1397 - 14.05.2007
	 */
	function frameAddTree($get) {
		$this->requireEventRight($get["event_id"], true);
		
		$this->Caption = "Turnier ausw&auml;hlen";
		
		include_once($this->mod_tournament);
		
		$r = array("event_id" => $get["event_id"]);
		$r["tournaments"] = array();
		foreach(TournamentGetTournaments() AS $aTournament) {
			$r["tournaments"][$aTournament["id"]] = TournamentGetTournamentString($aTournament["id"]);
		}
		
		return $r;
	}
	
	function submitTournament($post) {
		$this->requireEventRight($post["event_id"], true);
		include_once($this->page_tournament);
		global $PageExists;
		$PageExists = false;
		$_GET["id"] = $post["tournamentid"];
		$tpage = new TournamentPage();
		$tid = $post["tournamentid"];
		
		if(TournamentIsValidID($tid)) {
			$t = array();
			$t["event_id"] = $post["event_id"];
			$t["data"] = Condense($tpage->frameTree(array()));
			$t["caption"] = $tpage->turnier->GetTournamentString($tid);
			if(MysqlWriteByID(TblPrefix()."flip_archiv_trees", $t))
				return true;
		}
		return false;
	}
	/*********************************************** alle Turnierdaten archivieren **********************************/
		
	function frameAddTournamentData($get) {
		$this->requireEventRight($get["event_id"], true);
		
		$this->Caption = "Turnier ausw&auml;hlen";
		
		include_once($this->mod_tournament);
		
		$r = array("event_id" => $get["event_id"]);
		$r["tournaments"] = array();
		foreach(TournamentGetTournaments() AS $aTournament) {
			$r["tournaments"][$aTournament["id"]] = TournamentGetTournamentString($aTournament["id"]);
		}
		
		return $r;
	}
	
	/*********************************************** Teilnehmer anzeigen ***************************/

	function frameParticipants($get) {
		$event = $this->requireEventRight($get["event"]);
		$this->Caption = "Archiv: Teilnehmer von \"$event[caption]\"";
		$group = $event["participant_group_id"];
		if (empty ($group))
			trigger_error_text("Dem Event wurde keine Gruppe zugewiesen welche die Teilnehmer enth&auml;lt.", E_USER_ERROR);
		$group = new Group($group);
		$users = $group->getChilds();
		return array (
			"count" => count($users
		), "users" => $users);
	}
}

RunPage("ArchivPage");
?>