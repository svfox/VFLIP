<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: index.php 1384 2007-03-15 23:20:53Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");

$nextpage = ConfigGet("page_index");

//wenn keine absolute URL, dann eine erstellen
if (strpos($nextpage, '://') === false) {
	//serveradresse ermitteln
	$host = trim($_SERVER["HTTP_HOST"], '/');
	$dir = trim(dirname($_SERVER["PHP_SELF"]), '/');
	if ($dir == "\\")
		$dir = "";
	$prot = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? "https" : "http";
	$port = ($_SERVER["SERVER_PORT"] != 80) ? ":$_SERVER[SERVER_PORT]" : "";

	//slash ans Verzeichnis anh&auml;ngen
	if (!empty ($dir))
		$dir .= '/';

	//wenn relativer pfad, verzeichnis davor schreiben
	if ($nextpage[0] != '/')
		$nextpage = "/$dir$nextpage";

	//URI zusammensetzen
	$nextpage = "$prot://$host$port$nextpage";
}

header("Location: $nextpage");
?>