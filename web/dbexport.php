<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: archiv.php 1054 2005-08-03 09:21:59Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class DBExportPage extends Page {
	var $CreateDumpRight = "dbexport_create_dump";

	function printInsert($Table, $Row) {
		foreach ($Row as $k => $v) {
			if (is_null($v))
				$Row[$k] = "NULL";
			elseif (is_numeric($v)) $Row[$k] = $v;
			elseif (preg_match('/[\x0-\x8\xB\xC\xE-\x1F]/', $v)) $Row[$k] = "0x".bin2hex($v);
			else
				$Row[$k] = "'".addcslashes($v, "\n\r\t\\'")."'";
		}
		echo "INSERT INTO `$Table` VALUES (".implode(",", $Row).");\n";
		flush();
	}

	function printCreateTable($Table) {
		echo "\n";
		echo "DROP TABLE IF EXISTS `$Table`;\n";
		echo MysqlReadField("SHOW CREATE TABLE $Table", "Create Table").";\n";
	}

	function printTable($Table) {
		$this->printCreateTable($Table);
		foreach (MysqlReadArea("SELECT * FROM $Table ORDER BY id;") as $row)
			$this->printInsert($Table, $row);
	}

	function printHeader() {
		echo "# SQL-Dump, erstellt vom FLIP (www.flipdev.org)\n";
		echo "# Datum: ".date("r")."\n";
		echo "# Server: ".$_SERVER["SERVER_NAME"]."\n";
		echo "# Script: ".$_SERVER["SCRIPT_NAME"]."\n";
		echo "\n";
	}

	function frameExport($get) {
		global $User;
		$User->requireRight($this->CreateDumpRight);
		ArrayWithKeys($get, array("mode"));
		switch ($get["mode"]) {
			case ("select") :
				$tables = $this->getSelectedTables($get);
				break;
			case ("exclude") :
				$tables = $this->getNotSelected($get);
				break;
			default :
				$tables = MysqlReadCol("SHOW TABLES;");
				break;
		}
		return $this->printTables($tables);
	}

	function printTables($tables) {
		$this->saveAs("flip_db_dump-".date("YmdHis").".sql");
		$this->printHeader();
		natcasesort($tables);
		foreach ($tables as $table)
			$this->printTable($table);
		return true;
	}

	function getSelectedTables($get) {
		ArrayWithKeys($get, array("ids"));
		$t = array ();
		if (is_array($get["ids"]))
			foreach ($get["ids"] as $table)
				$t[] = $table;
		return $t;
	}

	function getNotSelected($get) {
		$all = MysqlReadCol("SHOW TABLES;");
		ArrayWithKeys($get, array("ids"));
		if (!is_array($get["ids"]))
			return $all;
		$t = array ();
		foreach ($all as $table)
			if (!in_array($table, $get["ids"]))
				$t[] = $table;
		return $t;
	}

	function actionExportSelected($get) {
		ArrayWithKeys($get, array("ids"));
		$this->NextPage = EditURL(array ("frame" => "export", "ids" => $get["ids"], "mode" => "select"), "", false);
	}

	function actionExportNotSelected($get) {
		ArrayWithKeys($get, array("ids"));
		$this->NextPage = EditURL(array ("frame" => "export", "ids" => $get["ids"], "mode" => "exclude"), "", false);
	}

	function frameDefault($get, $post) {
		global $User;
		$User->requireRight($this->CreateDumpRight);
		$this->Caption = "Datenbankexport";
		return array ();
	}

	function frameSelectTables($get) {
		global $User;
		$User->requireRight($this->CreateDumpRight);
		$this->Caption = "Datenbankexport";
		$tables = MysqlReadArea("SHOW TABLE STATUS;");
		foreach ($tables as $k => $v)
			$tables[$k]["id"] = $v["Name"];
		return array ("tables" => $tables);
	}

}

RunPage("DBExportPage");
?>