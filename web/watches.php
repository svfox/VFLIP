<?php
/**
 * Die Watches-Page bietet einem User die M&ouml;glichkeit, seine
 * Watches einzusehen und zu bearbeiten
 * 
 * @author Matthias Gro&szlig;
 * @version $Id: watches.php 1702 2019-01-09 09:01:12Z scope $ edit by VulkanLAN
 * @copyright 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class WatchesPage extends Page {
	
	var $UseRight = "watches_use";
	
	//php 7 public function __construct()
	//php 5 original function WatchesPage()
	function __construct() {
		global $User;
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		$User->requireRight($this->UseRight);
	}
	
	function frameDefault($get) {
		global $User;
		$this->Caption = "Meine Watches";
		$watches = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_forum_watches` WHERE `owner_id` = '{$User->id}';");
		$threads = MysqlReadCol("SELECT `id`, `title` FROM `".TblPrefix()."flip_forum_threads`;","title","id");
		
		if(ConfigGet("watches_enabled") == "N") {
			$r['w_enabled'] = false;
			return $r;
		} else
			$r['w_enabled'] = true;
		
		foreach(GetSubjects("forum",array('id','name')) as $f_id => $f)
			$foren[$f_id] = $f['name'];
			
		foreach($watches as $w) {
			$r['watches'][] = array("id"   => $w['id'],
									"type" => ucfirst($w['type']), 
									"name" => (($w['type'] == "forum") ? $foren[$w['target_id']] : $threads[$w['target_id']]));
		}
		
		return empty($r) ? array() : $r;
	}
	
	function actionDeleteWatches($g) {
		if(!empty($g['ids'])) {
			$in = implode_sqlIn($g['ids']);
			if(!MysqlWrite("DELETE FROM `".TblPrefix()."flip_forum_watches` WHERE `id` IN ($in);"))
				return trigger_error_text("Die Watches konnten nicht gel&ouml;scht werden!",E_USER_ERROR);
			else {
				trigger_error_text("Die Watches wurden gel&ouml;scht!",E_USER_NOTICE);
				return true;
			}
		}
	}
	
	function actionAddWatch($g) {
		global $User;
		if(empty($g['id']))
			return trigger_error_text("Die ID des hinzuzuf&uuml;genden Watches wurde nicht &uuml;bergeben!",E_USER_ERROR);
		if(empty($g['type']))
			return trigger_error_text("Der Typ des hinzuzuf&uuml;genden Watches wurde nicht &uuml;bergeben!",E_USER_ERROR);

		$this->NextPage = "forum.php?frame=view".$g['type']."&id=".$g['id'];
		
		$v['target_id'] = escape_sqlData_without_quotes($g['id']);
		$v['type']		= escape_sqlData_without_quotes(strtolower($g['type']));
		$v['owner_id']	= $User->id;
		
		$on	   = (ucfirst($g['type']) == "Forum") ? "s Forum!" : "n Thread!";
		$where = "WHERE `owner_id` = '{$User->id}' AND `type` ='".$v['type']."' AND `target_id` = '".$v['target_id']."'";
		
		if(MysqlReadField("SELECT `id` FROM `".TblPrefix()."flip_forum_watches` $where;","id",true))
			return trigger_error_text("Du besitzt bereits einen Watch auf diese$on",E_USER_WARNING);
			
		// Bei einem Forumswatch alle enthaltenen Threadwatches l&ouml;schen, um Redundanzen zu vermeiden!
		if(strtolower($g['type']) == "forum") {
			$threads = MysqlReadCol("SELECT `id` FROM `".TblPrefix()."flip_forum_threads` WHERE `forum_id` = '".$v['target_id']."';","id");
			if(!empty($threads)) {
				$ids = implode_sqlIn($threads);
				MysqlWrite("DELETE FROM `".TblPrefix()."flip_forum_watches` WHERE `target_id` IN ($ids) AND `type` = 'thread';");				
			}
		}
		
		if(!MysqlWriteByID(TblPrefix()."flip_forum_watches",$v))
			return trigger_error_text("Ein Watch konnte nicht hinzugef&uuml;gt werden!",E_USER_ERROR);
		else {
			$msg = "Du besitzt jetzt einen Watch auf diese$on";
			trigger_error_text($msg,E_USER_NOTICE);
			return true;
		}
	}
}

RunPage("WatchesPage");

?>