<?php

/**
 * Sucht in Modulen, welche mod.search.MODUL.php heissen
 * in Suchmodul muss Klasse Search_MODUL vorhanden sein mit den Methoden Search() und SearchTitle()
 * diese Methoden liefern ein Array mit jeweils folgendne Elementen zur&uuml;ck: Titel, Link, (Teil von) Text
 * der Text wird vom jeweiligen modul entsprechend formatiert und angepasst (z.B. hervorhebungen, HTML-escaping)
 *
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.page.php');
require_once ('mod/mod.search.php');

class SearchPage extends Page {
	function framedefault($get, $post) {
		$this->Caption = 'Suche';
		
		ArrayWithKeys($get, array('titleonly','usedmodules','searchstring','mode'));
		
		$search = new Search($get['titleonly']);

		$r['modes'] = $search->modes;
		$r['time'] = 0;
		$r['usenextlink'] = false;
		
		$modules = $search->modules;
		
		foreach ($modules AS $amod => $file) {
			$r['modules'][] = array (
				'name' => $amod,
				'checked' => 
					(isset($get['usedmodules'][$amod]) && !empty($get['usedmodules'][$amod])) ? 1 : 0
			);
		}

		$link = null;
		$params = array();
		
		// Nextframelink - e.g. change the result's link to something else than the standard
		if(isset($get['nextframelink'])) {
			$nextFrameLink = base64_decode($get['nextframelink']);
			
			// Param contains something like "foo.php?frame=bar&userid={userid}"
			// Filter out the params and prepare them 
			if(preg_match('/{([a-zA-Z0-9\-]+)}/', $nextFrameLink, $params)) {
				$link = $nextFrameLink;
				unset($params[0]);
			}
			
			$r['nextframelink'] = $get['nextframelink'];
			$r['usenextlink'] = true;
		}
		
		// Searching
		$r['results'] = array ();
		if ($get['searchstring'] != "") {
			if (is_array($get['usedmodules'])) {
				$search->SetModules(array_keys($get['usedmodules']));
				$start = MicroSeconds();
				
				foreach ($search->GetResult($get['searchstring'], $get['mode']) AS $mod => $modresult) {
					if (empty ($modresult)) {
						continue; //Module die nichts zur&uuml;ckliefern
					}
					
					$results = array();
					
					foreach ($modresult as $aResult) {
						if($link != null) {
							$newlink = $link;
							
							foreach($params as $paramName) {
								if(isset($aResult[$paramName])) {
									$newlink = str_replace('{'.$paramName.'}', $aResult[$paramName], $newlink);
								}
							}
							
							$aResult['link'] = $newlink;
						}
						
						$results[] = array_merge(
							$aResult, 
							array ('mod' => $mod)
						);	
					}
					
					$r['results'] = array_merge($r['results'], $results);
				}
				
				$r['time'] = round((MicroSeconds() - $start), 3);
			} else {
				trigger_error_text(
					text_translate('Es wurde ein Suchbegriff angegeben, aber kein Modul ausgew&auml;hlt!'), 
					E_USER_WARNING
				);
			}
		}
		
		$r['count'] = count($r['results']);

		return $r;
	}
}

RunPage('SearchPage');
?>