<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: menu.php 1702 2019-01-09 09:01:12Z docfx $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class MenuPage extends Page {
	var $EditRight = "menu_edit";

	//php 7 public function __construct()
	//php 5 original function MenuPage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$User->requireRight($this->EditRight);
	}

	function getPath($id) {
		$id = addslashes($id);
		$level = 0;
		$path = array ();
		while (!empty ($id)) {
			$q = MysqlReadRow("
			  	   SELECT b.parent_item_id, b.level_index, i.id, i.caption 
			  	     FROM `".TblPrefix()."flip_menu_links` i, `".TblPrefix()."flip_menu_blocks` b
			  	     WHERE ((i.block_id = b.id) AND (i.id = '$id'));
			  	  ");
			$id = $q["parent_item_id"];
			$path[] = $q;
			$level ++;
			if (!empty ($q["level_index"])) {
				$level += $q["level_index"];
				$path[] = array ("id" => 0, "caption" => "???");
				return array ($level, $path);
			}
		}
		$path[] = array ("id" => "0", "caption" => "root");
		return array ($level, $path);
	}

	function frameDefault($get, $post) {
		global $User;
		//include_once("inc/inc.menu.php");
		//$this->ShowContentCell = false;
		$this->Caption = "Menu";
		ArrayWithKeys($get, array("id")); 
		list ($level, $path) = $this->getPath($get["id"]);
		$parent = addslashes($get["id"]);
		$blocks = MysqlReadArea("
			  SELECT * FROM `".TblPrefix()."flip_menu_blocks` 
			    WHERE (
			     ((`parent_item_id` = '$parent') AND (`level_index` = 0)) OR 
			     ((`parent_item_id` = 0) AND (`level_index` = '$level'))
			  ) ORDER BY `order`;
			", "id");
		if (!empty ($blocks)) {
			$ids = implode_sqlIn(array_keys($blocks));
			$items = MysqlReadArea("
					  SELECT l.*, COUNT(b.id) AS `used` FROM (`".TblPrefix()."flip_menu_links` l)
					    LEFT JOIN `".TblPrefix()."flip_menu_blocks` b ON (b.parent_item_id = l.id)
					    WHERE (l.block_id IN ($ids)) 
					    GROUP BY l.id 
					    ORDER BY l.order;
					");
			
			foreach ($items as $i) {
				$blocks[$i["block_id"]]["items"][] = $i;
			}
			
			$lastidb = 0;
			foreach ($blocks as $k => $i) {
				$blocks[$k]["lastid"] = $lastidb;
				$lastidb = $i["id"];
				$lastidi = 0;
				if (is_array($i["items"]))
					foreach ($i["items"] as $ki => $vi) {
						$blocks[$k]["items"][$ki]["lastid"] = $lastidi;
						$lastidi = $vi["id"];
					}
			}
		}
		return array ("path" => array_reverse($path), "blocks" => $blocks, "parent" => $get["id"], "level" => $level);
	}

	function frameEditBlock($get) {
		//ArrayWithKeys($get, array("id", "level_index", "parent_item_id")); 
		if (empty ($get["id"])) { // neuen datensatz erstellen
			$this->Caption = "Menu -> Block erstellen";
			$r = array ("caption" => "new Block", "order" => time());
			if (!isset($get["level_index"]))
				$r["parent_item_id"] = (isset($get["parent_item_id"])) ? $get["parent_item_id"] : null;
			else
				$r["level_index"] = $get["level_index"];
		} else { // vorhandenen datensatz laden
			$this->Caption = "Menu -> Block bearbeiten";
			$r = MysqlReadRowByID(TblPrefix()."flip_menu_blocks", $get["id"]);
		}
		if (!empty ($r["parent_item_id"]))
			list ($r["level"], $r["path"]) = $this->getPath(isset($r[$r["parent_item_id"]]) ? $r[$r["parent_item_id"]] : null);
		else
			$r["level"] = isset($r["level_index"]) ? intval($r["level_index"]) : 0;
			
		$r["parents"] = MysqlReadCol("SELECT l.id, CONCAT(b.`caption`, '-', l.`caption`) AS `caption` FROM `".TblPrefix()."flip_menu_links` l LEFT JOIN `".TblPrefix()."flip_menu_blocks` b ON b.id=l.block_id ORDER BY b.`caption`", "caption", "id");
		return $r;
	}

	function submitBlock($data) {
		if (!empty ($data["parent_item_id"]) and !empty ($data["level_index"]))
			unset($data["parent_item_id"]); // Statische Bl&ouml;cke haben keine Eltern ;)
			
		$r = MysqlWriteByID(TblPrefix()."flip_menu_blocks", $data, $data["id"]);
		if ($r != $data["id"])
			$action = "erstellt";
		else
			$action = "bearbeitet";
		LogChange("Der <strong>Men&uuml;block</strong> '".escapeHtml($data["caption"])."' wurde $action.");
		return $r;
	}

	function actionDeleteBlock($data) {
		if (MysqlReadField("SELECT COUNT(`id`) FROM `".TblPrefix()."flip_menu_links` WHERE (`block_id` = '".addslashes($data["id"])."');") > 0)
			trigger_error_text("Ein Men&uuml;block kann erst gel&ouml;scht werden, wenn er keine Men&uuml;eintr&auml;ge mehr hat.", E_USER_ERROR);
		$caption = MysqlReadFieldByID(TblPrefix()."flip_menu_blocks", "caption", $data["id"]);
		MysqlDeleteByID(TblPrefix()."flip_menu_blocks", $data["id"]);
		LogChange("Der <strong>Men&uuml;block</strong> '".escapeHtml($caption)."' wurde gel&ouml;scht.");
	}

	function actionMoveBlock($data) {
		$o1 = MysqlReadFieldByID(TblPrefix()."flip_menu_blocks", "order", $data["id1"]);
		$o2 = MysqlReadFieldByID(TblPrefix()."flip_menu_blocks", "order", $data["id2"]);
		MysqlWriteByID(TblPrefix()."flip_menu_blocks", array ("order" => $o1), $data["id2"]);
		MysqlWriteByID(TblPrefix()."flip_menu_blocks", array ("order" => $o2), $data["id1"]);
	}

	function frameEditItem($get) {
		if (empty ($get["id"])) {
			$this->Caption = "Menu -> Men&uuml;eintrag erstellen";
			if (empty ($get["block_id"]))
				trigger_error_text("Ein neuer Men&uuml;eintrag braucht eine block_id.", E_USER_ERROR);
			$r = array ("block_id" => $get["block_id"], "caption" => "new Item", "order" => time());
		} else {
			$this->Caption = "Menu -> Men&uuml;eintrag bearbeiten";
			$r = MysqlReadRowByID(TblPrefix()."flip_menu_links", $get["id"]);
		}
		return $r;
	}

	function submitItem($data) {
		$c = 0;
		if (!empty ($data["link"])) {
			$c ++;
		}
		
		if (!empty ($data["frameurl"])) {
			$c ++;
		}
		
		if (!empty ($data["text"])) {
			$c++;
		}
		
		if ($c != 1) {
			trigger_error_text("Von Link, Text und Frame darf nur genau ein Feld verwendet werden.", E_USER_WARNING);
			return false;
		}
		
		$r = MysqlWriteByID(TblPrefix()."flip_menu_links", $data, $data["id"]);
		if ($r != $data["id"])
			$action = "erstellt";
		else
			$action = "bearbeitet";
		LogChange("Der <strong>Men&uuml;eintrag</strong> '".escapeHtml($data["caption"])."' wurde $action.");
		return $r;
	}

	function actionDeleteItem($data) {
		if (MysqlReadField("SELECT COUNT(`id`) FROM `".TblPrefix()."flip_menu_blocks` WHERE (`parent_item_id` = '".addslashes($data["id"])."');") > 0)
			trigger_error_text("Ein Men&uuml;eintrag kann erst gel&ouml;scht werden, wenn er keine UnterBl&ouml;cke mehr hat.", E_USER_ERROR);
		$caption = MysqlReadFieldByID(TblPrefix()."flip_menu_links", "caption", $data["id"]);
		MysqlDeleteByID(TblPrefix()."flip_menu_links", $data["id"]);
		LogChange("Der <strong>Men&uuml;eintrag</strong> '".escapeHtml($caption)."' wurde gel&ouml;scht.");
	}

	function actionMoveItem($data) {
		$o1 = MysqlReadFieldByID(TblPrefix()."flip_menu_links", "order", $data["id1"]);
		$o2 = MysqlReadFieldByID(TblPrefix()."flip_menu_links", "order", $data["id2"]);
		MysqlWriteByID(TblPrefix()."flip_menu_links", array ("order" => $o1), $data["id2"]);
		MysqlWriteByID(TblPrefix()."flip_menu_links", array ("order" => $o2), $data["id1"]);
	}

	function actionFlushCache($data) {
		SessionAddDoNext("inc/inc.menu.php|MenuFlushCache");
		trigger_error("Jeder, der die Seite ab jetzt aufruft, wird das aktualisierte Men&uuml; bekommen.");
	}

}

RunPage("MenuPage");
?>
