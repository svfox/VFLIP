<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: image.php 1441 2007-07-30 10:19:14Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("mod/mod.image.php");

class ImagePage extends Page {
	function frameDefault($get, $post) {
		global $User;
		$i = new DataImage(new ImageLoaderDB($get["name"]));
		$info = $i->getInfo(false);

		$tag = md5(serialize($info));
		ArrayWithKeys($_SERVER, array("HTTP_IF_NONE_MATCH"));
		$match = trim($_SERVER["HTTP_IF_NONE_MATCH"], "\"");
		if ($match == $tag) {
			header("HTTP/1.1 304 Not Modified");
			header("ETag: \"$tag\"");
			header("Content-Length: 0");
		} else {
			header("Etag: \"$tag\"");
			$i->printData();
		}
		return true;
	}

	function frameThumbnail($get) {
		global $User;
		header("Content-type: image/png");
		$ident = addslashes($get["name"]);
		$typ = (is_posDigit($ident)) ? "id" : "name";
		$dat = MysqlReadRow("
		      SELECT `tn_data` FROM `" . TblPrefix() . "flip_content_image` 
		      WHERE ((`$typ` = '$ident') AND " . $User->sqlHasRight("view_right") . ")
		    ", true);
		if (is_array($dat) and !empty ($dat["tn_data"])) {
			echo $dat["tn_data"];
		} else {
			$im = ImageCreate(100, 48);
			imagecolortransparent($im, ImageColorAllocate($im, 255, 255, 255));
			$text_color = ImageColorAllocate($im, 255, 0, 0);
			ImageString($im, 5, 8, 2, "Kein", $text_color);
			ImageString($im, 5, 8, 17, "Thumbnail", $text_color);
			ImageString($im, 5, 8, 32, "vorhanden", $text_color);
			ImagePNG($im);
		}
		return true;
	}
}

RunPage("ImagePage");
?>