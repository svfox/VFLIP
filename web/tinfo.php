<?php
/**
 * @author Matthias Gro&szlig;
 * @version $Id: tinfo.php 1702 2019-01-09 09:01:12Z scope $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file
 * COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");
require_once ("mod/mod.tournament.php");

class TurnierInfoPage extends Page {

	var $ViewRight = "tinfopage_view";
	var $AdminRight = "tinfopage_admin";

	function frameDefault($get, $post) {
		global $User;

		$this->Caption = "Turnierinfoseite";

		$User->requireRight($this->ViewRight);
		$r["isadmin"] = $User->hasRight($this->AdminRight);

		$r["conf"] = "Soll die News wirklich gel&ouml;scht werden?";

		$tournaments = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_tournament_tournaments` WHERE `status` = 'open';", "id");
		$currencies = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_tournament_coins`;", "id");
		$news = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_tinfo_news` ORDER BY `date` DESC;", "id");

		foreach ($tournaments as $t) {
			$t["currency"] = $currencies[$t["coin_id"]]["currency"];
			$t["participants"] = count(TournamentGetCombatants($t["id"]));
			$r["tlist"][] = $t;
		}

		if (empty ($news))
			$r["news"][] = array (
				"caption" => "Keine News",
				"content" => "Es gibt leider nichts Neues!",
			"date" => time());
		else
			$r["news"] = $news;

		return $r;
	}

	function actionDeleteNews($g) {
		global $User;
		$User->requireRight($this->AdminRight);
		if (MysqlDeleteByID(TblPrefix() . "flip_tinfo_news", $g["id"]))
			LogAction("Ein Newseintrag auf der Turnierpage wurde gel&ouml;scht!");
		else
			trigger_error_text("Ein Newseintrag konnte nicht entfernt werden!", E_USER_ERROR);
	}

	function frameEditNews($g) {
		global $User;
		$User->requireRight($this->AdminRight);

		$this->Caption = ($g["id"] == "new") ? "Eintrag erstellen" : "Eintrag bearbeiten";
		if ($g["id"] == "new") {
			$r["caption"] = "Neuer Newseintrag";
			$r["content"] = "";
			$r["id"] = "";
			return $r;
		} else
			return MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_tinfo_news` WHERE `id` = " .escape_sqlData($g["id"]) . ";");
	}

	function submitEditNews($g) {
		global $User;
		$User->requireRight($this->AdminRight);

		$g["date"] = time();
		if (empty ($g["id"]))
			$g["content"] = str_replace("\n", "<br />", $g["content"]);

		if (MysqlWriteByID(TblPrefix() . "flip_tinfo_news", $g, $g["id"])) {
			LogAction("Es wurde eine neue Turniernews hinzugef&uuml;gt!");
			return true;
		} else {
			trigger_error_text("Die Turniernews '$g[title]' konnte nicht gespeichert werden!", E_USER_WARNING);
			return false;
		}
	}

	function frameShowDownloads($g) {
		global $User;

		$this->Caption = "Downloads";
		$User->requireRight($this->ViewRight);
		$r["isadmin"] = $User->hasRight($this->AdminRight);
		$r["tid"] = $g["tid"];
		$r["downloads"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_tinfo_downloads` WHERE `tournament_id` = " .escape_sqlData($g["tid"]) . " ORDER BY `description`;", "id");
		return $r;
	}

	function frameEditDownload($g) {
		global $User;
		$User->requireRight($this->AdminRight);

		$this->Caption = ($g["id"] == "new") ? "Download hinzuf&uuml;gen" : "Download bearbeiten";

		if ($g["id"] == "new") {
			$r["tournament_id"] = $g["tid"];
			$r["description"] = "Neuer Download";
			$r["link"] = "http://";
			$r["id"] = "";
			return $r;
		} else
			return MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_tinfo_downloads` WHERE `id` = " .escape_sqlData($g["id"]) . ";");
	}

	function submitEditDownload($g) {
		global $User;
		$User->requireRight($this->AdminRight);

		if (MysqlWriteByID(TblPrefix() . "flip_tinfo_downloads", $g, $g["id"])) {
			LogAction("Es wurde ein neuer Download hinzugef&uuml;gt!");
			return true;
		} else {
			trigger_error_text("Der Download '$g[description]' konnte nicht gespeichert werden!", E_USER_WARNING);
			return false;
		}
	}

	function actionDeleteDownload($g) {
		global $User;
		$User->requireRight($this->AdminRight);
		$this->NextPage = "tinfo.php?frame=showdownloads&tid=$g[tid]&nomenu=1";
		if (MysqlDeleteByID(TblPrefix() . "flip_tinfo_downloads", $g["id"]))
			LogAction("Ein Download auf der Turnierpage wurde gel&ouml;scht!");
		else
			trigger_error_text("Ein Download konnte nicht entfernt werden!", E_USER_ERROR);
	}
}

RunPage("TurnierInfoPage");
?>
