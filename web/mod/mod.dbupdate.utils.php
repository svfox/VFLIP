<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: mod.time.php 1702 2019-01-09 09:01:12Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/**
 * Anfang einer DBUpdate-Aktion
 */
function _DBUQueryStart() {
	old_Try();
}

function _DBUQueryEnd($Success, $LogMsg) {
	$err = old_Except(false);
	$Fehler = "<span class=\"important\">Fehler</span>";
	if (is_array($err))
		foreach ($err as $k => $v)
			$err[$k] = $v->getMessage(true);
	if (!$Success or is_array($err))
		$LogMsg = "$Fehler bei: $LogMsg";
	if (is_array($err))
		$LogMsg = "$Fehler: " . implode("<br/>\n", $err);
	LogDBUpdate($LogMsg);
}

function _DBUExecQuery($Query, $LogMsg) {
	_DBUQueryStart();
	$r = MysqlWrite($Query, "Beim Datenbankupdate \"$LogMsg\" ist ein Fehler aufgetreten.");
	_DBUQueryEnd($r, $LogMsg);
}

function _DBUExecInsert($TableName, $Values, $LogMsg) {
	_DBUQueryStart();
	$r = MysqlWriteByID($TableName, $Values, 0, "Beim Datenbankupdate \"$LogMsg\" ist ein Fehler aufgetreten.");
	_DBUQueryEnd($r, $LogMsg);
	return $r;
}

function _DBUAddRight($Right, $Description, $Owner = null) {
	$msg = "Recht '$Right' ($Description) erstellt";
	$rightid = MysqlReadField("SELECT `id` FROM `" . TblPrefix() . "flip_user_right` WHERE (`right` = '$Right');", 0, true);
	if (!$rightid)
		$rightid = _DBUExecInsert(TblPrefix()."flip_user_right", array (
			"right" => $Right,
			"description" => $Description
		), "Recht '$Right' ($Description) erstellt.");

	// Rechtecache neu laden.
	GetRights(true);

	if (!is_null($Owner)) {
		if (!is_array($Owner))
			$Owner = array (
				$Owner
			);
		foreach ($Owner as $k => $u) {
			_DBUQueryStart();
			$id = GetSubjectID($u);
			$r = false;
			if (is_posDigit($k)) {
				if (!empty ($id))
					$r = MysqlWriteByID(TblPrefix() . "flip_user_rights", array (
						"owner_id" => $id,
						"right_id" => $rightid
					));
				_DBUQueryEnd($r, "Recht '$Right' dem Subject '$u' hinzugef&uuml;gt.");
			} else {
				$kid = GetSubjectID($k);
				if (!empty ($id) and !empty ($kid))
					$r = MysqlWriteByID(TblPrefix() . "flip_user_rights", array (
						"owner_id" => $kid,
						"right_id" => $rightid,
						"controled_id" => $id
					));
				_DBUQueryEnd($r, "Recht '$Right' dem Subject '$k' &uuml;ber '$u' hinzugef&uuml;gt.");
			}
		}
	}
}

function _DBUSetConfig($Key, $Val, $Description, $Type = null) {
	_DBUQueryStart();
	$r = ConfigSet($Key, $Val, $Val, $Description, $Type);
	if (!is_null($Type))
		$Type = " [$Type]";
	_DBUQueryEnd($r, "Configeintrag '$Key'$Type ($Description) hinzugef&uuml;gt/bearbeitet.");
}

function _DBUUpdateByKey($TableName, $Values, $KeyAndVals, $LogMsg, $AllowCreate = false) {
	_DBUQueryStart();
	if (!is_array($KeyAndVals))
		return _DBUQueryEnd(false, "Der Parameter KeyAndVals von _DBUUpdateByKey() muss ein Array sein.");
	$w = array ();
	foreach ($KeyAndVals as $k => $v)
		$w[] = "(`" . addslashes($k) . "` = " .escape_sqlData($v) . ")";
	$w = implode(" AND ", $w);
	$id = MysqlReadField("SELECT `id` FROM `" . addslashes($TableName) . "` WHERE ($w);", 0, true);
	if (!empty ($id) or $AllowCreate) {
		if(empty($id)) {
			// create with keys
			$Values = array_merge($KeyAndVals, $Values);
		}
		$r = MysqlWriteByID($TableName, $Values, $id, "Beim Datenbankupdate \"$LogMsg\" ist ein Fehler aufgetreten.");
	} else
		$r = false;
	_DBUQueryEnd($r, $LogMsg);
	return $r;
}

function _DBUUpdateMenu($Blockname, $Caption, $Link, $Right, $Description, $isframe = false) {
	$b = addslashes($Blockname);
	$id = MysqlReadField("SELECT `id` FROM `" . TblPrefix() . "flip_menu_blocks` WHERE (`caption` = '$b');", 0, true);
	if (empty ($id))
		return LogDBUpdate("<span class=\"important\">Warnung</span>: Der Men&uuml;eintrag $Caption konnte nicht hinzugef&uuml;gt werden, da der Men&uuml;block $Blockname nicht existiert.<br>" .
		"Wenn du die neue Seite nutzen m&ouml;chtest, kannst du den Men&uuml;eintrag manuell erstellen: Titel: $Caption, " .
		"Link: <a href=\"$Link\">$Link</a>, Beschreibung: $Description");
	else {
		$urlcolumn = ($isframe) ? "frameurl" : "link";
		return _DBUExecInsert(TblPrefix()."flip_menu_links", array (
			"block_id" => $id,
			"caption" => $Caption,
			"description" => $Description,
			"view_right" => (empty ($Right
		)) ? 0 : GetRightID($Right), $urlcolumn => $Link, "order" => time()), "Men&uuml;eintrag $Caption ($Description, $Link) hinzugef&uuml;gt.");
	}
}

function _DBUUpdateSubjectProperties($Subjectname, $Properties) {
	_DBUQueryStart();
	$succ = false;
	if (SubjectExists($Subjectname)) {
		$s = new Subject($Subjectname);
		$succ = $s->setProperties($Properties);
	}
	$log = array ();
	foreach ($Properties as $k => $v)
		$log[] = "$k => \"$v\"";
	$log = implode(", ", $log);
	_DBUQueryEnd($succ, "Die Eigenschaften des Subjects \"$Subjectname\" wurden aktualisiert: $log");
	return $succ;
}

function _DBUUpdateSubjectColumn($Type, $Name, $ValType, $Caption, $ViewRight, $EditRight, $Hint = "", $callback_read = "") {
	static $c = null;
	if (is_null($c))
		$c = time();
	else
		$c++;
	$access = (empty ($callback_read)) ? "data" : "callback";
	$r = _DBUUpdateByKey(TblPrefix()."flip_user_column", array (
		"type" => $Type,
		"name" => $Name,
		"val_type" => $ValType,
		"caption" => $Caption,
		"required_view_right" => (empty ($ViewRight)) ? 0 : GetRightID($ViewRight),
		"required_edit_right" => (empty ($EditRight)) ? 0 : GetRightID($EditRight),
		"hint" => $Hint,
		"sort_index" => $c,
		"callback_read" => $callback_read,
		"access" => $access
	), array (
		"name" => $Name,
		"type" => $Type
	), "Die SubjectColumn \"$Name\" wurden f&uuml;r Subjekte vom Typ $Type hinzugef&uuml;gt oder bearbeitet. " .
	"(ValTyp: $ValType, Caption: $Caption, ViewRight: $ViewRight, EditRight: $EditRight, Hint: $Hint)", true);

	// den cache leeren
	_GetColumnInfo("group", true);
	return $r;
}

function _DBUUpdateStart() {
	if (ConfigGet("dbupdate_autoupdate") != "Y") {
		$rev = ConfigGet("dbupdate_revision");
		list (, $file, $r, $date, $time, $user) = explode(" ", $rev);
		if ($r <= 828 && !ConfigGet("dbupdate_autoupdate"))
			return false;
	}
	// autoupdate w&auml;hrend des updatens deaktivieren.
	ConfigSet("dbupdate_autoupdate", "N");
	$rev = ConfigGet("dbupdate_revision");
	if (!empty ($rev)) {
		// example: $Id: flip.sql 568 2004-10-20 21:05:01Z docfx $
		list (, $file, $r, $date, $time, $user) = explode(" ", $rev);
		LogDBUpdate("----- Starte Update bei $rev -----");
		if (!empty ($r) and is_posDigit($r))
			return $r;
	}
	LogDBUpdate("Fehler: Die Revision der vorhandenen Datenbank konnte nicht ermittelt werden.");
	return false;
}

function _DBUUpdateEnd($Rev) {
	if (!empty ($Rev)) {
		$date = date("Y-m-d H:i:s\Z"); //2004-10-20 21:05:01Z
		$rev = "\$Id: flip.sql $Rev $date dbupdate \$";
		ConfigSet("dbupdate_revision", $rev);
		ConfigSet("dbupdate_autoupdate", "Y");
	} else {
		LogDBUpdate("Fehler: es sollte eine leere Revision geschrieben werden.");
	}
	
	_DBUOptimizeTables();
	LogDBUpdate("----- Update abgeschlossen mit $rev -----");
}

function _DBUOptimizeTables() {
	$tables = array ();
	foreach (MysqlReadCol("SHOW TABLES;") as $t)
		$tables[] = "`$t`";
	MysqlWrite("OPTIMIZE TABLE " . implode(",", $tables));
}
?>
