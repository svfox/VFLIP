<?php
/**
 * @author Daniel Raap
 * @version $Id: mod.tournament.php 1465 2007-09-13 12:18:40Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if (defined('MOD.TOURNAMENT.DM.PHP'))
	return 0;
define('MOD.TOURNAMENT.DM.PHP', 1);

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('mod/mod.tournament.php');

class DmMatches {
	/**
	 * tr&auml;gt den Gewinner ein
	 *
	 * @param Turnier $turnier 
	 * @param Match $match das Spiel
	 * @param int $points1 TeamID des Gewinners
	 * @param int $points2 nicht ben&ouml;tigt
	 * @param String $comment optionaler Kommentar
	 * @param bool $freecall wurde die Methode intern aufgerufen um
	 * Freilos-Ergebnisse einzutragen?
	 * @return String Nachricht an den Benutzer
	 */
	function setScore(&$turnier, $match = false, $points1 = false, $points2 = false, $comment = "", $freecall = false) {
		global $User;
		if(!$points1 || !$this->_isCombatant($turnier, $points1)) {
			return TournamentError('Der Gewinner ist kein Turnierteam!|ID='.$points1, E_USER_ERROR);
		}
		// Gewinner in das Match eintragen
		$match->team1 = $points1;
		$matchdata = array('team1'=>$points1);
		
		if(!($turnier->Orga || TournamentIsAdmin())) {
			// Eintragenden in Match eintragen
			// (n&ouml;tig damit Ergebnis eingetragen werden kann)
			$myteam = MysqlReadField('SELECT c.team_id FROM `'.TblPrefix().'flip_tournament_combatant` c LEFT JOIN `'.TblPrefix().'flip_user_groups` g ON c.team_id=g.parent_id WHERE c.tournament_id='.escape_sqlData($turnier->id).' AND g.child_id='.escape_sqlData($User->id));
			if(!is_posDigit($myteam)) {
				return TournamentError('Du nimmst nicht an diesem Turnier teil!', E_USER_ERROR, __FILE__,__LINE__);
			} else {
				$match->team2 = $myteam;
				$matchdata['team2'] = $myteam;
			}
		}
		// Match speichern
		MysqlWriteByID(TblPrefix().'flip_tournament_matches', $matchdata, $match->id);
		$match->submitScore(1, 0);
		// Platzierung schreiben
		MysqlWriteByID(TblPrefix().'flip_tournament_ranking', array('combatant_id'=>$points1, 'rank'=>1, 'tournament_id'=>$turnier->id));
		// Turnier beenden
		$turnier->setStatus('end');
		return 'Gewinner wurde gesetzt.';
	}
	
	/**
	 * prueft ob eine ID ein Team in dem Turnier ist
	 *
	 * @access private
	 * @param Turnier $turnier
	 * @param int $teamID
	 * @return boolean
	 **/
	function _isCombatant($turnier, $teamID) {
		return in_array($teamID, array_keys($turnier->GetCombatants()));
	}

	/**
	 * erstellt ein Spiel und setzt den Status auf "laufend"
	 *
	 * @param Turnier $turnier das Turnier
	 * @param String $status der gesetzt werden soll
	 * @return boolean
	 */
	function GenerateMatches(&$turnier, $status = false) {
		global $User;
		if(!$turnier->Orga)
			$User->requireRight(TournamentAdminright());
		if (empty ($turnier->id))
			return TournamentError("Spiele konnten nicht erstellt werden.|Es wurde keine ID angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		
		// vorhandene Spiele entfernen
		$turnier->_DeleteGames();
		
		$teams = $turnier->GetCombatants();
		if(count($teams) > 1) {
			// ein Match erstellen um Ergebnis eintragen zu k&ouml;nnen
			$endtime = ($turnier->roundtime > 0) ? (time() + $turnier->roundtime * 60) : 0;
			$team1 = array_shift($teams);
			$team2 = array_shift($teams);
			MysqlWriteByID(TblPrefix().'flip_tournament_matches', array ('tournament_id' => $turnier->id, 'team1'=>$team1['team_id'], 'team2'=>$team2['team_id'], 'endtime' => $endtime, 'levelid' => 1));
			$newstatus = (!$status) ? 'games' : $status;
			$msg = 'Spiele wurden erstellt.';
		} else {
			if(count($teams) == 1) {
				// Teams als Gewinner eintragen
				$team = array_shift($teams);
				MysqlWriteByID(TblPrefix().'flip_tournament_ranking', array('combatant_id'=>$team['team_id'], 'tournament_id'=>$turnier->id, 'rank'=>1));
				$msg = 'Das einzige Team ist Gewinner!';
			} else {
				$msg = 'Keine Teams vorhanden!?';
			}
			$newstatus = 'end';
		}
		
		TournamentError($msg);
		$turnier->setStatus($newstatus);
		
		return true;
	}
}
?>
