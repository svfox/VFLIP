<?php
/**
 * Klasse welche ein Spiel modelliert
 * @author Loom
 * @version $Id$ 1702 2019-01-09 09:01:12Z
 * @copyright � 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 * @since 1398 25.05.2007
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.TOURNAMENT.GAME.PHP"))
	return 0;
define("MOD.TOURNAMENT.GAME.PHP", 1);

/** FLIP-Kern */
require_once("core/core.php");
require_once("inc/inc.sqlrowobject.php");

/**
 * Liest einmal alle Spiele aus der DB und gibt die Daten des angeforderten
 * Spieles zur�ck
 * 
 * @param int $id ID eines Spieles
 * @return Array mit den Schl�sseln aus flip_table_values
 */
function GetGame($id) {
	if(is_digit($id)) {
		$gamestablename = TournamentTablename_Games();
		static $tableid;
		if(!isset($tableid)) {
			$tableid = MysqlReadField("SELECT id FROM �".TblPrefix()."flip_table_tables� WHERE name=".escape_sqlData($gamestablename).";", "id");
		}
		
		static $games = array();
		if(empty($games)) {
			$games = MysqlReadArea("SELECT * FROM �".TblPrefix()."flip_table_entry�
									WHERE table_id=".escape_sqlData($tableid)."
									", "key");
		}
		
		return $games[$id];
	}
	return false;
}

class TournamentGame extends SqlRowObject {
	var $id;
	var $name;
		
	/**
	 * Konstruktor
	 * 
	 * @param int $id ID des Spiels
	 */
	//php 7 public function __construct()
	//php 5 original function TournamentGame()
	function __construct($id) {
		$this->SqlRowObject(null, null, GetGame($id));
		$this->name = $this->value;
		unset($this->value);
	}
	
	/***************************** Getters *****************************/
	function getID() {
		return $this->id;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getDisplay() {
		return $this->display;
	}
}
?>
