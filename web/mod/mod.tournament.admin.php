<?php
/**
 * @author Daniel Raap
 * @version $Id: mod.tournament.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.TOURNAMENT.ADMIN.PHP"))
	return 0;
define("MOD.TOURNAMENT.ADMIN.PHP", 1);

/** FLIP-Kern */
require_once("core/core.php");
require_once ("mod/mod.tournament.php");

/**
 * Klasse zum verwalten von Turnieren
 * u.a. Erstellen, Bearbeiten, L&ouml;schen, Orgas hinzuf&uuml;gen und Turnierbaum erstellen
 */
class TurnierAdmin extends Turniersystem {
	/**
	 * Constructor
	 * ruft den Konstruktor der Elternklasse auf und Pr&uuml;ft die Berechtigung
	 * @param integer $id Turnier-ID; die Turnierdaten werden als Attribute gespeichert
	 */
	 
	//php 7 public function __construct()
	//php 5 original function TurnierAdmin()
	function __construct($id) {
		global $User;
		//php 5
		//$this->Turniersystem($id);
		//php 7:
		parent::__construct($id);
		if (!$this->Orga)
			$User->requireRight(TournamentAdminright());
	}

	/**
	 * Entfernt ung&uuml;ltige Schl&uuml;ssel aus einem Array
	 * Ung&uuml;ltig bedeutet, nicht vom Turniersystem verwertbar. Also keine nutzbaren Turnierdaten.
	 * 
	 * @access private 
	 * @return array Array mit g&uuml;ltigen Turnierdatennamen
	 **/
	function _ValidCols($tdata) {
		if (!is_array($tdata))
			return array ();
		$cols = array ("game", "teamsize", "maximum", "roundtime", "description", "start", "type", "liga", "group_id", "looser", "status", "groups", "screenshots", "u18", "coins", "treetype", "coin_id", "icon");
		foreach ($tdata AS $key => $value)
			if (!in_array($key, $cols))
				unset ($tdata[$key]);
		return $tdata;
	}

	/**
	 * Gibt nur g&uuml;ltige Turnierdaten zur&uuml;ck
	 * ggf. werden Daten ge&auml;ndert oder formatiert
	 * alle nicht g&uuml;ltigen Schl&uuml;ssel werden gel&ouml;scht
	 *
	 * @access private 
	 * @param Array $tdata Array mit Turnierdaten; Schl&uuml;ssel: Spalte, Wert: Wert
	 * @return Array enth&auml;lt alle ben&ouml;tigten Schl&uuml;ssel/Spalten mit g&uuml;ltigen Werten 
	 */
	function _ValidTournamentData($tdata) {
		$tdata = $this->_ValidCols($tdata);
		$tgroups = MysqlReadCol("SELECT id, name FROM ".TblPrefix()."flip_tournament_groups", "name", "id");
		foreach ($tdata as $key => $value) {
			//Checks und Formatierungen
			switch ($key) {
				case "liga" :
					if(!TournamentValidLiga($value))
						TournamentError("Es wurde keine g&uuml;ltige Liga angegeben!", E_USER_ERROR, __FILE__, __LINE__);
				case "game" :
					if (empty ($tdata["game"])) {
						TournamentError("Es wurde kein Spiel angegeben!", E_USER_ERROR, __FILE__, __LINE__);
						return array ();
					}
					if($tdata["liga"] == "")
						if (strlen($tdata["game"]) > 4)
							TournamentError("Das Spielk&uuml;rzel darf nur maximal vier Zeichen lang sein!", E_USER_WARNING, __FILE__, __LINE__);
				case "start" :
					//keine Angabe, dann...
					if (!$tdata["start"])
						$tdata["start"] = time() + 60 * 60; //+1 Stunde
					break;
				case "type" :
					//Maximum muss f&uuml;r KO 2er Potenz sein, ggf entsprechend aufrunden
					if (strtolower($tdata["type"]) == "ko")
						$tdata["maximum"] = pow(2, ceil(log($tdata["maximum"]) / log(2)));
					//Mindestens eine Gruppe
					if (strtolower($tdata["type"]) == "group" && $tdata["groups"] < 1)
						TournamentError("Es muss mindestens eine Gruppe geben!", E_USER_ERROR, __FILE__, __LINE__);
					break;
				case "group_id" :
					if (!in_array($tdata["group_id"], array_keys($tgroups))) //keine g&uuml;ltige ID
						if ($key = array_search($tdata["group_id"], $tgroups)) //g&uuml;ltiger Name
							$tdata["group_id"] = $tgroups[$key];
						else
							$tdata["group_id"] = null;
				case "teamsize":
					$liga = TournamentGetLiga($tdata['liga']);
					$gamename = $liga->GetGameString($tdata["game"]);
					if(!($size = TournamentGameIncludeTeamsize($gamename)))
					{
						if($tdata["teamsize"]<1)
							return TournamentError("Es muss mindestens ein Spieler je Team erlaubt werden :P", E_USER_ERROR, __FILE__,__LINE__);
					} else {
						$tdata["teamsize"] = $size;
					}
				case "maximum" :
					if ($tdata["maximum"] < 2)
						return TournamentError("Was soll das denn? Mit nur EINEM Team lohnt sich kein Turnier ;)", E_USER_ERROR, __FILE__, __LINE__);
			}
		}
		return $tdata;
	}

	/**
	 * Erstellt ein neues Turnier(-subjekt)
	 * @param Array $tdata Turnierdaten; Schl&uuml;ssel: Spalte/Eigenschaft, Wert: Wert
	 */
	function createTournament($tdata) {
		global $User;
		$User->requireRight(TournamentAdminright());
		$tdata = $this->_ValidTournamentData($tdata); //array ("status" => $status, "liga" => $liga, "group_id" => $group, "looser" => $looserid, "teamsize" => $teamsize, "maximum" => $maximum, "roundtime" => $roundtime, "start" => $start, "groups" => $groups, "game" => $game, "description" => $description, "type" => $type, "screenshots" => $screenshots, "u18" => $u18, "coins" => $coins, "treetype" => $treetype, "coin_id" => $coin_id, "icon" => $icon)
		/* Create Tournament */
		if ($newid = $this->_WriteTData($tdata)) {
			ArrayWithKeys($tdata, array("icon"));
			$this->_Icon($newid, $tdata["icon"]);
			/* Create Subject */
			$s = CreateSubject("turnier", "Turniersubjekt$newid");
			/* Logging */
			$t = new Turniersystem($newid);
			$logtext = "Das ".$t->GetTournamentString()."-Turnier wurde mit der ID $newid angelegt.";
			LogChange($logtext);
			$this->_Log($logtext);

			//Freilos als Teilnehmer
			if ($t->AddCombatant("0")) {
				MysqlWrite("INSERT INTO `".TblPrefix()."flip_tournament_ranking` (tournament_id, combatant_id)
							VALUES ('".$t->id."', '0')", "SQL-Fehler in ".basename(__FILE__)." line ".__LINE__."<br>\n");
				return $t;
			} else {
				return TournamentError("Konnte Freilos nicht hinzuf&uuml;gen.|TID:$newid", E_USER_WARNING, __FILE__, __LINE__);
			}
		}
		return TournamentError("Fehler beim Erstellen des Turnieres.", E_USER_ERROR, __FILE__, __LINE__);
	}

	/**
	 * Bearbeitet ein Turnier
	 * 
	 * @param integer $id Turnier-ID
	 * @param Array $tdata Turnierdaten; Schl&uuml;ssel: Spalte, Wert: Wert
	 * @return integer ID des neuen Turnieres oder false bei Fehler
	 */
	function Edit($id, $tdata) {
		global $User;
		if (!$this->Orga)
			$User->requireRight(TournamentAdminright());

		$tdata = $this->_ValidTournamentData($tdata);
		ArrayWithKeys($tdata, array('icon','teamsize','status'));

		if ($tdata["icon"] === null)
			unset ($tdata["icon"]); //nicht l&ouml;schen, wenn nicht angegeben
		if ($this->status == "open")
			$backup = MysqlReadRowByID(TblPrefix()."flip_tournament_tournaments", $id);

		$oldteamsize = $this->teamsize;
		//bei verringerung der Teamgr&ouml;&szlig;e die fixed_size der Teams pr&uuml;fen
		if($oldteamsize > $tdata["teamsize"]) {
			foreach($this->GetCombatants() As $aCombatant) {
					$s = CreateSubjectInstance($aCombatant["team_id"], "turnierteam");
					$teamsize = $s->CountTeam();
					if($teamsize > $tdata["teamsize"])
						return TournamentError("Das Team \"".$s->name."\" ist gr&ouml;&szlig;er als die neue Teamgr&ouml;&szlig;e!|ist:$teamsize, soll:".$tdata["teamsize"],E_USER_ERROR,__FILE__,__LINE__);
			}
			unset($s);
		}
		if ($r = $this->_WriteTData($tdata, $id)) {
			if (!$this->changeStatus($tdata["status"])) {
				MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", $backup, $id);
				return false;
			}
			//bei Teamgr&ouml;&szlig;en&Auml;nderung die fixed_size der Teams &auml;ndern
			if($oldteamsize != $tdata["teamsize"]) {
				foreach($this->GetCombatants() AS $aCombatant) {
					$s = CreateSubjectInstance($aCombatant["team_id"], "turnierteam");
					$s->setProperty("fixed_size", $tdata["teamsize"]);
				}
			}
		}
		return $r;
	}

	/**
	 * Speichert das Turnier und erstellt ggf. ein Mineaturbild
	 *
	 * @access private 
	 * @param array $tdata Array mit Turnierdaten: Spalte => Wert
	 * @param integer $id optionale Turnier-ID; wenn nicht angegeben wird ein neues erstellt
	 * @return integer geschriebene ID oder false bei Fehler
	 */
	function _WriteTData($tdata, $id = 0) {
		if ($writeid = MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", $tdata, $id, "Fehler beim schreiben der Turnierdaten.<br>\n")) {
			ArrayWithKeys($tdata, array("icon"));
			if ($writeid > 0) {
				$this->_Icon($writeid, $tdata["icon"]);
			} else {
				/* some debug info for a strange problem which can't occur */
				ob_start();
				var_dump($writeid);
				$writeiddump = ob_get_contents();
				ob_end_clean();
				return TournamentError("Neues Turnier hat keine ID!?.|MysqlWriteByID() returns '$writeiddump'", E_USER_ERROR, __FILE__, __LINE__);
			}
		} else {
			return TournamentError("Fehler beim speichern der Turnierdaten!",E_USER_ERROR,__FILE__,__LINE__);
		}
		return $writeid;
	}

	/**
	 * erstellt ein Thumbnail, wenn $icon g&uuml;ltig ist
	 * 
	 * @param int $tournamentid ID des Turnieres f&uuml;r welches das Vorschaubild erstellt werden soll
	 * @param mixed $icon Daten des Turnierbildes (NULL = kein Bild)
	 * @return void nix
	 */
	function _Icon($tournamentid, $icon) {
		if (!empty ($icon))
			if (!$this->createThumbnail($tournamentid))
				TournamentError("Konnte kein kleineres Icon erstellen!|tid=$tournamentid", E_USER_WARNING, __FILE__, __LINE__);
	}

	/**
	 * &auml;ndert den Status eines Turnieres und passt den Baum ggf. an
	 * D.h. wenn der Status von Anmeldung auf KO-Spiele gesetzt wird, werden die
	 * Spielpaarungen (der Turnierbaum) erstellt und wenn der Baum bereits
	 * besteht und der Status auf Anmeldung zur&uuml;ckgesetzt wird, werden auch die
	 * Spiele zur&uuml;ckgesetzt.
	 *
	 * @param string $status Der Name des Statuses welcher gesetzt werden soll
	 * @param integer $id TurnierID (optional)
	 * @return bool Wenn keine Fehler auftreten 'true' (siehe setStatus())
	 **/
	function changeStatus($status, $id = false) {
		if (!$id)
			$id = $this->id;
		$t = new Turniersystem($id);
		$type = $t->type;
		unset ($t);
		if ($status == "open") {
			//bei Anmeldung Spiele l&ouml;schen
			$this->_DeleteGames($id);
		}
		elseif ($this->status == "open" && (($status == 'games' && ($type == 'ko' || $type == 'dm')) || ($status == "grpgames" && $type == "group") || $status == "start")) {
			//wenn der Status von Anmeldung auf Spiele gesetzt wurde, diese erstellen
			return $this->GenerateMatches($status);
		}
		elseif ($this->status == "start" && ($status == "grpgames" || $status == "games")) {
			//von Aufw&auml;rmphase zu Spiele: Freilose eintragen
			$r = $this->setStatus($status);
			$this->_PlayFreeGames();
			return $r;
		}
		elseif ($this->status == "games" && $status == "grpgames") {
			//KO-Spiele beim zur&uuml;cksetzen auf Gruppenspiele l&ouml;schen
			MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_matches WHERE tournament_id='".escape_sqlData_without_quotes($id)."' AND level<'".TournamentGroupmatchestartlevel()."'", "SQL-Fehler beim l&ouml;schen in ".basename(__FILE__)." line ".__LINE__."<br>\n");
			//Delete Screenshots from tournament with ID not 1xxxx (not groupmatch screenshots)
			MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_content_image WHERE name LIKE '%_turnier_ID".escape_sqlData_without_quotes($id)."_Match%' AND name NOT REGEXP '%_turnier_ID".escape_sqlData_without_quotes($id)."_Match1[0-9]{4}%'", "SQL-Fehler beim l&ouml;schen in ".basename(__FILE__)." line ".__LINE__."<br>\n");
			MysqlWrite("UPDATE ".TblPrefix()."flip_tournament_ranking SET rank=NULL WHERE tournament_id='".escape_sqlData_without_quotes($id)."'", "SQL-Fehler in ".basename(__FILE__)." line ".__LINE__."<br>\n");
			$this->_Log("Die KO-Spiele wurden gel&ouml;scht.");
		}

		return $this->setStatus($status);
	}

	/**
	 * erstellt ein Icon mit bestimmter gr&ouml;&szlig;e von einem Turniericon
	 * mostly mod.imageedit.php, ResImage::CreateThumbnail()
	 **/
	function createThumbnail($tournamentid, $MaxWidth = 32, $MaxHeight = 32) {
		include_once ("mod/mod.imageedit.php");
		$icon = new ImageLoaderDBField("flip_tournament_tournaments", "icon", $tournamentid, MysqlReadFieldByID(TblPrefix()."flip_tournament_tournaments", "mtime", $tournamentid));
		$info = $icon->getInfo();
		$icon = new ResImage($icon);
		if ($info["width"] > $MaxWidth)
			$h = $info["height"] * (($w = $MaxWidth) / $info["width"]);
		if ($h > $MaxHeight || (!isset ($h) && $info["height"] > $MaxHeight))
			$w = $info["width"] * (($h = $MaxHeight) / $info["height"]);
		if (!isset ($w)) //image is not larger than thumbnail
			return MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", array ("icon_small" => $icon->getData()), $tournamentid);

		$truecolor = IsGD2();
		$img = new ResImage(new ImageLoaderNew($w, $h, $truecolor));
		if ($truecolor)
			imagecopyresampled($img->Res, $icon->Res, 0, 0, 0, 0, $w, $h, $info["width"], $info["height"]);
		else
			imagecopyresized($img->Res, $icon->Res, 0, 0, 0, 0, $w, $h, $info["width"], $info["height"]);
		return MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", array ("icon_small" => $img->getData()), $tournamentid);
	}

	/**
	 * L&ouml;scht ein Turnier
	 * und alle Spiele, Teilnehmer, Platzierungen und Einladungen
	 * 
	 * @return bool Fehlerlos?
	 **/
	function Delete($tournamentid) {
		global $User;
		$User->requireRight(TournamentAdminright());
		if (!TournamentIsValidID($tournamentid))
			return false;

		//Spiele entfernen
		$this->_DeleteGames($tournamentid);
		//Teams entfernen
		$t = new Turniersystem($tournamentid);
		foreach ($t->GetCombatants() AS $combatant) {
			$t->RemoveCombatant($combatant["team_id"]);
		}
		$tournamentname = $t->GetTournamentString();
		unset ($t);
		//Daten des Turnieres l&ouml;schen
		if (MysqlDeleteByID(TblPrefix()."flip_tournament_tournaments", $tournamentid) && MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_combatant WHERE tournament_id='$tournamentid'") && MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_invite WHERE tournaments_id='$tournamentid'")) {
			MysqlWrite('DELETE FROM ´'.TblPrefix().'flip_user_subjects` WHERE type=\'server\' AND name=\'TurnierSubjekt'.$tournamentid.'\'');
			$this->_Log("Das $tournamentname-Turnier (ID ".$tournamentid.") wurde gel&ouml;scht.");
			TournamentError("Das Turnier wurde gel&ouml;scht.");
		} else {
			return TournamentError("Fehler beim l&ouml;schen des $tournamentname-Turnieres|(".mysqli_error().")", E_USER_WARNING, __FILE__, __LINE__);
		}
		return true;
	}

	function GenerateMatches($status = false) {
		return $this->matches->GenerateMatches($this, $status);
	}

	/**
	 * Gibt ein Array mit Servern zur&uuml;ck, welche nicht einem offenem Match zugewiesen sind
	 *
	 * @return array Zeilen aus flip_tournament_servers
	 **/
	function GetFreeServers() {
		$usedservers = MysqlReadCol("SELECT s.id
	                                 FROM ".TblPrefix()."flip_tournament_servers s
	                                   INNER JOIN ".TblPrefix()."flip_tournament_serverusage u ON s.id = u.server_id
	                                   INNER JOIN ".TblPrefix()."flip_tournament_matches m ON m.id = u.match_id
	                                 WHERE m.points1 IS NULL
	                                 ", "id");
		return MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_tournament_servers
                              WHERE id NOT IN (".implode_sqlIn($usedservers).")
                              ORDER BY game", "id");
	}
}
?>
