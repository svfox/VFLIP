<?php
/**
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.SEARCH.FORUM.PHP")) return 0;
define("MOD.SEARCH.FORUM.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

class Search_forum extends Search //part after Search_ should be the same as filenamepart after mod.search. (case!)
{
  var $forum_viewright = "forum_view";
  
  function Search($searchtexts, $seperator="AND")
  {
    global $User;
    
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    $result = MysqlReadArea("SELECT `title`, t.`id`, p.`poster_id`, p.`post_time`, p.`text`, t.`forum_id`
                             FROM ".TblPrefix()."flip_forum_posts p INNER JOIN ".TblPrefix()."flip_forum_threads t ON p.thread_id=t.id
                             WHERE (`text` LIKE ".implode_sql(" $seperator `text` LIKE ", $searchtexts).")"
                           );
    foreach($result AS $row)
    {
      if($User->hasRightOver($this->forum_viewright, $row["forum_id"]))
        $r[] = array("title"  => $row["title"],
                     "link"   => "forum.php?frame=viewthread&amp;id=".$row["id"],
                     "text"   => date("d.m.y",$row["post_time"])." ".escapeHtml(GetSubjectName($row["poster_id"])).":".$this->_format($row["text"], $searchtexts)
                    );
    }
    return array_merge($r, $this->SearchTitle($searchtexts, $seperator));
  }
  
  function SearchTitle($searchtexts, $seperator="AND")
  {
    global $User;
    
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    $result = MysqlReadArea("SELECT `title`, `id`, `poster_id`, `posts`, `last_change`, `forum_id`
                             FROM ".TblPrefix()."flip_forum_threads
                             WHERE (`title` LIKE ".implode_sql(" $seperator `title` LIKE ", $searchtexts).")"
                           );
    foreach($result AS $row)
    {
      if($User->hasRightOver($this->forum_viewright, $row["forum_id"]))
      $r[] = array("title"  => $row["title"],
                   "link"   => "forum.php?frame=viewthread&amp;id=".$row["id"],
                   "text"   => "von ".escapeHtml(GetSubjectName($row["poster_id"]))." (".$row["posts"]." Beitr&auml;ge, letzter ".date("d.m.y", $row["last_change"]).")"
                  );
    }
    return $r;
  }
}

?>