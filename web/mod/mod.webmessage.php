<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.webmessage.php 1376 2007-03-05 12:03:24Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/**
 * Anzahl ungelesener Nachrichten
 */
function WebMessageUnreadMessagesCount($User)
{
  $id = GetSubjectID($User);
  return MysqlReadField("SELECT COUNT(`id`) AS `count` FROM `".TblPrefix()."flip_webmessage_message` WHERE ((`owner_id` = '$id') AND (`status` = 'unread') AND (`owner_deleted` = 0));","count");  
}

/**
 * Sendet eine Webmessage
 * 
 * @param mixed $Receiver Empf&auml;nger Ident
 * @param mixed $Sender Sender Ident
 * @param String $Subject Betreff
 * @param String  $Message Nachrichtentext
 * @param int $SourceMessageID ID der vorherigen Nachricht
 * @param String $SourceSendType Typ der vorherigen Nachricht
 */
function WebMessageSendMessage($Receiver,$Sender,$Subject,$Message,$SourceMessageID = 0, $SourceSendType = "")
{
  global $User;
  
  if(empty($SourceMessageID)) $SourceMessageID = NULL;
  if(empty($SourceSendType))  $SourceSendType = NULL;
  
  if(!is_null($SourceSendType)) 
    if(!in_array($SourceSendType,array("resend","replay","oforward","sforward")))
      trigger_error_text("Der SourceSendType ist ung&uuml;ltig.|SourceSendType:$SourceSendType Recv:$Receiver Sender: $Sender Subj:$Subject",E_USER_WARNING);
      
  $r = MysqlWriteByID(TblPrefix()."flip_webmessage_message",array(
    "owner_id" => GetSubjectID($Receiver),
    "sender_id" => GetSubjectID($Sender),
    "processor_id" => $User->id,
    "source_id" => $SourceMessageID,
    "source_type" => $SourceSendType,
    "subject" => $Subject,
    "text" => $Message,
    "date" => time()    
  ));
  
  if($r and $SourceMessageID) MysqlWriteByID(TblPrefix()."flip_webmessage_message",array("status" => "processed"),$SourceMessageID);
  return $r;
}

?>