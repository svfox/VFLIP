<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.netlog.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** XML-Encoding-Formate */
define("ENCODING_UTF_8",      "UTF-8");
define("ENCODING_ISO_8859_1", "ISO-8859-1");
define("ENCODING_ISO_8859_15","ISO-8859-15");

class XMLDocument extends XMLTag
{
  var $encoding = ENCODING_UTF_8;
  
  function get()
  {
    $r = "<?xml version=\"1.0\" encoding=\"{$this->encoding}\"?>\n";
    $r .= '<!DOCTYPE characters [<!ELEMENT characters (character*) > <!ELEMENT character  (#PCDATA   ) > ' .
		  '<!ENTITY auml   "&#228;" > ' .
		  '<!ENTITY ouml   "&#246;" > ' .
		  '<!ENTITY uuml   "&#252;" > ' .
		  '<!ENTITY szlig  "&#223;" > ' .
		  '<!ENTITY Auml   "&#196;" > ' .
		  '<!ENTITY Ouml   "&#214;" > ' .
		  '<!ENTITY Uuml   "&#220;" > ' .
		  '<!ENTITY nbsp   "&#160;" > ' . 
		  '<!ENTITY Agrave "&#192;" > ' . 
		  '<!ENTITY Egrave "&#200;" > ' . 
		  '<!ENTITY Eacute "&#201;" > ' . 
		  '<!ENTITY Ecirc  "&#202;" > ' . 
		  '<!ENTITY egrave "&#232;" > ' . 
		  '<!ENTITY eacute "&#233;" > ' . 
		  '<!ENTITY ecirc  "&#234;" > ' . 
		  '<!ENTITY agrave "&#224;" > ' . 
		  '<!ENTITY iuml   "&#239;" > ' . 
		  '<!ENTITY ugrave "&#249;" > ' . 
		  '<!ENTITY ucirc  "&#251;" > ' . 
		  '<!ENTITY uuml   "&#252;" > ' . 
		  '<!ENTITY ccedil "&#231;" > ' . 
		  '<!ENTITY AElig  "&#198;" > ' . 
		  '<!ENTITY aelig  "&#330;" > ' . 
		  '<!ENTITY OElig  "&#338;" > ' . 
		  '<!ENTITY oelig  "&#339;" > ' . 
		  '<!ENTITY euro   "&#8364;"> ' . 
		  '<!ENTITY laquo  "&#171;" > ' . 
		  '<!ENTITY raquo  "&#187;" > ' . 
		  ']>';
    
    //TODO: implement DTDs: i.e.: $r .= "<!DOCTYPE rootelement PUBLIC "-//W3C//DTD Specification V2.10//EN" "http://www.w3.org/2002/xmlspec/dtd/2.10/xmlspec.dtd">\n";
    $r .= $this->build(0, $this->encoding);
    if(GetErrors())
    {
      $r = "";
      global $User;
      foreach(GetErrors() AS $error)
        $r .= $error->getMessage($User->hasRight("view_debug_messages")) . "\n";
    }
    return $r;
  }
  
  function out()
  {
    header("Content-Type: text/xml");
    echo $this->get();
  }

  
  function save($filename = "document.xml")
  {
    header("Content-Type: text/xml");
    header("Content-Disposition: attachment; filename=$filename");
    echo $this->get();
  }
}

class XMLTag 
{
  var $name = "";
  var $attribs = array();
  var $tags = array();
  var $content = "";
  
	//php 7 public function __construct()
	//php 5 original function XMLTag()
  function __construct()
  {
    $args = func_get_args();
    if(is_array($args[0])) $args = $args[0];
    $this->_checkName($args[0]);
    $this->name = array_shift($args);
    foreach($args as $a) 
    {
      if(is_array($a))                    $this->addAttribs($a);
      elseif(is_a($a,get_class($this)))   $this->addTag($a);
      else                                $this->content = $a;
    }
  }
  
  function _checkName($Name)
  {
    if(preg_match("/^[\d\w]+$/",$Name)) return "Name";
    trigger_error_text("Der Bezeichner \"$Name\" ist kein g&uuml;ltiger Name f&uuml;r einen XML-Tag oder Attribut.",E_USER_WARNING);  
    return "error_invalid_name";
  }
  
// ------------------- Hinzuf&uuml;gen von Attributen/Tags ----------------------

  function addTag($aTag)
  {
    if(is_a($aTag,"XMLTag")) $this->tags[] = $aTag;
    else $this->tags[] = new XMLTag(func_get_args());
  }
  
  function addAttribs($KeyValueAr)
  {
    foreach($KeyValueAr as $k => $v) 
      $this->addAttrib($k,$v);
  }
  
  function addAttrib($aKey, $aValue)
  {
    $this->attribs[$aKey] = $aValue;
  }
  
  function addTwoDimArrayContent($ItemTagName, $Array, $Cols = array())
  {
    $col = $Cols;
    foreach($Array as $i)
    { 
      $item = new XMLTag($ItemTagName);
      if(empty($Cols)) $col = array_keys($i);
      foreach($col as $c)
        if(is_a($i[$c],"XMLTag"))
          $item->addTag($i[$c]);
        else
          $item->addTag($c,$i[$c]);
      $this->addTag($item);
    }
  }
  
// -------------------- XML generieren -----------------------------------
  
  function _encode($dat) 
  {
    switch($this->encoding)
    {
      case(ENCODING_UTF_8):
        return utf8_encode(escapeHtml($dat));
        
      case(ENCODING_ISO_8859_1):
      case(ENCODING_ISO_8859_15):
        return escapeHtml($dat);

      default:
        trigger_error_text("Ein XML-Dokument konnte nicht generiert werden, weil das Encoding-Format ung&uuml;ltig ist.|Encoding:".$this->encoding,E_USER_WARNING);
        return "";
    }
  }
  
  function _mergeAttribs() 
  {
    $r = "";
    foreach($this->attribs as $k => $v) 
      $r .= " $k=\"".$this->_encode($v)."\"";
    return $r;
  }
  
  function _getContent($prefix)
  {
    if(empty($this->content) && $this->content!=="0")
      return "";
      
    $cont = $this->_encode($this->content);
    if(strpos($cont,"\n") === false) return $cont;
      
    $r = "\n";
    foreach(explode("\n",$cont) as $line)
      $r .= "$prefix  $line\n";
    $r .= $prefix;
    return $r;
  }
  
  function _getTags($IdentLevel)
  {
    $r = "";
    foreach($this->tags as $tag)
      $r .= $tag->build($IdentLevel + 1, $this->encoding);
    return $r;
  }
  
  function build($IdentLevel, $encoding) 
  {
    $this->encoding = $encoding;
    
    $attrs = $this->_mergeAttribs();
    $prefix = str_repeat("  ",$IdentLevel);
    
    if(empty($this->content) && $this->content!=="0" and empty($this->tags))
      return "$prefix<{$this->name}$attrs />\n";
    
    if(!empty($this->content) || $this->content==="0" and empty($this->tags))
      return "$prefix<{$this->name}$attrs>".$this->_getContent($prefix)."</{$this->name}>\n";

    if(empty($this->content) and !empty($this->tags))
      return "$prefix<{$this->name}$attrs>\n".$this->_getTags($IdentLevel)."$prefix</{$this->name}>\n";
      
    trigger_error_text("Ein XML-Tag kann nur Kontent ODER Untertags haben.|Tag:".$this->name,E_USER_WARNING);
    return "";
  }
  
}

?>