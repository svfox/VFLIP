<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.time.php 1356 2007-02-06 15:14:30Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/
 
/**
 * Formatiert relative Zeit als String
 */ 
function FormatTimeDist($Seconds,$Precision = 2)
{
  $r = array();
  $sec = $Seconds % 60;
  if($sec) $r[] = ($sec == 1) ? "eine Sek" : "$sec Sek";
  $dat = floor($Seconds / 60);
  if($dat)
  {
    $min = $dat % 60;
    if($min) $r[] = ($min == 1) ? "eine Min" : "$min Min";
    $dat = floor($dat / 60);
	if($dat)
	{
	  $hours = $dat % 24;
      if($hours) $r[] = ($hours == 1) ? "eine Stunde" : "$hours Stunden";
      $dat = floor($dat / 24);
	  if($dat)
	  {
	    $days = $dat % 7;
	    if($days) $r[] = ($days == 1) ? "ein Tag" : "$days Tage";
        $dat = floor($dat / 7);
		if($dat) $r[] = ($dat == 1) ? "eine Woche" : "$dat Wochen";
	  }
	}  
  }
  $r = array_slice(array_reverse($r),0,$Precision);
  $last = array_pop($r);
  if(count($r) > 0) return implode(", ",$r)." und $last";
  else return $last;
}

?>