<?php
/** 
 * Der Watches-Handler stellt eine Erweiterung des Forums dar.
 * Ein Watch ist ein 'Bewegungsmelder', der das Forum auf Ver&Auml;nderungen hin
 * &uuml;berwacht und User, die einen Watch auf ein Forum oder einen Thread haben,
 * informiert, wo und was sich ge&auml;ndert hat.
 * 
 * Die Implementation des Handlers erfolgt an den Stellen, an denen das Forum
 * ver&auml;ndert werden k&ouml;nnte mit der &uuml;bergabe der Thread- und/oder Forums-ID. 
 * So muss bei einer Veranderung nicht etwa das ganze Forum gedifft werden.
 * 
 * Bei bestimmten Ereignissen 's&auml;ubert' sich das System von selbst. So werden
 * alle Foren-Watches gel&ouml;scht, wenn das dazugeh&ouml;rige Forum auch gel&ouml;scht wird
 * 
 * TODO: 
 * - Benachrichtigung bei S&auml;uberungen
 * - Recht watches_use einbauen
 * 
 * @author Matthias Gro&szlig;
 * @version $Id: mod.forum.watches.php 1500 2018-06-09 03:01:38Z scope $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("mod/mod.sendmessage.php");

class ForumWatchesHandler {
	
	/**
	 * CheckWatches() veranlasst das Senden von Benachrichtigungen, dass
	 * sich etwas ge&auml;ndert hat.
	 * @param string $TargetType Der Typ des ver&auml;nderten Objekts (thread, forum)
	 * @param integer $TargetID Die ID des Ziels
	 * @param string $Action Die ausgef&uuml;hrte Aktion (add, delete, move, delall ...)
	 * @param array $AdditionalData Zus&auml;tzliche Daten, die schon voher bekannt sind, um unn&ouml;tige
	 * Datenbankabfragen zu vermeiden
	 * @return boolean true, wenn die Benachrichtigung erfolgreich war, false, wenn keine Watches
	 * vorhanden sind oder ein Fehler aufgetreten ist
	 */	
	
	function CheckWatches($TargetType, $TargetID, $Action, $AdditionalData) {
		if(ConfigGet("watches_enabled") != "Y")
			return false;
		if(empty($TargetType) || empty($TargetID) || empty($Action)) {
			trigger_error_text("Dem Watches-Handler fehlen Parameter!",E_USER_WARING);
			return false;	
		}

		global $User;
		
		$TargetType = trim(strtolower($TargetType));
		$Action		= trim(strtolower($Action));
		$f_id		= -1;
		$params		= array();
		
		if(($TargetType != "forum") && ($TargetType != "thread")) {
			trigger_error("Unbekannter Watches-Typ '$TargetType'!",E_USER_WARNING);
			return false;
		}
		
		// 2 ForEachs sind zwar etwas umst&auml;ndlicher, ersparen aber eine Mysql-Query
		// -> DB-Last ist geringer, da via PHP sortiert wird...
		
		$allwatches = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_forum_watches`;");
		
		if($TargetType == "thread") {
			$thread = MysqlReadRow("SELECT * FROM `".TblPrefix()."flip_forum_threads` WHERE `id` = '".escape_sqlData_without_quotes($TargetID)."';");
			foreach($allwatches as $w) {
				if(($w['type'] == "thread") && ($w['target_id'] == $TargetID))
					$watches['thread'][] = $w;
			}
		}
		
		$f_id = empty($thread) ? $TargetID : $thread['forum_id'];
		
		foreach($allwatches as $w) {
			if(($w['type'] == "forum") && ($w['target_id'] == $f_id))
				$watches['forum'][] = $w;
		}
		
		// Keine Watches
		if(empty($watches['forum']) && empty($watches['thread']))
			return false;

		$wmsg = new ExecMessage();
		$wmsg->messageFromDB("watches_notify");
		
		if($Action != "threadmove") {
			if(isset($AdditionalData['forum']) && is_object($AdditionalData['forum'])) {
				$forum = & $AdditionalData['forum'];
				$f_name = $forum->GetProperty("name");
			} else {
				$f_name = "!Forum nicht angegeben!"; //TODO: Fehler oder bessere Meldung
			}
		}

		// Benachrichtigungstext basteln ;)
		$baseURL = ServerURL().dirname($_SERVER['PHP_SELF']).'/';
		$threadLink = "\n".' Thread: '.$baseURL.'forum.php?frame=viewthread&id='.$thread['id'];
		$forumLinkBase = 'Forum: '.$baseURL.'forum.php?frame=viewforum&id=';
		switch($TargetType) {
			case "thread":	// Alle Thread-Watches benachrichtigen
				switch($Action) {		
					case "lock":
						$lock = $AdditionalData['lock'] ? "gesperrt" : "entsperrt";
						$params['actionmsg'] = 'Der Thread \''.$thread['title']."' im Forum '$f_name' wurde $lock!\n";
						$params['actionmsg'] .= $threadLink;
						break;		
					case "threaddelete":
						$params['actionmsg'] = "Der Thread '".$thread['title']."' im Forum '$f_name' wurde gel&ouml;scht!";
						$this->DeleteWatchesOnID("thread",$thread['id']);
						break;
					case "threadmove":
						$tforum = & $AdditionalData['targetforum'];
						$name = $tforum->GetProperty("name");
						$params['actionmsg'] = "Der Thread '".$thread['title']."' wurde ins Forum '$name' verschoben!";
						$params['actionmsg'] .= $forumLinkBase . $tforum->id;
						break;
					case "postadd":
						$params['actionmsg'] = "Im Thread '".$thread['title']."' des Forums '$f_name' wurde gepostet!";
						$params['actionmsg'] .= $threadLink;
						break;
					case 'postdelete':
						$params['actionmsg'] = "Im Thread '".$thread['title']."' des Forums '$f_name' wurde ein Post gel&oouml;scht!";
						$params['actionmsg'] .= $threadLink;
						break;
					case 'postmodify':
						$params['actionmsg'] = "Im Thread '".$thread['title']."' des Forums '$f_name' wurde ein Post ge&auml;ndert!";
						$params['actionmsg'] .= $threadLink;
						break;
					case "threadrename":
						$params['actionmsg'] = "Der Thread '".$thread['title']."' im Forum '$f_name' wurde in '".$AdditionalData['newname']."' umbenannt!";
						$params['actionmsg'] .= $threadLink;
						break;
					case 'right':
						$params['actionmsg'] = "Das Beitragsrecht im Thread '".$thread['title']."' des Forums '$f_name' wurde in '".$AdditionalData['right']."' ge&auml;ndert!";
						$params['actionmsg'] .= $threadLink;
						break;
					case "sticky":
						$params['actionmsg'] = "Der Thread '".$thread['title']."' im Forum '$f_name' wurde als ".$AdditionalData['sticky']." markiert!";
						$params['actionmsg'] .= $threadLink;
						break;
				}
				$params['threadname'] = $thread['title'];
 				$wmsg->Params = $params;
				
				// Nachrichten senden
				if(is_array($watches['thread'])) {
					foreach($watches['thread'] as $w) {
						if($w['owner_id'] != $User->id) // Keine Meldung &uuml;ber selbstgemachte &Auml;nderungen
							$wmsg->sendMessage(CreateSubjectInstance($w['owner_id'], "user"));
					}
				}
					
				// Kein break -> Weitermachen mit Forenwatches...

			case "forum": // Alle Foren-Watches benachrichtigen
				switch($Action) {
					case "empty":
						$params['actionmsg'] = "Das Forum '$f_name' wurde geleert!";
						$this->DeleteWatchesOnThreads($AdditionalData['forumids']);
						break;
					case "forumdelete":
						$params['actionmsg'] = "Das Forum '$f_name' wurde gel&ouml;scht!";
						$this->DeleteWatchesOnID("forum",$forum->id);
						break;
					case "threadcreate":
						$params['actionmsg'] = "Ein Thread namens '".$AdditionalData['newthread']."' wurde im Forum '$f_name' erstellt!";
						$this->DeleteWatchesOnID("thread",$thread['id']);
						break;
				}
				
 				$wmsg->Params = $params;
 				
				// Nachrichten senden
				if(is_array($watches['forum'])) {
	 				foreach($watches['forum'] as $w) {
	 					if($w['owner_id'] != $User->id)
							$wmsg->sendMessage(CreateSubjectInstance($w['owner_id'], "user"));
	 				}
				}
		}
		return true;
	}
	
	
	/**
	 * DeleteWatchesOnID() l&ouml;scht alle Foren- oder Thread-Watches die zu einem bestimmten
	 * Objekt geh&ouml;ren mittels der ID des Objekts
	 * @param string $WatchType Der Typ der Watches (thread oder forum)
	 * @param integer $OnID Die ID des Ziels des Watches
	 * @return boolean true, falls der Vorgang geklappt hat, sonst false
	 */	
	 
	function DeleteWatchesOnID($WatchType, $OnID) {
		$WatchType = trim(strtolower($WatchType));
		return MysqlWrite("DELETE FROM `".TblPrefix()."flip_forum_watches` WHERE `type` = '$WatchType' AND `target_id` = '$OnID';");
	}
	
	
	/**
	 * DeleteWatchesOnThreads() l&ouml;scht alle Thread-Watches, deren Watch-Ziel im
	 * eindimensionalen Array $ThreadIDs enthalten ist
	 * Objekt geh&ouml;ren mittels der ID des Objekts
	 * @param array $ThreadIDs Ein eindimensionales Array, das die IDs der Threads enth&auml;lt
	 * @return boolean true, falls der Vorgang geklappt hat, sonst false
	 */
	
	function DeleteWatchesOnThreads($ThreadIDs) {
		return MysqlWrite("DELETE FROM `".TblPrefix()."flip_forum_watches` WHERE `type` = 'thread' AND `target_id` IN (".implode_sqlIn($ThreadIDs).")");
	}
	
	
	/**
	 * IsWatching() teilt mit, ob man ein bestimmtes Element im Forum beobachtet
	 * @param string $Type Typ des zu pr&uuml;fenden Elements (forum oder thread)
	 * @param integer $ID Die ID der Elements
	 * @return boolean true, falls der Vorgang geklappt hat, sonst false
	 */
	
	function IsWatching($Type, $ID) {
		global $User;
		static $cache = array();
		if(!isset($cache[$User->id][$Type])) {
			$cache[$User->id][$Type] = MysqlReadCol("SELECT `target_id` FROM `".TblPrefix()."flip_forum_watches` WHERE `type` = '".escape_sqlData_without_quotes($Type)."' AND `owner_id` = ".escape_sqlData_without_quotes($User->id).";",'target_id','target_id'); 
		}
		if(isset($cache[$User->id][$Type][$ID]))
			return true;
		else
			return false;
	}
	
}

?>