<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.subject.forum.php 1351 2007-01-30 13:36:09Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

class Forum extends ChildSubject {
	var $PostRight = "";
	var $ViewRight = "";
	var $PostHtmlRight = "";
	var $PostFlipRight = "";

	function setLastPost() {
		$p = array ();
		$p["thread_count"] = MysqlReadField("SELECT COUNT(`id`) FROM `".TblPrefix()."flip_forum_threads` WHERE (`forum_id` = '{$this->id}');");
		$p["post_count"] = MysqlReadField("SELECT COUNT(p.id) FROM `".TblPrefix()."flip_forum_posts` p, `".TblPrefix()."flip_forum_threads` t WHERE ((t.forum_id = '{$this->id}') AND (t.id = p.thread_id));");
		$p["last_post_time"] = MysqlReadField("SELECT MAX(`last_change`) FROM `".TblPrefix()."flip_forum_threads` WHERE (`forum_id` = '{$this->id}');");
		$p["last_post_user"] = (empty ($p["last_post_time"])) ? 0 : MysqlReadField("SELECT `last_poster` FROM `".TblPrefix()."flip_forum_threads` WHERE (`last_change` = '$p[last_post_time]');");
		$this->setProperties($p);
	}

	function hasViewRight($aUser = NULL) {
		global $User;
		$u = (is_object($aUser)) ? $aUser : $User;
		return $u->hasRightOver($this->ViewRight, $this);
	}

	function requireAllowCreate() {
		global $User;
		$User->requireRight("forum_admin");
	}

}
?>