<?php
/**
 * Klasse welche ein KO-Baum modelliert
 * @author Loom
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 * @since 1441 29.07.2007
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.TOURNAMENT.POINTS.PHP"))
	return 0;
define("MOD.TOURNAMENT.POINTS.PHP", 1);

/** FLIP-Kern */
require_once("core/core.php");
require_once ('mod/mod.tournament.php');

class PointsMatches {
	/**
	 * tr&auml;gt das Ergebnis eines Spiels ein und ermittelt das n&auml;chste Match
	 * @param Turnier $turnier das Turnier
	 * @param Match $match das Spiel
	 * @param integer $points1 Punkte des ersten Teams
	 * @param integer $points2 Punkte des zweiten Teams
	 * @param String $comment optionaler Kommentar
	 * @param bool $freecall wurde die Methode intern aufgerufen um
	 * Freilos-Ergebnisse einzutragen?
	 * @return String Nachricht an den Benutzer
	 */
	function setScore(&$turnier, &$match, $points1 = false, $points2 = false, $comment = "", $freecall = false) {
		global $User;
		
		if(!is_a($turnier, 'Turniersystem'))
		{
			return text_translate('Fehler: Kein Turnier angegeben!');
		}
		if(!is_a($match, 'TournamentMatch'))
		{
			return text_translate('Fehler: Kein Match angegeben!');
		}
		
		//ID des Eintragenden
		$editor = ($freecall) ? 0 : $User->id;
		
		$before = array('points1'=>$match->points1, 'points2'=>$match->points2, 'winner'=>$match->getWinner(), 'editor'=>$match->editor, 'comment'=>$match->comment);
	
		$done = text_translate('Es wurde nichts gemacht');
		if ($turnier->status == 'games' || ($turnier->status == 'start' && $turnier->type == 'points')) {
			/***************/
			/*  KO-System  */
			/***************/
	
			//Unentschieden?
			if ($points1 == $points2)
				return TournamentError("Es muss ein Gewinner ermittelt werden!", E_USER_WARNING, __FILE__, __LINE__);
	
			// Ergebnis eintragen
			if($match->submitScore($points1, $points2, $editor) === false)
			{
				$msg = text_translate('Fehler beim Speichern des Ergebnisses!');
				TournamentError($msg, E_USER_ERROR, __FILE__,__LINE__);
				return $msg;
			}
			$match->setComment($comment);
	
			//Spiel aus dem Looserbracket?
			$islooserbracket = $turnier->isLoserBracket($match->level);
	
			//Platzierung zur&uuml;cksetzen, falls Verlierer sich ge&auml;ndert hat
			if ($before['points1'] != '' && ($turnier->looser == 'N' || ($turnier->looser == 'Y' && $islooserbracket))) {
				$turnier->setRank($match->getWinner(), NULL, $match->tournament_id);
				$turnier->setRank($match->getLoser(), NULL, $match->tournament_id);
			}
	
			//erste Runde?
			if ($match->level == $turnier->FirstRound)
				$firstround = true;
			else
				$firstround = false;
	
			//n&auml;chste Runde ermitteln
			$newlevel = $turnier->getnextLevel($match->level);
	
			//LevelID f&uuml;r n&auml;chstes Match
			if ($match->levelid % 2 == 1)
				$newlevel['winnerid'] = ($match->levelid + 1) / 2;
			else
				$newlevel['winnerid'] = $match->levelid / 2;
			if (!$islooserbracket) {
				if(($turnier->FirstRound - $match->level) % 2 == 0) {
					//jede zweite Runde ins Loserbracket gekreuzt
					$maxlevelid = pow(2, (round($newlevel['looser']) - 1));
					$oldlevelid = ($firstround) ? ceil($match->levelid/2) : $match->levelid;
					$newlevel['looserid'] = 1 + $maxlevelid - $oldlevelid;
				}
				else {
					$newlevel['looserid'] = $match->levelid;
				}
			} elseif (($match->level * 100 % 100) / 100 == 0.25) {
				$newlevel['winnerid'] = $match->levelid;
			}
			
			//Ende der n&auml;chsten Runde festlegen (vorhandene Zeit bevorzugen, um manuelle Zeit&Auml;nderungen beizubehalten)
			if ($islooserbracket)
				$bracket = 'looser';
			else
				$bracket = 'winner';
			if (!($endtime = MysqlReadField("SELECT endtime FROM ".TblPrefix()."flip_tournament_matches
		                                      WHERE level='".$newlevel[$bracket]."'
		                                        AND tournament_id='".$match->tournament_id."'
		                                      ORDER BY endtime DESC LIMIT 1
		                                     ", "endtime", true))) {
				$endtime = ($turnier->roundtime > 0) ? $match->endtime + $turnier->roundtime * 60 : 0;
			}
	
			//Anzahl der Turnierteilnehmer
			$combatants = count($turnier->GetCombatants());
	
			/* KO-System: Paarungen und Platzierungen */
			// Endspiel?
			if ($match->level == 0) {
				$turnier->setRank($match->getWinner(), 1, $match->tournament_id);
				$turnier->setRank($match->getLoser(), 2, $match->tournament_id);
				MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", array ("status" => "end"), $match->tournament_id);
				$turnier->_Log("Ergebnis des Finales wurde eingetragen und Turnier beendet.", true);
				return 'Ergebnis wurde eingetragen und Turnier beendet.';
			}
			elseif ($turnier->looser == "Y" && ($combatants <= 2 || MysqlReadField("SELECT level FROM ".TblPrefix()."flip_tournament_matches
							                                                   WHERE tournament_id='".$match->tournament_id."'
							                                                     AND level<'".TournamentGroupmatchestartlevel()."'
							                                                   ORDER BY level DESC
							                                                  ", "level") == 1) && $newlevel["winner"] == 0) {
				MysqlWriteByID(TblPrefix()."flip_tournament_matches", array ("tournament_id" => $match->tournament_id, "endtime" => $endtime, "team1" => $match->getWinner, "team2" => $match->getLoser(), "level" => 0, "levelid" => 1));
				return TournamentError("Ergebnis wurde eingetragen und Finale erstellt.");
			}
			// Gewinner f&uuml;r n&auml;chste Runde eintragen
			if (($matchexistsId = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_tournament_matches
		                                          WHERE levelid='".$newlevel["winnerid"]."'
		                                            AND level='".$newlevel["winner"]."'
		                                            AND tournament_id='".$match->tournament_id."'
		                                         ", "id", true))) {
				$matchexists = new TournamentMatch($matchexistsId);
			} else {
				$matchexistsId = 0;
				$matchexists = null;
			}
	
			if (($match->levelid % 2 == 1 || ($match->level * 100 % 100) / 100 == 0.25) && !($newlevel["winner"] == 0 && $islooserbracket)) {
				// als erstes Team des Matches setzen
				$team = "team1";
			} else {
				// als zweites Team des Matches setzen
				$team = "team2";
			}
			$newid = MysqlWriteByID(TblPrefix().'flip_tournament_matches', array ('tournament_id' => $match->tournament_id, 'endtime' => $endtime, $team => $match->getWinner(), 'level' => $newlevel['winner'], 'levelid' => $newlevel['winnerid']), $matchexistsId);
			require_once('mod/mod.tournament.match.php');
			TournamentGetMatch($newid, true); //refresh cache
			//Message
			if($matchexists) {
				$turnier->readyUp($matchexists);
			}
			
			// Looser Bracket
			if ($turnier->looser == 'Y' && !$islooserbracket && $match->level != '0') {
				// Verlierer kommt ins LooserBracket
				if ($matchexistsId = MysqlReadField('SELECT id FROM `'.TblPrefix().'flip_tournament_matches`
		                                            WHERE levelid=\''.$newlevel['looserid'].'\'
		                                              AND level=\''.$newlevel['looser'].'\'
		                                              AND tournament_id=\''.$match->tournament_id.'\''
		                                           , 'id', true)) {
					$matchexists = new TournamentMatch($matchexistsId);
				} else {
					$matchexistsId = 0;
					$matchexists = null;
				}
	
				if ($firstround && $match->levelid % 2 == 1) {
					// als erstes Team des Matches setzen
					$team = 'team1';
				} else {
					// als zweites Team des Matches setzen
					$team = 'team2';
				}
				$newid = MysqlWriteByID(TblPrefix().'flip_tournament_matches', array ('tournament_id' => $match->tournament_id, 'endtime' => $endtime, $team => $match->getLoser(), 'level' => $newlevel['looser'], 'levelid' => $newlevel['looserid']), $matchexistsId);
				TournamentGetMatch($newid, true); //refresh cache
				//Message
				if($matchexists) {
					$turnier->readyUp($matchexists);
				}
			}
			elseif ($turnier->looser == 'N' && $match->getLoser() != '0') {
				// Single-Elimination-Verlierer Rang zuweisen (ohne LoserBracket)
				$rank = pow(2, $match->level - 1) + 1;
				$turnier->setRank($match->getLoser(), $rank, $match->tournament_id);
			}
			elseif ($islooserbracket) {
				if ($match->getLoser() != '0') {
					// Verlierer (aus LooserBracket) Rang zuweisen
					if (($match->level * 100 % 100) / 100 != 0.75)
						$rank = pow(2, floor($match->level)) + 1 + pow(2, floor($match->level) - 1);
					else
						$rank = pow(2, ceil($match->level)) + 1;
					$turnier->setRank($match->getLoser(), $rank, $match->tournament_id);
				}
			}
	
			/* Matches mit Freilosen in der Folgerunde zur&uuml;cksetzen.
			   f&uuml;r ge&auml;nderte Gewinner oder Seeding notwendig.
			   Dadurch werden sie neu eingetragen und die darauffolgenden Matches entsprechend ge&auml;ndert. (_PlayFreeGames())
			*/
			MysqlWrite("UPDATE ".TblPrefix()."flip_tournament_matches SET points1=NULL, points2=NULL, editor=0
		                  WHERE (team1='0' OR team2='0')
		                    AND (   (levelid='".$newlevel["winnerid"]."' AND level='".$newlevel["winner"]."')
		                         OR (levelid='".$newlevel["looserid"]."' AND level='".$newlevel["looser"]."')
		                        )
		                    AND tournament_id='".$match->tournament_id."'
		                 ");
	
			if ($before['winner'] !== null && $match->getWinner() != $before['winner']) {
				$done = 'Ergebnis und n&auml;chste Paarung wurde ge&auml;ndert.';
			} else {
				$done = 'Ergebnis wurde eingetragen und n&auml;chste Paarung festgelegt.';
			}
	
		} else {
			TournamentError('Es werden keine KO-Spiele ausgetragen, aber es soll ein Ergebnis eingetragen werden!|Turniertyp: '.$turnier->type.', Status: '. $turnier->status, E_USER_ERROR);
		}
		
		return $done;
	}

	/**
	 * Erstellt den Turnierbaum
	 */
	function GenerateMatches(&$turnier, $status = false) {
		global $User;
		if (!$turnier->Orga)
			$User->requireRight(TournamentAdminright());
		$tournamentid = $turnier->id;
		if (empty ($tournamentid))
			return TournamentError("Spiele konnten nicht erstellt werden.|Es wurde keine ID angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		/* Status &uuml;berpr&uuml;fen */
		if ($turnier->status != "open")
			return TournamentError("Turniere k&ouml;nnen nur erstellt werden wenn der Status auf 'Anmeldung' steht.|Nur w&auml;hrend der Anmeldung k&ouml;nnen Spiele erstellt werden.", E_USER_WARNING, __FILE__, __LINE__);

		/* vorhandene Spiele entfernen */
		$turnier->_DeleteGames();

		settimelimit(0);
		/* neue Spiele erstellen */
		// Spieler des Turniers auslesen
		$combatant = $turnier->GetCombatants();
		$combatant = array_filter($combatant, "is_array");
		srand((double) microtime() * 1000000);
		shuffle($combatant);

		// KO-Spiele
		if (!$r = $this->_CreatePointsTree($turnier, $combatant, $status))
			return $r;
		//Zeit nur setzen wenn explizit erstellt (GenerateMatches aufgerufen wurde)
		MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", array ("start" => time()), $tournamentid);
		
		TournamentError("Spiele wurden erstellt und Status auf \"".$turnier->GetStatusString()."\" gesetzt.");

		return true;
	}

	function _CreatePointsTree(&$turnier, $combatant, $status = false) {
		global $User;
		$tournamentid = $turnier->id;
		if (empty ($tournamentid))
			return TournamentError("Spiele konnten nicht erstellt werden.|Es wurde keine ID angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		// KO-Spiele
		$num = count($combatant);
		if ($num < 1) {
			return TournamentError("Keine Spieler vorhanden.", E_USER_WARNING, __FILE__, __LINE__);
		}
		srand((double) microtime() * 1000000);
		shuffle($combatant);
		$exp = ceil(log($num) / log(2));
		$maxplayers = pow(2, $exp);
		$add = $maxplayers - $num;
		$start = time();
		$endtime = ($turnier->roundtime > 0) ? $start + $turnier->roundtime * 60 : 0;
		$levelid = 1;
		if (count($combatant) <= 2) {
			$add = 2 - $num;
			$exp = 0; //=level=0
		}
		while (count($combatant) > 0) {
			if ($add > 0 && ($add > count($combatant) / 2 || $levelid % 2 == 0)) {
				$players = array_splice($combatant, 0, 1);
				$grp1id = $players[0]["team_id"];
				$grp2id = "0";
				$add --;
			} else {
				$players = array_splice($combatant, 0, 2);
				$grp1id = $players[0]["team_id"];
				$grp2id = $players[1]["team_id"];
			}
			$matchid = MysqlWriteByID(TblPrefix()."flip_tournament_matches",array("tournament_id"=>$tournamentid, "endtime"=>$endtime, "team1"=>$grp1id, "team2"=>$grp2id, "level"=>$exp, "levelid"=>$levelid));
			$turnier->readyUp($matchid);
			if($grp1id>0) //skip Freilos
				MysqlWrite("INSERT INTO ".TblPrefix()."flip_tournament_ranking (tournament_id, combatant_id)
							VALUES ('$tournamentid', '$grp1id')", "SQL-Fehler in ".basename(__FILE__)." line ".__LINE__."<br>\n");
			if($grp2id>0) //skip Freilos
				MysqlWrite("INSERT INTO ".TblPrefix()."flip_tournament_ranking (tournament_id, combatant_id)
							VALUES ('$tournamentid', '$grp2id')", "SQL-Fehler in ".basename(__FILE__)." line ".__LINE__."<br>\n");
			$levelid ++;
		}
		$turnier->status = (!$status) ? "games" : $status;
		$turnier->FirstRound = MysqlReadField("SELECT level FROM ".TblPrefix()."flip_tournament_matches
	                                        WHERE tournament_id='$tournamentid'
	                                          AND level<'".TournamentGroupmatchestartlevel()."'
	                                        ORDER BY level DESC
	                                       ", "level", true);
		MysqlWriteByID(TblPrefix()."flip_tournament_tournaments", array ("status" => $turnier->status), $tournamentid);
		$turnier->_PlayFreeGames();

		$turnier->_Log("KO-Baum des \"".TournamentGetTournamentString($tournamentid)."\"-Turnieres wurde erstellt.");

		return true;
	}
}
?>
