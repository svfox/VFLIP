<?php
/**
 * Die Klasse Barcode93 kapselt einen Generator f&uuml;r Code93-Barcodes.
 * Der generierte Barcode kann dann als Bild abgerufen werden.
 * 
 * Ein Barcode hat folgende Eigenschaften:
 * 
 * - $_BarWidth		Die Breite eines Balkens im Code
 * - $_DrawText		Wenn true, wird der codierte Text unter dem Barcode dargestellt
 * - $_colorWhite	Die Farbe eines weissen Balkens (einer freien Stelle, normal #FFFFFF)
 * - $_colorBlack	Die Farbe eines schwarzen Balkens (normal #000000)
 * 
 * @author Matthias Gro&szlig;
 * @version $Id: mod.barcode.code93.php 1702 2019-01-09 09:01:12Z scope $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ('core/core.php');

/** Image-Module */
require_once ('mod/mod.image.php');
require_once ('mod/mod.imageedit.php');

/** Konstanten */

define('DEFAULT_FONT', 5);

class Barcode93 {
	/** Bildressource */
	
	var $_ResImage = NULL;
	
	var $_ImageWidth = 300;
	var $_ImageHeight = 100;
	
	/** Aussehen */
	
	var $_BarWidth = 1;
	var $_DrawText = true;
	var $_DrawBorder = true;
	
	/** Farben */
	
	var $_colorWhite = 0;
	var	$_colorBlack = 0;
	
	/**
	 * Codierungstabelle f&uuml;r alle g&uuml;ltigen Zeichen
	 * - code = Sequenz eines codierten Zeichens
	 * - pos  = Position (Wert) eines Zeichens f&uuml;r die Pr&uuml;fsummenbildung
 	 */	
	 
	var $_startstop = '101011110'; // Start- und Stopsequenz
	var $_codes = array( 
		'0' => array('code' => '100010100', 'pos' => '0'),
		'1' => array('code' => '101001000', 'pos' => '1'),
		'2' => array('code' => '101000100', 'pos' => '2'),
		'3' => array('code' => '101000010', 'pos' => '3'),
		'4' => array('code' => '100101000', 'pos' => '4'),
		'5' => array('code' => '100100100', 'pos' => '5'),
		'6' => array('code' => '100100010', 'pos' => '6'),
		'7' => array('code' => '101010000', 'pos' => '7'),
		'8' => array('code' => '100010010', 'pos' => '8'),
		'9' => array('code' => '100001010', 'pos' => '9'),
		'A' => array('code' => '110101000', 'pos' => '10'),
		'B' => array('code' => '110100100', 'pos' => '11'),
		'C' => array('code' => '110100010', 'pos' => '12'),
		'D' => array('code' => '110010100', 'pos' => '13'),
		'E' => array('code' => '110010010', 'pos' => '14'),
		'F' => array('code' => '110001010', 'pos' => '15'),
		'G' => array('code' => '101101000', 'pos' => '16'),
		'H' => array('code' => '101100100', 'pos' => '17'),
		'I' => array('code' => '101100010', 'pos' => '18'),
		'J' => array('code' => '100110100', 'pos' => '19'),
		'K' => array('code' => '100011010', 'pos' => '20'),
		'L' => array('code' => '101011000', 'pos' => '21'),
		'M' => array('code' => '101001100', 'pos' => '22'),
		'N' => array('code' => '101000110', 'pos' => '23'),
		'O' => array('code' => '100101100', 'pos' => '24'),
		'P' => array('code' => '100010110', 'pos' => '25'),
		'Q' => array('code' => '110110100', 'pos' => '26'),
		'R' => array('code' => '110110010', 'pos' => '27'),
		'S' => array('code' => '110101100', 'pos' => '28'),
		'T' => array('code' => '110100110', 'pos' => '29'),
		'U' => array('code' => '110010110', 'pos' => '30'),
		'V' => array('code' => '110011010', 'pos' => '31'),
		'W' => array('code' => '101101100', 'pos' => '32'),
		'X' => array('code' => '101100110', 'pos' => '33'),
		'Y' => array('code' => '100110110', 'pos' => '34'),
		'Z' => array('code' => '100111010', 'pos' => '35'),
		'-' => array('code' => '100101110', 'pos' => '36'),
		'.' => array('code' => '111010100', 'pos' => '37'),
		' ' => array('code' => '111010010', 'pos' => '38'), // Space
		'$' => array('code' => '111001010', 'pos' => '39'),
		'/' => array('code' => '101101110', 'pos' => '40'),
		'+' => array('code' => '101110110', 'pos' => '41'),
		'%' => array('code' => '110101110', 'pos' => '42'),  // Pos. 43 bis 46 sind Sonderzeichen
		'#' => array('code' => '100100110', 'pos' => '43'),  // Diese werden f&uuml;r die 
		'§' => array('code' => '111011010', 'pos' => '44'),  // Codierung von weiteren Zeichen
		'~' => array('code' => '111010110', 'pos' => '45'),  // beim erweiterten Code93 als
		'=' => array('code' => '100110010', 'pos' => '46')); // Offset verwendet
	
	
	/**
	 * Der Klassenkonstruktor
	 */
	
	//php 7 public function __construct()
	//php 5 original function Barcode93($iWidth = 300, $iHeigth = 100)
	function __construct($iWidth = 300, $iHeigth = 100) {
		$this->_ImageWidth = $iWidth;
		$this->_ImageHeight = $iHeigth;
		$this->_ResImage   = new ResImage(new ImageLoaderNew($this->_ImageWidth, $this->_ImageHeight, false));
		$this->_colorWhite = imagecolorallocate($this->_ResImage->Res,255,255,255);
		$this->_colorBlack = imagecolorallocate($this->_ResImage->Res,0,0,0);
	}
	
	
	/**
	 * Wandelt einen String in eine Folge von 0/1 um, die den Barcode repr&auml;sentiert
	 * @param String $CodeString Der zu codierende String
	 * @return Der kodierte String
	 */
	
	function getBarSequence($CodeString) {
		if(empty($CodeString))
			return trigger_error_text('Kein String zum Umwandeln in eine Barcode-Sequenz vorhanden!',E_USER_ERROR);
		$CodeString = strtoupper($CodeString);
		$errchars = $this->PopInvalidChars($CodeString);
		$CodeString = $this->getChecksummed($CodeString);
		$sequence = '';
		for($i=0;$i < strlen($CodeString);$i++)
			$sequence .= $this->_codes[$CodeString{$i}]['code'];
		if(!empty($errchars))
			trigger_error_text('Folgende Zeichen sind nicht Code93-g&uuml;ltig und wurden ignoriert: "'.$errchars.'"',E_USER_WARNING);
		return $this->_startstop.$sequence.$this->_startstop.'1'; // .'1' -> Termination Bar
	}
	
	
	/**
	 * Pr&uuml;ft einen String auf ung&uuml;ltige Zeichen und entfernt diese.
	 * Die entfernten Zeichen werden zur&uuml;ckgegeben
	 * @param String $Text Der zu parsende String
	 * @return String Alle aussortierten Zeichen
	 */
	
	function PopInvalidChars(& $Text) {
		$inv = '';
		$vchars = array_flip(array_keys($this->_codes));
		unset($vchars['#']);
		unset($vchars['§']);
		unset($vchars['~']);
		unset($vchars['=']);
		for($chr = 0;$chr < strlen($Text);$chr++) {
			if(!isset($vchars[$Text{$chr}])) {
				$inv .= $Text{$chr};
				$Text = substr_replace($Text,'',$chr,1);
				$chr--;
			}
		}	
		return $inv;
	}
	
	
	/**
	 * Gibt das zur Position passende Zeichen zur&uuml;k
	 * @param char $pos Das Zeichen
	 * @return Ein Integer, der die Position darstellt
	 */
	
	function Pos2Char($pos) {
		$ks = array_keys($this->_codes);
		return $ks[$pos];
	}
	
	
	/**
	 * Berechnet die Code93-Checksummen eines Strings und gibt diese mitsamt
	 * dem Orginalstring zur&uuml;k
	 * @param String $CodeString Der zu bearbeitende String
	 * @return Der Originalstring mit angeh&auml;ngten Checksummenzeichen
	 */
	
	function getCheckSummed($CodeString) {
		//C-checksum calc
		$c_sum = 0;
		$c_pos = 1;
		$len = strlen($CodeString);
		for($c=1;$c <= $len;$c++) {
			$c_sum += ($this->_codes[$CodeString{$len-$c}]['pos'] * $c_pos);
			if(($c_pos+1) > 20)
				$c_pos = 1;
			else
				$c_pos++;
		}
		$CodeString .= $this->Pos2Char($c_sum % 47);
		//K-checksum calc
		$k_sum = 0;
		$k_pos = 1;
		$len = strlen($CodeString);
		for($k=1;$k <= $len;$k++) {
			$k_sum += ($this->_codes[$CodeString{$len-$k}]['pos'] * $k_pos);
			if(($k_pos+1) > 15)
				$k_pos = 1;
			else
				$k_pos++;
		}
		$CodeString .= $this->Pos2Char($k_sum % 47);
		return $CodeString;
	}
	
	
	/**
	 * Erzeugt ein Bild mit dem Barcode und gibt es &uuml;ber die Standartausgabe aus
	 * Wird ein nicht unterst&uuml;tzter Bildtyp ausgegeben, so wird automatisch ein PNG erzeugt
	 * @param String $Text Der String, den der Barcode repr&auml;sentieren soll
	 * @param String $Type Der Ausgabetyp des Barcode-Bildes (PNG oder JPG)
	 */
	
	function drawBarcodeImage($Text, $Type = 'png') {
		require_once('mod/mod.image.php');
		if(strlen($Text) < 1)
			trigger_error_text('Der Barcode-Input-String ist leer!',E_USER_WARNING);
		$seq = $this->getBarSequence($Text);		
		// Position berechnen
		$o = round(($this->_ImageWidth - (strlen($seq) * $this->_BarWidth)) / 2);
		$y_start = ($this->_DrawBorder) ? 5 : 4;
		$y_end = ($this->_DrawText) ? $this->_ImageHeight - (imagefontheight(DEFAULT_FONT) + 4) : $this->_ImageHeight;
		$w = $this->_BarWidth;
		for($i = 0; $i < strlen($seq);$i++) {
			$x_start = ($i*$w) + $o;
			$x_end = $x_start + ($w-1);
			if($seq{$i} == '0')
				imagefilledrectangle($this->_ResImage->Res,$x_start,$y_start,$x_end,$y_end,$this->_colorWhite);
			else
				imagefilledrectangle($this->_ResImage->Res,$x_start,$y_start,$x_end,$y_end,$this->_colorBlack);
		}
		// Text?
		if($this->_DrawText) {
			$textWidth = strlen($Text) * imagefontwidth(DEFAULT_FONT);
			$textXPos  = round(($this->_ImageWidth - $textWidth) / 2);
			$textYPos  = $this->_ImageHeight - (imagefontheight(DEFAULT_FONT) + 2);
			imagestring($this->_ResImage->Res,DEFAULT_FONT,$textXPos,$textYPos,$Text,$this->_colorBlack);
		}
		
		// Rand?
		if($this->_DrawBorder)
			imagerectangle($this->_ResImage->Res,0,0,$this->_ImageWidth-1,$this->_ImageHeight-1,$this->_colorBlack);

		switch(strtolower($Type)) {
			case 'jpg':
				$this->_ResImage->setFormat('jpg',95);
				$this->_ResImage->printData();
				break;
			default:
				$this->_ResImage->setFormat('png',95);
				$this->_ResImage->printData();
				break;
		}
	}
}

?>