<?php

/**
 * Das PayPal-System stellt einen kompletten Handler f&uuml;r Express-Zahlungen dar.
 * f&uuml;r die Funktion reicht es, die vom PayPal-Server empfangenen FORM-Variablen
 * bei der Konstruktion zu &uuml;bergeben und den Handler via ProcessIPN() anzusto&szlig;en.
 * Der in der Klasse FLIPPayPalHandler gekapselte Handler erledigt
 * - Die Freischaltung des zahlenden Users
 * - Den Versand etwaiger Benachrichtigungen bei Erfolg/Fehlern
 * - Eine komplette &uuml;berpr&uuml;fung der IPN-Variablen auf Richtigkeit
 * 
 * @author Matthias Gro&szlig;
 * @version $Id: mod.paypal.ipn.php 1702 2019-01-09 09:01:12Z scope $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

require_once("mod/mod.user.php");

class FLIPPayPalHandler {
	
	var $_IPN_Form 		= "";
	var $_Errorstring 	= "";
	var $_Noticestring  = "";
	var $_Target_UID	= 0;
	var $_Target_UNick	= "";


	/**
	 * FLIPPayPalHandler() - Der Klassenkonstruktor
	 * Er &uuml;berimmt bei der Erzeugung einer Klasseninstanz das
	 * IPN-Formular as-is und sorgt f&uuml;r die Zuweisung der Eigenschaften
	 * Target_UID (User-ID des Users, von dem die Zahlung kommt)
	 * und Target_UNick (FLIP_Nickname desselbigen Users)
	 * @param mixed $IPN_Form Das IPN-Formular, das PayPal verschickt hat
	 */		
	 
	//php 7 public function __construct()
	//php 5 original function FLIPPayPalHandler()
	function __construct($IPN_Form) {
		// Konstruktor
		
		/* PayPal schickt uns ein Formular in folgender Form (Ausschnitt):
		 *<input type="hidden" name="option_name1" value="FLIP-ID" />
		 *<input type="hidden" name="option_name2" value="Nick" />
		 *<input type="hidden" name="option_selection1" value="1296" />
		 *<input type="hidden" name="option_selection2" value="test" />
		 */
		
		//LogAction("FLIPPayPalHandler - Konstuktion");
		
		$this->_IPN_Form = $IPN_Form;
		$this->_Target_UID	 = $this->_IPN_Form['option_selection1'];
		$this->_Target_UNick = $this->_IPN_Form['option_selection2'];
	}
	
	
	/**
	 * ProcessIPN() erledigt folgende Schritte:
	 * - Komplette &uuml;berpr&uuml;fung der Bezahlung via CheckFormValid()
	 * - Freischaltung des Useraccounts bei korrekter Zahlung
	 * - Benachrichtigung von User und Geldempf&auml;nger bei Erfolg/Fehlern
	 * @return boolean true, falls der Vorgang geklappt hat, sonst false.
	 */	
	
	function ProcessIPN() {
		
		//LogAction("ProcessIPN()");
		
		// Gibt es den User &uuml;berhaupt?
		$s_id = escape_sqlData_without_quotes($this->_Target_UID);		
		if(!MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_user_subject` WHERE `id` = '$s_id' AND `type` = 'user';",true)) {
			$this->_Errorstring = "Eine PayPal-Zahlung wurde empfangen, aber weder User-ID noch Username wurden angegeben!";
			$this->SendNotification("fail","admin");
			return false;
		}
		
		//LogAction("User existiert!");
		
		// Ist die Form korrekt und g&uuml;ltig?
		if(!$this->CheckFormValid()) {
			// Form ung&uuml;ltig / etwas stimmt nicht
			// Die Fehlermeldung wurde durch CheckFormValid() generiert
			$this->SendNotification("fail");
			return false;
		}
		
		// Form g&uuml;ltig, User freischalten und Nachrichten schicken
		UserSetStatus($this->_Target_UID,"paid");
		$this->SendNotification();
		
		// Logging
		LogAction("Der User ".$this->_Target_UNick." hat mit dem PayPal-System bezahlt!");
		
		return true;
	}


	/**
	 * SendNotification() schickt IPN-Nachrichten an Systemuser und den zahlenden User 
	 * @param mixed $type Typ der Nachricht - 'success' f&uuml;r eine Erfolgsmeldung, 'fail' f&uuml;r eine Fehlermeldung
	 * @param mixed $reciever 'user' - nur der User erh&auml;lt die Nachricht, 
	 * 'admin' - nur der Admin erh&auml;lt die Nachricht, 
	 * 'all' (Standart) - alle erhalten die Nachricht
	 * @return boolean true, falls der Vorgang geklappt hat, sonst false.
	 */

	function SendNotification($type = "success", $reciever = "all") {
		require_once ("mod/mod.sendmessage.php");
		 
		$ppmsg = new ExecMessage();
		
		// Message-Variablen
		$msgparams['paypal_targetname'] = $this->_Target_UNick;
		$msgparams['paypal_ipn_date']   = date("m.d.y \u\m H:i:s");
		$msgparams['paypal_ipn_info']  = " --- Userdaten --- \n\n";
		$msgparams['paypal_ipn_info'] .= "Nick: ".$this->_Target_UNick."\n";
		$msgparams['paypal_ipn_info'] .= "ID: ".$this->_Target_UID."\n\n\n";
		$msgparams['paypal_ipn_info'] .= " --- PayPal IPN-Infos --- \n\n";
		$msgparams['paypal_ipn_info'] .= "IPN-Server: ".ConfigGet("paypal_ipn_server")."\n\n";
		$msgparams['paypal_ipn_info'] .= " --- Dump IPN-Formular --- \n\n";
		$msgparams['paypal_ipn_info'] .= print_r($this->_IPN_Form,true)."\n\n";
		$msgparams['paypal_ipn_info'] .= " --- Ende Infos ---\n\n";
		
		if($type == "success")
			$ppmsg->messageFromDB("flip_paypal_paymentsuccess");
		else $ppmsg->messageFromDB("flip_paypal_paymentfailure");
		
		// Vars einf&uuml;gen
		$ppmsg->Params = $msgparams;
			
		// Nachricht schicken @User
		if($reciever != "admin")
			$r = $ppmsg->sendMessage(CreateSubjectInstance($this->_Target_UID, "user"));
		
		// Der Sysuser l&auml;sst sich nicht &uuml;ber CreateSubjectInstance() ansprechen...
		$sys = new User(SYSTEM_USER_ID);
		
		// Nachricht schicken @Sys
		if($reciever != "user")
			$r = $ppmsg->sendMessage($sys);
	}
	
	
	/**
	 * SaveToDB() speichert die aktuelle Transaktion in der Datenbank.
	 * Das Format in der Tabelle ist so festgelegt:
	 * + - - - - + - - - + - - - - - - +
	 * | txn_id  | mtime | formstring  |
	 * + - - - - + - - - + - - - - - - +
	 * @return boolean true, falls der Vorgang geklappt hat, sonst false.
	 */
	 	
	function SaveToDB() {
		// Formular in eine Zeichenkette umwandeln
		$formstring = "";
		$tx_id = $this->_IPN_Form['txn_id'];
		
		foreach ($this->_IPN_Form as $key => $value)
			$formstring .= "&$key=$value";
			
		$formstring = escape_sqlData_without_quotes($formstring);
		
		// Formular in die Datenbank schreiben.
		return MysqlWrite("INSERT INTO `" . TblPrefix() . "flip_paypal_transactions` (tx_id, formstring) VALUES ('$tx_id','$formstring');");
	}
	
	
	/**
	 * CheckFormValid() f&uuml;hrt eine komplette Pr&uuml;fung des IPN-Formulars aus.
	 * Die Form wird gepr&uuml;ft:
	 * 1) Auf eindeutige tx_id (Abgleich per DB)
	 * 2) Die richtige reciever_address ( == MailAddy in der Config)
	 * 3) Auf g&uuml;ltigkeit via Server (VERIFIED || INVALID)
	 * 4) Auf den korrekten TX-Code (payment_status == completed)
	 * Ist alles korrekt, dann gibt die Methode true zur&uuml;ck und ein Hinweis wird abgelegt.
	 * Ansonsten l&auml;sst sich ein Fehlerstring abrufen, der die Details zum Fehler enth&auml;lt.
	 * @return boolean true, wenn alles korrekt ist, sonst false.
	 */
	 
	function CheckFormValid() {
		$this->_Errorstring = "";
		// LogAction("CheckFormValid()");
		if(empty($this->_IPN_Form)) {
			$this->_Errorstring = "Das Formular enth&auml;lt keine Daten und kann nicht verarbeitet werden!";
			return false;
		}
		
		// Ist die Transaktions-ID eindeutig?
		// Wenn ja weitermachen und Form in die DB Speichern
		$txid = escape_sqlData_without_quotes($this->_IPN_Form['tx_id']);
		$try  = MysqlReadRow("SELECT `tx_id` FROM `" . TblPrefix() . "flip_paypal_transactions` WHERE `tx_id` = '$txid';",true);
		if(!empty($try)) {
			$this->_Errorstring = "Die Transaktionsnummer wurde schon einmal verwendet!";
			return false;
		} 
		else { $this->SaveToDB(); }
		
		// Stimmen die E-Mail-Addys &uuml;berein?
		// TODO: Option via Config: paypal_accept_only_insystem
		// -> Falls true, Code unter ausf&uuml;hren
		$flip_ipn_mail = $this->_IPN_Form['business'];
		$button_mails = MysqlReadCol("SELECT `business` FROM `" . TblPrefix() . "flip_paypal_buttons`","business");
		
		if(!in_array($flip_ipn_mail,$button_mails)) {
			$this->_Errorstring = "Die angegebene Empf&auml;nger-Mailadresse wird von keinem der Angebote (Buttons) im FLIP verwendet!";
			return false;
		}
		
		// Ist das Formular g&uuml;ltig?
		
		if(!$this->CheckTransactionValid()) {			
			$this->_Errorstring = "Das IPN-Formular ist ung&uuml;ltig: " . $this->_Errorstring;
			return false;
		}
		
		// Welcher Transaktions-Code ist gegeben?
		$form_txtype = strtolower($this->_IPN_Form['payment_status']);
		if($form_txtype == "completed") {
			$this->_Noticestring = "Die Zahlung ist einwandfrei erfolgt!";
			return true;
		} 
		else {
			// Anderes Ereignis eingetreten...
			switch($form_txtype) {
				case "denied":
					// Die Zahlung wurde vom Verk&auml;ufer verweigert
					$this->_Errorstring = "Die PayPal-Zahlung wurde storniert!";
					return false;
					break;
				case "expired":
					// Die Authorisierung der Zahlung ist verweigert worden
					$this->_Errorstring = "Die Zeit, um die Transaktion zu authorisieren ist abgelaufen!";
					return false;
					break;
				case "failed":
					// Die Zahlung ist fehlgeschlagen
					$this->_Errorstring = "Die Transaktion ist fehlgeschlagen!";
					return false;
					break;
				case "in-progress":
					// Die Zahlung wird gerade bearbeitet
					$this->_Errorstring = "Die Transaktion wird gerade bearbeitet - der User-Account wird bald freigeschalten sein!";
					return false; // return true;
					break;
				case "partially-refunded":
					// Die Zahlung wurde zum Teil r&uuml;ckerstattet
					$this->_Errorstring = "Die Zahlung wurde Teilweise wieder r&uuml;ckerstattet!";
					return false;
					break;
				case "pending":
					// Die Transaktion "wartet" - Grund herausfinden und benachrichtigen
					$pending_reason = strtolower($this->_IPN_Form['pending_reason']);
					
					switch($pending_reason) {
						case "address":
							$this->_Errorstring = "Der Kunde hat keine best&auml;tigte Lieferadresse angegeben!";
							break;
						case "echeck":
							$this->_Errorstring = "Die Zahlung erfolgte via &uuml;berweisung, welche noch offen ist - bitte Geduld haben!";
							break;
						case "intl":
							$this->_Errorstring = "Der Verk&auml;ufer besitzt ein Non-US-Konto aber keinen Bankeinzugsmechanismus!";
							break;
						case "multi-currency":
							$this->_Errorstring = "Die Zahlung erfolgte in einer dem Account des Verk&auml;ufers unbekannten W&auml;hrung!";
							break;
						case "unilateral":
							$this->_Errorstring = "Die Zahlung ist an eine weder registrierte noch verifizierte E-Mail-Adresse gerichtet!";
							break;
						case "upgrade":
							$this->_Errorstring = "Der Empf&auml;nger-Account besitzt nicht den n&ouml;tigen Status um Kreditkartenabrechnungen zu empfangen!";
							break;
						case "verify":
							$this->_Errorstring = "Der Account des Empf&auml;ngers ist noch nicht verifiziert worden!";
							break;
						case "other":
							$this->_Errorstring = "PANIC: Die Zahlung wurde aus einem unbekannten Grund aufgehalten! Bitte umgehend mit PayPal in Verbindung setzen!";
							break;
						default:
							$this->_Errorstring = "Die Zahlung wurde aufgehalten!";
					}
					return false;
					break;
				case "refunded": // r&uuml;ckerstattung
					$this->_Errorstring = "Der Empf&auml;nger hat die Zahlung r&uuml;ckerstattet!";
					return false;
					break;
				case "reversed": // Die Zahlung wurde VOM USER r&uuml;ckg&auml;ngig gemacht D: - Wer macht sowas?
					$this->_Errorstring = "Die Zahlung wurde vom Zahler r&uuml;ckg&auml;ngig gemacht, ";
					$reason_code = strtolower($this->_IPN_Form['reason_code']);
					
					switch($reason_code) {
						case "chargeback":
							$this->_Errorstring .= "da er sein Geld zur&uuml;ck gefordert hat!";
							break;
						case "guarantee":
							$this->_Errorstring .= "da er von seiner Geld-zur&uuml;ck-Garantie gebrauch gemacht hat!";
							break;
						case "buyer-complaint":
							$this->_Errorstring .= "da er mit der 'Ware' unzufrieden war!";
							break;
						case "refund":
							$this->_Errorstring .= "da der Empf&auml;nger das Geld zur&uuml;ck erstattet hat!";
							break;
						default:
							$this->_Errorstring .= "es wurde aber kein Grund angegeben!";
					}
					
					return false;
					break;
				case "processed": // Die Zahlung wurde bearbeitet
					$this->_Noticestring = "Die Zahlung wurde erfolgreich bearbeitet!";
					return true;
					break;
				case "voided":	// Die Zahlung ist ung&uuml;ltig geworden
					$this->_Errorstring = "Die Zahlung ist nicht mehr g&uuml;ltig!";
					return false;
					break;
				default:
					$this->_Errorstring = "Der Zahlungsstatus '$form_txtype' ist unbekannt!";
					return false;
			}
		}
	}


	/**
	 * CheckTransactionValid() sendet ein empfangenes IPN-Formular an den PayPal-
	 * Server zur&uuml;ck und Pr&uuml;ft, ob es g&uuml;ltig ist
	 * 
	 * @param string $formdata Die 'rohen' Formdaten vom Server
	 * @param string $errstr Enth&auml;lt eine Fehlermeldung, falls die Pr&uuml;fung fehlschl&auml;gt
	 * @return boolean Die R&uuml;ckgabe ist false, wenn die Form nicht g&uuml;ltig ist, ansonsten ist sie 'true'
	 **/
	 
	function CheckTransactionValid(/*$formdata , $errstr, $target*/) {
		
		$formdata = $this->_IPN_Form;
		$target   = ConfigGet("paypal_ipn_server");
		
		// Form aufbereiten
	
		$req = 'cmd=_notify-validate';
		$host = parse_url($target);
	
		foreach ($formdata as $key => $value) {
			$value = urlencode(stripslashes($value));
			$req .= "&$key=$value";
		}
	
		// Header
	
		$header .= "POST /cgi-bin/webscr HTTP/1.1\r\n";
		$header .= "Host: $host[host]\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n";
		$header .= "Connection: close\r\n\r\n";
	
		// SSL-Verbindung?
	
		if ($host['scheme'] == "https") {
			$host['host'] = "ssl://$host[host]";
			$host['port'] = "443";
		}
	
		// Form zur Pr&uuml;fung an den Server zur&uuml;cksenden
		$errstr = "";
		$errno  = 0;
		$fp = fsockopen($host['host'], $host['port'], $errno, $errstr, 10);
	
		if (!$fp) { // Verbindungsfehler
			$this->_Errorstring = "Verbindung mit Server $host[host] an Port $host[port] nicht m&ouml;glich ($errstr)!";
			return false;
		} else {
	
			fputs($fp, $header . $req);
	
			// Auf Serverantwort warten
	
			while (!feof($fp)) {
				$response[] = @ fgets($fp, 1024);
			}
	
			fclose($fp); // Socket schlie&szlig;en
	
			// Serverresponse auswerten
	
			$response = implode("", $response);
	
			if (substr_count($response, "VERIFIED") > 0) {
				return true;
			}
			elseif (substr_count($response, "INVALID") > 0) {
				$this->_Errorstring = "Das Formular ist nicht g&uuml;ltig und wurde vom Server abgeweisen!";
				return false;
			}
			elseif (empty ($response)) {
				$this->_Errorstring = "Der Server antwortete mit einem leeren Response!";
			} else {
				$response = str_replace("\n", "<br />", $response);
				$response = str_replace("\r", "<br />", $response);
				$this->_Errorstring = "Server oder Scriptfehler!\n";
				$this->_Errorstring .= "Header:\n$header\n";
				$this->_Errorstring .= "POST-Daten:\n$req\n";
				$this->_Errorstring .= "Serverantwort:\n$response\n\n-- ENDE SERVERANTWORT --\n";
				return false;
			}
		}
	}
}

?>
