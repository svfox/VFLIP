<?php
/**
 * Eine Spielpaarung
 * @author loom
 * @version $Id$ 1702 2019-01-09 09:01:12Z naaus $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 * @since 1398 25.05.2007 */

/** Die Datei nur einmal includen */
if (defined("MOD.TOURNAMENT.MATCH.PHP"))
	return 0;
define("MOD.TOURNAMENT.MATCH.PHP", 1);

/** FLIP-Kern */
require_once("core/core.php");
require_once("inc/inc.sqlrowobject.php");

/**
 * Liefert die Matchdaten zu der ID
 * 
 * @param int $id ID eines Matches
 */
function TournamentGetMatch($id, $forceLookup = false) {
	if(is_posDigit($id)) {
		$readMaxNearbyRows = "32";
		static $matches = array();
		if(!isset($matches[$id])) {
			//update Cache
			$someMatches = MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_tournament_matches`
									WHERE id BETWEEN '.escape_sqlData($id-$readMaxNearbyRows/2).' AND '.escape_sqlData($id+$readMaxNearbyRows/2).' 
									', 'id');
			foreach($someMatches AS $aMatch) {
				$matches[$aMatch['id']] = $aMatch;
			}
		} elseif($forceLookup) {
			$matches[$id] = MysqlReadRowByID(TblPrefix().'flip_tournament_matches', $id);
		}
		return $matches[$id];
	}
	return false;
}

class TournamentMatch extends SqlRowObject {
	//Identifizierung
	var $id;
	//IDs der zwei Mannschaften
	var $team1;
	var $team2;
	//Ergebnis welches jedes Team erzielt hat
	var $points1;
	var $points2;
	//Status der Teams (bereit/nicht bereit)
	var $ready1;
	var $ready2;
	//Zeit bis ein Ergebnis vorliegen muss
	var $endtime;
	//Kommentar zum Ergebnis
	var $comment;
	//UserID des letzten (Ergebnis-)Bearbeiters
	var $editor;
	//Float (z.B. Level/Runde im KO-Baum (ganze Zahlen = Winnerbr., Komma = Loserbr.)
	var $level;
	//Integer (z.B. Ordnung/Reihenfolge im KO-Baum (von oben nach unten))
	var $levelid;
	
	//config
	var $config_losersubmit = "tournament_losersubmit";
	
	/**
	 * Konstruktor
	 * 
	 * @param int $id ID des Matches
	 */
	//php 7 public function __construct()
	//php 5 original function TournamentMatch()
	function __construct($id) {
		if(!is_posDigit($id)) {
			TournamentError('Es wurde keine ID f&uuml;r ein Match angegeben!|id='.escapeHtml(var_export($id, true)).' ('.gettype($id).')',E_USER_ERROR);
		}
		//php 5:
		//$this->SqlRowObject(null, null, TournamentGetMatch($id)); //use cache of GetMatch()
		//php7 neu:
		parent::__construct(null, null, TournamentGetMatch($id)); //use cache of GetMatch()
		$this->_setWinner();
		$this->losersubmit = ConfigGet($this->config_losersubmit);
	}
	
	/**
	 * Ergebnis eintragen
	 * 
	 * @param int $points1 Punkte des ersten Teams
	 * @param int $points2 Punkte des zweiten Teams
	 * @param int $editor (optional) UserID des Eintragenden (0 = System)
	 * (default = aktueller User)
	 */
	function submitScore($points1, $points2, $editor = false) {
		global $User;
		if(!$editor) {
			$editor = $User->id;
		}
		//Ist User berechtigt?
		if($User->isChildOf($this->team1) || $User->isChildOf($this->team2) || TournamentIsOrga($this->tournament_id)) {
			//angegebene Daten OK?
			if(is_numeric($points1) && is_numeric($points2)) {
				//Ergebnis speichern
				$backup = array('points1'=>$this->points1, 'points2'=>$this->points2);
				//notwendig zur Ermittlung des Gewinners/Verlierers
				$this->points1 = $points1;
				$this->points2 = $points2;
				//Nur Verlierer darf eintragen
				if ($this->losersubmit == 'Y') {
					if (!($User->isChildOf($this->GetLoser()) || TournamentIsOrga($this->tournament_id) || $editor == 0)) {
						$this->points1 = $backup['points1'];
						$this->points2 = $backup['points2'];
						return TournamentError('Nur das Verliererteam darf die Punkte eintragen.', E_USER_WARNING, __FILE__, __LINE__);
					}
				}
				//in DB schreiben
				if(!MysqlWriteByID(TblPrefix().'flip_tournament_matches', array('points1'=>$points1, 'points2'=>$points2, 'editor'=>$editor), $this->id)) {
					$this->points1 = $backup['points1'];
					$this->points2 = $backup['points2'];
					return TournamentError('Das Ergebnis konnte nicht in die Datenbank eingetragen werden.|Matchid='.$this->id, E_USER_WARNING, __FILE__, __LINE__);
				}
				TournamentGetMatch($this->id, true); //refresh cache
				LogAction('Turniersystem: Match (ID '. $this->id .') wurde '. $points1 .':'. $points2 .' von '. GetSubjectName($editor) .' eingetragen.');
				$this->editor = $editor;
				return true;
			} else {
				return TournamentError('Es d&uuml;rfen nur numerische Punkte eingegeben werden!', E_USER_WARNING, __FILE__, __LINE__);
			}
		} else {
			return TournamentError('Du bist nicht berechtigt ein Ergebnis f&uuml;r dieses Match einzutragen!', E_USER_WARNING, __FILE__, __LINE__);
		}
	}
	
	/**
	 * Ermittelt anhand der Punkte den Gewinner
	 * 
	 * @access private
	 */
	function _setWinner() {
		if($this->points1 > $this->points2) {
			$this->winner = $this->team1;
			$this->loser = $this->team2;
		} elseif($this->points2 > $this->points1) {
			$this->winner = $this->team2;
			$this->loser = $this->team1;
		} else {
			$this->winner = null;
			$this->loser = null;
		}
	}
	
	/**
	 * Gibt den Gewinner an oder NULL wenn keiner vorliegt
	 * @return int
	 */
	function getWinner() {
		$this->_setWinner();
		return $this->winner;
	}
	
	/**
	 * Gibt den Verlierer an oder NULL wenn keiner vorliegt
	 * @return int
	 */
	function getLoser() {
		$this->_setWinner();
		return $this->loser;
	}
	
	/**
	 * Liefert den Kommentar zu diesem Match
	 */
	function getComment() {
		return $this->comment;
	}
	
	/**
	 * Setzt den Kommentar fuer dieses Match
	 * 
	 * @param String $comment Kommentar
	 */
	function setComment($comment) {
		$this->comment = $comment;
		$this->_setField("comment", $comment);
	}
	
	/**
	 * Setzt die Endzeit
	 */
	function setEndtime($time) {
		if(is_posDigit($time)) {
			$this->endtime = $time;
			$this->_setField("endtime", $time);
		}
	}
	
	/**
	 * Gibt an ob man noch in der vorgegeben Zeit ist
	 */
	function inTime() {
		return ($this->endtime == 0 || time() < $this->endtime) ? true : false;
	}
	
	/**
	 * @access private
	 */
	function _setField($field, $value) {
		MysqlWriteByID(TblPrefix()."flip_tournament_matches", array($field=>$value), $this->id);
		TournamentGetMatch($this->id, true); //refresh cache
	}
}
?>
