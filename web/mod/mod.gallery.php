<?php
/**
 * Function for removing a directory recursivly
 * 
 * @param String $dir
 */
function rmdirr($dir) {
	if (substr($dir,-1) != '/') $dir .= '/';
	if (!is_dir($dir)) return false;
	if (($dh = opendir($dir)) !== false) {
		while (($entry = readdir($dh)) !== false) {
			if ($entry != '.' && $entry != '..') {
				if (is_file($dir . $entry) || is_link($dir . $entry)) unlink($dir . $entry);
				else if (is_dir($dir . $entry)) rmdirr($dir . $entry);
			}
		}
		closedir($dh);
		rmdir($dir);
		return true;
	}
	return false;
}

/**
	 * Function for rotating an Image
	 * 
	 * @param unknown_type $img
	 * @param unknown_type $rotation
	 */
	function rotateImage($img, $rotation) {
	  $width = imagesx($img);
	  $height = imagesy($img);
	  switch($rotation) {
	    case 90: $newimg= @imagecreatetruecolor($height , $width );break;
	    case 180: $newimg= @imagecreatetruecolor($width , $height );break;
	    case 270: $newimg= @imagecreatetruecolor($height , $width );break;
	    case 0: return $img;break;
	    case 360: return $img;break;
	  }
	  if($newimg) {
	    for($i = 0;$i < $width ; $i++) {
	      for($j = 0;$j < $height ; $j++) {
	        $reference = imagecolorat($img,$i,$j);
	        switch($rotation) {
	          case 90: if(!@imagesetpixel($newimg, ($height - 1) - $j, $i, $reference )){return false;}break;
	          case 180: if(!@imagesetpixel($newimg, $width - $i, ($height - 1) - $j, $reference )){return false;}break;
	          case 270: if(!@imagesetpixel($newimg, $j, $width - $i, $reference )){return false;}break;
	        }
	      }
	    } return $newimg;
	  }
	  return false;
	}
?>