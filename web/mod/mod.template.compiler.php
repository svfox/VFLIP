<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.template.compiler.php 1702 2019-01-09 09:01:12Z scope $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package tpl
 **/
        
/** Die Datei nur einmal includen */
if(defined("MOD.TEMPLATE.COMPILER.PHP")) return 0;
define("MOD.TEMPLATE.COMPILER.PHP",1);

require_once("mod/mod.template.php");
require_once("mod/mod.parse.php");

function _getVarName($var)
{
  $a = array();
  if(preg_match("/^[\\\$%]([\d\w]+)$/",$var,$a)) return $a[1];
  return "";
}

function _isVar($var)
{
  return preg_match("/^\\\$(\\\$)?[\d\w\.]+$/",$var);
}      

function _isEscVar($var)
{
  return preg_match("/^%(\\\$)?[\d\w\.]+$/",$var);
}

function _getVarByName($varname,$NSIndex)
{
  $a = array();
  if(preg_match("/^\\\$([\d\w\.]+)$/",$varname,$a)) $double = $varname = $a[1];
  else $double = false;
  $v = implode("']['",explode(".",$varname));
  $r = "\$var{$NSIndex}['$v']";
  if($double)
    $r = "\$var{$NSIndex}[$r]";
  return $r;
}

function _getVar($var,$NSIndex)
{
  $a = array();
  if(!preg_match("/^\\\$(\\\$?[\d\w\.]+)$/",$var,$a)) return false;
  return _getVarByName($a[1],$NSIndex);
}

function _getEscVar($var,$NSIndex) {
	$a = array();
	
	if(!preg_match("/^%(\\\$?[\d\w\.]+)$/",$var,$a)) {
		return false;
	}
	
	$varValue = _getVarByName($a[1],$NSIndex);
	
	return "escapeHtml((".$varValue."))";
}

function _getArray($var,$NSIndex)
{
  if(!is_array($var)) $p = _getArrayItems($var);
  elseif(empty($var)) $p = array();
  else $p = $var;
  if(!is_array($p)) return false;
  $r = array();
  foreach($p as $k => $v) $r[] = _getPhpVar($k,$NSIndex)." => "._getPhpVar($v,$NSIndex);
  return "array(".implode(", ",$r).")";
}

function _getArrayItems($var)
{
  $a = array();
  if(!preg_match("/^array\((.*)\)$/i",$var,$a)) return false;
  $r = array();
  $p = ParseParam($a[1],array(),false);
  if(!is_array($p)) return trigger_error_text("Fehler beim Parsen eines Arrays im Template: $p|array: $var",E_USER_ERROR);
  return $p;
}

function _isArray($var)
{
  return preg_match("/^array\((.*)\)$/i",$var);
}

function _isIdent($var)
{
  return preg_match("/^[\d\w]+$/",$var);
}

function _isNumber($var)
{
  return preg_match("/^\d+$/",$var);
}

function __RepVarsCallback($a)
{
  global $__CurrentNSIndex,$__CurrentCallback;
  return $__CurrentCallback($a[1],$__CurrentNSIndex);
}

function _replaceVars($exp,$NSIndex,$VarType = "php")
{
  global $__CurrentNSIndex,$__CurrentCallback;
  $__CurrentNSIndex = $NSIndex;
  $__CurrentCallback = "_Get{$VarType}Var";
  //falls URL-kodierte Zeichen vorhanden sind, diese nicht als Variablen betrachten (beides beginnt mit %)
  preg_match("/\"[^\"]+php\?([^\"]+)\"/", $exp, $urlParameter);
  if(isset($urlParameter[1]) && strpos($urlParameter[1], "%") !== false){
  	$orig_param = $urlParameter[1];
  	$repl_char = "§percent§";
  	$repl_param = str_replace("%", $repl_char, $orig_param);
  	$exp = str_replace($orig_param, $repl_param, $exp);
  }
  
  $r = preg_replace_callback("/([\\\$%][\w\d\.]+)/","__RepVarsCallback",$exp);
  
  if(isset($repl_param)) {
  	$r = str_replace($repl_char, "%", $r);
  }
  unset($__CurrentNSIndex);
  unset($__CurrentCallback);
  return $r;
}

function _getPhpVar($var,$NSIndex)
{
  if(is_null($var))                  return "NULL";
  if(empty($var))                    return "\"\"";
  if($v = _getVar($var,$NSIndex))    return $v;
  if($v = _getEscVar($var,$NSIndex)) return $v;
  if($v = _getArray($var,$NSIndex))  return $v;
  if(preg_match("/^\d+$/",$var))     return $var;  
  return "\""._getStrVar($var,$NSIndex)."\"";
}

function _getHtmlVar($var,$NSIndex)
{
  if(empty($var))                    return "";
  if($v = _getVar($var,$NSIndex))    return "<?php echo $v ?>";
  if($v = _getEscVar($var,$NSIndex)) return "<?php echo $v ?>";
  return _replaceVars($var,$NSIndex,"html");
}

function _getStrVar($var,$NSIndex)
{
  if(empty($var))                          return "";
  if($v = _getVar($var,$NSIndex))    return "{".$v."}";
  if($v = _getEscVar($var,$NSIndex)) return "\".$v.\"";
  return _replaceVars(addcslashes($var,"\""),$NSIndex,"str");
}

class TemplateCompiler
{
  var $_hasError = false;
  
  function _Error($Msg,$File = "",$Line = 0)
  {
    ErrorCallback(E_USER_WARNING,$Msg,dirname($_SERVER["SCRIPT_FILENAME"])."/".$File,$Line,array());
    $this->_hasError = true;
    return false;
  }

  function _check($Expression,$File,$Line)
  {  
    list($e,$r) = array_pad(explode("->",$Expression),2,false);
    if(!empty($e)) $this->_Error($e,$File,$Line);
    return $r;
  }

  function _execCmd(&$subs,&$blocks,$cmd,$param)
  {            
    if($cmd == "end")
    {
      $blocks[0]->finish();
      $b = array_shift($blocks);
      if($b->getName() == "sub") 
      {
        $subs[] = $b;
        if($b->Name == MAINSUB) return "Es wurde versucht mit \"END\" einen Block zu beenden der nie begonnen wurde.->return";
      }
      else 
      { 
        if(!isset($blocks[0])) return "Es wurde versucht mit \"END\" einen Block zu beenden der nie begonnen wurde.";
        $blocks[0]->add($b);
      }
      if(!empty($param)) return "Der Command \"END\" akzeptiert keine Parameter.";
      return "";
    }
    //if(!isset($blocks[0]) and $cmd != "sub") return "Es wurde versucht mit \"END\" einen Block zu beenden der nie begonnen wurde.";
    
    if(isset($blocks[0])) if($blocks[0]->isValidSection($cmd)) return $blocks[0]->newSection($cmd,$param);

    $class = "_Cmd_$cmd";
    if(!class_exists($class)) return "Der Command \"$cmd\" existiert nicht.";
    $class = new $class();
    if(isset($blocks[0])) 
    {
      if(!$class->isValidParent($blocks[0])) return "der Command \"$cmd\" darf sich nicht im Block \"".$blocks[0]->getName()."\" befinden";      
    } else {
    	if(!$class->isBlock())
    		return "Befehle m&uuml;ssen sich in einem Block befinden";
    }
    $r = $class->init($param);
    if($class->isBlock()) array_unshift($blocks,$class);
    else $blocks[0]->add($class);
    return $r;
  }

  function _parseFile($aLines,$aName,$AddWrap = false)
  {
    $subs = $blocks = array();
    $this->_execCmd($subs,$blocks,"sub",MAINSUB);

    $line = 0;
    foreach($aLines as $aline)
    {
      $line++;
      $cmds = explode("{",$aline);
      $this->_check($blocks[0]->add(array_shift($cmds)),$aName,$line);
      foreach($cmds as $v)
      {
        $l = explode("}",$v,2);
        if($AddWrap and (count($l) == 2)) if(in_array($l[1],array("\r\n","\n","\r"))) $l[1] .= $l[1];
        $a = array();
        if(count($l) != 2) $this->_check($blocks[0]->add("{".$v),$aName,$line); 
        elseif(preg_match("/^#([\w\d]+)( (.*))?$/",$l[0],$a))
        {
          if(!isset($a[3])) $a[3] = null;
          if($this->_check($this->_execCmd($subs,$blocks,strtolower($a[1]),trim($a[3])),$aName,$line) == "return") return $subs;
          $this->_check($blocks[0]->add($l[1]),$aName,$line);
        }
        elseif(preg_match("/^([\\\$%].*)$/",$l[0],$a))
        {
          $this->_check($this->_execCmd($subs,$blocks,"var",trim($a[1])),$aName,$line);
          $this->_check($blocks[0]->add($l[1]),$aName,$line);
        }
        elseif(preg_match("/^§(.*)$/",$l[0],$a))
        {
          $this->_check($this->_execCmd($subs,$blocks,"translate",trim($a[1])),$aName,$line);
          $this->_check($blocks[0]->add($l[1]),$aName,$line);
        }
        elseif(preg_match("/^!.*$/",$l[0]))
        {
          // comment (nichts ausf&uuml;hren ;) )
          $this->_check($blocks[0]->add($l[1]),$aName,$line);
        }
        else $this->_check($blocks[0]->add("{".$v),$aName,$line); 
      }
    }
    $this->_execCmd($subs,$blocks,"end","");
    if(count($blocks) > 0) $this->_Error("Es wurden nicht alle Bl&ouml;cke geschlossen",$aName,$line);
    return $subs;
  }  
  
  function compileFile($aFileName) {
    $f = file($aFileName);
    foreach($f as $k => $v) $f[$k] = chop($v)."\n";
    $subs = $this->_parseFile($f,$aFileName);
    foreach($subs as $sub)
    {                                   
      $fn = _GetTempName($aFileName,$sub->Name);   
      $sub->compileToFile($fn);
    }   
    return true;
  }
  
  function compileCode($TplCode,$name = "compiled code")
  {
    $this->_hasError = false;
  
    $c = explode("\n",$TplCode);
    foreach($c as $k => $v) $c[$k] .= "\n";
    
    $subs = $this->_parseFile($c,$name,true);
    if($this->_hasError) return false;
    if(count($subs) != 1) return $this->_Error("Compilierter Code darf keine SUBs enthalten.",$name,count($c));
    
    return $subs[0]->compile(0,0);
  }
}


class _Cmd
{

	//php 7 public function __construct()
	//php 5 original function _Cmd()
  function __construct()
  {
  }

  function init($aParam)
  {
    if(!empty($aParam)) return text_translate("Der Befehl '?1?' darf keine Parameter haben.", get_class($this));
    else return "";
  }

  function isValidParent($ParentClass)
  {
    return true;
  }

  function getName()
  {                   
    $a = array();
    preg_match('/_cmd_(.*)/i', get_class($this), $a);
    return strtolower($a[1]); 
  }                          
  
  function compile($NSIndex,$Depth)
  {
    return "";
  }
  
  function isBlock()
  {
    return false;	
  }
}

class _CmdBlock extends _Cmd
{
  function newSection($aName,$aParam)
  {
    return "der Block ".get_class($this)." darf keine Sektionen haben.";
  }

  function isValidSection($aName)
  {
    return false;
  }

  function add($data)
  {
    return "pure virtual function call (_Cmd->add).";
  }

  function finish()
  {
  }              
  
  function _compileArray($Array,$NSIndex,$Depth)
  {
    foreach($Array as $k => $v) { 
    	if(is_object($v)) {
    		$Array[$k] = $v->compile($NSIndex,$Depth);
    	}
    }
    return implode("",$Array);
  }   
  
  function isBlock()
  {
    return true;	
  }

}

 /**
 * Kapselt eine Menge Code, welcher mit Parametern ausgef&uuml;hrt werden kann.
 * 
 * <pre>
 * {#SUB name=subname}
 * {#END}
 * </pre>
 * 
 * Ein Sub ist vergleichbar mit einer Funktion. Er enth&auml;lt beliebigen Code, welcher ausgef&uuml;hrt wird, 
 * wenn der Sub mit {#VAR sub=subname} aufgerufen wird.
**/

class _Cmd_Sub extends _CmdBlock
{
  var $_Code = array();
  var $Name;

  function init($param)
  {
    $p = ParseParam($param,array("name"));
    if(!is_array($p)) return $p;
    if(!_isIdent($p["name"])) return text_translate("Der Name \"?1?\" ist f&uuml;r eine Subroutine ung&uuml;ltig. Er Darf nur aus Buchstaben, Ziffern und dem Unterstrich bestehen", $p["name"]);
    $this->Name = strtolower($p["name"]);
    return "";
  }

  function add($data)
  {
    $this->_Code[] = $data;
  }

  function isValidParent($ParentClass)
  {
    if("_cmd_sub" == strtolower(get_class($ParentClass))) return true;
    if(is_subclass_of($ParentClass,"_cmd_sub")) return true;
    return false;
  }   
  
  function compile($NSIndex,$Depth)
  {
    return $this->_compileArray($this->_Code,$NSIndex,$Depth);
  } 
  
  function compileToFile($aFileName)
  {
    $f = fopen($aFileName,"w");
    if(!$f) return ErrorCallback(E_FLIP_SYSERROR,"Konnte eine tempor&auml;re Datei nicht zum schreiben &ouml;ffnen.|FileName: $aFileName",__FILE__,__LINE__);
    
    //ignore notices which say "Notice: Undefined index: xxxx in /xx/x/xxxx on line n"
    //cause we cannot check every var which is dynamically created by templatecompiler
    $showerrors = false;
    $fileid = sprintf("%u", crc32($aFileName));
    $prefix = "<?php \$oldErrorLvl$fileid = error_reporting(E_ALL & ~E_NOTICE); ?>\n";
    $postfix = "<?php error_reporting(\$oldErrorLvl$fileid); ?>\n";
    if($showerrors) {
    	$prefix = $postfix = "";
    }
    
    $data = $prefix.$this->compile(0,0).$postfix;
    $data = convertSpecialChars($data);
    $data = utf8_encode($data);
    
    fwrite($f, $data);
    fclose($f);    
    return true;
  }
  
  	function compileAndReturn() {
	    //ignore notices which say "Notice: Undefined index: xxxx in /xx/x/xxxx on line n"
	    //cause we cannot check every var which is dynamically created by templatecompiler
	    
	    $showerrors = false;
	    $fileid = sprintf("%u", md5(time()));
	    $prefix = "<?php \$oldErrorLvl$fileid = error_reporting(E_ALL & ~E_NOTICE); ?>\n";
	    $postfix = "<?php error_reporting(\$oldErrorLvl$fileid); ?>\n";
	    
	    if($showerrors) {
	    	$prefix = $postfix = "";
	    }
	    
	    $data = $prefix.$this->compile(0,0).$postfix;
//	    $data = convertSpecialChars($data);
	    $data = utf8_encode($data);
	     
	    return $data;
	}
}

/**
 * Gibt den Inhalt einer Variablen aus oder ruft einen Sub auf.
 * 
 * <pre>
 * {#VAR var=$varname sub=subname file=filename [key1=val1]}
 * </pre>
 * 
 * Es muss mindestens einer der drei Parameter angegeben werden. Wird nur $varname angegeben, so wird der 
 * Inhalt der Variable ausgegeben. Es ist auch m&ouml;glich, die Variable statt mit einem $ mit einem % (%varname) 
 * zu kennzeichnen, dann wird ihr Inhalt vor dem Ausgeben durch htmlentities_single() geschickt.
 * 
 * Wenn sub oder file angegeben wurden, so wird der Sub subname in der Datei filename ausgef&uuml;hrt. Ist auch var angegeben,
 * so wird varname dem Sub als Namespace &uuml;berreicht. Varname sollte also ein Array sein. Es k&ouml;nnen noch beliebige weitere 
 * Attribute von #VAR angegeben werden, (hier z.B. key1=var1), welche als Variablen im Namespace des aufgerufenen 
 * SUbs wieder auftauchen. So lassen sich Subs "mit Parametern" aufrufen.
**/

class _Cmd_Var extends _Cmd
{
  var $_Var = "";
  var $_Sub = "";
  var $_File = "";
  var $_Vars = array();
  
  function init($param)
  {
    $p = ParseParam($param,array("var","sub","file"),false);
    if(!is_array($p)) return $p;
    $this->_Var  = (!isset($p["var"]) || preg_match("/^[\\\$%]$/",$p["var"])) ? "" : $p["var"];
    $this->_Sub  = $p["sub"];
    $this->_File = $p["file"];
    unset($p["var"]); unset($p["sub"]); unset($p["file"]);
    $this->_Vars = $p;
    if(empty($this->_Var) and empty($this->_Sub) and empty($this->_File))
      return text_translate("Es muss mindesten ein Parameter angegeben werden.");
    if((!empty($this->_File) or !empty($this->_Sub)) and !empty($this->_Var))
    {
      if(_isEscVar($this->_Var)) return text_translate("Ein Eingebundener Sub kann nicht wie eine HTML-Variable in HTML-Spezialzeichen konvertiert werden.");
      if(!_isVar($this->_Var)) return text_translate("\"?1?\" ist kein g&uuml;ltiger Variablenname.", $this->_Var);
    }
    return "";
  }
  
  function compile($NSIndex,$Depth)
  {
    if(($this->_Sub == "") and ($this->_File =="")) return _getHtmlVar($this->_Var,$NSIndex);
    
    $var = (empty($this->_Var)) ? "\$var$NSIndex" : _getPhpVar($this->_Var,$NSIndex);
    $sub = (empty($this->_Sub)) ? "\"".MAINSUB."\"" : _getPhpVar($this->_Sub,$NSIndex);
    if(empty($this->_File)) $this->_File = "\$".TPLFILE;
    if(count($this->_Vars) > 0)
    {
      $prep = "\$v = $var; ";
      $var = "\$v";
      foreach($this->_Vars as $k => $v)
        $prep .= "\$v["._getPhpVar($k,$NSIndex)."] = "._getPhpVar($v,$NSIndex)."; ";
    }
    else $prep = "";    
    return "<?php {$prep}TemplateOut("._getPhpVar($this->_File,$NSIndex).",$var,$sub); ?>";
  }		
}

/**
 * Executes some code for every entry of the given array
 * 
  * <pre>
 * {#FOREACH array=$list var=$item key=$itemkey index=$itemindex odd=$indexisodd limit=$limit} 
 * {#SEC}
 * {#MID}
 * {#ELSE}
 * {#FINALLY}
 * {#END}
 * </pre>
 * 
 * If no array is given, the current namespace is used.
 * 
 * --- Parameters ---
 * 
 * The variable $item will be set to the current entry of the array $list every iteration.
 * If var=$item is not used, every entry of $list should be an array itself.
 * The elements of this array are simply added to the current namespace.
 * 
 * The key of the current array element will be saved in the $itemkey variable.
 * 
 * $itemindex contains an serially numbered index, starting at 1 for the
 * first data record
 * 
 * In $indexisodd is stored, whether $itemindex-1 is odd. For the first data record,
 *  $indexisodd is 0, for the second 1, for the third 0 again, and so on.
 *  
 *  With $limit the maximal amount of processed data records can be set.
 *  For example you can limit the rows shown by the template.
 *  
 *  --- Sections ---
 *  
 *  Optional {#SEC}:
 *  If set, the code in {#SEC} will be displayed instead of every second entry of the
 *  array, forach is currently iterating over.
 *  
 *  Optional {#MID}:
 *  The content of the {#MID} Section will be displayed in between the data records.
 *  
 *  Optional {#ELSE}
 *  This code will be parsed and displayed if no valid array is given.
 *  More formally, the code is displayed, if "is_array($array) && !empty($array)"
 *  evaluates to false.
 *  
 *  Optional {#FINALLY}
 *  The code in the finally block is displayed, at the end of the iteration,
 *  even if limit is used.
 *  It will not be displayed, if the else part is displayed.
 *  
 *  Usage Examples:
 *  
 *  #1:
 *  Variables:
 *  $list = array(
 *   array("name" => "Peter", "age" => 28),
 *   array("name" => "Alice", "age" => 19),
 *   array("name" => "Bob", "age" => 21)
 * );
 *  
 *  Templatecode:
 *  
 *  {#FOREACH $list}
 *  	name: {%name}, age: {$age}
 *  	{#MID}
 *  	<br />
 *  	{#ELSE}
 *  	<b>No entries to display: <a href="#">Add new</a></b>
 *  	{#FINALLY}
 *  	<a href="#">Print list</a>
 * 	{#END}
 * 
 * Html result:
 * 
 * name: Peter, age: 28<br />
 * name: Alice, age: 19<br />
 * name: Bob, age: 21<br />
 * <a href="#">Print list</a>
 * 
 */

class _Cmd_Foreach extends _CmdBlock
{
  var $_VarKey = "";
  var $_VarVar = "";
  var $_VarArray = "";
  var $_VarIndex = "";
  var $_VarOdd = "";
  var $_VarLimit = "";
  var $_Code = array();
  var $_Section = "first";

  function init($param)
  {                                           
    $p = ParseParam($param,array("array","var","key","index","odd","limit"));
    if(!is_array($p)) return $p;
    if(isset($p["limit"]) && ($p["limit"] < 1)) return text_translate("Der Parameter limit muss mindestens 1 sein!");
    $this->_VarLimit = $p["limit"];
    unset($p["limit"]);
    foreach($p as $k => $v) if(!is_null($v) && !_isVar($v)) return text_translate("\"?1?\" ist keine g&uuml;ltige Variable.", $v);
    if(empty($p["array"])) return text_translate("Es wurde kein Array &uuml;bergeben");
    $this->_VarArray = $p["array"];
    $this->_VarVar   = $p["var"];
    $this->_VarKey   = $p["key"];
    $this->_VarIndex = $p["index"];
    $this->_VarOdd   = $p["odd"];
  //  if(empty($this->_VarArray)) return "Der Parameter \"array\" muss angegeben werden.";
    return "";
  }

  function newSection($aName,$aParam)
  {             
    if(!in_array($aName,array("else", "finally" ,"mid","sec"))) return "Ung&uuml;ltige Section in Foreach-Schleife: \"$aName\"";
    if(isset($this->_Code[$aName]) && is_array($this->_Code[$aName])) return "Die $aName-Section der Foreach-Schleife wurde bereits er&ouml;ffnet.";
    if(!empty($aParam)) return "Die Section \"$aName\" in der Foreach-Schleife darf keine Parameter haben";
                                   
    $this->_Section = $aName;
    $this->_Code[$aName] = array();
    return "";
  }

  function isValidSection($aName)
  {                             
    return (in_array($aName,array("else", "finally" ,"mid","sec"))) ? true : false;
  }

  function add($data)
  {                                   
    $this->_Code[$this->_Section][] = $data;
  }   
  
  function compile($NSIndex,$Depth)
  {                    
    $nk = "\$k$Depth";
    $nv = "\$v$Depth";  
    $ni = "\$i$Depth";  
    $nm = "\$m$Depth";
    if(empty($this->_VarArray)) $array = "\$var$NSIndex";
    else $array = _getVar($this->_VarArray,$NSIndex);
    
    if(!empty($this->_VarLimit))
    	$array = "array_slice($array,0,".$this->_VarLimit.")";
    
    $prepend = $key = $append = "";
    
    if(!empty($this->_VarVar)) $append .=  _getVar($this->_VarVar,$NSIndex)." = $nv; ";
    else $append .= "\$var".(++$NSIndex)." = array_merge(\$var".($NSIndex-1).",$nv); ";
    
    if(isset($this->_Code["mid"]) && is_array($this->_Code["mid"]))
    {
      $prepend .= "$nm = false; ";
      $append .= "if($nm) { ?>";
      $append .= $this->_compileArray($this->_Code["mid"],$NSIndex,$Depth+1);
      $append .= "<?php } else $nm = true; ";
    }

    if(!empty($this->_VarKey)) 
    {
      $key = "$nk => ";
      $append .= _getVar($this->_VarKey,$NSIndex)." = $nk; ";
    }
    
    $doindex = !empty($this->_VarIndex);
    $doodd   = !empty($this->_VarOdd);
    $index   = _getVar($this->_VarIndex,$NSIndex);
    $odd     = _getVar($this->_VarOdd,$NSIndex);
    
    if(isset($this->_Code["sec"]) && is_array($this->_Code["sec"]))
    {
      $prepend .= "$ni = 0; ";
      if($doindex) 
      {
        if($doodd) $append .= "$odd = ($ni %2); $index = ++$ni; if(!$odd) { ?>";
        else $append .= "$index = ++$ni; if(($ni % 2) == 1) { ?>";
      }
      else 
      {      
        if($doodd) $append .= "$odd = (($ni++) % 2); if(!$odd) { ?>";
        else $append .= "if((($ni++) % 2) == 1) { ?>";
      }
      $append .= $this->_compileArray($this->_Code["first"],$NSIndex,$Depth+1);
      $append .= "<?php } else { ?>";
      $append .= $this->_compileArray($this->_Code["sec"],$NSIndex,$Depth+1);
      $append .= "<?php } ";
    }
    else
    {
      if($doindex or $doodd) $prepend .= "$ni = 0; ";
      if($doindex) 
      {
        if($doodd) $append .= "$odd = ($ni % 2); $index = ++$ni; ?>";
        else $append .= "$index = ++$ni; ?>";
      }
      else
      {
        if($doodd) $append .= "$odd = (($ni++) % 2); ?>";
        else $append .= "?>";      
      }
      $append .= $this->_compileArray($this->_Code["first"],$NSIndex,$Depth+1);
      $append .= "<?php ";
    }
    
    if(isset($this->_Code["else"]) && is_array($this->_Code["else"]))
    	$else = " else {?>".$this->_compileArray($this->_Code["else"], $NSIndex, $Depth+1)."<?php }";
    else $else = "";
    if(isset($this->_Code["finally"]) && is_array($this->_Code["finally"]))
    	$finally = " ?>".$this->_compileArray($this->_Code["finally"], $NSIndex, $Depth+1)."<?php ";
    else $finally = "";
    
    return "<?php {$prepend}if(is_array($array) && !empty($array)) { foreach($array as $key$nv) { $append}$finally}$else ?>";
  }
}

/**
 * f&uuml;hrt den eingeschlossenen Code nur unter einer Bedingung aus.
 * 
 * <pre>
 * {#IF phpexpression}
 * {#ELSEIF phpexpression}
 * {#ELSE}
 * {#END} 
 * </pre>
 * 
 * Die klassische If-Bedingung inklusive optionalem elseif.
 * 
 * phpexpression ist g&uuml;ltiger PHP-Code, der im Namespace 
 * des Templates ausgef&uuml;hrt wird.
 * 
 * Beispiele:
 * <pre>
 * {#IF $var}foo{#END}
 * {#IF !$var}foo{#END}
 * {#IF !empty($var)}foo{#END}
 * {#IF $var1 == $var2}foo{#END}
 * </pre>
**/

class _Cmd_If extends _CmdBlock
{
  var $_Exp = "";
  var $_IfCode = array();
  var $_ElseCode = array();
  var $_Section = "";
  var $_ElseIf = array();
  var $_HasElse = false;
  
  function init($param)
  {
    if(empty($param)) return text_translate("Die If-Bedingung muss mindestens einen Parameter haben");
    $this->_Exp = $param;  
    return "";
  }
  
  function add($data)
  {
    switch($this->_Section)
    {
      case("elseif"): $this->_ElseIf[0]["code"][] = $data; break;
      case("else"): $this->_ElseCode[] = $data; break;       
      default: $this->_IfCode[] = $data; break;
    }
  }
  
  function newSection($aName,$aParam)
  {
    switch($aName)
    {
      case("else"):
        if(!empty($aParam)) return "Die Section \"ELSE\" in der If-Bedingung darf keine Parameter haben.";
        if($this->_Section == "else") return "Die \"ELSE\"-Section der If-Bedingung wurde bereits er&ouml;ffnet.";
        $this->_HasElse = true;
        break;
      case("elseif"):
        if(empty($aParam)) return "Die Section \"ELSEIF\" in der If-Bedingung ben&ouml;tigt einen Parameter.";
        if($this->_HasElse) return "Die Section \"ELSEIF\" in der If-Bedingung muss vor der \"ELSE\"-Section kommen.";
        array_unshift($this->_ElseIf,array("exp" => $aParam, "code" => array()));
        break;
      default: 
        return "Ung&uuml;ltige Section in If-Bedingung: \"$aName\"";
    }
    $this->_Section = $aName;
    return "";
  }

  function isValidSection($aName)
  {                             
    return (in_array($aName,array("else","elseif"))) ? true : false;
  }
  
  function compile($NSIndex,$Depth)
  {
    $r  = "<?php if("._replaceVars($this->_Exp,$NSIndex).") { ?>";
    $r .= $this->_compileArray($this->_IfCode,$NSIndex,$Depth);
    foreach(array_reverse($this->_ElseIf) as $elseif)
    {
      $r .= "<?php } elseif("._replaceVars($elseif["exp"],$NSIndex).") { ?>";
      $r .= $this->_compileArray($elseif["code"],$NSIndex,$Depth);   
    }
    if($this->_HasElse) $r .= "<?php } else { ?>".$this->_compileArray($this->_ElseCode,$NSIndex,$Depth);
    $r .= "<?php } ?>";
    return $r;  
  }
}

/**
 * f&uuml;hrt die Instanz einer Template-Klasse aus.
 * 
 * <pre>
 * {#TPL $tplinstance}
 * </pre>
 * 
 * $tplinstance enth&auml;lt eine Instanz der Klasse Template.
 **/

class _Cmd_Tpl extends _Cmd
{
  var $_Var = "";
  
  function init($param)
  {
    if(!_isVar($param)) return text_translate("\"?1?\" ist keine g&uuml;ltige Variable.", $param);
    $this->_Var = $param;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $ls = _getVar($this->_Var,$NSIndex);
    return "<?php if(is_a($ls,\"Template\")) {$ls}->out(); ?>";
  }
}

/**
 * Gibt den aktuellen Namespace oder andere Variablen aus.
 * 
 * <pre>
 * {#DEBUGPRINT $var}
 * </pre>
 * 
 * Wenn $var angegeben wird, wird sein Inhalt mittels print_r() ausgegeben. Ansonsten wird
 * der Inhalt des aktuellen Namespaces ausgegeben.
 **/

class _Cmd_DebugPrint extends _Cmd
{
  var $_Var = "";

  function init($param)
  {
    if(!empty($param)) if(!_isVar($param)) return text_translate("\"?1?\" ist keine g&uuml;ltige Variable.", $param);
    $this->_Var = $param;
    return "";    
  }

  function compile($NSIndex,$Depth)
  {
    $var = (empty($this->_Var)) ? "\$var$NSIndex" : _getPhpVar($this->_Var,$NSIndex);
    return "<pre><?php print_r($var); ?></pre>";
  }

}

/**
 * l&auml;dt eine Variable in den Namespace
 * 
 * <pre>
 * {#WITH $array}
 * {#END}
 * </pre>
 * 
 * Alle Keys mit ihren Values aus $array werden in den aktuellen Namespace &uuml;bernommen.
 * 
 **/

class _Cmd_With extends _CmdBlock
{
  var $_Var = "";
  var $_Code = array();
  
  function init($param)
  {
    if(!_isVar($param)) return text_translate("\"?1?\" ist keine g&uuml;ltige Variable.", $param);
    $this->_Var = $param;
    return "";    
  }
  
  function add($data)
  {
    $this->_Code[] = $data;
  }
  
  function compile($NSIndex,$Depth)
  {
    $r  = "<?php \$var".($NSIndex+1)." = array_merge(\$var{$NSIndex},"._getVar($this->_Var,$NSIndex)."); ?>";
     
    $r .= $this->_compileArray($this->_Code,$NSIndex+1,$Depth);
    return $r;    
  }
}

/**
 * Ein bedingter Befehl wie IF, aber ohne ein Block zu sein.
 * 
 * <pre>
 * {#WHEN if=$phpexpression then=first else=second}
 * </pre>
 * 
 * Wenn die $phpexpression (siehe #IF-Befehl) wahr ist, wird
 * first ausgegeben. Ansonsten second.
 * 
 * Beispiele:
 * <pre>
 * {#WHEN $var true false}
 * {#WHEN "$var == 1" "eine Stunde" "$var Stunden"}
 * </pre>
 **/

class _Cmd_When extends _Cmd
{
  var $_If = "";
  var $_Then = "";
  var $_Else = "";
  
  function init($param)
  {
    $p = ParseParam($param,array("if","then","else"));
    if(!is_array($p)) return $p;
    $this->_If   = $p["if"];
    $this->_Then = $p["then"];
    $this->_Else = $p["else"];
    if(empty($this->_If)) return text_translate("Der WHEN-Befehl braucht eine Bedingung.");
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $if   = _replaceVars($this->_If,$NSIndex);
    $then = _getPhpVar($this->_Then,$NSIndex);
    $else = _getPhpVar($this->_Else,$NSIndex);
    return "<?php echo ($if) ? $then : $else ?>";
  }
}

/**
 * f&uuml;hrt PHP-Code aus.
 * 
 * <pre>
 * {#LOAD func=phpexpression var=$var file=$file}
 * </pre>
 * 
 * Load erm&ouml;glicht es dem Template dynamisch ben&ouml;tigte Daten nachzuaden, indem es eine PHP-Funktion
 * ausf&uuml;hrt, und dessen R&uuml;ckgabewert in einer Variablen speichert.
 * 
 * phpexpression ist der auszuf&uuml;hrende PHP-Code
 * $var ist die Varibale, in welcher de R&uuml;ckgabewert des Codes abgelegt wird.
 * $file wird includet, bevor der Code ausgef&uuml;hrt wird.
 **/
class _Cmd_Load extends _Cmd
{
	var $_Func = "";
	var $_Var = "";
	var $_File = "";
	
	function init($param)
	{
		$p = ParseParam($param,array("func","var","file"));
	    if(!is_array($p)) return $p;
	    $this->_Func = $p["func"];
	    $this->_Var  = $p["var"];
	    $this->_File = $p["file"];
	    if(empty($this->_Func)) return text_translate("Das func-Attribut des LOAD-Befehls darf nicht leer sein.");
	    if(!empty($this->_Var)) if(!_isVar($this->_Var)) return text_translate("Das var-Attribut des LOAD-Befehls muss eine Variale sein.");
	    return "";
	}
	
	function compile($NSIndex,$Depth)
	{
		$var = (empty($this->_Var)) ? "\$var$NSIndex" : _getVar($this->_Var,$NSIndex);
		$func = _replaceVars($this->_Func,$NSIndex);
		$file = (empty($this->_File)) ? "" : "include_once("._getPhpVar($this->_File,$NSIndex)."); ";
		return "<?php $file$var = $func; ?>";	
	}
}

class _Cmd_LBrace extends _Cmd  // Befehle k&ouml;nnen so einfach sein *fg*
{
  function compile($NSIndex,$Depth)
  {
    return "{";
  }
}

/**
 * Gibt "}" aus.
 * 
 * <pre>
 * {#RBRACE}
 * </pre>
 **/

class _Cmd_RBrace extends _Cmd 
{
  function compile($NSIndex,$Depth)
  {
    return "}";
  }
}

/**
 * Formatiert eine Zahl mit 1000er-Trennzeichen und Nachkommastellen
 * 
 * <pre>
 * {#NUMBER val=$val prec=precision}
 * </pre>
 * 
 * $val ist eine beliebige Zahl
 * 
 * precision gibt die Anzahl der Nachkommastellen an. 
 **/

class _Cmd_Number extends _Cmd
{
  var $_Number = 0;
  var $_Prec = 0;
  
  function init($param)
  {
    $p = ParseParam($param,array("val","prec"));
    if(!is_array($p)) return $p;
    $this->_Number   = $p["val"];
    $this->_Prec   = $p["prec"];
    if(empty($this->_Prec)) $this->_Prec = 0;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $n = _getPhpVar($this->_Number,$NSIndex);
    $p = _getPhpVar($this->_Prec,$NSIndex);
    $thsdf = ",";
    $decf  = ".";
    //HACK to not rely on correct server configuration
    if(ConfigGet("translate_language")=="de_DE") {
    	$thsdf = ".";
	    $decf  = ",";
    }
    return "<?php echo number_format($n, $p, \"$decf\", \"$thsdf\"); ?>";
  }
}

/**
 * Formatiert einen Byte-Wert.
 * 
 * <pre>
 * {#BYTE val=$val format=format prec=precision}
 * </pre>
 * 
 * $val ist ein Integer-Wert, der eine gr&ouml;&szlig;e in Byte darstellt.
 * 
 * format ist b, kb, mb, gb oder tb.
 * 
 * precision gibt die Anzahl der Nachkommastellen an.
 **/

class _Cmd_Byte extends _Cmd
{
  var $_Byte = "";
  var $_Format = "";
  var $_Prec = 0;
  
  function init($param)
  {
    $p = ParseParam($param,array("val","format","prec"));
    if(!is_array($p)) return $p;
    $this->_Byte   = $p["val"];
    $this->_Format = strtolower($p["format"]);
    $this->_Prec   = $p["prec"];
    if(empty($this->_Byte)) return text_translate("Der BYTE-Befehl braucht einen Wert.");    
    if(empty($this->_Format)) $this->_Format = "kb";
    elseif(!in_array($this->_Format,array("b","kb","mb","gb","tb"))) return text_translate("Der Format-Wert des BYTE Befehls muss B, kB, MB, GB, oder TB sein.");
    if(empty($this->_Prec)) $this->_Prec = 0;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $b = _getPhpVar($this->_Byte,$NSIndex);
    $p = _getPhpVar($this->_Prec,$NSIndex);
    switch($this->_Format)
    {
      case("tb"): $b = "$b/".(1024*1024*1024*1024); break;
      case("gb"): $b = "$b/".(1024*1024*1024); break;
      case("mb"): $b = "$b/".(1024*1024); break;
      case("kb"): $b = "$b/1024"; break;
      case("b"):  break;    
    }
    if(_isNumber($p)) return "<?php echo sprintf(\"%.{$p}f\",$b); ?>";
    else return "<?php echo sprintf(\"%.\".($p).\"f\",$b); ?>";
  }
}

/**
 * Formatiert einen Unix-Timestamp mit der php-funktion date()
 * 
 * <pre>
 * {#DATE val=$timestamp format=format}
 * </pre>
 * 
 * f&uuml;r die genaue Bedeutung der Attribute siehe die PHP-Dokumentation
 * der Funktion date()
**/

class _Cmd_Date extends _Cmd
{
  var $_Time = 0;
  var $_Format = "";
  
  function init($param)
  {
    $p = ParseParam($param,array("val","format"));
    if(!is_array($p)) return $p;
    $this->_Time   = $p["val"];
    $this->_Format = $p["format"];
    if(empty($this->_Format)) $this->_Format = "d.m.y H:i";
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $f = _getPhpVar($this->_Format,$NSIndex);
    $t = _getPhpVar($this->_Time,$NSIndex);
    if(empty($this->_Time)) return "<?php echo date($f) ?>";
    else return "<?php echo (empty($t)) ? \"\" : date($f,$t) ?>";
  }
}

/**
 * Formatiert einen Unix-Timestamp mit der php-funktion strftime()
 * 
 * <pre>
 * {#STRFTIME val=$timestamp format=format}
 * </pre>
 * 
 * f&uuml;r die genaue Bedeutung der Attribute siehe die PHP-Dokumentation
 * der Funktion strftime(). Achtung: Das datumsformat muss hier mit
 * einem "§" anstelle des "%" angegeben werden.
**/

class _Cmd_StrFTime extends _Cmd
{
  var $_Time = 0;
  var $_Format = "";
  
  function init($param)
  {
    $p = ParseParam($param,array("val","format"));
    if(!is_array($p)) return $p;
    $this->_Time   = $p["val"];
    $this->_Format = str_replace('§', '%', $p["format"]);
    
    if(empty($this->_Format)) {
    	$this->_Format = "§d.§m.§Y §H:§I";
    }
    
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
//    $f = _getPhpVar($this->_Format, $NSIndex);
    $t = _getPhpVar($this->_Time, $NSIndex);
    
    // Aktuelle Zeit nehmen
    if(empty($this->_Time)) {
    	return "<?php echo strftime('" . $this->_Format . "') ?>";
    }
    
    // Mitgelieferte Zeit nehmen
    else {
    	return "<?php echo (empty($t)) ? \"\" : strftime('" . $this->_Format . "', $t) ?>";
    }
  }
}

/**
 * Encodiert einen String in Base64
 * 
 * <pre>
 * {#BASE64 $data}
 * </pre>
 **/

class _Cmd_Base64 extends _Cmd
{
  var $_Val = "";
  
  function init($param)
  {
    if(!_isVar($param)) return text_translate("\"?1?\" ist keine g&uuml;ltige Variable", $param);
    $this->_Val = $param;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $v = _getPhpVar($this->_Val,$NSIndex);
    return "<?php echo base64_encode($v) ?>";
  }
}

/**
 * Encodiert einen String mittels urlencode()
 * 
 * <pre>
 * {#URL $data}
 * </pre>
 */

class _Cmd_URL extends _Cmd
{
  var $_Val = "";
  
  function init($param)
  {
    $this->_Val = $param;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $v = _getPhpVar($this->_Val,$NSIndex);
    return "<?php echo urlencode($v) ?>";
  }
}

/**
 * Encodiert einen String mittels htmlentities_single()
 * 
 * <pre>
 * {#HTML $data}
 * </pre>
 */

class _Cmd_HTML extends _Cmd
{
  var $_Val = "";
  
  function init($param)
  {
    $this->_Val = $param;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $v = _getPhpVar($this->_Val,$NSIndex);
    return "<?php echo htmlentities_single($v) ?>";
  }
}

/** 
 * Bearbeitet eine URL mittels EditURL().
 * Weitere Dokumentation dazu findet sich in core/core.utils.php
 * 
 * <pre>
 * {#EDITURL key1=val1 key2=val2}
 * </pre>
 **/
 
class _Cmd_EditURL extends _Cmd
{
  var $_Vals = "";
  
  function init($param)
  {
    $this->_Vals = ParseParam($param,array(),false);
    if(!is_array($this->_Vals)) return $this->_Vals;
    else return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $r = array();
    foreach($this->_Vals as $k => $v) $r[] = _getPhpVar($k,$NSIndex)." => "._getPhpVar($v,$NSIndex);
    return "<?php echo EditURL(array(".implode(", ",$r).")) ?>";
  }
}

/** 
 * Bestimmt den Dateinamen einer Templatedatei in Abh&auml;ngigkeit des vom User ausgew&auml;hlten Templates.
 * 
 * <pre>
 * {#STATICFILE file=file default=def}
 * </pre>
 * 
 * file ist ein Dateiname relaiv zum Verzeichnis des Templates. Ausgegeben wird der 
 * Dateiname einer existierenden Datei relativ zum Wurzelverzeichnis vom FLIP.
 * 
 * Wenn die mit file angegebene Datei nicht existiert, wird versucht die Datei default zu nehmen.
 **/
 
class _Cmd_StaticFile extends _Cmd
{
  var $_File = "";
  var $_Def = "";
  
  function init($param)
  {
	$p = ParseParam($param,array("file","default"));
	if(!is_array($p)) return $p;
	$this->_File = $p["file"];
	$this->_Def = $p["default"];
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $v = _getPhpVar($this->_File,$NSIndex);
    if(empty($this->_Def)) return "<?php echo getTplFileDir($v).$v; ?>";
	$d = _getPhpVar($this->_Def,$NSIndex);
	return "<?php echo (\$d = getTplFileDir($v,true)) ? \$d.$v : getTplFileDir($d).$d ?>";
  }
}

/**
 * Macht einen String uppercase.
 * 
 * <pre>
 * {#TOUPPER $string}
 * </pre>
 **/

class _Cmd_ToUpper extends _Cmd
{
  var $_Str = "";
  
  function init($param)
  {
    $this->_Str = $param;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $v = _getPhpVar($this->_Str,$NSIndex);
    return "<?php echo strtoupper($v); ?>";
  }
}


/**
 * Takes the first char of the string, converts it to uppercase
 * and prints the result.
 * 
 * <pre>
 * {#UCFIRST $toCaps}
 * </pre>
 * 
 * @author Scope
 *
 */
class _Cmd_UcFirst extends _Cmd {
	var $_String = '';
	
	function init($aParam) {
		$this->_String = $aParam;
		
		return '';
	}
	
	function compile($NSIndex,$Depth) {
		$v = _getPhpVar($this->_String, $NSIndex);
		return "<?php echo ucfirst($v); ?>";
	}
}

/**
 * Uebersetzt ein Text
 * 
 * <pre>
 * {§a simple text}
 * {§"a text with ?1? variable(s)" $n}
 * {#TRANSLATE text="full format of ?1? ?2?" param="array(0=a 1=\"text translation\")"}
 * </pre>
 **/

class _Cmd_Translate extends _Cmd
{
  var $_Str = "";
  var $_Param = array();
  
  function init($param)
  {
    //default
    $this->_Str = $param;
    //if text contains a variable, then we need a second parameter
    if(strpos($param, "?1?") !== false) {
		$p = ParseParam($param,array("text","param"));
		if(!is_array($p)) {
			return $p;
		}
	    $this->_Str = $p["text"];
    	$this->_Param = $p["param"];
    }
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $v = _getPhpVar($this->_Str,$NSIndex);
    $p = _getPhpVar($this->_Param,$NSIndex);
    return "<?php echo text_translate($v, $p); ?>";
  }
}

?>
