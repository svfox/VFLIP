<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.template.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.TEMPLATE.PHP")) return 0;
define("MOD.TEMPLATE.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

define("MAINSUB","mainsubpartoffile"); 
define("TPLFILE","__FILE__");
define("TPLSUB","__SUB__"); 
define("TPLDIR","__DIR__");
define("TPLSCRIPT","__SCRIPT__");
define("TPLURI","__URI__");

$TemplateCompiler = array(
  "mod/mod.template.compiler.php",
  "mod/mod.template.flipcompiler.php"
);

function _InitTemplateEngine()
{
  global $TemplateCompiler,$NewestCompiler,$User;
  $NewestCompiler = 0;
  foreach($TemplateCompiler as $t) if(filemtime($t) > $NewestCompiler) $NewestCompiler = filemtime($t);
}

function _GetTempName($FileName,$SubName = MAINSUB)
{
  $tmp = ConfigGet("template_temp");
  ForceDir($tmp,E_FLIP_SYSERROR);
  return $tmp.md5($FileName.$SubName).".php";
}

function getTplFileDir($aFileName, $TryOnly = false)
{
  $dir = ConfigGet("template_dir");
  $basename = ConfigGet("template_basename");
  $customname = ConfigGet("template_customname");
  
  $c = "$dir$customname/";
  $b = "$dir$basename/";
  
  if(is_file("$c$aFileName"))     $dir = $c;
  elseif(is_file("$b$aFileName")) $dir = $b;
  else
  {
    $cwd = getcwd();
    if(!$TryOnly) trigger_error_text(text_translate("Eine Templatedatei wurde nicht gefunden.")."|Filename:$aFileName, Basename:$basename, Customname:$customname, Dir:$dir, Paths:$cwd/$c;$cwd/$b",E_USER_WARNING);
    return false;
  }
  return $dir;
}

class Template 
{
  var $Vars;
  var $File;
  var $Sub;
  
	//php 7 public function __construct()
	//php 5 original function Template()
  function __construct($TplFile = "",$Vars = array(),$SubName = MAINSUB)
  {
    $this->File = $TplFile;
    $this->Sub  = strtolower($SubName);
    $this->Vars = $Vars;
  }
  
  function _prepareTempFile()
  {            
    global $TplTempCache,$TemplateCompiler,$NewestCompiler;
    if(!($dir = getTplFileDir($this->File))) return false;
     
    $this->Vars[TPLFILE]   = $this->File;
    $this->Vars[TPLSUB]    = $this->Sub;
    $this->Vars[TPLDIR]    = $dir;
    $this->Vars[TPLSCRIPT] = basename($_SERVER["PHP_SELF"]);
    $u = array();
    foreach($_GET as $k => $v) 
      if(is_array($v)) foreach($v as $w) $u[] = "{$k}[]=".urlencode($w);
      else $u[] = "$k=".urlencode($v);
      
    $this->Vars[TPLURI] = $this->Vars[TPLSCRIPT].((empty($u)) ? "" : "?".implode("&amp;",$u));
       
    $cacheid = "{$this->File}-{$this->Sub}";
    if(isset($TplTempCache[$cacheid])) return $TplTempCache[$cacheid];

    $file = "$dir{$this->File}";
    $TmpName = _GetTempName($file,$this->Sub);
    if(!is_file($TmpName) or (filemtime($TmpName) < filemtime($file)) or (filemtime($TmpName) < $NewestCompiler))
    {
      foreach($TemplateCompiler as $c) include_once($c);
      $t = new TemplateCompiler();
      if(!$t->compileFile($file)) {
		return false;
      }
    }
    if(is_file($TmpName)) return $TplTempCache[$cacheid] = $TmpName;
    ErrorCallback(E_FLIP_SYSERROR,text_translate("Der aufgerufene Template-Sub existiert nicht, oder ein interner Fehler beim compilieren des Templates ist aufgetreten.")."|Tpl: {$this->File} Sub:{$this->Sub}",__FILE__,__LINE__);
    return false;
  }              
  
  function out()
  {
    $file = $this->_prepareTempFile();
    $var0 = $this->Vars;
    if(!$file) return false;
    ob_start();
    
    include($file);
    
    $contents = convertSpecialChars(ob_get_contents());
    ob_end_clean();
    echo $contents;
    
    return true;  
  }
  
  function get()
  {
    $file = $this->_prepareTempFile();
    $var0 = $this->Vars;
    if(!$file) return false;
    ob_start();
    include($file);
    $r = convertSpecialChars(ob_get_contents());
    
    ob_end_clean();
    return $r;  
  }
}

function TemplateOut($TplFile,$Vars,$SubName = MAINSUB)
{
  $t = new Template($TplFile,$Vars,$SubName);
  $t->out();
}

function TemplateCompile($TplCode,$Name = "compiled code")
{
  global $TemplateCompiler;
  foreach($TemplateCompiler as $c) include_once($c);
  $t = new TemplateCompiler();
  if($c = $t->compileCode($TplCode,$Name)) return $c;
  trigger_error_text(text_translate("Beim compilieren des Codes ist ein Fehler aufgetreten."),E_USER_WARNING);
  return false;
}

function TemplateExecute($CompiledCode,$Vars)
{
  if (empty($CompiledCode)) return "";
  $var0 = $Vars;
  ob_start();
  eval("?>$CompiledCode<?php ;");
  $r = ob_get_contents();
  ob_end_clean();
  return $r;
}

_InitTemplateEngine();

?>