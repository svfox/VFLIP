<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.imageedit.php 1702 2019-01-09 09:01:12Z docfx $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined('MOD.IMAGEFONT.PHP')) return 0;
define('MOD.IMAGEFONT.PHP',1);

require_once('mod/mod.image.php');
require_once('mod/mod.imageedit.php');

class ImageFont extends _AbstractImage
{
  var $Res;
  var $Chars;
  var $OrigWidth;
  var $OrigHeight;
  var $CharWidth;
  var $CharHeight;
  var $DefChar;
  var $OptionHCenter = false;
  var $OptionHSpacing;
  var $OptionVSpacing;
  
	//php 7 public function __construct($Img,$aChars,$aDefChar = null)
	//php 5 original function ImageFont($Img,$aChars,$aDefChar = null)
  function __construct($Img,$aChars,$aDefChar = null)
  {
	//php 5:
	//parent :: _AbstractImage();
	//php7 neu:		
	parent::__construct($Img);
	
    if(is_object($this->_Loader)) $this->Res = $this->_Loader->getRes();
    else trigger_error_text("Eine Schriftart konnte nicht geladen werden.|Image:".var_export($Img)." Chars:$aChars",E_USER_WARNING);
    
    $this->OrigWidth = $this->CharWidth = round(imagesx($this->Res) / strlen($aChars));
    $this->OrigHeight = $this->CharHeight = imagesy($this->Res);
        
    for($i = 0; $i < strlen($aChars); $i++)
      $this->Chars[$aChars[$i]] = $i * $this->OrigWidth;
      
    if(is_null($aDefChar)) $this->DefChar = $aChars[0];
    else $this->DefChar = $aDefChar;
  }
  
  function writeChar($aResource, $Left, $Top, $Char)
  {
    if(!isset($this->Chars[$Char])) 
    {
      $l = strtolower($Char);
      $u = strtoupper($Char);
      if(isset($this->Chars[$l])) $Char = $l;
      elseif(isset($this->Chars[$u])) $Char = $u;
      else $Char = $this->DefChar;    
    }
    if(($this->OrigHeight == $this->CharHeight) and ($this->OrigWidth == $this->CharWidth))
    {
      imagecopy($aResource,$this->Res,$Left,$Top,$this->Chars[$Char],0,$this->OrigWidth,$this->OrigHeight);
    }
    else
    {
      imagecopyresampled($aResource,$this->Res,$Left,$Top,$this->Chars[$Char],0,$this->CharWidth,
                         $this->CharHeight,$this->OrigWidth,$this->OrigHeight);
    }
  }
  
  function cleanString($text) {
    $r = "";
    for($i = 0; $i < strlen($text); $i++) {
      switch($text[$i]) {
        case('ä'):
          $r .= "ae";
          break;
        case('ö'):
          $r .= "oe";
          break;
        case('ü'):
          $r .= "ue";
          break;
        case('Ä'):
          $r .= "Ae";
          break;
        case('Ö'):
          $r .= "Oe";
          break;
        case('Ü'):
          $r .= "Ue";
          break;
        case('ß'):
          $r .= "ss";
          break;
        default:
          $r .= $text[$i];
      }    
    }
    return $r;
  }
  
  function calcWidth($Text)
  {
    return strlen($Text) * $this->CharWidth;
  }
  
  function writeText($aResource, $Left, $Top, $aText, $MaxWidth = null)
  {
    $aText = $this->cleanString($aText);
    
    if($this->OptionHCenter)
      $Left -= round($this->calcWidth($aText) / 2);

    $x = $Left;
    $y = $Top;
    for($i = 0; $i < strlen($aText); $i++) 
    {
      switch($aText[$i])
      {
        case("\n"):
          $x = $Left;
          $y += $this->CharHeiht + $this->OptionVSpacing;
          break;
          
        case(' '):
          $x += $this->OptionHSpacing + $this->CharWidth;
          break;
          
        case("\t"):
          $x += ($this->OptionHSpacing + $this->CharWidth) * 4;
          break;
          
        case("\r"):
          break;

        default:
          if(!is_null($MaxWidth) and (($x-$Left) + $this->OptionHSpacing + $this->CharWidth) > $MaxWidth) break;
          $this->writeChar($aResource,$x,$y,$aText[$i]);
          $x += $this->OptionHSpacing + $this->CharWidth;
      }
    }  
  }
  
  function setCenter($hCenter, $vCenter)
  {  
    // $vCenter ist not yet implemented.
    $this->OptionHCenter = $hCenter;  
  }
  
  function setSpacing($hSpacing, $vSpacing) 
  {
    $this->OptionHSpacing = $hSpacing;
    $this->OptionVSpacing = $vSpacing;  
  }
  
  function setSize($width, $height) 
  {
    $this->CharWidth = $width;
    $this->CharHeight = $height;
  }
  
  function setSizeFactor($factor)
  {
    $this->CharWidth = round($this->OrigWidth * $factor);
    $this->CharHeight = round($this->OrigHeight * $factor);
  }
}


?>