<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.seats.php 1702 2019-01-09 09:01:12Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/**
 * Liste der Bilder
 */
function _GetSeatImages($Dir) {
	static $Images = array ();
	if (isset ($Images[$Dir]))
		return $Images[$Dir];
	$d = @ opendir($Dir);
	if (!$d)
		trigger_error_text("Das Verzeichnis f&uuml;r die Sitzplanicons kann nicht ge&ouml;ffnet werden.|seats_imagedir:$Dir", E_USER_ERROR);
	$a = array ();
	while ($n = readdir($d))
		if (preg_match("/^(large|small|normal)_([a-z_]+)(_(\d+))?\.(jpg|png)$/i", $n, $a))
			$Images[$Dir][strtolower($a[1])][strtolower($a[2])][(int) $a[4]] = $Dir.$n;
	closedir($d);
	return $Images[$Dir];
}

function GetSeatImage($Dir, $Type, $Size = "small", $ReadInfo = true) {
	$Images = _GetSeatImages($Dir);
	$TypeAlt = $Type;
	if (!isset ($Images[$Size][$Type]))
		$Type = "default";
	if (!isset ($Images[$Size][$Type])) {
		trigger_error_text("Ein Sitzplan-Bild existiert nicht.|dir: $Dir size: $Size type: $TypeAlt, $Type ", E_USER_WARNING);
		return array ();
	}
	$r = array ("imgname" => array_shift($Images[$Size][$Type]));
	if ($ReadInfo)
		list ($r["width"], $r["height"]) = getimagesize($r["imgname"]);
	return $r;
}

function LoadSeatImage($Dir, $Type, $Angle = 0, $Large = false, $TryOnly = false) {
	include_once ("mod/mod.imageedit.php");
	static $cache = array ();
	if (isset ($cache[$Dir][$Type][$Angle][$Large]))
		return $cache[$Dir][$Type][$Angle][$Large];

	$Images = _GetSeatImages($Dir);
	$size = ($Large) ? "large" : "small";
	if (!isset ($Images[$size][$Type])) {
		if ($TryOnly)
			return false;
		else
			$Type = "default";
	}
	$c = 0;
	if (isset ($Images[$size][$Type][$Angle]))
		$name = $Images[$size][$Type][$Angle];
	else {
		$i = $Images[$size][$Type];
		$diff = 360;
		foreach ($i as $a => $f) {
			$d = min(abs($Angle - $a), abs($Angle - ($a +360)));
			if ($d < $diff) {
				$c = $a;
				$diff = $d;
			}
		}
		$name = $Images[$size][$Type][$c];
		if (isset ($cache[$Dir][$Type][$c][$Large]))
			return $cache[$Dir][$Type][$Angle][$Large] = $cache[$Dir][$Type][$c][$Large];
	}
	return $cache[$Dir][$Type][$Angle][$Large] = $cache[$Dir][$Type][$c][$Large] = new ResImage(new ImageLoaderFile($name));
}

function GetSeatColor($row) {
	if ($row["enabled"] == "N")
		return "grey";
	if ($row["user_id"] == 0)
		return "green";
	if ($row["reserved"] == "Y")
		return "red";
	return "yellow";
}

function _getSeatImageClass($dir) {
	static $cache = array ();

	if (isset ($cache[$dir]))
		return $cache[$dir];

	$file = "{$dir}images.php";
	if (is_file($file))
		include_once ($file);
	else
		$ImageClass = "AbstractSeatImages";

	if (empty ($ImageClass))
		trigger_error_text("Fehler beim Laden einer Bildklasse|Die Variable \$ImageClass wurde in $file nicht definiert.", E_USER_ERROR);
	if (!class_exists($ImageClass))
		trigger_error_text("Die Klasse '$ImageClass' existiert nicht.|file: $file", E_USER_ERROR);

	return $cache[$dir] = new $ImageClass ($dir);
}

function DrawSeatSelected($Dir, $Image, $Seat) {
	$class = _getSeatImageClass($Dir);
	return call_user_func(array($class, 'DrawSeatSelected'), $Image, $Seat);
}

function DrawSeatUnselected($Dir, $Image, $Seat) {
	$class = _getSeatImageClass($Dir);
	return $class->DrawSeatUnselected($Image, $Seat);
}

function SeatsGetLegend($Dir) {
	$class = _getSeatImageClass($Dir);
	return call_user_func(array($class, 'getLegend'));
}

class AbstractSeatImages {

	var $Path = "";

	function loadImage($filename) {
		include_once ("mod/mod.imageedit.php");
		static $cache = array ();
		if (isset ($cache[$filename]))
			return $cache[$filename];
		$file = $this->Path.$filename;
		if (is_file($file))
			$r = new ResImage(new ImageLoaderFile($file));
		else
			$r = false;
		return $cache[$filename] = $r;
	}
	//original function AbstractSeatImages
	//php 7 public function __construct()
	//php 5 original function funcAbstractSeatImages()
	function __construct($aPath) {
		$this->Path = $aPath;
	}

	function DrawSeatSelected($Image, $Seat) {
	}

	function DrawSeatUnselected($Image, $Seat) {
		include_once ("mod/mod.user.php");
		$col = GetSeatColor($Seat);

		$userstatus = $Seat["user_status"];

		// versuche, ein statusicon aus zu lesen. wenn es existiert, wird es &uuml;ber den sitzplatz gelegt.
		$table = $stat = false;
		if (!empty ($userstatus) and ($col == "red")) {
			$stat = LoadSeatImage($this->Path, "status$userstatus", $Seat["angle"], false, true);
			if (!$stat)
				$table = LoadSeatImage($this->Path, "table$userstatus", $Seat["angle"], false);
		}
		if (!$table)
			$table = LoadSeatImage($this->Path, "table$col", $Seat["angle"], false);
		//			if(!$table) trigger_error("err!-{$this->Path}-$col",E_USER_ERROR);

		// tisch und overlay-status zeichnen  
		$cords = $Image->drawRotated($table, $Seat["center_x"], $Seat["center_y"], $Seat["angle"], true);
		if ($stat)
			$Image->drawRotated($stat, $Seat["center_x"], $Seat["center_y"], $Seat["angle"], false);
		return $cords;
			
	}

	function getLegend() {
		return array (array 
		("img1" => GetSeatImage($this->Path, "tablegreen", "small", true), "desc1" => "Ein freier Sitzplatz.", "img2" => GetSeatImage($this->Path, "tableyellow", "small", true),
		"desc2" => "Ein vorgemerkter Sitzplatz."),
		array ("img1" => GetSeatImage($this->Path, "tablegrey", "small", true),
		"desc1" => "Ein deaktivierter Sitzplatz.", "img2" => GetSeatImage($this->Path,
		"tablered", "small", true), "desc2" => "Ein reservierter Sitzplatz."),
		array ("img1" => GetSeatImage($this->Path, "tablechecked_in", "small", true),
		"desc1" => "Ein Sitzplatz mit eingechecktem User.", "img2" => GetSeatImage($this->Path, "tableonline", "small", true),
		"desc2" => "Ein Sitzplatz mit User, der online ist."),
		array ("img1" => GetSeatImage($this->Path, "tableoffline", "small", true), "desc1" => "Ein Sitzplatz mit User, der offline ist.",
		"img2" => GetSeatImage($this->Path, "tablechecked_out", "small", true), "desc2" => "Ein Sitzplatz mit ausgechecktem User."),
		array ("img1" => GetSeatImage($this->Path, "chair", "small", true),
		"desc1" => "Diese Markierung kennzeichnet Sitzpl&auml;tze,<br>die du reserviert oder vorgemerkt hast,<br>bzw. die auf deine Suchanfrage zutreffen."));
	}

}
?>
