<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.lastvisit.php 1351 2007-01-30 13:36:09Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/
 
/** FLIP-Kern */
require_once ("core/core.php");

function LastvisitOldest() 
{
  return time() - (ConfigGet("lastvisit_timespan") * 60 * 60 * 24);
}

function LastvisitCleanup()
{
  static $hasdone = false;
  if($hasdone) return true;
  $hasdone = true;
  $oldest = LastvisitOldest();
  return MysqlWrite("DELETE FROM `".TblPrefix()."flip_lastvisit` WHERE (FROM_UNIXTIME($oldest) > `mtime`);");
}

function doVisit($aName)
{
  global $User,$Session;
  // f&uuml;r anonymous nichts speichern.
  if(!$User->hasRight("logged_in")) return time();
  $name = addslashes($aName);
  $row = MysqlReadRow("
    SELECT UNIX_TIMESTAMP(`mtime`) AS `time`, `id` 
      FROM `".TblPrefix()."flip_lastvisit` 
      WHERE ((`name` = '$name') AND (`user_id` = '{$User->id}'));
    ",true);
  if(is_array($row)) MysqlWriteByID(TblPrefix()."flip_lastvisit",array("mtime" => NULL), $row["id"]);
  else MysqlWriteByID(TblPrefix()."flip_lastvisit",array("mtime" => NULL, "name" => $aName, "user_id" => $User->id), $row["id"]);
  return (empty($row["time"])) ? LastvisitOldest() : $row["time"];
}

function forceVisit($aName)
{
  global $User,$Session;
  if(!$User->hasRight("logged_in")) return false;
  $name = addslashes($aName);
  LastvisitCleanup();
  return MysqlWrite("REPLACE `".TblPrefix()."flip_lastvisit` SET `mtime` = NULL, `user_id` = '{$User->id}', `name` = '$name';");
}

function getVisits($Names)
{
  global $User,$Session;
  if(!is_array($Names)) return array();
  if(count($Names) < 1) return array();
  if(!$User->hasRight("logged_in"))
  {
    $r = array();
    foreach($Names as $v) $r[$v] = time();
    return $r;
  }
  $n = $r = array();
  $cols = MysqlReadCol("
    SELECT `name`, UNIX_TIMESTAMP(`mtime`) AS `time` FROM `".TblPrefix()."flip_lastvisit`
      WHERE ((`user_id` = '{$User->id}') AND (`name` IN (".implode_sqlIn($Names).")));
  ","time","name");
  $oldest = LastvisitOldest();
  foreach($Names as $v)
    $r[$v] = (empty($cols[$v])) ? $oldest : $cols[$v];
  return $r;
}


?>