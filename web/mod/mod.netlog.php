<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.netlog.php 1384 2007-03-15 23:20:53Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/
 
/** Netlog-Protokoll */
require_once("mod/mod.netlog.protocol.php");

/**
 * NetLogSubmitIPv4() Sendet eine IPv4 und weitere Daten an den netlogd.
 * 
 * @param $Keys
 **/
function NetLogSubmitIPv4($Keys)
{
  $Server = ConfigGet("netlog_server");
  if(empty($Server)) return NetLogSaveData($Keys);
  else
  {
    $p = new NetLogPacket();
    $p->Command = "submit_ipv4";
    $p->Keys = $Keys;
    $r = $p->send($Server,5);
    if(is_object($r)) $r->handleResponse();
  }
}

function NetLogSaveData($Keys)
{
  $cols = NetLogGetCols();
  foreach($Keys as $k => $v) if(!in_array($k,$cols))
  {
    trigger_error_text("NetLog: ein Key ist ung&uuml;ltig und kann deshalb nicht gespeichert werden.|key:$k value:$v",E_USER_WARNING);
    unset($Keys[$k]);
  }
  
  $id = MysqlWriteByID(TblPrefix()."flip_netlog_log",$Keys);
  if(!$id) return;
  
  $statcols = array("mac","ipv4","nbname","dnsname","user_id","user_ip");
  foreach($statcols as $k) {
  	if(!isset($Keys[$k])) {
  		$Keys[$k] = null;
  	}
  	_NLStat($id,$k,$Keys[$k]);
  }
  NetLogSetUserStatus($Keys);
}

function NetLogSetUserStatus($Keys)
{
 /* if(empty($Keys["ipv4"])) return;
  include_once("mod/mod.user.php");  
  $uid = UserGetUserIDByIP($Keys["ipv4"]);
  if(!$uid) return;
  $u = CreateSubjectInstance($uid,"user");
  $stat = UserGetStatus($u);
  if(!in_array($stat,array("checked_in","online","offline")))
    return trigger_error("Ein User muss mindestens eingecheckt sein, damit sein onlinestatus gesetzt werden kann.|User:{$u->name}[{$u->id}] stat:$stat action:{$Keys[action]}",E_USER_WARNING);
  if($stat == "checked_out")
    return trigger_error("Der User ist bereits ausgecheckt, deswegen kann sein onlinestatus nicht gesetzt werden.|User:{$u->name}[{$u->id}] stat:$stat action:{$Keys[action]}",E_USER_WARNING);
  switch($Keys["action"])
  {
    case("host_offline"): UserSetStatus($u,"offline"); break;
    default: UserSetStatus($u,"online"); break;  
  }*/
}

function NetLogGetCols()
{
  static $cols;
  if(is_array($cols)) return $cols;
  return $cols = MysqlReadCol("SHOW COLUMNS FROM `".TblPrefix()."flip_netlog_log`;","Field");
}

function _NLStat($log_id,$key,$val)
{
  if(empty($val)) return;
  $i = addslashes($log_id);
  $k = addslashes($key);
  $v = addslashes($val);
  return MysqlWrite("REPLACE `".TblPrefix()."flip_netlog_status` SET `log_id` = '$i', `key` = '$k', `value` = '$v';");
}


/**
 * NetLogCheckTime() Pr&uuml;ft die Zeit, ob sie sich in akzeptablem Rahmen befindet.
 * H&auml;ufig kahm es vor, dass auf dem Host das netlogds die Uhrzeit nicht richtig gestellt war.
 * In diesem Falle soll &uuml;ber NetLogCheckTime eine warnung ausgegeben werden.
 * 
 * @param $time
 **/
function NetLogCheckTime($time)
{
  $MaxDiffTime = 20; // Minuten
  if(empty($time)) 
    trigger_error_text("NetLog: Der Datensatz enh&auml;lt keinen Timestamp.",E_USER_WARNING);
  if(($time > time() + ($MaxDiffTime*60)) or ($time < time() - ($MaxDiffTime*60)))
    trigger_error_text("NetLog: Der Timestamp des Datensatzes weicht um mehr als $MaxDiffTime Min. (n&auml;mlich ".abs(round(($time-time())/60))." Min.) von der Systemzeit des Webservers ab. Vermutlich ist die Systemzeit des netlogd-Hosts nicht richtig gestellt, oder die Queue des netlogds ist extrem stark gef&uuml;llt.");
}

function NetLogIsAuthorizedClient()
{
  list($host) = explode(":",ConfigGet("netlog_server"));
  $ip = nslookupbyname($host);
  if(!$ip) $ip = $host;
  return ($_SERVER["REMOTE_ADDR"] == $ip) ? true : false;
}


?>