<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.imageedit.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.IMAGEEDIT.PHP")) return 0;
define("MOD.IMAGEEDIT.PHP",1);

require_once("mod/mod.image.php");

function ArrayToColor($ArrayCol)
{
  return ($ArrayCol["red"] * 0x10000) + ($ArrayCol["green"] * 0x100) + $ArrayCol["blue"];
}

function _SaveImageToDB($Ident,$Img,$Thumbnail = NULL,$Param = array(),$CheckRights = true)
{
  global $User;
  $info = $Img->getInfo();
  $r = $Param;
  $r["width"]     = $info["width"];
  $r["height"]    = $info["height"];
  $r["mime"]      = $info["mime"];
  $r["data"]      = $Img->getData();
  if(is_a($Thumbnail,"_AbstractImage"))
  {
    $tn = $Thumbnail->getInfo();
    $r["tn_width"]  = $tn["width"];
    $r["tn_height"] = $tn["height"];
    $r["tn_data"]   = $Thumbnail->getData("png");
  }
  $r["edit_time"] = time();
  
  if(!is_posDigit($Ident) and !empty($Ident)) 
  {    
    $id = MysqlReadField("SELECT `id` FROM `".TblPrefix()."flip_content_image` WHERE (`name` = '".addslashes($Ident)."');",0,true);
    if(!$id)
    {
      $r["name"] = $Ident;
      $Ident = 0;
      $r["group_id"] = 1;
      $r["edit_right"] = GetRightID("edit_public_text");
      $CheckRights = false;
    }
    else $Ident = $id;
  }    
  if(!$CheckRights) return MysqlWriteByID(TblPrefix()."flip_content_image",$r,$Ident);
  else return MysqlWriteByRight(TblPrefix()."flip_content_image",$r,array("view_right","edit_right"),$Ident);
}

class ResImage extends _AbstractImage
{
  var $Res = NULL;
  var $Format = "png"; // png, jpeg
  var $Flag = 95;
  
	//php 7 public function __construct()
	//php 5 original function ResImage()
  function __construct($Img)
  {  
	//php 5:
	//parent :: _AbstractImage();
	//php7 neu:		
    parent::__construct($Img);
    $this->Res = $this->_Loader->getRes();
    if(!is_resource($this->Res)) trigger_error_text("Eine Bild-Resource ist ung&uuml;ltig.|img:".var_export($Img,true),E_USER_WARNING);
  }
  
  function setFormat($aFormat,$aFlag)
  {
    $this->Format = $aFormat;
    $this->Flag   = $aFlag;
  }
  
  function getInfo($RaiseErrors = true)
  {
    $r = parent::getInfo($RaiseErrors);
    $r["width"] = imagesx($this->Res);
    $r["height"] = imagesy($this->Res);
    switch($this->Format)
    {
      case("jpeg"): $r["mime"] = "image/jpeg"; break;
      default:      $r["mime"] = "image/png"; break;
    }
    return $r;
  }
  
  function saveToDB($aIdent,$Param = array(),$CheckRights = true, $UpdateThumbnail = true)
  {
    $tn = ($UpdateThumbnail) ? $this->createThumbnail(100,100) : NULL;
    return _SaveImageToDB($aIdent,$this,$tn,$Param,$CheckRights);
  }
  
  function saveToFile($aFileName)
  {
    switch($this->Format)
    {
      case("jpeg"): 
        imagejpeg($this->Res,$aFileName,$this->Flag);
        break;
      default:
        if(empty($aFileName)) imagepng($this->Res);
        else imagepng($this->Res,$aFileName);
        break;
    }
  }
  
  function printData()
  {
    $i = $this->getInfo();
    header("Content-Type: $i[mime]");
    $this->saveToFile(NULL);
  }
  
  function getData($Format = "png",$Flag = 95)
  {
    ob_start();
    $this->saveToFile(NULL);
    $r = ob_get_contents();
    ob_end_clean();
    return $r;
  }
  
  function createThumbnail($MaxWidth,$MaxHeight)
  {  
    $info = $this->getInfo();    
    $h = $info["height"] * (($w = $MaxWidth) / $info["width"]);
    if($h > $MaxHeight) $w = $info["width"] * (($h = $MaxHeight) / $info["height"]);
    
    $truecolor = IsGD2();
    $img = new ResImage(new ImageLoaderNew($w,$h,$truecolor));
	// Transparenz f&uuml;r Thumbnail
	imagealphablending($img->Res, false);
	imagesavealpha($img->Res, true);
    if($truecolor) imagecopyresampled($img->Res,$this->Res,0,0,0,0,$w,$h,$info["width"],$info["height"]);
    else imagecopyresized($img->Res,$this->Res,0,0,0,0,$w,$h,$info["width"],$info["height"]);
    return $img;    
  }
  
  function drawRotatedNN($Img,$CenterX,$CenterY,$Angle)
  {
    $w = imagesx($Img->Res);
    $h = imagesy($Img->Res);
    //$col = imagecolorallocate($this->Res,255,0,0);
    switch(round($Angle))
    {
      case(0):
        $l = $CenterX - round($w/2);
        $t = $CenterY - round($h/2);
        imagecopy($this->Res,$Img->Res,$l,$t,0,0,$w,$h);
        break;
      case(90):
        $l = $CenterX - round($h/2);
        $t = $CenterY - round($w/2);
        for($x = 0; $x < $h; $x++)
          for($y = 0; $y < $w; $y++)
            imagecopy($this->Res,$Img->Res,$x+$l,$y+$t,$w-($y+1),$x,1,1);
        break;
      case(180):
		$l = $CenterX - round($w/2);
        $t = $CenterY - round($h/2);
        for($x = 0; $x < $w; $x++)
          for($y = 0; $y < $h; $y++)
            imagecopy($this->Res,$Img->Res,$x+$l,$y+$t,$w-($x+1),$h-($y+1),1,1);
        break;
      case(270):
        $l = $CenterX - round($h/2);
        $t = $CenterY - round($w/2);
        for($x = 0; $x < $h; $x++)
          for($y = 0; $y < $w; $y++)
            imagecopy($this->Res,$Img->Res,$x+$l,$y+$t,$y,$h-($x+1),1,1);
        break;
      default:
        $dx1 = $dx2 = (float)$CenterX;
        $dy1 = $dy2 = (float)$CenterY;
        $a = ($Angle/180) * pi();
        $dx = cos($a);
        $dy = sin($a);
        $sx1 = $sx2 = $w / 2;
        $sy1 = $sy2 = $h / 2;
        $rad = sqrt($w*$w + $h*$h) / 2;
        for($j = -1; abs($j) == 1; $j += 2)
        {
          while((sqrt(($sx1-$sx2)*($sx1-$sx2) + ($sy1-$sy2)*($sy1-$sy2)) < $rad))
          {
            if(($sy2 >= 0) and ($sy2 < $h) and ($sx2 >= 0) and ($sx2 < $w))
              imagecopy($this->Res,$Img->Res,$dx2,$dy2,$sx2,$sy2,1,1); 
           // else imagesetpixel($this->Res,$dx2,$dy2,$col);
            for($i = -1; abs($i) == 1; $i += 2)
            {
              $sx3 = $sx2;
              $sy3 = $sy2;
              $dy3 = $dy2;
              $dx2q = $dx2*$dx2;
              while(sqrt(($sx1-$sx3)*($sx1-$sx3) + ($sy1-$sy3)*($sy1-$sy3)) < $rad)
              {
                $sy3 += ($dx * $i);
                $sx3 -= ($dy * $i);
                $dy3 += $i;        
                if(($sy3 >= 0) and ($sy3 < $h) and ($sx3 >= 0) and ($sx3 < $w))
                  imagecopy($this->Res,$Img->Res,$dx2,$dy3,$sx3,$sy3,1,1); 
             //   else imagesetpixel($this->Res,$dx2,$dy3,$col);
              }              
            }
            $sx2 += ($dx * $j);
            $sy2 += ($dy * $j);
            $dx2 += $j;
          }
          $dx2 = $dx1 + 1;
          $dy2 = $dy1;
          $sx2 = $sx1 + $dx;
          $sy2 = $sy1 + $dy;
        }
    }
  }
  
  function calcRotated($Img,$CenterX,$CenterY,$Angle)
  {
    $w = imagesx($Img->Res);
    $h = imagesy($Img->Res);
    $wh = $w / 2;
    $hh = $h / 2;
    switch($Angle) 
    {
      case(0):
      case(180):
        return array(
          "shape" => "rectangle",
          "left" =>   round($CenterX - $wh),
          "top" =>    round($CenterY - $hh),
          "right" =>  round($CenterX + $wh),
          "bottom" => round($CenterY + $hh)
        );
      case(90):
      case(270):
        return array(
          "shape" => "rectangle",
          "left" =>   round($CenterX - $hh),
          "top" =>    round($CenterY - $wh),
          "right" =>  round($CenterX + $hh),
          "bottom" => round($CenterY + $wh)        
        );
      default:
        $a = (2*pi()) - ($Angle/180) * pi();
        $ca = cos($a);
        $sa = sin($a);
        return array(
          "shape" => "polygon",
          "x1" => array("x" => $CenterX - round($ca*$wh - $sa*$hh),  "y" => $CenterY - round($sa*$wh + $ca*$hh)), 
          "x2" => array("x" => $CenterX - round($ca*$wh + $sa*$hh),  "y" => $CenterY - round($sa*$wh - $ca*$hh)),
          "x3" => array("x" => $CenterX - round(-$ca*$wh + $sa*$hh), "y" => $CenterY - round(-$sa*$wh - $ca*$hh)),
          "x4" => array("x" => $CenterX - round(-$ca*$wh - $sa*$hh), "y" => $CenterY - round(-$sa*$wh + $ca*$hh)),
        );
    }

     /*
     $r["left"]   = min($r["x1"]["x"],$r["x2"]["x"],$r["x3"]["x"],$r["x4"]["x"]);
     $r["top"]    = min($r["x1"]["y"],$r["x2"]["y"],$r["x3"]["y"],$r["x4"]["y"]);
     $r["width"]  = max($r["x1"]["x"],$r["x2"]["x"],$r["x3"]["x"],$r["x4"]["x"]) - $r["left"];
     $r["height"] = max($r["x1"]["y"],$r["x2"]["y"],$r["x3"]["y"],$r["x4"]["y"]) - $r["top"];
     */
/*     $col = imagecolorallocate($this->Res,255,0,0);
     imagesetpixel($this->Res,$r["x1"]["x"],$r["x1"]["y"],$col);
     imagesetpixel($this->Res,$r["x2"]["x"],$r["x2"]["y"],$col);
     imagesetpixel($this->Res,$r["x3"]["x"],$r["x3"]["y"],$col);
     imagesetpixel($this->Res,$r["x4"]["x"],$r["x4"]["y"],$col);
     imagesetpixel($this->Res,$CenterX,$CenterY,$col);
     return $r;     */
  }
  
  function drawRotated($Img,$CenterX,$CenterY,$Angle,$CalcCorners = false)
  {
  	 if(!is_resource($Img->Res) or !is_resource($this->Res)) trigger_error_text("Ein Bild ist keine g&uuml;ltige Resource.",E_USER_ERROR);
     $Angle = round($Angle % 360);
     $seats_new_template_global = ConfigGet("seats_new_template");
		 if ($seats_new_template_global != 'Y'){ 
     $this->drawRotatedNN($Img,$CenterX,$CenterY,$Angle);
     }
     //imagerotate($Img,$Angle);
     // imagerotate() schein kein alphablending zu unterst&uuml;tzen... 
     // solange ist es leider unbrauchbar.
     // sollte php bzw. gd das eines tages k&ouml;nnen, dann ist hier der richtige ort es einzusetzen.
     if($CalcCorners) return $this->calcRotated($Img,$CenterX,$CenterY,$Angle);
  }
  
  function drawImage($Img,$DstX,$DstY,$SrcX = 0,$SrcY = 0,$SrcW = NULL,$SrcH = NULL)
  {
    if(is_null($SrcW)) $SrcW = $Img->getWidth();
    if(is_null($SrcH)) $SrcH = $Img->getHeight();
    return imagecopy($this->Res,$Img->Res,$DstX,$DstY,$SrcX,$SrcY,$SrcW,$SrcH);
  }
}

?>
