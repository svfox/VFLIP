<?php 

/**
 * Provides functions and algorithms for managing the catering module.
 * 
 * @author Matthias Gro&szlig;
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 */

class CateringManager {
	
	
	function setOrderState($orderIDs = array(), $stateName) {
		
	}
	
	function setItemsWaiting($itemIDs) {
		
	}
	
	function setItemsOrdered($itemIDs) {
		
	}
	
	function setItemsPaid($itemIDs) {
		
	}
	
	function setItemsDelivered($itemIDs) {
		
	}
	
}
?>