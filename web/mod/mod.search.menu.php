<?php
/**
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.SEARCH.MENU.PHP")) return 0;
define("MOD.SEARCH.MENU.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

class Search_menu extends Search //part after Search_ should be the same as filenamepart after mod.search. (case!)
{
  function Search($searchtexts, $seperator="AND")
  {
    global $User;
    $r = $this->SearchTitle($searchtexts, $seperator);
    return array_merge($r, $this->_searchMenu("description", $searchtexts, $seperator));
  }
  
  function SearchTitle($searchtexts, $seperator="AND")
  {
    return $this->_searchMenu("caption", $searchtexts, $seperator);
  }
  
  function _searchMenu($col, $searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    
    $result = MysqlReadArea("SELECT l.`caption`, l.`link`, l.`frameurl`, l.`description`, l.`use_new_wnd`, l.`block_id`, b.`parent_item_id`
                             FROM ".TblPrefix()."flip_menu_links l INNER JOIN ".TblPrefix()."flip_menu_blocks b ON l.block_id=b.id
                             WHERE (l.`$col` LIKE ".implode_sql(" $seperator l.`$col` LIKE ", $searchtexts).")
                               AND (".$User->sqlHasRight("l.view_right").")
                               AND l.enabled='1'"
                           );
    $result = array_merge($result, MysqlReadArea("SELECT `caption`, `parent_item_id`, `description`
                                                  FROM ".TblPrefix()."flip_menu_blocks
                                                  WHERE (`$col` LIKE ".implode_sql(" $seperator `$col` LIKE ", $searchtexts).")
                                                    AND (".$User->sqlHasRight("view_right").")
                                                    AND enabled='1'"
                                                 )
                         );
    foreach($result AS $row)
    {
      if(!$this->_checkparents($row)) continue;
      $link = (isset($row["link"])) ? (empty($row["frameurl"])) ? $row["link"]
                                                                : $row["frameurl"]
                                    : MysqlReadFieldByID(TblPrefix()."flip_menu_links", "link", $row["parent_item_id"], true);
      if($row["parent_item_id"]>0 && !empty($link))
        $link .= ((strpos($link, "?")) ? "&amp;" : "?")."menudir=".$row["parent_item_id"];
      $text = (isset($row["block_id"])) ? $this->_format($row["description"], $searchtexts)
                                        : "<i>Block</i> ".$this->_format($row["description"], $searchtexts);
      $r[] = array("title"  => $row["caption"],
                   "link"   => $link,
                   "link_new" => $row["use_new_wnd"],
                   "text"   => $text
                  );
    }
    return $r;
  }
  
  function _checkparents($parent)
  {
      global $User;
      static $sqlhasright;
      static $parents = array();
      if(!isset($sqlhasright))
        $sqlhasright = $User->sqlHasRight("l.view_right");
      
      $continue = false;
      while($parent["parent_item_id"] != 0)
      {
        $currentparent = $parent;
        if(isset($parents[$parent["parent_item_id"]]))
          $parent = $parents[$parent["parent_item_id"]];
        else
          if(false ==
              ($parent = MysqlReadRow("SELECT b.`parent_item_id`
                                        FROM ".TblPrefix()."flip_menu_links l INNER JOIN ".TblPrefix()."flip_menu_blocks b ON l.block_id=b.id
                                        WHERE l.`id`='".$parent["parent_item_id"]."'
                                          AND $sqlhasright", true
                                      )
              )
            )
            return false;
          else
            $parents[$currentparent["parent_item_id"]] = $parent;
      }
      return true;
  }
}

?>