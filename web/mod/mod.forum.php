<?php
/**
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

function ForumUserPostCount($aUser) {
	$uid = GetSubjectID($aUser);
	return MysqlReadField("SELECT COUNT(*) FROM ".TblPrefix()."flip_forum_posts WHERE poster_id = '$uid';", "COUNT(*)");
}

/**
 * Behebt ein Problem, bei dem die Eigenschaft sort_index
 * der Foren leer war und sie sich somit nicht Sortieren lie&szlig;en.
 */

function ForumFixSortIndexes() {
	// Alle Forengruppen auslesen
	$groups = MysqlReadCol('SELECT `id` FROM `'.TblPrefix().'flip_forum_groups` ORDER BY `sort_index`;','id');
	if(!empty($groups)) {
		// Alle Foren neu indizieren, aber die Reihenfolge (m&ouml;glichst) erhalten
		$sort_index_id = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_user_column` WHERE `type` = \'forum\' AND `name` = \'sort_index\';');
		$forums = GetSubjects('forum', array() , '', 'sort_index');
		$new_index = 0;
		foreach($groups as $id) {
			foreach($forums as $f) {
				if($f['group_id'] == $id) {
					$new_index++;
					$f_s = CreateSubjectInstance($f['id'],'forum');
					$f_s->setProperty('sort_index',$new_index);
				}
			}
		}
	}
}
?>
