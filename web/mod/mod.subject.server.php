<?php
/**
 * @author Daniel Raap
 * @version $Id: mod.subject.server.php 1417 2007-06-23 10:40:02Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

class Server extends ChildSubject {
	function GetIP() {
		return ServerGetIP($this);
	}

	function _notifyUser() {
		require_once ("mod/mod.sendmessage.php");

		$message = new ExecMessage();
		$message->messageFromDB("server_add");
		$message->Params = $this->getProperties();
		return $message->sendMessage(CreateSubjectInstance($message->Params["owner_id"], "user"));
	}

	function setProperties($Prop) {
		$colsToWatch = array ("ip_fix", "server_gateway", "server_subnetmask", "server_dns", "server_dns_ip", "server_wins_ip");

		$before = $this->getProperties($colsToWatch);
		$r = parent :: setProperties($Prop);

		$changed = false;
		foreach ($colsToWatch as $col)
			if (($before[$col] != $Prop[$col]) and isset ($Prop[$col]))
				$changed = true;

		if ($changed)
			$this->_notifyUser();

		return $r;
	}

	function requireAllowCreate() {
		global $User;
		$User->requireRight("server_edit_own");
	}

	function afterCreation() {
		global $User;
		$this->addParent("servers");
		$User->addRight("server_edit_own", $this);
		$owner = $this->getProperty("owner_id");
		if (empty ($owner))
			$this->setProperty("owner_id", $User->id);
	}
}

function ServerGetIP($aServer) {
	switch (ConfigGet("server_iptype")) {
		case "seat" :
			include_once ("mod/mod.seats.php");
			return GetUserIPBySeat($aServer);
		case "user" :
			include_once ("mod/mod.user.php");
			return GetIPByID($aServer);
			
		case "id" :
			//IP-Bereich auslesen
			$ipsPerBlock = 250;
			foreach(array('start', 'end') AS $type) {
				${$type.'IP'} = ConfigGet('server_ip_'.$type);
				$$type = explode('.', ${$type.'IP'});
				if(count($$type) != 4) {
					trigger_error_text('Es wurde keine IP in der Config "server_ip_'.$type.'" angegeben!', E_USER_ERROR, __FILE__,__LINE__);
					return false;
				}
			}
			$ipCount = $end[3] - $start[3];
			for($i=0; $i<3; $i++) {
				$range[$i] = $end[$i] - $start[$i];
				$ipCount += $range[$i] * pow($ipsPerBlock, 3-$i); 
			}
			//Server auslesen
			static $serversAtParty;
			if(!isset($serversAtParty)) {
				$serversAtParty = MysqlReadCol('SELECT s.id FROM `'.TblPrefix().'flip_user_data` d' .
						' LEFT JOIN `'.TblPrefix().'flip_user_column` c ON d.column_id=c.id' .
						' LEFT JOIN `'.TblPrefix().'flip_user_subject` s ON d.subject_id=s.id' .
						" WHERE s.type='server' AND c.name='atparty' AND d.val='Y'" .
						' ORDER BY s.id','id');
				if(count($serversAtParty) > $ipCount) {
					trigger_error_text('Es gibt mehr Server als verf&uuml;gbare IPs! ('.$ipCount.' IPs f&uuml;r '.count($serversAtParty).' Server)', E_USER_WARNING);
				}
			}
			if (is_a($aServer, "Server"))
				$s = $aServer;
			else
				$s = CreateSubjectInstance($aServer);
			if(!in_array($s->id, $serversAtParty)) {
				return '';
			}
			//IP festlegen
			$num = array_search($s->id, $serversAtParty) % $ipCount;
			$num += $start[3] - 1;
			$ip = array();
			for ($i = 0; $i < 3; $i++) {
				$base = pow($ipsPerBlock, 3-$i);
				$add = floor($num / $base);
				$ip[$i] = $start[$i] + $add;
				$num = $num % $base;
			}
			$ip[3] = $num + 1;
			return implode('.', $ip);
		case "fix" :
		default :
			if (is_a($aServer, "Server"))
				$s = $aServer;
			else
				$s = CreateSubjectInstance($aServer);
			return $s->GetProperty("ip_fix");
	}

}
?>