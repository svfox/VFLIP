<?php 
/**
 * This is the backend of the shop module.
 * It contains the business logic of the shopping system.
 * 
 * @author Matthias Gro&szlig;
 * @version $Id$ 1702 2019-01-09 09:01:12Z Matthias $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 */
include_once 'core/core.php';

define('FLIP_SHOP_ITEMS_ALL',		   -1);

define('FLIP_SHOP_GROUP_ALL',			0);
define('FLIP_SHOP_GROUP_ENABLED_ONLY',	1);

define('FLIP_SHOP_CHECKOUT_BALANCE', 	2);
define('FLIP_SHOP_CHECKOUT_PLACEORDER',	4);

define('FLIP_SHOP_TRANSACTION_STATE_ALL', 'shoppingcart,cashdesk,ordered,waiting,readyforpickup,completed');

define('FLIP_SHOP_USERS_ALL', 0);

class ShopBackend {
	
	/** Rights needed to access the system */
	var $rightUseShop 	= 'shop_use';
	var $rightAdminShop = 'shop_admin';
	
	var $currentUser = null;
	
	/** The name of the table containing item groups */
	var $tableShopGroups 	 = 'flip_shop_groups';
	var $tableShopItems  	 = 'flip_shop_items';
	var $tableShopOrders 	 = 'flip_shop_orders';
	var $tableShopOrderItems = 'flip_shop_order_items';
	var $tableShopUserBalance = 'flip_shop_user_balance';
	
	var $validTransactionStates = array('shoppingcart','cashdesk','ordered','waiting','readyforpickup','completed');
	
	/**
	 * This constructor checks if the user given is valid
	 * and has the right to use the shop system.
	 * @param $currentUser The User instance for which actions will be taken
	 */
	//php 7 public function __construct()
	//php 5 original function ShopBackend($currentUser)
	function __construct($currentUser) {
		if(!is_object($currentUser)) {
			trigger_error_text(
				'Das Shop System ben&ouml;tigt einen g&uuml;ltigen User!|currentUser != Object', 
				E_USER_ERROR
			);
			
			return;
		}
		
		if(!is_a($currentUser, 'User')) {
			trigger_error_text(
				'Das Shop System kann nur mit normalen Usern zusammen arbeiten.|currentUser ist kein User', 
				E_USER_ERROR
			);
		}
		
		// Require using the shop module
		$currentUser->requireRight($this->rightUseShop);
		
		// Set up the objects internals if the user is valid and
		// has the correct rights
		$this->currentUser = $currentUser;
		
		// Preprend table prefixes
		$this->tableShopGroups      = TblPrefix() . $this->tableShopGroups;
		$this->tableShopItems       = TblPrefix() . $this->tableShopItems;
		$this->tableShopOrders	    = TblPrefix() . $this->tableShopOrders;
		$this->tableShopOrderItems  = TblPrefix() . $this->tableShopOrderItems;
		$this->tableShopUserBalance = TblPrefix() . $this->tableShopUserBalance;
	}
	
	/**
	 * Retrieves the groups which contain the offered items.
	 * @param $groupSelector 
	 *		FLIP_SHOP_GROUP_ALL to get all groups, FLIP_SHOP_GROUP_ENABLED_ONLY (default) to get only enabled groups.
	 * @return mixed An array containing the groups or false on error.
	 */
	function getItemGroups($groupSelector = FLIP_SHOP_GROUP_ENABLED_ONLY) {
		$query = '';
		
		if($groupSelector == FLIP_SHOP_GROUP_ALL) {
			$query = 'SELECT * FROM `' . $this->tableShopGroups . '`';
		} elseif($groupSelector == FLIP_SHOP_GROUP_ENABLED_ONLY) {
			$query = 'SELECT * FROM `' . $this->tableShopGroups . '` WHERE `enabled` = 1';
		} else {
			trigger_error_text(
				'Die Gruppen konnten nicht aus dem System gelesen werden!|getItemGroups(): Falscher Parameter!',
				E_USER_ERROR
			);
			
			return false;
		}
		
		$groups = MysqlReadArea($query . ' ORDER BY `name`', 'id');
		
		if($groups === false) {
			trigger_error_text(
				'Die Gruppen konnten nicht aus dem System gelesen werden!|getItemGroups(): MySQL Fehler!',
				E_USER_ERROR
			);
		}
		
		return $groups;
	}
	
	/**
	 * Reads and returns an item group from the database
	 * 
	 * @param $itemID The id of the item to read or false on failure
	 */
	function getGroup($groupID = 0) {
		if(empty($groupID) || ($groupID < 0)) {
			trigger_error_text('Es wurde keine g&uuml;ltige GruppenID angegeben!', E_USER_ERROR);
			return -1;
		}
		
		return MysqlReadRowByID($this->tableShopGroups, $groupID, true);
	}
	
	/**
	 * Returns the name of a group for a given group id.
	 * If the group does not exist, an empty string is returned.
	 * 
	 * @param mixed $groupID The id of the group to read from.
	 */
	function getGroupName($groupID) {
		$sqlGroupID = escape_sqlData_without_quotes($groupID);
		$groupName = MysqlReadFieldByID($this->tableShopGroups, 'name', $sqlGroupID);
		
		if($groupName === false) {
			trigger_error_text(
				'Gruppe #' . $groupID .' existiert nicht!|getGroupName(): MySQL Fehler!',
				E_USER_ERROR
			);
			
			return '';
		}
		
		return $groupName;
	}
	
	/**
	 * Saves the data of an item group into the database or creates
	 * a new group if the id == -1.
	 *
	 * @param $data The data of the item group
	 */
	function writeGroup($data) {
		if(!isset($data['id']) 
			|| !isset($data['name']) 
			|| !isset($data['description']) 
			|| !isset($data['enabled']) 
			|| !isset($data['imageid'])
		) {
			trigger_error_text(
				'Die Daten f&uuml;r eine Artikelgruppe sind unvollst&auml;ndig!|' 
					. 'Daten: ' . print_r($data)
			);
			
			return false;
		}
		
		$id = ($data['id'] == -1) ? null : $data['id'];
		
		return MysqlWriteByID($this->tableShopGroups, $data, $id);
	}
	
	/**
	 * Deletes a group from the database
	 * 
	 * @param mixed $groupID The id of the group to delete
	 */
	function deleteGroup($groupID) {
		return $this->deleteGroups(array($groupID));
	}
	
	/**
	 * Deletes multiple groups from the database
	 * 
	 * @param mixed $groupIDs The ids of the group to delete in an array
	 */
	function deleteGroups($groupIDs) {
		if(empty($groupIDs) || (!is_array($groupIDs))) {
			trigger_error_text('Umg&uuml;ltige Artikelgruppe!|deleteGroup(' . print_r($groupIDs) . ')');
			return false;
		}
		
		$items = $this->getItems();
		
		foreach($items as $item) {
			if(!empty($item) && in_array($item['groupid'], $groupIDs)) {
				trigger_error_text(
					'Die Gruppe #"' . $item['groupid'] . '" ist nicht leer - L&ouml;schen abgebrochen!', 
					E_USER_ERROR
				);
			}
		}
		
		$sqlIDs = implode_sqlIn($groupIDs);
		$deleteOK = MysqlWrite('DELETE FROM `' . $this->tableShopGroups .'` WHERE `id` IN(' . $sqlIDs . ')');
		
		if($deleteOK === false) {
			trigger_error_text('Die Gruppen ' . implode(', ', $groupIDs) . ' konnten nicht gel&ouml;scht werden!');
		}
		
		return $deleteOK;
	}
	
	/**
	 * Reads and returns an item from the item table.
	 * 
	 * @param $itemID The ID of the item or FALSE on failure
	 */
	function getItem($itemID) {
		if(empty($itemID) || ($itemID < 1)) {
			trigger_error_text('Die ID eines Artikels muss bekannt sein um ihn auszulesen!', E_USER_ERROR);
			return false;
		}
		
		$item = MysqlReadRowByID($this->tableShopItems, $itemID, true);
		
		return $item;
	}
	
	/**
	 * Returns the items that belong to a specific group.
	 * 
	 * @param array $groupID The record ID of the group to read.
	 * @return An array of item records or FALSE on error.
	 */
	function getItems($groupID = FLIP_SHOP_ITEMS_ALL) {
		$msgGroup = '';
		$sqlWhere = '';
		
		if($groupID != FLIP_SHOP_ITEMS_ALL){
			$sqlGroupID = escape_sqlData_without_quotes($groupID);
			$group = MysqlReadRow('SELECT * FROM `' . $this->tableShopGroups . '` WHERE `id` = ' .$sqlGroupID, true);
			
			if($group === false) {
				trigger_error_text('Gruppe #' . $group['id'] . ' wurde nicht gefunden!|getItems(): ID nicht gefunden!');
				return false;
			}
			
			$msgGroup = ' aus Gruppe #' . $group['id'];
			$sqlWhere = 'WHERE `groupid` = ' . $sqlGroupID;
		}
		
		$items = MysqlReadArea('SELECT * FROM `' . $this->tableShopItems . '` i ' . $sqlWhere, 'id');
		
		if($items === false) {
			trigger_error_text(
				'Die Artikel' . $msgGroup . ' konnten nicht gelesen werden!|getItems(): MySQL Fehler!'
			);
		}
		
		return $items;
	}
	
	/**
	 * Reads an retrieves the total amount of items available to buy.
	 * If an item has its stock disabled, this method will always return 1. 
	 * 
	 * @return A key-value array having the key as the item id and the remaining items amount as the value
	 */
	function getItemStockAmounts() {
		$stock_amounts = MysqlReadArea('SELECT * FROM `' . $this->tableShopItems . '`', 'id');
		$order_amounts = MysqlReadCol(
			'SELECT `itemid`, SUM(`amount`) AS sum ' 
				. 'FROM `' . $this->tableShopOrderItems . '` ' 
				. 'GROUP BY `itemid`',
			'sum',
			'itemid'
		);
		
		$items_amount = array();
		
		foreach ($stock_amounts as $item) {
			if($item['stockenabled'] == '0') {
				$items_amount[$item['id']] = 1;
			} else {
				$current_amount = isset($order_amounts[$item['id']]) ? $order_amounts[$item['id']] : 0;
				$items_amount[$item['id']] = $item['stockamount'] - $current_amount;
			}
		}
		
		return $items_amount;
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param $itemData
	 */
	function writeItem($itemData) {
		if(!isset($itemData['id']) 
			|| !isset($itemData['name']) 
			|| !isset($itemData['price']) 
			|| !isset($itemData['description'])
			|| !isset($itemData['enabled'])
			|| !isset($itemData['stockenabled'])
			|| !isset($itemData['stockamount'])
			|| !isset($itemData['queued'])
			|| !isset($itemData['imageid'])
		) {
			trigger_error_text(
				'Die Daten des Artikels sind unvollst&auml;ndig und k&ouml;nnen nicht gespeichert werden!|' 
					. 'writeItem(' . print_r($itemData, true) .  ')',
				E_USER_ERROR
			);
			
			return false;
		}
		
		$itemData['price'] = formatMySQLDecimal($itemData['price']);
		
		$itemID = ($itemData['id'] == -1) ? null : $itemData['id'];
		$writeOk = MysqlWriteByID($this->tableShopItems, $itemData, $itemID);
		
		if($writeOk === false) {
			trigger_error_text(
				'Der Artikel konnte nicht gespeichert werden!|' . 'writeItem(' . print_r($itemData, true) .  ')'
			);	
		}
		
		return $writeOk;
	}
	
	function deleteItems($itemIds) {
		if(!is_array($itemIds)) {
			trigger_error_text('Konnte die Artikel nicht l&ouml;schen - die IDs sind ung&uuml;ltig!|', E_USER_ERROR);
			return false;
		}
		
		$sqlIDs = implode_sqlIn($itemIds);
		$deleteOK = MysqlWrite('DELETE FROM `' . $this->tableShopItems . '` WHERE `id` IN(' . $sqlIDs . ')');
		
		if($deleteOK === false) {
			trigger_error_text('Die Artikel konnten nicht gel&ouml;scht werden!', E_USER_ERROR);
		}
		
		return $deleteOK;
	}
	
	/**
	 * Reads and returns the current shopping cart contents.
	 * 
	 * @return An array that contains the shopping cart contents
	 */
	function getShoppingCartContents() {
		$shoppingCart = MysqlReadArea(
			'SELECT * FROM `' . $this->tableShopOrders . '` ' 
				. 'LEFT JOIN `' . $this->tableShopOrderItems . '` ' 
				. 'ON `' . $this->tableShopOrders . '`.id = `' . $this->tableShopOrderItems . '`.orderid ' 
				. 'LEFT JOIN `' . $this->tableShopItems . '` ' 
				. 'ON `' . $this->tableShopOrderItems . '`.itemid = `' . $this->tableShopItems . '`.id ' 
				. 'HAVING `status` = \'shoppingcart\' '
				. 'AND `userid` = ' . $this->currentUser->id . ' ' 
				. 'AND `itemid` IS NOT NULL', 
			'id'
		);
		
		return $shoppingCart;
	}
	
	/**
	 * Retrieves the id of the current shoppping cart.
	 * If the cart does not exist, it is created first.
	 * 
	 * @return The current cart id or FALSE on failure
	 */
	function getCurrentCartID() {
		// Get the id of the current cart, if === false create one
		$currentCart = MysqlReadField(
			'SELECT * ' 
				. 'FROM `' . $this->tableShopOrders . '` ' 
				. 'WHERE `userid` = ' . $this->currentUser->id . ' ' 
				. 'AND `status` = \'shoppingcart\'',
			'id',
			true
		);
		
		if($currentCart === false) {
			$createOK = MysqlWrite(
				'INSERT INTO `' . $this->tableShopOrders . '` (ordertime, userid, status) ' 
					. 'VALUES (UNIX_TIMESTAMP(), ' 
					. escape_sqlData_without_quotes($this->currentUser->id) 
					. ', \'shoppingcart\')'
			);
			
			if($createOK === false) {
				trigger_error_text('Konnte den Warenkorb nicht initialisieren!', E_USER_ERROR);
				return;		
			}
		}
		
		// Get the id of the current cart, it should now return a value
		$currentCart = MysqlReadField(
			'SELECT * ' 
				. 'FROM `' . $this->tableShopOrders . '` ' 
				. 'WHERE `userid` = ' . $this->currentUser->id . ' ' 
				. 'AND `status` = \'shoppingcart\'',
			'id',
			true
		);
		
		if($currentCart === false) {
			trigger_error_text('Der Warenkorb hat selbst nach der Initialisierung keine ID!', E_USER_ERROR);				
		}
		
		return $currentCart;
	}
	
	function getTransactionWithStatusAndUser($status = array('completed'), $userID) {
		$valid_states = array('shoppingcart','cashdesk','ordered','waiting','readyforpickup','completed');
		
		if(!is_array($status)) {
			trigger_error_text('Der Transaktionsstatus muss eine Liste sein!', E_USER_ERROR);
			return array();			
		}
		
		$where = '';
		
		if($userID > 0) {
			$where = 'AND `userid` = ' . escape_sqlData_without_quotes($userID);
		}
		
		$sqlStatus = implode_sqlIn($status);
		$transactions = MysqlReadArea(
			'SELECT *, 0 AS `total`, 0 AS `num_items` FROM `' . $this->tableShopOrders . '` ' 
				. ' WHERE `status` IN (' . $sqlStatus . ') ' 
				. $where,
			'id'
		);
		
		if($transactions === false) {
			trigger_error_text('Konnte die Transaktionen nicht lesen!', E_USER_ERROR);
			return array();
		}
		
		if(empty($transactions)) {
			return array();
		}
			
		$transactionIDs = array_keys($transactions);
		$sqlTransactionIDs = implode_sqlIn($transactionIDs);
		
		$orderitems = MysqlReadArea(
			'SELECT *, (`price` * `amount`) AS price_sum, 0 AS `num_items` ' 
				. 'FROM `' . $this->tableShopOrderItems . '` oi ' 
				. 'LEFT JOIN `flip_shop_items` i ' 
				. 'ON oi.itemid = i.id WHERE `orderid` IN (' . $sqlTransactionIDs . ')'
		);
		
		if($orderitems === false) {
			trigger_error_text('Konnte die Artikel der abgeschlossenen Transaktionen nicht lesen!', E_USER_ERROR);
			return array();			
		}
		
		foreach ($orderitems as $item) {
			$transactions[$item['orderid']]['total'] += $item['price_sum'];
			$transactions[$item['orderid']]['num_items'] += $item['amount'];
			$transactions[$item['orderid']]['items'][] = $item;
		}
		
		return $transactions;
	}
	
	function getTransactionWithStatus($status = array('completed')) {
		return $this->getTransactionWithStatusAndUser($status, $this->currentUser->id);
	}
	
	/**
	 * Returns the transaction with the specified id.
	 * 
	 * @param $id The id of the transaction to get
	 */
	function getTransactionWithID($id) {
		if(!is_numeric($id) || empty($id)) {
			trigger_error_text('Die angegebene ID ist ung&uuml;ltig!', E_USER_ERROR);
			return array();
		}
		
		$sqlID = escape_sqlData_without_quotes($id);
		$transactionHead = MysqlReadRow('SELECT * FROM `' . $this->tableShopOrders . '` WHERE `id` = ' . $sqlID);
		
		if($transactionHead == false) {
			trigger_error_text('Es gibt keine Transaktion #' . $id . '!', E_USER_ERROR);
			return array();
		}
		
		$transactionItems = MysqlReadArea(
			'SELECT * FROM `' . $this->tableShopOrderItems . '` o ' 
				. 'LEFT JOIN `' . $this->tableShopItems . '` i ' 
				. 'ON (o.`itemid` = i.`id`) '
				. 'WHERE `orderid` = ' . $sqlID
		);
		
		if($transactionItems == false) {
			trigger_error_text('Konnte die Artikel der Transaktion #' . $id . ' nicht lesen!', E_USER_ERROR);
			return array();			
		}
		
		$transaction = $transactionHead;
		$transaction['items'] = $transactionItems;
		
		return $transaction;
	}
	
	/**
	 * Returns the completed transactions for the current user.
	 */
	function getCompleteOrders() {
		return $this->getTransactionWithStatus(array('completed'));
	}
	
	function getIncompleteOrders() {
		return $this->getTransactionWithStatus(array('ordered', 'waiting', 'readyforpickup'));
	}
	
	/**
	 * Returns the user's balance.
	 * If there is no record for the user, one will be created with no balance (0.0).
	 * 
	 * @return The user's current balance value.
	 */
	function getUserBalance() {
		$sqlUserID = escape_sqlData_without_quotes($this->currentUser->id);
		$balance = MysqlReadField(
			'SELECT `balance` ' 
				. 'FROM `' . $this->tableShopUserBalance . '` ' 
				. 'WHERE `userid` = ' . $sqlUserID,
			'balance',
			true
		);
		
		// Try creating a new record
		if($balance === false) {
			$insertOK = MysqlWrite(
				'INSERT INTO `' . $this->tableShopUserBalance . '` (`userid`, `balance`) ' 
					. 'VALUES(' . $sqlUserID . ', 0.0)'
			);
			
			if($insertOK === false) {
				trigger_error_text(
					'Konnte kein neues Guthaben für den User #' 
						. $sqlUserID 
						. ' schreiben!', 
					E_USER_ERROR
				);
			}
			
			return 0.0;
		}
		
		return $balance;
	}
	
	function getCurrentUserID() {
		return $this->currentUser->id;
	}
	
	function setUserBalance($newBalance = 0.0) {
		if($newBalance < 0.0) {
			trigger_error_text('Negative Guthaben können nicht gesetzt werden!', E_USER_ERROR);
			return false;
		}
		
		if(MysqlTransactionStart() == false) {
			return false;
		}
		
		// HACK: Replace numeric value comma (,) with a dot (.)
		
		
		$sqlBalance = escape_sqlData_without_quotes(formatMySQLDecimal($newBalance));
		$sqlUserID  = escape_sqlData_without_quotes($this->currentUser->id);
		
		// Read the current balance thus creating missing balance records
		$currentBalance = $this->getUserBalance();
		
		// Update the balance record
		$updateOK = MysqlWrite(
			'UPDATE `' . $this->tableShopUserBalance .'` ' 
				. 'SET `balance` = ' . $sqlBalance . ' ' 
				. 'WHERE `userid` = ' . $sqlUserID
		);
		
		if($updateOK === false) {
			MysqlTransactionRollback();
			trigger_error_text(
				'Das neue Guthaben (' . $sqlBalance . ') des Users #'.$sqlUserID.' konnte nicht gesetzt werden!', 
				E_USER_ERROR
			);
			return false;
		}
		
		if(MysqlTransactionCommit() === false) {
			return false;
		}
		
		return true;
	}

	/**
	 * Deletes the user's balance records, thus clearing his current balance.
	 * 
	 * @return TRUE, if the process was successful, else FALSE.
	 */
	function deleteUserBalanceAccount() {
		$sqlUserID = escape_sqlData_without_quotes($this->currentUser->id);
		$deleteOK = MysqlWrite('DELETE FROM `' . $this->tableShopUserBalance . '` WHERE `userid` = ' . $sqlUserID);
		
		return $deleteOK;
	}
	
	/**
	 * Adds an item to the shopping cart.
	 * If it's already in the cart, the existing record amount will be increased.
	 * Furthermore, if there is no shopping cart, a new one will be created first.
	 * 
	 * @param $itemID The id of the new item.
	 * @param $amount The amount of the item to add
	 */
	function addItemToCart($itemID, $itemAmount = 1) {
		// Check if itemID and amount are valid
		if(empty($itemID)) {
			trigger_error_text('Die Artikelnummer muss bekannt sein, um ihn in den Warenkorb zu legen!', E_USER_ERROR);
			return;
		}
		
		if(MysqlTransactionStart() === false) {
			return false;
		}
		
		// Check if the item is available
		$remaining_amount = $this->getItemStockAmounts();
		
		if($remaining_amount[$itemID] < 1) {
			trigger_error_text('Der Artikel ist nicht mehr verf&uuml;gbar, sorry!', E_USER_ERROR);
			return;			
		}
		
		// Get the id of the current cart
		$currentCart = $this->getCurrentCartID();
		
		// Check if the item is aleady in the cart
		$alreadyInCart = MysqlReadRow(
			'SELECT * FROM `' . $this->tableShopOrderItems . '` ' 
				. 'WHERE `orderid` = ' . escape_sqlData_without_quotes($currentCart) . ' '
				. 'AND `itemid` = ' . escape_sqlData_without_quotes($itemID),
				true
		);
		
		$sqlOK = false;
		
		// Add the item to the current shopping cart.
		if($alreadyInCart === false) {
			$sqlOK = MysqlWrite(
				'INSERT INTO `' . $this->tableShopOrderItems . '` (orderid, itemid, amount) ' 
					. 'VALUES (' 
					. escape_sqlData_without_quotes($currentCart) . ', ' 
					. escape_sqlData_without_quotes($itemID) . ', ' 
					. escape_sqlData_without_quotes($itemAmount) 
					. ')'
			);
		} 
		
		// Add the amount to the one in the database and update
		else {
			$sqlOK = MysqlWrite(
				'UPDATE `' . $this->tableShopOrderItems . '` ' 
				. 'SET `amount` = `amount` + ' . escape_sqlData_without_quotes($itemAmount) . ' ' 
				. 'WHERE `id` = ' . escape_sqlData_without_quotes($alreadyInCart['id'])
			);
		}	
		
		if($sqlOK === false) {
			trigger_error_text(
				'Konnte den Artikel in der angegebenen Menge nich in den Warenkorb legen!', 
				E_USER_ERROR
			);
		}
		
		if(MysqlTransactionCommit() === false) {
			return false;
		}
		
		return $sqlOK;
	}
	
	/**
	 * Removes an item from the shopping cart.
	 * 
	 * @param integer $itemID The id of the item to remove
	 * @return TRUE, if the item could be removed, else FALSE
	 */
	function removeItemFromCart($itemID) {
		$sqlItemID = escape_sqlData_without_quotes($itemID);
		
		if(MysqlTransactionStart() === false) {
			return false;
		}
		
		// Check if the item exists
		$itemidDB = MysqlReadField('SELECT * FROM `' . $this->tableShopItems . '` WHERE `id` = ' . $sqlItemID, 'id');
		
		if($itemidDB === false) {
			trigger_error_text('Der Artikel mit der Nummer #' . $itemID . ' existiert nicht!', E_USER_ERROR);
			return false;
		}
		
		// Get the id of the current cart
		$currentCart = $this->getCurrentCartID();
		
		if($currentCart === false) {
			trigger_error_text('Konnte die Anzahl nicht &auml;ndern - keine Warenkorbnummer vorhanden!');
			return false;
		}
		
		$deleteOK = MysqlWrite(
			'DELETE FROM `' . $this->tableShopOrderItems . '` ' 
				. 'WHERE `orderid` = ' . escape_sqlData_without_quotes($currentCart) . ' ' 
				. 'AND `itemid` = ' . $sqlItemID
		);
		
		if($deleteOK === false) {
			trigger_error_text('Der Artikel konnte leider nicht aus dem Warenkorb entfernt werden!', E_USER_ERROR);
		}
		
		if(MysqlTransactionCommit() === false) {
			return false;
		}
		
		return $deleteOK;
	}
	
	function changeItemAmountInCart($itemID, $amount) {
		$sqlItemID = escape_sqlData_without_quotes($itemID);
		
		if(MysqlTransactionStart() === false) {
			return false;
		}
			
		// Check if the item exists
		$itemidDB = MysqlReadField('SELECT * FROM `' . $this->tableShopItems . '` WHERE `id` = ' . $sqlItemID, 'id');
		
		if($itemidDB === false) {
			MysqlTransactionRollback();
			trigger_error_text('Der Artikel mit der Nummer #' . $itemID . ' existiert nicht!', E_USER_ERROR);
			return false;
		}
		
		if($amount < 1) {
			MysqlTransactionRollback();
			trigger_error_text('Die neue Menge muss mindestens 1 betragen!', E_USER_ERROR);
			return false;
		}
		
		// Get the id of the current cart
		$currentCart = $this->getCurrentCartID();
		
		if($currentCart === false) {
			MysqlTransactionRollback();
			trigger_error_text('Konnte die Anzahl nicht &auml;ndern - keine Warenkorbnummer vorhanden!');
			return false;
		}
		
		$sqlCurrentCartID = escape_sqlData_without_quotes($currentCart);
		
		$changeOK = MysqlWrite(
			'UPDATE `' . $this->tableShopOrderItems . '` ' 
				. 'SET `amount` = ' . escape_sqlData_without_quotes($amount) . ' ' 
				. 'WHERE `orderid` = ' . $sqlCurrentCartID . ' ' 
				. 'AND `itemid` = ' . $sqlItemID
		);
		
		if($changeOK === false) {
			trigger_error_text(
				'Konnte die Menge von Artikel #' . $itemID . ' nicht auf ' . $amount . ' setzen!'
				, E_USER_ERROR
			);
		}
		
		if(MysqlTransactionCommit() === false) {
			return false;
		}
		
		return $changeOK;
	}
	
	/**
	 * Checks out the contents of the cart and sets the state
	 * of the order to the given state.
	 * 
	 * If the cart contains only non-waiting items, they will all be
	 * kept in the current order with a new timestamp and processed 
	 * according to $checkoutMethod.
	 * 
	 * If the cart contains only waiting items, they will be processed
	 * like non-waitiung items, except that even if they are paid right away,
	 * they remain in the waiting state.
	 * 
	 * Last, but not least, a cart can contain mixed type items. 
	 * In that case, the items will be grouped by their queue state and each group
	 * will be placed in their own order. The orders will be then processed according
	 * to the upper two methods.
	 * 
	 * @return TRUE on success, FALSE on error
	 */
	function checkoutCurrentCart($checkoutMethod = FLIP_SHOP_CHECKOUT_PLACEORDER) {
		if(($checkoutMethod != FLIP_SHOP_CHECKOUT_BALANCE) && ($checkoutMethod != FLIP_SHOP_CHECKOUT_PLACEORDER)) {
			trigger_error_text('Die angegebene Checkout-Methode ist unbekannt!', E_USER_ERROR);
			return false;
		}
		
		if(MysqlTransactionStart() === false) {
			return false;
		}
		
		$sqlUserID = escape_sqlData_without_quotes($this->currentUser->id);
		
		// Get the contents of the shopping cart
		$shoppingCartContents = $this->getShoppingCartContents();
		
		if(empty($shoppingCartContents)) {
			MysqlTransactionRollback();
			trigger_error_text('Der Warenkorb ist leer!', E_USER_ERROR);
			return false;			
		}
		
		// Read the shopping cart id for later
		$cartID = MysqlReadField(
			'SELECT `id` FROM `' . $this->tableShopOrders . '` ' 
				. 'WHERE `userid` = ' . $sqlUserID . ' ' 
				. 'AND `status` = \'shoppingcart\'',
			'id',
			true
		);
		
		if($cartID === false) {
			MysqlTransactionRollback();
			trigger_error_text('Die ID des aktuellen Warenkorbs kann nicht gelesen werden!', E_USER_ERROR);
			return false;			
		}
		
		$itemGroup = array();
		
		// Sort the items into group and add the groups
		foreach ($shoppingCartContents as $index => $item) {
			if($item['queued'] == '0') {
				$itemGroup['nowait'][] = $item;
			} else {
				$itemGroup['wait'][] = $item;
			}
		}
		
		$newOrderStatus = '';
		
		// Proccess the sorted groups individually
		foreach ($itemGroup as $groupName => $groupItems) {
			$paidForTransaction = false;
			
			if($checkoutMethod == FLIP_SHOP_CHECKOUT_BALANCE) {
				// Get the current balance of the user
				$userBalance = $this->getUserBalance();
				
				// Calculate the total price of the cart contents
				$total_sum = 0.0;
			
				foreach ($groupItems as $key => $value) {
					$total_sum += $value['amount'] * $value['price'];
				}
				
				// Check if the resulting balance is >= 0.00
				$resultingBalance = floatval($userBalance - $total_sum);
				
				if($resultingBalance < 0.0) {
					MysqlTransactionRollback();
					trigger_error_text('Dein Guthaben reicht nicht aus um den Warenkorb auszuchecken!', E_USER_ERROR);
					return false;
				}
				
				// Write the new balance
				$setBalanceOK = $this->setUserBalance($resultingBalance);
				
				if($setBalanceOK === false) {
					MysqlTransactionRollback();
					trigger_error_text('Die Bearbeitung des Warenkorbs wurde abgebrochen!', E_USER_ERROR);
					return false;
				}
				
				$paidForTransaction = true;
			}
			
			$newOrderStatus = '';
			
			if(($groupName == 'nowait') && ($checkoutMethod == FLIP_SHOP_CHECKOUT_BALANCE)) {
				$newOrderStatus = 'readyforpickup';
			} elseif(($groupName == 'nowait') && ($checkoutMethod == FLIP_SHOP_CHECKOUT_PLACEORDER)) {
				$newOrderStatus = 'ordered';
			} else {
				$newOrderStatus = 'waiting';
			}
			
			$newOrderTimestamp = escape_sqlData_without_quotes(time());
				
			// Now that the balance has been set, create a new order and add the processed items
			$createNewOrder = MysqlWrite(
				'INSERT INTO `' . $this->tableShopOrders . '` (ordertime, userid, status, paid) ' 
					. 'VALUES (' . $newOrderTimestamp . ', ' 
								 . $sqlUserID . ', \'' 
								 . $newOrderStatus . '\', ' 
								 . ($paidForTransaction ? 1 : 0) 
							. ')'
			);
			
			if($createNewOrder === false) {
				MysqlTransactionRollback();
				trigger_error_text(
					'Konnte keine Bestellung f&uuml;r die Gruppe "' . $groupName . '" erstellen!', E_USER_ERROR
				);
				return false;
			}
			
			// Read the id of the new order, which is the one with the new timestamp, the highest id and the
			// current user id.
			$newOrderID = MysqlReadField(
				'SELECT `id` FROM `' . $this->tableShopOrders . '` ' 
					. 'WHERE `userid` = ' . $sqlUserID . ' ' 
					. 'AND `ordertime` = ' . $newOrderTimestamp . ' ' 
					. 'AND `status` = \'' . $newOrderStatus . '\' ' 
					. 'ORDER BY `id` DESC',
				'id',
				true
			);
			
			if($newOrderID === false) {
				MysqlTransactionRollback();
				trigger_error_text(
					'Konnte die Bestellungsnummer der Gruppe "' . $groupName . '" nicht lesen!', E_USER_ERROR
				);
				return false;
			}
			
			$sqlNewOrderID = escape_sqlData_without_quotes($newOrderID);
			
			// Insert the items into the orderitems database
			foreach ($groupItems as $index => $item) {
				$sqlItemID = escape_sqlData_without_quotes($item['itemid']);
				$sqlAmount = escape_sqlData_without_quotes($item['amount']);
				
				$insertOK = MysqlExecQuery(
					'INSERT INTO `' . $this->tableShopOrderItems . '` (orderid, itemid, amount) ' 
						. 'VALUES (' . $sqlNewOrderID . ', ' . $sqlItemID . ', ' . $sqlAmount . ')'
				);
				
				if($newOrderID === false) {
					MysqlTransactionRollback();
					trigger_error_text(
						'Konnte den Artikel "' 
							. $item['name'] 
							.'" nicht in die neue Bestellung einf&uuml;gen!', E_USER_ERROR
					);
					return false;
				}
			}
		}
		
		// Now we only need to clean up - e.g. deleting the old cart order
		// Start with the cart items, and then the cart itself
		$deleteItems = MysqlWrite(
			'DELETE FROM `' . $this->tableShopOrderItems . '` ' 
				. 'WHERE `orderid` = ' . escape_sqlData_without_quotes($cartID)
		);
		
		if($deleteItems === false) {
			MysqlTransactionRollback();
			trigger_error_text('Der alte Warenkorb konnte nicht geleert werden!', E_USER_ERROR);
			return false;			
		}
		
		$deleteCart = MysqlWrite(
			'DELETE FROM `' . $this->tableShopOrders . '` ' 
				. 'WHERE `userid` = ' . $sqlUserID . ' ' 
				. 'AND `status` = \'shoppingcart\' '
		);
		
		if($deleteCart === false) {
			MysqlTransactionRollback();
			trigger_error_text('Der alte Warenkorb konnte nicht gel&ouml;scht werden!', E_USER_ERROR);			
			return false;			
		}		
		
		// Finally, commit the changes
		if(MysqlTransactionCommit() === false) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Ensures the callee has admin privileges to manage the shop system
	 * 
	 */
	function requireAdminPrivileges() {
		global $User;
		$User->requireRight($this->rightAdminShop);
	}
	
	/**
	 * Returns an overview of the users which have ordered, their balance and
	 * how many items where ordered.
	 */
	function getTransactionUsers() {
		$users = array();
		
		$users = MysqlReadArea(
			'SELECT *, COUNT(o.`id`) AS `num_orders`, ' 
				. 'o.`userid` AS `userid`, IF(ub.`balance` IS NULL, 0.0, ub.`balance`) AS `balance`' 
				. 'FROM `' . $this->tableShopOrders . '` o ' 
				. 'LEFT JOIN `flip_shop_user_balance` ub ' 
				. 'ON o.userid = ub.userid ' 
				. 'LEFT JOIN `flip_user_subject` us ' 
				. 'ON o.`userid` = us.`id` ' 
				. 'GROUP BY o.`userid`', 
			'id'
		);
		
		return $users;
	}
	
	
	/**
	 * Sets the state of several transactions to a new value.
	 * 
	 * @param $transactionIDs The IDs of the transaction to modify
	 * @param $state The new state.
	 * @return TRUE on success, FALSE on failure
	 */
	function setTransactionStates($transactionIDs, $state) {
		if(!in_array($state, $this->validTransactionStates) || empty($state)) {
			trigger_error_text('Der Status ' . $state . ' ist ung&uuml;ltig!', E_USER_ERROR);
			return false;
		}
		
		if(!is_array($transactionIDs)) {
			trigger_error_text('Die Transaktionsids m&uuml;ssen eine Liste sein!', E_USER_ERROR);
			return false;			
		}
		
		$paidSet = ($state == 'completed') ? ', `paid` = 1' : '';
		
		$writeOk = MysqlWrite(
			'UPDATE `' . $this->tableShopOrders . '` ' 
				. 'SET `status` = \'' . escape_sqlData_without_quotes($state) . '\' ' . $paidSet . ' ' 
				. 'WHERE `id` IN (' . implode_sqlIn($transactionIDs) . ')'
		);
		
		if($writeOk === false) {
			trigger_error_text(
				'Die Transaktion(en) ' 
					. implode(', ', $transactionIDs) 
					. ' konnte(n) nicht auf ' 
					. $state .' gesetzt werden!', 
				E_USER_ERROR
			);
		}
		
		return $rewriteOk;
	}
	
	/**
	 * Deletes one or more transactions from the database.
	 * 
	 * @param $transactionIDs The ids of the transactions.
	 * @param $creditNote If true, the amount of money from the transaction will be refunded.
	 * @return 
	 */
	function deleteTransactions($transactionIDs, $creditNote = true) {
		if(!is_array($transactionIDs)) {
			trigger_error_text('Die IDs der Transaktionen m&uuml;ssen eine Liste sein!', E_USER_ERROR);
			return false;
		}
		
		if(MysqlTransactionStart() === false) {
			return false;
		}
		
		$transactions = $this->getTransactionWithStatus(
			array('ordered','waiting','readyforpickup','completed')
		);
		
		foreach ($transactionIDs as $id) {
			$paid = false;
			
			if(isset($transactions[$id])) {
				$paid = ($transactions[$id]['paid'] == 1) ? true : false;
			}
			
			$refund = $creditNote;
			$refundAmount = formatMySQLDecimal($transactions[$id]['total']);
			
			// Add the money to the user's balance
			if($refund) {
				$refundOK = MysqlWrite(
					'UPDATE `' . $this->tableShopUserBalance .'` ' 
						. 'SET `balance` = `balance` + ' . $refundAmount . ' ' 
						. 'WHERE `userid` = ' . escape_sqlData_without_quotes($transactions[$id]['userid'])
				); 
				
				if($refundOK === false) {
					MysqlTransactionRollback();
					trigger_error_text(
						'Dem User #' . $transactions[$id]['userid'] 
							. ' konnte der Betrag von ' . sprintf('%.2f', $refundAmount) 
							. ' nicht gutgeschrieben werden!', 
						E_USER_ERROR
					);
					return false;
				} 
			}
		}
		
		$sqlTransactionIDs = implode_sqlIn($transactionIDs);
		
		// Delete the transaction items
		$delItemsOK = MysqlWrite(
			'DELETE FROM `' . $this->tableShopOrderItems . '` ' 
				. 'WHERE `orderid` IN (' . $sqlTransactionIDs . ')'
		);
		
		$delTransOk = MysqlWrite(
			'DELETE FROM `' . $this->tableShopOrders . '` ' 
				. 'WHERE `id` IN (' . $sqlTransactionIDs . ')'
		);
		
		if(($delItemsOK === false) || ($delTransOk === false)) {
			MysqlTransactionRollback();
			trigger_error_text(
				'Die Transaktionen (' 
					. implode(', ', $transactions) 
					. ') konnten nicht gel&ouml;scht werden!', 
				E_USER_ERROR
			);
			return false;
		}
		
		if(MysqlTransactionCommit() === false) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Calculates and returns statistics about how often an item has been ordered.
	 * 
	 * @return mixed The stats data
	 */
	function getItemStats() {
		// Get the item stats from the database
		$itemstats = MysqlReadArea(
			'SELECT *, SUM(oi.`amount`) AS `numorders`, (i.`price` * SUM(oi.`amount`)) AS `itemsum` ' 
				. 'FROM `' . $this->tableShopOrderItems . '` oi ' 
				. 'LEFT JOIN `' . $this->tableShopItems . '` i ' 
				. 'ON oi.`itemid` = i.`id` ' 
				. 'LEFT JOIN `' . $this->tableShopOrders . '` o ' 
				. 'ON o.`id` = oi.`orderid` ' 
				. 'WHERE o.`status` NOT IN(\'shoppingcart\', \'cashdesk\', \'ordered\') ' 
				. 'AND o.`paid` = 1 '
				. 'GROUP BY oi.`itemid` ' 
		);
		
		if($itemstats === false) {
			trigger_error_text('Konnte die Artikelstatistiken nicht auslesen!', E_USER_ERROR);
			return array();
		}
		
		// Calculate additional stats
		$itemPercentage = MysqlReadArea(
			'SELECT `itemid`, SUM(`amount`) AS `itemcount`, ' 
				. '(SELECT SUM(oi.`amount`) FROM `' . $this->tableShopOrderItems .'` oi ' 
					. 'LEFT JOIN `' . $this->tableShopOrders .'` o ON (oi.`orderid` = o.`id`) ' 
					. 'WHERE o.`status` NOT IN (\'shoppingcart\', \'cashdesk\', \'ordered\') AND o.`paid` = 1' 
				. ') AS `totalcount` ' 
				. 'FROM `' . $this->tableShopOrderItems . '` oi ' 
				. 'LEFT JOIN `' . $this->tableShopOrders .'` o ON (oi.`orderid` = o.`id`) ' 
				. 'WHERE o.`status` NOT IN (\'shoppingcart\', \'cashdesk\', \'ordered\') ' 
				. 'AND o.`paid` = 1 ' 
				. 'GROUP BY `itemid`',
			'itemid'
		);
		
		if($itemPercentage === false) {
			trigger_error_text('Konnte die Artikelstatistiken nicht auslesen!', E_USER_ERROR);
			return array();			
		}
		
		$groups = $this->getItemGroups();
		
		foreach ($itemstats as $index => $item) {
			if($itemPercentage[$item['itemid']]['totalcount'] > 0) {
				$itemstats[$index]['percentage'] 
					= ($itemPercentage[$item['itemid']]['itemcount'] 
						/ $itemPercentage[$item['itemid']]['totalcount']) * 100;
			}
			$itemstats[$index]['groupname'] = $groups[$item['groupid']]['name'];
		}
		
		return $itemstats;
	}
	
	/**
	 * Calculates the sales amount which is the total amount of money
	 * from records that are at least waiting and have paid = 1.
	 * 
	 * @return double Sales amount
	 */
	function getSales() {
		$sales = MysqlReadField(
			'SELECT SUM(i.`price` * oi.`amount`) AS `sales` ' 
				. 'FROM `' . $this->tableShopOrderItems . '` oi ' 
				. 'LEFT JOIN `' . $this->tableShopItems . '` i ' 
				. 'ON oi.`itemid` = i.`id` ' 
				. 'LEFT JOIN `' . $this->tableShopOrders . '` o ' 
				. 'ON o.`id` = oi.`orderid` ' 
				. 'WHERE o.`status` NOT IN (\'shoppingcart\', \'cashdesk\', \'ordered\') AND o.`paid` = 1',
			'sales',
			true
		);
		
		if($sales === false) {
			trigger_error_text('Konnte den Umsatz nicht auslesen!', E_USER_ERROR);
			return 0;		
		}
		
		return $sales;
	}
	
	/**
	 * Returns a list containing the balance accounts of users.
	 * 
	 * @return array The list of events. Returns an empty array on error!
	 */
	function getBalanceAccounts() {
		$accounts = MysqlReadArea(
			'SELECT *,s.`name` AS `username` FROM `' . $this->tableShopUserBalance . '` b ' 
				. 'LEFT JOIN `' . TblPrefix() . 'flip_user_subject` s ' 
				. 'ON (b.`userid` = s.`id`)'
		);
		
		if($accounts === false) {
			trigger_error_text('Konnte die Liste der Guthaben nicht auslesen!', E_USER_ERROR);
			return array();
		}
		
		return $accounts;
	}
}

?>