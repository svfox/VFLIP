<?php
/**
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.SEARCH.CONTENT.PHP")) return 0;
define("MOD.SEARCH.CONTENT.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

class Search_content extends Search //part after Search_ should be the same as filenamepart after mod.search. (case!)
{
  function Search($searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    $r = $this->_searchContent("text", $searchtexts, $seperator);
    $r = array_merge($r, $this->SearchTitle($searchtexts, $seperator));
    
    return $r;
  }
  
  function SearchTitle($searchtexts, $seperator="AND")
  {
    return $this->_searchContent("caption", $searchtexts, $seperator);
  }
  
  function _searchContent($col, $searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    $result = MysqlReadArea("SELECT `caption`, `name`, `text`
                                FROM ".TblPrefix()."flip_content_text
                                WHERE (`$col` LIKE ".implode_sql(" $seperator `$col` LIKE ", $searchtexts).")
                                  AND ".$User->sqlHasRight("view_right"));
    foreach($result AS $row)
      $r[] = array("title"  => $row["caption"],
                   "link"   => "text.php?name=".$row["name"],
                   "text"   => $this->_format($row["text"], $searchtexts)
                  );
    return $r;
  }
}

?>