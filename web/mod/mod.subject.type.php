<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: mod.subject.type.php 1376 2007-03-05 12:03:24Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

class Type extends Subject {
	function requireAllowCreate() {
		global $User;
		$User->requireRight("user_admin_subjects");
	}
}
?>