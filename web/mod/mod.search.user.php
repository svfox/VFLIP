<?php
/**
 * @author Daniel Raap
 * @version $Id$ 1702 2019-01-09 09:01:12Z gregor $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.SEARCH.USER.PHP")) return 0;
define("MOD.SEARCH.USER.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

class Search_user extends Search //part after Search_ should be the same as filenamepart after mod.search. (case!)
{
  function Search($searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    //es werden nur access=data durchsucht! Also keine Emailadressen (subject)
    //Gregor Nebel: 08.09.2011 - Suche  ignoriert Gross/Kleinschreibung
    $result = MysqlReadArea("SELECT c.caption, d.subject_id, d.val, c.required_view_right
                             FROM ".TblPrefix()."flip_user_data d INNER JOIN ".TblPrefix()."flip_user_column c ON d.column_id=c.id
                             WHERE (lower(d.`val`) LIKE lower(".implode_sql(" $seperator d.`val` LIKE ", $searchtexts)."))
                               AND c.type='user'"
                           );
    foreach($result AS $row)
      if($User->hasRightOver($row["required_view_right"], $row["subject_id"]))
        $r["ID".$row["subject_id"]] = array("title"   => GetSubjectName($row["subject_id"]),
                                            "link"    => "user.php?frame=editproperties&amp;id=".$row["subject_id"],
                                            "text"    => escapeHtml($row["caption"]).": ".$this->_format($row["val"], $searchtexts)
                                           );
    $r = array_merge($this->SearchTitle($searchtexts, $seperator), $r);
    return $r;
  }
  
  function SearchTitle($searchtexts, $seperator="AND")
  {
	$r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    //Gregor Nebel: 08.09.2011 - Suche  ignoriert Gross/Kleinschreibung
    $result = MysqlReadArea("SELECT `name`, `id`
                             FROM ".TblPrefix()."flip_user_subject
                             WHERE (lower(`name`) LIKE lower(".implode_sql(" $seperator `name` LIKE ", $searchtexts)."))
                               AND `type`='user'"
                           );
    $namecaption = MysqlReadField("SELECT caption FROM ".TblPrefix()."flip_user_column WHERE `name`='name' AND `type`='user'", "caption");
    
    foreach($result AS $row) {
		$r["ID".$row["id"]] = array(
			"userid" => $row["id"],
			"title"  => $row["name"],
			"link"   => "user.php?frame=viewsubject&amp;id=".$row["id"],
			"text"   => "$namecaption: ".$this->_format($row["name"], $searchtexts)
		);
    }
    
    return $r;
  }
}

?>