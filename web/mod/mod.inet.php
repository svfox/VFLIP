<?php

/**
 * Mit dem FLIP-INet-Modul werden Antr&auml;ge auf Internet verwaltet.
 * Die Datei inet.php stellt damit lediglich das Frontend mit GUI dar, &uuml;ber
 * das sich das Modul einstellen l&auml;sst und User ihren Antragsstatus stellen k&ouml;nnen.
 * 
 * Ein Antrag kann 3+1 Zust&auml;nde haben: 
 * - waiting -> User in der Warteschlange
 * - granted -> User freigeschalten
 * - expired -> Antrag abgelaufen & Internet wieder gesperrt.
 * 
 * Der 4. Zustand heisst 'unrequested' - er ist nur f&uuml;r das Template interessant und
 * daher nicht in der DB verwendbar. 
 * Alle Zust&auml;nde sind zudem als Key in der Content-Tabelle inet_status aufgelistet.
 * 
 * Werden die Adressen aller freigeschaltenen User zur&uuml;ckgegeben, so sind zuerst
 * die User mit der h&ouml;chsten Priorit&auml;t an der Reihe. Damit ist es auch m&ouml;glich
 * den Usern Vorrang zu geben, die z.B. vor einem Turnier ihr Steam updaten m&uuml;ssen.
 * Die Zuweisung der Prios erfolgt via Mitgliedschaft in einer Gruppe. Ist ein User
 * in mehreren Gruppen, dann erh&auml;lt er immer die Einstellungen der Gruppe, die die h&ouml;chste
 * Priorit&auml;t hat.
 * Beispiel: User1 ist in der Gruppe registered mit Prio 1 aber auch in der 
 * Gruppe Orga (Prio 5) -> Der User erh&auml;lt die Prio 5.
 * 
 * Ruft z.B. ein Router die IPs zur Freischaltung ab, dann muss seine IP-Addy in der 
 * Config-Variable inet_allowed_hosts enthalten sein.
 * Es wird ihm dann eine Liste mit den Adressen zur&uuml;ckgegeben - jede Adresse mit einem
 * via inet_address_seperator einstellbaren Trennzeichen ('.' LOL :D) getrennt von der Anderen.
 * Zus&auml;tzlich kann via inet_address_header bzw. inet_address_footer noch ein Header oder Footer
 * hinzugef&uuml;gt werden - n&uuml;tzlich f&uuml;r z.B. CVS-Exports :)
 * 
 * Hinweis:
 * 
 * Einige Funktionen wurden in eine einzige vereinigt, wie bei GetUserInfo
 * Wenn man nur eine einzige Variable (z.B. die Prio) ben&ouml;tigt, dann ergibt
 * sich hier kein Vorteil, da alle Infos auf einmal herausgegeben werden.
 * Werden jedoch mehrere Variablem (Prio, Status...) ben&ouml;tigt, dann arbeitet die 
 * jetzige Funktion schneller, da die Sortierung bzw. das Extrahieren mittels PHP erledigt wird.
 * Da somit nur eine einzige MySQL-Query n&ouml;tig ist, ist das besser als 3 oder 4 hintereinander...
 * 
 * @author Matthias Gro&szlig;
 * @version $Id: mod.inet.php 1702 2019-01-09 09:01:12Z scope $ by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ('core/core.php');

/** Additional includes */
require_once ('mod/mod.sendmessage.php');

class FLIPINetModule {

	var $_AddressSeparator = '';
	var $_AddressHeader = '';
	var $_AddressFooter = '';
	var $_AllowedHosts = array ();
	var $_AllowMultipleRequests = false;
	var $_DefaultPriority = 1;
	var $_DefaultOnlineTime = 0;
	var $_MaxOnlineUsers = 0;
	var $_MaxRequests = '';
	var $_NoExp_As_Online = true;

	var $_NotifyMessage = 'inet_notification';

	/**
	 * Der Klassenkonstruktor.
	 * Er sorgt f&uuml;r die Zuweisung der Klassenvariablen.
	 */
	//php 7 public function __construct()
	//php 5 original function FLIPINetModule()
	function __construct() {
		// Config-Vars holen
		$t = '';
		// String von PHP parsen lassen (erlaubt Sonderzeichen, z.B. \n)
		eval ('$t = "' . ConfigGet('inet_address_seperator') . '";');
		$this->_AddressSeparator = $t;
		eval ('$t = "' . ConfigGet('inet_address_header') . '";');
		$this->_AddressHeader = $t;
		eval ('$t = "' . ConfigGet('inet_address_footer') . '";');
		$this->_AddressFooter = $t;
		$this->_AllowedHosts = ConfigGet('inet_allowed_hosts');
		$this->_AllowMultipleRequests = (ConfigGet('inet_allow_multiple_requests') == 'Y') ? true : false;
		$this->_DefaultPriority = ConfigGet('inet_default_priority');
		$this->_DefaultOnlineTime = ConfigGet('inet_default_onlinetime') * 60;
		$this->_MaxRequests = ConfigGet('inet_max_requests');
		$this->_MaxOnlineUsers = ConfigGet('inet_max_online');
		$this->_NoExp_As_Online = (ConfigGet('inet_noexpiry_as_online') == 'Y') ? true : false;
	}

	/**
	 * Mit RequestAccess() stellt ein User ($UserID) einen Antrag auf Internet.
	 * Hat er schon einen Antrag gestellt, dann wird sein Alter &uuml;berschrieben
	 * @param integer $Target Die ID oder eine Instanz des Usersubjektes
	 * @return boolean true, bei erfolgreicher Anfrage sonst false
	 */
	function RequestAccess($Target) {
		// Warten bereits zuviele User?
		$numwaiting = MysqlReadField("SELECT COUNT(`id`) FROM `" . TblPrefix() . "flip_inet_requests` WHERE `status` = 'waiting';");
		if ($numwaiting >= $this->_MaxRequests)
			return trigger_error_text('Es sind bereits zuviele User in der Warteschlange!', E_USER_ERROR);
		// Hat der User bereits INet beantragt, wenn ja, darf er nochmal?
		$userinfo = $this->GetUserInfo($Target);
		if ($userinfo['status'] != 'unrequested') {
			if (!$this->_AllowMultipleRequests) {
				trigger_error_text('Du hast bereits einen Antrag gestellt!', E_USER_ERROR);
				return false;
			} else
				$this->UnRequestAccess($Target, false); // Alten Antrag l&ouml;schen
		}
		$u = CreateSubjectInstance($Target);
		// INet-Gruppe herausfinden f&uuml;r Prio und OnlineTime
		$groups = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_inet_groups` ORDER BY `priority` DESC;");
		$u_groups = $u->getParents('group');
		$db['user_id'] = escape_sqlData($u->id);
		$db['status'] = 'waiting';
		$db['online_time'] = $this->_DefaultOnlineTime;
		$db['priority'] = $this->_DefaultPriority;
		foreach ($groups as $g) {
			if (isset ($u_groups[$g['subject_group_id']])) {
				$db['priority'] = $g['priority'];
				$db['online_time'] = $g['online_time'];
				break;
			}
		}
		// Anfrage in die DB schreiben
		return (MysqlWriteByID(TblPrefix() . 'flip_inet_requests', $db) > 0) ? true : false;
	}

	/**
	 * Mit UnRequest() nimmt ein User ($UserID) einen Antrag auf Internet wieder zur&uuml;ck.
	 * @param integer $Target Die ID oder eine Instanz des Usersubjektes
	 * @return boolean true, bei Erfolg sonst false
	 */
	function UnRequestAccess($Target, $checkwaiting = true) {
		if (($this->GetRequestStatus($Target) != 'waiting') && $checkwaiting)
			return false;
		return MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_inet_requests` WHERE `user_id` = '" .escape_sqlData_without_quotes(GetSubjectID($Target)) . "';");
	}

	/**
	 * GetUserInfo() liefert diese Informationen &uuml;ber einen User:
	 * > Alle Infos aus der Datenbank PLUS
	 * - Seine Position in der Warteschlange, falls er wartet
	 * - Die Zeit, die er Online verbleibt (inkl. sein vorraussichtlicher Sperrzeitpunkt), falls er online ist
	 * -> array('id','user_id','status','priority','online_time','online_since',['timeleft'],['online_until'],['queuepos']);
	 * @param integer $UserID Die ID des Users
	 * @return array Ein Array mit den Userinformationen
	 */
	function GetUserInfo($UserID) {
		static $cache = array ();
		if (!isset ($cache[$UserID])) {
			// versuchen Daten aus DB zu lesen
			$row = MysqlReadRow('SELECT * FROM `' . TblPrefix() . 'flip_inet_requests` WHERE user_id=\'' .escape_sqlData_without_quotes($UserID) . '\' ORDER BY `status`,`priority` DESC,`id` LIMIT 1;', true);
			if (is_array($row)) {
				// Position in der Warteschlange, falls User wartet
				if ($row['status'] == 'waiting')
					$row['queuepos'] = $this->GetUserQueuePos($UserID);
				// Zeit berechnen, falls User online sein darf
				if ($row['status'] == 'granted') {
					$row['timeleft'] = ($row['online_since'] + $row['online_time']) - time();
					$row['online_until'] = $row['online_since'] + $row['online_time'];
				}
				$cache[$UserID] = $row;
			}
		}

		// return
		if (isset ($cache[$UserID])) {
			return $cache[$UserID];
		} else {
			// keine Anfrage des Users vorhanden
			return array (
				'status' => 'unrequested'
			);
		}
	}

	/**
	 * GetRequestStatus() gibt den Status des Antrags zur&uuml;ck.
	 * Existiert keiner, dann ist die R&uuml;ckgabe 'unrequested'
	 * @param integer $UserID Die ID des Users
	 * @return string Der Status des Antrags 'unrequested', 'waiting', 'granted' oder 'noexpiry'
	 */
	function GetRequestStatus($UserID) {
		$userinfo = $this->GetUserInfo($UserID);
		return $userinfo['status'];
	}

	/** OBSOLETE
	 * GetOnlineUsers() gibt die Anzahl User zur&uuml;ck, die freigeschalten sind.
	 * @return integer Anzahl User
	 */
	function GetOnlineUsers() {
		return MysqlReadCol("SELECT `user_id` FROM `" . TblPrefix() . "flip_inet_requests` WHERE `status` = 'granted' OR `status` = 'noexpiry';", "user_id", "user_id");
	}

	/**
	 * GetUserQueuePos gibt die Position eines Users in der Warteschlange zur&uuml;ck
	 * @param integer $UserID Die ID des Users
	 * @return integer Die Position des Users 
	 */
	function GetUserQueuePos($UserID) {
		// einmal alle Wartenden auslesen
		static $waiting;
		if (!isset ($waiting))
			$waiting = MysqlReadCol('SELECT user_id FROM `' . TblPrefix() . 'flip_inet_requests` WHERE `status` = \'waiting\' ORDER BY `priority` DESC, `id`;', 'user_id');
		// pr&uuml;fen ob der User dabei ist
		if (is_array($waiting)) {
			$n = array_search($UserID, $waiting);
			if ($n !== false) // 0 z&auml;hlt auch als false
				return $n+1;
		}
		return -1;
	}

	/**
	 * GetUserTimeleft gibt die Zeit zur&uuml;ck, die ein User noch online sein darf
	 * @param integer $UserID Die ID des Users
	 * @return integer Die Restzeit des Users 
	 */
	function GetUserTimeleft($UserID) {
		$userinfo = $this->GetUserInfo($UserID);
		ArrayWithKeys($userinfo, array (
			'timeleft'
		));
		return $userinfo['timeleft'];
	}

	/**
	 * 
		MysqlExecQuery("UPDATE `".TblPrefix()."flip_inet_requests` SET `status` = 'expired' WHERE `status` = 'granted' AND ((`online_since` + `online_time`) < UNIX_TIMESTAMP());");
		$online = $this->GetOnlineUsers();
		$freeplaces = $this->_MaxOnlineUsers - count($online);
		if($freeplaces > 0) {
			MysqlExecQuery("UPDATE `".TblPrefix()."flip_inet_requests` SET `status` = 'granted', `online_since` = UNIX_TIMESTAMP() WHERE `status` = 'waiting' ORDER BY `priority` DESC LIMIT ".escape_sqlData($freeplaces).";");
			$online = $this->GetOnlineUsers(); // Nochmal, da $online out-of-date!
		}
		return $online;
		
	 * UpdateRequests()
	 * - Holt sich alle Requests die wartend oder freigeschalten sind.
	 * - Filtert alle Requests heraus, deren Zeit noch nicht abgelaufen ist
	 * - Holt sich Wartende Requests, die jetzt nachger&uuml;ckt werden k&ouml;nnen gem&auml;&szlig; ihrer Prio
	 * - Und f&uuml;hrt Updates auf die betroffenen Datens&auml;tze aus.
	 * - Zus&auml;tzlich werden betroffene User via Webmessage informiert.
	 * - r&uuml;ckt neue, warteden User gem&auml;&szlig; ihrer Prio nach aber nie mehr als zul&auml;ssig
	 * @return array Die IDs der User, die jetzt online sind.
	 */
	function UpdateRequests() {
		$requests = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_inet_requests` WHERE (`status` != 'expired') ORDER BY `status` DESC, `priority` DESC, `id`;");
		$granted = $expired = $online = array ();
		$onlinetimes = array (); // f&uuml;r Nachrichten
		$time = time();
		$num_online = 0;
		$freeplaces = -1;
		foreach ($requests as $r) {
			$onlinetimes[$r['user_id']] = $r['online_time'];
			if ($r['status'] == 'granted') {
				if (($r['online_since'] + $r['online_time']) <= $time)
					$expired[] = $r['user_id'];
				else {
					$online[] = $r['user_id'];
					$num_online++;
				}
			}
			elseif ($r['status'] == 'noexpiry') {
				if ($this->_NoExp_As_Online)
					$num_online++; // Noexpiry ber&uuml;cksichtigen
				$online[] = $r['user_id'];
			} else { // Durch ORDER BY kommen jetzt keine 'granted' oder 'noexpiry' mehr!
				if ($freeplaces < 0) // Freie Pl&auml;tze berechnen!
					$freeplaces = $this->_MaxOnlineUsers - $num_online;
				if ($freeplaces > 0) { // User kann nachger&uuml;ckt werden
					$granted[] = $r['user_id'];
					$freeplaces--;
				}
				elseif ($freeplaces < 1) break; // Abbruch, keine freien Pl&auml;tze mehr
			}
		}
		// $expired -> IDs aller User, die zu sperren sind
		// $granted -> IDs aller User, die freigeschalten werden k&ouml;nnen
		// $online  -> IDs aller User, die unber&uuml;hrt (noch online) bleiben

		// User benachrichtigen
		if (!empty ($expired))
			$this->SendINetNotice($expired, 'expired');
		if (!empty ($granted))
			$this->SendINetNotice($granted, 'granted', array (
				'onlinetimes' => $onlinetimes,
				'inet_granttime' => date('d.m.Y \u\m G:i \U\h\r',
				$time
			)));

		// Update ausf&uuml;hren
		$ids = implode_sqlIn(array_merge($expired, $granted));
		MysqlWrite("UPDATE `" . TblPrefix() . "flip_inet_requests` SET `status` = IF(`status`='granted','expired','granted'), `online_since` = '" .escape_sqlData_without_quotes($time) . "' WHERE `user_id` IN ($ids);");
		return array_merge($online, $granted);
	}

	/**
	 * GetRawUserIPs() liefert ein Array mit den IPs der freischaltenen Usern.
	 * @return array Die Adressen der User im Format array('IP1','IP2',...) usw.
	 */
	function GetRawUserIPs() {
		$online = $this->UpdateRequests();
		$ips = GetSubjects('user', array (
			'id',
			'ip'
		), 's.id IN (' . implode_sqlIn($online) . ')');
		$rawips = array ();
		foreach ($ips as $u)
			$rawips[] = $u['ip'];
		return $rawips;
	}

	/**
	 * GetFormattedUserIPs() gibt die IP-Adressen der User zur&uuml;ck,
	 * die f&uuml;rs Internet freigeschalten wurden.
	 * @param string $Host Die IP-Adresse des anfragenden Hosts
	 * @return string Ein String, der die Adressen im eingestellten Format darstellt. 
	 */
	function GetFormattedUserIPs($host) {
		if (!in_array($host, explode(';', $this->_AllowedHosts)))
			return 'Dieser Host (' . $host . ') ist nicht berechtigt die UserIPs abzurufen!';

		$rawips = $this->GetRawUserIPs();

		$formatted_ips = '';

		if (!empty ($this->_AddressHeader))
			$formatted_ips .= $this->_AddressHeader;

		$formatted_ips .= !empty ($rawips) ? implode($this->_AddressSeparator, $rawips) : '';

		if (!empty ($this->_AddressFooter))
			$formatted_ips .= $this->_AddressFooter;

		return $formatted_ips;
	}

	/**
	 * SendINetNotice() benachrichtigt User, dass ihre Antr&auml;ge jetzt
	 * den Status $Status haben. Der Content der Webmessage wird dynamisch anhand
	 * der Eigenschaft $Status generiert.
	 * @param mixed $Targets Die IDs der User -> array('1296','1297',...)
	 * @param string $Status Der jetztige Status
	 * @param array $AdditionalParams Ein Array, das Zusatzinfos enth&auml;lt
	 * @return boolean Ob die Benachrichtigung geklappt hat
	 */
	function SendINetNotice($Targets, $Status, $AdditionalParams = array ()) {
		if (empty ($Targets) or !is_array($Targets))
			return false;
		$msg = new ExecMessage();
		$msg->messageFromDB($this->_NotifyMessage);
		$params = array ();
		switch (strtolower($Status)) {
			case 'waiting' : // User in der Warteschlange
				$msg->Subject = 'INet: Warteschlange';
				break;
			case 'expired' : // User gesperrt
				$msg->Subject = 'INet: Zeit abgelaufen';
				break;
			default : // User freigeschalten (granted oder noexpiry)
				$onlinetimes = $AdditionalParams['onlinetimes'];
				unset ($AdditionalParams['onlinetimes']);
				$msg->Subject = 'INet: Freigeschalten';
				break;
		}
		$AdditionalParams['inet_status'] = strtolower($Status);
		$msg->Params = $AdditionalParams;
		foreach ($Targets as $t) {
			if (isset ($onlinetimes))
				$msg->Params['inet_accesstimespan'] = $this->Seconds2Time($onlinetimes[$t]);
			$msg->sendMessage(CreateSubjectInstance($t, 'user'));
		}
	}

	/**
	 * Seconds2Time() extrahiert die Anzahl der Stunden, Minuten und Sekunden aus 
	 * einer Variablen, die eine Zeitspanne in Sekunden enth&auml;lt und gibt die 
	 * Informationen als String.
	 * -> Seconds2Time(3677) ergibt '1 Stunde 1 Minute und 17 Sekunden'
	 * @param integer $Timespan Die Zeitspanne in Sekunden
	 * @return string Ein String mit den Zeitinfos
	 */
	function Seconds2Time($Timespan) {
		if (!empty ($Timespan)) {
			if ($Timespan < 1)
				return 'keine';
			$hours = floor($Timespan / 3600);
			$seconds = ($Timespan - ($hours * 3600)) % 60;
			$minutes = floor((($Timespan - ($hours * 3600)) - $seconds) / 60);
			
			// Formatieren - Viel Code f&uuml;r eine sch&ouml;ne Darstellung :P
			if ($hours > 0)
				$h = ($hours > 1) ? 'Stunden' : 'Stunde';
			else
				$hours = $h = '';
			if ($minutes > 0)
				$m = ($minutes > 1) ? 'Minuten' : 'Minute';
			else
				$minutes = $m = '';
			if ($seconds > 0)
				$s = ($seconds > 1) ? 'Sekunden' : 'Sekunde';
			else
				$seconds = $s = '';
				
			$and_ms = (!empty ($minutes) && !empty ($seconds)) ? 'und' : '';
			$and_hm = (!empty ($hours) && !empty ($minutes) && empty ($and_ms)) ? 'und' : '';
			return "$hours $h $and_hm $minutes $m $and_ms $seconds $s";
		}
		return '';
	}

}
?>