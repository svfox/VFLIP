<?php
/**
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.SEARCH.TICKETS.PHP")) return 0;
define("MOD.SEARCH.TICKETS.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

class Search_tickets extends Search //part after Search_ should be the same as filenamepart after mod.search. (case!)
{
  var $ticket_viewright = "ticket_view";
  
  function Search($searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    if($User->hasRight($this->ticket_viewright))
    {
      if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
      $result = MysqlReadArea("SELECT `title`, e.`ticket_id`, MAX(`version`), `description`
                               FROM ".TblPrefix()."flip_ticket_event e INNER JOIN ".TblPrefix()."flip_ticket_tickets t ON e.ticket_id=t.ticket_id
                               WHERE (`description` LIKE ".implode_sql(" $seperator `description` LIKE ", $searchtexts).")
                               GROUP BY ticket_id
                               ORDER BY t.version DESC"
                            );
      foreach($result AS $row)
        $r[] = array("title"  => $row["title"],
                     "link"   => "ticket.php?frame=ticket&amp;id=".$row["ticket_id"],
                     "text"   => $this->_format($row["description"], $searchtexts)
                    );
      $r = array_merge($r, $this->SearchTitle($searchtexts, $seperator));
    }
    
    return $r;
  }
  
  function SearchTitle($searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if($User->hasRight($this->ticket_viewright))
    {
      if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
      $result = MysqlReadArea("SELECT `title`, `ticket_id`, `create_time`, `create_user`, MAX(`version`) AS `version`
                              FROM ".TblPrefix()."flip_ticket_tickets
                              WHERE (`title` LIKE ".implode_sql(" $seperator `title` LIKE ", $searchtexts).")
                              GROUP BY ticket_id"
                            );
      foreach($result AS $row)
        $r[] = array("title"  => $row["title"],
                     "link"   => "ticket.php?frame=ticket&amp;id=".$row["ticket_id"],
                     "text"   => "erstellt am ".date("d.m.y",$row["create_time"])." von ".GetSubjectName($row["create_user"])." (v".$row["version"].")"
                    );
    }
    return $r;
  }
}

?>