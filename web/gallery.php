<?php
/**
 * @author Daniel Herrmann
 * @version $Id: gallery.php 1702 2019-01-09 09:01:12Z daniel $ edit by VulkanLAN
 * @copyright 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 * 
 * Ideas to include:
 * 
 * TODO: admin-tool: resize all pictures (if the config max-width or max-height value was changes after adding some pictures)
 * FIXME: Increase Piccounter only once per ip in 24h
 * 
 **/

/** FLIP core **/
require_once ('core/core.php');
require_once ('inc/inc.page.php');
require_once ('inc/inc.text.php');
require_once ('mod/mod.image.php');
require_once ('mod/mod.imageedit.php');

require_once ('mod/mod.gallery.php');

/**
 * 
 * @author waza-ari
 *
 * Class for representing a picture gallery in Flip.
 *
 */
class GalleryPage extends Page {
	
	//rights
	private $right_admin = 'gallery_admin';
	private $right_view = 'gallery_view';
	
	//config stuff
	private $picsperrow = 'gallery_maxpics';
	private $picmaxheight = 'gallery_maxheight';
	private $picmaxwidth = 'gallery_maxwidth';
	private $thumbmaxheight = 'gallery_thmaxheight';
	private $thumbmaxwidth = 'gallery_thmaxheight';
	private $hits = 'gallery_hits';
	private $maxrows = 'gallery_maxrows';
	private $cpicpath = 'gallery_picpath';
	private $cthumbpath = 'gallery_thumbpath';
	private $endings = 'gallery_file_ends';
	private $albperpage = 'gallery_albumsperrow';
	private $directpics = 'gallery_directtopics';
	
	//template variables read from config in constructor
	private $picpath;
	private $thumbpath;

	/**
	 * Contructor of this class.
	 * Sets some settings, checks whether the user has the required right and checks, whether the paths are writeable by webserver.
	 */
	 //php 7 public function __construct()
	//php 5 original function GalleryPage()
	public function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		global $User;
		$User->requireRight($this->right_view);
		$this->Caption = $this->Title;
		$this->picpath = ConfigGet($this->cpicpath);
		$this->thumbpath = ConfigGet($this->cthumbpath);
		If (!is_writeable($this->picpath)) trigger_error('Der Ordner für die Bilder ('.$this->picpath.') existiert nicht oder ist nicht beschreibar. Der Ordner ist in der Config änderbar.', E_USER_ERROR);
		If (!is_writeable($this->thumbpath)) trigger_error('Der Ordner für die Bilder ('.$this->thumbpath.') existiert nicht oder ist nicht beschreibar. Der Ordner ist in der Config änderbar.', E_USER_ERROR);
	}

	 /****************************************************\
	 * 			        End-User Frames                   *
	 \****************************************************/

	/**
	 * Default Frame, simply shows the overview.
	 * 
	 * @param $get The get paramters, not needed in this case.
	 */
	public function frameDefault($get, $post = false) {
		$this->Caption = 'Bildergalerie';
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		$r['parent'] = (is_numeric($get['parentid'])) ? $get['parentid'] : 0;
		$r['pparent'] = ($r['parent'] == 0) ? null : MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'parent_id', $get['parentid']);
		$r['path'] = ($r['parent'] == 0) ? 'Root' : implode(' => ', array_merge(array('Root'), array_reverse($this->getPathToRoot($r['parent']))));
		$r['cats'] = $this->showcats($r['parent'], false, 1);
		$r['countcats'] = MysqlReadField('SELECT count(`id`) AS `count` FROM `'.TblPrefix().'flip_gallery_categories`;', 'count');
		$r['countalbums'] = MysqlReadField('SELECT count(`id`) AS `count` FROM `'.TblPrefix().'flip_gallery_albums`;', 'count');
		$r['countpics'] = MysqlReadField('SELECT count(`id`) AS `count` FROM `'.TblPrefix().'flip_gallery_pics`;', 'count');
		global $User;
		If ($User->hasright($this->right_admin)) $r['admin'] = true;
		return $r;
	}
	
	/**
	 * Frame showing the albums of a categorie
	 * 
	 * @param array $get the get parameters
	 */
	public function frameAlbums($get) {
		//Some tpl vars
		$r = $this->setTplVars();
		
		//Set the catid
		$r['catid'] = $get['catid'];
		if (!is_numeric($r['catid'])) trigger_error_text('Given category id is not valid', E_USER_ERROR);
		
		$r['cat'] = MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $r['catid']);
		
		if (!is_array($r['cat'])) trigger_error_text('Given category id is not valid', E_USER_ERROR);
		//Set the caption
		$this->Caption = 'Alben der Kategorie '.$r['cat']['name'];
		
		//Read out the albums, including the titlepic path
		//If no titlepic is specified, just use a random one
		//returns null, if there is no picture at all
		$albums = MysqlReadArea('SELECT IF(`p`.`filename` IS NOT NULL, `p`.`filename`, (SELECT `p2`.`filename` FROM `'.TblPrefix().'flip_gallery_pics` AS `p2` WHERE `p2`.`album_id`=`a`.`id` ORDER BY RAND() LIMIT 1)) AS `titlepic`, `a`.`name`, `a`.`id`, (SELECT COUNT(`p2`.`id`) FROM `'.TblPrefix().'flip_gallery_pics` AS `p2` WHERE `p2`.`album_id`=`a`.`id`) AS `piccount`, DATE_FORMAT(`a`.`mtime`, \'%e.%m.%Y %k:%i\') AS `mtime` FROM `'.TblPrefix().'flip_gallery_albums` AS `a` LEFT JOIN `'.TblPrefix().'flip_gallery_pics` AS `p` ON `a`.`titlepicid`=`p`.`id` WHERE `a`.`catid`=\''.$r['catid'].'\' ORDER BY `a`.`position`;');
		
		//set the album array
		$r['albums'] = array();
		$i = 0;
		$b = array();
		foreach ($albums as $a) {
			$b[] = $a;
			$i++;
			if ($i == ConfigGet($this->albperpage)) {
				$r['albums'][] = $b;
				$b = array();
				$i = 0;
			}
		}
		if ($i != 0) $r['albums'][] = $b;
		
		return $r;
	}

	/**
	 * Picture for displaying the pictures of an album
	 * 
	 * @param array $get The get parameters
	 */
	public function framePics($get) {
		//Get the necessary config values
		$r = $this->setTplVars();
		$maxRows = ConfigGet($this->maxrows);
		$maxPicsPerRow = ConfigGet($this->picsperrow);
		
		//Get information about the album
		$r['albumid'] = $get['albumid'];
		$r['piccount'] = $this->getpiccount($r['albumid'] );
		
		//some calculations
		$r['page'] = (empty($get['page']) || !is_numeric($get['page'])) ? 1 : $get['page'];
		
		$numofpics = $maxPicsPerRow * $maxRows;
		$start = ($r['page'] - 1) * $numofpics;
		
		$r['bwidth'] = $r['thumbmaxwidth'] + 15;
		$r['bheight'] = $r['thumbmaxheight'] + 50;
		
		$r['pages'] = ceil($r['piccount'] / $numofpics);
		
		//Get required Data
		if (!is_numeric($r['albumid']) || ($album = MysqlReadRow('SELECT * FROM `'.TblPrefix().'flip_gallery_albums` WHERE `id` = \''.$r['albumid'].'\';')) == false) {
			trigger_error_text('Das angegebene Album existiert nicht.', E_USER_ERROR, 'gallery.php', 119);
		}
		$pics = MysqlReadArea('SELECT `id`, `name`, `filename`, `clicks` FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.$r['albumid'] .'\' ORDER BY `position` LIMIT '.$start.', '.$numofpics.';');
		$r['pcatid'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_albums', 'catid', $r['albumid']);
		
		//Set caption
		$this->Caption = sprintf('Album %s in der Kategorie %s', $album['name'], MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'name', $album['catid']));
		
		//If no pictures are present
		if (count($bilder) == 0) $bilder = array();
		
		//calculate the PictureArray
		$r['rows'] = array();
		$i = 0;
		$b = array();
		foreach ($pics as $a) {
			$b[] = $a;
			$i++;
			if ($i == $maxPicsPerRow) {
				$r['rows'][] = $b;
				$b = array();
				$i = 0;
			}
		}
		if ($i != 0) $r['rows'][] = $b;
		
		//Calculate whether there is a previous and next page existing
		If ($r['page'] - 1 > 0) $r['less'] = $r['page'] - 1;
		If ((($r['page']) * $numofpics) < $r['piccount']) $r['next'] = $r['page'] + 1;
		return $r;
	}

	/**
	 * Frame for showing one picture
	 * 
	 * @param array $get The given get-parameters
	 */
	public function frameShowpic($get) {
		$this->headerRevalidate();
		//set some necessary template vars
		$r = $this->setTplVars();
		//Read out the data from database
		$r = array_merge($r,MysqlReadRowByID(TblPrefix().'flip_gallery_pics', escape_sqlData($get['picid'])));
		//get the next and previous pictures
		$r = $this->getnextandprevpics($r['id'], $r['album_id'], $r);
		
		//Update the hits of this picture when enabled at config
		if (ConfigGet($this->hits) == 'Y')
			MysqlExecQuery('UPDATE `'.TblPrefix().'flip_gallery_pics` SET clicks=clicks+1 WHERE `id`='.$r['id'].';');
		
		//Read out album-specific Data
		$r['albumname'] =MysqlReadFieldByID(TblPrefix().'flip_gallery_albums', 'name', $r['album_id']);
		$r['backlink'] = $_SERVER["HTTP_REFERER"];
		$this->Caption = sprintf("Bild %s im Album %s", $r['name'], $r['albumname']);
		return $r;
	}
	
	 /****************************************************\
	 * 			       admin - functions                  *
	 \****************************************************/
	
	/********** categorie administration **************/

	/**
	 * Default view for admin - just shows up the categories to mange them
	 * 
	 * @param unknown_type $get
	 */
	public function frameAdmin($get) {
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		global $User;
		$User->requireRight($this->right_admin);
		$c = (is_numeric($get['pid'])) ? $get['pid'] : 0;
		$r['pid'] = $c;
		$r['ppid'] = ($c == 0) ? -1 : MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'parent_id', $c);
		$r['cats'] = $this->showcats($c, true);
		$r['path'] = ($c == 0) ? "Root" : implode(" => ", array_merge(array("Root"), array_reverse($this->getPathToRoot($c))));
		$this->Caption = "Galerie - Administration";
		return $r;
	}

	/**
	 * Frame for creating or editing a (new) categorie
	 * 
	 * @param array $get get Data containing the pid of the categorie we want to add a sub categorie to
	 * @return array Template information
	 */
	public function frameModCat($get) {
		//Does the user have the right to add a categorie?
		global $User;
		$User->requireRight($this->right_admin);
		$this->Caption="Kategorie hinzufügen";
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		$r['pcatid'] = escape_sqlData($get['pid']);
		$r['catid'] = (is_numeric($get['id']) && MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $get['id'], true)) ? $get ['id'] : 0;
		//If we change the categorie, read out the data from database
		if ($r['catid'] != 0) {
			$data = MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $r['catid']);
			$this->Caption="Kategorie bearbeiten";
			$r['pcatid'] = $data['parent_id'];
			$r['name'] = $data['name'];
			$r['desc'] = $data['description'];
		}
		If ($r['pcatid'] == '0' OR empty($r['pcatid'])) {
			$r['pcatname'] = 'Neue Hauptkategorie';
		} else {
			$r['pcatname'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'name', $r['pcatid']);
		}
		return $r;
	}
	
	/**
	 * Submits the given data to the database
	 * 
	 * @param array $data The data entered by the user
	 * @return boolean whether the data was written or not
	 */
	public function submitcat($data) {
		if (empty($data['name']) || ($data['parentid'] != 0 && MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $data['parentid'], true) === false)) {
			trigger_error_text('Es wurde kein Name angegeben.');
			return false;
		}
		if ($data['catid'] == 0) {
			$lastPosition = (MysqlReadField('SELECT count(*) AS `count` FROM `'.TblPrefix().'flip_gallery_categories`;', 'count') > 0) ? MysqlReadField('SELECT `position` FROM `'.TblPrefix().'flip_gallery_categories` ORDER BY `position` DESC LIMIT 1;', 'position') : 1;
			if ($lastPosition == 0) die('Kritischer Fehler');
			MysqlWriteByID(TblPrefix().'flip_gallery_categories', array('name' => $data['name'], 'description' => $data['desc'], 'position' => $lastPosition + 1, 'parent_id' => $data['parentid']), $data['catid']);
			LogChange('In der <b>Galerie</b> wurde die neue Kategorie <b><a href="gallery.php?parentid='.$data['parentid'].'>'.$data['name'].'</a></b> angelegt');
		} else {
			MysqlWriteByID(TblPrefix().'flip_gallery_categories', array('name' => $data['name'], 'description' => $data['desc'], 'parent_id' => $data['parentid']), $data['catid']);
			LogChange('In der <b>Galerie</b> wurde die Kategorie <b><a href="gallery.php?parentid='.$data['parentid'].'>'.$data['name'].'</a></b> bearbeitet.');
		}
		$this->NextPage = 'gallery.php?frame=admin&pid='.$data['parentid'];
		return true;
	}
	
	/**
	 * Changes the positions of two given catefories
	 * 
	 * @param array $get
	 */
	public function actionmovecat($get) {
		$album = MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $get['id']);
		$id2 = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_gallery_categories` WHERE `parent_id`=\''.$album['parent_id'].'\' AND `position` < \''.$album['position'].'\' ORDER BY `position` DESC LIMIT 1;', 'id');
		$pos2 = MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'position', $id2);
		MysqlExecQuery('UPDATE '.TblPrefix().'flip_gallery_categories SET `position`=IF(`position` = \''.$album['position'].'\',\''.$pos2.'\',\''.$album['position'].'\') WHERE `position`=\''.$album['position'].'\' OR `position`=\''.$pos2.'\';');
	}

	/**
	 * Action for deleting a categorie
	 * 
	 * @param $get
	 */
	public function actiondelCat($get) {
		//Berechtigung ?
		global $User;
		$User->requireRight($this->right_admin);
		$catid = escape_sqlData($get['id']);
		$catname = MysqlReadFieldByID(TblPrefix().'flip_gallery_categorie', 'name', $get['id']);
		if (MysqlReadField('SELECT COUNT(id) FROM `'.TblPrefix().'flip_gallery_albums` WHERE `catid` = \''.$catid.'\';') > 0 || MysqlReadField('SELECT COUNT(id) FROM `'.TblPrefix().'flip_gallery_categories` WHERE `parent_id` = \''.$catid.'\';') > 0) {
			trigger_error('Die Kategorie '.$catname.' enthält Unterkategorien oder Alben. Bitte diese zuerst löschen.');
		}
		MysqlDeleteByid(TblPrefix().'flip_gallery_categories', $catid);
		//$r['id'] = $catid;
		//$r['page'] = '2';
		LogChange('In der <b>Galerie</b> wurde die Kategorie <b>'.$catname.'</b> gelöscht.');
		return $r;
	}
	
	/********** album - administration ***************/
	/**
	 * Frame for adding a new album
	 * 
	 * @param $get
	 */
	public function frameAddAlbum($get) {
		global $User;
		$User->requireRight($this->right_admin);
		if (!is_numeric($get['catid'])) trigger_error('Die angegebene catid ist nicht g&uuml;ltig.');
		$r['catid'] = $get['catid'];
		$r['catname'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'name', $r['catid']);
		$this->Caption = 'Alben der Kategorie "' . $r['catname'].'"';
		return $r;
	}
	
	/**
	 * Action when submitting a new album
	 * 
	 * @param $data
	 */
	public function submitalbum($data) {
		if (empty($data['albumname'])) $this->frameAddAlbum(array('catid' => $data['catid']));
		$lastPosition = (MysqlReadField('SELECT count(*) AS `count` FROM `'.TblPrefix().'flip_gallery_albums`;', 'count') > 0) ? MysqlReadField('SELECT `position` FROM `'.TblPrefix().'flip_gallery_albums` ORDER BY `position` DESC LIMIT 1;', 'position') : 1;
		$id = MysqlWriteByID(TblPrefix().'flip_gallery_albums', array('name' => $data['albumname'], 'catid' => escape_sqlData_without_quotes($data['catid']), 'titlepicid' => 0, 'position' => $lastPosition + 1));
		if (!$id) {
			trigger_error('Das Album konnte nicht in die Datenbank geschrieben werden');
			return false;
		} else {
			$this->addFoldersForAlbum($id);
			$this->NextPage = 'gallery.php?frame=ManageAlbums&id='.$data['catid'];
			return true;
		}
	}
	
	/**
	 * Frame for modifiying an album
	 * 
	 * @param $get Get parameters including the album id
	 */
	public function frameModAlbum($get) {
		//When the image is loaded, it might be cached, so avoid this for this frame
		$this->headerRevalidate();
		$r = $this->setTplVars();
		$albumid = $get['albumid'];
		if (!is_numeric($albumid) || $albumid < 0) trigger_error_text("The given albumid is no valid numeric value", E_USER_ERROR);
		$r += MysqlReadRowByID(TblPrefix().'flip_gallery_albums', $albumid);
		$r['picpath'] = ($r['titlepicid'] != 0) ? $this->getpicnamebyid($r['titlepicid']) : null;
		$this->Caption = sprintf("Album \"%s\" bearbeiten", $r['name']);
		if (!$r) trigger_error_text("The given album was not found in database.", E_USER_ERROR);
		//Read out the categories available for moving the album
		$cats = $this->showcats(0, false, -1, false, true);
		foreach ($cats as $cat) {
			$r['cats'][$cat['id']] = $cat['path'];
		}
		//Read out the pictures available for being titlepicture
		$r['pics'] = MysqlReadCol('SELECT `id`, `filename` FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = '.$albumid.' ORDER BY `position`;', 'filename', 'id');
		return $r;
	}
	
	/**
	 * Writes the new Data to the album
	 * 
	 * @param array $data The data entered by the user
	 */
	public function submitModAlbum($data) {
		//Name has to be set.
		if (empty($data['albumname'])) return false;
		//The hidden id must be set and the record must be existing
		if (empty($data['id']) || !MysqlReadRowByID(TblPrefix().'flip_gallery_albums', $data['id'], true)) return false;
		//The given categorie must be existing
		if (!is_numeric($data['newcat']) || !MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $data['newcat'], true)) return false;
		//The given titlepicit must be existing or 0
		if (!is_numeric($data['titlepic']) || (!MysqlReadRowByID(TblPrefix().'flip_gallery_pics', $data['titlepic'], true)) && $data['titlepic'] != 0) return false;
		//Okay, everthing allright, write data to database
		$this->NextPage = "gallery.php?frame=ManageAlbums&id=".$data['newcat'];
		$id = MysqlWriteByID(TblPrefix().'flip_gallery_albums', array('name' => $data['albumname'], 'catid' => $data['newcat'], 'titlepicid' => $data['titlepic']), $data['id']);
		return ($id == $data['id']);
	}
	
	/**
	 * Frame for managing albums of an specific categorie
	 * 
	 * @param $get
	 */
	public function frameManageAlbums($get) {
		global $User;
		$User->requireRight($this->right_admin);
		$r['catid'] = $get['id'];
		$r['pid'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'parent_id', $r['catid']);
		$this->Caption = 'Alben der Kategorie "'. MysqlReadFieldByID(TblPrefix().'flip_gallery_categories', 'name', $r['catid']).'"';
		$r['alben'] = MysqlReadArea('SELECT `a`.`id`, `a`.`name`, `p`.`name` AS `titlepic` FROM `'.TblPrefix().'flip_gallery_albums` AS `a` LEFT JOIN `'.TblPrefix().'flip_gallery_pics` AS `p` ON `a`.`titlepicid`=`p`.`id` WHERE `a`.`catid`=\''.$r['catid'].'\' ORDER BY `a`.`position`;');
		$r['firstid'] = $r['alben'][0]['id'];
		return $r;
	}
	
	
	/**
	 * Action for deleting some albums, used by the album table
	 * 
	 * @param $data
	 */
	public function actionremalbums($data) {
		foreach ($data['ids'] as $id) {
			//The albums are to be deleted
			//1.) Remove the pictures
			$pictures = MysqlReadCol('SELECT `id` FROM `'.TblPrefix().'flip_gallery_pics` WHERE album_id=\''.$id.'\';', 'id');
			$this->actionRemPics(array('ids' => $pictures, 'albumid' => $id));
			//2.) Ordner löschen
			$this->removeAlbumFolders($id);
			//3.) Album aus Datenbank löschen
			MysqlDeleteByID(TblPrefix().'flip_gallery_albums', $id);
		}
	}
	
	/**
	 * Moves up an album by changing the position with the one of the album above
	 * 
	 * @param $data
	 */
	public function actionmoveupalbum($data) {
		$albumid = escape_sqlData($data['id']);
		$album1 = MysqlReadRowByID(TblPrefix().'flip_gallery_albums', $albumid);
		$offset = MysqlReadField('SELECT COUNT(id) as offset FROM '.TblPrefix().'flip_gallery_albums WHERE `position` < \''.$album1['position'].'\' AND `catid`= '.$album1['catid'].' ORDER BY `position`;') - 1;
		$album2 = MysqlReadRow('SELECT `id`, `position` FROM '.TblPrefix().'flip_gallery_albums WHERE `catid`= '.$album1['catid'].' ORDER BY `position` LIMIT '.$offset.',1;');
		MysqlWriteByID(TblPrefix().'flip_gallery_albums', array('position' => null), $album2['id']);
		MysqlWriteByID(TblPrefix().'flip_gallery_albums', array('position' => $album2['position']), $album1['id']);
		MysqlWriteByID(TblPrefix().'flip_gallery_albums', array('position' => $album1['position']), $album2['id']);
	}
	
	/********** picture administation **************/

	/**
	 * Frame for deleting and modifying pictures
	 * 
	 * @param $get
	 */
	public function frameRemPics($get) {
		global $User;
		$this->headerRevalidate();
		$User->requireRight($this->right_admin);
		$this->Caption = "Bilder verwalten";
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		$r['albumid'] = $get['albumid'];
		$r['catid'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_albums', 'catid', escape_sqlData($get['albumid']));
		$r['bilder'] = MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.escape_sqlData($get['albumid']).'\' ORDER BY `position`');
		$r['firstid'] = $r['bilder'][0]['id'];
		return !empty($r) ? $r : array();
	}
	
	/**
	 * Action used by the RemPics frame
	 * 
	 * @param $get
	 */
	public function actionRemPics($get) {
		$ids = $get['ids'];
		$albumid = escape_sqlData_without_quotes($get['albumid']);
		foreach ($ids as $id) {
			$datei = $this->getpicnamebyid($id);
			if (file_exists($this->picpath.'/'.$albumid.'/'.$datei) && is_writeable($this->picpath.'/'.$albumid.'/'.$datei)) {
				unlink($this->picpath.'/'.$albumid.'/'.$datei);
			} else {
				trigger_error_text('Das Bild '.$datei.' in '.$this->picpath.'/'.$albumid.'/ konnte nicht gelöscht werden.', E_USER_NOTICE);
			}
			if (file_exists($this->thumbpath.'/'.$albumid.'/'.$datei) && is_writeable($this->thumbpath.'/'.$albumid.'/'.$datei)) {
				unlink($this->thumbpath.'/'.$albumid.'/'.$datei);
			} else {
				trigger_error_text('Das Bild '.$datei.' in '.$this->thumbpath.'/'.$albumid.'/ konnte nicht gelöscht werden.', E_USER_NOTICE);
			}
			$this->rempicfromdb($id);
		}
	}
	
	/**
	 * Moves up a picture by switching the position with the previous picture
	 * 
	 * There must not be any picture with position 0 if you expect this to work!
	 * 
	 * @param $get
	 */
	public function actionmoveup($get) {
		$picid = escape_sqlData($get['id']);
		$pic1 = MysqlReadRow('SELECT `id`, `album_id`, `position` FROM '.TblPrefix().'flip_gallery_pics WHERE `id`=\''.$picid.'\' LIMIT 1;');
		$offset = MysqlReadField('SELECT COUNT(`id`) as offset FROM '.TblPrefix().'flip_gallery_pics WHERE `position` < ('.$pic1['position'].') AND `album_id`= '.$pic1['album_id'].' ORDER BY `position`') - 1;
		$pic2 = MysqlReadRow('SELECT `id`, `position` FROM '.TblPrefix().'flip_gallery_pics WHERE `album_id`= '.$pic1['album_id'].' ORDER BY `position` LIMIT '.$offset.',1;');
		//Switch positions
		MysqlWriteByID(TblPrefix().'flip_gallery_pics', array('position' => 0), $pic2['id']);
		MysqlWriteByID(TblPrefix().'flip_gallery_pics', array('position' => $pic2['position']), $pic1['id']);
		MysqlWriteByID(TblPrefix().'flip_gallery_pics', array('position' => $pic1['position']), $pic2['id']);
	}
	
	/**
	 * Frame for adding pictures, part one. User will be advised where to place the pictures
	 * 
	 * @param $get
	 */
	public function frameAddPics($get) {
		//Step one of adding Pictures
		//Just save the albumid and proceed
		$this->Caption = "Galerie - Bilder hinzufügen";
		global $User;
		$User->requireRight($this->right_admin);
		if (!is_numeric($get['albumid']) ||! $get['albumid'] > 0)
			trigger_error('Es wurde keine Album-ID übergeben!');
		$r['albumid'] = $get['albumid'];
		$r['catid'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_albums', 'catid', $r ['albumid']);
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		return $r;
	}
	
	/**
	 * Second frame of image adding process. The Files are read out and shown in a table. User can select, which images to add.
	 * 
	 * @param $get
	 */
	public function frameAddPicsStep2($get) {
		//Step two of adding pictures
		//user now (hopefully) copied the pictures
		$this->Caption = "Galerie - Bilder hinzufügen";
		if (!is_numeric($get['albumid']) ||! $get['albumid'] > 0) {
			trigger_error_text('Es wurde keine Album-ID übergeben!', E_USER_ERROR);
		}
		$albumid = $get['albumid'];
		$fileendings = explode(',', ConfigGet($this->endings));
		if ($handle = opendir($this->picpath.'/'.$albumid.'/')) {
			while (false !== ($file = readdir($handle))) {
            	if ($file != '.' && $file != '..' && !$this->checkpicbyname($file, $albumid) && in_array(strtolower(substr($file, -3)), $fileendings)) {
            		$datei['id'] = $file;
                	$datei['thumb'] = is_file($this->thumbpath.'/'.$albumid.'/'.$file);
                    $files[]=$datei;
				}
			}
			$r['files'] = (empty($files)) ? null : $files;
			$r['albumid'] = $albumid;
		}
		$r['albumid'] = $albumid;
		$r['catid'] = MysqlReadFieldByID(TblPrefix().'flip_gallery_albums', 'catid', $r ['albumid']);
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		return $r;
	}
	
	/**
	 * Action when table was filled out. The Pictures will be added to database and a thumbnail will be created if neccesary
	 * @param $data
	 */
	public function actionAddPics($data) {
		//Tricky: imagenames in $data['ids']
		$albumid = $data['albumid'];
		if (!is_numeric($albumid) ||! $albumid > 0) {
			trigger_error_text('Es wurde keine Album-ID &uuml;bergeben!', E_USER_ERROR);
		}
		$position = $this->getLastPosition() + 1;
		foreach ($data['ids'] as $picname) {
			//Create the thumbnail, if neccessary
			$bild = new ResImage(new ImageLoaderFile($this->picpath.'/'.$albumid.'/'.$picname));
			if (!file_exists($this->thumbpath.'/'.$albumid.'/'.$picname)) {
				$thumb = $bild->createThumbnail(ConfigGet($this->thumbmaxwidth), ConfigGet($this->thumbmaxheight));
				$thumb->saveToFile($this->thumbpath.'/'.$albumid.'/'.$picname);
				unset($thumb);
			}
			//Check if the picture itself has the correct dimensions, otherwise shrink it.
			if ($bild->getHeight() > ConfigGet($this->picmaxheight) || $bild->getWidth() > ConfigGet($this->picmaxwidth)) {
				$thumb = $bild->createThumbnail(ConfigGet($this->picmaxwidth), ConfigGet($this->picmaxheight));
				$thumb->saveToFile($this->picpath.'/'.$albumid.'/'.$picname);
				unset($thumb);
			}
			unset($bild);
			MysqlWrite('INSERT INTO `'.TblPrefix().'flip_gallery_pics` (`filename`, `name`, `album_id`, `position`) VALUES (\''.$picname.'\', \''.$picname.'\', \''.$albumid.'\', \''.$position.'\');');
			$position++;
		}
		$this->NextPage = "gallery.php?frame=RemPics&albumid=".$albumid;
		return true;
	}

	/**
	 * Frame for editing a picture
	 * 
	 * @param array $get
	 * @return array data of the picture to edit
	 */
	public function frameEditPic($get) {
		$r['picpath'] = $this->picpath;
		$r['thumbpath'] = $this->thumbpath;
		global $User;
		$User->requireRight($this->right_admin);
		$id = escape_sqlData($get['picid']);
		$pic = MysqlReadRowByid(TblPrefix().'flip_gallery_pics', $id);
		$this->Caption = sprintf('Bild %s bearbeiten', $pic['name']);
		$r['pic'] = $pic;
		return !empty($r) ? $r : array();
	}
	
	/**
	 * Frame for submitting the changes made at a picture
	 * 
	 * @param array $data The data entered by the user
	 */
	public function submitpic($data) {
		$name = $data['picname'];
		$picid = escape_sqlData($data['picid']);
		$r = MysqlWriteByID(TblPrefix().'flip_gallery_pics', array('name' => $name), $picid);
		return $r;
	}
	
	 /****************************************************\
	 * 			       general - actions                  *
	 \****************************************************/
	
	/**
	 * Action for rotating an image
	 * 
	 * @param array(int rotate, int picid) $data Required Data for action
	 */
	public function actionRotate($data) {
		//When rotating, the frame should always be completly reloaded
		$this->headerRevalidate(); 
		return $this->rotatePicture($data['picid'], $data['rotate']);
	}
	
	 /****************************************************\
	 * 			       general - functions                *
	 \****************************************************/

	/**
	 * Checks, whether a picture with $name is already existing in album $albumid
	 * 
	 * @param $name The name of the image to check
	 * @param $catid The categorie to look in
	 */
	private function checkpicbyname($name, $albumid) {
		$pic = MysqlReadField('SELECT COUNT(id) FROM `'.TblPrefix().'flip_gallery_pics` WHERE `filename` = \''.$name.'\' AND `album_id` = \''.$albumid.'\';');
		return $pic > 0;
	}

	/**
	 * Reads out categories ordered by their position recursive
	 * 
	 * 
	 * @param int $parentid The parentid we start from building the categorie tree
	 * @param boolean $nonRecursive Defines, whether the categories should be read out recursive (=false) or only this level (=true)
	 * @param int $limit Limits the levels 
	 * @param boolean $onlyWithoutAlb Defines, whether only categories without albums should be shown
	 * @param boolean $onlyWithoutSubcats Defines, whether only categories without sub-categories should be shown
	 * 
	 */
private function showcats($parentid = 0, $nonRecursive = false, $limit = -1, $onlyWithoutAlb = false, $onlyWithoutSubcats = false) {
		//Get the root-cats
		$rootcats = MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_gallery_categories` WHERE `parent_id` = \''.$parentid.'\' ORDER BY `position`;');
		if (empty($rootcats)) return false;
		$lastid = 0;
		$result = array();
		foreach ($rootcats as $rootcat) {
			if ($lastid == 0) $rootcat['first'] = true;
			$rootcat['albums'] = MysqlReadField('SELECT COUNT(`id`) FROM `'.TblPrefix().'flip_gallery_albums` WHERE `catid`=\''.$rootcat['id'].'\';');
			if ($rootcat['albums'] == 1 && ConfigGet($this->directpics) == 'Y')
				$rootcat['albid'] = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_gallery_albums` WHERE `catid`=\''.$rootcat['id'].'\' LIMIT 1;', 'id');
			$rootcat['subcats'] = MysqlReadField('SELECT COUNT(`id`) FROM `'.TblPrefix().'flip_gallery_categories` WHERE `parent_id`=\''.$rootcat['id'].'\';');
			if (!($onlyWithoutAlb && $rootcat['albums'] > 0) && !($onlyWithoutSubcats && ($rootcat['subcats'] > 0))) {
				$lastid = $rootcat['id'];
				$rootcat['path'] = implode(' => ', array_reverse($this->getPathToRoot($rootcat['id'])));
				$result[] = $rootcat;
			}
			if (!$nonRecursive && $limit != 0 && ($subcats = $this->showcats($rootcat['id'], $nonRecursive, $limit - 1, $onlyWithoutAlb, $onlyWithoutSubcats)) != false) {
				$result = array_merge($result, $subcats);
			}
		}
		return $result;
	}
	
	/**
	 * Creates folders for a album, if not already existing
	 * 
	 * @param $catid The catid to create folders for
	 */
	private function addFoldersForAlbum($id) {
		If (is_writeable($this->thumbpath.'/')) {
			if (!file_exists($this->thumbpath.'/'.$id.'/'))
				mkdir($this->thumbpath.'/'.$id.'/', 0777);
		} else {
			trigger_error_text('Das Verzeichnis '.$this->thumbpath.'/ ist nicht beschreibbar. Bitte ändere die Schreibrechte (chmod) auf 777.', E_USER_ERROR);
		}
		If (is_writeable($this->picpath.'/')) {
			if (!file_exists($this->picpath.'/'.$id.'/'))
				mkdir($this->picpath.'/'.$id.'/', 0777);
		} else {
			trigger_error_text('Das Verzeichnis '.$this->picpath.'/ ist nicht beschreibbar. Bitte ändere die Schreibrechte (chmod) auf 777.', E_USER_ERROR);
		}
		
	}
    
	/**
	 * Removes all folders correspondending to $catid
	 * 
	 * @param $catid The catid to delete folders for
	 */
    private function removeAlbumFolders ($id) {
    	if (file_exists($this->picpath.'/'.$id.'/')) rmdirr($this->picpath.'/'.$id.'/');
    	if (file_exists($this->thumbpath.'/'.$id.'/')) rmdirr($this->thumbpath.'/'.$id.'/');
    }

    /**
     * Deletes a picture with the given id from database
     * 
     * @param $id The id of the picture to be deleted
     */
    private function rempicfromdb($id) {
    	MysqlDeleteByid(TblPrefix().'flip_gallery_pics', $id);
    }
    
    /**
     * Reads out the amount of images in a categorie
     * 
     * @param int $catid The categorie id to count the pictures for
     */
	private function getpiccount($albumid) {
		return MysqlReadField('SELECT COUNT(id) FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.$albumid.'\';');
	}
    
    /**
     * Returns the filename of a specific picture
     * 
     * @param int $picid The id of the picture to get the filename for
     */
    private function getpicnamebyid($picid) {
    	return MysqlReadFieldByID(TblPrefix().'flip_gallery_pics', 'filename', $picid);
    }

	/**
	 * Reads out the previous and next picture in album $albumid of the given picture
	 * 
	 * @param int $picid
	 * @param int $albumid
	 * @param array currentReturnArray
	 */
    private function getnextandprevpics($picid, $albumid, $currentReturnArray = array()) {
    	$currentPos = MysqlReadFieldByID(TblPrefix().'flip_gallery_pics', 'position', $picid);
		$offset = MysqlReadField('SELECT COUNT(`id`) as offset FROM `'.TblPrefix().'flip_gallery_pics` WHERE `position` < ('.$currentPos.') AND `album_id` = \''.$albumid.'\' ORDER BY `position`;') - 1;
		$count = MysqlReadField('SELECT COUNT(`id`) FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.$albumid.'\';');
		If ($offset < 0) {
			$picids = MysqlReadCol('SELECT `id` FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.$albumid.'\' ORDER BY `position` LIMIT 0, 2;', 'id');
			$r['next'] = $picids[1];
			$r['begin'] = true;
		} elseif ($offset == ($count - 2)) {
			$picids = MysqlReadCol('SELECT `id` FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.$albumid.'\' ORDER BY `position` LIMIT '.$offset.', 2;', 'id');
			$r['prev'] = $picids[0];
			$r['end'] = true;
		} else {
			$picids = MysqlReadCol('SELECT `id` FROM `'.TblPrefix().'flip_gallery_pics` WHERE `album_id` = \''.$albumid.'\' ORDER BY `position` LIMIT '.$offset.', 3;', 'id');
			$r['next'] = $picids[2];
			$r['prev'] = $picids[0];
		}
		return array_merge($r, $currentReturnArray);
	}
	
	/**
	 * Returns all parents including the root cat-id.
	 * 
	 * @param int $catid The categorie id to get the path from
	 */
	private function getPathToRoot($catid) {
		$root = false;
		$current = $catid;
		
		while(!$root) {
			$data = MysqlReadRowByID(TblPrefix().'flip_gallery_categories', $current);
			$r[] = $data['name'];
			$current = $data['parent_id'];
			$root = $current == 0;
		}
		return $r;
	}
	
	/**
	 * Reads out the highest currently used position in the flip_gallery_pics table
	 * 
	 * @return int The highest currently used posision
	 */
	private function getLastPosition() {
		return (MysqlReadField('SELECT count(*) AS `anzahl` FROM `'.TblPrefix().'flip_gallery_pics`;', 'anzahl') > 0)
		? MysqlReadField('SELECT `position` FROM `'.TblPrefix().'flip_gallery_pics` ORDER BY `position` DESC LIMIT 1;', 'position')
		: 1;
	}
	
	/**
	 * Sets the most important template variables and returns an array containing these variables. Setted variables are:
	 * 
	 * picsperrow, picmaxheight, picmaxwidth, thumbmaxheight, thumbmaxwidth, hits, maxrows, cpicpath, cthumbpath
	 * 
	 * @return an array containing the above mentioned vars as keys and their config value as value
	 * 
	 */
	private function setTplVars() {
		$vars = array('picsperrow', 'hits', 'picmaxheight', 'picmaxwidth', 'thumbmaxheight', 'thumbmaxwidth', 'maxrows', 'cpicpath', 'cthumbpath');
		foreach ($vars as $var) {
			$r[$var] = ConfigGet($this->$var);
		}
		return $r;
	}
	
	/**
	 * Rotates the picture with the given id around the given angle
	 * 
	 * @param int $picid The id of the picture to rotate
	 * @param int $angle Amount of degrees to rotate the picture
	 */
	private function rotatePicture($picid, $angle) {
		//First, lets check, whether the picture is existing
		$pic = MysqlReadRowByID(TblPrefix().'flip_gallery_pics', $picid);
		if (!$pic) return false;
		//Rotate the Thumbnail
		$thumbpath = $this->thumbpath.'/'.$pic['album_id'].'/'.$pic['filename'];
		if (file_exists($thumbpath)) {
			$thumb = new ResImage(new ImageLoaderFile($thumbpath));
			$thumb->Res = rotateImage($thumb->Res, $angle % 360);
			$thumb->saveToFile($thumbpath);
		}
		//Rotate the picture
		$picpath = $this->picpath.'/'.$pic['album_id'].'/'.$pic['filename'];
		if (file_exists($picpath)) {
			$newpic = new ResImage(new ImageLoaderFile($picpath));
			$newpic->Res = rotateImage($newpic->Res, $angle % 360);
			$newpic->saveToFile($picpath);
		}
		return true;
	}
	
	/**
	 * function which sets some header information not to cache the page
	 */
	private function headerRevalidate() {
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") ." GMT");
		header("Cache-Control: no-store, no-cache, max-age=0, must-revalidate");
	}
}

RunPage('GalleryPage');
?>