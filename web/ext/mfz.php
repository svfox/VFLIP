<?php
/*
  Mini XML Parser
  (c) 2002 by LANparty.de
  Dieses Script unterliegt der GPL (General Public License)
  Eine Modifikation und Weitergabe ist erlaubt, solange die 
  &Auml;nderungen dokumentiert und weitergegeben werden

  Aufbau der Datendatei

  <?xml version="1.0" ?> 
  <mfz>
   <item>
    <id>x</id> 
    <kategorie>Kategorie</kategorie> 
    <start>Startort</start> 
    <nick>Fahrer / Mitfahrer</nick> 
    <date>Startzeit</date> 
   </item>
  </mfz>

  Hier bitte unbedingt die PartyID eurer Veranstaltung einsetzen
  Die erfahrt ihr wenn ihr in der Best&auml;tigungsmail nachschaut.
  Ein Submit ist derzeit leider nicht m&ouml;glich

  Das Script l&auml;sst sich einfach durch einen include Befehl 
  in euer vorhandenes PHP Script einsetzen, nur solltet ihr die dann HTML Tags anpassen
  Der Link zum Eintragen einer neuen Route lautet:
  http://www.lanparty.de/events/?action=mfzsetup&id= PartyID
*/

//$backend = "http://www.lanparty.de/events/mfz/?id=" . $partyid;
//$fpread = fopen($backend, "r");
$server = "www.lanparty.de";
$getstring = "/events/mfz/?id=" . $partyid;

$fpread = fsockopen ("$server", 80, $errno, $errstr, 30);

if (!$fpread) {	echo "$errstr ($errno)<br>\n";	exit;
} else {
	fputs ($fpread, "GET $getstring HTTP/1.0\r\nHost: $server\r\n\r\n");
	$counter = 0;
	while (!feof($fpread) ) {
		$buffer = ltrim(chop(fgets($fpread, 256)));
		if ($buffer == "<item>") {
			$data[$counter]["id"] = ltrim(chop(fgets($fpread, 256)));
			$data[$counter]["kategorie"] = ltrim(chop(fgets($fpread, 256)));
			$data[$counter]["start"] = ltrim(chop(fgets($fpread, 256)));
			$data[$counter]["nick"] = ltrim(Chop(fgets($fpread, 256)));
			$data[$counter]["date"] = ltrim(Chop(fgets($fpread, 256)));
			# Entfernen der Tags
			$data[$counter]["id"] = ereg_replace("(<)+((/)?id)(>)+", "", $data[$counter]["id"]);
//			$data[$counter]["kategorie"] = ereg_replace("(<)+((/)?kategorie)(>)+", "", $data[$counter]["kategorie"]);
			$data[$counter]["start"] = ereg_replace("(<)+((/)?start)(>)+", "", $data[$counter]["start"]);
			$data[$counter]["nick"] = ereg_replace("(<)+((/)?nick)(>)+", "", $data[$counter]["nick"]);
			$data[$counter]["date"] = ereg_replace("(<)+((/)?date)(>)+", "", $data[$counter]["date"]);
			$data[$counter]["link"] = "http://www.lanparty.de/events/?action=mfzshow&amp;mfzid=" . $data[$counter]["id"];
			# Konvertierung des Datums vom Iso Format ins Deutsche
			if (ereg("^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$", $data[$counter]["date"], $regs)) {
				$data[$counter]["date"] = $regs[3].".".$regs[2].".".$regs[1]." ".$regs[4].":".$regs[5].":".$regs[6];
			}
			$counter++;
		}	}
}
fclose($fpread);?>
