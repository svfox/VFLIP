<?php
  /*
  Dieses Script setzt die Anzahl der angemeldeten und bezahlten G&auml;ste 
  in den Partydetails von LANparty.de automatisch wenn diese Seite aufgerufen wird.
  
  basiert auf den zwei Klassen nusoap und rc4 Crypt.
  Bitte ladet euch vorher diese zwei Klassen separat herunter.
  NuSoap erhaltet ihr unter: http://dietrich.ganx4.com/nusoap/
  rc4Crypt unter http://sourceforge.net/projects/rc4crypt/
  
  Die Passphrase k&ouml;nnt ihr jederzeit unter 
  http://www.lanparty.de/customize/?action=eventedit&id=Partyid
  editieren, wobei ihr Partyid durch die ID ersetzen m&uuml;sst, die ihr 
  in der Mail beim Submit erhalten habt.
  
  Dieses Script ist dazu gedacht, in einer Best&auml;tigungsseite f&uuml;r 
  die Anmeldung oder in eure Adminstrationsseiten mit eingebaut zu werden.
  Alleine wird das Teil nicht viel n&uuml;tzen.
  */
  	
  $pathtonusoapclass = "./ext/nusoap061.php";			# Pfad zur nusoap Klasse
  $pathtorc4class = "./ext/class.rc4crypt.php";			# Pfad zur rc4crypt Klasse
  $partyid = 17738;					# Party ID bei LANparty.de
  $passphrase = 'ICWienerToo';				# Passphrase bei LANparty.de
  $email = "a2d.yogi@nwsnet.de";				# Email Adresse zum dazugeh&ouml;rigen Account
  
  # Ab hier wirds interessant. Wenn ihr die Verarbeitung automatisieren wollt, 
  # dann m&uuml;sst ihr die Variablen $guest und $wait selber setzen und die folgenden Zeilen l&ouml;schen
  
  //$guest = 13;						# Das hier ist die Variable f&uuml;r die Bezahlten G&auml;ste
  //$wait = 13;						# Das hier ist die Variable f&uuml;r die nicht Bezahlten G&auml;ste
  //^siehe updatelpde() in lanparty.php
  
  # Die Basisklassen werden initialisiert
  require_once($pathtonusoapclass);
  require_once($pathtorc4class);
  $soapclient = new soapclient("http://soap.lanparty.de/events/index.php");
  $rc4 = new rc4crypt;
  
  # Hier kommt der eigentliche Teil und Soapcall
  # Die Partyid, Email Adresse werden hintereinandergeh&auml;ngt und mit einem Trennzeichen versehen
  # anschliessend wird das ganze verSchl&uuml;sselt
  $data = $partyid.";".$guest.";".$wait.";".$email;
  $data = $rc4->endecrypt($passphrase, $data, ""); 
  
  # Es wird ein assoziatives Array gebildet, welches die Parameterliste enth&auml;lt.
  # Die Werte sind die Partyid und die verSchl&uuml;sselten Daten
  $array["data"] = $data; 
  $array["partyid"] = $partyid;
  
  # Der eigentliche Soap Aufruf
  $res = $soapclient->call("status",$array);
  
  # Das Ergebnis ist entweder true oder false
  # Hier k&ouml;nnt ihr euren Code zur Fehlerbehandlung einsetzen
  if($res){
  	return true;
  } else {
  	return false;
  }
?>