<?php

/**
 * Die Datei core.cache.php stellt einige grundlegende Funktionen zum Cachen von Daten zur Verf&uuml;gung.
 * Dabei wird &uuml;ber einen Timestamp daf&uuml;r gesorgt, dass der Cache niemals veraltete Daten zur&uuml;ck liefert.
 * 
 * Anwendungsbeispiel:
 * <code>
 * function getComplexData($ID)
 * {
 *   if($r = CacheGet("complex_data")) return $r;
 * 
 *   $ID = addslashes($ID);
 *   $data = MysqlReadArea("SELECT * FROM `complex_table1`, `complex_table2` WHERE ('id' == $ID);");
 *   
 *   return CacheSet("complex_data",$data,array("complex_table1","complex_table2"));
 * }
 * </code>
 * 
 * @author Moritz Eysholdt
 * @version $Id: core.cache.php 1484 2007-10-15 22:18:05Z loom $
 * @copyright (c) 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined('CORE.CACHE.PHP'))
	return 0;
define('CORE.CACHE.PHP', 1);

require_once ('core/core.mysql.php');

/**
 * CacheInit() initialisiert die Cache-Funktionen CacheGet() und CacheSet().
 * Die Funktion wird nur einmalig aus core.php aufgerufe und sollte 
 * ansonsten nicht verwendet werden.
 **/
function CacheInit() {
	global $CacheTableTimes, $CacheDisabled, $CoreConfig;
	$CacheDisabled = (!is_dir($CoreConfig['cache_path']) or empty ($CoreConfig['cache_path'])) ? true : false;
	if (!$CacheDisabled) {
		$CacheTableTimes = MysqlReadCol('SHOW TABLE STATUS;', 'Update_time', 'Name');
		foreach ($CacheTableTimes as $k => $v)
			$CacheTableTimes[$k] = strtotime($v);
	}
}

/**
 * CacheGet() Gibt die durch $ItemName spezifizierten Daten aus dem Cache zur&uuml;ck.
 * 
 * @param string $ItemName Der Bezeichner des Cache-Eintrags.
 * @param integer $LastChange (optional) Ein Timestamp welcher den Zeitpunkt der 
 * letzten Aenderung der Originaldaten angibt. 
 * @return mixed false, wenn der Cache-Eintrag nicht existiert oder der Inhalt des 
 * Caches veraltet ist. Ansonsten wird der Inahlt des Caches zurueck gegeben.
 **/
function CacheGet($ItemName, $LastChange = 0) {
	global $CacheTableTimes, $CoreConfig, $CacheDisabled;
	if ($CacheDisabled)
		return false;
	$file = "$CoreConfig[cache_path]$ItemName.php";
	if (!is_file($file))
		return false;
	$time = -1;
	$tables = array ();
	$data = null;
	include ($file);
	if (($LastChange != 0) and ($LastChange > $time))
		return false;
	foreach ($tables as $t) {
		if (!isset ($CacheTableTimes[$t])) {
			trigger_error_text(text_translate('Eine vom Cache verwendete Tabelle existiert nicht.') . '|TableName: '.$t, E_USER_WARNING);
			return false;
		}
		if ($CacheTableTimes[$t] >= $time)
			return false;
	}
	return $data;
}

/**
 * CacheSet() Speichert einen Cache-Eintrag im Cache.
 * 
 * @param string $ItemName Der Bezeichner des Cache-Eintrags.
 * @param mixed $Data Die zu cachenden Daten
 * @param array $RelatedTables ein array, welches die Namen der Tabellen enth&auml;lt, 
 * von denen die aktualit&auml;t der zu cacheden Daten abh&auml;ngt. Wird eine dieser Tabellen in der
 * Zwischenzeit geupdated, wird der Cache-Eintrag automatisch als veraltet erkannt.
 * @return mixed der Inhalt der Variable Data.
 **/
function CacheSet($ItemName, $Data, $RelatedTables = array ()) {
	global $CoreConfig, $CacheDisabled;
	if ($CacheDisabled)
		return $Data;
	$file = "<?php\n";
	$file .= "/******* DBCache of $ItemName *******/\n";
	$file .= "\$time = " . time() . ";\n";
	$file .= "\$tables = " . var_export($RelatedTables, true) . ";\n";
	$file .= "\$data = " . var_export($Data, true) . ";\n";
	$file .= '?>';
	file_put_contents("$CoreConfig[cache_path]$ItemName.php", $file);
	return $Data;
}
?>