<?php


/**
 * Die Datei core.log.php &uuml;bernimmt die Aufgabe der Fehlerbehandlung und des Loggens.
 * Die Logdateien werden in der Config-Section "log" definiert.
 * 
 * Es gibt vier verschiedene Logdateien und "Nachrichtenarten", wobei jede Nachrichtenart 
 * eine Logdatei besitzt. Zu jedem Eintrag in eine Logdatei werden noch Datum, Uhrzeit und die 
 * SessionID gespeichert. Daher ist es nicht n&ouml;tig in einer Nachricht den Namen des Users 
 * aufzuf&uuml;hren der sie verursacht hat.
 * 
 * Es besteht die M&ouml;glichkeit zu Nachrichten, die mittels der Funktion trigger_error() 
 * ausgegeben werden, Debuginformationen hinzuzuf&uuml;gen um den Fehler sp&auml;ter besser
 * reproduzieren zu k&ouml;nnen. Diese Informationen werden nicht an User ausgegeben, 
 * die nicht berechtigt sind sie einzusehen. Die Debuginformationen werden zu einer
 * Nachricht hinzugef&uuml;gt, indem die Meldung das Zeichen "|" enth&auml;lt. Der Text davor ist die 
 * eigentliche Nachricht, dahinter folgen die Debuginfos, welche auch Zeilenumbr&uuml;che 
 * enthalten d&uuml;rfen.
 * 
 * Es gibt Folgende Nachrichtenarten:
 * -notice: Eine Bemerkung, die den User auf etwas hinweist. Dies k&ouml;nnen auch 
 *   Erfolgsmeldungen ("Daten wurden gespeichert.") sein. 
 * -warning: Eine Fehlermeldung, der Script wird aber weiterhin ausgef&uuml;hrt.
 *   Dies k&ouml;nnen Meldungen sein, die den User auf falsche Formulareingaben hinweisen.
 * -error: Eine Fehlermeldung, der Script wird daraufhin sofort beendet. Das bedeutet, dass 
 *   der Code, der sich hinter der Stelle befindet, an welcher die Error-Meldung ausgegeben wurde,
 *   nicht mehr ausgef&uuml;hrt wird. Dies sind kritische Fehler wie z.B. fehlende Userrechte.
 * -action: Es wird keine Meldung ausgegeben, sondern nur in die Logdatei geschrieben.
 *   Der action-Log dient dazu, Protokoll &uuml;ber die T&auml;tigkeiten des Users zu f&uuml;hren, nicht im Sinne 
 *   des "Big Brothers" sondern um sp&auml;ter nachvollziehen zu k&ouml;nnen, wer wann welche Daten ge&auml;ndert hat.
 * 
 * Beispiele:
 * <code>
 * // ein Hinweis
 * trigger_error("Die Daten wurden erfolgreich gespeichert.",E_USER_NOTICE); 
 * // oder (f&uuml;r alle, die es hassen, mit einer "error"-Funktion eine Erfolgsmeldung auszugeben,
 * //       genauso wie zum "beenden" erst auf "start" clicken zu m&uuml;ssen. thx@bill f&uuml;r den hinweis ;-)) 
 * LogNotice("Die Daten wurden erfolgreich gespeichert."); 
 * 
 * // ein Fehler
 * trigger_error("dsad@sa@xx.x ist keine g&uuml;ltige Emailadresse.",E_USER_WARNING); 
 * 
 * // ein fataler Fehler
 * trigger_error("Du bist nicht berechtigt diese Seite zu betreten.|Seite: adminsection",E_USER_ERROR);
 * 
 * // ein Eintrag in den action-Log
 * LogAction("der Text xxxxx wurde ge&auml;ndert.");
 * </code>
 * 
 * @author Moritz Eysholdt
 * @version $Id: core.log.php 1484 2007-10-15 22:18:05Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined('CORE.LOG.PHP'))
	return 0;
define('CORE.LOG.PHP', 1);

define('E_FLIP_SYSERROR', 33);

/**
 * Die Klasse LogMsg stellt die einfachste Form einer nachricht da, wie sie z.B. von den Funktionen
 * LogAction() und LogNotice() erstellt wird.
 * Das von GetErrors() zur&uuml;ckgegebene Array kann Instanzen ihres Typs enthalten.
 **/
class LogMsg {
	var $Type; // der Typ der Nachricht (notice|warning|error|action)
	var $Message; // Die Nachricht

	function LogMsg($aType, $aMessage) {
		$this->Type = $aType;
		$this->Message = trim($aMessage);
	}

	function getLogMsg() {
		return $this->Message;
	}

	function getMessage() {
		return $this->Message;
	}
}

/**
 * Die Klasse ErrorMsg erg&auml;nzt die Klasse LogMsg um Informationen, die zum Debuggen 
 * wichtig sind.
 * Das von GetErrors() zur&uuml;ckgegebene Array kann Instanzen ihres Typs enthalten.
 **/
class ErrorMsg extends LogMsg {
	var $Error = NULL; // die Fehlerbezeichnung von PHP (E_ERROR, E_USER_ERROR, ...)
	var $Debug = NULL; // die zusammen mit der Meldung ausgegebenen Debuginformationen
	var $File = NULL; // der Name der Datei in welcher der Fehler aufgetreten ist.
	var $Line = NULL; // die Zeile in der der Fehler aufgetreten ist.
	var $BackTrace = NULL; // ein array zur&uuml;ckgegeben von debug_backtrace()
	var $Args = NULL; // Parameter mit denen die Funktion aufgerufen wurde in der der Fehler aufgetreten ist.
	var $LowLevel = false;

	function ErrorMsg($aNo, $aMsg, $aDebug = "", $aFile = "", $aLine = 0, $aArgs = NULL, $aBackTrace = NULL) {
		switch ($aNo) {
			case (E_ERROR) :
				$err = 'E_ERROR';
				$type = 'error';
				break;
			case (E_FLIP_SYSERROR) :
				$err = 'E_FLIP_SYSERROR';
				$type = 'error';
				$this->LowLevel = true;
				break;
			case (E_USER_ERROR) :
				$err = 'E_USER_ERROR';
				$type = 'error';
				break;
			case (E_WARNING) :
				$err = 'E_WARNING';
				$type = 'warning';
				break;
			case (E_USER_WARNING) :
				$err = 'E_USER_WARNING';
				$type = 'warning';
				break;
			case (E_NOTICE) :
				$err = 'E_NOTICE';
				$type = 'notice';
				break;
			case (E_USER_NOTICE) :
				$err = 'E_USER_NOTICE';
				$type = 'notice';
				break;
			default :
				$err = 'E_UNKNOWN';
				$type = 'notice';
				break;
		}
		$this->LogMsg($type, $aMsg);
		if ($type != 'notice') // dubug-information werden f&uuml;r informationsmeldungen nicht ben&ouml;tigt
			{
			$this->Error = $err;
			$this->Debug = trim($aDebug);
			$this->File = $aFile;
			$this->Line = $aLine;
			$this->BackTrace = $aBackTrace;
			$this->Args = $aArgs;
		}
	}

	function getLogMsg() {
		$debug = (empty ($this->Debug)) ? '' : ' ' . addcslashes($this->Debug, "\n\r");
		$file = empty ($this->File) ? '' : "[{$this->File},{$this->Line}]";
		$uri = GetRequestURI();
		$get = ($this->Type == 'error') ? ' GET:' . substr($uri, strrpos($uri, '/') + 1) : '';
		return parent :: getLogMsg() . " @ {$this->Error}$file$debug$get";
	}

	function getMessage($DisplayDebug = false) {
		$r = parent :: getMessage();
		if ($DisplayDebug) {
			$r .= " [{$this->File},{$this->Line}]";
			if (!empty ($this->Debug))
				$r .= (strpos($this->Debug, "\n") === false) ? " ({$this->Debug})" : "\n<pre>{$this->Debug}</pre>";
			if (is_array($this->BackTrace))
				$r .= "<form method=\"post\" action=\"log.php?frame=backtrace\"><input type=\"hidden\" name=\"data\" value=\"" . escapeHtml(condense($this)) . "\" /><input type=\"submit\" class=\"button\" value=\"" . text_translate("BackTrace!") . "\" /></form>";
		}
		return $r;
	}

}

/**
 * die Funktion ErrorCallback() ist die Callbackfuntion f&uuml;r set_error_handler()
 * 
 * @access private
 **/
function ErrorCallback($No, $Msg, $File, $Line, $Args = array ()) {
//	print '<pre>' . $Msg . '</pre>';
//	return false;
	if ($No == E_NOTICE || $No == E_STRICT) {
		return false;
	}

	global $FatalErrorMessageFunc, $LogDoCache, $CoreConfig;
	$File = RelativePath(dirname($_SERVER['SCRIPT_FILENAME']), $File);
	list ($message, $debug) = array_pad(explode('|', $Msg, 2), 2, '');
	
	// Wenn der Fehler durch eine fehlgeschlagene DB-Verbindung ausgeloest
	// wurde, gib ihn nur aus, da weiteres Vorgehen u.U. die Datenbank
	// vorraussetzt.
	global $MysqlConnection;
	
	if(!$MysqlConnection) {
		$msg_wrapped = wordwrap($Msg, 80, '<br />');
		
		print "[$File, $Line]:<br/><pre>$msg_wrapped</pre><hr />";
		return false;
	}
	
	$backtrace = NULL;
	
	if ($CoreConfig['debug_backtrace'] && false) {
		//Bug: unter fcgi f&uuml;hrt jeder Aufruf von debug_backtrace() zu einem "Internal Server Error" mit PHP 4.4.7
		if ((function_exists('debug_backtrace')) and ($No != E_USER_NOTICE) and (version_compare(PHP_VERSION, '5.0', '>=') || php_sapi_name() != 'cgi-fcgi')) { 
			$backtrace = debug_backtrace();
			foreach ($backtrace as $k => $v) {
				$backtrace[$k]['file'] = RelativePath(dirname($_SERVER['SCRIPT_FILENAME']), isset ($v['file']) ? $v['file'] : null);
			}
		}
	}
	
	$m = new ErrorMsg($No, $message, $debug, $File, $Line, $Args, $backtrace);
	
	_DoWriteLog($m);
	
	if ($m->Type == 'error') {
		if ($LogDoCache)
			_DoCacheLog($m);
		if ($m->LowLevel)
			_DefaultFatalErrorMessage($m);
		else
			$FatalErrorMessageFunc ($m);
		exit ();
	} else
		_DoShowLog($m);
}

/**
 * Mit der Funktion old_Try() kann die Ausgabe von Nachrichten bis zum Aufruf von 
 * old_Except() unterbunden werden. old_Try() und old_Except() haben auf das Logging
 * keinerlei Einfluss.
 *
 * WARNUNG! die Funktion old_Try() ist veraltet, da PHP 5 try & catch nativ unterst&uuml;tzt. (hoffe das funzt auch)
 *
 * 
 * @see old_Except()
 **/
function old_Try() {
	global $LogTryCount, $Errors;
	$LogTryCount++;
	$Errors[$LogTryCount] = array ();
}

/**
 * Die Funktion old_Except() beendet die Ausgabesperre von Nachrichten welche mit
 * der Funktion old_Try() begonnen wurde.
 * 
 * WARNUNG! die Funktion old_Except() ist veraltet, da PHP 5 try & catch nativ unterst&uuml;tzt. (hoffe das funzt auch)
 *
 * @param boolean $ShowErrors gibt an, ob die in der Zwischenzeit aufgetretenen 
 *   Nachrichten angezeigt werden sollen.
 * @return array gibt ein Array mit Instanzen der Klassen LogMsg und ErrorMsg zur&uuml;ck, welche 
 *   in der Zwischenzeit aufgetretenen Nachrichten n&auml;her beschreiben. Sind keine Nachrichten 
 *   aufgetreten wird False zur&uuml;ckgegeben.
 * @see old_Try()
 **/
function old_Except($ShowErrors = true) {
	global $LogTryCount, $Errors;
	$r = (count($Errors[$LogTryCount]) > 0) ? $Errors[$LogTryCount] : false;
	$LogTryCount--;
	if ($ShowErrors)
		foreach ($Errors[$LogTryCount +1] as $e)
			_DoShowLog($e);
	if ($LogTryCount < 0)
		trigger_error_text(text_translate('Die Funktion Except() darf nicht h&auml;ufiger aufgerufen werden as die Funktion Try().'), E_USER_ERROR);
	return $r;
}

/**
 * Die Funktion GetErrors() gibt eine Liste der bis jetzt aufgetretenen Nachrichten zur&uuml;ck.
 * 
 * @return array gibt ein Array mit Instanzen der Klassen LogMsg und ErrorMsg zur&uuml;ck, welche 
 *   die aufgetretenen Nachrichten n&auml;her beschreiben. Sind keine Nachrichten aufgetreten wird 
 *   False zur&uuml;ckgegeben.
 **/
function GetErrors() {
	global $Errors;
	return (count($Errors[0]) > 0) ? $Errors[0] : false;
}

/**
 * @access private
 **/
function _WriteError($TypeToDel, $Error, $Debug) {
	global $CoreConfig;
	unset ($CoreConfig["log_$TypeToDel"]); // damit nicht nochmal versucht wird, was in diesen log zu schreiben
	_DoShowLog(new ErrorMsg(E_USER_WARNING, $Error, $Debug));
	return false;
}

/**
 * Die Funktion _DoWriteLog() schreibt in eine Logdatei.
 *   
 * @access private
 **/
function _DoWriteLog($Msg) {
	global $CoreConfig, $REMOTE_ADDR, $Session, $User;

	$log = $CoreConfig["log_{$Msg->Type}"];
	if (!isset ($log))
		$log = 'db';
	elseif (empty ($log)) return false;

	$session_id = (is_object($Session)) ? $Session->id : 0;
	$user_id = (is_object($User)) ? $User->id : 0;
	$msg = $Msg->getLogMsg();

	foreach (explode('|', $log) as $file) {
		if (empty ($file))
			continue;
			
		// In die Datenbank schreiben
		if ($file == 'db') {
			// Ueberspringe, wenn der zu schreibende Fehler 
			// durch eine fehlgeschlagene DB-Verbindung ausgeloest wird
			global $MysqlConnection;
			
			if(!$MysqlConnection) {
				continue;
			}
			
			// workaround, damit die logeintr&auml;ge in der richtigen reihenfolge bleiben, 
			// auch wenn zeitgleich sehr viele geschrieben werden.
			static $time = 0;
			if (empty ($time))
				$time = time();
			else
				$time++;

			MysqlWriteByID(TblPrefix() . 'flip_log_log', array (
				'time' => $time,
				'user_id' => $user_id,
				'session_id' => $session_id,
				'message' => $msg,
				'type' => $Msg->Type
			));
			if (!empty ($CoreConfig['log_dbtimeout'])) {
				MysqlWrite('DELETE FROM `' . TblPrefix() . 'flip_log_log` WHERE (`time` < ' . (time() - ($CoreConfig['log_dbtimeout'] * 60 * 60 * 24)) . ');');
			}
		} else // in eine Datei schreiben.
			{
			if (is_file($file)) {
				if (!($f = @ fopen($file, 'a')))
					return _WriteError($Msg->Type, text_translate('Eine Logdatei konnte nicht geoeffnet werden.'), 'Dateiname: '.$file);
			} else {
				$d = dirname($file);
				if (!ForceDir($d, 0))
					return _WriteError($Msg->Type, text_translate('Das Verzeichnis fuer die Logdateien konnte nicht erstellt werden.'), 'Verzeichnis: '.$d);
				if (!($f = @ fopen($file, 'w')))
					return _WriteError($Msg->Type, text_translate('Eine Logdatei konnte nicht erstellt werden.'), 'Dateiname: '.$file);
				$txt = "<?php return 0; ?> begin {$Msg->Type}-log on " . date('Y-m-d H:i') . ' <?php return 0; ?>'."\n";
				if (@ fputs($f, $txt) < 1)
					return _WriteError($Msg->Type, text_translate('In eine Logdatei konnte nicht geschrieben werden'), 'Dateiname: '.$file);
			}
			// $id = (is_object($Session)) ? $Session->id : 0;
			$txt = str_pad(date('Y-m-d H:i'), 17) . str_pad($session_id, 8, ' ', STR_PAD_LEFT) . ' ' . str_pad($Msg->Type, 10) . " $msg\n";
			if (!empty ($Msg->BackTrace) && $CoreConfig['debug_backtrace_log'])
				$txt .= var_export($Msg->BackTrace, true) . "\n";

			if (@ fputs($f, $txt) < 1)
				return _WriteError($Msg->Type, text_translate('In eine Logdatei konnte nicht geschrieben werden'), 'Dateiname: '.$file);
			fclose($f);
		}
	}
	return true;
}

/**
 * Die Funktion _DoShowLog() f&uuml;gt die Nachricht einer Liste hinzu die mit getErrors() 
 * angefragt werden kann.
 * 
 * @access private
 **/
function _DoShowLog($Msg) {
	global $Errors, $LogTryCount, $LogDoCache;
	// fehler in der console sofort ausgeben
	if (php_sapi_name() == 'cli')
		echo $Msg->getMessage(true) . "\n";
	$Errors[$LogTryCount][] = $Msg;
	if ($LogDoCache)
		_DoCacheLog($Msg);
}

/**
 * Die Funktion _DefaultFatalErrorMessage() ist die Default-Callbackfunktion zur
 * ausgabe fataler Fehler. (Fatale Fehler beenden das Script)
 * 
 * @see setFatalErrorCallback()
 * @access private
 **/
function _DefaultFatalErrorMessage($Msg) {
	global $Errors;

	if (php_sapi_name() == 'cli') {
		echo text_translate('Fataler Fehler') . ': ' . $Msg->getMessage(true) . "\n";
		if (count($Errors[0]) > 0) {
			echo "\n" . text_translate('Weitere Meldungen') . ":\n";
			foreach ($Errors[0] AS $Err)
				echo '- ' . $Msg->getMessage(true) . "\n";
		}
	} else {
		echo "<pre>\n";
		echo text_translate('Aufgrund folgenden Fehlers wird der Script beendet') . ":\n";
		echo $Msg->getMessage(false) . "\n";
		if (count($Errors[0]) > 0) {
			echo "\n" . text_translate('Weitere Meldungen') . ":\n";
			foreach ($Errors[0] AS $Err)
				echo "- " . $Msg->getMessage(false) . "\n";
		}
		echo "\n" . text_translate("Details zu den Fehlern finden sich in den Logfiles.") . "\n";
		echo "</pre>\n";
	}
}

/**
 * Die Funktion SetFatalErrorCallback() setzt eine Benutzerdefinierte Funktion
 * zur Ausgabe von von fatalen Fehlern. (Fatale Fehler beenden das Script)
 * 
 * Die Callbackfunktion bekommt einen Parameter &uuml;bergeben, welcher eine Instanz der
 * Klasse ErrorMsg ist. Diese enth&auml;lt Informationen &uuml;ber den aufgetretenen Fehler.
 * 
 * @param string $func der Name der Callbackfunktion.
 **/
function SetFatalErrorCallback($func) {
	global $FatalErrorMessageFunc;
	$FatalErrorMessageFunc = $func;
}

/**
 * Die Funktion LogRegisterErrorHook() initialisiert das Log-System.
 * Sie wird aus der Datei core.php aufgerufen, weitere Aufrufe sind nicht n&ouml;tig.
 **/
function LogRegisterErrorHook() {
	global $FatalErrorMessageFunc, $Errors, $LogTryCount, $LogTryErrors, $LogDoCache;
	set_error_handler("ErrorCallback");
	$FatalErrorMessageFunc = "_DefaultFatalErrorMessage";
	$Errors = array (
		array ()
	);
	$LogTryCount = 0;
	$LogDoCache = false;
	_LogLoadCookie();
}

function _DoCacheLog($msg) {
	global $MysqlConnection;
	
	if(!$MysqlConnection) {
		return;
	}
	
	return MysqlWriteByID(TblPrefix() . "flip_session_cache", array (
		"session_id" => $Session->id,
		"type" => "log",
		"data" => serialize($msg
	), "time" => time()));
}

function LogCacheActivate() {
	global $LogDoCache, $Errors;
	$LogDoCache = true;
	foreach ($Errors[0] as $e)
		_DoCacheLog($e);
}

function LogCacheFlush() {
	global $LogDoCache, $Errors, $Session;
	$LogDoCache = false;
	$msgs = MysqlReadCol("SELECT `data` FROM `" . TblPrefix() . "flip_session_cache` WHERE ((`type` = 'log') AND (`session_id` = '{$Session->id}')) ORDER BY `time`,`id`;", "data");
	foreach ($msgs as $m)
		_DoShowLog(unserialize($m));
	MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_session_cache` WHERE ((`type` = 'log') AND (`session_id` = '{$Session->id}'));");
}

function LogSaveAsCookie() {
	global $Errors;
	if (count($Errors[0]) < 1)
		return;
	setcookie("LogCache", Condense($Errors[0]), time() + 3600);
}

function _LogLoadCookie() {
	if (empty ($_COOKIE["LogCache"]))
		return;
	setcookie("LogCache", "", time() - 3600);
	$err = Uncondense($_COOKIE["LogCache"]);
	foreach ($err as $e)
		_DoShowLog($e);
}

/**
 * Die Funktion LogAction() schreibt eine Nachricht in den action-Log. n&auml;here Informationen
 * zum action-Log befinden sich in der Dokumentation zu der Datei core.log.php
 * 
 * @param string $Message die Nachricht
 **/
function LogAction($Message) {
	_DoWriteLog(new LogMsg("action", $Message));
	return true;
}

/**
 * Die Funktion LogChange() schreibt eine Nachricht in den change-Log. n&auml;here Informationen
 * zum change-Log befinden sich in der Dokumentation zu der Datei core.log.php
 * 
 * @param string $Message die Nachricht
 **/
function LogChange($Message) {
	global $User;
	_DoWriteLog(new LogMsg("change", $Message));
	return true;
}

/**
 * Die Funktion LogChange() schreibt eine Nachricht in den dbupdate-Log.
 * 
 * @param string $Message die Nachricht
 **/
function LogDBUpdate($Message) {
	global $User;
	_DoWriteLog(new LogMsg("dbupdate", $Message));
	return true;
}

/**
 * Die Funktion LogNotice() gibt eine Nachricht aus und schreibt diese in den notice-Log.
 * 
 * @param $Message die Nachricht.
 **/
function LogNotice($Message) {
	$m = new LogMsg("notice", $Message);
	_DoWriteLog($m);
	_DoShowLog($m);
	return true;
}

/**
 * Die Funktion LogNetLog() schreibt einen Datensatz in die Tabelle TblPrefix().flip_netlog
 * 
 * @param $Action
 * @param $Description
 * @param unknown $aUser
 * @param string $aSession
 * @param string $IP
 * @param string $HostName
 **/
function LogNetLog($Action, $Description, $aUser = NULL, $aSession = "", $IP = "", $HostName = "", $UserAgent = "") {
	global $User, $Session;

	$NetLogType = ConfigGet("netlog_enabled");
	if ($NetLogType != "Y")
		return true;

	if (!is_object($aSession))
		$aSession = $Session;

	// f&uuml;r tempor&auml;re Sessions wird nicht geloggt. Diese werden zB vom RPC verwendet.
	if (is_object($aSession) && $aSession->Temporary)
		return true;
	if (!is_a($aUser, "user"))
		$aUser = $User;
	if (empty ($IP))
		$IP = $_SERVER["REMOTE_ADDR"];
	if (empty ($HostName))
		$HostName = nslookupbyaddr($IP);
	$p = array (
		"ip" => "",
		"seat" => ""
	);
	
	// Default ist anonymous
	$uid = 1;
	
	if (is_object($aUser)) {
		$p = $aUser->getProperties(array (
			"ip",
			"seat"
		));
		$uid = $aUser->id;
	}

	$sessionID = (is_object($Session)) ? $Session->id : "";

	include_once ("mod/mod.netlog.php");
	NetLogSubmitIPv4(array (
		"action" => $Action,
		"description" => $Description,
		"ipv4" => $IP,
		"hostname" => $HostName,
		"session_id" => $sessionID,
	"time" => time(), "user_id" => $uid, "user_ip" => $p["ip"], "user_seat" => $p["seat"], "useragent" => $UserAgent));
}

/**
 * Die Funktion DisplayErrors() gibt alle Nachtichten aus.
 * Sie sollte nur zu Debuggingzwecken verwendet werden.
 **/
function DisplayErrors() {
	global $Errors;
	echo "<pre>\n";
	echo text_translate("Meldungen") . ":\n";
	print_r($Errors);
	echo "</pre>\n";
}
?>