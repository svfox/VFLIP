<?php

/**
 * 
 * @author Moritz Eysholdt
 * @version $Id: core.config.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined('CORE.CONFIG.PHP'))
	return 0;
define('CORE.CONFIG.PHP', 1);

/**
 * ConfigGet() Liest zu einem Config-Key den Wert aus der Datenbank.
 * Dabei wird die gesamte Konfiguration gecached, so dass sich viele Aufrufe
 * von ConfigGet() nicht negativ auf die Performance auswirken.
 * Wenn der Konfigurationeintrag nicht existiert und ein Wert f&uuml;r $DefValue angegeben wurde,
 * dann wird er automatisch erstellt.
 * 
 * Wenn die User-Properties einen gleichnamigen Key enthalten wie ein Konfigurationseitrag, 
 * dann &uuml;berl&auml;dt der Wert der User-Property den Wert des Konfigurationseintrages.
 * 
 * @param string $Key Der Key des zu lesenden Config-Wertes
 * @param string $DefValue Wird zusammen mit $Key und $Description als Konfurationeintrag gespeichert,
 * wenn der Key noch nicht existiert
 * @param string $Description (siehe $DefValue)
 * @param boolean $UseUserCfg wenn false, kann der User keine Konfigurationeintraege ueberladen.
 * @return string Der Wert zum Konfiguratoneintrag $Key. Ist dieser nicht vorhanden, wird $DefValue zur&uuml;ckgegeben.
 * Wurde $DefValue nicht angegeben, wird NULL zur&uuml;ckgegeben.
 **/
function ConfigGet($Key, $DefValue = NULL, $Description = "", $UseUserCfg = true) {
	global $User;
	static $cfg, $usercfg;
	if (!is_array($cfg)) {
		$cfg = CacheGet('config');
		if (!is_array($cfg))
			$cfg = CacheSet('config', MysqlReadCol('SELECT `key`,`value` FROM `' .
			TblPrefix() . 'flip_config`;', 'value', 'key'), array (
				'flip_config'
			));
	}
	
	if (!is_array($usercfg)) {
		$usercfg = array_intersect(
			array_keys(GetColumns('user')), 
			array_keys($cfg)
		);
	}
	
	if (isset ($cfg[$Key])) {
		if (is_object($User) 
			&& in_array($Key, array_keys(GetColumns('user'))) 
			&& $UseUserCfg
		) {
			return $User->getConfig($Key);
		}
		
		else {
			return $cfg[$Key];
		}
	}
	if (!is_null($DefValue))
		ConfigSet("", $Key, $DefValue, $DefValue, $Description);
	else
		trigger_error_text('Ein Konfigurationseintrag existiert nicht.|Key:'.$Key, E_USER_WARNING);
	return $DefValue;
}

/**
 * ConfigSetByID() Erstellt oder bearbeitet einen Konfigurationeintrag.
 * Wenn $Ident leer ist (empty($Ident) === true), wird ein neuer Konfigurationeintrag
 * erstellt. Ansonsten wird der &uuml;ber $Ident identifizierte Konfigurationseintrag geupdated.
 * 
 * @param string $ID Die ID eines Konfigurationseintrages.
 * @param string $Key Der Key des Konfigurationeintrages. Er darf nicht nur aus Ziffern bestehen.
 * @param string $Value 
 * @param string $Default Ein Default-Wert f&uuml;r $Value.
 * @param string $Description Eine Beschreibung der Funktionalit&auml;t und des Auswirkungen des Konfigurationseintrages.
 * @return boolean true bei Erfolg, ansonsten false
 **/
function ConfigSetByID($ID, $Key, $Value, $Default = null, $Description = null, $Type = null) {
	if (is_numeric($Key)) {
		trigger_error_text(text_translate('Ein Config-Key darf keine Zahl sein.') . '|Key:'.$Key, E_USER_WARNING);
		return false;
	}
	$a = array (
		'key' => $Key,
		'value' => $Value
	);
	$a['default_value'] = (is_null($Default)) ? $Value : $Default;
	if (!is_null($Description))
		$a['description'] = $Description;
	if (!is_null($Type))
		$a['type'] = $Type;
	return MysqlWriteByID(TblPrefix() . 'flip_config', $a, $ID);
}

/**
 * ConfigSet() Erstellt oder bearbeitet einen Configeintrag.

 * @param string $Key Der Key des Konfigurationeintrages. Er darf nicht nur aus Ziffern bestehen.
 * @param string $Value 
 * @param string $Default Ein Default-Wert f&uuml;r $Value.
 * @param string $Description Eine Beschreibung der Funktionalit&auml;t und des Auswirkungen des Konfigurationseintrages.
 * @return boolean true bei Erfolg, ansonsten false
 **/
function ConfigSet($Key, $Value, $Default = null, $Description = null, $Type = null) {
	$key = addslashes($Key);
	$id = MysqlReadField('SELECT `id` FROM `' . TblPrefix() . 'flip_config` WHERE (`key` = \''.$key.'\')', 0, true);
	return ConfigSetByID($id, $Key, $Value, $Default, $Description, $Type);
}

/**
 * ConfigDelete() L&ouml;scht einen Konfigurationseintrag.
 * 
 * @param string $Ident Der Key oder die ID eines Konfigurationeintrages.
 * @return boolean true bei Erfolg, ansonsten false.
 **/
function ConfigDelete($Ident) {
	$i = addslashes($Ident);
	$id = MysqlReadField('SELECT `id` FROM `' . TblPrefix() . 'flip_config` WHERE (`key` = \''.$i.'\') OR (`id` = \''.$i.'\');');
	MysqlExecQuery('DELETE FROM `' . TblPrefix() . 'flip_config_values` WHERE `config_id`=\''.$id.'\'');
	return MysqlDeleteByID(TblPrefix() . 'flip_config', $id);
}

/**
 * ConfigGetAll() Liest alle Konfigurationseintr&auml;ge aus der Datenbank.
 * Wenn es nur darum geht, die Werte bestimmter Keys zu ermitteln, 
 * arbeitet ConfigGet() wesentlich performanter.
 * 
 * @return array Alle Konfigurationeintr&auml;ge sortiert nach Key:
 * <code>
 * array(
 *   "key1" => array("key" => "key1", "value" => "foo", "default_value" => "works_always", "description" => "blablupp"),
 *   "key2" => array("key" => "key2", "value" => "bar", "default_value" => "bar", "description" => ""),
 *   "key_new" => array("key" => "key_new", "value" => "12345", "default_value" => "1337", "description" => ""),
 *   (...)
 * );
 * </code>
 * @see ConfigGet()
 **/
function ConfigGetAll() {
	return MysqlReadArea('SELECT * FROM `' . TblPrefix() . 'flip_config` ORDER BY `key`;', 'key');
}

/**
 * ConfigGetLong() Liest einen gesamten Konfigurationseintrag aus der Datenbank.
 * Wenn es nur darum geht, die Werte bestimmter Keys zu ermitteln, 
 * arbeitet ConfigGet() wesentlich performanter.

 * @param string $Ident Der Key oder die ID eines Konfigurationeintrages.
 * @return array Ein Konfigurationseintrag oder false, wenn dieser nicht gefunden wurde. z.B.:
 * <code>
 * array(
 *   "key" => "key1",
 *   "value" => "foo",
 *   "default_value" => "works_always",
 *   "description" => "blablupp"
 * );
 * </code>
 * @see ConfigGet()
 **/
function ConfigGetLong($Ident) {
	$i = addslashes($Ident);
	return MysqlReadRow('SELECT * FROM `' . TblPrefix() . 'flip_config` WHERE (`key` = \''.$i.'\') OR (`id` = \''.$i.'\');');
}

/**
 * Reads the possible values for a config value.
 * 
 * @param unknown_type $name
 */
function ConfigGetPossibleValues($name = '') {
	if(empty($name)) {
		trigger_error_text(text_translate('Der name eines Config-Werts wurde nicht angegeben!'), E_USER_ERROR);
		return array();
	}
	
	$valueID = MysqlReadField('SELECT `id` FROM `' . TblPrefix() . 'flip_config` WHERE `key` = \'' . escape_sqlData_without_quotes($name) . '\'', 'id');
	
	if($valueID === false) {
		trigger_error_text(text_translate('Der Config-Wert "?1?" wurde nicht gefunden!', $name), E_USER_ERROR);
		return array();
	}
	
	return MysqlReadCol('SELECT `value`, `id`  FROM `' . TblPrefix() . 'flip_config_values` WHERE `config_id` = ' . escape_sqlData_without_quotes($valueID), 'value', 'value');
}
?>