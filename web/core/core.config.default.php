<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: core.config.default.php 1415 2007-06-18 13:56:00Z loom $
 * @copyright (c) 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 * 
 * Dies ist die zentrale Konfigurationsdatei des FLIPs. Sie enth&auml;lt nur grundlegende Optionen um
 * das FLIP lauff&auml;hig zu machen. Alles weitere kann (sobald das FLIP l&auml;uft) &uuml;ber das Webinterface unter 
 * admin -> config angepasst werden.
 **/

// WICHITG! Das Rootpassword sollte ge&auml;ndert werden!
$CoreConfig["root_password"] = md5("thisisareallylongpassword"); // Wird f&uuml;r serverseitige synchrone Verschl&uuml;sselung verwendet.

// Dieser Pr&auml;fix muss in dem initialen SQL-Dump entsprechend angepa&szlig;t werden.
$CoreConfig["table_prefix"] = ""; // Pr&auml;fix f&uuml;r jede Tabelle dieser Instanz des Flip

$CoreConfig["db_host"] = "localhost"; // Adresse des Datenbankservers
$CoreConfig["db_port"] = "3306"; // Port des Datenbankservers oder Pfad des MySQL-Sockets
$CoreConfig["db_name"] = "flip"; // Name der zu verwendeden Datenbank
$CoreConfig["db_user"] = "root"; // Benutzername mit dem sich das FLIP bei der Datenbank anmeldet.
$CoreConfig["db_pass"] = ""; // Das zum Benutzernamen geh&ouml;rige Passwort
$CoreConfig["db_writelock"] = false; // Wenn true, werden keine Schreibzugriffe auf die DB erlaubt.
$CoreConfig["db_persistent"] = false; // Soll eine dauerhafte Verbindung zur Datenbank aufgebaut werden? (pconnect statt connect)
$CoreConfig["db_performancelogdir"] = ""; // sollen Performancerelevante Informationen geloggt werden? ACHTUNG: Die entstehende Datenmenge ist nicht unerheblich!
$CoreConfig["db_performancelog_mintime"] = "50"; // Zeit in ms die ein Query &uuml;berschreiten muss, um im Perflog erw&auml;hnt zu werden. 
$CoreConfig["db_performancelog_mincount"] = "10"; // Anzahl der Queries pro Seitenaufruf, der &uuml;berschritten werden muss, damit er im Perflog auftaucht.
$CoreConfig["db_compression"] = false; // sollen die Daten f&uuml;r den Transfer zwischen Datenbank und Webserver komprimiert werden?

$CoreConfig["cache_path"] = ""; // Pfad, in dem Datenbankzugriffe gecached werden.
$CoreConfig["image_tmp"] = "tmp/img/"; // Pfad, in dem &uuml;ber http zug&auml;ngliche Bilder abgelegt werden.
$CoreConfig["text_tmp"] = "tmp/txt/"; // Pfad, in dem Texte aus der Datenbank gecached werden.


/*
Ein Log kann in mehrere Dateien gleichzeitig geschrieben werden, ihre Namen m&uuml;ssen dann durch getrennt durch ein "|" angegeben werden.
Beispiel: $CoreConfig["log_action"] = "log1dir/log.action.php|log45dir/log.action.php";

Wenn "db" als Dateiname angegeben wird, so schreibt das FLIP die Logeintr&auml;ge in die Datenbank. Von dort
k&ouml;nnen sie mittels log.php?frame=viewdblog ausgelesen werden. Der Changelog wird standardm&auml;&szlig;ig in eine 
Datei und in die Datenbank geschrieben.

Es ist nicht schlimm, wenn die Logfiles in einem von au&szlig;en &uuml;ber HTTP erreichbaren Verzeichnis liegen.
Zumindest so lange sie die Dateiendung .php haben. Sie beginnen mit ein wenig PHP-Code (<?php return 0; ?>), 
so dass der Webserver niemals ihren Inhalt preisgeben wird.
*/
 
$CoreConfig["log_dir"]       = "log/";// ACHTUNG! der Webserver ben&ouml;tigt Schreibrechte im Logverzeichnis! 
$CoreConfig["log_error"]     = "$CoreConfig[log_dir]log.error.php"; // Fehler, die zum sofortigen Abbruch des Scripts f&uuml;hrten.
$CoreConfig["log_warning"]   = "$CoreConfig[log_dir]log.warning.php"; // Warnungen...
$CoreConfig["log_notice"]    = ""; // Hinweise...
$CoreConfig["log_action"]    = "$CoreConfig[log_dir]log.action.php"; // Was der User so treibt...
$CoreConfig["log_change"]    = "$CoreConfig[log_dir]log.change.php|db"; // Was an der Page so ge&auml;ndert wird.
$CoreConfig["log_dbupdate"]  = "$CoreConfig[log_dir]log.dbupdate.php|db"; // Der Log des automatischen Datenbankupdates.
$CoreConfig["log_dbtimeout"] = "30"; // Die log-Eintr&auml;ge in der Datenbank werden nach x Tagen gel&ouml;scht.

$CoreConfig["debug_backtrace"] = false; // Ob bei einem Fehler auch ein Backtrace angezeigt werden soll.
$CoreConfig["debug_backtrace_log"] = false; // Ob der Backtrace auch im Logfile gespeichert werden soll.


// Verzeichnisse erstellen, falls nicht vorhanden.
if(!empty($CoreConfig["db_performancelogdir"])) ForceDir($CoreConfig["db_performancelogdir"],E_USER_ERROR);
if(!empty($CoreConfig["cache_path"])) ForceDir($CoreConfig["cache_path"],E_USER_ERROR);
ForceDir($CoreConfig["log_dir"],E_USER_ERROR);
ForceDir($CoreConfig["image_tmp"],E_USER_ERROR);
ForceDir($CoreConfig["text_tmp"],E_USER_ERROR);

if(empty($CoreConfig["root_password"])) die("Das Root-Passwort wurde nicht gesetzt.");


?>
