<?php

/**
 * @author Daniel Raap
 * @version $Id: lanparty.php 1500 2018-06-09 13:17:11Z loom $ edit by vulkanlan
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class ImportPage extends Page {
	function framelansurfer() {
		$this->Caption = "Lansurfer User-/Sitzplanimport";
		return array (
			"users" => "Y",
			"seats" => "Y"
		);
	}

	function frameDefault($GetVars, $PostVars) {
		$r = array();
		
		return $r;
	}
	
	function frameClansphereImport() {
		$r = array();
		
		$this->Caption = text_translate('Clansphere importieren');
		
		return $r;
	}
	
	function submitImportClansphere($getVars) {
		if(!isset($getVars['dbserver']) 
			|| !isset($getVars['dbuser']) 
			|| !isset($getVars['dbpass']) 
			|| !isset($getVars['dbname'])
		) {
			trigger_error_text(text_translate('Es fehlen Daten, die f&uuml;r den Import n&ouml;tig sind!'), E_USER_ERROR);
			return array();	
		}
		
		$r = array();
		$clansphere = mysqli_connect($getVars['dbserver'], $getVars['dbuser'], $getVars['dbpass']);
		
		if($clansphere === false) {
			trigger_error_text(text_translate('Konnte mich nicht mit dem Server verbinden!', E_USER_ERROR));
			trigger_error_text(mysqli_error(), E_USER_ERROR);
			return array();
		}
		
		if(!mysqli_select_db($getVars['dbname'], $clansphere)) {
			trigger_error_text(text_translate('Es gibt keine Clansphere-Datenbank namens "' . $getVars['dbname'] . '"!', E_USER_ERROR));
			return array();			
		}
		
		$cs_users = mysqli_query('SELECT * FROM `cs_users`', $clansphere);
		
		if(($cs_users === false)) {
			trigger_error_text(text_translate('Konnte die Benutzertabelle nicht auslesen!', E_USER_ERROR));
			return array();				
		}

		while($user = mysqli_fetch_assoc($cs_users)) {
			$newUser = CreateSubject('user', $user['users_nick'], $user['users_email']);
			
			if(!is_a($newUser, 'User')) {
				trigger_error_text(text_translate('Konnte den User "' . $user['users_nick'] . '" nicht importieren!'), E_USER_WARNING);
				continue;
			}
			
			// Import just the necessary fields
			$newUser->setProperty('email', $user['users_email']);
			$newUser->setProperty('givenname', $user['users_name']);
			$newUser->setProperty('familyname', $user['users_surname']);
			$newUser->setProperty('password', md5(time()));
			
			// Change the password
			$passOK = MysqlWrite(
				'UPDATE `flip_user_data` ' 
					. 'SET `val` = \'' .escape_sqlData_without_quotes($user['users_pwd']) . '\' ' 
					. 'WHERE `subject_id` = ' . $newUser->id . ' AND `column_id` = 7'
			);
			
			if($passOK === false) {
				trigger_error_text(text_translate('Konnte das Passwort von "' . $user['users_nick'] . '" nicht importieren!'), E_USER_WARNING);
				continue;				
			} else {
				trigger_error_text(text_translate('Benutzer "' . $user['users_nick'] . '" importiert!'));
			}
		}
		
		trigger_error_text(text_translate('User importiert!'));
		
		return $r;
	}
	
	function framelscheck($get, $post) {
		global $User;
		$User->requireRight("edit_db");
		//save the file
		move_uploaded_file($_FILES["xmldatei"]["tmp_name"], "lansurfer.xml");
		/*if(!is_file("lansufer.xml"))
		{
		  trigger_error_text("Die Datei konnte nicht als './lansurfer.xml' gespeichert werden!", E_USER_ERROR);
		  return array();
		}*/
		//parse the file
		$r = simplexml_load_file("lansurfer.xml");

		$this->Caption = "Daten von \"" . utf8_decode($r->LANsurfer_header->event) . "\" vervollst&auml;ndigen";

		//check seats
		$blocks = array ();
		if ($post["seats"] == "Y") {
			foreach ($r->importdata->seat_blocks->block AS $block) {
				$blocks["blocks"][]["name"] = $block->name;
			}
			if (count($blocks["blocks"]) == 1)
				unset ($blocks); //for just one block we don't need to ask
		}
		return array_merge($blocks, $post);
	}

	function submitlansurfer($post) {
		global $User;
		$User->requireRight("edit_db");

		//check file
		if (!is_file("lansurfer.xml")) {
			trigger_error_text("Die XML-Datei ist nicht vorhanden!");
			return false;
		}

		set_time_limit(0);
		srand((float) microtime() * 1000000);

		$xmldata = simplexml_load_file("lansurfer.xml");

		//users
		if ($post["users"] == "Y") {
			$users = 0;
			$createdusers = array ();
			foreach ($xmldata->importdata->users->user AS $user) {
				//Nickname pr&uuml;fen:
				//Whitespace entfernen
				$user->username = trim(utf8_decode($user->username));
				$user->email = utf8_decode($user->email);
				//wenn der Name eine Email-Adresse ist, wird das @ durch ein $ ersetzt
				if (IsValidEmail($user->username))
					$user->username = strtr($user->username, "@", "$");
				//emailadresse g&uuml;ltig?
				if (!IsValidEmail($user->email))
					$user->email = md5($user->email) . "@invalidemail.com";
				//bei doppelten Usernamen wird eine Zufallszahl angeh&auml;ngt
				if (in_array(strtolower($user->username), $createdusers)) {
					$user->username = $user->username . "_" . rand();
				}
				//bei nur einem Zeichen einen Unterstrich anh&auml;ngen
				if (strlen($user->username) < 2)
					$user->username .= "_";
				//User erstellen
				$newflipuser = CreateSubject("user", $user->username, $user->email);
				if (!is_object($newflipuser)) {
					trigger_error_text("Fehler: " . $user->username . " konnte nicht erstellt werden.", E_USER_WARNING);
					continue;
				}
				$createdusers[] = strtolower($user->username);
				//Eigenschaften
				$newflipuser->setProperties(array (//lansurfer data (XML)
					"givenname" => utf8_decode($user->firstname
				), "familyname" => utf8_decode($user->name), "password" => utf8_decode($user->password), "clan" => utf8_decode($user->clan), "tournament_wwcl_player_id" => utf8_decode($user->wwclid), "tournament_wwcl_clan_id" => utf8_decode($user->wwclclanid), "homepage" => utf8_decode($user->homepage),
				//flipdata (internal)
				"enabled" => "Y", "registration_time" => time(), "random_id" => md5(rand()),));
				//Rechte (user, orga)
				switch ($user->type) {
					case "3" :
						$newflipuser->addParent("orga");
						$newflipuser->addParent("registered");
						break;
					case "1" :
					default :
						$newflipuser->addParent("registered");
				}
				//Partystatus (angemeldet, bezahlt)
				require_once ("mod/mod.user.php");
				if ($user->paid == "1")
					UserSetStatus($newflipuser, "paid");
				elseif ($user->type != "3") UserSetStatus($newflipuser, "registered");
				$users++;
			}
		}

		if ($post["seats"] == "Y") {
			//seat_blocks
			$userids = MysqlReadCol("SELECT `id`, `email` FROM `" . TblPrefix() . "flip_user_subject` WHERE `type`='user'", "id", "email");

			//calculation data (imagesizes)
			require_once ("mod/mod.seats.php");
			$imagedata = GetSeatImage("default");

			$i = 1;
			$tallestblocks = $widestblocks = array ();
			//read blockdata
			foreach ($xmldata->importdata->seat_blocks->block AS $block) {
				//check coordinates
				if (!preg_match("/^[0-9]+\/[0-9]+$/", $_POST["coord$i"])) {
					trigger_error_text("Keine g&uuml;ltigen Koordinaten f&uuml;r " . $block->name . " (" . $_POST["coord$i"] . ")", E_USER_ERROR);
					return false;
				}
				//tablesize
				if ($block->orientation == "1") {
					$width = $imagedata["height"];
					$height = $imagedata["width"];
				} else {
					$width = $imagedata["width"];
					$height = $imagedata["height"];
				}

				//space (separators)
				$xspace = $yspace = 0;
				$xspacers = $yspacers = array ();
				foreach ($block->seat_sep->sep AS $space) {
					if ($space->orientation == "1") {
						$xspacers[] = (integer) $space->value;
						$xspace += 2 * $width;
					} else {
						$yspacers[] = (integer) $space->value;
						$yspace += 2 * $height;
					}
				}

				//blocksize
				$width = ($block->cols + 1) * $width + $xspace;
				$height = ($block->rows + 1) * $height + $yspace;

				//save it
				$coords = explode("/", $_POST["coord$i"]);
				$blocks[$coords[1]][$coords[0]] = $block; //$blocks[y][x]
				$blocksizes[$coords[1]][$coords[0]]["width"] = $width;
				$blocksizes[$coords[1]][$coords[0]]["height"] = $height;
				$blocksizes[$coords[1]][$coords[0]]["xspacers"] = $xspacers;
				$blocksizes[$coords[1]][$coords[0]]["yspacers"] = $yspacers;
				if ($width > $widestblocks[$coords[1]])
					$widestblocks[$coords[0]] = $width;
				if ($height > $tallestblocks[$coords[0]])
					$tallestblocks[$coords[1]] = $height;
				$i++;
			}

			//create plan
			//...dimensions
			$planwidth = 100; //border (left+right)
			$planheight = 100; //border (top+bottom)
			$blockspace = 50; //space between blocks
			foreach ($widestblocks AS $blockwidth)
				$planwidth += $blockwidth;
			foreach ($tallestblocks AS $blockheight)
				$planheight += $blockheight;
			foreach ($blocksizes AS $y)
				foreach ($y AS $x) {
					$planwidth += $blockspace;
					$planheight += $blockspace;
				}
			$planwidth -= $blockspace;
			$planheight -= $blockspace;
			//...resource
			require_once ("mod/mod.imageedit.php");
			$baseplan = new ResImage(new ImageLoaderNew($planwidth, $planheight));
			$baseplan->saveToDB(ConfigGet("seats_baseplan"));

			//calculate/place tables
			$blockcount = 0;
			MysqlWrite("DELETE FROM " . TblPrefix() . "flip_seats_seats");
			foreach ($blocks AS $y => $xarr) {
				foreach ($xarr AS $x => $block) {
					$xstart = $ystart = 0;
					for ($i = 0; $i < $x; $i++)
						$xstart += $blocksizes[$y][$i]["width"];
					$xstart += $blockspace * $i;
					for ($i = 0; $i < $y; $i++)
						$ystart = $blocksizes[$i][$x]["height"];
					$ystart += $blockspace * $i;
					//stehen die Tische quer oder hoch?
					if ($block->orientation == "1") {
						$width = $imagedata["height"];
						$height = $imagedata["width"];
					} else {
						$width = $imagedata["width"];
						$height = $imagedata["height"];
					}
					foreach ($block->seat_seats->seat AS $seat) {
						//Tischkoordinaten
						$xspace = $yspace = 0;
						foreach ($blocksizes[$y][$x]["xspacers"] AS $sep)
							if ($sep <= $seat->col)
								$xspace += 2 * $width;
						foreach ($blocksizes[$y][$x]["yspacers"] AS $sep)
							if ($sep <= $seat->row)
								$yspace += 2 * $height;
						//          blockorigin     seatorigin      channels         center (half) table    border
						$center_x = $xstart + $seat->col * $width + $xspace +round($imagedata["width"] * 0.5) + 50; //border
						$center_y = $ystart + $seat->row * $height + $yspace +round($imagedata["height"] * 0.5) + 50; //border

						//Data 4 DB
						if ($block->orientation == "1")
							$rotate = ($seat->col % 2 == 0) ? 180 : 0;
						else
							$rotate = ($seat->row % 2 == 0) ? 180 : 0;
						$angle = ($block->orientation * 90) + $rotate;
						$seat->owner = utf8_decode($seat->owner);
						if (!IsValidEmail($seat->owner))
							$seat->owner = md5($seat->owner) . "@invalidemail.com";
						$email = (string) $seat->owner;
						if ($seat->status == "2")
							$reserved = "Y";
						else
							$reserved = "N";
						//IP-Vergabe
						$blockno = explode("Block ", $block->name);
						$hundret = ($seat->row % 2 == 0) ? 1 : 2;
						$ip = "172.16." . $blockno[1] . round(($seat->row + 1) / 2, 0) . "." . $hundret . sprintf("%02d", $seat->col + 1);
						$dbseat = array (
							"enabled" => "Y",
							"center_x" => $center_x,
							"center_y" => $center_y,
							"angle" => $angle,
							"user_id" => $userids[$email],
							"reserved" => $reserved,
							"name" => $block->name . " " . chr($seat->row + 65
						) . ($seat->col + 1), "ip" => $ip);
						MysqlWriteByID(TblPrefix() . "flip_seats_seats", $dbseat);
					}
					$blockcount++;
				}
			}
			$seatdonetext = " und $blockcount Sitzbl&ouml;cke";
		}

		//Datei l&ouml;schen, da sie sonst von jedem eingesehen werden kann
		if (unlink("lansurfer.xml"))
			$delfile = " Die XML-Datei wurde gel&ouml;scht.";
		$logtext = "$users User$seatdonetext wurden importiert.$delfile";
		LogAction($logtext);
		return $logtext;
	}

	function frametshserver() {
		global $User;
		$User->requireRight("edit_db");

		//some config
		$editright = "server_edit_own";
		$table = "tsh_lan_server";

		if (!$allserver = MysqlReadArea("SELECT * FROM $table"))
			return trigger_error_text("Daten konnten nicht aus der Tabelle '$table' gelesen werden!", E_USER_ERROR);

		$created = array ();
		foreach ($allserver AS $server) {
			if (!($owner = MysqlReadField("SELECT id FROM " . TblPrefix() . "flip_user_subject WHERE name='" .escape_sqlData($server["lansurfer_nick"]) . "'", "id", true)))
				if (!($owner = MysqlReadField("SELECT id FROM " . TblPrefix() . "flip_user_subject WHERE email='" .escape_sqlData($server["mail"]) . "'", "id", true))) {
					trigger_error_text("User '" . $server["lansurfer_nick"] . "' wurde nicht gefunden!<br>\n", E_USER_WARNING);
					continue;
				}
			//create new Serversubject
			if (in_array($server["dns"], $created))
				$server["dns"] = $server["dns"] . $server["id"];
			if (!$newserver = CreateSubject("server", $server["dns"])) {
				trigger_error_text("Fehler beim Erstellen des Servers!", E_USER_WARNING);
				continue;
			}
			$created[] = $server["dns"];
			//set rights
			$newserver->addParent("servers");
			$u = CreateSubjectInstance($owner);
			$u->addRight($editright, $newserver);
			//set properties
			$newserver->setProperties(array (
				"name" => $server["dns"],
				"owner_id" => $u->id,
				//"hardware"=>"",
				"description" => $server["misc"],
				//"os"=>"",
				"services" => $server["content"],
				"atparty" => "Y"
			));
			trigger_error_text($newserver->name . " wurde hinzugef&uuml;gt.");
		}
		$this->TplFile = "text.tpl";
		$this->TplSub = "text";
		return array ();
	}
}
$User->requireRight("edit_db");
RunPage("ImportPage");
?>
